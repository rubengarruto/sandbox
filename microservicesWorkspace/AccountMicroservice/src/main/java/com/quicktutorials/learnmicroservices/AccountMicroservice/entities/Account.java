package com.quicktutorials.learnmicroservices.AccountMicroservice.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="accounts")
public class Account {
	
	@Id
	@Column(name="ID")
	@NotNull@NotEmpty@NotBlank
	private String id;

	@Column(name="FK_USER")
	@NotNull@NotEmpty@NotBlank
	private String fkUser;

	@Column(name="TOTAL")
	private double total;
	
}
