package com.quicktutorials.learnmicroservices.AccountMicroservice.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.quicktutorials.learnmicroservices.AccountMicroservice.entities.User;

public interface UserDao extends JpaRepository<User, String>{ 
	//extends JpaRepository<User, String> ci evita di creare l'implementazione per sfruttare i metodi predefiniti
	
	//metodi Customizzati
	Optional<User>  findById(String id);
	

}
