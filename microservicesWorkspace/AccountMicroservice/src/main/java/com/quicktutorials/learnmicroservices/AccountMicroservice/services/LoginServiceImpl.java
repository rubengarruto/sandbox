package com.quicktutorials.learnmicroservices.AccountMicroservice.services;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quicktutorials.learnmicroservices.AccountMicroservice.AccountMicroserviceApplication;
import com.quicktutorials.learnmicroservices.AccountMicroservice.daos.UserDao;
import com.quicktutorials.learnmicroservices.AccountMicroservice.entities.User;
import com.quicktutorials.learnmicroservices.AccountMicroservice.utils.EncryptionUtils;
import com.quicktutorials.learnmicroservices.AccountMicroservice.utils.JwtUtils;
import com.quicktutorials.learnmicroservices.AccountMicroservice.utils.UserNotLoggedException;


@Service
public class LoginServiceImpl implements LoginService {

	private static final Logger log = LoggerFactory.getLogger(AccountMicroserviceApplication.class);
	private static final String myLog="[MyLog] --- ";

	    @Autowired
	    UserDao userDao;

	    @Autowired
	    EncryptionUtils encryptionUtils;



	    @Override
	    public Optional<User> getUserFromDbAndVerifyPassword(String id, String password) throws UserNotLoggedException{

	        Optional<User> theUser = userDao.findById(id);
	        if(theUser.isPresent()){
	            User user = theUser.get();
	            if(encryptionUtils.decrypt(user.getPassword()).equals(password)){
	                log.info(myLog+"Username and Password verified");
	            }else{
	                log.info(myLog+"Username verified. Password not");
	                throw new UserNotLoggedException("Password/Username not correct");
	            }
	        }
	        return theUser;
	    }


	    @Override
	    public String createJwt(String subject, String name, String permission, Date datenow) throws UnsupportedEncodingException{
	        Date expDate = datenow;
	        expDate.setTime(datenow.getTime() + (300*1000)); //durata del Token [Scadenza] 300*1000 =5 min
	        log.info(myLog+"JWT Creation. Expiration time: " + expDate.getTime());
	        String token = JwtUtils.generateJwt(subject, expDate, name, permission);
	        return token;
	    }


	    @Override
	    public Map<String, Object> verifyJwtAndGetData(HttpServletRequest request) throws UserNotLoggedException, UnsupportedEncodingException{
	        String jwt = JwtUtils.getJwtFromHttpRequest(request);
	        if(jwt == null){
	            throw new UserNotLoggedException("Authentication token not found in the request");
	        }
	        Map<String, Object> userData = JwtUtils.jwt2Map(jwt);
	        return userData;
	    }


	}