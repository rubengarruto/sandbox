package com.quicktutorials.learnmicroservices.AccountMicroservice.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="users") //mappa  questa entity sulla tabella presente a db di nome "users"
public class User {

	@Id
	@Column(name="ID")
	@NotNull@NotEmpty@NotBlank
	private String id;
	
	@Column(name="USERNAME")
	@NotNull@NotEmpty@NotBlank
	private String username;
	
	@Column(name="PASSWORD")
	@NotNull@NotEmpty@NotBlank
	private String password;
	
	@Column(name="PERMISSION")
	@NotNull@NotEmpty@NotBlank
	private String permission;
	
}
