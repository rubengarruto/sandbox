package com.quicktutorials.learnmicroservices.AccountMicroservice.utils;



@SuppressWarnings("serial")
public class UserNotLoggedException extends Exception {

	public UserNotLoggedException(String errorMessage) {	
		
		super(errorMessage);
	}
	
	
	
	
}
