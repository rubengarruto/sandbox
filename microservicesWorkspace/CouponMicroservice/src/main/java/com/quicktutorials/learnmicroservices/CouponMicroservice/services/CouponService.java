package com.quicktutorials.learnmicroservices.CouponMicroservice.services;

public interface CouponService {
	
	String getAvailableCoupons(String jwt);

}
