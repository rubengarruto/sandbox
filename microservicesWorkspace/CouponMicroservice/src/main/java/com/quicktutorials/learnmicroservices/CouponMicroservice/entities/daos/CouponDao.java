package com.quicktutorials.learnmicroservices.CouponMicroservice.entities.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.quicktutorials.learnmicroservices.CouponMicroservice.entities.Coupon;

public interface CouponDao extends JpaRepository<Coupon, Integer> {
	
	Optional<Coupon>findByAccount(String account);
}
