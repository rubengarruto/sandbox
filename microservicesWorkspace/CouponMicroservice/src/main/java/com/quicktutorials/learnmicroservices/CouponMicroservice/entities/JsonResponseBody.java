package com.quicktutorials.learnmicroservices.CouponMicroservice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JsonResponseBody{
   
    private int server;
 
    private Object response;
}