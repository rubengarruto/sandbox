package com.ministeroSabaoth.lifeGroupManagerDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeGroupManagerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LifeGroupManagerDemoApplication.class, args);
	}

}
