import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "LIFERTAB")
@Data
public class lifer  implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
	@Column(name = "lifer_id")
	private String idLifer;
	
	@Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
	private String lastName;

    @Column(name = "email")
    private String email;
    
    @Column(name = "telephone")
    private String tel;
    
    @ManyToOne
	@JoinColumn(name = "id_ministerio", referencedColumnName = "idMinisterio")
	private String idMinisterio="NO_ROLE";  
}