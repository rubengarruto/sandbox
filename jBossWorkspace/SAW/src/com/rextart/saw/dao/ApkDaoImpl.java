package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabApk;


@Repository
public class ApkDaoImpl implements ApkDao{

	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public TabApk getLastVersion() {
		TabApk entityEmpty = new TabApk();
		entityEmpty.setNumeVersione(0);
		
		List<TabApk> list=new ArrayList<TabApk>();
		try {
			list = (List<TabApk>)em.createNamedQuery("TabApk.getLastVersion").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list.size()>0 ? list.get(0) : entityEmpty;
	}
	
	
	@SuppressWarnings("unchecked")
	public Integer getNumeLastVersion() {		
		List<Integer> list= new ArrayList<Integer>();
		try {
			list = (List<Integer>)em.createNamedQuery("TabApk.getNumeLastVersion").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list.size()>0 ? list.get(0) : 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] getScriptDbFromVersion(int startVersion,int currentVersion){
		List<String> list= new ArrayList<String>();
		Object[] obj = new Object[2];
		try {
			list = (List<String>)em.createNamedQuery("TabApk.getScriptDbFromVersion").setParameter("numeVersione", startVersion).getResultList();
			TabApk apk =  (TabApk) em.createNamedQuery("TabApk.getApkLastVersion").setParameter("numeVersione", currentVersion).getSingleResult();
			obj[0]=list;
			obj[1]=apk;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	public List<TabApk> getSqlFromVersion(int startVersion){
		List<TabApk> list=null;
		try {
			list = (List<TabApk>)em.createNamedQuery("TabApk.getFromVersion").setParameter("numeVersione", startVersion).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list.size()>0 ? list : null;
	}
}
