package com.rextart.saw.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabAnagOperTrace;
import com.rextart.saw.entity.TabLoginFailed;
import com.rextart.saw.entity.TabOperTrace;
import com.rextart.saw.entity.ViwOperTrace;


@Repository
public class LogOperationDaoImpl implements LogOperationDao{

	
	@PersistenceContext
    private EntityManager em;
	
		
	@Override
	public void logOperation(int codiAnagOperTrace,Timestamp timeDataOper,Integer codiUtente,String descDettaglio){
		try{
			TabOperTrace tabOperTrace = new TabOperTrace();
			tabOperTrace.setTimeDataOper(timeDataOper);
			tabOperTrace.setCodiUtente(codiUtente);
			tabOperTrace.setCodiAnagOperTrace(codiAnagOperTrace);
			tabOperTrace.setDescDettaglio(descDettaglio);
			em.persist(tabOperTrace);
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ViwOperTrace> getAllOperTrace() {
		List<ViwOperTrace> listaOperTrace = new ArrayList<ViwOperTrace>();
		try {
			listaOperTrace = em.createNamedQuery("ViwOperTrace.findAll").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaOperTrace;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagOperTrace> getAllAnagOperTrace() {
		List<TabAnagOperTrace> listaAnagOperTrace = new ArrayList<TabAnagOperTrace>();
		try {
			listaAnagOperTrace = em.createNamedQuery("TabAnagOperTrace.findAll").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaAnagOperTrace;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<TabLoginFailed> getAllLoginFailed() {
		List<TabLoginFailed> listaLoginFailed = new ArrayList<TabLoginFailed>();
		try {
			listaLoginFailed = em.createNamedQuery("TabLoginFailed.findAll").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaLoginFailed;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<TabLoginFailed> getAllLoginFailedByIp(String ipUser) {
		List<TabLoginFailed> listaLoginFailed = new ArrayList<TabLoginFailed>();
		try {
			listaLoginFailed = em.createNamedQuery("TabLoginFailed.findByIp").setParameter("ipUser", ipUser).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaLoginFailed;
	}


	@Override
	public void saveLoginFailed(TabLoginFailed tabLoginFailed) {
		try {
			em.persist(tabLoginFailed);
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void removeLoginFailed(String ipUser) {
		try {
			em.createNamedQuery("TabLoginFailed.deleteByIp").setParameter("ipUser", ipUser).executeUpdate();
			em.flush();			
		} catch (Exception e) {
		}
	}
	
}
