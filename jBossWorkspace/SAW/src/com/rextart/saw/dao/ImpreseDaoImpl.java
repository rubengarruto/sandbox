package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabImprese;

@Repository
public class ImpreseDaoImpl implements ImpreseDao {
	
	@PersistenceContext
    private EntityManager em;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabImprese> getAllImprese() {
		List<TabImprese> listImprese = new ArrayList<TabImprese>();
		try {
			listImprese = em.createNamedQuery("TabImprese.findAll").getResultList();
		} catch (Exception e) {}
		return listImprese;
	}
	
	 public void saveImpresa(TabImprese tabImprese){
		 try {
			 em.persist(tabImprese);
			 em.flush();
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
    	
    }
	 
	 public void updateImpresa(TabImprese tabImprese){
		 try {
			 em.merge(tabImprese);
			 em.flush();
		 } catch (Exception e) {}
	 }
	 
	 @Override
	 public TabImprese getImpresaById(int codiImpresa) {
		TabImprese tab = null;
		try {
			tab = em.find(TabImprese.class, codiImpresa);
			em.refresh(tab);
		} catch (Exception e) {}
		return tab;
	 }
	 
	 @Override
		public Object[] removeImpresa(TabImprese tabImprese) {
			Object[] obj = new Object[2];
			boolean result = false;
			int codi = 0;
			try {
				tabImprese = em.find(TabImprese.class, tabImprese.getCodiImpresa());
				em.remove(tabImprese);
				em.flush();
				codi = tabImprese.getCodiImpresa();
				result = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			obj[0] = result;
			obj[1] = codi;
			return obj;
		}

	@Override
	public TabImprese getImpresaByCF(String descCf) {
		TabImprese tabImpresa = null;
		try {
			tabImpresa = (TabImprese) em.createNamedQuery("TabImprese.findByDescCf").setParameter("descCf", descCf).getSingleResult();
		} catch (Exception e) {}
		return tabImpresa;
	}

	@Override
	public TabImprese getImpresaByCFAndByCodiImpresa(String descCf,Integer codiImpresa) {
		TabImprese tabImpresa = null;
		try {
			tabImpresa = (TabImprese) em.createNamedQuery("TabImprese.getImpresaByCfAndByCodiImpresa").setParameter("descCf", descCf).setParameter("codiImpresa", codiImpresa).getSingleResult();
		} catch (Exception e) {}
		return tabImpresa;
	}

}
