package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.entity.TabSchedaDyn;


@Repository
public class RepositoryMediaDaoImpl implements RepositoryMediaDao {


	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Object[] saveRepositoryMedia(TabRepositoryMedia tabRepositoryMedia, boolean insert) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		try {
			if(insert){
				em.persist(tabRepositoryMedia);
			}
			else{
				em.merge(tabRepositoryMedia);
			}
			codi = tabRepositoryMedia.getCodiRepositoryMedia();
			em.flush();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryMedia> getRepTabMediaByFilePath(String descPath) {
		List<TabRepositoryMedia> list = new ArrayList<TabRepositoryMedia>();
		try {
			list = em.createNamedQuery("TabRepositoryMedia.findRepMediaByFilePath").setParameter("descPath", descPath).getResultList();
		} catch (Exception e) {}
		return list;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryDocument> getRepDocExist(Integer codiSessione, String descNome) {
		List<TabRepositoryDocument> list = new ArrayList<TabRepositoryDocument>();
		try {
			list = em.createNamedQuery("TabRepositoryDocument.findBySessioneNome").setParameter("codiSessione", codiSessione).setParameter("descNome", descNome).getResultList();
		} catch (Exception e) {}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryDocument> getRepDocExistBytipo(Integer codiSessione, Integer tipoFile) {
		List<TabRepositoryDocument> list = new ArrayList<TabRepositoryDocument>();
		try {
			list = em.createNamedQuery("TabRepositoryDocument.findBySessioneTipo").setParameter("codiSessione", codiSessione).setParameter("tipoFile", tipoFile).getResultList();
		} catch (Exception e) {}
		return list;
	}
	
	@Override
	public Object[] saveRepositoryDocument(TabRepositoryDocument tabRepositoryDocument, boolean insert) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		try {
			if(insert){
				em.persist(tabRepositoryDocument);
				em.flush();
				result = true;
			}else{
				em.merge(tabRepositoryDocument);
				em.flush();
				result = true;
			}
			
		} catch (Exception e) {
			System.out.println("Errore nel salvataggio del documento in TabRepositoryDocument ");
			e.printStackTrace();
		}
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}
	
	public TabRepositoryDocument getTabRepositoryDocumentVerbaleD1(Integer codiSessione) {
		TabRepositoryDocument doc = null;
		try {
			doc = (TabRepositoryDocument) em.createNamedQuery("TabRepositoryDocument.findVerbaleD1ByCodiSessione").setParameter("codiSessione", codiSessione).getSingleResult();
		} catch (Exception e) {}
		return doc;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryDocument> getRepositoryDocumentByCodiSessione(Integer codiSessione){
		List<TabRepositoryDocument> list= new ArrayList<TabRepositoryDocument>();
		try {
			list = (List<TabRepositoryDocument>) em.createNamedQuery("TabRepositoryDocument.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryMedia> getRepositoryMediaByCodiSessione(Integer codiSessione){
		List<TabRepositoryMedia> list= new ArrayList<TabRepositoryMedia>();
		try {
			list = (List<TabRepositoryMedia>) em.createNamedQuery("TabRepositoryMedia.findRepMediaByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRepositorySchedaRischi> getRepSchedaRischiExist(Integer codiSessione) {
		List<TabRepositorySchedaRischi> list = new ArrayList<TabRepositorySchedaRischi>();
		try {
			list = em.createNamedQuery("TabRepositorySchedaRischi.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return list;
	}
	
	@Override
	public Object[] saveRepositorySchedaRischi(TabRepositorySchedaRischi tabRepositorySchedaRischi) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		try {
				em.persist(tabRepositorySchedaRischi);
				em.flush();
				result = true;
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeRepositorySchedaRischi(Integer codiSessione) throws Exception {
		List<TabRepositorySchedaRischi> tabRepositorySchedaRischiToRemove= new ArrayList<TabRepositorySchedaRischi>();
		try {
			tabRepositorySchedaRischiToRemove = (List<TabRepositorySchedaRischi>) em.createNamedQuery("TabRepositorySchedaRischi.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		if(tabRepositorySchedaRischiToRemove.size()>0){
			for (TabRepositorySchedaRischi tabRepositorySchedaRischi : tabRepositorySchedaRischiToRemove) {
				em.remove(tabRepositorySchedaRischi);
			}
			em.flush();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void removeRepositoryD1(Integer codiSessione) throws Exception{
		List<TabRepositoryDocument> tabRepositoryDocumentToRemove = new ArrayList<TabRepositoryDocument>();
		try {
			tabRepositoryDocumentToRemove = (List<TabRepositoryDocument>) em.createNamedQuery("TabRepositoryDocument.findVerbaleD1ByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		if(tabRepositoryDocumentToRemove.size()>0){
			for (TabRepositoryDocument tabRepositoryDocument : tabRepositoryDocumentToRemove) {
				em.remove(tabRepositoryDocument);
			}
			em.flush();
		}
	}
	

	@Override
	public Object[] updateSchedaDyn(TabSchedaDyn tabSchedaDyn) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		try {
				em.merge(tabSchedaDyn);
				em.flush();
				result = true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}

	@Override
	public Object[] saveSchedaDyn(TabSchedaDyn tabSchedaDyn) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		try {
				em.persist(tabSchedaDyn);
				em.flush();
				result = true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}

	public TabRepositorySchedaRischi getTabRepositorySchedaRischi(Integer codiSessione) {
		TabRepositorySchedaRischi doc = null;
		try {
			doc = (TabRepositorySchedaRischi) em.createNamedQuery("TabRepositorySchedaRischi.findByCodiSessione").setParameter("codiSessione", codiSessione).getSingleResult();
		} catch (Exception e) {}
		return doc;
	}
	
	public TabSchedaDyn getTabSchedaDyn(Integer codiSessione) {
		TabSchedaDyn tab = null;
		try {
			tab = (TabSchedaDyn) em.createNamedQuery("TabSchedaDyn.findByCodiSessione").setParameter("codiSessione", codiSessione).getSingleResult();
		} catch (Exception e) {}
		return tab;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TabRepositorySchedaRischi> getRepositoryDocumentDynByCodiSessione(Integer codiSessione){
		List<TabRepositorySchedaRischi> list= new ArrayList<TabRepositorySchedaRischi>();
		try {
			list = (List<TabRepositorySchedaRischi>) em.createNamedQuery("TabRepositorySchedaRischi.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return list;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRepositoryDocument> getRepDocD1Exist(Integer codiSessione) {
		List<TabRepositoryDocument> list = new ArrayList<TabRepositoryDocument>();
		try {
			list = em.createNamedQuery("TabRepositoryDocument.findVerbaleD1ByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<TabRepositoryMedia> getAllBySessioneAndCategoria(
			Integer codiSessione, String descCategoria) {
		List<TabRepositoryMedia> list = new ArrayList<TabRepositoryMedia>();
		try {
			list = em.createNamedQuery("TabRepositoryMedia.findAllBySessioneAndCategoria").setParameter("codiSessione", codiSessione).setParameter("descCategoria", descCategoria).getResultList();
		} catch (Exception e) {}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<TabRepositoryMedia> getFiveRepMediaBySessione(
			Integer codiSessione) {
		List<TabRepositoryMedia> list = new ArrayList<TabRepositoryMedia>();
		try {
			list  = em.createNamedQuery("TabRepositoryMedia.findFiveRepMediaByCodiSessione").setParameter("codiSessione", codiSessione).setFirstResult(0).setMaxResults(5).getResultList();
		} catch (Exception e) {}
		return list;
	}



	@Override
	public TabRepositoryDocument findDocBySessioneAndNome(Integer codiSessione, String descNome) {
		TabRepositoryDocument tabRepositoryDocument = null;
		try {
			tabRepositoryDocument = (TabRepositoryDocument) em.createNamedQuery("TabRepositoryDocument.findDocBySessioneAndNome").setParameter("codiSessione", codiSessione).setParameter("descNome", descNome).getSingleResult();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return tabRepositoryDocument;
	}
}
