package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.TabApk;


public interface ApkDao {

	public TabApk getLastVersion();
	public List<TabApk> getSqlFromVersion(int startVersion);
	public Integer getNumeLastVersion();
	Object[] getScriptDbFromVersion(int startVersion,int currentVersion);

}
