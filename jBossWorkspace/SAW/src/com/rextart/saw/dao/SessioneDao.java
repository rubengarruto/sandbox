package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.bean.CollaudoDatiGeneraliBean;
import com.rextart.saw.bean.SessioniFilter;
import com.rextart.saw.bean.WorkRequestBean;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabComuni;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabPoloTecnologico;
import com.rextart.saw.entity.TabProvince;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSesVerbaleParziale;
import com.rextart.saw.entity.TabSchedaDyn;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabWorkRequestColl;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwRisultatoSurvey;
import com.rextart.saw.entity.ViwSessImpUser;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwSessioniNote;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;

public interface SessioneDao {

	public List<TabSessioni> getAllSessioni();
	public boolean isUserAssocSessione(Integer codiUtente, Integer codiSessione);
	public boolean isLockUnLockOggetto(Integer codiSessione, Integer codiOggetto, boolean flagLock);
	public void lockUnLockDoc(Integer codiSessione, Integer codiOggetto, Integer codiUtente, boolean flagLock);
	public void storeLock(Integer codiSessione, Integer codiOggetto, Integer codiUtente, boolean flagLock);
	public List<ViwSessioni> getAllCollaudiInLavorazione(Integer statoFiltro);
	public List<ViwSessioni> getCollaudiInLavorazioneByAreaComp(Integer codiAreaComp,Integer statoFiltro);
	public List<ViwSessioni> getAllCollaudiArchiviati();
	public List<ViwSessioni> getCollaudiArchiviatiByAreaComp(Integer codiAreaComp);
	public Integer getCodiAreaCompetenza(String descSiglaAreaComp);
	public Integer getCodiVendor(String descVendor);
	public Object[] saveCollaudo(TabSessioni tabSessioni) throws Exception;
	ViwSessioni findViwSessioni(Integer codiIdSessione);
	public void update(CollaudoDatiGeneraliBean selectedBean, Integer codiUser)throws Exception;
	public String getDescGruppoByCLLI(String clliSito);
	Boolean saveAssocUtente(List<ViwUtenti> utenti, Integer codiSessione,List<Integer> codiUserAssoc);
	List<ViwUtenti> getAllUserAssocSession(Integer codiSessione);
	List<ViwUtenti> getAllUserMailFineCollaudo(List<Integer> listCodiUtenti);
	public List<TabWorkRequestColl> getWorkRequestBySessione(
			Integer codiSessione);
	public TabWorkRequestColl saveWorkRequest(WorkRequestBean workRequestBean,
			Integer codiUtente)throws Exception;
	public void removeWorkRequest(TabWorkRequestColl selectedWorkRequest,
			Integer codiUtente)throws Exception;
//	boolean findByCodiUserCodiAreaComp(Integer codiSessione,
//			Integer codiUtente, Integer codiAreaComp);
	public TabAnagSistemiBande findBandaSistemaByCodiImpianto(String descIdentificativo);
	
	public void changeStatoSessione(Integer codiSessione, Integer codiNewStato);
	public TabSessioni getSessioneByCodi(Integer codiSessione);
	public boolean isSessioneStatoAcquisito(Integer codiSessione);
	public Integer getCodiStateInLavorazione();
	public void insertRelSessioneStati(Integer codiSessione, Integer codiStato);
	public Integer getCodiStateDisponibile();
	public boolean isSessioneStatoDisponibile(Integer codiSessione);
	public boolean isSessioneStatoInLavorazione(Integer codiSessione);
	public boolean isSessioneStatoAnnullato(Integer codiSessione);
	public Integer getStateAcquisito();
	public Boolean saveAssocUtenteOperAOU(List<ViwUtenti> utenti, Integer codiSessione);
	public List<TabRelOggettiTipo> getListRelOggettiTipoBySessione(CollaudoDatiGeneraliBean sessione);
	public Boolean saveTabSurvey(List<TabRelOggettiTipo> tabOggettiTipos, Integer codiSessione);
	public List<ViwSessioni>  getSessioniDownload();
	void unlock(ViwSessioni sessioneSelected, Integer codiUser,Integer codiOggetto)throws Exception;
	void saveAssocUtenteRefTerOPByCodiAreaComp(List<ViwUtenti> utenti, Integer codiSessione);
	public List<ViwSessioni> getViwSessioneByCodi(Integer codiSessione);
	
	public void deleteWorkRequestByCodiSessione(Integer codiSessione);
	
	public List<TabRelRitardiSessioni> getRitardiBySessione(Integer codiSessione);
	
	public TabGruppi getGruppoByDescGruppo(String descGruppo);
	public List<TabAreaCompetenza> getAllAreaCompetenza();
	public List<ViwAreaCompRegioni> getRegioneByAreaComp(Integer codiAreaComp);
	public List<TabProvince> getProvinceByIdRegione(String codiRegione);
	public List<TabComuni> getComuneByIdProvincia(String codiProvincia);
	public String  getRegioneByNome(String nomeRegione);
	public String  getProvinciaByNome(String nomeProvincia);
	public List<TabVendors> getAllVendors();
	public List<ViwVerbale> getListVerbaliBySessioni(List<Integer> listCodiSessione);
	public TabRelSesVerbaleParziale findByOggettoSessione(Integer codiOggetto,Integer codiSessione);
	public TabPoloTecnologico findPoloTecnologicoByRegione(String descRegione);
	public String getDescRegioneByNome(String nomeRegione);
	public List<TabSchedaDyn> getSchedaDynByCodiSessioneList(List<Integer> listCodiSessione);
	public List<ViwSessioni> getCollaudiInLavorazioneByUserVendor(Integer codiVendor,Integer statoFiltro);
	public List<ViwSessioni> getCollaudiArchiviatiByUserVendor(Integer codiVendor);
	public List<ViwSessioni> getCollaudiInLavorazioneByMoi(Integer codiImpresa,Integer statoFiltro);
	public List<ViwSessioni> getCollaudiInLavorazioneByMoiAssoc(Integer codiUtente,Integer statoFiltro);
	public List<ViwSessioni> getCollaudiArchiviatiByMoi(Integer codiImpresa);
	public List<ViwSessioni> getCollaudiArchiviatiByMoiAssoc(Integer codiUtente);
	public List<ViwSessImpUser> getDitteByCodiSessione(Integer codiSessione);
	public List<ViwSessImpUser> getUsersByCodiImpresaAndCodiSessione(Integer codiImpresa, Integer codiSessione);
//	Metodi per invio a pev
	public List<ViwVerbale> getListVerbaliByCodiSessione(Integer codiSessione);
	public void setSentPev(Integer codiSessione);
	public void deleteRelSessioneutenti(Integer codiSessione);
	public List<ViwSessioni> getFilterViwSessioni(SessioniFilter sessioniFilter);
	ViwSessioni findViwSessioniToClone(Integer codiCollaudo);
	public Integer clonaCollaudo(Integer codiCollaudo, Integer codiSessione,Integer codiAreaComp);
	public List<ViwSessioni> findViwSessioniByCodiCollaudo(Integer codiCollaudo);
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessione (Integer codiSessione);
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessioneAndCodiOggetto (Integer codiSessione, Integer codiOggetto);
	public TabAnagStatiSessione loadStatoChiuso();
	public List<ViwRisultatoSurvey> getAllRisultatiNegativiRiservaByCodiSessione(Integer codiSessione);
	public String findIdentificativoByBandaAndSistema(Integer codiBanda, Integer codiSistema);
	public TabWorkRequestColl findLastWRByCodiSessione(Integer codiSessione, Integer codiAnagWrs);
	public TabRegioni getIdRegioneByNome(String nomeMaiuscolo);
	public List<ViwEsitoSurvey> getNoteTabletByCodiSessione(Integer codiSessione);
	public void saveNotaWeb(String descNota, Integer codiSessione, String username);
	public List<TabNote> getNote(Integer codiSessione);
//	public String getDescOggetto(Integer codiOggetto);
	public void updateNotaWeb(TabNote selectedNota);
	public List<ViwEsitoSurvey> getNoteDitta(Integer codiSessione);
	public List<TabNote> getNoteDittaBySessione(Integer codiSessione);
	public List<ViwSessioniNote> getSessioniNoteByListSessioni(List<Integer> listCodiSessioni);
}
