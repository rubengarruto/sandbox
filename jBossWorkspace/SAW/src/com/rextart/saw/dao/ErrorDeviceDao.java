package com.rextart.saw.dao;

import com.rextart.saw.entity.TabErrorDevice;


public interface ErrorDeviceDao {

	public void save(TabErrorDevice tabErrorDevice);
	
	public void deleteByCodiUtenteImei(Integer codiUtente, String codiImei);
	
	public boolean isExistError(Integer codiUtente, Integer numeVersionApp, String descErrore);
}
