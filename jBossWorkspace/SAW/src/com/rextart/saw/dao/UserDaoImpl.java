package com.rextart.saw.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.UserFilter;
import com.rextart.saw.entity.TabAnagStatiUtente;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabRelImpreseUtenti;
import com.rextart.saw.entity.TabRelSessioneUtenti;
import com.rextart.saw.entity.TabRelUteRegioni;
import com.rextart.saw.entity.TabRole;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwProfiloUtente;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwZoneRegioni;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.ConstInterfaces;

@Repository
public class UserDaoImpl implements UserDao {

	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public TabUtente authenticationUser(String user, String pass){
		List<TabUtente> tabUtente = new ArrayList<TabUtente>();
		try {
	    	tabUtente = (List<TabUtente>)em.createNamedQuery("TabUtente.authenticationUser").setParameter("descUser", user).setParameter("descPwd", pass).getResultList();
		} catch (Exception e) {
		}
    	if(tabUtente.size()>0){
    		return tabUtente.get(0);
    	}
    	return null;
	}
	
	/**Questo metoto � utilizzato per l'autenticazione dell' utente */
    @Override
	public ViwUtenti getAuthUser(String username) {
    	ViwUtenti user = null;
		try{
			user = (ViwUtenti) em.createNamedQuery("ViwUtenti.findByUser").setParameter("username", username).getSingleResult();
			if(user!=null && user.getFlgImpresa()&& !Const.MOI_ON)
				user=null;
		}catch(NoResultException e){
		}catch (Exception e1) {
			e1.printStackTrace();}
		return user;
	}
    @SuppressWarnings("unchecked")
	@Override
    public List<ViwUtenti> getAllUsers(){
 		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();;
 		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findAll").getResultList();
 		} catch (Exception e) {}
 		return listUser;
 	}
    
    @SuppressWarnings("unchecked")
	@Override
    public List<TabAreaCompetenza> getAllAreeComp(){
 		List<TabAreaCompetenza> list = new ArrayList<TabAreaCompetenza>();
 		try {
	 			list = em.createNamedQuery("TabAreaCompetenza.findAll").getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
    
    @SuppressWarnings("unchecked")
	@Override
    public List<String> getAllDescAreaComp(){
    	List<String> listDescAreaComp = new ArrayList<String>();
 		List<TabAreaCompetenza> list = new ArrayList<TabAreaCompetenza>();
    	try{
 			list = em.createNamedQuery("TabAreaCompetenza.findAll").getResultList();
 			for(TabAreaCompetenza t:list){
 				listDescAreaComp.add(t.getDescAreaComp());
 			}
    	}catch(Exception e){}
    	return listDescAreaComp;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<String> getAllDescRoles(){
    	List<String> listDescRoles = new ArrayList<String>();
 		List<TabRole> list = new ArrayList<TabRole>();
    	try{
 			list = em.createNamedQuery("TabRole.findAllNoSupervisorAndImp").setParameter("ruolo", Const.SUPERVISOR).getResultList();
 			for(TabRole t:list){
 				listDescRoles.add(t.getDescRole());
 			}
    	}catch(Exception e){}
    	return listDescRoles;
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public List<TabRole> getAllRoles(){
 		List<TabRole> list = new ArrayList<TabRole>();
 		try {
	 			list = em.createNamedQuery("TabRole.findAllNoSupervisorAndImp").setParameter("ruolo", Const.SUPERVISOR).getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
    
    @SuppressWarnings("unchecked")
	@Override
    public List<ViwProfiloUtente> getStruttureByRole(Integer codiRole){
 		List<ViwProfiloUtente> list = new ArrayList<ViwProfiloUtente>();
 		try {
	 			list = em.createNamedQuery("ViwProfiloUtente.findStrutturaByRole").setParameter("codiRole", codiRole).setParameter("struttura", Const.Ditte_MOI).getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
    
    @SuppressWarnings("unchecked")
	@Override
    public List<TabStruttura> getAllStrutture(){
 		List<TabStruttura> list = new ArrayList<TabStruttura>();
 		try {
	 			list = em.createNamedQuery("TabStruttura.findAll").getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
    
    @SuppressWarnings("unchecked")
	@Override
    public List<TabImprese> getAllImprese(){
 		List<TabImprese> list = new ArrayList<TabImprese>();
 		try {
	 			list = em.createNamedQuery("TabImprese.findAll").getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
    
    @Override
	public List<ViwUtenti> getFilterViwUser(UserFilter filter){
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<ViwUtenti> criteria = builder.createQuery(ViwUtenti.class);
			Root<ViwUtenti> root = criteria.from(ViwUtenti.class);
			criteria.select(root);
			List<Predicate> prList = new ArrayList<Predicate>();
			prList.add(builder.equal(root.get("flgIsAdmin"), false));
			if(filter.getCodiStatoUtente()!=null)
				prList.add(builder.equal(root.get("codiStatoUtente"), filter.getCodiStatoUtente()));
			if(filter.getCodiStruttura()!=null && !filter.getCodiStruttura().equals(0))
				prList.add(builder.equal(root.get("codiStruttura"), filter.getCodiStruttura()));
			if(filter.getCodiRuolo()!=null && !filter.getCodiRuolo().equals(0))
				prList.add(builder.equal(root.get("codiRole"), filter.getCodiRuolo()));
			if(filter.getCodiAreaComp()!=null && !filter.getCodiAreaComp().equals(0))
				prList.add(builder.equal(root.get("codiAreaComp"), filter.getCodiAreaComp()));
			
			Predicate[] list = new Predicate[prList.size()];
			list = prList.toArray(list);
			Predicate restriction = builder.and(list);
			criteria.where(restriction);
			return em.createQuery(criteria).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<ViwUtenti>();
		}
	}
    
    @Override
	public TabUtente getUtenteById(int codiUtente) {
		TabUtente tab = null;
		try {
			tab = em.find(TabUtente.class, codiUtente);
			em.refresh(tab);
		} catch (Exception e) {}
		return tab;
	}
    
    @Override
    public Object[] saveUser(TabUtente tabUtente,Integer impresa,List<Integer> selectedRegioni) throws Exception{
    	Object[] obj = new Object[2];
    	boolean result = false;
    	Integer error = 0;
    	try {
    		if(tabUtente.getDescUser()!=null && em.createNamedQuery("TabUtente.findUserbyUsername").setParameter("descUser", tabUtente.getDescUser()).getSingleResult()!=null){
    			error=1;
    			result=false;
    			obj[0] = result;
    			obj[1] = error;
    			return obj;
    		}
    	} catch (NoResultException e) {

    		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
    		tabUtente.setTimeCreazione(currentDate);
    		tabUtente.setCodiStatoUtente(ConstInterfaces.UTENTE_CREATO);
    		if(impresa!=null && tabUtente.isFlgImpresa()){
    			tabUtente.setDescUser(fillDescUserImp(tabUtente));
    			tabUtente.setCodiRole(ConstInterfaces.OPERATORE);
    			tabUtente.setCodiStruttura(ConstInterfaces.DITTE_MOI);
    			tabUtente.setCodiVendor(Const.EMPTY_VALUE);
    		}
    		
    		tabUtente.setDescPwd(tabUtente.getDescUser());//TODO da rivedere dopo requisiti
    		
    		em.persist(tabUtente);
    		if(tabUtente.isFlgImpresa()){
    			TabRelImpreseUtenti impreseUtenti = new TabRelImpreseUtenti();
    			impreseUtenti.setCodiImpresa(impresa);
    			impreseUtenti.setCodiUtente(tabUtente.getCodiUtente());
    			em.persist(impreseUtenti);
    		}
    		
    		// RELAZIONI UTENTE REGIONE ZONA	
    		for (int i=0; i<selectedRegioni.size(); i++) {
    			TabRelUteRegioni tabUtenteReg = new TabRelUteRegioni();
    			tabUtenteReg.setCodiUtente(tabUtente.getCodiUtente());
    			tabUtenteReg.setCodiIdRegione(selectedRegioni.get(i));
    			tabUtenteReg.setCodiAreaComp(tabUtente.getCodiAreaComp());
    			em.persist(tabUtenteReg);
    		}
    		
    		em.flush();
    		result= true;
    	}
    	obj[0] = result;
		obj[1] = error;
    	return obj;
    }
    
    @SuppressWarnings("unchecked")
	public void updateUser(TabUtente tabUtente,Integer impresa,boolean cessato, List<Integer> selectedRegioni){
    	try {
    		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
    		if(cessato){
    			tabUtente.setCodiStatoUtente(ConstInterfaces.UTENTE_CESSATO);
    			tabUtente.setTimeCeased(currentDate);
    			tabUtente.setFlgAbilitato(false);
        		tabUtente.setFlgCessSosp(true);
    		}
    		em.merge(tabUtente);
    		
    		ArrayList<TabRelSessioneUtenti> sessioneUtenti= new ArrayList<TabRelSessioneUtenti>();
    		try{
    			sessioneUtenti= (ArrayList<TabRelSessioneUtenti>) em.createNamedQuery("TabRelSessioneUtenti.findByCodiUserCodiAreaComp").setParameter("codiUtente", tabUtente.getCodiUtente()).setParameter("codiAreaComp", tabUtente.getCodiAreaComp()).getResultList();
    		}catch (NoResultException nre){}
    		if(sessioneUtenti.size()>0){
    			em.createNamedQuery("TabRelSessioneUtenti.deleteByCodiUser").setParameter("codiUtente", tabUtente.getCodiUtente()).executeUpdate();
    			em.flush();
			}
    		 		
    		TabRelImpreseUtenti impreseUtenti=null;
    		try{
    			impreseUtenti=(TabRelImpreseUtenti) em.createNamedQuery("TabRelImpreseUtenti.findByCodiUtente").setParameter("codiUtente", tabUtente.getCodiUtente()).getSingleResult();
    		}catch (NoResultException nre){
    		}
    		if(impreseUtenti!=null){
    			em.remove(impreseUtenti);
    			em.flush();
    			if(tabUtente.isFlgImpresa()){
    			impreseUtenti = new TabRelImpreseUtenti();
    			impreseUtenti.setCodiUtente(tabUtente.getCodiUtente());
    			impreseUtenti.setCodiImpresa(impresa);
    			em.persist(impreseUtenti);
    			}
    		}
    		
    		em.createNamedQuery("TabRelUteRegioni.deleteByCodiUtente").setParameter("codiUtente", tabUtente.getCodiUtente()).executeUpdate();
    		for(int i = 0; i<selectedRegioni.size(); i++) {
    			TabRelUteRegioni tabRelUteRegioni = new TabRelUteRegioni();
    			tabRelUteRegioni.setCodiUtente(tabUtente.getCodiUtente());
    			tabRelUteRegioni.setCodiAreaComp(tabUtente.getCodiAreaComp());
    			tabRelUteRegioni.setCodiIdRegione(selectedRegioni.get(i));
    			em.persist(tabRelUteRegioni); 
    		}
    		
    		em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
    @Override
	public TabUtente getUserById(Integer codiUtente) {
    	TabUtente tab = null;
		try {
			tab = em.find(TabUtente.class, codiUtente);
			em.refresh(tab);
		} catch (Exception e) {}
		return tab;
	}
    
    public void abilitaUtente(TabUtente tabUtente) {
    	try {
    		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
    		tabUtente.setCodiStatoUtente(ConstInterfaces.UTENTE_ABILITATO);
    		tabUtente.setTimeEnable(currentDate);
    		tabUtente.setFlgAbilitato(true);
    		tabUtente.setFlgCessSosp(false);
    		em.merge(tabUtente);
    		em.flush();
    	} catch (Exception e) {}
    }
    
    public void disabilitaUtente(TabUtente tabUtente) {
    	try {
    		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
    		tabUtente.setCodiStatoUtente(ConstInterfaces.UTENTE_SOSPESO);
    		tabUtente.setTimeSuspended(currentDate);
    		tabUtente.setTimeSuspension(currentDate);
    		tabUtente.setFlgAbilitato(false);
    		tabUtente.setFlgCessSosp(true);
        	em.merge(tabUtente);
        	em.flush();
		} catch (Exception e) {}
	}
    
    /**
     * Metodo che costruisce l' username di un nuovo utente telecom o impresa (assegnato da sistema). 
     * @param TabUtenti: Ruolo utente 
     * @return String: Username utente **/
	private String fillDescUserImp(TabUtente newUser) {
    	try {
    			String prefix = ConstInterfaces.DESC_UTENTE_IMPRESA;
    			Integer seq = callSequence("SELECT SEQ_USERID_IMPRESA.NEXTVAL from Dual");
        		return  prefix+StringUtils.leftPad(seq.toString(), 5, "0");
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		return null;
    	
    }

    private Integer callSequence(String sequence) {
   		BigDecimal seq = (BigDecimal) em.createNativeQuery(sequence).getSingleResult();
		return seq.intValue();
	}
    
    
  @SuppressWarnings("unchecked")
@Override
    public List<TabZone> getTabZoneByAreaComp(Integer codiAreaComp){
 		List<TabZone> list = new ArrayList<TabZone>();
 		try {
	 			list = em.createNamedQuery("TabZone.findByAreaComp").setParameter("codiAreaComp", codiAreaComp).getResultList();
 		} catch (Exception e) {}
 		return list;
 	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<TabGruppi> getTabGruppiByZona(Integer codiZona){
		List<TabGruppi> list = new ArrayList<TabGruppi>();
		try {
	 			list = em.createNamedQuery("TabGruppi.findByZona").setParameter("codiZona", codiZona).getResultList();
		} catch (Exception e) {}
		return list;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getRefTerOPByCodiAreaComp(Integer codiAreaComp){
		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findRefTerOPByCodiAreaComp").setParameter("codiAreaComp", codiAreaComp).setParameter("role", Const.Referente_Territoriale_OP).getResultList();
		} catch (Exception e) {}
		return listUser;
	}
  
  @Override
  public TabGruppi getTabGruppiDescGruppo(String descGruppo){
		TabGruppi tabGruppi = new TabGruppi();
		try {
			tabGruppi = (TabGruppi) em.createNamedQuery("TabGruppi.findByDesc").setParameter("descGruppo", descGruppo).getSingleResult();
		} catch (Exception e) {}
		return tabGruppi;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getUserMOIByAreaComp(Integer codiImpresa,Integer codiAreaComp){
	  List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findUserMOIByCodiAreaComp").setParameter("codiImpresa", codiImpresa).setParameter("codiAreaComp", codiAreaComp).getResultList();
		} catch (Exception e) {}
		return listUser;
	}

  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getUtentiListByAreaComp(Integer codiAreaComp) {
	  List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
	  try {
		  listUser = em.createNamedQuery("ViwUtenti.findUserByAreaComp").setParameter("codiAreaComp", codiAreaComp).getResultList();
	  } catch (Exception e) {}
	  return listUser;
  }

  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getOperNoaByCodiAreaComp(Integer codiAreaComp){
		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findOperNoaByCodiAreaComp").setParameter("codiAreaComp", codiAreaComp).setParameter("role", Const.Operatore).setParameter("struttura", Const.N_NOA).getResultList();
		} catch (Exception e) {}
		return listUser;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getAllAOUByAreaComp(Integer codiAreaComp){
		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findAllAOUByAreaComp").setParameter("codiAreaComp", codiAreaComp).setParameter("struttura", Const.AOU).getResultList();
		} catch (Exception e) {}
		return listUser;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getOperAOUByAreaComp(Integer codiAreaComp){
		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findOperAOUByAreaComp").setParameter("codiAreaComp", codiAreaComp).setParameter("struttura", Const.AOU).setParameter("role", Const.Operatore).getResultList();
		} catch (Exception e) {}
		return listUser;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<ViwUtenti> getDGoperRToperByAreaComp(Integer codiAreaComp){
		List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
		try {
	 			listUser = em.createNamedQuery("ViwUtenti.findDGoperRToperByAreaComp").setParameter("codiAreaComp", codiAreaComp).setParameter("roleDGOP", Const.DG_OP).setParameter("roleRTOP", Const.Referente_Territoriale_OP).getResultList();
		} catch (Exception e) {}
		return listUser;
	}
  
  @SuppressWarnings("unchecked")
@Override
  public List<TabAreaCompetenza> getAllAreeCompNotNazionale(){
		List<TabAreaCompetenza> list = new ArrayList<TabAreaCompetenza>();
		try {
	 			list = em.createNamedQuery("TabAreaCompetenza.findAllNotNazionale").getResultList();
		} catch (Exception e) {}
		return list;
	}

  @SuppressWarnings("unchecked")
@Override
  public ArrayList<ViwUtenti> getUtentiAssociatiByCodiSessione(Integer codiSessione) {
  	
  	List<TabRelSessioneUtenti> listRelUtentiAssociati = null;
  	ArrayList<ViwUtenti> listUtentiAssociati = null; 
  	try {
  		
  		listRelUtentiAssociati = em.createNamedQuery("TabRelSessioneUtenti.findByIdSessioneDistinct").setParameter("codiSessione", codiSessione).getResultList();
  		if (listRelUtentiAssociati!=null && listRelUtentiAssociati.size()>0) {
  			listUtentiAssociati = new ArrayList<>();
  			for (TabRelSessioneUtenti utente : listRelUtentiAssociati) {
  				listUtentiAssociati.add(getUtenteById(utente.getCodiUtente()));
  			}
  		}
  	} catch (Exception e) {}
  	return listUtentiAssociati;
  }


@SuppressWarnings("unchecked")
@Override
public List<TabRelSessioneUtenti> getAllUtentiAssociatiByCodiSessione(Integer codiSessione) {
	List<TabRelSessioneUtenti> listRelUtentiSessione = new ArrayList<TabRelSessioneUtenti>();
	
	try{
		listRelUtentiSessione = em.createNamedQuery("TabRelSessioneUtenti.findByIdSessione").setParameter("codiSessione", codiSessione).getResultList();
	}catch(Exception e){
		
	}
	return listRelUtentiSessione;
}

@Override
public ViwUtenti getUtenteById(Integer codiUtente) {
	ViwUtenti viw = null;
	try {
		viw = em.find(ViwUtenti.class, codiUtente);
		em.refresh(viw);
	} catch (Exception e) {}
	return viw;
}

@Override
public ViwUtenti getViwUtenteById(Integer codiUtente) {
	ViwUtenti viw = null;
	try {
		viw = em.find(ViwUtenti.class, codiUtente);
		em.refresh(viw);
	} catch (Exception e) {}
	return viw;
}

@SuppressWarnings("unchecked")
public TabUtente authenticationByUserName(String user){
	List<TabUtente> tabUtente = new ArrayList<TabUtente>();
	try {
		tabUtente = (List<TabUtente>)em.createNamedQuery("TabUtente.authenticationByUserName").setParameter("descUser", user).getResultList();
	} catch (Exception e) {
	}
	if(tabUtente.size()>0){
		return tabUtente.get(0);
	}
	return null;
}

@SuppressWarnings("unchecked")
@Override
public List<TabVendors> getAllVendor() {
	List<TabVendors> vendor=new ArrayList<TabVendors>();
	try {
		vendor=em.createNamedQuery("TabVendors.findAll").getResultList();
	} catch (Exception e) {
	}
		return vendor;
}

@SuppressWarnings("unchecked")
@Override
public List<TabVendors> getVendorByCodi(Integer codiVendor) {
	List<TabVendors> vendor=new ArrayList<TabVendors>();
	try {
		vendor=em.createNamedQuery("TabVendors.findByCodi").setParameter("codiVendor", codiVendor).getResultList();
	} catch (Exception e) {
	}
		return vendor;
}

@SuppressWarnings("unchecked")
@Override
public List<ViwUtenti> getUtentiVendor(Integer codiVendor) {
	List<ViwUtenti> listUser = new ArrayList<ViwUtenti>();
	try {
 			listUser = em.createNamedQuery("ViwUtenti.findUtentiVendorByCodiVendor").setParameter("codiVendor", codiVendor).getResultList();
	} catch (Exception e) {}
	return listUser;
}
@Override
public Boolean updatePassword(String pwd, String descUser) {
	try {
			TabUtente utente = null;
			utente=(TabUtente) em.createNamedQuery("TabUtente.findUserbyUsername").setParameter("descUser", descUser).getSingleResult();
		    utente.setDescPwd(pwd);
			utente.setFlgChangePwd(true);
			em.merge(utente);
			em.flush();
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}

	return true;
	
}
@Override
	public TabUtente getTabUtenteByUserName(String descUser) {
	TabUtente user = null;
		try{
			user = (TabUtente) em.createNamedQuery("TabUtente.findByUser").setParameter("username", descUser).getSingleResult();
		}catch(NoResultException e){
		}catch (Exception e1) {
			e1.printStackTrace();}
		return user;
	}

@Override
public Boolean resetPassword(TabUtente utente) {
	try {
		    utente.setDescPwd(utente.getDescUser());
			utente.setFlgChangePwd(false);
			em.merge(utente);
			em.flush();
	} catch (Exception e) {
		e.printStackTrace();
		return false;
	}

	return true;
	
}

@SuppressWarnings("unchecked")
@Override
public List<TabAnagStatiUtente> getAllStatiUtente() {
	List<TabAnagStatiUtente> statiUtente = new ArrayList<TabAnagStatiUtente>();
	try {
		statiUtente = em.createNamedQuery("TabAnagStatiUtente.findAll").getResultList();
	} catch (Exception e) {}
	return statiUtente;
}

@SuppressWarnings("unchecked")
@Override
public List<ViwUtenti> getUserRefTerAndCordByAreaComp(Integer codiAreaComp){
	List<ViwUtenti> listUtentiFiltered = new ArrayList<ViwUtenti>();
	try{
		listUtentiFiltered = em.createNamedQuery("ViwUtenti.findUserRefTerAndCordByAreaComp").setParameter("roleRefTer", Const.Referente_Territoriale).setParameter("roleCorOp", Const.Coordinatore_Operativo).setParameter("codiAreaComp", codiAreaComp).getResultList();
	} catch(Exception e){
		e.printStackTrace();
	}
	return listUtentiFiltered;
}

@SuppressWarnings("unchecked")
@Override
public List<String> getUserForMailDiSistema(List<String> selectedAreaDiComp,List<String> selectedRoles){
	List<ViwUtenti> listUtenti = new ArrayList<ViwUtenti>();
	List<String> mailUtentiFiltered = new ArrayList<String>();
	try{
		listUtenti = em.createNamedQuery("ViwUtenti.findUsersForMailDiSistema").setParameter("descAreaDiComp", selectedAreaDiComp).setParameter("descRoles", selectedRoles).getResultList();
		for(ViwUtenti v:listUtenti){
			mailUtentiFiltered.add(v.getDescEmail());
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	
	return mailUtentiFiltered;
}

@SuppressWarnings("unchecked")
@Override
public List<ViwUtenti> getAdmin() {
	List<ViwUtenti> listAdmin = new ArrayList<ViwUtenti>();
	try {
		listAdmin = em.createNamedQuery("ViwUtenti.findAdmin").getResultList();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return listAdmin;
}

public List<ViwAreaCompRegioni> getViwRegioniByAreaComp(Integer codiAreaComp) {
	List<ViwAreaCompRegioni> listRegioni = new ArrayList<ViwAreaCompRegioni>();
	try {
		listRegioni = em.createNamedQuery("ViwAreaCompRegioni.findByAreaComp").setParameter("codiAreaComp", codiAreaComp).getResultList();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return listRegioni;
}

@SuppressWarnings("unchecked")
public List<ViwZoneRegioni> getViwZoneRegioni(List<Integer> selectedRegioni) {
	List<ViwZoneRegioni> listZoneByRegione = new ArrayList<ViwZoneRegioni>();
	
	try {
		listZoneByRegione= (List<ViwZoneRegioni>)em.createNamedQuery("ViwZoneRegioni.findByCodiIdRegione").setParameter("selectedRegioni", selectedRegioni).getResultList();
	}
	 catch (Exception e) {
			e.printStackTrace();
		}
		return listZoneByRegione;
	
	}

public ViwAreaCompRegioni getRegioneByCodi(Integer codiRegione) {
	ViwAreaCompRegioni listDescRegioni = new ViwAreaCompRegioni();
	
	try {
		listDescRegioni= (ViwAreaCompRegioni) em.createNamedQuery("ViwAreaCompRegioni.findByCodiIdRegione").setParameter("codiRegione", codiRegione).getSingleResult();
	}
	 catch (Exception e) {
			e.printStackTrace();
		}
		return listDescRegioni;
}

public List<Integer> getRegioniSelectedByCodiUtente(Integer codiUtente) {
	List<Integer> tabRelUteRegione = new ArrayList<Integer>();
	try {
		
		tabRelUteRegione = em.createNamedQuery("TabRelUteRegioni.findSelectedRegioni").setParameter("codiUtente", codiUtente).getResultList();
		
	} catch(Exception e) {
			e.printStackTrace();
	}
	return tabRelUteRegione;
	
	
	}

@SuppressWarnings("unchecked")
public void updateProfile(CurrentUser currentUser,  Integer impresa, List<Integer> selectedRegioni, Integer codiZona, Integer codiGruppo) {
	try {
		
		// find by id utente su tab utente
		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
		TabUtente tabUtente = new TabUtente();
			tabUtente = (TabUtente) em.createNamedQuery("TabUtente.findByCodiUtente").setParameter("codiUtente", currentUser.getCodiUtente()).getSingleResult();
			tabUtente.setFlgReceiveEmail(currentUser.getFlgEmail());
    		tabUtente.setCodiZona(codiZona);
    		tabUtente.setCodiGruppo(codiGruppo);
    		em.merge(tabUtente);
			
		// collaudo
		ArrayList<TabRelSessioneUtenti> sessioneUtenti= new ArrayList<TabRelSessioneUtenti>();
		try{
			sessioneUtenti= (ArrayList<TabRelSessioneUtenti>) em.createNamedQuery("TabRelSessioneUtenti.findByCodiUserCodiAreaComp").setParameter("codiUtente", currentUser.getCodiUtente()).setParameter("codiAreaComp", currentUser.getCodiAreaComp()).getResultList();
		}catch (NoResultException nre){}
		if(sessioneUtenti.size()>0){
			em.createNamedQuery("TabRelSessioneUtenti.deleteByCodiUser").setParameter("codiUtente", currentUser.getCodiUtente()).executeUpdate();
			em.flush();
		}
		 		
		TabRelImpreseUtenti impreseUtenti=null;
		try{
			impreseUtenti=(TabRelImpreseUtenti) em.createNamedQuery("TabRelImpreseUtenti.findByCodiUtente").setParameter("codiUtente", currentUser.getCodiUtente()).getSingleResult();
		}catch (NoResultException nre){
		}
		if(impreseUtenti!=null){
			em.remove(impreseUtenti);
			em.flush();
			if(currentUser.getViwUtente().getFlgImpresa()){
			impreseUtenti = new TabRelImpreseUtenti();
			impreseUtenti.setCodiUtente(currentUser.getCodiUtente());
			impreseUtenti.setCodiImpresa(impresa);
			em.persist(impreseUtenti);
			}
		}
		
		em.createNamedQuery("TabRelUteRegioni.deleteByCodiUtente").setParameter("codiUtente", currentUser.getCodiUtente()).executeUpdate();
		for(int i = 0; i<selectedRegioni.size(); i++) {
			TabRelUteRegioni tabRelUteRegioni = new TabRelUteRegioni();
			tabRelUteRegioni.setCodiUtente(currentUser.getCodiUtente());
			tabRelUteRegioni.setCodiAreaComp(currentUser.getCodiAreaComp());
			tabRelUteRegioni.setCodiIdRegione(selectedRegioni.get(i));
			em.persist(tabRelUteRegioni); 
		}
		
		em.flush();
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}

@Override
public List<ViwUtenti> getRefTerOPByRegione(List<Integer> codiUtente) {
	List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
	try {
		listaUtenti = em.createNamedQuery("ViwUtenti.findRefTerOPByRegione").setParameter("codiRole", Const.Referente_Territoriale_OP).setParameter("codiUtente", codiUtente).getResultList();
	} catch (Exception e) {
	}
	return listaUtenti;
}

@Override
public List<Integer> getUtentiByRegione(Integer codiIdRegione) {
	List<Integer> codiUtenti = new ArrayList<Integer>();
	try {
		codiUtenti = em.createNamedQuery("TabRelUteRegioni.findUtentiByRegione").setParameter("codiIdRegione", codiIdRegione).getResultList();
	} catch (Exception e) {
	}
	return codiUtenti;
}

@Override
public List<ViwUtenti> getOperAOUByRegione(List<Integer> codiUtente) {
	List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
	try {
		listaUtenti = em.createNamedQuery("ViwUtenti.findOperAOUByRegione").setParameter("codiRole", Const.Operatore).setParameter("struttura", Const.AOU).setParameter("codiUtente", codiUtente).getResultList();
	} catch (Exception e) {
	}
	return listaUtenti;
}

@Override
public List<ViwUtenti> getUserRefTerAndCordByRegione(List<Integer> codiUtente) {
	List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
	try {
		listaUtenti = em.createNamedQuery("ViwUtenti.findUserRefTerAndCordByRegione").setParameter("roleRefTer", Const.Referente_Territoriale).setParameter("roleCorOp", Const.Coordinatore_Operativo).setParameter("codiUtente", codiUtente).getResultList();
	} catch (Exception e) {
	}
	return listaUtenti;
}

@Override
public List<ViwUtenti> getUserInCodiUtente(List<Integer> codiUtente) {
	List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
	try {
		listaUtenti = em.createNamedQuery("ViwUtenti.findUserInCodiUtente").setParameter("codiUtente", codiUtente).getResultList();
	} catch (Exception e) {
	}
	return listaUtenti;
}

@Override
public List<ViwUtenti> getAllAssocSessionManuale(Integer codiSessione) {
    List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
    try {
		listaUtenti = em.createNamedQuery("ViwUtenti.findAllAssocSessionManuale").setParameter("codiSessione", codiSessione).setParameter("struttura", Const.AOU).setParameter("roleOper", Const.Operatore).setParameter("roleRefTerOp", Const.Referente_Territoriale_OP).setParameter("roleVendor", Const.UTENTE_VENDOR).getResultList();
	} catch (Exception e) {
	}
	return listaUtenti;
}
}

