package com.rextart.saw.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabOggettoLock;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSesVerbaleParziale;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.MethodUtilities;


@Repository
public class SincronizzazioneDaoImpl implements  SincronizzazioneDao{
	
	@PersistenceContext
	private EntityManager em;

	
	@Override
	public Object[] saveTabSurveyDettaglio(TabSurveyDettaglio tabSurveyDettaglio) {
		Object[] obj = new Object[2];
		boolean result = false;
		int codi = 0;
		em.persist(tabSurveyDettaglio);
		em.flush();
		result = true;
		obj[0] = result;
		obj[1] = codi;
		return obj;
	}

	@Override
	public void updateTabSurveyDettaglio(TabSurveyDettaglio tabSurveyDettaglio) {
		em.merge(tabSurveyDettaglio);
		em.flush();
	}
	
    @SuppressWarnings("unchecked")
	@Override
	public ArrayList<TabSurvey> getSurveyByCodiUtenteSessione(Integer codiUtente, Integer codiSessione) {
    	ArrayList<TabSurvey> list = new ArrayList<TabSurvey>();
    	try {
    		list  = (ArrayList<TabSurvey>) em.createNamedQuery("TabSurvey.findByCodiUtenteCodiSessione").setParameter("codiUtente", codiUtente).setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		return list ;
	}
    

    
    @Override
    public boolean deleteTabSurveyDettaglioByCodiSurvey(Integer codiSurvey) {
		if (codiSurvey!=null){
				em.createNamedQuery("TabSurveyDettaglio.deleteByCodiSurvey").setParameter("codiSurvey",codiSurvey).executeUpdate();
				em.flush();
		}
		return true;
	}
    
    public void saveRiferimentiFirma(TabRiferimentiFirma tabRiferimentiFirma){
    		em.persist(tabRiferimentiFirma);
    		em.flush();
    }
    
    @SuppressWarnings("unchecked")
	public boolean isRiferimentiExist(Integer codiSurvey){
    	List<TabRiferimentiFirma> list  = new ArrayList<TabRiferimentiFirma>();
    	try {
        	list = (ArrayList<TabRiferimentiFirma>) em.createNamedQuery("TabRiferimentiFirma.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).setParameter("codiSurvey", codiSurvey).getResultList();
		} catch (Exception e) {
		}
		if(list.size() > 0 ){
			return true;
		}
    	return false;
    }
    
    public TabRiferimentiFirma getRiferimentoByCodiSurvey(Integer codiSurvey){
    	TabRiferimentiFirma entity  = (TabRiferimentiFirma)em.createNamedQuery("TabRiferimentiFirma.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getSingleResult();
		return entity;
    }
    
    public TabRiferimentiFirma getRiferimentoByCodiSurveyFlgDitta(Integer codiSurvey, Boolean flgDitta ){
    	TabRiferimentiFirma entity=null;
    	try {
    		entity  = (TabRiferimentiFirma)em.createNamedQuery("TabRiferimentiFirma.findByCodiSurveyFlgDitta").setParameter("codiSurvey", codiSurvey).setParameter("flgDitta", flgDitta).getSingleResult();
    	} catch (Exception e) {
    	}
		return entity;
    }
    
    public void saveRisultatoSurvey(TabRisultatoSurvey tabRisultatoSurvey){
    	em.persist(tabRisultatoSurvey);
    	em.flush();
    }
    
    @SuppressWarnings("unchecked")
	public boolean isRisultatoSurveyExist(Integer codiSurvey){
    	List<TabRisultatoSurvey> list  = new ArrayList<TabRisultatoSurvey>();
    	try {
        	list = (ArrayList<TabRisultatoSurvey>) em.createNamedQuery("TabRisultatoSurvey.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getResultList();
		} catch (Exception e) {
		}
		if(list.size() > 0 ){
			return true;
		}
    	return false;
    }
    
    @SuppressWarnings("unchecked")
	public TabRisultatoSurvey getRisultatoSurveyByCodiSurvey(Integer codiSurvey){
    	List<TabRisultatoSurvey> list = new ArrayList<TabRisultatoSurvey>();
    	try {
        	list  = (List<TabRisultatoSurvey>)em.createNamedQuery("TabRisultatoSurvey.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getResultList();
		} catch (Exception e) {
		}
    	if(list != null && list.size()>0){
    			return list.get(0);
    	}
    	
    	return null;
    }
    
  /* public List<TabRisultatoSurvey> getListRisultatoSurveyByCodiSurvey(Integer codiSurvey){
    	List<TabRisultatoSurvey> list = null;
    	return list;
    }
    */
    @SuppressWarnings("unchecked")
	public boolean isExistCollaudoAllResults(Integer codiSessione){
    	List<TabSurvey> listSurvey  = new ArrayList<TabSurvey>();
    	try {
        	listSurvey = (List<TabSurvey>)em.createNamedQuery("TabSurvey.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
    	TabRisultatoSurvey tabRisultatoTempD1 = null;
    	TabRisultatoSurvey tabRisultatoTempD2 = null;
    	int counter = 0;
    	for(TabSurvey tabSurvey : listSurvey){
    		if(!tabSurvey.getTabTipoOggetto().isFlgDatiGenerali()){
    			TabRisultatoSurvey tabRisultato = getRisultatoSurveyByCodiSurvey(tabSurvey.getCodiSurvey());
    			
    			if((tabRisultato == null && tabSurvey.getCodiOggetto() != Const.ALLEGATO_D1) && (tabRisultato == null && tabSurvey.getCodiOggetto() != Const.ALLEGATO_D)){
        			return false;
        		}
    			
    			if((tabSurvey.getCodiOggetto() == Const.ALLEGATO_D)){
    				counter += 1;
    				tabRisultatoTempD2 = tabRisultato;
    			}else if(tabSurvey.getCodiOggetto() == Const.ALLEGATO_D1){
    				counter += 1;
    				tabRisultatoTempD1 = tabRisultato;
    			}
    			
    		}	
    	}
    	if(counter != 0){
    		if((tabRisultatoTempD2 == tabRisultatoTempD1) && (tabRisultatoTempD2 == null)){
            	return false;
        	}
    	}
    	return true;
    }
    
    @SuppressWarnings("unchecked")
	public boolean isOggettoOwner(Integer codiSessione, Integer codiOggetto, Integer codiUtenteLocking){
    	List<TabOggettoLock> listTabOggettoLock  = new ArrayList<TabOggettoLock>();
    	try {
    		listTabOggettoLock =(List<TabOggettoLock>)em.createNamedQuery("TabOggettoLock.findBySessOgg").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
    	} catch (Exception e) {
    	}
    	TabOggettoLock tabOggettoLock = new TabOggettoLock();
    	if(listTabOggettoLock.size() > 0){
    		tabOggettoLock = listTabOggettoLock.get(0);
    	}
		if(tabOggettoLock.getCodiUtente().equals(codiUtenteLocking)){
			return true;
		}
		
		return false;
    }
    
    public void saveNoteCaratteristiche(TabNoteCaratteristiche tabNoteCaratteristiche){
    	TabNoteCaratteristiche tabCaratteristicheExist = isNoteCaratteristicheExist(tabNoteCaratteristiche.getCodiSurvey(), tabNoteCaratteristiche.getCodiCaratteristica());
    	if(tabCaratteristicheExist !=null){
    		UtilLog.printUpdateNoteCarrattSurvey(tabNoteCaratteristiche.getCodiSurvey(), tabNoteCaratteristiche.getCodiCaratteristica(), tabNoteCaratteristiche.getDescNote());
    		tabCaratteristicheExist.setDescNote(tabNoteCaratteristiche.getDescNote());
    		em.merge(tabCaratteristicheExist);
        	em.flush();
    	}else{
    		em.persist(tabNoteCaratteristiche);
    		em.flush();
    	}
    }
    
    @SuppressWarnings("unchecked")
	public TabNoteCaratteristiche isNoteCaratteristicheExist(Integer codiSurvey, Integer codiCaratteristica){
    	List<TabNoteCaratteristiche> list  = new ArrayList<TabNoteCaratteristiche>();
    	try {
        	list = (ArrayList<TabNoteCaratteristiche>) em.createNamedQuery("TabNoteCaratteristiche.findByCodiSurveyCodiSess").setParameter("codiSurvey", codiSurvey).setParameter("codiCaratteristica", codiCaratteristica).getResultList();
		} catch (Exception e) {
		}
		if(list.size() > 0 ){
			return list.get(0);
		}
    	return null;
    }
    
    
    public void saveRelSessioneRitardo(TabRelRitardiSessioni tabRelRitardiSessioni){
    	em.merge(tabRelRitardiSessioni);
		em.flush();
    }
    @SuppressWarnings("unchecked")
	public TabRelSessioneStatiVerbale isRelSessioneStatiVerbaleExist(Integer codiOggetto, Integer codiSessione){
    	List<TabRelSessioneStatiVerbale> list  = new ArrayList<TabRelSessioneStatiVerbale>();
    	try {
        	list = (ArrayList<TabRelSessioneStatiVerbale>) em.createNamedQuery("TabRelSessioneStatiVerbale.findByCodiOggettoCodiSess").setParameter("codiOggetto", codiOggetto).setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
    	if(list.size() > 0 ){
			return list.get(0);
		}
		return null;
    	
    }
    
    public void saveRelSessioniStatiVerbale(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale,Boolean flagUpdateStato){
    	if(flagUpdateStato){
    		em.merge(tabRelSessioneStatiVerbale);
        	em.flush();
    	}else{
    		em.persist(tabRelSessioneStatiVerbale);
    		em.flush();
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void isVerbaleAllPositive(Integer codiOggetto,Integer codiSessione){
    	List<TabSurvey> listSurvey  = new ArrayList<TabSurvey>();
    	try {
        	listSurvey = (List<TabSurvey>)em.createNamedQuery("TabSurvey.findByCodiSessioneCodiOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
		} catch (Exception e) {
		}
    	Boolean toUpdate=true;
    	for(TabSurvey tabSurvey : listSurvey){
    		if(!tabSurvey.getTabTipoOggetto().isFlgDatiGenerali()){
    			TabRisultatoSurvey tabRisultato = getRisultatoSurveyByCodiSurvey(tabSurvey.getCodiSurvey());
    			
    			if(tabRisultato==null || (tabRisultato.getCodiValoreRisultatoSurvey()!=null && tabRisultato.getCodiValoreRisultatoSurvey()!=Const.POSITIVO_INT)){
    				toUpdate=false;
    				break;
    			} 
    		}

    	}
    	if(toUpdate){
    		TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale=isRelSessioneStatiVerbaleExist(codiOggetto, codiSessione);
    		tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_POSITIVO);
    		saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, true);
    	}

    }


	@Override
	 public Object [] saveRelSessVerbaleParziale(Integer codiOggetto,Integer codiSessione,Boolean isFlgParziale)throws Exception {
		Object[] obj = new Object[2];
		Integer error = 0;
		boolean result = false;
		try {
    	Timestamp currentDate = new Timestamp(System.currentTimeMillis());
		TabRelSesVerbaleParziale tab = new TabRelSesVerbaleParziale();
		tab.setCodiOggetto(codiOggetto);
		tab.setCodiSessione(codiSessione);
		tab.setFlgParziale(isFlgParziale);
		tab.setData(currentDate);
		em.persist(tab);
		em.flush();
		result=true;
		}catch (Exception e) {
				e.printStackTrace();
			}
		obj[0] = result;
		obj[1] = error;
    	return obj;
	}


	@Override
	public TabOggetti findTabOggetti(Integer codiOggetto) {
		TabOggetti tabOggetti=null;
		try {
			tabOggetti=(TabOggetti) em.createNamedQuery("TabOggetti.findByCodi").setParameter("codiOggetto", codiOggetto).getSingleResult();
			
		} catch (Exception e) {}
		return tabOggetti;
	}


	@Override
	public void saveOrUpdateRiferimentiFirma(TabRiferimentiFirma riferimenti) {
		TabRiferimentiFirma tabRiferimentiFirmaExist = getRiferimentoByCodiSurveyFlgDitta(riferimenti.getCodiSurvey(),riferimenti.getFlgDitta()); 
		if (tabRiferimentiFirmaExist!=null) {
			riferimenti.setCodiRiferimentiFirma(tabRiferimentiFirmaExist.getCodiRiferimentiFirma());
			em.merge(riferimenti);
		} else {
			em.persist(riferimenti);
		}
		
	}


	@Override
	public ViwVerbale getViwVerbaleBySessioneOggetto(Integer codiSessione, Integer codiOggetto) {
		ViwVerbale viwVerbale=null;
		try {
			viwVerbale= (ViwVerbale)  em.createNamedQuery("ViwVerbale.findBySessioneOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getSingleResult();
		} catch (Exception e) {
		}
		return viwVerbale;
	}


	
	@Override
	public void saveNoteWebFromTablet(TabNote entity) {
			TabNote tabNota = new TabNote();
			tabNota = isNotaWebExist(entity.getCodiSurvey());
			if(tabNota.getCodiSurvey() != null) {
				tabNota.setCodiSessione(entity.getCodiSessione());
				tabNota.setDescNota(MethodUtilities.creaAccentate(entity.getDescNota()));
				tabNota.setCodiSurvey(entity.getCodiSurvey());
				tabNota.setFlgTablet(entity.getFlgTablet());
				tabNota.setDescUser(entity.getDescUser());		
				tabNota.setCodiOggetto(entity.getCodiOggetto());
				em.merge(tabNota);
				em.flush();
			} 
			else {
//			tabNota = new TabNote();
			tabNota.setCodiSessione(entity.getCodiSessione());
			tabNota.setDescNota(MethodUtilities.creaAccentate(entity.getDescNota()));
			tabNota.setCodiSurvey(entity.getCodiSurvey());
			tabNota.setFlgTablet(entity.getFlgTablet());
			tabNota.setDescUser(entity.getDescUser());		
			tabNota.setCodiOggetto(entity.getCodiOggetto());
			em.persist(tabNota);		
			}
		}



	private TabNote isNotaWebExist(Integer codiSurvey) {
		TabNote nota = new TabNote();
		try {
			nota = (TabNote) em.createNamedQuery("TabNote.findByCodiSurveyAndCodiSess").setParameter("codiSurvey", codiSurvey).getSingleResult();
		} catch (Exception e) {
			
		}
		return nota;
	}
	

	
}
