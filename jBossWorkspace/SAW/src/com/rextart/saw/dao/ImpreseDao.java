package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.TabImprese;


public interface ImpreseDao {

	public List<TabImprese> getAllImprese();
	public void saveImpresa(TabImprese tabImprese);
	public void updateImpresa(TabImprese tabImprese);
	public TabImprese getImpresaById(int codiImpresa);
	public Object[] removeImpresa(TabImprese tabImprese);
	public TabImprese getImpresaByCF (String descCf);
	public TabImprese getImpresaByCFAndByCodiImpresa (String descCf, Integer codiImpresa);
	
}
