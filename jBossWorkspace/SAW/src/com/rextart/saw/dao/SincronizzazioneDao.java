package com.rextart.saw.dao;

import java.util.ArrayList;

import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwVerbale;


public interface SincronizzazioneDao {

	public ArrayList<TabSurvey> getSurveyByCodiUtenteSessione(Integer codiUtente, Integer codiSessione); 
	
	public Object[] saveTabSurveyDettaglio(TabSurveyDettaglio tabSurveyDettaglio);
	
	public boolean deleteTabSurveyDettaglioByCodiSurvey(Integer codiSurvey);
	
	public void saveRiferimentiFirma(TabRiferimentiFirma tabRiferimentiFirma);
	
	public void saveRisultatoSurvey(TabRisultatoSurvey tabRisultatoSurvey);
	
	public boolean isExistCollaudoAllResults(Integer codiSessione);
	
	public boolean isOggettoOwner(Integer codiSessione, Integer codiOggetto, Integer codiUtenteLocking);
	
	public void saveNoteCaratteristiche(TabNoteCaratteristiche tabNoteCaratteristiche);
	
	public void saveRelSessioneRitardo(TabRelRitardiSessioni tabRelRitardiSessioni);
	
	public void saveRelSessioniStatiVerbale(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale,Boolean flagUpdateStato);
	
	public TabRelSessioneStatiVerbale isRelSessioneStatiVerbaleExist(Integer codiOggetto, Integer codiSessione);
	
	public void isVerbaleAllPositive(Integer codiOggetto,Integer codiSessione);
	
	public Object[]  saveRelSessVerbaleParziale(Integer codiOggetto,Integer codiSessione,Boolean isFlgParziale) throws Exception ;
	
	public  TabOggetti findTabOggetti(Integer codiOggetto);

	public void saveOrUpdateRiferimentiFirma(TabRiferimentiFirma riferimenti);
	
	//salvataggio note web from tablet
	public void saveNoteWebFromTablet(TabNote entity);

	public ViwVerbale getViwVerbaleBySessioneOggetto(Integer codiSessione, Integer codiOggetto);

	public void updateTabSurveyDettaglio(TabSurveyDettaglio tabSurveyDettaglio);

	
}
