package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.entity.TabSchedaDyn;


public interface RepositoryMediaDao {

	public Object[] saveRepositoryMedia(TabRepositoryMedia tabRepositoryMedia, boolean insert);
	//public List<ViwRepositoryMedia> getRepMediaByCodiSessione(Integer codiSessione);
	//List<ViwRepositoryMedia> getRepMediaByCodiSurveyOrderMedia(Integer codiSurvey, Integer codiSessione);

	public  Object[] saveRepositoryDocument(TabRepositoryDocument tabRepositoryDocument, boolean insert);
	
	public List<TabRepositoryDocument> getRepDocExist(Integer codiSessione, String descNome) ;

	public Object[] saveRepositorySchedaRischi(TabRepositorySchedaRischi tabRepositorySchedaRischi);

	public Object[] updateSchedaDyn(TabSchedaDyn tabSchedaDyn);
	public Object[] saveSchedaDyn(TabSchedaDyn tabSchedaDyn);

	public void removeRepositorySchedaRischi(Integer codisessione) throws Exception;

	public List<TabRepositorySchedaRischi> getRepositoryDocumentDynByCodiSessione(Integer codiSessione);
	
	public List<TabRepositoryDocument> getRepDocD1Exist(Integer codiSessione);
	
	public void removeRepositoryD1 (Integer codisessione) throws Exception;
	
	public List<TabRepositoryMedia> getAllBySessioneAndCategoria(Integer codiSessione, String descCategoria);
	
	public List<TabRepositoryMedia> getFiveRepMediaBySessione(Integer codiSessione);
	
	public TabRepositoryDocument findDocBySessioneAndNome(Integer codiSessione, String descNome) ;

}
