package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.bean.UserFilter;
import com.rextart.saw.entity.TabAnagStatiUtente;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabRelSessioneUtenti;
import com.rextart.saw.entity.TabRole;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwProfiloUtente;
import com.rextart.saw.entity.ViwUtenti;



interface UserDao {
		
	public ViwUtenti getAuthUser(String username);
	public List<ViwUtenti> getAllUsers();
	public List<TabAreaCompetenza> getAllAreeComp();
	public List<TabRole> getAllRoles();
	public List<String> getAllDescAreaComp();
	public List<String> getAllDescRoles();
	public List<TabStruttura> getAllStrutture();
	public List<ViwUtenti> getFilterViwUser(UserFilter filter);
	public TabUtente getUtenteById(int codiUtente);
	public List<TabImprese> getAllImprese();
	public Object[] saveUser(TabUtente tabUtente, Integer impresa, List<Integer> selectedRegioni) throws Exception;
	public TabUtente getUserById(Integer codiUtente);
	public List<ViwProfiloUtente> getStruttureByRole(Integer codiRole);
	public List<TabZone> getTabZoneByAreaComp(Integer codiAreaComp);
	public List<TabGruppi> getTabGruppiByZona(Integer codiZona);
	public List<ViwUtenti> getRefTerOPByCodiAreaComp(Integer codiGruppo);
	public TabGruppi getTabGruppiDescGruppo(String descGruppo);
	public List<ViwUtenti> getUserMOIByAreaComp(Integer codiImpresa,Integer codiAreaComp);
	public List<ViwUtenti> getUtentiListByAreaComp(Integer codiAreaComp);
	public List<ViwUtenti> getOperNoaByCodiAreaComp(Integer codiAreaComp);
	List<ViwUtenti> getAllAOUByAreaComp(Integer codiAreaComp);
	List<ViwUtenti> getOperAOUByAreaComp(Integer codiAreaComp);
	List<ViwUtenti> getDGoperRToperByAreaComp(Integer codiAreaComp);
	List<ViwUtenti> getUtentiAssociatiByCodiSessione(Integer codiSessione);
	List<TabRelSessioneUtenti> getAllUtentiAssociatiByCodiSessione(Integer codiSessione);
	ViwUtenti getUtenteById(Integer codiUtente);
	ViwUtenti getViwUtenteById(Integer codiUtente);
	public List<TabAreaCompetenza> getAllAreeCompNotNazionale();
	public List<TabVendors> getAllVendor();
	public List<TabVendors> getVendorByCodi(Integer codiVendor);
	public List<ViwUtenti> getUtentiVendor(Integer codiVendor);
	public Boolean updatePassword(String pwd, String descUser);
	public TabUtente getTabUtenteByUserName(String descUser);
	Boolean resetPassword(TabUtente utente);
	public List<TabAnagStatiUtente> getAllStatiUtente();
	public List<ViwUtenti> getUserRefTerAndCordByAreaComp(Integer codiAreaComp);
	public List<String> getUserForMailDiSistema(List<String> selectedAreaDiComp,List<String> selectedRoles);
	public List<ViwUtenti> getAdmin();
	public List<ViwUtenti> getRefTerOPByRegione(List<Integer> codiUtente);
	public List<Integer> getUtentiByRegione(Integer codiIdRegione);
	public List<ViwUtenti> getOperAOUByRegione(List<Integer> codiUtente);
	public List<ViwUtenti> getUserRefTerAndCordByRegione(List<Integer> codiUtente);
	public List<ViwUtenti> getUserInCodiUtente(List<Integer> codiUtente);
	public List<ViwUtenti> getAllAssocSessionManuale(Integer codiSessione);
}