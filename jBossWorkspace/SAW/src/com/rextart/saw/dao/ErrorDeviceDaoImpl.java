package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabErrorDevice;


@Repository
public class ErrorDeviceDaoImpl implements ErrorDeviceDao{
	
	@PersistenceContext
    private EntityManager em;
	
	public void save(TabErrorDevice tabErrorDevice){
		em.persist(tabErrorDevice);
		em.flush();
	}
	
	public void deleteByCodiUtenteImei(Integer codiUtente, String codiImei){
		 em.createNamedQuery("TabErrorDevice.deleteByCodiUtenteImei").setParameter("codiUtente",codiUtente).setParameter("descImeiDevice",codiImei).executeUpdate();
		 em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public boolean isExistError(Integer codiUtente, Integer numeVersionApp, String descErrore){
		List<TabErrorDevice> list = new ArrayList<TabErrorDevice>();
		try {
			list = em.createNamedQuery("TabErrorDevice.existError").setParameter("codiUtente",codiUtente).setParameter("numeVersionApp",numeVersionApp).setParameter("descErrore",descErrore).getResultList();
			if(list.size()>0){
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabErrorDevice> getAll(){
		List<TabErrorDevice> list = new ArrayList<TabErrorDevice>();
		try {
			list = em.createNamedQuery("TabErrorDevice.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
}
