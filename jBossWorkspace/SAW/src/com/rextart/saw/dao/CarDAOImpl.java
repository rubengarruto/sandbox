package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rextart.saw.miaSezione.myBeans.Car;
import com.rextart.saw.rs.utility.CarDB;
import com.rextart.saw.rs.utility.CarFactory;

@Repository
public class CarDAOImpl implements CarDAOInt {

	private CarDB carDB = new CarDB();
	private List<Car> carsList = new ArrayList<Car>();
	

	public List<Car> getAllCars() {
		this.carsList = carDB.getAllCars();
		return carsList;
	}

	public void addCar(Car car) {
		CarFactory carFactory = new CarFactory();
		car.setId(carFactory.getRandomId());
		carsList.add(car);
	}
	
	public void removeCar(Car car) {				
		carsList.remove(car);		
	}

}
