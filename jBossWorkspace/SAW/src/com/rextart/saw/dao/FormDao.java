package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.ViwAnagAntenneTmaDiplexer;
import com.rextart.saw.entity.ViwAnagLovCaratt;
import com.rextart.saw.entity.ViwElemCarattWOrder;
import com.rextart.saw.entity.ViwElementiCaratteristiche;
import com.rextart.saw.entity.ViwElementiOggetti;
import com.rextart.saw.entity.ViwTipiOggetti;

public interface FormDao {
	
	public List<ViwTipiOggetti> getAllTipiOggetti();
	public List<ViwTipiOggetti> getTipiByOggetto(Integer codiOggetto);
	public List<ViwElementiOggetti> getAllElementiOggetti();
	public List<ViwElementiOggetti> getElementiByTipoOggetto(Integer codiTipoOggetto);
	public List<ViwElementiCaratteristiche> getAllElementiCaratteristiche();
	public List<ViwElementiCaratteristiche> getCaratteristicheByElemento(Integer codiElemento);
	public List<ViwElemCarattWOrder> getAllElementiCaratteristicheWithOrder();
	public List<ViwElemCarattWOrder> getCaratteristicheWithOrderByElemento(Integer codiElemento);
	public List<ViwAnagLovCaratt> getAllAnagLovCaratt();
	public List<ViwAnagLovCaratt> getAnagLovByCaratt(Integer codiCaratteristica);
	public List<ViwAnagLovCaratt> getAnagLovByCaratt(Integer codiCaratteristica, String descLabel);
	public List<ViwAnagLovCaratt> getAllAnagLovCarattCatalogo();
	public List<ViwAnagAntenneTmaDiplexer> getAllAnagAntenneTmaDiplexer();
	public List<ViwAnagAntenneTmaDiplexer> getAnagAntenneTmaDiplexerByCodiCatalogo(Integer codiCatalogo);
	public List<String> getDistinctDescMarcaByCodiCatalogo(Integer codiCatalogo);
	public List<ViwAnagAntenneTmaDiplexer> getModelloByDescMarca(Integer codiCatalogo, String descMarca);
}
