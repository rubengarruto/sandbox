package com.rextart.saw.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.rextart.saw.bean.CollaudoDatiGeneraliBean;
import com.rextart.saw.bean.SessioniFilter;
import com.rextart.saw.bean.WorkRequestBean;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagWorkRequest;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabClli;
import com.rextart.saw.entity.TabComuni;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabOggettoLock;
import com.rextart.saw.entity.TabPoloTecnologico;
import com.rextart.saw.entity.TabProvince;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSesVerbaleParziale;
import com.rextart.saw.entity.TabRelSessioneStati;
import com.rextart.saw.entity.TabRelSessioneUtenti;
import com.rextart.saw.entity.TabSchedaDyn;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.TabStoricoModifiche;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabWorkRequestColl;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwRisultatoSurvey;
import com.rextart.saw.entity.ViwSessImpUser;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwSessioniNote;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.DataConverter;

@Repository
public class SessioneDaoImpl implements SessioneDao{

	@PersistenceContext
	private EntityManager em;
	
	private static TabAnagStatiSessione stateAcquisito;
	private static TabAnagStatiSessione stateInLavorazione;
	private static TabAnagStatiSessione stateDisponibile;
	private static TabAnagStatiSessione stateAnnullato;
	private static TabAnagStatiSessione stateChiuso;
	
	@PostConstruct
	private  void init() {
		stateAcquisito = (TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateAcquisito").getSingleResult();
		stateInLavorazione = (TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateInLavorazione").getSingleResult();
		stateDisponibile = (TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateDisponibile").getSingleResult();
		stateAnnullato=(TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateAnnullato").getSingleResult();
		stateChiuso= (TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateChiuso").getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabWorkRequestColl> getWorkRequestBySessione(Integer codiSessione) {
		List<TabWorkRequestColl> listTabWrs = new ArrayList<TabWorkRequestColl>();
		try {
			listTabWrs = em.createNamedQuery("TabWorkRequestColl.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
			for (TabWorkRequestColl tabWorkRequestColl : listTabWrs) {
				tabWorkRequestColl.setTabAnagWorkRequest(em.find(TabAnagWorkRequest.class, tabWorkRequestColl.getCodiAnagWrs()));
			}
			
			return listTabWrs;
		} catch (Exception e) {
		}
		return listTabWrs;
	}

	
	@Override
	public void update(CollaudoDatiGeneraliBean selectedBean, Integer codiUser) throws Exception {
		TabSessioni tabSessioni = em.find(TabSessioni.class, selectedBean.getCodiSessione());
		tabSessioni = DataConverter.getTabSessioni(tabSessioni, selectedBean);
		em.merge(tabSessioni);
		TabStoricoModifiche tabMod = new TabStoricoModifiche(tabSessioni.getCodiSessione(), codiUser);
		em.persist(tabMod);
		em.flush();
	}
	
	@Override
	public void unlock(ViwSessioni sessioneSelected, Integer codiUser,Integer codiOggetto)throws Exception {
		TabSessioni tabSessioni = em.find(TabSessioni.class, sessioneSelected.getCodiSessione());
		switch (codiOggetto) {
		case Const.ALLEGATO_A:
			tabSessioni.setFlgLockA(false);
			break;
		case Const.ALLEGATO_B:
			tabSessioni.setFlgLockB(false);
			break;
		case Const.ALLEGATO_C:
			tabSessioni.setFlgLockC(false);
			break;
		case Const.ALLEGATO_D:
			tabSessioni.setFlgLockD(false);
			break;
		case Const.ALLEGATO_E:
			tabSessioni.setFlgLockE(false);
			break;
		case Const.ALLEGATO_ERICSSON:
			tabSessioni.setFlgLockEricsson(false);
			break;
		case Const.ALLEGATO_HUAWEI:
			tabSessioni.setFlgLockHuawei(false);
			break;
		case Const.ALLEGATO_NOKIA:
			tabSessioni.setFlgLockNsn(false);
			break;
		}
			em.merge(tabSessioni);
		TabOggettoLock tabOggettoLock = new TabOggettoLock();
		tabOggettoLock.setCodiSessione(tabSessioni.getCodiSessione());
		tabOggettoLock.setCodiUtente(codiUser);
		tabOggettoLock.setFlagLock(false);
		tabOggettoLock.setCodiOggetto(codiOggetto);
		tabOggettoLock.setTimeLastOperation(new Date());
		em.persist(tabOggettoLock);
		em.flush();
		
	}
	@Override
	public TabWorkRequestColl saveWorkRequest(WorkRequestBean workRequestBean,Integer codiUtente) throws Exception {
		
		TabWorkRequestColl tab = null;
		  try {
			  tab = (TabWorkRequestColl) em.createNamedQuery("TabWorkRequestColl.findByCodiSessioneDescNumeroWrs").setParameter("codiSessione", workRequestBean.getCodiSessione()).setParameter("descNumeroWrs", workRequestBean.getDescNumeroWrs())
		     .getSingleResult();
		  } catch (NoResultException nre) {
			  tab = null;
		  }
	
		if(tab==null){
		TabWorkRequestColl newWR = new TabWorkRequestColl(workRequestBean);
		em.persist(newWR);
		TabStoricoModifiche tabMod = new TabStoricoModifiche(workRequestBean.getCodiSessione(), codiUtente);
		em.persist(tabMod);
		em.flush();
		newWR.setTabAnagWorkRequest(em.find(TabAnagWorkRequest.class, newWR.getCodiAnagWrs()));
		return newWR;
		}else
			return null;
		
	}
	
	@Override
	public void removeWorkRequest(TabWorkRequestColl selectedWorkRequest,
			Integer codiUtente) throws Exception {
		TabWorkRequestColl workRequestToDelete =em.find(TabWorkRequestColl.class, selectedWorkRequest.getCodiWrsColl());
		em.remove(workRequestToDelete);
		em.flush();
	}

	
	@SuppressWarnings("unchecked")
	public List<TabSessioni> getAllSessioni() {
		List<TabSessioni> list = new ArrayList<TabSessioni>();
		try {
			list = em.createNamedQuery("TabSessioni.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isUserAssocSessione(Integer codiUtente, Integer codiSessione){
		List<TabRelSessioneUtenti> list = new ArrayList<TabRelSessioneUtenti>();
		try{
			list = em.createNamedQuery("TabRelSessioneUtenti.findByIdUserIdSessione").setParameter("codiUtente", codiUtente).setParameter("codiSessione", codiSessione).getResultList();
			if(list.size()>0){
				return true;
			}
		}catch (Exception e){
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isLockUnLockOggetto(Integer codiSessione, Integer codiOggetto, boolean flagLock){
		String queryControlFlag = "SELECT t FROM TabSessioni t WHERE t.codiSessione= :codiSessione and t."+
				Constants.mapOggettoFlagLock.get(codiOggetto)+ " = :flagControl";
		System.out.println("queryControlFlag: "+queryControlFlag);
		
		List<TabSessioni> list = new ArrayList<TabSessioni>();
		try {
			list = em.createQuery(queryControlFlag).setParameter("flagControl", true).setParameter("codiSessione", codiSessione).getResultList();
			if(list.size() > 0 ){
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}
	
	public void lockUnLockDoc(Integer codiSessione, Integer codiOggetto, Integer codiUtente, boolean flagLock){
		String queryUpdateFlag = "UPDATE TabSessioni set "+Constants.mapOggettoFlagLock.get(codiOggetto)+
				"= :flagLock WHERE codiSessione = :codiSessione";
		System.out.println("queryUpdateFlag: "+queryUpdateFlag);
		
		em.createQuery(queryUpdateFlag).setParameter("flagLock", flagLock).setParameter("codiSessione", codiSessione).executeUpdate();
	}
	
	public void storeLock(Integer codiSessione, Integer codiOggetto, Integer codiUtente, boolean flagLock){
		TabOggettoLock tabOggettoLock= new TabOggettoLock();
		tabOggettoLock.setCodiOggetto(codiOggetto);
		tabOggettoLock.setCodiSessione(codiSessione);
		tabOggettoLock.setCodiUtente(codiUtente);
		tabOggettoLock.setFlagLock(flagLock);
		tabOggettoLock.setTimeLastOperation(new Date());
		em.persist(tabOggettoLock);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getAllCollaudiInLavorazione(Integer statoFiltro) {
		List<ViwSessioni> listCollaudiInLavorazione = new ArrayList<ViwSessioni>();
		try {
			listCollaudiInLavorazione = em.createNamedQuery("ViwSessioni.findCollaudiInLavorazione").setParameter("stato", Const.CHIUSO).setParameter("statoFiltro",statoFiltro).getResultList();
		} catch (Exception e) {}
		return listCollaudiInLavorazione;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiInLavorazioneByAreaComp(Integer codiAreaComp,Integer statoFiltro) {
		List<ViwSessioni> listCollaudiInLavorazione = new ArrayList<ViwSessioni>();
		try {
			listCollaudiInLavorazione = em.createNamedQuery("ViwSessioni.findCollaudiInLavorazioneByAreaComp").setParameter("stato", Const.CHIUSO).setParameter("codiAreaComp", codiAreaComp).setParameter("statoFiltro",statoFiltro).getResultList();
		} catch (Exception e) {}
		return listCollaudiInLavorazione;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getAllCollaudiArchiviati() {
		List<ViwSessioni> listCollaudiArchiviati = new ArrayList<ViwSessioni>();
		try {
			listCollaudiArchiviati = em.createNamedQuery("ViwSessioni.findCollaudiArchiviati").setParameter("stato", Const.CHIUSO).getResultList();
		} catch (Exception e) {}
		return listCollaudiArchiviati;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiArchiviatiByAreaComp(Integer codiAreaComp) {
		List<ViwSessioni> listCollaudiArchiviati = new ArrayList<ViwSessioni>();
		try {
			listCollaudiArchiviati = em.createNamedQuery("ViwSessioni.findCollaudiArchiviatiByAreaComp").setParameter("stato", Const.CHIUSO).setParameter("codiAreaComp", codiAreaComp).getResultList();
		} catch (Exception e) {}
		return listCollaudiArchiviati;
	}
	
	public Integer getCodiAreaCompetenza(String descSiglaAreaComp){
		TabAreaCompetenza tabAreaCompetenza= new TabAreaCompetenza();
		try {
			tabAreaCompetenza= (TabAreaCompetenza) em.createNamedQuery("TabAreaCompetenza.findBySigla").setParameter("descSiglaAreaComp", descSiglaAreaComp).getSingleResult();
		} catch (Exception e) {
		}
		return tabAreaCompetenza.getCodiAreaComp();
	}
	
	
	
	@Override
	 public Object [] saveCollaudo(TabSessioni tabSessioni) throws Exception{
	    	Object[] obj = new Object[3];
	    	Integer codiSessione=null;
	    	boolean result = false;
	    	Integer error = 0;
	    	try {
	    		Timestamp currentDate = new Timestamp(System.currentTimeMillis());

	    		tabSessioni.setTimeAcquisizione(currentDate);
	    		em.persist(tabSessioni);
	    		codiSessione=tabSessioni.getCodiSessione();

	    		TabRelSessioneStati relStati= new TabRelSessioneStati();
	    		
	    		relStati.setTimeData(currentDate);
	    		relStati.setCodiSessione(tabSessioni.getCodiSessione());
	    		relStati.setCodiStato(stateAcquisito.getCodiStato());
	    		em.persist(relStati);
	    		em.flush();
	    		result= true;
			} catch (Exception e) {
				System.out.println("Codice collaudo gi� presente");
			}
	    	
	    	obj[0] = result;
			obj[1] = error;
			obj[2] = codiSessione;
	    	return obj;
	    }

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public static TabAnagStatiSessione getStateCreate() {
		return stateAcquisito;
	}

	public static void setStateCreate(TabAnagStatiSessione stateCreate) {
		SessioneDaoImpl.stateAcquisito = stateCreate;
	}

	public static TabAnagStatiSessione getStateInLavorazione() {
		return stateInLavorazione;
	}

	public static void setStateInLavorazione(TabAnagStatiSessione stateInLavorazione) {
		SessioneDaoImpl.stateInLavorazione = stateInLavorazione;
	}

	public static TabAnagStatiSessione getStateDisponibile() {
		return stateDisponibile;
	}

	public static void setStateDisponibile(TabAnagStatiSessione stateDisponibile) {
		SessioneDaoImpl.stateDisponibile = stateDisponibile;
	}

	public static TabAnagStatiSessione getStateAnnullato() {
		return stateAnnullato;
	}

	public static void setStateAnnullato(TabAnagStatiSessione stateAnnullato) {
		SessioneDaoImpl.stateAnnullato = stateAnnullato;
	}

	public static TabAnagStatiSessione getStateChiuso() {
		return stateChiuso;
	}

	public static void setStateChiuso(TabAnagStatiSessione stateChiuso) {
		SessioneDaoImpl.stateChiuso = stateChiuso;
	}

	@Override
	public Integer getCodiVendor(String descVendor) {
		
		TabVendors tabVendors= new TabVendors();
		try {
			tabVendors= (TabVendors) em.createNamedQuery("TabVendors.findByDesc").setParameter("descVendor", descVendor).getSingleResult();
		} catch (Exception e) {
		}
		return tabVendors.getCodiVendor();
	}

	@Override
	public  ViwSessioni findViwSessioni(Integer codiIdSessione) {
		ViwSessioni sessione = em.find(ViwSessioni.class, codiIdSessione);
		em.refresh(sessione);
		return sessione;
	}


	@SuppressWarnings("unchecked")
	@Override
	public String getDescGruppoByCLLI(String clliSito){
		List<TabClli> tabClli = new ArrayList<TabClli>();
		try {
			 tabClli=  em.createNamedQuery("TabClli.findByClliSito").setParameter("clliSito", clliSito).getResultList();
		} catch (Exception e) {
		}
		if(tabClli!=null && tabClli.size()>0)
			return tabClli.get(0).getDescNou();
		else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwUtenti> getAllUserAssocSession(Integer codiSessione) {
		List<ViwUtenti> utentiList=new ArrayList<ViwUtenti>();
		try {
			utentiList = em.createNamedQuery("ViwUtenti.findAllAssocSession").setParameter("codiSessione", codiSessione).setParameter("struttura", Const.AOU).setParameter("role", Const.Referente_Territoriale_OP).setParameter("codiVendor", Const.UTENTE_VENDOR).getResultList();
		} catch (Exception e) {
		}
		return utentiList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwUtenti> getAllUserMailFineCollaudo(List<Integer> listCodiUtenti){
		List<ViwUtenti> listUtentiFiltered = new ArrayList<ViwUtenti>();
		try{
			listUtentiFiltered = em.createNamedQuery("ViwUtenti.findByCodiUtenteAndCodiRole").setParameter("codiUtente", listCodiUtenti).setParameter("roleRefTeROP", Const.Referente_Territoriale_OP).setParameter("roleRefTer", Const.Referente_Territoriale).setParameter("roleCorOp", Const.Coordinatore_Operativo).setParameter("roleOp", Const.Operatore).setParameter("roleVendor", Const.UTENTE_VENDOR).getResultList();
		} catch(Exception e){
			e.printStackTrace();
		}
		return listUtentiFiltered;
	}
	
	private void persistTabRelSessioneUtenti(Integer codiUser, Integer codiSessione) {
		TabRelSessioneUtenti tabRelSessioneUtenti = new TabRelSessioneUtenti();
		tabRelSessioneUtenti.setCodiUtente(codiUser);
		tabRelSessioneUtenti.setCodiSessione(codiSessione);
		em.persist(tabRelSessioneUtenti);
	}
	@Override
	public Boolean saveAssocUtente(List<ViwUtenti> utenti, Integer codiSessione,List<Integer> codiUserAssoc) {
		Boolean result = false;
		try {
			if(codiUserAssoc.size()>0)
			em.createNamedQuery("TabRelSessioneUtenti.deleteByCodisUserAndSessione").setParameter("codiUtente", codiUserAssoc).setParameter("codiSessione", codiSessione).executeUpdate();
			for (ViwUtenti viwUtenti : utenti) {
				try {
						persistTabRelSessioneUtenti(viwUtenti.getCodiUtente(), codiSessione);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			em.flush();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;	
	}
	
	@Override
	public Boolean saveAssocUtenteOperAOU(List<ViwUtenti> utenti, Integer codiSessione) {
		Boolean result = false;
		try {
			for (ViwUtenti viwUtenti : utenti) {
				try {
						persistTabRelSessioneUtenti(viwUtenti.getCodiUtente(), codiSessione);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			em.flush();
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;	
	}
	
	@Override
	public void saveAssocUtenteRefTerOPByCodiAreaComp(List<ViwUtenti> utenti, Integer codiSessione) {
		try {
			for (ViwUtenti viwUtenti : utenti) {
				try {
						persistTabRelSessioneUtenti(viwUtenti.getCodiUtente(), codiSessione);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//	@Override
//	public boolean findByCodiUserCodiAreaComp(Integer codiSessione,Integer codiUtente, Integer codiAreaComp){
//		
//		List<TabRelSessioneUtenti> list = null;
//		list = em.createNamedQuery("TabRelSessioneUtenti.findByCodiUserCodiAreaComp").setParameter("codiSessione", codiSessione).setParameter("codiUtente", codiUtente).setParameter("codiAreaComp", codiAreaComp).getResultList();
//		if(list.size()>0){
//			return true;
//		}
//		return false;
//	}
	
	@Override
	public TabAnagSistemiBande findBandaSistemaByCodiImpianto(String descIdentificativo){
		
		TabAnagSistemiBande tabAnagSistemiBande =null;
		try {
			tabAnagSistemiBande =(TabAnagSistemiBande) em.createNamedQuery("TabAnagSistemiBande.findBandaSistemaByCodiImpianto").setParameter("descIdentificativo", descIdentificativo).getSingleResult();
		} catch (Exception e) {
			return null;
		}
			return tabAnagSistemiBande;
		
	}
	
	public void changeStatoSessione(Integer codiSessione, Integer codiNewStato){
		UtilLog.printCambioStatoSessione(codiSessione, codiNewStato);
		
		insertRelSessioneStati(codiSessione, codiNewStato);
	}
	
	public TabSessioni getSessioneByCodi(Integer codiSessione){
		return em.find(TabSessioni.class, codiSessione);
	}
	
	public boolean isSessioneStatoAcquisito(Integer codiSessione){
		ViwSessioni viwSessioni = findViwSessioni(codiSessione);
		if(viwSessioni.getCodiStato().equals(stateAcquisito.getCodiStato())){
			return true;
		}
		
		return false;
	}
	
	public Integer getCodiStateInLavorazione(){
		return stateInLavorazione.getCodiStato();
	}
	
	public void insertRelSessioneStati(Integer codiSessione, Integer codiStato){
		TabRelSessioneStati relStati= new TabRelSessioneStati();
		Timestamp currentDate = new Timestamp(System.currentTimeMillis());
		relStati.setTimeData(currentDate);
		relStati.setCodiSessione(codiSessione);
		relStati.setCodiStato(codiStato);
		em.persist(relStati);
		em.flush();
	}
	
	public Integer getCodiStateDisponibile(){
		return stateDisponibile.getCodiStato();
	}
	
	public boolean isSessioneStatoDisponibile(Integer codiSessione){
		ViwSessioni viwSessioni = findViwSessioni(codiSessione);
		if(viwSessioni.getCodiStato().equals(stateDisponibile.getCodiStato())){
			return true;
		}
		
		return false;
	}
	public boolean isSessioneStatoInLavorazione(Integer codiSessione){
		ViwSessioni viwSessioni = findViwSessioni(codiSessione);
		if(viwSessioni.getCodiStato().equals(stateInLavorazione.getCodiStato())){
			return true;
		}
		
		return false;
	}
	
	public boolean isSessioneStatoAnnullato(Integer codiSessione){
		ViwSessioni viwSessioni = findViwSessioni(codiSessione);
		if(viwSessioni.getCodiStato().equals(stateAnnullato.getCodiStato())){
			return true;
		}
		
		return false;
	}

	public Integer  getStateAcquisito() {
		return stateAcquisito.getCodiStato();
	}

	public static void setStateAcquisito(TabAnagStatiSessione stateAcquisito) {
		SessioneDaoImpl.stateAcquisito = stateAcquisito;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelOggettiTipo> getListRelOggettiTipoBySessione(CollaudoDatiGeneraliBean sessione){
		List<TabRelOggettiTipo> list = new ArrayList<TabRelOggettiTipo>();
		try {

			List<Integer> codisOggetto = new ArrayList<Integer>();

			if(sessione.getFlgAllegatoA())
				codisOggetto.add(Const.ALLEGATO_A);
			if(sessione.getFlgAllegatoB())
				codisOggetto.add(Const.ALLEGATO_B);
			if(sessione.getFlgAllegatoC())
				codisOggetto.add(Const.ALLEGATO_C);
			if(sessione.getFlgAllegatoE())
				codisOggetto.add(Const.ALLEGATO_E);
			if(sessione.getFlgAllegatoD())
				codisOggetto.add(Const.ALLEGATO_D);
			if (sessione.getFlgAllegatoD1())
				codisOggetto.add(Const.ALLEGATO_D1);
			if(sessione.getFlgApparatoNsn())
				codisOggetto.add(Const.ALLEGATO_NOKIA);
			if(sessione.getFlgApparatoEricsson())
				codisOggetto.add(Const.ALLEGATO_ERICSSON);
			if(sessione.getFlgApparatoHuawei())
				codisOggetto.add(Const.ALLEGATO_HUAWEI);

			list = (List<TabRelOggettiTipo>) em.createNamedQuery("TabRelOggettiTipo.findByListCodiOggetto").setParameter("listCodiOggetto", codisOggetto).getResultList();
		} catch (Exception e) {
		}

		return list;
	}
	
	@Override
	public Boolean saveTabSurvey(List<TabRelOggettiTipo> tabOggettiTipos, Integer codiSessione){
		Boolean result = false;
		try {
			for (TabRelOggettiTipo tabRelOggettiTipo : tabOggettiTipos) {
				try {
				persistTabSurvey(tabRelOggettiTipo.getCodiTipoOggetto(), tabRelOggettiTipo.getCodiOggetto(), codiSessione);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			em.flush();
			result= true;
		} catch (Exception e) {
		}
		return result;
	}
	
	private void persistTabSurvey(Integer codiTipoOggetto,Integer codiOggetto, Integer codiSessione) {
		TabSurvey tabSurvey = new TabSurvey();
		tabSurvey.setCodiSessione(codiSessione);
		tabSurvey.setCodiOggetto(codiOggetto);
		tabSurvey.setCodiTipoOggetto(codiTipoOggetto);
		em.persist(tabSurvey);
	}
	
	@SuppressWarnings("unchecked")
	public List<ViwSessioni> getViwSessioniAll(){
		List<ViwSessioni> listViwAll = new ArrayList<ViwSessioni>();
		try {
			listViwAll =(List<ViwSessioni>) em.createNamedQuery("ViwSessioni.findAll").getResultList();
		} catch (Exception e) {
		}
		return listViwAll;
	}
	
	public List<ViwSessioni> getSessioniDownload(){
		List<ViwSessioni> listViwAll = getViwSessioniAll();
		List<ViwSessioni> listReturn = new ArrayList<ViwSessioni>();
		for(ViwSessioni viwSess : listViwAll){
			if(viwSess.getCodiStato().equals(stateDisponibile.getCodiStato()) || viwSess.getCodiStato().equals(stateInLavorazione.getCodiStato())){
				listReturn.add(viwSess);
			}
		}
		return listReturn;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getViwSessioneByCodi(Integer codiSessione){
		List<ViwSessioni> list = new ArrayList<ViwSessioni>();
		try {
			list = (List<ViwSessioni>) em.createNamedQuery("ViwSessioni.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	public void deleteWorkRequestByCodiSessione(Integer codiSessione){
		em.createNamedQuery("TabWorkRequestColl.deleteByCodiSessione").setParameter("codiSessione", codiSessione).executeUpdate();
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRelRitardiSessioni> getRitardiBySessione(Integer codiSessione){
		List<TabRelRitardiSessioni> list = new ArrayList<TabRelRitardiSessioni>();
		try {
			list = (List<TabRelRitardiSessioni>) em.createNamedQuery("TabRelRitardiSessioni.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	/*
	 *TODO : FixATO 05/03/2018...
	 *
	 */

	@SuppressWarnings("unchecked")
	@Override
	public TabGruppi getGruppoByDescGruppo(String descGruppo) {
		List<TabGruppi> gruppi= new ArrayList<TabGruppi>();
		try {
			 gruppi = (List<TabGruppi>) em.createNamedQuery("TabGruppi.findByDesc").setParameter("descGruppo", descGruppo).getResultList();
		} catch (Exception e) {
		}
		return gruppi.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public  List<TabAreaCompetenza> getAllAreaCompetenza() {
		List<TabAreaCompetenza> list = new ArrayList<TabAreaCompetenza>();
		try {
			list = em.createNamedQuery("TabAreaCompetenza.findAllNotNazionale").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAreaCompRegioni> getRegioneByAreaComp(Integer codiAreaComp){
		List<ViwAreaCompRegioni> list = new ArrayList<ViwAreaCompRegioni>();
		try {
			list = (List<ViwAreaCompRegioni>) em.createNamedQuery("ViwAreaCompRegioni.findByAreaComp").setParameter("codiAreaComp", codiAreaComp).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabProvince> getProvinceByIdRegione(String codiRegione) {
		List<TabProvince> list = new ArrayList<TabProvince>();
		try {
			list = (List<TabProvince>) em.createNamedQuery("TabProvince.findByRegionId").setParameter("codiRegione", codiRegione).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabComuni> getComuneByIdProvincia(String codiProvincia) {
		List<TabComuni> list = new ArrayList<TabComuni>();
		try {
			list = (List<TabComuni>) em.createNamedQuery("TabComuni.findByIdProvincia").setParameter("codiProvincia", codiProvincia).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@Override
	public String getRegioneByNome(String nomeRegione) {
		TabRegioni regione=null;
		try {
			regione= (TabRegioni) em.createNamedQuery("TabRegioni.findRegioneByNome").setParameter("nomeMaiuscolo", nomeRegione).getSingleResult();
		} catch (Exception e) {}
			
		return regione.getCodiRegione();
	}

	@Override
	public String getDescRegioneByNome(String nomeMaiuscolo) {
		try {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<TabRegioni> criteria = builder.createQuery(TabRegioni.class);
		Root<TabRegioni> root = criteria.from(TabRegioni.class);
		criteria.select(root);
		List<Predicate> prList = new ArrayList<Predicate>();
		if(nomeMaiuscolo!=null && !nomeMaiuscolo.equals(Const.EMPTY_STRING)){
			prList.add(builder.like(builder.upper(root.get("nomeMaiuscolo")), "%"+nomeMaiuscolo.toUpperCase()+"%"));
		}
		Predicate[] list = new Predicate[prList.size()];
		list = prList.toArray(list);
		Predicate restriction = builder.and(list);
		criteria.where(restriction);
		return em.createQuery(criteria).getSingleResult().getNomeMaiuscolo();
		} catch (Exception e) {
			e.printStackTrace();
			return Const.EMPTY_STRING;
		}
	}
	
	
	@Override
	public String  getProvinciaByNome(String nomeProvincia) {
		TabProvince provincia= (TabProvince) em.createNamedQuery("TabProvince.findProvinciaByNome").setParameter("nomeProvincia", nomeProvincia).getSingleResult();
		return provincia.getCodiProvincia();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabVendors> getAllVendors() {
		List<TabVendors> list = new ArrayList<TabVendors>();
		try {
			list = (List<TabVendors>) em.createNamedQuery("TabVendors.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerbale> getListVerbaliBySessioni(List<Integer> listCodiSessione) {
		List<ViwVerbale> list = new ArrayList<ViwVerbale>();
		try {
			list = (List<ViwVerbale>) em.createNamedQuery("ViwVerbale.findByCodiSessioneList").setParameter("codiSessioneList", listCodiSessione).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public TabRelSesVerbaleParziale findByOggettoSessione(Integer codiOggetto,Integer codiSessione) {
		List<TabRelSesVerbaleParziale> list = new ArrayList<TabRelSesVerbaleParziale>();
		try {
			list = (List<TabRelSesVerbaleParziale>) em.createNamedQuery("TabRelSesVerbaleParziale.findByOggettoSessione").setParameter("codiOggetto", codiOggetto).setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		TabRelSesVerbaleParziale tabRelSesVerbaleParziale = new TabRelSesVerbaleParziale();
    	if(list.size() > 0){
    		tabRelSesVerbaleParziale = list.get(0);
    	}
		return tabRelSesVerbaleParziale;
	}

	@Override
	public TabPoloTecnologico findPoloTecnologicoByRegione(String descRegione) {
		TabPoloTecnologico tab= null;
		try {
			tab = (TabPoloTecnologico) em.createNamedQuery("TabPoloTecnologico.findByRegione").setParameter("descRegione", descRegione).getSingleResult();
		} catch (Exception e) {
		}
		return tab;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabSchedaDyn> getSchedaDynByCodiSessioneList(List<Integer> codiSessioneList) {
		List<TabSchedaDyn> list = new ArrayList<TabSchedaDyn>();;
		try {
			list= (List<TabSchedaDyn>) em.createNamedQuery("TabSchedaDyn.findByCodiSessioneList").setParameter("codiSessioneList", codiSessioneList).getResultList();
		} catch (Exception e) {}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiInLavorazioneByUserVendor(Integer codiVendor,Integer statoFiltro) {
		List<ViwSessioni> listCollaudiInLavorazione = new ArrayList<ViwSessioni>();
		try {
			listCollaudiInLavorazione = em.createNamedQuery("ViwSessioni.findCollaudiInLavorazioneByUtenteVendor").setParameter("stato", Const.CHIUSO).setParameter("codiVendor", codiVendor).setParameter("statoFiltro",statoFiltro).getResultList();
		} catch (Exception e) {}
		return listCollaudiInLavorazione;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiArchiviatiByUserVendor(Integer codiVendor) {
		List<ViwSessioni> listCollaudiArchiviati = new ArrayList<ViwSessioni>();
		try {
			listCollaudiArchiviati = em.createNamedQuery("ViwSessioni.findCollaudiArchiviatiByUtenteVendor").setParameter("stato", Const.CHIUSO).setParameter("codiVendor", codiVendor).getResultList();
		} catch (Exception e) {}
		return listCollaudiArchiviati;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiInLavorazioneByMoi(Integer codiImpresa,Integer statoFiltro) {
		List<ViwSessioni> listCollaudiInLavorazioneDuplicate = new ArrayList<ViwSessioni>();
		List<ViwSessioni> listCollaudiInLavorazione= new ArrayList<ViwSessioni>();;
		try {
			listCollaudiInLavorazioneDuplicate = em.createNamedQuery("ViwSessioni.findCollaudiInLavorazioneByMoi").setParameter("stato", Const.CHIUSO).setParameter("codiImpresa", codiImpresa).setParameter("statoFiltro",statoFiltro).getResultList();
			for (ViwSessioni viwSessioni : listCollaudiInLavorazioneDuplicate) {
				if(!listCollaudiInLavorazione.contains(viwSessioni))
					listCollaudiInLavorazione.add(viwSessioni);
			}
		} catch (Exception e) {}
		return listCollaudiInLavorazione;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiInLavorazioneByMoiAssoc(Integer codiUtente,Integer statoFiltro) {
		List<ViwSessioni> listCollaudiInLavorazione = new ArrayList<ViwSessioni>();
		try {
			listCollaudiInLavorazione = em.createNamedQuery("ViwSessioni.findCollaudiInLavorazioneByMoiAssoc").setParameter("stato", Const.CHIUSO).setParameter("codiUtente", codiUtente).setParameter("statoFiltro",statoFiltro).getResultList();
		} catch (Exception e) {}
		return listCollaudiInLavorazione;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiArchiviatiByMoi(Integer codiImpresa) {
		List<ViwSessioni> listCollaudiArchiviatiDuplicate = new ArrayList<ViwSessioni>();
		List<ViwSessioni> listCollaudiArchiviati=new ArrayList<ViwSessioni>();
		try {
			listCollaudiArchiviatiDuplicate = em.createNamedQuery("ViwSessioni.findCollaudiArchiviatiByMoi").setParameter("stato", Const.CHIUSO).setParameter("codiImpresa", codiImpresa).getResultList();
			for (ViwSessioni viwSessioni : listCollaudiArchiviatiDuplicate) {
				if(!listCollaudiArchiviati.contains(viwSessioni))
					listCollaudiArchiviati.add(viwSessioni);
			}
		} catch (Exception e) {}
		return listCollaudiArchiviati;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> getCollaudiArchiviatiByMoiAssoc(Integer codiUtente) {
		List<ViwSessioni> listCollaudiArchiviati = new ArrayList<ViwSessioni>();
		try {
			listCollaudiArchiviati = em.createNamedQuery("ViwSessioni.findCollaudiArchiviatiByMoiAssoc").setParameter("stato", Const.CHIUSO).setParameter("codiUtente", codiUtente).getResultList();
		} catch (Exception e) {}
		return listCollaudiArchiviati;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessImpUser> getDitteByCodiSessione(Integer codiSessione) {
		List <ViwSessImpUser> listDitte = new ArrayList<ViwSessImpUser>();
		try{
			listDitte = em.createNamedQuery("ViwSessImpUser.findDittaByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		}catch (Exception e){}
		return listDitte;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessImpUser> getUsersByCodiImpresaAndCodiSessione(Integer codiImpresa, Integer codiSessione) {
		List<ViwSessImpUser> listUser = new ArrayList<ViwSessImpUser>();
		try {
			listUser = em.createNamedQuery("ViwSessImpUser.findUsersByCodiImpresaAndCodiSessione").setParameter("codiImpresa", codiImpresa).setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {}
		return listUser;
	}

	//	Metodi per invio a pev	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerbale> getListVerbaliByCodiSessione(Integer codiSessione) {
		List<ViwVerbale> list = new ArrayList<ViwVerbale>();
		try {
			list = (List<ViwVerbale>) em.createNamedQuery("ViwVerbale.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@Override
	public void setSentPev(Integer codiSessione) {
		try {
			TabSessioni tabSessione = getSessioneByCodi(codiSessione);
			tabSessione.setFlgSpeditoPev(true);
    		em.merge(tabSessione);
    		em.flush();
    	} catch (Exception e) {}
	}
	
	public void deleteRelSessioneutenti(Integer codiSessione){
		em.createNamedQuery("TabRelSessioneUtenti.deleteByCodiSessione").setParameter("codiSessione", codiSessione).executeUpdate();
		em.flush();
	}

	@Override
	public List<ViwSessioni> getFilterViwSessioni(SessioniFilter sessioniFilter) {
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<ViwSessioni> criteria = builder.createQuery(ViwSessioni.class);
			Root<ViwSessioni> root = criteria.from(ViwSessioni.class);
			criteria.select(root);
			List<Predicate> prList = new ArrayList<Predicate>();
			if(sessioniFilter.getCodiStato()!=null && !sessioniFilter.getCodiStato().equals(0))
				prList.add(builder.equal(root.get("codiStato"), sessioniFilter.getCodiStato()));
			if(sessioniFilter.getCodiSistema()!=null && !sessioniFilter.getCodiSistema().equals(0))
				prList.add(builder.equal(root.get("codiSistema"), sessioniFilter.getCodiSistema()));
			
			
			Predicate[] list = new Predicate[prList.size()];
			list = prList.toArray(list);
			Predicate restriction = builder.and(list);
			criteria.where(restriction);
			return em.createQuery(criteria).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<ViwSessioni>();
		}
	}

	@Override
	public ViwSessioni findViwSessioniToClone(Integer codiCollaudo) {
		ViwSessioni sessione;
		try {
			sessione= (ViwSessioni) em.createNamedQuery("ViwSessioni.findViwSessioniToClone").setParameter("codiCollaudo", codiCollaudo).getSingleResult();
			
		} catch (NoResultException n) {
			return null;
		}
		return sessione;
	}

	@Override
	public Integer clonaCollaudo(Integer codiCollaudo, Integer codiSessione,Integer codiAreaComp) {
		String result = (String) em.createNativeQuery("SELECT FNC_CLONA_SESSIONE(:codiCollaudo,:codiSessione,:codiAreaComp) FROM DUAL").setParameter("codiCollaudo", codiCollaudo).setParameter("codiSessione", codiSessione).setParameter("codiAreaComp", codiAreaComp).getSingleResult();
		
		if(result != null && !result.equals("")) {
		return new Integer(result);
		}
		else {
		return 0;	
			}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioni> findViwSessioniByCodiCollaudo(Integer codiCollaudo) {
		List<ViwSessioni> sessioni = new ArrayList<ViwSessioni>();
		try {
			sessioni= (List<ViwSessioni>) em.createNamedQuery("ViwSessioni.findByCodiCollaudo").setParameter("codiCollaudo", codiCollaudo).getResultList();
			
		} catch (Exception e) {}
		return sessioni;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessione(Integer codiSessione) {
		List<ViwRisultatoSurvey> listRisultati = new ArrayList<ViwRisultatoSurvey>();
		try {
			listRisultati = em.createNamedQuery("ViwRisultatoSurvey.findAllByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRisultati;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		List<ViwRisultatoSurvey> listRisultati = new ArrayList<ViwRisultatoSurvey>();
		try {
			listRisultati = em.createNamedQuery("ViwRisultatoSurvey.findAllByCodiSessioneAndCodiOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRisultati;
	}

	@Override
	public TabAnagStatiSessione loadStatoChiuso(){
		return  (TabAnagStatiSessione) em.createNamedQuery("TabAnagStatiSessione.findStateChiuso").getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRisultatoSurvey> getAllRisultatiNegativiRiservaByCodiSessione(Integer codiSessione) {
		List<ViwRisultatoSurvey> listRisultati = new ArrayList<ViwRisultatoSurvey>();
		try {
			listRisultati = em.createNamedQuery("ViwRisultatoSurvey.findAllByCodiSessioneNegativoRiserva").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRisultati;
	}

	@Override
	public String findIdentificativoByBandaAndSistema(
			Integer codiBanda, Integer codiSistema) {
		TabAnagSistemiBande tab = (TabAnagSistemiBande) em.createNamedQuery("TabAnagSistemiBande.findIdentificativoByBandaAndSistema").setParameter("codiBanda", codiBanda).setParameter("codiSistema", codiSistema).getSingleResult();
		return tab.getDescIdentificativo();
	}

	@Override
	public TabWorkRequestColl findLastWRByCodiSessione(Integer codiSessione, Integer codiAnagWrs) {
		TabWorkRequestColl tabWorkRequestColl = null;
//		List<TabWorkRequestColl> listaTabWorkRequestColls = new ArrayList<TabWorkRequestColl>();
		try {
			tabWorkRequestColl = (TabWorkRequestColl) em.createNamedQuery("TabWorkRequestColl.findLastWRByCodiSessione").setParameter("codiSessione", codiSessione).setParameter("codiAnagWrs",codiAnagWrs).setFirstResult(0).setMaxResults(1).getSingleResult();
		} catch (Exception e) {
		}
		return tabWorkRequestColl;
	}

	@Override
	public TabRegioni getIdRegioneByNome(String nomeMaiuscolo) {
		TabRegioni regione = null;
		try {
			regione =  (TabRegioni) em.createNamedQuery("TabRegioni.findIdRegioneByNome").setParameter("nomeMaiuscolo", nomeMaiuscolo).getSingleResult();
		} catch (Exception e) {
		}
		return regione;
	}

	@Override
	public List<ViwEsitoSurvey> getNoteTabletByCodiSessione(Integer codiSessione) {
		List<ViwEsitoSurvey> listNoteEsito = new ArrayList<ViwEsitoSurvey>();
		try {
			listNoteEsito = (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findNoteEsitoByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listNoteEsito;
	}

	@Override
	public void saveNotaWeb(String descNota, Integer codiSessione,String username) {
		// TODO Auto-generated method stub) {
		TabNote tabNote = new TabNote();
		tabNote.setDescNota(descNota);
		tabNote.setCodiSessione(codiSessione);
		tabNote.setFlgTablet(false);
		tabNote.setDescUser(username);
		em.persist(tabNote);
		em.flush();
	}

	@Override
	public List<TabNote> getNote(Integer codiSessione) {
		List<TabNote> listTabNote = new ArrayList<TabNote>();
		try {
			listTabNote = (List<TabNote>) em.createNamedQuery("TabNote.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listTabNote;
	}

//	@Override
//	public String getDescOggetto(Integer codiOggetto) {
//		String tabOggetti = null;
//		try {
//			tabOggetti = (String) em.createNamedQuery("TabOggetti.findDescByCodi").setParameter("codiOggetto", codiOggetto).getSingleResult();
//			
//		} catch (Exception e) {
//			
//		}
//		
//		return tabOggetti;
//	}

	@Override
	public void updateNotaWeb(TabNote tabNote) {
		try {
		em.merge(tabNote);
		em.flush();
		} catch (Exception e) {
			
		}
	}

	@Override
	public List<ViwEsitoSurvey> getNoteDitta(Integer codiSessione) {
		List<ViwEsitoSurvey> listNoteDitta = new ArrayList<ViwEsitoSurvey>();
		try {
			listNoteDitta = (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findNoteDittaByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listNoteDitta;
	}

	@Override
	public List<TabNote> getNoteDittaBySessione(Integer codiSessione) {
		List<TabNote> listaNoteDitta = new ArrayList<TabNote>();
		try {
			listaNoteDitta = em.createNamedQuery("TabNote.findNoteDittaByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
		return listaNoteDitta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSessioniNote> getSessioniNoteByListSessioni(
			List<Integer> listCodiSessioni) {
		List<ViwSessioniNote> list = new ArrayList<ViwSessioniNote>();
		try {
			list = em.createNamedQuery("ViwSessioniNote.findSessioniNoteByListSessioni").setParameter("listCodiSessioni", listCodiSessioni).getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	
}
