package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwSbloccoSezioni;
import com.rextart.saw.rs.utility.Constants;

@Repository
public class SurveyDaoImpl implements SurveyDao{
	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<TabSurvey> getListSurveyBySessione(List<Integer> listCodiSessione){
		List<TabSurvey> list = new ArrayList<TabSurvey>();
		try {
			list = (List<TabSurvey>)em.createNamedQuery("TabSurvey.findByCodiSessioni").setParameter("listCodiSessione", listCodiSessione).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabSurveyDettaglio> getListDettagliBySurvey(List<Integer> listCodiSurvey){
		List<TabSurveyDettaglio> list = new ArrayList<TabSurveyDettaglio>();
		try {
			list = (List<TabSurveyDettaglio>)em.createNamedQuery("TabSurveyDettaglio.getListByCodiSurvey").setParameter("listCodiSurvey", listCodiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabNoteCaratteristiche> getListNoteCaratt(List<Integer> listCodiSurvey){
		List<TabNoteCaratteristiche> list = new ArrayList<TabNoteCaratteristiche>();
		try {
			list = (List<TabNoteCaratteristiche>)em.createNamedQuery("TabNoteCaratteristiche.getListByCodiSurvey").setParameter("listCodiSurvey", listCodiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<TabRisultatoSurvey> getListRisultatiSurvey(List<Integer> listCodiSurvey){
		List<TabRisultatoSurvey> list = new ArrayList<TabRisultatoSurvey>();
		try {
			list = (List<TabRisultatoSurvey>)em.createNamedQuery("TabRisultatoSurvey.getListByCodiSurvey").setParameter("listCodiSurvey", listCodiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isExistSurveyEsito(Integer codiSurvey){
		List<TabRisultatoSurvey> list = new ArrayList<TabRisultatoSurvey>();
		try {
			list = (List<TabRisultatoSurvey>)em.createNamedQuery("TabRisultatoSurvey.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getResultList();
	    	if(list.size()>0){
	    		return true;
	    	}
		} catch (Exception e) {
		}
    	return false;
	}
	
	public void deleteSurveyBySessione(Integer codiSessione){
			em.createNamedQuery("TabSurvey.deleteByCodiSessione").setParameter("codiSessione", codiSessione).executeUpdate();
			em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public void saveSurveyForD1(TabRisultatoSurvey tabRisultatoSurvey, Integer codiSessione, Integer codiTipoOggetto){
			List<TabSurvey> tabSurveyList = (List<TabSurvey>)em.createNamedQuery("TabSurvey.findByCodiSessioneCodiTipoOggetto").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto", codiTipoOggetto).getResultList();
			
			TabSurvey tabSurveyExist = tabSurveyList.get(0);
			
			
			if(isExistSurveyEsito(tabSurveyExist.getCodiSurvey())){
				TabRisultatoSurvey risultatoExist = (TabRisultatoSurvey)em.createNamedQuery("TabRisultatoSurvey.findByCodiSurvey").setParameter("codiSurvey", tabSurveyExist.getCodiSurvey()).getSingleResult();
				risultatoExist.setCodiValoreRisultatoSurvey(tabRisultatoSurvey.getCodiValoreRisultatoSurvey());
				risultatoExist.setDescValoreRisultatoSurvey(Constants.mapRisultatoSurvey.get(tabRisultatoSurvey.getCodiValoreRisultatoSurvey()));
				
				em.merge(risultatoExist);
		    	em.flush();
			}else{
				tabRisultatoSurvey.setCodiSurvey(tabSurveyExist.getCodiSurvey());
				em.persist(tabRisultatoSurvey);
		    	em.flush();
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabSurvey> getListSurveyBySessioneByOggetto(int codiSessione,int codiOggetto) {
		List<TabSurvey> list = new ArrayList<TabSurvey>();
		try {
			list = (List<TabSurvey>)em.createNamedQuery("TabSurvey.findByCodiSessioneCodiOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto",codiOggetto).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessione(Integer codiSessione) {
		List<ViwSbloccoSezioni> list = new ArrayList<ViwSbloccoSezioni>();
		try {
			list = (List<ViwSbloccoSezioni>)em.createNamedQuery("ViwSbloccoSezioni.findByCodiSessione").setParameter("codiSessione", codiSessione).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@Override
	public void removeRisultatoSurveyByCodiSurvey(Integer codiSurvey) {
		ArrayList<Integer> listCodiSurvey = new ArrayList<Integer>();
		listCodiSurvey.add(codiSurvey);
		List<TabRisultatoSurvey> risultatoSurveys = getListRisultatiSurvey(listCodiSurvey);		
		for (TabRisultatoSurvey risultatoSurvey: risultatoSurveys) {
			em.remove(risultatoSurvey);
		}
		em.flush();
	}

	@Override
	public void removeRiferimentoFirmaByCodiSurvey(Integer codiSurvey) {
		
		List<TabRiferimentiFirma> risultatoSurveys = getListRiferimentiFirmaByCodiSurvey(codiSurvey);		
		for (TabRiferimentiFirma firma: risultatoSurveys) {
			em.remove(firma);
		}
		em.flush();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRiferimentiFirma> getListRiferimentiFirmaByCodiSurvey(Integer codiSurvey) {
		List<TabRiferimentiFirma> list = new ArrayList<TabRiferimentiFirma>();
		try {
			list = (List<TabRiferimentiFirma>)em.createNamedQuery("TabRiferimentiFirma.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessioneByCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		List<ViwSbloccoSezioni> list = new ArrayList<ViwSbloccoSezioni>();
		try {
			list = (List<ViwSbloccoSezioni>)em.createNamedQuery("ViwSbloccoSezioni.findByCodiSessioneByCodiOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@Override
	public void removeRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		
		List<TabRelSessioneStatiVerbale> relSessioneStatiVerbales = getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(codiSessione,codiOggetto);		
		for (TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale: relSessioneStatiVerbales) {
			em.remove(tabRelSessioneStatiVerbale);
		}
		em.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelSessioneStatiVerbale> getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		List<TabRelSessioneStatiVerbale> list = new ArrayList<TabRelSessioneStatiVerbale>();
		try {
			list = (List<TabRelSessioneStatiVerbale>)em.createNamedQuery("TabRelSessioneStatiVerbale.findByCodiOggettoCodiSess").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@Override
	public void saveRelSessioneStatiVerbale(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale) {
		em.persist(tabRelSessioneStatiVerbale);
    	em.flush();	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRiferimentiFirma> getListRiferimentiFirmaByCodiSurvey(List<Integer> listCodiSurvey) {
		List<TabRiferimentiFirma> list = new ArrayList<TabRiferimentiFirma>();
		try {
			list = (List<TabRiferimentiFirma>)em.createNamedQuery("TabRiferimentiFirma.getListByCodiSurvey").setParameter("listCodiSurvey", listCodiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRisultatoSurvey> getListRisultatoByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		List<TabRisultatoSurvey> list = new ArrayList<TabRisultatoSurvey>();
		List<TabRisultatoSurvey> listaFiltrata = new ArrayList<TabRisultatoSurvey>();
		boolean primoRisultato = true;
		try {
			list=(List<TabRisultatoSurvey>)em.createNamedQuery("TabRisultatoSurvey.findByCodiSessioneAndCodiOggetto").setParameter("codiSessione", codiSessione).setParameter("codiOggetto", codiOggetto).getResultList();
			for(TabRisultatoSurvey risultatoSurvey : list){
				if(primoRisultato){
					listaFiltrata.add(risultatoSurvey);
					primoRisultato=false;
				}
				boolean giąPresente = false;
				for(TabRisultatoSurvey risultatoFiltrato : listaFiltrata){
					if(risultatoFiltrato.getCodiSurvey().equals(risultatoSurvey.getCodiSurvey())){
						giąPresente=true;
						break;
					}
				}
				if(!giąPresente){
					listaFiltrata.add(risultatoSurvey);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaFiltrata;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabNote> getListNoteTablet(List<Integer> listCodiSurvey) {
		List<TabNote> list = new ArrayList<TabNote>();
		try {
			list = (List<TabNote>)em.createNamedQuery("TabNote.getListByCodiSurvey").setParameter("listCodiSurvey", listCodiSurvey).getResultList();
		} catch (Exception e) {
		}
    	return list;
	}
	
	@Override
	public TabSurveyDettaglio getSurveyDettaglioByFormKey(Integer codiSurvey, Integer codiElemento, Integer codiCaratteristica, String descLabel, Integer numeCella, Integer numeVia, Integer numeLivelloE)
	{
		TabSurveyDettaglio tabSurveyDettaglio = null;
		try
		{
			String queryName = descLabel == null ? "TabSurveyDettaglio.findByFormKeyWithoutDescLabel" : "TabSurveyDettaglio.findByFormKey";
			
			Query query = em.createNamedQuery(queryName)
					.setParameter("codiSurvey", codiSurvey)
					.setParameter("codiElemento", codiElemento)
					.setParameter("codiCaratteristica", codiCaratteristica)
					.setParameter("numeCella", numeCella)
					.setParameter("numeVia", numeVia)
					.setParameter("numeLivelloE", numeLivelloE);
			
			if(descLabel != null) query.setParameter("descLabel", descLabel);
			
			tabSurveyDettaglio = (TabSurveyDettaglio) query.getSingleResult();
		}
		catch(Exception e)
		{
		}
		
		return tabSurveyDettaglio;
	}
	
	@Override
	public TabSurveyDettaglio getValorizedSurveyDettaglioByCodiCaratteristica(Integer codiSurvey, Integer codiCaratteristica)
	{
		TabSurveyDettaglio tabSurveyDettaglio = null;
		
		try {
			tabSurveyDettaglio = (TabSurveyDettaglio) em.createNamedQuery("TabSurveyDettaglio.findValorizedSurveyDettaglioByCodiCaratteristica")
					.setParameter("codiSurvey", codiSurvey)
					.setParameter("codiCaratteristica", codiCaratteristica)
					.getSingleResult();
					
		} catch (Exception e) {
		}
		
    	return tabSurveyDettaglio;
	}
}
