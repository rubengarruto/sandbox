package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabDevice;

@Repository
public class DeviceDaoImpl implements DeviceDao{
	
	@PersistenceContext
    private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public boolean isDeviceExist(String descImei){
		List<TabDevice> list = new ArrayList<TabDevice>();
		try {
			list = em.createNamedQuery("TabDevice.findByImei").setParameter("descImei",descImei).getResultList();
			if(list.size()>0){
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}
}
