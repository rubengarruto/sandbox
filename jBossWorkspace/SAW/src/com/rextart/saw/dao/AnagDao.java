package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabAnagDocumenti;
import com.rextart.saw.entity.TabAnagLov;
import com.rextart.saw.entity.TabAnagRisultati;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabAnagWorkRequest;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCaratteristiche;
import com.rextart.saw.entity.TabCostrModAntenna;
import com.rextart.saw.entity.TabCountDocument;
import com.rextart.saw.entity.TabDettaglioCollaudo;
import com.rextart.saw.entity.TabElementi;
import com.rextart.saw.entity.TabFinalitaCollaudo;
import com.rextart.saw.entity.TabImpianti;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelCaratteristicheLov;
import com.rextart.saw.entity.TabRelCascadeCarattLov;
import com.rextart.saw.entity.TabRelElementiCaratt;
import com.rextart.saw.entity.TabRelOggettiElementi;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelServiziOggetti;
import com.rextart.saw.entity.TabServizi;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.entity.TabTipologieOggetto;
import com.rextart.saw.entity.TabZone;

public interface AnagDao {
	
	public List<TabAnagLov> getAllAnagLov();
	
	public List<TabCaratteristiche> getAllCaratteristiche();

	public List<TabElementi> getAllElementi();

	public List<TabOggetti> getAllOggetti();

	public List<TabServizi> getAllServizi();

	public List<TabRelCaratteristicheLov> getAllRelCaratteristicheLov() ;

	public List<TabRelElementiCaratt> getAllRelElementiCaratt() ;

	public List<TabRelOggettiElementi> getAllRelOggettiElementi();

	public List<TabRelServiziOggetti> getAllRelServiziOggetti();

	public List<TabTipologieOggetto> getAllTipologieOggetto();

	public List<TabRelOggettiTipo> getAllTabRelOggettiTipo() ;

	public List<TabRelOggettiTipo> getAllRelOggettiTipo();
	
	public List<TabStruttura> getAllStruttura();
	
	public List<TabSyncAnagrafica> getAllTabSyncAnagrafica() ;
	
	public TabSyncAnagrafica getByNameTable(String nameTable) ;
	
	public List<TabAnagStatiSessione> getAllAnagStatiSessione();
	
	public List<TabAnagAntenne> getAllAnagAntenne();
	
	public List<TabAnagDiplexer> getAllAnagDiplexer();
	
	public List<TabAnagTma> getAllAnagTma();
	
	public List<TabAnagRitardi> getAllRitardi();

	List<TabSistemi> getAllSistema();

	List<TabImpianti> getAllImpianti();

	List<TabZone> getAllUnitaTerr(Integer codiAreaComp);

	List<TabFinalitaCollaudo> getAllFinalitaColl();

	List<TabDettaglioCollaudo> getAllDettaglioColl();

	public List<TabBande> getAllBande();

	public List<TabAnagWorkRequest> getAllAnagWorkRequest();
	
	public List<TabAnagRisultati> getAllRisultati();
	
	public List<TabSistemi> getAllSistemi();
	
	public List<TabAnagDocumenti> getAllAnagDocumenti();
	
	public List<TabAnagDocumenti> getAllAnagDocumentiWithoutD1();
	
	public TabCountDocument findProgessiovoDocument(Integer codiSessione,Integer codiAnagDocumento);
	
	public void saveRitardi(TabAnagRitardi tabAnagRitardi);
	
	public void saveAntenne(TabAnagAntenne tabAnagAntenne);
	
	public void saveDiplexer(TabAnagDiplexer tabAnagDiplexer);
	
	public void saveTma(TabAnagTma tabAnagTma);
	
	public void updateSyncAnagrafica(TabSyncAnagrafica tabSyncAnagrafica);
	
	public TabAnagRitardi getAnagRitardiUnique(String descRitardo, boolean flgCheckOut);
	
	public TabAnagAntenne getAnagAntenneUnique(String descMarca, String descModello);
	
	public TabAnagDiplexer getAnagDiplexerUnique(String descMarca, String descModello);
	
	public TabAnagTma getAnagTmaUnique(String descMarca, String descModello);

	public List<TabAnagStatiSessione> getAllAnagStatiSessioneNotClosed();
	
	public void updateRitardi(TabAnagRitardi tabAnagRitardi);
	
	public TabAnagRitardi getAnagRitardiByCodiAnagRitardo(Integer codiAnagRitardo);

	public List<String> getAllAnagAntenneDescMarca();

	public List<TabAnagAntenne> getAllAnagAntenneByDescMarca(String descMarca);

	public List<String> getAllAnagTmaDescMarca();

	public List<String> getAllAnagDiplexerDescMarca();

	public List<TabAnagDiplexer> getAllAnagDiplexerByDescMarca(String descMarca);

	public List<TabAnagTma> getAllAnagTmaByDescMarca(String descMarca);
	
	public List<TabCostrModAntenna> getAllCostrModAntenna();
	
	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByCodiOggetto(Integer codiOggetto);

	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByPrimaryCaratt(Integer codiOggetto, Integer codiCarattPrimary, Integer codiAnagLovPrimary);
}
