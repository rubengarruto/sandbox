package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabAnagDocumenti;
import com.rextart.saw.entity.TabAnagLov;
import com.rextart.saw.entity.TabAnagRisultati;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabAnagWorkRequest;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCaratteristiche;
import com.rextart.saw.entity.TabCostrModAntenna;
import com.rextart.saw.entity.TabCountDocument;
import com.rextart.saw.entity.TabDettaglioCollaudo;
import com.rextart.saw.entity.TabElementi;
import com.rextart.saw.entity.TabFinalitaCollaudo;
import com.rextart.saw.entity.TabImpianti;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelCaratteristicheLov;
import com.rextart.saw.entity.TabRelCascadeCarattLov;
import com.rextart.saw.entity.TabRelElementiCaratt;
import com.rextart.saw.entity.TabRelOggettiElementi;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelServiziOggetti;
import com.rextart.saw.entity.TabServizi;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.entity.TabTipologieOggetto;
import com.rextart.saw.entity.TabZone;

@Repository
public class AnagDaoImpl implements AnagDao{

	@PersistenceContext
	private EntityManager em;

	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabSistemi> getAllSistema() {
		List<TabSistemi> listSistemi = new ArrayList<TabSistemi>();
		try {
			listSistemi = em.createNamedQuery("TabSistemi.findAll").getResultList();
		} catch (Exception e) {
		}
		return listSistemi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabImpianti> getAllImpianti() {
		List<TabImpianti> listImpianti = new ArrayList<TabImpianti>();
		try {
			listImpianti = em.createNamedQuery("TabImpianti.findAll").getResultList();
		} catch (Exception e) {
		}
		return listImpianti;
	}
	
	@SuppressWarnings("unchecked")
	@Override	
	public List<TabZone> getAllUnitaTerr(Integer codiAreaComp) {
		List<TabZone> listZone = new ArrayList<TabZone>();
		try {
			listZone = em.createNamedQuery("TabZone.findByAreaComp").setParameter("codiAreaComp", codiAreaComp).getResultList();
		} catch (Exception e) {
		}
		return listZone;
	}

	@SuppressWarnings("unchecked")
	@Override	
	public List<TabFinalitaCollaudo> getAllFinalitaColl() {
		List<TabFinalitaCollaudo> listTabFinalitaCollaudo = new ArrayList<TabFinalitaCollaudo>();
		try {
			listTabFinalitaCollaudo =em.createNamedQuery("TabFinalitaCollaudo.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTabFinalitaCollaudo;
	}

	@SuppressWarnings("unchecked")
	@Override	
	public List<TabDettaglioCollaudo> getAllDettaglioColl() {
		List<TabDettaglioCollaudo> listTabDettaglioCollaudo = new ArrayList<TabDettaglioCollaudo>();
		try {
			listTabDettaglioCollaudo =em.createNamedQuery("TabDettaglioCollaudo.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTabDettaglioCollaudo;
	}
	
	@SuppressWarnings("unchecked")
	@Override	
	public List<TabBande> getAllBande() {
		List<TabBande> listTabBande = new ArrayList<TabBande>();
		try {
			listTabBande =em.createNamedQuery("TabBande.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTabBande;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagWorkRequest> getAllAnagWorkRequest() {
		List<TabAnagWorkRequest> listTabAnagWorkRequest = new ArrayList<TabAnagWorkRequest>();
		try {
			listTabAnagWorkRequest =em.createNamedQuery("TabAnagWorkRequest.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTabAnagWorkRequest;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagLov> getAllAnagLov() {
		List<TabAnagLov> listAnagLov = new ArrayList<TabAnagLov>();
		try {
			listAnagLov = em.createNamedQuery("TabAnagLov.findAll").getResultList();
		} catch (Exception e) {
		}
		return listAnagLov;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public List<TabCaratteristiche> getAllCaratteristiche() {
		List<TabCaratteristiche> listCaratteristiche = new ArrayList<TabCaratteristiche>();
		try {
			listCaratteristiche = em.createNamedQuery("TabCaratteristiche.findAll").getResultList();
		} catch (Exception e) {
		}
		return listCaratteristiche;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabElementi> getAllElementi() {
		List<TabElementi> listElementi = new ArrayList<TabElementi>();
		try {
			listElementi = em.createNamedQuery("TabElementi.findAll").getResultList();
		} catch (Exception e) {
		}
		return listElementi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabOggetti> getAllOggetti() {
		List<TabOggetti> listOggetti = new ArrayList<TabOggetti>();
		try {
			listOggetti = em.createNamedQuery("TabOggetti.findAll").getResultList();
		} catch (Exception e) {
		}
		return listOggetti;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabServizi> getAllServizi() {
		List<TabServizi> listServizi = new ArrayList<TabServizi>();
		try {
			listServizi = em.createNamedQuery("TabServizi.findAll").getResultList();
		} catch (Exception e) {
		}
		return listServizi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelCaratteristicheLov> getAllRelCaratteristicheLov() {
		List<TabRelCaratteristicheLov> listRelCaratteristicheLov = new ArrayList<TabRelCaratteristicheLov>();
		try {
			listRelCaratteristicheLov = em.createNamedQuery("TabRelCaratteristicheLov.findAll").getResultList();
		} catch (Exception e) {
		}
		return listRelCaratteristicheLov;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelElementiCaratt> getAllRelElementiCaratt() {
		List<TabRelElementiCaratt> listRelElementiCaratt = new ArrayList<TabRelElementiCaratt>();
		try {
			listRelElementiCaratt = em.createNamedQuery("TabRelElementiCaratt.findAll").getResultList();
		} catch (Exception e) {
		}
		return listRelElementiCaratt;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelOggettiElementi> getAllRelOggettiElementi() {
		List<TabRelOggettiElementi> listRelOggettiElementi = new ArrayList<TabRelOggettiElementi>();
		try {
			listRelOggettiElementi = em.createNamedQuery("TabRelOggettiElementi.findAll").getResultList();
		} catch (Exception e) {
		}
		return listRelOggettiElementi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelServiziOggetti> getAllRelServiziOggetti() {
		List<TabRelServiziOggetti> listRelServiziOggetti = new ArrayList<TabRelServiziOggetti>();
		try {
			listRelServiziOggetti = em.createNamedQuery("TabRelServiziOggetti.findAll").getResultList();
		} catch (Exception e) {
		}
		return listRelServiziOggetti;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabTipologieOggetto> getAllTipologieOggetto() {
		List<TabTipologieOggetto> listTipologieOggetto = new ArrayList<TabTipologieOggetto>();
		try {
			listTipologieOggetto = em.createNamedQuery("TabTipologieOggetto.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTipologieOggetto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelOggettiTipo> getAllTabRelOggettiTipo() {
		List<TabRelOggettiTipo> listTabRelOggettiTipo = new ArrayList<TabRelOggettiTipo>();
		try {
			listTabRelOggettiTipo = em.createNamedQuery("TabRelOggettiTipo.findAll").getResultList();
		} catch (Exception e) {
		}
		return listTabRelOggettiTipo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelOggettiTipo> getAllRelOggettiTipo() {
		List<TabRelOggettiTipo> list = new ArrayList<TabRelOggettiTipo>();
		try {
			list = em.createNamedQuery("TabRelOggettiTipo.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabStruttura> getAllStruttura(){
		List<TabStruttura> list = new ArrayList<TabStruttura>();
		try {
			list = em.createNamedQuery("TabStruttura.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabSyncAnagrafica> getAllTabSyncAnagrafica() {
		List<TabSyncAnagrafica> list = new ArrayList<TabSyncAnagrafica>();
		try {
			list = em.createNamedQuery("TabSyncAnagrafica.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@Override
	public TabSyncAnagrafica getByNameTable(String nameTable) {
		TabSyncAnagrafica entity = (TabSyncAnagrafica) em.createNamedQuery("TabSyncAnagrafica.getByNameTable").setParameter("descNameTable", nameTable).getSingleResult();
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagStatiSessione> getAllAnagStatiSessione() {
		List<TabAnagStatiSessione> listAnagStatiSessione = new ArrayList<TabAnagStatiSessione>();
		try {
			listAnagStatiSessione = em.createNamedQuery("TabAnagStatiSessione.findAll").getResultList();
		} catch (Exception e) {
		}
		return listAnagStatiSessione;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagStatiSessione> getAllAnagStatiSessioneNotClosed() {
		List<TabAnagStatiSessione> listAnagStatiSessione = new ArrayList<TabAnagStatiSessione>();
		try {
			listAnagStatiSessione = em.createNamedQuery("TabAnagStatiSessione.findAllNotClosed").getResultList();
		} catch (Exception e) {
		}
		return listAnagStatiSessione;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagAntenne> getAllAnagAntenne() {
		List<TabAnagAntenne> list = new ArrayList<TabAnagAntenne>();
		try {
			list = em.createNamedQuery("TabAnagAntenne.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllAnagAntenneDescMarca() {
		List<String> list = new ArrayList<String>();
		try {
			list = em.createNamedQuery("TabAnagAntenne.findAllDistinctDescMarca").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllAnagDiplexerDescMarca() {
		List<String> list = new ArrayList<String>();
		try {
			list = em.createNamedQuery("TabAnagDiplexer.findAllDistinctDescMarca").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllAnagTmaDescMarca() {
		List<String> list = new ArrayList<String>();
		try {
			list = em.createNamedQuery("TabAnagTma.findAllDistinctDescMarca").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagAntenne> getAllAnagAntenneByDescMarca(String descMarca)
	{
		List<TabAnagAntenne> list = new ArrayList<TabAnagAntenne>();
		try {
			list = em.createNamedQuery("TabAnagAntenne.findByDescMarca").setParameter("descMarca", descMarca).getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagDiplexer> getAllAnagDiplexerByDescMarca(String descMarca)
	{
		List<TabAnagDiplexer> list = new ArrayList<TabAnagDiplexer>();
		try {
			list = em.createNamedQuery("TabAnagDiplexer.findByDescMarca").setParameter("descMarca", descMarca).getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagTma> getAllAnagTmaByDescMarca(String descMarca)
	{
		List<TabAnagTma> list = new ArrayList<TabAnagTma>();
		try {
			list = em.createNamedQuery("TabAnagTma.findByDescMarca").setParameter("descMarca", descMarca).getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagDiplexer> getAllAnagDiplexer() {
		List<TabAnagDiplexer> list = new ArrayList<TabAnagDiplexer>();
		try {
			list = em.createNamedQuery("TabAnagDiplexer.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagTma> getAllAnagTma() {
		List<TabAnagTma> list = new ArrayList<TabAnagTma>();
		try {
			list = em.createNamedQuery("TabAnagTma.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagRisultati> getAllRisultati(){
		List<TabAnagRisultati> list = new ArrayList<TabAnagRisultati>();
		try {
			list = em.createNamedQuery("TabAnagRisultati.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagRitardi> getAllRitardi(){
		List<TabAnagRitardi> list = new ArrayList<TabAnagRitardi>();
		try {
			list = em.createNamedQuery("TabAnagRitardi.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabSistemi> getAllSistemi(){
		List<TabSistemi> list = new ArrayList<TabSistemi>();
		try {
			list = em.createNamedQuery("TabSistemi.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagDocumenti> getAllAnagDocumentiWithoutD1() {
		List<TabAnagDocumenti> list = new ArrayList<TabAnagDocumenti>();
		try {
			list = em.createNamedQuery("TabAnagDocumenti.findAllWithoutD1").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabAnagDocumenti> getAllAnagDocumenti() {
		List<TabAnagDocumenti> list = new ArrayList<TabAnagDocumenti>();
		try {
			list = em.createNamedQuery("TabAnagDocumenti.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@Override
	public TabCountDocument findProgessiovoDocument(Integer codiSessione,Integer codiAnagDocumento) {
		try {
			TabCountDocument tab=(TabCountDocument) em.createNamedQuery("TabCountDocument.findBySessioneCodiDoc").setParameter("codiSessione", codiSessione).setParameter("codiAnagDocumento", codiAnagDocumento).getSingleResult();
			tab.setCountDocument(tab.getCountDocument()+1);
			em.merge(tab);
			em.flush();
			return tab;
		} catch (Exception e) {
			TabCountDocument tab = new TabCountDocument();
			tab.setCodiSessione(codiSessione);
			tab.setCodiAnagDocumento(codiAnagDocumento);
			tab.setCountDocument(1);
			em.persist(tab);
			em.flush();
			return tab;
		}
		
	}
	
	@Override
	public void saveRitardi(TabAnagRitardi tabAnagRitardi){
		 try {
			 em.persist(tabAnagRitardi);
			 em.flush();
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	}
	
	@Override
	public void saveAntenne(TabAnagAntenne tabAnagAntenne){
		try{
			em.persist(tabAnagAntenne);
			em.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveDiplexer(TabAnagDiplexer tabAnagDiplexer){
		try{
			em.persist(tabAnagDiplexer);
			em.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveTma(TabAnagTma tabAnagTma){
		try{
			em.persist(tabAnagTma);
			em.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateSyncAnagrafica(TabSyncAnagrafica tabSyncAnagrafica){
		try{
			em.merge(tabSyncAnagrafica);
			em.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public TabAnagRitardi getAnagRitardiUnique(String descRitardo, boolean flagCheckOut){
		TabAnagRitardi tabAnagRitardi = null;
		try {
			tabAnagRitardi = (TabAnagRitardi) em.createNamedQuery("TabAnagRitardi.findByDescRitAndFlgCheck").setParameter("descRitardo", descRitardo).setParameter("flagCheckOut", flagCheckOut).getSingleResult();
		} catch (Exception e) {		
		}
		return tabAnagRitardi;
	}
	
	@Override
	public TabAnagAntenne getAnagAntenneUnique(String descMarca, String descModello){
		TabAnagAntenne tabAnagAntenne = null;
		try{
			tabAnagAntenne = (TabAnagAntenne) em.createNamedQuery("TabAnagAntenne.findByDescMarcaAndModello").setParameter("descMarca", descMarca).setParameter("descModello", descModello).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tabAnagAntenne;
	}
	
	@Override
	public TabAnagDiplexer getAnagDiplexerUnique(String descMarca, String descModello){
		TabAnagDiplexer tabAnagDiplexer = null;
		try{
			tabAnagDiplexer = (TabAnagDiplexer) em.createNamedQuery("TabAnagDiplexer.findByDescMarcaAndModello").setParameter("descMarca", descMarca).setParameter("descModello", descModello).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tabAnagDiplexer;	
	}
	
	@Override
	public TabAnagTma getAnagTmaUnique(String descMarca, String descModello){
		TabAnagTma tabAnagTma = null;
		try{
			tabAnagTma = (TabAnagTma) em.createNamedQuery("TabAnagTma.findByDescMarcaAndModello").setParameter("descMarca", descMarca).setParameter("descModello", descModello).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tabAnagTma;	
	}

	@Override
	public void updateRitardi(TabAnagRitardi tabAnagRitardi) {
		try {
			 em.merge(tabAnagRitardi);
			 em.flush();
		 } catch (Exception e) {}		
	}

	@Override
	public TabAnagRitardi getAnagRitardiByCodiAnagRitardo(Integer codiAnagRitardo) {
		TabAnagRitardi tabAnagRitardi = null;
		try {
			tabAnagRitardi = (TabAnagRitardi) em.createNamedQuery("TabAnagRitardi.findByCodiAnagRitardo").setParameter("codiAnagRitardo", codiAnagRitardo).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tabAnagRitardi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabCostrModAntenna> getAllCostrModAntenna() {
		List<TabCostrModAntenna> list = new ArrayList<TabCostrModAntenna>();
		try {
			list = em.createNamedQuery("TabCostrModAntenna.findAll").getResultList();
		} catch (Exception e) {
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByCodiOggetto(Integer codiOggetto)
	{
		List<TabRelCascadeCarattLov> list = new ArrayList<>();
		try
		{
			list = em.createNamedQuery("TabRelCascadeCarattLov.findByCodiOggetto").setParameter("codiOggetto", codiOggetto).getResultList();
		}
		catch(Exception e){}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByPrimaryCaratt(Integer codiOggetto, Integer codiCarattPrimary, Integer codiAnagLovPrimary)
	{
		List<TabRelCascadeCarattLov> list = new ArrayList<>();
		try
		{
			list = em.createNamedQuery("TabRelCascadeCarattLov.findCarattByPrimaryCaratt")
					.setParameter("codiOggetto", codiOggetto)
					.setParameter("codiCarattPrimary", codiCarattPrimary)
					.setParameter("codiAnagLovPrimary", codiAnagLovPrimary)
					.getResultList();
		}
		catch(Exception e){}
		
		return list;
	}
}