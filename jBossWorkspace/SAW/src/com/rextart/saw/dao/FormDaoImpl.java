package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.ViwAnagAntenneTmaDiplexer;
import com.rextart.saw.entity.ViwAnagLovCaratt;
import com.rextart.saw.entity.ViwElemCarattWOrder;
import com.rextart.saw.entity.ViwElementiCaratteristiche;
import com.rextart.saw.entity.ViwElementiOggetti;
import com.rextart.saw.entity.ViwTipiOggetti;

@Repository
public class FormDaoImpl implements FormDao
{
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwTipiOggetti> getAllTipiOggetti() {
		List<ViwTipiOggetti> listTipologieOggetti = new ArrayList<ViwTipiOggetti>();

		try
		{
			listTipologieOggetti = em.createNamedQuery("ViwTipiOggetti.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listTipologieOggetti;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwTipiOggetti> getTipiByOggetto(Integer codiOggetto) {
		List<ViwTipiOggetti> listTipologieOggetti = new ArrayList<ViwTipiOggetti>();

		try
		{
			listTipologieOggetti = em.createNamedQuery("ViwTipiOggetti.findByOggetto").setParameter("codiOggetto", codiOggetto).getResultList();
		}
		catch(Exception e) {
		}

		return listTipologieOggetti;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElementiOggetti> getAllElementiOggetti() {
		List<ViwElementiOggetti> listElementiOggetti = new ArrayList<ViwElementiOggetti>();

		try
		{
			listElementiOggetti = em.createNamedQuery("ViwElementiOggetti.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listElementiOggetti;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElementiOggetti> getElementiByTipoOggetto(Integer codiTipoOggetto) {
		List<ViwElementiOggetti> listElementiOggetti = new ArrayList<ViwElementiOggetti>();

		try
		{
			listElementiOggetti = em.createNamedQuery("ViwElementiOggetti.findByTipo").setParameter("codiTipoOggetto", codiTipoOggetto).getResultList();
		}
		catch(Exception e) {
		}

		return listElementiOggetti;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElementiCaratteristiche> getAllElementiCaratteristiche() {
		List<ViwElementiCaratteristiche> listElementiCaratteristiche = new ArrayList<ViwElementiCaratteristiche>();

		try
		{
			listElementiCaratteristiche = em.createNamedQuery("ViwElementiCaratteristiche.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listElementiCaratteristiche;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElementiCaratteristiche> getCaratteristicheByElemento(Integer codiElemento) {
		List<ViwElementiCaratteristiche> listElementiCaratteristiche = new ArrayList<ViwElementiCaratteristiche>();

		try
		{
			listElementiCaratteristiche = em.createNamedQuery("ViwElementiCaratteristiche.findByElemento").setParameter("codiElemento", codiElemento).getResultList();
		}
		catch(Exception e) {
		}

		return listElementiCaratteristiche;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElemCarattWOrder> getAllElementiCaratteristicheWithOrder() {
		List<ViwElemCarattWOrder> listElementiCaratteristiche = new ArrayList<ViwElemCarattWOrder>();

		try
		{
			listElementiCaratteristiche = em.createNamedQuery("ViwElemCarattWOrder.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listElementiCaratteristiche;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwElemCarattWOrder> getCaratteristicheWithOrderByElemento(Integer codiElemento) {
		List<ViwElemCarattWOrder> listElementiCaratteristiche = new ArrayList<ViwElemCarattWOrder>();

		try
		{
			listElementiCaratteristiche = em.createNamedQuery("ViwElemCarattWOrder.findByElemento").setParameter("codiElemento", codiElemento).getResultList();
		}
		catch(Exception e) {
		}

		return listElementiCaratteristiche;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagLovCaratt> getAllAnagLovCaratt() {
		List<ViwAnagLovCaratt> listAnagLov = new ArrayList<ViwAnagLovCaratt>();

		try
		{
			listAnagLov = em.createNamedQuery("ViwAnagLovCaratt.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listAnagLov;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagLovCaratt> getAnagLovByCaratt(Integer codiCaratteristica) {
		List<ViwAnagLovCaratt> listAnagLov = new ArrayList<ViwAnagLovCaratt>();

		try
		{
			listAnagLov = em.createNamedQuery("ViwAnagLovCaratt.findByCarattId").setParameter("codiCaratteristica", codiCaratteristica).getResultList();
		}
		catch(Exception e) {
		}

		return listAnagLov;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagLovCaratt> getAnagLovByCaratt(Integer codiCaratteristica, String descLabel)
	{
		List<ViwAnagLovCaratt> listAnagLov = new ArrayList<ViwAnagLovCaratt>();

		try
		{
			listAnagLov = em.createNamedQuery("ViwAnagLovCaratt.findByCaratt").setParameter("codiCaratteristica", codiCaratteristica).setParameter("descColumn", descLabel).getResultList();
		}
		catch(Exception e) {
		}

		return listAnagLov;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagLovCaratt> getAllAnagLovCarattCatalogo()
	{
		List<ViwAnagLovCaratt> listAnagLovCat = new ArrayList<ViwAnagLovCaratt>();

		try
		{
			listAnagLovCat = em.createNamedQuery("ViwAnagLovCaratt.findAllCatalogo").getResultList();
		}
		catch(Exception e) {
		}

		return listAnagLovCat;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagAntenneTmaDiplexer> getAllAnagAntenneTmaDiplexer() {
		
		List<ViwAnagAntenneTmaDiplexer> listAnagAntenneTmaDiplexer = new ArrayList<ViwAnagAntenneTmaDiplexer>();

		try
		{
			listAnagAntenneTmaDiplexer = em.createNamedQuery("ViwAnagAntenneTmaDiplexer.findAll").getResultList();
		}
		catch(Exception e) {
		}

		return listAnagAntenneTmaDiplexer;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagAntenneTmaDiplexer> getAnagAntenneTmaDiplexerByCodiCatalogo(Integer codiCatalogo) {
		List<ViwAnagAntenneTmaDiplexer> listAnagAntenneTmaDiplexer = new ArrayList<ViwAnagAntenneTmaDiplexer>();

		try
		{
			listAnagAntenneTmaDiplexer = em.createNamedQuery("ViwAnagAntenneTmaDiplexer.findByCodiCatalogo").setParameter("codiCatalogo", codiCatalogo).getResultList();
		}
		catch(Exception e) {
		}

		return listAnagAntenneTmaDiplexer;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDistinctDescMarcaByCodiCatalogo(Integer codiCatalogo) {
		List<String> listAnagAntenneTmaDiplexer = new ArrayList<String>();

		try
		{
			listAnagAntenneTmaDiplexer = em.createNamedQuery("ViwAnagAntenneTmaDiplexer.findDistinctDescMarcaByCodiCatalogo").setParameter("codiCatalogo", codiCatalogo).getResultList();
		}
		catch(Exception e) {
		}

		return listAnagAntenneTmaDiplexer;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwAnagAntenneTmaDiplexer> getModelloByDescMarca(Integer codiCatalogo, String descMarca) {
		List<ViwAnagAntenneTmaDiplexer> listAnagAntenneTmaDiplexer = new ArrayList<ViwAnagAntenneTmaDiplexer>();

		try
		{
			listAnagAntenneTmaDiplexer = em.createNamedQuery("ViwAnagAntenneTmaDiplexer.findByDescMarca").setParameter("codiCatalogo", codiCatalogo).setParameter("descMarca", descMarca).getResultList();
		}
		catch(Exception e) {
		}

		return listAnagAntenneTmaDiplexer;
	}


}
