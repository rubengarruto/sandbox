package com.rextart.saw.dao;

import java.util.List;

import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwSbloccoSezioni;

public interface SurveyDao {

	public List<TabSurvey> getListSurveyBySessione(List<Integer> listCodiSessione);

	public List<TabSurveyDettaglio> getListDettagliBySurvey(List<Integer> listCodiSurvey);

	public List<TabNoteCaratteristiche> getListNoteCaratt(List<Integer> listCodiSurvey);

	public List<TabRisultatoSurvey> getListRisultatiSurvey(List<Integer> listCodiSurvey);

	public boolean isExistSurveyEsito(Integer codiSurvey);

	public void deleteSurveyBySessione(Integer codiSessione);

	public void saveSurveyForD1(TabRisultatoSurvey tabRisultatoSurvey,Integer codiSessione, Integer codiTipoOggetto);

	public List<TabSurvey> getListSurveyBySessioneByOggetto(int codiSessione,int codiOggetto);

	public List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessione(Integer codiSessione);

	public void removeRisultatoSurveyByCodiSurvey(Integer codiSurvey);

	public void removeRiferimentoFirmaByCodiSurvey(Integer codiSurvey);

	public List<TabRiferimentiFirma> getListRiferimentiFirmaByCodiSurvey(Integer codiSurvey);

	public List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessioneByCodiOggetto(Integer codiSessione, Integer codiOggetto);

	public void removeRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto);	

	public List<TabRelSessioneStatiVerbale> getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto);

	public void saveRelSessioneStatiVerbale(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale);

	public List<TabRiferimentiFirma> getListRiferimentiFirmaByCodiSurvey(List<Integer> listCodiSurvey);	

	public List<TabRisultatoSurvey> getListRisultatoByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto);

	public List<TabNote> getListNoteTablet(List<Integer> listNoteTablet);

	public TabSurveyDettaglio getSurveyDettaglioByFormKey(Integer codiSurvey, Integer codiElemento, Integer codiCaratteristica, String descLabel, Integer numeCella, Integer numeVia, Integer numeLivelloE);

	public TabSurveyDettaglio getValorizedSurveyDettaglioByCodiCaratteristica(Integer codiSurvey, Integer codiCaratteristica);
}	
