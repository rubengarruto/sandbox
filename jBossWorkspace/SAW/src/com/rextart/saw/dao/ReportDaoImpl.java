package com.rextart.saw.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.ViwApparatoEricsson;
import com.rextart.saw.entity.ViwApparatoHuawei;
import com.rextart.saw.entity.ViwApparatoNokia;
import com.rextart.saw.entity.ViwConfGenRadC;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwMisureDiPimRadD;
import com.rextart.saw.entity.ViwMisureGenRadB;
import com.rextart.saw.entity.ViwProveFunzEseEricsson;
import com.rextart.saw.entity.ViwProveFunzSerEricsson;
import com.rextart.saw.entity.ViwRadiantiE;
import com.rextart.saw.entity.ViwRiferimentiFirma;
import com.rextart.saw.entity.ViwVerFunzAllarmi;
import com.rextart.saw.entity.ViwVerFunzAllarmiEricsson;
import com.rextart.saw.entity.ViwVerFunzMisureEricsson;
import com.rextart.saw.entity.ViwVerGenRadA;
import com.rextart.saw.entity.ViwVerInstWipmEricsson;
import com.rextart.saw.entity.ViwVerificheInstEricsson;

@Repository
public class ReportDaoImpl implements ReportDao{

	@PersistenceContext
	private EntityManager em;
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerificheInstEricsson> getListVerificheInstEricsson(Integer sessione) {
	List<ViwVerificheInstEricsson> list = new ArrayList<ViwVerificheInstEricsson>();
		try {
			list= (List<ViwVerificheInstEricsson>) em.createNamedQuery("ViwVerificheInstEricsson.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListEsitoVeriInstEricsson(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVeriInstEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzAllarmiEricsson> getListVerFunzAllarmiEricsson(Integer sessione) {
		List<ViwVerFunzAllarmiEricsson> list=new ArrayList<ViwVerFunzAllarmiEricsson>();
		try {
			list= (List<ViwVerFunzAllarmiEricsson>) em.createNamedQuery("ViwVerFunzAllarmiEricsson.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureStrumentiEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureStrumentiByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureOffsetEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOffsetByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureCanaliSettoreEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureCanaliSettoreByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureCanaliSettoreCellaEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureCanaliSettoreCellaByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureVerTestReportEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureVerTestReportEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureTratteInFibraEricsson(Integer sessione) {
		
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureTratteInFibraEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureOutPowValRifEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOutPowValRifByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureGsmOutPow(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureGsmOutPowByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureUmtsOutPow(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureUmtsOutPowByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureLteOutPow(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureLteOutPowByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureFrequencyErrorEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureFrequencyErrorSettoreCellaEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreCellaByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVerFunzMisureFrequencyErrorSettoreBranchEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreBranchByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureCPICHValRifEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureCPICHValRifEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureCPICHSettoreCellaEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureCPICHSettoreCellaEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureOCCUPIEDValRifEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDValRifEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureOCCUPIEDSettoreCellaEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list=new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDSettoreCellaEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureOCCUPIEDValRifLTEEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDValRifLTEEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureOCCUPIEDsettoriBranch(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDsettoriBranchEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureEVMValRifEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureEVMValRifEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureEVMSettoreCellaEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureEVMSettoreCellaEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureEVMValRifLTEEricsson(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureEVMValRifLTEEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListVeriFunzMisureEVMsettoriBranch(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureEVMsettoriBranchEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListMisureVerTMA(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureVerTMAEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzMisureEricsson> getListMisureVerRET(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list=new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureVerRETEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ViwVerFunzMisureEricsson> getListMisureVerGestioneRET(Integer sessione) {
		List<ViwVerFunzMisureEricsson> list= new ArrayList<ViwVerFunzMisureEricsson>();
		try {
			list= (List<ViwVerFunzMisureEricsson>) em.createNamedQuery("ViwVerFunzMisureEricsson.findFunzMisureVerGestioneRETEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListEsitoVerFunzionaliApparati(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerFunzionaliApparati").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwProveFunzEseEricsson> getListProveFunzEseEricssonBean(Integer sessione) {
		List<ViwProveFunzEseEricsson> list = new ArrayList<ViwProveFunzEseEricsson>();
		try {
			list= (List<ViwProveFunzEseEricsson>) em.createNamedQuery("ViwProveFunzEseEricsson.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListEsitoProveFunzEseApparati(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoProveFunzEseApparati").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	public List<ViwProveFunzSerEricsson> getListProveFunzSerEricsson(Integer sessione){
		List<ViwProveFunzSerEricsson> list= new ArrayList<ViwProveFunzSerEricsson>();
		try {
			list= (List<ViwProveFunzSerEricsson>) em.createNamedQuery("ViwProveFunzSerEricsson.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListEsitoProveFunzSerEricsson(Integer sessione){
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoProveFunzSerEricsson").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ViwVerInstWipmEricsson> getListVerificheInstWIPMEricsson(Integer sessione) {
		List<ViwVerInstWipmEricsson> list = new ArrayList<ViwVerInstWipmEricsson>();
			try {
				list= (List<ViwVerInstWipmEricsson>) em.createNamedQuery("ViwVerInstWipmEricsson.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list;
		}
	
	@SuppressWarnings("unchecked")
	public List<ViwEsitoSurvey> getListEsitoVerificheInstWIPM(Integer sessione){
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerificheInstWIPM").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerGenRadA> getListElementiCostituente(Integer sessione) {
		List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findElementiCostituente").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerGenRadA> getListVerificheGenRadiantiA(Integer sessione) {
	List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	
	@Override
	public List<ViwVerGenRadA> getListElementiGenerali(Integer sessione) {
	List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findElementiGenerali").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerGenRadA> getListElementiTriploValoreGenerali(Integer sessione) {
	List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findElementiGeneraliValAtteso").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerGenRadA> getListElementiChainingRETGenerali(Integer sessione) {
	List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findElementiChainingRET").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListVerbaleRadiandiA(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerbaleRadiantiA").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListMisureGenRadiantiB(Integer sessione) {
	List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListCondivisioneCalataRadiantiB(Integer sessione) {
	List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findCondivisioneCalata").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListMisureGeneraliRadiantiB(Integer sessione) {
		List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findMisureGenerali").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListVerbaleRadiandiB(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerbaleRadiantiB").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListMisureTipoCavoRadiantiB(Integer sessione) {
		List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findMisureTipoCavo").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwConfGenRadC> getListConRadiantiC(Integer sessione){
	List<ViwConfGenRadC> list = new ArrayList<ViwConfGenRadC>();
		try {
			list= (List<ViwConfGenRadC>) em.createNamedQuery("ViwConfGenRadC.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwConfGenRadC> getListConfCelleRadiantiC(Integer sessione) {
		List<ViwConfGenRadC> list = new ArrayList<ViwConfGenRadC>();
		try {
			list= (List<ViwConfGenRadC>) em.createNamedQuery("ViwConfGenRadC.findConfCelle").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwConfGenRadC> getListAntenneInstallateRadiantiC(Integer sessione) {
		List<ViwConfGenRadC> list = new ArrayList<ViwConfGenRadC>();
		try {
			list= (List<ViwConfGenRadC>) em.createNamedQuery("ViwConfGenRadC.findAntenneInstallate").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwConfGenRadC> getListDettaglioTecnologieRadiantiC(Integer sessione) {
		List<ViwConfGenRadC> list = new ArrayList<ViwConfGenRadC>();
		try {
			list= (List<ViwConfGenRadC>) em.createNamedQuery("ViwConfGenRadC.findDettaglioTecnologie").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListVerbaleRadiandiC(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerbaleRadiantiC").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListConRadiantiD(Integer sessione){
	List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListMisuraPim(Integer sessione) {
		List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findMisuraPim").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListMisuraSistemaPim(Integer sessione) {
		List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findMisuraSistemaPim").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListMisuraDtpValoreDiPim(Integer sessione) {
		List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findDtpValoreDiPim").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListMisuraDtpDistanza(Integer sessione) {
		List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findDtpDistanza").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListVerbaleRadiandiD(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerbaleRadiantiD").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  List<ViwRiferimentiFirma> getRiferimentoByCodiSurvey(Integer codiSurvey){
		List<ViwRiferimentiFirma> list = new ArrayList<ViwRiferimentiFirma>();
		try {
			list= (List<ViwRiferimentiFirma>) em.createNamedQuery("ViwRiferimentiFirma.findByCodiSurvey").setParameter("codiSurvey", codiSurvey).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
		
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRadiantiE> getListConRadiantiE(Integer sessione){
	List<ViwRadiantiE> list = new ArrayList<ViwRadiantiE>();
		try {
			list= (List<ViwRadiantiE>) em.createNamedQuery("ViwRadiantiE.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRadiantiE> getListCelleRadiantiE(Integer sessione) {
		List<ViwRadiantiE> list = new ArrayList<ViwRadiantiE>();
		try {
			list= (List<ViwRadiantiE>) em.createNamedQuery("ViwRadiantiE.findCelleRadiantiE").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwRadiantiE> getListAntennaRadiantiE(Integer sessione) {
		List<ViwRadiantiE> list = new ArrayList<ViwRadiantiE>();
		try {
			list= (List<ViwRadiantiE>) em.createNamedQuery("ViwRadiantiE.findAntennaRadiantiE").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListVerbaleRadiandiE(Integer sessione) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoVerbaleRadiantiE").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoNokia> getListApparatoNokiaBySessioneByTipoOggetto(Integer sessione, Integer codiTipoOggetto) {
		List<ViwApparatoNokia> list = new ArrayList<ViwApparatoNokia>();
		try {
			list= (List<ViwApparatoNokia>) em.createNamedQuery("ViwApparatoNokia.findByCodiSessioneAndCodiTipoOggetto").setParameter("codiSessione", sessione).setParameter("codiTipoOggetto",codiTipoOggetto).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoNokia> getListApparatoNokia(Integer sessione) {
		List<ViwApparatoNokia> list = new ArrayList<ViwApparatoNokia>();
		try {
			list= (List<ViwApparatoNokia>) em.createNamedQuery("ViwApparatoNokia.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoHuawei> getListApparatoHuawei(Integer sessione) {
		List<ViwApparatoHuawei> list = new ArrayList<ViwApparatoHuawei>();
		try {
			list= (List<ViwApparatoHuawei>) em.createNamedQuery("ViwApparatoHuawei.findByCodiSessione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoHuawei> getListApparatoHuaweiBySessioneByTipoOggetto(Integer sessione, Integer codiTipoOggetto) {
		List<ViwApparatoHuawei> list = new ArrayList<ViwApparatoHuawei>();
		try {
			list= (List<ViwApparatoHuawei>) em.createNamedQuery("ViwApparatoHuawei.findByCodiSessioneAndCodiTipoOggetto").setParameter("codiSessione", sessione).setParameter("codiTipoOggetto",codiTipoOggetto).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwEsitoSurvey> getListEsitoByTipoOggetto(Integer codiSessione,Integer codiTipoOggetto) {
		List<ViwEsitoSurvey> list = new ArrayList<ViwEsitoSurvey>();
		try {
			list= (List<ViwEsitoSurvey>) em.createNamedQuery("ViwEsitoSurvey.findEsitoByTipoOggetto").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoNokia> getListApparatoNokiaBySessioneByTipoOggettoElemento(Integer codiSessione, int codiTipoOggetto, int codiElemento) {
			List<ViwApparatoNokia> list = new ArrayList<ViwApparatoNokia>();
			try {
				list= (List<ViwApparatoNokia>) em.createNamedQuery("ViwApparatoNokia.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerFunzAllarmi> getListViwVerFunzBySessioneByTipoOggettoElemento(Integer codiSessione,int codiTipoOggetto,int codiElemento) {
			List<ViwVerFunzAllarmi> list = new ArrayList<ViwVerFunzAllarmi>();
			try {
				list= (List<ViwVerFunzAllarmi>) em.createNamedQuery("ViwVerFunzAllarmi.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoHuawei> getListApparatoHuaweiBySessioneByTipoOggettoElemento(Integer codiSessione,int codiTipoOggetto,int codiElemento) {
		List<ViwApparatoHuawei> list = new ArrayList<ViwApparatoHuawei>();
		try {
			list= (List<ViwApparatoHuawei>) em.createNamedQuery("ViwApparatoHuawei.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwVerGenRadA> getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadA(Integer codiSessione, int codiTipoOggetto, int codiElemento) {
		List<ViwVerGenRadA> list = new ArrayList<ViwVerGenRadA>();
		try {
			list= (List<ViwVerGenRadA>) em.createNamedQuery("ViwVerGenRadA.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadB(Integer codiSessione, int codiTipoOggetto, int codiElemento) {
		List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureDiPimRadD> getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadD(Integer codiSessione, int codiTipoOggetto, int codiElemento) {
		List<ViwMisureDiPimRadD> list = new ArrayList<ViwMisureDiPimRadD>();
		try {
			list= (List<ViwMisureDiPimRadD>) em.createNamedQuery("ViwMisureDiPimRadD.findBySessioneTipoOggettoCodiElemento").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).setParameter("codiElemento",codiElemento).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<ViwApparatoEricsson> getListApparatoEricssonBySessioneByTipoOggetto(Integer codiSessione, int codiTipoOggetto) {
		List<ViwApparatoEricsson> list = new ArrayList<ViwApparatoEricsson>();
		try {
			list= (List<ViwApparatoEricsson>) em.createNamedQuery("ViwApparatoEricsson.findByCodiSessioneAndCodiTipoOggetto").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto",codiTipoOggetto).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TabSurvey getSurveyBySessioneAndTipoOggetto(Integer codiSessione, Integer codiTipoOggetto){
		TabSurvey tabSurvey = new TabSurvey();
		try{
			tabSurvey = (TabSurvey)em.createNamedQuery("TabSurvey.findByCodiSessioneCodiTipoOggetto").setParameter("codiSessione", codiSessione).setParameter("codiTipoOggetto", codiTipoOggetto).getSingleResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tabSurvey;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ViwMisureGenRadB> getListMisureCalibrazioneRadiantiB(Integer sessione) {
		List<ViwMisureGenRadB> list = new ArrayList<ViwMisureGenRadB>();
		try {
			list= (List<ViwMisureGenRadB>) em.createNamedQuery("ViwMisureGenRadB.findMisureCalibrazione").setParameter("codiSessione", sessione).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	
	
}
