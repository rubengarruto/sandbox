package com.rextart.saw.rs.utility;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.openkm.sdk4j.bean.Document;
import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabAnagLov;
import com.rextart.saw.entity.TabAnagRisultati;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabApk;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCaratteristiche;
import com.rextart.saw.entity.TabElementi;
import com.rextart.saw.entity.TabErrorDevice;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelCaratteristicheLov;
import com.rextart.saw.entity.TabRelElementiCaratt;
import com.rextart.saw.entity.TabRelOggettiElementi;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelServiziOggetti;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSchedaDyn;
import com.rextart.saw.entity.TabServizi;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.entity.TabTipologieOggetto;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.bean.request.ApkBean;
import com.rextart.saw.rs.bean.request.ErrorDeviceBean;
import com.rextart.saw.rs.bean.request.MediaBean;
import com.rextart.saw.rs.bean.request.NoteBean;
import com.rextart.saw.rs.bean.request.NoteCaratteristicheBean;
import com.rextart.saw.rs.bean.request.RelRitardiSessioniBean;
import com.rextart.saw.rs.bean.request.RiferimentiFirmaBean;
import com.rextart.saw.rs.bean.request.RisultatoSurveyBean;
import com.rextart.saw.rs.bean.request.SurveyBean;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;
import com.rextart.saw.rs.bean.request.SyncAnagBean;
import com.rextart.saw.rs.bean.response.AnagAntenneBean;
import com.rextart.saw.rs.bean.response.AnagDiplexerBean;
import com.rextart.saw.rs.bean.response.AnagLovBean;
import com.rextart.saw.rs.bean.response.AnagRisultatiBean;
import com.rextart.saw.rs.bean.response.AnagRitardiBean;
import com.rextart.saw.rs.bean.response.AnagStatiSessioneBean;
import com.rextart.saw.rs.bean.response.AnagTmaBean;
import com.rextart.saw.rs.bean.response.BandeBean;
import com.rextart.saw.rs.bean.response.CaratteristicheBean;
import com.rextart.saw.rs.bean.response.ElementiBean;
import com.rextart.saw.rs.bean.response.OggettiBean;
import com.rextart.saw.rs.bean.response.RelCaratteristicheLovBean;
import com.rextart.saw.rs.bean.response.RelElementiCarattBean;
import com.rextart.saw.rs.bean.response.RelOggettiElementiBean;
import com.rextart.saw.rs.bean.response.RelOggettiTipoBean;
import com.rextart.saw.rs.bean.response.RelServiziOggettiBean;
import com.rextart.saw.rs.bean.response.RepositorySchedaRischiBean;
import com.rextart.saw.rs.bean.response.SchedaDynBean;
import com.rextart.saw.rs.bean.response.ServiziBean;
import com.rextart.saw.rs.bean.response.SessioniBean;
import com.rextart.saw.rs.bean.response.SistemiBean;
import com.rextart.saw.rs.bean.response.StrutturaBean;
import com.rextart.saw.rs.bean.response.TipologieOggettoBean;
import com.rextart.saw.rs.bean.response.UserBean;
import com.rextart.saw.rs.bean.response.VerbaleBean;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.MethodUtilities;
import com.rextart.saw.ws.bean.CollaudoBean;


public class DataConverter {
	
	public static UserBean getUserBeanByViwUtenti(ViwUtenti viwUser,TabUtente user){
		UserBean userBean = new UserBean();
		userBean.setCodiUtente(user.getCodiUtente());
		userBean.setDescCgn(user.getDescCgn());
		userBean.setDescEmail(user.getDescEmail());
		userBean.setDescImei(user.getDescImei());
		userBean.setDescNome(user.getDescNome());
		userBean.setDescPwd(user.getDescPwd());
		userBean.setDescUser(user.getDescUser());
		userBean.setFlgAbilitato(user.isFlgAbilitato());
		userBean.setFlgCessSosp(user.isFlgCessSosp());
		userBean.setFlgIsAdmin(user.isFlgIsAdmin());
		userBean.setFlgReceiveEmail(user.isFlgReceiveEmail());
		userBean.setFlgReset(user.isFlgReset());
		userBean.setFlgTablet(user.isFlgTablet());
		userBean.setDescNumeroSim(user.getDescTelefono());
		userBean.setDescImpresa(viwUser.getDescImpresa());
		userBean.setFlgImpresa(viwUser.getFlgImpresa()?1:0);
		userBean.setCodiVendor(viwUser.getCodiVendor());
		
		if(user.getTimeCeased()!=null)
			userBean.setTimeCeased(user.getTimeCeased().getTime());
		userBean.setTimeCreazione(user.getTimeCreazione().getTime());
		if(user.getTimeDisable()!=null)
			userBean.setTimeDisable(user.getTimeDisable().getTime());
		if(user.getTimeEnable()!=null)
			userBean.setTimeEnable(user.getTimeEnable().getTime());
		if(user.getTimeSuspended()!=null)
			userBean.setTimeSuspended(user.getTimeSuspended().getTime());
		if(user.getTimeSuspension()!=null)
			userBean.setTimeSuspension(user.getTimeSuspension().getTime());
		userBean.setCodiAreaComp(user.getCodiAreaComp());
		userBean.setCodiRole(user.getCodiRole());
		userBean.setCodiStruttura(user.getCodiStruttura());
		
		return userBean;
	}
	
	public static List<AnagLovBean> getAllAnagLovBean(List<TabAnagLov> arr){
		List<AnagLovBean> list = new ArrayList<AnagLovBean>();
        for (TabAnagLov entity : arr) {
            AnagLovBean bean = new AnagLovBean();
            bean.setCodiAnagLov(entity.getCodiAnagLov());
            bean.setDescLabel(entity.getDescLabel());
            bean.setFlagAltro(entity.getFlagAltro()==true ? 1 : 0);
            list.add(bean);
        }
        return list;
    }
	
	public static List<StrutturaBean> getAllTabStruttura(List<TabStruttura> arr){
		List<StrutturaBean> list = new ArrayList<StrutturaBean>();
        for (TabStruttura entity : arr) {
            StrutturaBean bean = new StrutturaBean();
            bean.setCodiStruttura(entity.getCodiStruttura());
            bean.setDescStruttura(entity.getDescStruttura());
            list.add(bean);
        }
        return list;
    }
	
	public static List<CaratteristicheBean> getAllCaratteristicheBean(List<TabCaratteristiche> arr){
		List<CaratteristicheBean> list = new ArrayList<CaratteristicheBean>();
        for (TabCaratteristiche entity : arr) {
        	CaratteristicheBean bean = new CaratteristicheBean();
        	bean.setCodiCaratteristica(entity.getCodiCaratteristica());
        	bean.setDescCaratteristica(entity.getDescCaratteristica());
        	bean.setMemoHelp(entity.getMemoHelp());
        	bean.setDescComponente(entity.getDescComponente());
        	bean.setDescLabelCarat1(entity.getDescLabelCarat1());
        	bean.setDescLabelCarat2(entity.getDescLabelCarat2());
        	bean.setDescLabelCarat3(entity.getDescLabelCarat3());
        	bean.setDescLabelCarat4(entity.getDescLabelCarat4());
        	bean.setDescLabelCarat5(entity.getDescLabelCarat5());
        	bean.setFlagLov1(entity.isFlagLov1() == true ? 1 : 0);
        	bean.setFlagLov2(entity.isFlagLov2() == true ? 1 : 0);
        	bean.setFlagLov3(entity.isFlagLov3() == true ? 1 : 0);
        	bean.setFlagLov4(entity.isFlagLov4() == true ? 1 : 0);
        	bean.setFlagLov5(entity.isFlagLov5() == true ? 1 : 0);
        	bean.setFlagObbligatorio(entity.isFlagObbligatorio() == true ? 1 : 0);
        	
        	bean.setNumeSettore(entity.getNumeSettore());
        	bean.setNumeVia(entity.getNumeVia());
        	bean.setNumeBranch(entity.getNumeBranch());
        	bean.setNumeCella(entity.getNumeCella());
        	bean.setNumeE(entity.getNumeE());
        	bean.setFlagNote(entity.isFlagNote() == true ? 1 : 0);
        	list.add(bean);
        }
        return list;
    }
	
	public static List<ElementiBean> getAllElementiBean(List<TabElementi> arr) {
		List<ElementiBean> list = new ArrayList<ElementiBean>();
        for (TabElementi entity : arr) {
        	ElementiBean bean = new ElementiBean();
        	bean.setCodiElemento(entity.getCodiElemento());
        	bean.setDescElemento(entity.getDescElemento());
        	bean.setDescNote(entity.getDescNote());
            list.add(bean);
        }
        return list;
	 }
	
	public static List<OggettiBean> getAllOggettiBean(List<TabOggetti> arr)  {
		List<OggettiBean> list = new ArrayList<OggettiBean>();
        for (TabOggetti entity  : arr) {
        	OggettiBean bean = new OggettiBean();
            bean.setCodiOggetto(entity.getCodiOggetto());
            bean.setDescOggetto(entity.getDescOggetto());
            bean.setDescSigla(entity.getDescSigla());
            bean.setDescNote(entity.getDescNote());
            bean.setCodiOggettoPadre(entity.getCodiOggettoPadre());
            bean.setDescGerarchia(entity.getDescGerarchia());
            list.add(bean);
        }
        return list;
	 }
	 

	 
	public static List<RelElementiCarattBean> getAllRelElementiCarattBean(List<TabRelElementiCaratt> arr){
		 List<RelElementiCarattBean> list = new ArrayList<RelElementiCarattBean>();
	        for (TabRelElementiCaratt tab : arr) {
	        	RelElementiCarattBean bean = new RelElementiCarattBean();
	        	bean.setCodiRelElemCaratt(tab.getCodiCaratteristica());
	        	bean.setCodiElemento(tab.getCodiElemento());
	        	bean.setCodiCaratteristica(tab.getCodiCaratteristica());
	        	bean.setFlagPrimaCaratteristica(tab.getFlagPrimaCaratteristica()==true ? 1 : 0);
	        	bean.setFlagUltimaCaratteristica(tab.isFlagUltimaCaratteristica()==true ? 1 : 0);
	        	list.add(bean);
	        }
	        return list;
	 }
	 
	public static List<TipologieOggettoBean> getAllAnagTipologiaOggettoBean(List<TabTipologieOggetto> arr){
		 List<TipologieOggettoBean> list = new ArrayList<TipologieOggettoBean>();
        for (TabTipologieOggetto tab : arr) {
        	TipologieOggettoBean bean = new TipologieOggettoBean();
        	bean.setCodiTipoOggetto(tab.getCodiTipoOggetto());
        	bean.setDescTipoOggetto(tab.getDescTipoOggetto());
        	bean.setDescNote(tab.getDescNote());
        	bean.setDescLabelFilterTipoOggetto(tab.getDescLabelFilterTipoOggetto());
        	bean.setFlgDitta(tab.getFlgDitta());
        	list.add(bean);
        }
        return list;
	  }
	 
	 public static List<RelOggettiElementiBean> getAllOggettiElementiBean(List<TabRelOggettiElementi> arr){
		 List<RelOggettiElementiBean> list = new ArrayList<RelOggettiElementiBean>();
	        for (TabRelOggettiElementi tab : arr) {
	        	RelOggettiElementiBean bean = new RelOggettiElementiBean();
	        	bean.setCodiRelOggElem(tab.getCodiRelOggElem());
	        	bean.setCodiOggetto(tab.getCodiOggetto());
	        	bean.setCodiElemento(tab.getCodiElemento());
	        	bean.setCodiTipoOggetto(tab.getCodiTipoOggetto());
	        	bean.setNumeOrder(tab.getNumeOrder());
	            list.add(bean);
	        }
	        return list;
	   }
	 
	 public static List<ServiziBean> getAllServiziBean(List<TabServizi> arr){
		 List<ServiziBean> list = new ArrayList<ServiziBean>();
	        for (TabServizi tab : arr) {
	        	ServiziBean bean = new ServiziBean();
	        	bean.setCodiServizio(tab.getCodiServizio());
	        	bean.setDescServizio(tab.getDescServizio());
	        	bean.setDescNote(tab.getDescNote());
	        	bean.setDescSigla(tab.getDescSigla());
	            list.add(bean);
	        }
	        return list;
	 }
	 
	 public static List<RelCaratteristicheLovBean> getAllCaratteristicheLovBean(List<TabRelCaratteristicheLov> arr){
		 List<RelCaratteristicheLovBean> list = new ArrayList<RelCaratteristicheLovBean>();
	        for (TabRelCaratteristicheLov entity : arr) {
	        	RelCaratteristicheLovBean bean = new RelCaratteristicheLovBean();
	        	bean.setCodiRelCarattLov(entity.getCodiRelCarattLov());
            	bean.setCodiAnagLov(entity.getCodiAnaglov());
            	bean.setCodiCaratteristica(entity.getCodiCaratteristica());
            	bean.setCodiNextCaratt(entity.getCodiNextCaratt());
            	bean.setCodiCatalogo(entity.getCodiCatalogo());
            	bean.setCodiDescCaratt(entity.getCodiDescCaratt());
            	bean.setCodiPrecCaratt(entity.getCodiPrecCaratt());
            	bean.setDescLabel(entity.getDescLabel());
	            list.add(bean);
	        }
	        return list;
	 }
	 
	 public static List<RelServiziOggettiBean> getAllRelServiziOggettiBean(List<TabRelServiziOggetti> arr){
		 List<RelServiziOggettiBean> list = new ArrayList<RelServiziOggettiBean>();
	        for (TabRelServiziOggetti tab : arr) {
	        	RelServiziOggettiBean bean = new RelServiziOggettiBean();
	        	bean.setCodiRelServOggetto(tab.getCodiRelServOggetto());
	        	bean.setCodiOggetto(tab.getCodiOggetto());
	        	bean.setCodiServizio(tab.getCodiServizio());
	            list.add(bean);
	        }
	        return list;
	 }
	 
	 public static List<RelOggettiTipoBean> getAllRelOggettiTipoBean(List<TabRelOggettiTipo> arr){
		 List<RelOggettiTipoBean> list = new ArrayList<RelOggettiTipoBean>();
	        for (TabRelOggettiTipo tab : arr) {
	        	RelOggettiTipoBean bean = new RelOggettiTipoBean();
	        	bean.setCodiOggettoTipo(tab.getCodiOggettoTipo());
	        	bean.setCodiOggetto(tab.getCodiOggetto());
	        	bean.setCodiTipoOggetto(tab.getCodiTipoOggetto());
	            list.add(bean);
	        }
	        return list;
	 }
	 
	 public static List<SessioniBean> getAllSessioniBean(List<ViwSessioni> arr){
		List<SessioniBean> list = new ArrayList<SessioniBean>();
        for (ViwSessioni tabSessioni : arr) {
        	SessioniBean bean = new SessioniBean();
        	bean.setCodiSessione(tabSessioni.getCodiSessione());
            bean.setTimeAcquisizione(tabSessioni.getTimeAcquisizione().getTime());
            bean.setCodiAreaComp(tabSessioni.getCodiAreaComp());
            bean.setDescRegione(tabSessioni.getDescRegione());
            bean.setDescProvincia(tabSessioni.getDescProvincia());
            bean.setDescComune(tabSessioni.getDescComune());
            bean.setCodiUnitaTerr(tabSessioni.getCodiUnitaTerr());
            bean.setDescNomeSito(tabSessioni.getDescNomeSito());
            bean.setDescCodice(tabSessioni.getDescCodice());
            bean.setCodiTipoImpianto(tabSessioni.getCodiTipoImpianto());
            bean.setCodiSistema(tabSessioni.getCodiSistema());
            bean.setCodiBanda(tabSessioni.getCodiBanda());
            bean.setDescOperTim(tabSessioni.getDescOperTim());
            bean.setDescProgettoRadio(tabSessioni.getDescProgettoRadio());
            bean.setCodiFinalitaColl(tabSessioni.getCodiFinalitaColl());
            bean.setCodiDettaglioColl(tabSessioni.getCodiDettaglioColl());
            bean.setFlgAllegatoA(tabSessioni.getFlgAllegatoA() ? 1 : 0);
            bean.setFlgAllegatoB(tabSessioni.getFlgAllegatoB() ? 1 : 0);
            bean.setFlgAllegatoC(tabSessioni.getFlgAllegatoC() ? 1 : 0);
            bean.setFlgAllegatoD(tabSessioni.getFlgAllegatoD() ? 1 : 0);
            bean.setFlgAllegatoE(tabSessioni.getFlgAllegatoE() ? 1 : 0);
            bean.setFlgApparatoEricsson(tabSessioni.getFlgApparatoEricsson() ? 1 : 0);
            bean.setFlgApparatoNsn(tabSessioni.getFlgApparatoNsn() ? 1 : 0);
            bean.setFlgApparatoHuawei(tabSessioni.getFlgApparatoHuawei() ? 1 : 0);
            bean.setFlgLockA(tabSessioni.getFlgLockA() ? 1 : 0);
            bean.setFlgLockB(tabSessioni.getFlgLockB() ? 1 : 0);
            bean.setFlgLockC(tabSessioni.getFlgLockC() ? 1 : 0);
            bean.setFlgLockD(tabSessioni.getFlgLockD() ? 1 : 0);
            bean.setFlgLockE(tabSessioni.getFlgLockE() ? 1 : 0);
            bean.setFlgLockEricsson(tabSessioni.getFlgLockEricsson() ? 1 : 0);
            bean.setFlgLockHuawei(tabSessioni.getFlgLockHuawei() ? 1 : 0);
            bean.setFlgLockNsn(tabSessioni.getFlgLockNsn() ? 1 : 0);
            //TODO inserire valore codiLastSTato 
            bean.setCodiLastStato(1);
            
            bean.setClliSito(tabSessioni.getClliSito());
            bean.setCodiCollaudo(tabSessioni.getCodiCollaudo());
            bean.setCodiceDbr(tabSessioni.getCodiceDbr());
            bean.setDescIndirizzo(tabSessioni.getDescIndirizzo());
            bean.setDescLatitudine(tabSessioni.getDescLatitudine());
            bean.setDescLongitudine(tabSessioni.getDescLongitudine());
            bean.setDataSchedaPrRaEs(tabSessioni.getDataSchedaPrRaEs().getTime());
            bean.setDescComune(tabSessioni.getDescComune());
            bean.setDescNote(tabSessioni.getDescNote());
            bean.setDescProvincia(tabSessioni.getDescProvincia());
            bean.setDescRegione(tabSessioni.getDescRegione());
            bean.setTimePrevistoCollaudo(tabSessioni.getTimePrevistoCollaudo().getTime());
            list.add(bean);
        }
        return list;
	 }
	 
	 public static List<SyncAnagBean> getListSyncAnag(List<TabSyncAnagrafica> listEntity){
			SyncAnagBean bean = new SyncAnagBean();
			List<SyncAnagBean> listBean = new ArrayList<SyncAnagBean>();
			for(TabSyncAnagrafica entity : listEntity){
				bean = new SyncAnagBean();
				bean.setCodiSyncAnag(entity.getCodiSyncAnag());
				bean.setDescNameTable(entity.getDescNameTable());
				bean.setNumeVersione(entity.getNumeVersione());
				listBean.add(bean);
			}
			return listBean;
		}
	 
	 public static SyncAnagBean getBeanByNameTable(List<SyncAnagBean> listBean, String descNameTable){
			SyncAnagBean beanFound = new SyncAnagBean();
			for(SyncAnagBean bean : listBean){
				if(bean.getDescNameTable().equals(descNameTable)){
					beanFound.setCodiSyncAnag(bean.getCodiSyncAnag());
					beanFound.setDescNameTable(bean.getDescNameTable());
					beanFound.setNumeVersione(bean.getNumeVersione());
					return beanFound;
				}
			}
			beanFound.setNumeVersione(0);
			return beanFound;
		}
	 
	 public static TabErrorDevice getErrorDeviceEntity(ErrorDeviceBean errorDeviceBean){
		 TabErrorDevice errorDevice = new TabErrorDevice();
		 errorDevice.setCodiSessione(errorDeviceBean.getCodiSessione());
		 errorDevice.setCodiUtente(errorDeviceBean.getCodiUtente());
		 errorDevice.setCodiCliente(errorDeviceBean.getCodiCliente());
		 errorDevice.setDescErrore(errorDeviceBean.getDescErrore());
		 errorDevice.setDescTypeDevice(errorDeviceBean.getDescTypeDevice());
		 errorDevice.setNumeLevelApiDevice(errorDeviceBean.getNumeLevelApiDevice());
		 errorDevice.setDateError(new Date(errorDeviceBean.getDateError()));
		 errorDevice.setNumeVersionApp(errorDeviceBean.getNumeVersionApp());
		 errorDevice.setDescImeiDevice(errorDeviceBean.getDescImeiDevice());
		 errorDevice.setDescTypeError(errorDeviceBean.getDescTypeError());
		 return errorDevice;
	}
	 
	 public static ApkBean getBeanApkDownload(TabApk entity,boolean sqlSingle){
			ApkBean bean = new ApkBean();
			bean.setCodiApk(entity.getCodiApk());
			bean.setNumeVersione(entity.getNumeVersione());
			bean.setDescRelease(entity.getDescRelease());
			
			if(sqlSingle){
				bean.setDescScriptDb(entity.getDescScriptDb());
			}
			return bean;
		}
		
	 
	 public static TabSurvey getTabSurvey(SurveyBean surveyBean){
			TabSurvey tabSurvey = new TabSurvey();
			tabSurvey.setCodiSessione(surveyBean.getCodiSessione());
			tabSurvey.setCodiOggetto(surveyBean.getCodiOggetto());
			tabSurvey.setCodiTipoOggetto(surveyBean.getCodiTipoOggetto());
			Timestamp currentDate = new Timestamp(System.currentTimeMillis());
			tabSurvey.setTimeCreate(currentDate);
//			tabSurvey.setCodiUid(surveyBean.getCodiUid());
			return tabSurvey;
		}
		
		public static TabSurveyDettaglio getTabSurveyDettaglio(SurveyDettaglioBean surveyDettaglioBean) {
			TabSurveyDettaglio tabSurveyDettaglio = new TabSurveyDettaglio();
			tabSurveyDettaglio.setCodiAnagLov(surveyDettaglioBean.getCodiAnagLov());
			tabSurveyDettaglio.setCodiCaratteristica(surveyDettaglioBean.getCodiCaratteristiche());
			tabSurveyDettaglio.setCodiElemento(surveyDettaglioBean.getCodiElementi());
			tabSurveyDettaglio.setCodiGruppo(surveyDettaglioBean.getCodiGruppo());
			tabSurveyDettaglio.setCodiSurvey(surveyDettaglioBean.getCodiSurvey());
			tabSurveyDettaglio.setDescValore(MethodUtilities.creaAccentate(surveyDettaglioBean.getDescValore()));
			tabSurveyDettaglio.setCodiCatalogo(surveyDettaglioBean.getCodiCatalogo());
			tabSurveyDettaglio.setDescNote(surveyDettaglioBean.getDescNote());
			tabSurveyDettaglio.setNumeBranch(surveyDettaglioBean.getNumeBranch());
			tabSurveyDettaglio.setNumeCella(surveyDettaglioBean.getNumeCella());
			tabSurveyDettaglio.setNumeLivelloE(surveyDettaglioBean.getNumeLivelloE());
			tabSurveyDettaglio.setNumeSettore(surveyDettaglioBean.getNumeSettore());
			tabSurveyDettaglio.setNumeVia(surveyDettaglioBean.getNumeVia());
			tabSurveyDettaglio.setDescLabel(surveyDettaglioBean.getDescLabel());
			return tabSurveyDettaglio;
		}
		
		
		public static TabRepositoryMedia getTabRepositoryMedia(MediaBean mediaBean,TabUtente utente,Document doc) {
			TabRepositoryMedia tab = new TabRepositoryMedia();
			tab.setCodiSessione(mediaBean.getCodiSessione());
			tab.setCodiUtente(mediaBean.getCodiUtente());
			tab.setDescName(mediaBean.getFileName());
			tab.setDescCategoria(mediaBean.getDescCategoria());
			if(mediaBean.getMimeType()!=null){
				tab.setDescMimeType(mediaBean.getMimeType());
			}
			if(mediaBean.getDescMedia()!=null){
				tab.setDescNote(MethodUtilities.creaAccentate(mediaBean.getDescMedia()));
			}
			if(mediaBean.getDescSize()!=null){
				tab.setDescSize(mediaBean.getDescSize());
			}
			tab.setDescOwner(utente.getDescUser());
			tab.setDescPath(doc.getPath());
			
			return tab;
		}
		
		public static List <AnagStatiSessioneBean>getAllAnagStatiSessioneBean(List<TabAnagStatiSessione> listTabAnagStatiSessione) {
			AnagStatiSessioneBean anagStatiSessioneBean = new AnagStatiSessioneBean();
			List<AnagStatiSessioneBean> listAnagStatiSessioneBean = new ArrayList<AnagStatiSessioneBean>();
			 for (TabAnagStatiSessione tabAnagStatiSessione : listTabAnagStatiSessione) {
				 anagStatiSessioneBean = new AnagStatiSessioneBean();
				 anagStatiSessioneBean.setCodiStato(tabAnagStatiSessione.getCodiStato());
				 anagStatiSessioneBean.setDescStato(tabAnagStatiSessione.getDescStato());
				 anagStatiSessioneBean.setFlagInLavorazione((tabAnagStatiSessione.getFlagInLavorazione()==true)?1:0);
				 anagStatiSessioneBean.setFlagChiuso((tabAnagStatiSessione.getFlagChiuso()==true)?1:0);
				 listAnagStatiSessioneBean.add(anagStatiSessioneBean);
			}
			
			return listAnagStatiSessioneBean;
		}
		
		public static List<SurveyBean> getListSurveyBean(List<TabSurvey> listSurvey){
			 SurveyBean bean = new SurveyBean();
			 List<SurveyBean> listBean = new ArrayList<SurveyBean>();
			 for(TabSurvey tabSurvey : listSurvey){
				 bean = new SurveyBean();
				 
		         bean.setCodiSessione(tabSurvey.getCodiSessione());
		         bean.setCodiSurveyBackEnd(tabSurvey.getCodiSurvey());
		         bean.setCodiOggetto(tabSurvey.getCodiOggetto());
		         bean.setCodiTipoOggetto(tabSurvey.getCodiTipoOggetto());
				 Timestamp currentDate = new Timestamp(System.currentTimeMillis());
				 bean.setTimeCreate(currentDate.getTime());
				 bean.setFlagEsito(tabSurvey.isFlagEsito() == true ? 1 : 0);
		         listBean.add(bean);
			 }
	         
	         return listBean;
		}
		
		public static List<SurveyDettaglioBean> getSurveyDettaglioBean(List<TabSurveyDettaglio> list){
			List<SurveyDettaglioBean> listBean = new ArrayList<SurveyDettaglioBean>();
			SurveyDettaglioBean bean = null;
			for(TabSurveyDettaglio entity : list){
					bean = new SurveyDettaglioBean();
		            bean.setCodiAnagLov(entity.getCodiAnagLov());
		            bean.setCodiCaratteristiche(entity.getCodiCaratteristica());
		            bean.setCodiDettaglio(entity.getCodiDettaglio());
		            bean.setCodiElementi(entity.getCodiElemento());
		            bean.setCodiGruppo(entity.getCodiGruppo());
		            bean.setCodiSurvey(entity.getCodiSurvey());
		            bean.setDescValore(entity.getDescValore());
		            bean.setCodiCatalogo(entity.getCodiCatalogo());
		            bean.setDescNote(entity.getDescNote());
		            bean.setNumeBranch(entity.getNumeBranch());
		            bean.setNumeCella(entity.getNumeCella());
		            bean.setNumeLivelloE(entity.getNumeLivelloE());
		            bean.setNumeSettore(entity.getNumeSettore());
		            bean.setNumeVia(entity.getNumeVia());
		            bean.setDescLabel(entity.getDescLabel());
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<AnagAntenneBean> getAntenneBean(List<TabAnagAntenne> list){
			List<AnagAntenneBean> listBean = new ArrayList<AnagAntenneBean>();
			AnagAntenneBean bean = null;
			for(TabAnagAntenne entity : list){
					bean = new AnagAntenneBean();
					bean.setCodiAnagAntenne(entity.getCodiAnagAntenne());
					bean.setDescMarca(entity.getDescMarca());
					bean.setDescModello(entity.getDescModello());
					bean.setFlagAttivo(entity.isFlagAttivo() == true ? 1 : 0);
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<AnagDiplexerBean> getDiplexerBean(List<TabAnagDiplexer> list){
			List<AnagDiplexerBean> listBean = new ArrayList<AnagDiplexerBean>();
			AnagDiplexerBean bean = null;
			for(TabAnagDiplexer entity : list){
					bean = new AnagDiplexerBean();
					bean.setCodiAnagDiplexer(entity.getCodiAnagDiplexer());
					bean.setDescMarca(entity.getDescMarca());
					bean.setDescModello(entity.getDescModello());
					bean.setFlagAttivo(entity.isFlagAttivo() == true ? 1 : 0);
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<AnagTmaBean> getTmaBean(List<TabAnagTma> list){
			List<AnagTmaBean> listBean = new ArrayList<AnagTmaBean>();
			AnagTmaBean bean = null;
			for(TabAnagTma entity : list){
					bean = new AnagTmaBean();
					bean.setCodiAnagTma(entity.getCodiAnagTma());
					bean.setDescMarca(entity.getDescMarca());
					bean.setDescModello(entity.getDescModello());
					bean.setFlagAttivo(entity.isFlagAttivo() == true ? 1 : 0);
		            listBean.add(bean);
			}
			return listBean;
		}

		public static List<TabRiferimentiFirma> getTabRiferimentiFirma(List<RiferimentiFirmaBean> list) {
			TabRiferimentiFirma entity = null;
			List<TabRiferimentiFirma> listEntity = new ArrayList<TabRiferimentiFirma>();
			for(RiferimentiFirmaBean bean : list){
				entity = new TabRiferimentiFirma();
				entity.setCodiOggetto(bean.getCodiOggetto());
				entity.setCodiSessione(bean.getCodiSessione());
				entity.setCodiSurvey(bean.getCodiSurvey());
				entity.setCodiUtente(bean.getCodiUtente());
				entity.setDescNomeDitta(MethodUtilities.creaAccentate(bean.getDescNomeDitta()));
				entity.setDescTelefonoDitta(bean.getDescTelefonoDitta());
				entity.setObjMediaFirma(bean.getObjMediaFirma());
				entity.setFlgDitta(bean.getFlgDitta()==1 ? true : false);
				entity.setDescDittaInstallatrice(MethodUtilities.creaAccentate(bean.getDescDittaInstallatrice()));
				listEntity.add(entity);
			}
			return listEntity;
		}
		
		public static List<TabRisultatoSurvey> getTabRisultatoSurvey(List<RisultatoSurveyBean> list) {
			TabRisultatoSurvey entity = null;
			List<TabRisultatoSurvey> listEntity = new ArrayList<TabRisultatoSurvey>();
			for(RisultatoSurveyBean bean : list){
				entity = new TabRisultatoSurvey();
				entity.setCodiOggetto(bean.getCodiOggetto());
				entity.setCodiSessione(bean.getCodiSessione());
				entity.setCodiSurvey(bean.getCodiSurvey());
				entity.setCodiUtente(bean.getCodiUtente());
				entity.setDescRisultatoSurvey(MethodUtilities.creaAccentate(bean.getDescRisultatoSurvey()));
				entity.setCodiValoreRisultatoSurvey(bean.getCodiValoreRisultatoSurvey());
				entity.setDescValoreRisultatoSurvey(Constants.mapRisultatoSurvey.get(bean.getCodiValoreRisultatoSurvey()));
				entity.setCodiAnagRisultato(bean.getCodiAnagRisultato());
				entity.setDescReportFotografico(bean.getDescReportFotografico());
				listEntity.add(entity);
			}
			return listEntity;
		}

		
		public static TabSessioni getTabSessioni(CollaudoBean collaudoBean, Integer codiAreaComp,TabAnagSistemiBande tabAnagSistemi, boolean flagEricson,boolean flagHuawei,boolean flagNokia, Integer codiZona) {
			TabSessioni tabSessioni = new TabSessioni();
			tabSessioni.setCodiCollaudo(collaudoBean.getCodiCollaudo());
			tabSessioni.setDescNomeSito(collaudoBean.getNomeSito());
			tabSessioni.setClliSito(collaudoBean.getClliSito());
			tabSessioni.setCodiceDbr(collaudoBean.getCodiceDBR());
			tabSessioni.setDescRegione(collaudoBean.getRegione().toUpperCase());
			tabSessioni.setDescProvincia(collaudoBean.getProvincia());
			tabSessioni.setDescComune(collaudoBean.getComune());
			tabSessioni.setDescIndirizzo(collaudoBean.getIndirizzo());
			tabSessioni.setDescLongitudine(collaudoBean.getLongitudine());
			tabSessioni.setDescLatitudine(collaudoBean.getLatitudine());
			tabSessioni.setDataSchedaPrRaEs(new Date(collaudoBean.getSchedaProgettoRadioEsecutiva()));
			tabSessioni.setDescCodice(collaudoBean.getCodiceImpianto());
			tabSessioni.setCodiAreaComp(codiAreaComp);
			tabSessioni.setFlgApparatoEricsson(flagEricson);
			tabSessioni.setFlgApparatoHuawei(flagHuawei);
			tabSessioni.setFlgApparatoNsn(flagNokia);
			tabSessioni.setCodiSistema(tabAnagSistemi.getCodiSistema());
			tabSessioni.setCodiBanda(tabAnagSistemi.getCodiBanda());
			tabSessioni.setCodiUnitaTerr(codiZona);
			return tabSessioni;
			
		}
		
		public static List<AnagRisultatiBean> getRisultatiBean(List<TabAnagRisultati> list){
			List<AnagRisultatiBean> listBean = new ArrayList<AnagRisultatiBean>();
			AnagRisultatiBean bean = null;
			for(TabAnagRisultati entity : list){
					bean = new AnagRisultatiBean();
					bean.setCodiAnagRisultati(entity.getCodiAnagRisultati());
					bean.setCodiRisultato(entity.getCodiRisultato());
					bean.setDescAnagRisultati(entity.getDescAnagRisultati());
					bean.setCodiStruttura(entity.getCodiStruttura());
					bean.setFlagApparato(entity.getFlagApparato());
					bean.setFlagD1(entity.getFlagD1());
					bean.setFlagE(entity.getFlagE());
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<TabNoteCaratteristiche> getTabNoteCaratteristicheEntity(List<NoteCaratteristicheBean> list){
			List<TabNoteCaratteristiche> listEntity = new ArrayList<TabNoteCaratteristiche>();
			TabNoteCaratteristiche entity = null;
			for(NoteCaratteristicheBean bean : list){
				entity = new TabNoteCaratteristiche();
				entity.setCodiSessione(bean.getCodiSessione());
				entity.setCodiSurvey(bean.getCodiSurvey());
				entity.setDescNote(MethodUtilities.creaAccentate(bean.getDescNote()));
				entity.setCodiCaratteristica(bean.getCodiCaratteristica());
				entity.setCodiUtente(bean.getCodiUtente());
				entity.setCodiOggetto(bean.getCodiOggetto());
				listEntity.add(entity);
			}
			return listEntity;
		}
		
		// note from tablet
		public static List<TabNote> getTabNoteEntity(List<NoteBean> list) {
			List<TabNote> listEntity = new ArrayList<TabNote>();
			TabNote entity = null;
			for (NoteBean bean : list) {
				entity = new TabNote();
				entity.setDescNota(bean.getDescNota());
				entity.setCodiSessione(bean.getCodiSessione());
				entity.setFlgTablet(bean.getFlgTablet());
				entity.setDescUser(bean.getDescUsername());
				entity.setCodiSurvey(bean.getCodiSurvey());
				entity.setIdNota(bean.getIdNota());
				entity.setCodiOggetto(bean.getCodiOggetto());
				listEntity.add(entity);
			}
			return listEntity;
		}
		
		public static List<AnagRitardiBean> getAnagRitardiBean(List<TabAnagRitardi> list){
			List<AnagRitardiBean> listBean = new ArrayList<AnagRitardiBean>();
			AnagRitardiBean bean = null;
			for(TabAnagRitardi entity : list){
					bean = new AnagRitardiBean();
					bean.setCodiAnagRitardo(entity.getCodiAnagRitardo());
					bean.setDescRitardo(entity.getDescRitardo());
					bean.setFlagCheckOut(entity.getFlagCheckOut() == true ? 1 : 0);
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<TabRelRitardiSessioni> getRelRitardiSessioniEntity(List<RelRitardiSessioniBean> list) {
			TabRelRitardiSessioni entity = null;
	        List<TabRelRitardiSessioni> listEntity = new ArrayList<TabRelRitardiSessioni>();
	        for(RelRitardiSessioniBean bean : list){
	        	entity = new TabRelRitardiSessioni();
	        	entity.setCodiSessione(bean.getCodiSessione());
	        	entity.setCodiAnagRitardo(bean.getCodiAnagRitardo());
	        	listEntity.add(entity);
	        }
	        return listEntity;
	    }
		
		public static List<SistemiBean> getSistemiBean(List<TabSistemi> list){
			List<SistemiBean> listBean = new ArrayList<SistemiBean>();
			SistemiBean bean = null;
			for(TabSistemi entity : list){
					bean = new SistemiBean();
					bean.setCodiSistema(entity.getCodiSistema());
					bean.setDescSistema(entity.getDescSistema());
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<BandeBean> getBandeBean(List<TabBande> list){
			List<BandeBean> listBean = new ArrayList<BandeBean>();
			BandeBean bean = null;
			for(TabBande entity : list){
					bean = new BandeBean();
					bean.setCodiBanda(entity.getCodiBanda());
					bean.setDescBanda(entity.getDescBanda());
		            listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<NoteCaratteristicheBean> getNoteCaratteristicheBean(List<TabNoteCaratteristiche> list){
			List<NoteCaratteristicheBean> listBean = new ArrayList<NoteCaratteristicheBean>();
			NoteCaratteristicheBean bean = null;
			for(TabNoteCaratteristiche entity : list){
				bean = new NoteCaratteristicheBean();
				bean.setCodiSessione(entity.getCodiSessione());
				bean.setCodiSurvey(entity.getCodiSurvey());
				bean.setDescNote(entity.getDescNote());
				bean.setCodiCaratteristica(entity.getCodiCaratteristica());
				bean.setCodiUtente(entity.getCodiUtente());
				bean.setCodiOggetto(entity.getCodiOggetto());
				listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<NoteBean> getNoteBean(List<TabNote> list){
			List<NoteBean> listBean = new ArrayList<NoteBean>();
			NoteBean bean = null;
			for(TabNote entity : list){
				bean = new NoteBean();
				bean.setIdNota(entity.getIdNota());
				bean.setCodiSessione(entity.getCodiSessione());
				bean.setCodiSurvey(entity.getCodiSurvey());
				bean.setDescNota(entity.getDescNota());
				bean.setDescUsername(entity.getDescUser());
				bean.setFlgTablet(entity.getFlgTablet());
				bean.setCodiOggetto(entity.getCodiOggetto());
				listBean.add(bean);
			}
			return listBean;
		}
		
		public static List<RisultatoSurveyBean> getRisultatoSurveyBean(List<TabRisultatoSurvey> listRisultato) {
	        List<RisultatoSurveyBean> listBean = new ArrayList<RisultatoSurveyBean>();
	        RisultatoSurveyBean bean = null;
	        for (TabRisultatoSurvey entity : listRisultato) {
	        	bean = new RisultatoSurveyBean();
	        	bean.setCodiRisultato(entity.getCodiRisultato());
	        	bean.setCodiAnagRisultato(entity.getCodiAnagRisultato());
	        	bean.setCodiSessione(entity.getCodiSessione());
	        	bean.setCodiSurvey(entity.getCodiSurvey());
	        	bean.setCodiOggetto(entity.getCodiOggetto());
	        	bean.setCodiUtente(entity.getCodiUtente());
	        	bean.setDescRisultatoSurvey(entity.getDescRisultatoSurvey());
	        	bean.setCodiValoreRisultatoSurvey(entity.getCodiValoreRisultatoSurvey());
	        	bean.setDescReportFotografico(entity.getDescReportFotografico());
	        	listBean.add(bean);
	        }
	        return listBean;
	    }
		
		public static TabRepositoryMedia getTabRepositoryMedia(ViwSessioni mediaBean,Integer codiUtente,Document doc, String fileName) {
			TabRepositoryMedia tab = new TabRepositoryMedia();
			tab.setCodiSessione(mediaBean.getCodiSessione());
			tab.setCodiUtente(codiUtente);
			tab.setDescName(fileName);
			/*if(mediaBean.getMimeType()!=null){
				tab.setDescMimeType(mediaBean.getMimeType());
			}
			if(mediaBean.getDescMedia()!=null){
				tab.setDescNote(MethodUtilities.creaAccentate(mediaBean.getDescMedia()));
			}
			if(mediaBean.getDescSize()!=null){
				tab.setDescSize(mediaBean.getDescSize());
			}*/
			//tab.setDescOwner(utente.getDescUser());
			tab.setDescPath(doc.getPath());
			
			return tab;
		}

		public static RepositorySchedaRischiBean getRepositorySchedaRischiBean(TabRepositorySchedaRischi tabRepositorySchedaRischi) {
			RepositorySchedaRischiBean repositorySchedaRischiBean = new RepositorySchedaRischiBean();
			repositorySchedaRischiBean.setCodiSessione(tabRepositorySchedaRischi.getCodiSessione());
			repositorySchedaRischiBean.setDescMimeType(tabRepositorySchedaRischi.getDescMimeType());
			repositorySchedaRischiBean.setDescNome(tabRepositorySchedaRischi.getDescNome());
			repositorySchedaRischiBean.setDescPath(tabRepositorySchedaRischi.getDescPath());
			repositorySchedaRischiBean.setFlagSchedaRischi(tabRepositorySchedaRischi.isFlagSchedaRischi()? 1:0);
			if(tabRepositorySchedaRischi.getTimeCreate()!=null)
				repositorySchedaRischiBean.setTimeCreate(tabRepositorySchedaRischi.getTimeCreate().getTime());
			repositorySchedaRischiBean.setVersione(tabRepositorySchedaRischi.getVersione());
			return repositorySchedaRischiBean;
		}

		public static List<RiferimentiFirmaBean> getRiferimentiFirmaBean(List<TabRiferimentiFirma> listRiferimenti) {
	        List<RiferimentiFirmaBean> listBean = new ArrayList<RiferimentiFirmaBean>();
	        RiferimentiFirmaBean bean = null;
	        for (TabRiferimentiFirma entity : listRiferimenti) {
	        	bean = new RiferimentiFirmaBean();
	        	bean.setCodiOggetto(entity.getCodiOggetto());
	        	bean.setCodiSessione(entity.getCodiSessione());
	        	bean.setCodiSurvey(entity.getCodiSurvey());
	        	bean.setCodiUtente(entity.getCodiUtente());
	        	bean.setDescDittaInstallatrice(entity.getDescDittaInstallatrice());
	        	bean.setDescNomeDitta(entity.getDescNomeDitta());
	        	bean.setDescTelefonoDitta(entity.getDescTelefonoDitta());
	        	if (entity.getFlgDitta()) {
	        		bean.setFlgDitta(1);
	        	} else {
	        		bean.setFlgDitta(0);
	        	}
	        	
	        	bean.setObjMediaFirma(entity.getObjMediaFirma());
	        	listBean.add(bean);
	        }
	        return listBean;
	    }

		public static List<VerbaleBean> getListVerbaleBean(List<ViwVerbale> listViwVerbale) {
			
			List<VerbaleBean> list = new ArrayList<>();
			
			for (ViwVerbale viwVerbale:listViwVerbale) {
				list.add(getVerbaleBean(viwVerbale));
			}
			
			return list;
		}
		
		public static VerbaleBean getVerbaleBean(ViwVerbale viwVerbale) {
			
			VerbaleBean verbaleBean = new VerbaleBean();
			verbaleBean.setCodiOggetto(viwVerbale.getCodiOggetto());
			verbaleBean.setCodiSessione(viwVerbale.getCodiSessione());
			verbaleBean.setCodiRisultato(viwVerbale.getCodiStatoVerbale());
			verbaleBean.setFlgEsito(viwVerbale.getCodiStatoVerbale()==null || viwVerbale.getCodiStatoVerbale()==Const.VERBALE_NON_LAVORATO? 0:1);
			verbaleBean.setFlgCollaudoDittaTerminato(viwVerbale.getFlgParziale()==null ? 0 : viwVerbale.getFlgParziale());
			verbaleBean.setDescOggetto(viwVerbale.getDescOggetto());
			return verbaleBean;
		}

		public static List<SchedaDynBean> getListScheDynBean(List<TabSchedaDyn> schedaDynByCodiSessioneList) {
			
			List<SchedaDynBean> list = new ArrayList<>();
			
			for (TabSchedaDyn tabSchedaDyn : schedaDynByCodiSessioneList) {
				
				SchedaDynBean schedaDynBean = new SchedaDynBean();
				
				schedaDynBean.setAccessibilita(tabSchedaDyn.getAccessibilita());
				schedaDynBean.setCodiceMazzoChiavi(tabSchedaDyn.getCodiceMazzoChiavi());
				schedaDynBean.setCodiSessione(tabSchedaDyn.getCodiSessione());
				schedaDynBean.setDataCreazioneScheda(tabSchedaDyn.getDataCreazioneScheda()!=null?tabSchedaDyn.getDataCreazioneScheda().toString():null);
				schedaDynBean.setDisagiato(tabSchedaDyn.getDisagiato());
				schedaDynBean.setFasciaIntervento(tabSchedaDyn.getFasciaIntervento());
				schedaDynBean.setItinerario(tabSchedaDyn.getItinerario());
				schedaDynBean.setNoteAccessibilita(tabSchedaDyn.getNoteAccessibilita());
				schedaDynBean.setNoteAccesso(tabSchedaDyn.getNoteAccesso());
				schedaDynBean.setRegime(tabSchedaDyn.getRegime());
				schedaDynBean.setTipoAccesso(tabSchedaDyn.getTipoAccesso());
				schedaDynBean.setVersione(tabSchedaDyn.getVersione());
				list.add(schedaDynBean);
				
			}
			
			return list;
		}
}
