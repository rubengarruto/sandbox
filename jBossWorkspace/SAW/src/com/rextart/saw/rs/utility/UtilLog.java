package com.rextart.saw.rs.utility;

import java.util.HashMap;
import java.util.List;

import org.jboss.logging.Logger;

import com.rextart.saw.rs.bean.request.AnagraficaBeanRequest;
import com.rextart.saw.rs.bean.request.ApkBean;
import com.rextart.saw.rs.bean.request.CheckInOutBeanRequest;
import com.rextart.saw.rs.bean.request.DettaglioSurveyDownloadBeanRequest;
import com.rextart.saw.rs.bean.request.DocumentoDynRequestBean;
import com.rextart.saw.rs.bean.request.MediaBean;
import com.rextart.saw.rs.bean.request.NoteCaratteristicheBean;
import com.rextart.saw.rs.bean.request.RelRitardiSessioniBean;
import com.rextart.saw.rs.bean.request.RiferimentiFirmaBean;
import com.rextart.saw.rs.bean.request.RisultatoSurveyBean;
import com.rextart.saw.rs.bean.request.SincronizzazioneBean;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;


public class UtilLog {
	
	    public static final int COLLAUDO_ACQUISITO = 1;
	    public static final int COLLAUDO_DISPONIBILE = 2;
	    public static final int COLLAUDO_IN_LAVORAZIONE = 3;
	    public static final int COLLAUDO_ANNULLATO = 4;
	    public static final int COLLAUDO_CHIUSO = 5;
	    
	    public static HashMap<Integer,String> mapStatiSessione = new HashMap<Integer,String>();
		static{
			mapStatiSessione.put(COLLAUDO_ACQUISITO, "ACQUISITO");
			mapStatiSessione.put(COLLAUDO_DISPONIBILE, "DISPONIBILE");
			mapStatiSessione.put(COLLAUDO_IN_LAVORAZIONE, "IN_LAVORAZIONE");
			mapStatiSessione.put(COLLAUDO_ANNULLATO, "ANNULLATO");
			mapStatiSessione.put(COLLAUDO_CHIUSO, "CHIUSO");
		}
	    
	private static final Logger logger = Logger.getLogger(UtilLog.class);
	
	public static Logger getLog(){
		return logger;
	}
	public static void printListServizioDownAnag(AnagraficaBeanRequest bean){
		logger.info("-----------------------------------------------------");
		logger.info("Inizio Servizio Download Anagrafica: "+" CodiUtente: "+bean.getCodiUtente()+" DescImei: "+bean.getDescImei());
		
	}
	
	public static void printSincronizzaFigliSurvey(SincronizzazioneBean sincronizzazioneBean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Sincronizzazione Dettaglio Survey");
		logger.info("codiSessione:"+sincronizzazioneBean.getCodiSessione()+" numeVersionApp:" +sincronizzazioneBean.getNumeVersioneApp()+" codiUtente: "
		+sincronizzazioneBean.getCodiUtente()+" codiOggetto: "+sincronizzazioneBean.getCodiOggetto()+" codiImei: "+sincronizzazioneBean.getDescImei()
		+" lastCall: "+sincronizzazioneBean.isLastCall());
		
		for(SurveyDettaglioBean dettaglio : sincronizzazioneBean.getSurveyDettaglioBeans()){
			logger.info("codiSurvey:"+dettaglio.getCodiSurvey()+" codiElementi:"+dettaglio.getCodiElementi()+" codiDettaglio:"+dettaglio.getCodiDettaglio()+" codiAnagLov:"+dettaglio.getCodiAnagLov()+" codiCaratt:"+dettaglio.getCodiCaratteristiche()+" descValore:"+dettaglio.getDescValore());
		}
		
		logger.info("-----------------------------------------------------");
		logger.info("Salvataggio Riferimenti Firma");
		for(RiferimentiFirmaBean firma : sincronizzazioneBean.getRiferimentiFirmaBeans()){
			logger.info("codiSurvey:"+firma.getCodiSurvey()+" codiSessione:"+firma.getCodiSessione()+" codiOggetto:"+firma.getCodiOggetto()+" flagDitta:"+firma.getFlgDitta());
		}
		
		logger.info("-----------------------------------------------------");
		logger.info("Salvataggio Risultati Survey");
		for(RisultatoSurveyBean risultatoSurveyBean : sincronizzazioneBean.getRisultatoSurveyBeans()){
			logger.info("codiAnagRisultato:" + risultatoSurveyBean.getCodiAnagRisultato() + " codiValoreRisultato:" + risultatoSurveyBean.getCodiValoreRisultatoSurvey()+ " codiSurvey:"+risultatoSurveyBean.getCodiSurvey());
		}
		
		logger.info("-----------------------------------------------------");
		logger.info("Salvataggio Note caratteristiche");
		for(NoteCaratteristicheBean noteCaratteristicheBean : sincronizzazioneBean.getNoteCaratteristicheBeans()){
			logger.info("codiCaratteristica:" + noteCaratteristicheBean.getCodiCaratteristica() + " codiSurvey:" + noteCaratteristicheBean.getCodiSurvey()
					+" codiSessione: "+noteCaratteristicheBean.getCodiSessione()+ " codiOggetto:"+ noteCaratteristicheBean.getCodiOggetto());
		}
		
		logger.info("-----------------------------------------------------");
		logger.info("Salvataggio Rel Ritardi Sessioni");
		for(RelRitardiSessioniBean ritardiSessioniBean : sincronizzazioneBean.getRelRitardiSessioniBeans()){
			logger.info("codiAnagRitardo:" + ritardiSessioniBean.getCodiAnagRitardo());
		}
		
	}
	
	
	public static String creaAccentate(String stringa){
		String s = ""; 
		String aposString = new String("\'");
		try {
			s =  stringa.replace("&agrave;", "�").replace("&aacute;","�").replace("&egrave;", "�").replace("&igrave;", "�").replace("&uacute;", "�").replace("&ograve;", "�").replaceAll("&ugrave;", "�")
					.replace("&deg;", "�").replace("&oacute;", "�").replace("&eacute;", "�").replace( "&iacute;","�").replaceAll("&apos;", aposString).replace("&#8364", "�").replace("&#163", "�");
		} catch (NullPointerException e) {}
		return s; 
	}
	
	public static void printDownloadVersion(ApkBean bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Dowload Version via Blob");
		logger.info("numeVersioneDevice:"+bean.getNumeVersione());
	}
	
	public static void printDownloadVersionUrl(){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Dowload Version via URL");
	}
	
	public static void printCheckVersion(ApkBean bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Check Version");
		logger.info("numeVersioneDevice:"+bean.getNumeVersione());
	}
	
	
	public static void printDeleteSurvey(Integer codiSurveyBackEnd){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Cancellazione Survey");
		logger.info("codiSurveyBackEnd: "+codiSurveyBackEnd);
	}
	
	public static void printSincronizzaMedia(MediaBean mediaBean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio Sincronizzazione Media");
		logger.info("codiSessione:"+mediaBean.getCodiSessione()+ " fileName:"+mediaBean.getFileName()+ " codiUtente:"+mediaBean.getCodiUtente());
	}
	
	public static void printDownloadDettaglioSurvey(DettaglioSurveyDownloadBeanRequest bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio download Dettaglio Survey");
		String codiSurvey="";
		for(Integer codi : bean.getListCodiSurvey()){
			codiSurvey += codi +",";
		}
		logger.info("codiSessione:"+bean.getCodiSessione()+ " codiSurvey:"+codiSurvey +" codiUtente:"+bean.getCodiUtente());
	}
	
	public static void printCheckInOut(CheckInOutBeanRequest bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio CheckIn/Out Oggetto");
		logger.info("codiSessione: "+bean.getCodiSessione()+" codiUtente: "+bean.getCodiUtente()+" codiOggetto: "+bean.getCodiOggetto()+" isFlagLockDevice: "+bean.isFlagLock());
	}
	
	public static void printDownloadDocumentoDyn(DocumentoDynRequestBean bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio downloadDocumentoDyn");
	}
	
	public static void printIsLockOggetto(boolean isLock){
		logger.info("-----------------------------------------------------");
		logger.info("Stato Oggetto Lock: "+isLock);
	}
	
	public static void printCheckOutSync(SincronizzazioneBean bean){
		logger.info("-----------------------------------------------------");
		logger.info("Servizio CheckIN/Sync Rilascio oggetto");
		logger.info("codiSessione: "+bean.getCodiSessione()+" codiUtente: "+bean.getCodiUtente()+" codiOggetto: "+bean.getCodiOggetto()+" isFlagLockDevice: "+bean.isFlagLock());
	}
	
	public static void printCambioStatoSessione(Integer codiNuovoStato){
		logger.info("-----------------------------------------------------");
		logger.info("Cambio StatoSessione, nuovo codiStato: "+codiNuovoStato+ " descStato: "+mapStatiSessione.get(codiNuovoStato));
	}
	
	public static void printControllaSurveyRisultati(Integer codiSessione, boolean isValore){
		logger.info("-----------------------------------------------------");
		logger.info("Controllo SurveySessione se hanno tutti i risultati");
		logger.info("codiSessione: "+codiSessione+ " tutti i survey hanno risultati: "+isValore);
	}
	
	public static void printCambioStatoSessione(Integer codiSessione, Integer newStato){
		logger.info("-----------------------------------------------------");
		logger.info("Cambio stato Sessione");
		logger.info("codiSessione: "+codiSessione+ " newStato: "+newStato);
	}
	
	public static void printUpdateNoteCarrattSurvey(Integer codiSurvey, Integer codiCaratt, String descNote){
		logger.info("-----------------------------------------------------");
		logger.info("Update Note Caratteristiche Survey");
		logger.info("codiSurvey: "+codiSurvey+" codiCaratt: "+codiCaratt+" descNote: "+descNote);
	}
	
	public static void printCheckOutRitardo(boolean isRitardo){
		logger.info("-----------------------------------------------------");
		logger.info("CheckOut in ritardo:"+isRitardo);
		
	}
	
	public static void printSurveyDettCanc(List<Integer> listCodiSurvey){
		logger.info("-----------------------------------------------------");
		logger.info("Cancellazione Dettagli Survey");
		for(Integer codiSurvey : listCodiSurvey){
			logger.info("codiSurvey: "+codiSurvey);
		}
	}
	
	public static void printCallAddCollaudo() {
		logger.info("CHIAMATA DA PEV PER ACQUISIZIONE NUOVO COLLAUDO");
		
	}
}


