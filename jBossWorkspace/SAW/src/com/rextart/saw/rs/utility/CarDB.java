package com.rextart.saw.rs.utility;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

import com.rextart.saw.miaSezione.myBeans.Car;

@Named
@ApplicationScoped
public class CarDB {

	private CarFactory carFactory = new CarFactory();
	

	public List<Car> getAllCars() {
		return carFactory.createCars(40);
	}
}
