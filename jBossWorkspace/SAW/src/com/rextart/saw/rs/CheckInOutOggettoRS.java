package com.rextart.saw.rs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.entity.TabRelSesVerbaleParziale;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.rs.bean.request.CheckInOutBeanRequest;
import com.rextart.saw.rs.bean.response.CheckInOutBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SurveyService;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;

@RequestMapping("/rest")
@Controller
public class CheckInOutOggettoRS {

	@Autowired
	private SessioniService sessioniService;
	
	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SincronizzazioneDao sincronizzazioneDao;
	
	@RequestMapping(value = "/checkInOutOggetto", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse checkInOutOggetto(@RequestBody CheckInOutBeanRequest request) throws Exception {
		GeneralResponse gr = new GeneralResponse();
		CheckInOutBean checkInOutBean = new CheckInOutBean();
		UtilLog.printCheckInOut(request);
		TabRelSessioneStatiVerbale listaOggettiEsiti = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(Const.ALLEGATO_D1, request.getCodiSessione());
		
		if(listaOggettiEsiti == null){
			listaOggettiEsiti = new TabRelSessioneStatiVerbale();
			listaOggettiEsiti.setCodiOggetto(request.getCodiOggetto());
			listaOggettiEsiti.setCodiSessione(request.getCodiSessione());
			listaOggettiEsiti.setCodiStatoVerbale(Const.VERBALE_NON_LAVORATO);
		}
	
		
		try {
			// caso in cui il tablet vuole fare il lock dell oggetto
			if(request.isFlagLock()){
				//se gi� preso in carico, non permetto il check out
				if(sessioniService.isLockUnlockOggetto(request.getCodiSessione(), request.getCodiOggetto(), request.isFlagLock())){
					gr.setResponseCode(Constants.ERROR_CHECK_IN_NOT_ALLOWED);
				}else if(request.getCodiOggetto() == Const.ALLEGATO_D && !(listaOggettiEsiti.getCodiStatoVerbale() == null || listaOggettiEsiti.getCodiStatoVerbale() == Const.VERBALE_NON_LAVORATO)){
					gr.setResponseCode(Constants.ERROR_ESITOD_ALREADY_EXIST);
				}
				else {
					if(sessioniService.isSessioneStatoDisponibile(request.getCodiSessione())){
						
						// al primo checkout lo stato sessione va in LAVORAZIONE
						sessioniService.changeStatoSessione(request.getCodiSessione(), sessioniService.getCodiStateInLavorazione());
						UtilLog.printCambioStatoSessione(sessioniService.getCodiStateInLavorazione());
						
						UtilLog.printCheckOutRitardo(sessioniService.isCheckRitardo());
						checkInOutBean.setFlagRitardo(sessioniService.isCheckRitardo());
					}
					/*INIZIO Se lo prende in carico un utente ditta  e il verbale �ra stato precedentemente chiuso parzialmente(solo utente Ditta pu� chiuderlo) slavo nella tabella TabRelSesVerbaleParziale con flaParziale a false*/
					TabUtente tabUtente =userService.getUserById(request.getCodiUtente());
					TabRelSesVerbaleParziale tabRelSesVerbaleParziale=sessioniService.findByOggettoSessione(request.getCodiOggetto(), request.getCodiSessione());
					if(tabUtente.isFlgImpresa() || tabUtente.getCodiVendor()!=0){
						if(tabRelSesVerbaleParziale.isFlgParziale()){
							sessioniService.saveRelSessVerbaleParziale(request.getCodiOggetto(), request.getCodiSessione(), false);
							checkInOutBean.setFlagDittaParziale(false);
						}
					
					} else {//se sono un tim e comunque � chiuso parzialmente, aggiorno il tablet che � stato chiuso parzialmente
						if(tabRelSesVerbaleParziale.isFlgParziale()){
							checkInOutBean.setFlagDittaParziale(true);
						}
					}
					/*FINE*/
					
					//aggiorno il tablet sullo stato dei survey
					checkInOutBean.setListSurveyBean(DataConverter.getListSurveyBean(surveyService.getListSurveyBySessioneOggetto(request.getCodiSessione(),request.getCodiOggetto())));
					
					gr.setResponseObject(checkInOutBean);
					
					if(sessioniService.isSessioneStatoDisponibile(request.getCodiSessione()) || sessioniService.isSessioneStatoInLavorazione(request.getCodiSessione())){
						sessioniService.lockUnLockDoc(request.getCodiSessione(), request.getCodiOggetto(), request.getCodiUtente(), request.isFlagLock());
						
						gr.setResponseCode(Constants.RESPONSE_OK);
					}
					else{
						gr.setResponseCode(Constants.ERROR_SYNC_NO_MODIFY);
					}
					
				}
			}else{
				// effettuo la sync/checkin dell'oggetto in quale viene sbloccato
				if(sessioniService.isSessioneStatoInLavorazione(request.getCodiSessione())){
					sessioniService.lockUnLockDoc(request.getCodiSessione(), request.getCodiOggetto(), request.getCodiUtente(), request.isFlagLock());
					gr.setResponseCode(Constants.RESPONSE_OK);
				}
			}
		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO checkInOutOggetto "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}
	
}
