package com.rextart.saw.rs;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.entity.TabApk;
import com.rextart.saw.rs.bean.request.ApkBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.ApkService;


@RequestMapping("/rest")
@Controller
public class ApkRS {
	
	@Autowired
	private ApkService apkService;

	@RequestMapping(value = "/apk/checkVersion", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse checkVersion(@RequestBody ApkBean apkBean) throws Exception {
		GeneralResponse gr = new GeneralResponse();
		try {
			UtilLog.printCheckVersion(apkBean);
			
			TabApk entityApk = null;
			Integer numeVersione = apkService.getNumeLastVersion();
			
			if(apkBean.getNumeVersione() == null || apkBean.getNumeVersione() < numeVersione){
				Object[]obj = apkService.getScriptDbFromVersion(apkBean.getNumeVersione(),numeVersione);
				entityApk = (TabApk) obj[1];
				if((numeVersione - apkBean.getNumeVersione()) > 1){
					String sqlAppend = "";
					List<String> listScriptDb = (List<String>) obj[0];
					
					for(String script : listScriptDb){
						if(script!=null){
							sqlAppend += script;
						}
					}
					
					apkBean = DataConverter.getBeanApkDownload(entityApk, false);
					apkBean.setDescScriptDb(sqlAppend);
				}else{
					apkBean = DataConverter.getBeanApkDownload(entityApk, true);
				}	
				apkBean.setExistNewVersion(true);
			}
				
				
			gr.setResponseObject(apkBean);
			gr.setResponseCode(Constants.RESPONSE_OK);
					
		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO checkVersion "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}
	
	
	@RequestMapping(value="/apk/downloadVersion")
	@ResponseBody
	public void downloadVersion(HttpSession session,HttpServletResponse response) throws Exception {
		InputStream inputStream = null;
	    try {
	    	UtilLog.printDownloadVersionUrl();
	    	
	        String filePathToBeServed = Constants.PATH_APK + "/" + Constants.NAME_FILE_APK + Constants.EXTENTION_APK;
	        File fileToDownload = new File(filePathToBeServed);
	        inputStream = new FileInputStream(fileToDownload);
	        response.setContentType("application/octet-stream");
	        response.setHeader("Content-Disposition", "attachment; filename="+"ARIES"+".apk"); 
	        IOUtils.copy(inputStream, response.getOutputStream());
	   } catch (Exception e){
		   UtilLog.getLog().error("ERRORE SERVIZIO downloadVersion URL "+e.getMessage());
	       e.printStackTrace();
	    }finally{
	    	try{
	    		if(response!=null){
	    			response.flushBuffer();
	    		}
	    		
	    		if(inputStream!=null){
	    			inputStream.close();
	    		}
	    	}catch(Exception e){
	    		UtilLog.getLog().error("ERRORE SERVIZIO downloadVersion URL "+e.getMessage());
	    		e.printStackTrace();
	    	}
	    }
	}
	
}
