package com.rextart.saw.rs;

import java.io.InputStream;


import org.apache.commons.io.IOUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.rextart.saw.openKM.ServiceRepositoryImpl;
import com.rextart.saw.rs.bean.request.DocumentoDynRequestBean;
import com.rextart.saw.rs.bean.response.DocumentoDynBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SurveyService;




@RequestMapping("/rest")
@Controller
public class DocumentyDynRS {

	@Autowired
	private SessioniService sessioniService;

	@Autowired
	private RepositoryMediaService repositoryMediaService; 

	@Autowired
	private SurveyService surveyService;

	@RequestMapping(value = "/downloadDocumentiDyn", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse downloadDocumentiDyn(@RequestBody DocumentoDynRequestBean documentoDynRequestBean){
		GeneralResponse gr = new GeneralResponse();
		DocumentoDynBean documentoDynBean = new DocumentoDynBean();
		UtilLog.printDownloadDocumentoDyn(documentoDynRequestBean);
		
		try {
			
			String path= documentoDynRequestBean.getDocumentPath();
			
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.downloadDocumentiDynRS(path) ;
			documentoDynBean.setFile(IOUtils.toByteArray(is));
		
		
			gr.setResponseObject(documentoDynBean);
			gr.setResponseCode(Constants.RESPONSE_OK);

		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO Download Documenti Dyn "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}
	
	


}
