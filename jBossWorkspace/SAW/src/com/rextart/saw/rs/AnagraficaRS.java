 package com.rextart.saw.rs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabAnagLov;
import com.rextart.saw.entity.TabAnagRisultati;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCaratteristiche;
import com.rextart.saw.entity.TabElementi;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelCaratteristicheLov;
import com.rextart.saw.entity.TabRelElementiCaratt;
import com.rextart.saw.entity.TabRelOggettiElementi;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelServiziOggetti;
import com.rextart.saw.entity.TabServizi;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.entity.TabTipologieOggetto;
import com.rextart.saw.rs.bean.request.AnagraficaBeanRequest;
import com.rextart.saw.rs.bean.request.SyncAnagBean;
import com.rextart.saw.rs.bean.response.AnagraficheBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.AnagService;

@RequestMapping("/rest")
@Controller
public class AnagraficaRS {

	@Autowired
	private AnagService service;
	/**
	 * @description metodo utilizzato dal tablet per scaricare tutte le tabelle di anagrafica
	 * @return tabelle di anagrafica
	 */
	@RequestMapping(value = "/anag/getAnag", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse getAnag(@RequestBody AnagraficaBeanRequest anagraficaBeanRequest) {
		GeneralResponse gr = new GeneralResponse();
		try {
			UtilLog.printListServizioDownAnag(anagraficaBeanRequest);
			AnagraficheBean anagraficheBean = new AnagraficheBean();
			
			List<TabSyncAnagrafica> listSyncAnagraficaBackend = service.getAllTabSyncAnagrafica();
			anagraficheBean.setListSyncAnagraficas(DataConverter.getListSyncAnag(listSyncAnagraficaBackend));
			
			SyncAnagBean syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_CARATTERISTICHE);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_CARATTERISTICHE).getNumeVersione()){
				List<TabCaratteristiche> listTabCaratteristiche = service.getAllCaratteristiche();
				anagraficheBean.setArrCaratteristiche(DataConverter.getAllCaratteristicheBean(listTabCaratteristiche));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ELEMENTI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ELEMENTI).getNumeVersione()){
				List<TabElementi> listTabElementi = service.getAllElementi();
				anagraficheBean.setArrElementi(DataConverter.getAllElementiBean(listTabElementi));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_LOV);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_LOV).getNumeVersione()){
				List<TabAnagLov> listTabAnagLov = service.getAllAnagLov();
				anagraficheBean.setArrLovList(DataConverter.getAllAnagLovBean(listTabAnagLov));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_OGGETTI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_OGGETTI).getNumeVersione()){
				List<TabOggetti> listTabOggetti = service.getAllOggetti();
				anagraficheBean.setArrOggetti(DataConverter.getAllOggettiBean(listTabOggetti));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_SERVIZI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_SERVIZI).getNumeVersione()){
				List<TabServizi> listTabServizi = service.getAllServizi();
				anagraficheBean.setArrServiziBeans(DataConverter.getAllServiziBean(listTabServizi));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_STRUTTURA);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_STRUTTURA).getNumeVersione()){
				List<TabStruttura> listTabStruttura = service.getAllStruttura();
				anagraficheBean.setArrStrutturaBeans(DataConverter.getAllTabStruttura(listTabStruttura));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_REL_CARATTERISTICHE_LOV);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_REL_CARATTERISTICHE_LOV).getNumeVersione()){
				List<TabRelCaratteristicheLov> listTabRelCaratteristicheLov = service.getAllRelCaratteristicheLov();
				anagraficheBean.setArrRelCaratteristicheLovBeans(DataConverter.getAllCaratteristicheLovBean(listTabRelCaratteristicheLov));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_REL_OGGETTI_ELEMENTI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_REL_OGGETTI_ELEMENTI).getNumeVersione()){
				List<TabRelOggettiElementi> listTabRelOggettiElementi = service.getAllRelOggettiElementi();
				anagraficheBean.setArrRelOggettiElementiBeans(DataConverter.getAllOggettiElementiBean(listTabRelOggettiElementi));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_REL_ELEMENTI_CARATT);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_REL_ELEMENTI_CARATT).getNumeVersione()){
				List<TabRelElementiCaratt> listTabRelElementiCaratt = service.getAllRelElementiCaratt();
				anagraficheBean.setArrRelElementiCarattBeans(DataConverter.getAllRelElementiCarattBean(listTabRelElementiCaratt));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_REL_SERVIZI_OGGETTI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_REL_SERVIZI_OGGETTI).getNumeVersione()){
				List<TabRelServiziOggetti> listTabRelServiziOggetti = service.getAllRelServiziOggetti();
				anagraficheBean.setArrRelServiziOggettiBeans(DataConverter.getAllRelServiziOggettiBean(listTabRelServiziOggetti));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_TIPOLOGIE_OGGETTO);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_TIPOLOGIE_OGGETTO).getNumeVersione()){
				List<TabTipologieOggetto> listTabTipologieOggetto= service.getAllTipologieOggetto();
				anagraficheBean.setArrTipologieOggettoBeans(DataConverter.getAllAnagTipologiaOggettoBean(listTabTipologieOggetto));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_REL_OGGETTI_TIPO);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_REL_OGGETTI_TIPO).getNumeVersione()){
				List<TabRelOggettiTipo> listTabRelOggettiTipo= service.getAllRelOggettiTipo();
				anagraficheBean.setArrRelOggettiTipoBeans(DataConverter.getAllRelOggettiTipoBean(listTabRelOggettiTipo));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_STATI_SESSIONE);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_STATI_SESSIONE).getNumeVersione()){
				List<TabAnagStatiSessione> listTabAnagStatiSessione = service.getAllAnagStatiSessione();
				anagraficheBean.setArrStatiSessione(DataConverter.getAllAnagStatiSessioneBean(listTabAnagStatiSessione));
			}	
			
			/****/
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_ANTENNE);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_ANTENNE).getNumeVersione()){
				List<TabAnagAntenne> listTabAnag = service.getAllAnagAntenne();
				anagraficheBean.setArrAntenneBeans(DataConverter.getAntenneBean(listTabAnag));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_DIPLEXER);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_DIPLEXER).getNumeVersione()){
				List<TabAnagDiplexer> listTabAnag = service.getAllAnagDiplexer();
				anagraficheBean.setArrDiplexerBeans(DataConverter.getDiplexerBean(listTabAnag));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_TMA);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_TMA).getNumeVersione()){
				List<TabAnagTma> listTabAnag = service.getAllAnagTma();
				anagraficheBean.setArrTmaBeans(DataConverter.getTmaBean(listTabAnag));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_RISULTATI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_RISULTATI).getNumeVersione()){
				List<TabAnagRisultati> listTabAnagRisultati = service.getAllRisultati();
				anagraficheBean.setArrRisultatiBeans(DataConverter.getRisultatiBean(listTabAnagRisultati));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_ANAG_RITARDI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_ANAG_RITARDI).getNumeVersione()){
				List<TabAnagRitardi> listTabAnagRitardi = service.getAllRitardi();
				anagraficheBean.setArrRitardiBeans(DataConverter.getAnagRitardiBean(listTabAnagRitardi));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_BANDE);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_BANDE).getNumeVersione()){
				List<TabBande> listBande = service.getAllBande();
				anagraficheBean.setArrBandeBeans(DataConverter.getBandeBean(listBande));
			}
			
			syncAnagBean = DataConverter.getBeanByNameTable(anagraficaBeanRequest.getListSyncAnagBean(), SyncAnagBean.TABLE_TAB_SISTEMI);
			if(syncAnagBean.getNumeVersione() == 0 || syncAnagBean.getNumeVersione() < service.getSyncAnagByNameTable(SyncAnagBean.TABLE_TAB_SISTEMI).getNumeVersione()){
				List<TabSistemi> listSistemi = service.getAllSistemi();
				anagraficheBean.setArrSistemiBeans(DataConverter.getSistemiBean(listSistemi));
			}
			
			gr.setResponseObject(anagraficheBean);
			gr.setResponseCode(Constants.RESPONSE_OK);
			return gr;
		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO getAnag "+e.getMessage());
			e.printStackTrace();
			gr.setResponseCode(Constants.ERROR_GENERIC);
			return gr;
		}
		
	}
	
	
}
