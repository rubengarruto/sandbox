package com.rextart.saw.rs;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.rs.bean.request.DettaglioSurveyDownloadBeanRequest;
import com.rextart.saw.rs.bean.request.NoteBean;
import com.rextart.saw.rs.bean.request.NoteCaratteristicheBean;
import com.rextart.saw.rs.bean.request.RiferimentiFirmaBean;
import com.rextart.saw.rs.bean.request.RisultatoSurveyBean;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;
import com.rextart.saw.rs.bean.response.DettaglioDownloadBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SurveyService;

@RequestMapping("/rest")
@Controller
public class DownloadSurveyDettaglio {

	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private SessioniService sessioniService;
	
	@RequestMapping(value = "/download/dettaglio", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse downloadDettaglio(@RequestBody DettaglioSurveyDownloadBeanRequest surveyDownload) {
		GeneralResponse gr = new GeneralResponse();
		try {
			UtilLog.printDownloadDettaglioSurvey(surveyDownload);
			
			DettaglioDownloadBean surveyCollaudoBean = new DettaglioDownloadBean();
			
			if(sessioniService.isSessioneStatoDisponibile(surveyDownload.getCodiSessione()) || sessioniService.isSessioneStatoInLavorazione(surveyDownload.getCodiSessione())){
				if(surveyDownload.getListCodiSurvey().size()>0){
					surveyCollaudoBean.setListSurveyDettaglio(DataConverter.getSurveyDettaglioBean(surveyService.getListDettagliBySurvey(surveyDownload.getListCodiSurvey())));
					surveyCollaudoBean.setListNoteCaratteristiche(DataConverter.getNoteCaratteristicheBean(surveyService.getListNoteCaratt(surveyDownload.getListCodiSurvey())));
					surveyCollaudoBean.setListRisultatoSurvey(DataConverter.getRisultatoSurveyBean(surveyService.getListRisultatiSurvey(surveyDownload.getListCodiSurvey())));
					surveyCollaudoBean.setListRiferimentiFirma(DataConverter.getRiferimentiFirmaBean(surveyService.getListRiferimentiFirma(surveyDownload.getListCodiSurvey())));
					surveyCollaudoBean.setListNote(DataConverter.getNoteBean(surveyService.getListNoteTablet((surveyDownload.getListCodiSurvey()))));
				}else{
					surveyCollaudoBean.setListSurveyDettaglio(new ArrayList<SurveyDettaglioBean>());
					surveyCollaudoBean.setListNoteCaratteristiche(new ArrayList<NoteCaratteristicheBean>());
					surveyCollaudoBean.setListRisultatoSurvey(new ArrayList<RisultatoSurveyBean>());
					surveyCollaudoBean.setListRiferimentiFirma(new ArrayList<RiferimentiFirmaBean>());
					surveyCollaudoBean.setListNote(new ArrayList<NoteBean>());
				}
				gr.setResponseObject(surveyCollaudoBean);
				gr.setResponseCode(Constants.RESPONSE_OK);
			}else{
				gr.setResponseCode(Constants.ERROR_SYNC_NO_MODIFY);
			}
			
			
			
					
		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO Survey downloadDettaglio "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}
}
