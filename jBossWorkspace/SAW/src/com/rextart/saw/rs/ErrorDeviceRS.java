package com.rextart.saw.rs;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.entity.TabErrorDevice;
import com.rextart.saw.rs.bean.request.ErrorDeviceBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.service.UtilityService;


@RequestMapping("/rest")
@Controller
public class ErrorDeviceRS {

	@Autowired
	private UtilityService utilityService;
	
	@RequestMapping(value = "/error/sendError", method = RequestMethod.POST, produces = { org.springframework.http.MediaType.APPLICATION_JSON_VALUE }, consumes = { org.springframework.http.MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public GeneralResponse errorDevice(@RequestBody ErrorDeviceBean errorDeviceBean) throws Exception {
		GeneralResponse gr = new GeneralResponse();

		try {
			utilityService.saveErrorDevice(DataConverter.getErrorDeviceEntity(errorDeviceBean));
			gr.setResponseCode(Constants.RESPONSE_OK);

		} catch (Exception e) {
			gr.setResponseCode(Constants.ERROR_GENERIC);
		}
		return gr;
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	@ResponseBody
	public GeneralResponse error() throws Exception {
		GeneralResponse gr = new GeneralResponse();
			
		try {
			List<TabErrorDevice> list = utilityService.getAll();
			TableData bean = new TableData();
			List<String> errorList = new ArrayList<String>();
			bean.setSection("ERRORE");
			for(TabErrorDevice error : list){
				errorList.add(error.getDescErrore());
			}
			gr.setResponseObject(errorList);

		} catch (Exception e) {
			gr.setResponseCode(Constants.ERROR_GENERIC);
		}
		return gr;
	}
}
class TableData{
	String section;
	List<String> data;
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<String> getData() {
		return data;
	}
	public void setData(List<String> data) {
		this.data = data;
	}
	
	
}
