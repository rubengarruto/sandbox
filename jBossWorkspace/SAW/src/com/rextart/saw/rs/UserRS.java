package com.rextart.saw.rs;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.rs.bean.request.UserRequest;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.service.DeviceService;
import com.rextart.saw.service.UserService;



@RequestMapping("/rest")
@Controller
public class UserRS {
	private static final int ACCEPT_USER = 1;
	private static final int RECOVERY_PASSWORD = 2;
	private static final int CHANGE_PASSWORD = 3;
	
    @Autowired
    private UserService userService;
    
    @Autowired
    private DeviceService deviceService;
	
	@RequestMapping(value = "/users/acceptUser", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse acceptUser(@RequestBody UserRequest request) {
		return getServiceUser(request, ACCEPT_USER);
	}

	@RequestMapping(value = "/users/recoveryPassword", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse recoveryPassword(@RequestBody UserRequest request) {
		return getServiceUser(request, RECOVERY_PASSWORD);
	}
	
	@RequestMapping(value = "/users/changePassword", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse changePassword(@RequestBody UserRequest request) {
		return getServiceUser(request, CHANGE_PASSWORD);
	}
	/**
	 * @description metodo di utility per evitare di riscrivere i primi controlli che si ripetono per tutti i servizi
	 * @param request
	 * @param servizio
	 * @return
	 */
	private GeneralResponse getServiceUser(UserRequest request, int servizio){
		GeneralResponse gr = new GeneralResponse();
		try {
			switch (servizio) {
			case ACCEPT_USER:
				checkForRegister(request, gr);
				break;
			}
			
		} catch (Exception e) {
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}
	
	/**
	 * @description metodo per riempire il bean dell'utente e controllare che questo esista e che sia gia associato a un tablet
	 * @param request
	 * @param gr
	 * @return UserBean
	 */
	private GeneralResponse checkForRegister(UserRequest request, GeneralResponse gr){
		TabUtente tabUtente = userService.authenticationUserWithLDAP(request.getUsername(), request.getPassword());
		if(tabUtente != null){
			if(!tabUtente.isFlgAbilitato()){
				gr.setResponseCode(Constants.ERROR_USER_NOT_ENABLE);
			}else{
				ViwUtenti viwUtenti = userService.getViewByUtente(tabUtente);
				gr.setResponseObject(DataConverter.getUserBeanByViwUtenti(viwUtenti,tabUtente));
				gr.setResponseCode(Constants.RESPONSE_OK);
			}
			
		}else{
			gr.setResponseCode(Constants.ERROR_USER_NOT_FOUND);
		}
		
		return gr;
	}
	
	
	public static int getAcceptUser() {
		return ACCEPT_USER;
	}
	
	
}
