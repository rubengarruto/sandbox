package com.rextart.saw.rs;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.openKM.ServiceRepositoryImpl;
import com.rextart.saw.rs.bean.request.SurveyBean;
import com.rextart.saw.rs.bean.request.UserRequest;
import com.rextart.saw.rs.bean.response.DocumentiDynBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.bean.response.InitializationBean;
import com.rextart.saw.rs.bean.response.SchedaDynBean;
import com.rextart.saw.rs.bean.response.SchedaProggettoBean;
import com.rextart.saw.rs.bean.response.SessioniBean;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SurveyService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.MethodUtilities;




@RequestMapping("/rest")
@Controller
public class SessioniRS {

	@Autowired
	private SessioniService sessioniService;

	@Autowired
	private RepositoryMediaService repositoryMediaService; 

	@Autowired
	private SurveyService surveyService;

	@RequestMapping(value = "/sessioni/listSessioniAll", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse listSessioniAll(@RequestBody UserRequest userRequest){
		GeneralResponse gr = new GeneralResponse();
		try {
			InitializationBean initializationBean = new InitializationBean();

			List<SessioniBean> listSessioniAll = DataConverter.getAllSessioniBean(sessioniService.getSessioniDownload());
			List<SessioniBean> listSessioniAssocUser = new ArrayList<SessioniBean>();
			List<Integer> listCodiSessoniAssoc = new ArrayList<Integer>();
			for(SessioniBean sessioniBean : listSessioniAll){
				if(sessioniService.isUserAssocSessione(userRequest.getCodiUtente(), sessioniBean.getCodiSessione())){
					listSessioniAssocUser.add(sessioniBean);
					listCodiSessoniAssoc.add(sessioniBean.getCodiSessione());
				}
			}

			List<SurveyBean> listSurvey = DataConverter.getListSurveyBean(surveyService.getListSurveyBySessione(listCodiSessoniAssoc));

			initializationBean.setListSessioni(listSessioniAssocUser);
			initializationBean.setListSurvey(listSurvey);
			initializationBean.setListDocumentiDynBean(new ArrayList<>());

			//-------
			//setto i documenti di pev
			//-------
			initializationBean.setListSchedaProggettoBean(new ArrayList<>());
			
			for (SessioniBean sessione: initializationBean.getListSessioni()) {
				try {
					if (userRequest.getHashCollaudiWithDocumentoPev()!=null) {
						
						//mi prendo i nomi dei file
						Integer tipoSchedaProgetto = MethodUtilities.getTipoSchedaProgetto();
						Integer tipoSchedaRadioEsecutiva = MethodUtilities.getTipoSchedaRadioEsecutiva();
						Integer tipoCheckList = MethodUtilities.getTipoCheckList();
						
						//mi prendo i documenti
						ArrayList<TabRepositoryDocument> reps = new ArrayList<>();
						
						//vedo se ho gi� preso la scheda progetto, nel caso non la riprendo
						if (!userRequest.getHashCollaudiWithDocumentoPev().get(Const.SCHEDA_PROGETTO).contains(sessione.getCodiSessione())) {
							reps.addAll (repositoryMediaService.getTabRepositoryDocumentByTipo(sessione.getCodiSessione(),tipoSchedaProgetto));
						}
						
						//simile per radio esecutiva e check list
						if (!userRequest.getHashCollaudiWithDocumentoPev().get(Const.SCHEDA_RADIO_ESECUTIVA).contains(sessione.getCodiSessione())) {
							reps.addAll( repositoryMediaService.getTabRepositoryDocumentByTipo(sessione.getCodiSessione(),tipoSchedaRadioEsecutiva));
						}
						if (!userRequest.getHashCollaudiWithDocumentoPev().get(Const.CHECK_LIST).contains(sessione.getCodiSessione())) {
							reps.addAll( repositoryMediaService.getTabRepositoryDocumentByTipo(sessione.getCodiSessione(),tipoCheckList));
						}
						
						for (TabRepositoryDocument rep: reps) {
							//tutti i documenti usano lo stesso schedaProgettoBean
							SchedaProggettoBean schedaProgettoBean= new SchedaProggettoBean();
							schedaProgettoBean.setDescFile(rep.getDescNome());
							schedaProgettoBean.setCodiAnagDocumenti(rep.getTipoFile());
							ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
							InputStream is = srI.download(rep) ;
							if(is!=null) {
								//schedaProgettoBean.setFile(IOUtils.toByteArray(is));
								schedaProgettoBean.setCodiSessione(sessione.getCodiSessione());
								initializationBean.getListSchedaProggettoBean().add(schedaProgettoBean);
							}
						}
							
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			//-------
			//setto i documenti di dyn
			//-------
			try {
				for (SessioniBean sessioniBean: initializationBean.getListSessioni()) {
					
					//setto il documento
					DocumentiDynBean documentiDynBean = new DocumentiDynBean();
					documentiDynBean.setSchedaRischiList(new ArrayList<>());
					List<TabRepositorySchedaRischi> tabRepositorySchedaRischiList= repositoryMediaService.getRepositoryDocumentDynByCodiSessione(sessioniBean.getCodiCollaudo());
					for (TabRepositorySchedaRischi tabRepositorySchedaRischi : tabRepositorySchedaRischiList) {
						documentiDynBean.getSchedaRischiList().add(DataConverter.getRepositorySchedaRischiBean(tabRepositorySchedaRischi));
					}
					initializationBean.getListDocumentiDynBean().add(documentiDynBean);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			//------
			//setto le informazioni di dyn
			//------
			List<SchedaDynBean> listSchedaDynBean=new ArrayList<>();
			try {
				listSchedaDynBean = DataConverter.getListScheDynBean(sessioniService.getSchedaDynByCodiSessioneList(initializationBean.getListSessioni()));
			} catch (Exception e){
			}
			initializationBean.setListSchedaDynBean(listSchedaDynBean);
			
			
			//-------
			//setto le informazioni sui verbali
			//-------
			try {
				initializationBean.setListVerbaleBean(DataConverter.getListVerbaleBean(sessioniService.getListVerbaliBySessioni(initializationBean.getListSessioni())));
			} catch (Exception e) {
				initializationBean.setListVerbaleBean(new ArrayList<>());
			}
			
			//-----
			//setto le info sui risultati dei survey
			//-----
			try {
				initializationBean.setListRisultatoSurvey(DataConverter.getRisultatoSurveyBean(sessioniService.getListRisultatoSurveyBySessioni(listSurvey)));
			} catch (Exception e) {
				e.printStackTrace();
				initializationBean.setListRisultatoSurvey(new ArrayList<>());
			}
			
			
			
			
			gr.setResponseObject(initializationBean);
			gr.setResponseCode(Constants.RESPONSE_OK);

		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO listSessioniAll "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}

	public StreamedContent downloadDocDyn(TabRepositorySchedaRischi rep){
		StreamedContent file = null;
		try {
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.downloadDocumentiDyn(rep) ;
			if(is!=null)
				file = new DefaultStreamedContent(is, rep.getDescMimeType(), rep.getDescNome());
			else
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione problemi di connessione con il repository. Riprovare pi� tardi"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
	
	@RequestMapping(value = "/sessioni/documentiDownload", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse documentiDownload(@RequestBody SchedaProggettoBean schedaProgettoBean){
		GeneralResponse gr = new GeneralResponse();
		try {
			TabRepositoryDocument tabRepositoryDocument = (TabRepositoryDocument) repositoryMediaService.getDocumentBySessioneAndNome(schedaProgettoBean.getCodiSessione(), schedaProgettoBean.getDescFile());
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.download(tabRepositoryDocument) ;
			if(is!=null) {
		    schedaProgettoBean.setFile(IOUtils.toByteArray(is));
			}
			gr.setResponseObject(schedaProgettoBean);
			gr.setResponseCode(Constants.RESPONSE_OK);
		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO documentiDownload "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		return gr;
	}

	
}
