package com.rextart.saw.rs.bean.response;

public class ServiziBean {
	 	private Integer codiServizio;
	    private String descNote;
	    private String descServizio;
	    private String descSigla;
	    
		public Integer getCodiServizio() {
			return codiServizio;
		}
		public void setCodiServizio(Integer codiServizio) {
			this.codiServizio = codiServizio;
		}
		public String getDescNote() {
			return descNote;
		}
		public void setDescNote(String descNote) {
			this.descNote = descNote;
		}
		public String getDescServizio() {
			return descServizio;
		}
		public void setDescServizio(String descServizio) {
			this.descServizio = descServizio;
		}
		public String getDescSigla() {
			return descSigla;
		}
		public void setDescSigla(String descSigla) {
			this.descSigla = descSigla;
		}
	    
	    
}
