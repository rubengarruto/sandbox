package com.rextart.saw.rs.bean.response;

public class OggettiBean {
	private Integer codiOggetto;
    private String descOggetto;
    private String descSigla;
    private String descNote;
    private Integer codiOggettoPadre;
    private String descGerarchia;
    //private Integer flagTarga;

    /*public Integer getFlagTarga() {
        return flagTarga;
    }
    public void setFlagTarga(Integer flagTarga) {
        this.flagTarga = flagTarga;
    }*/

    public Integer getCodiOggettoPadre() {
        return codiOggettoPadre;
    }

    public void setCodiOggettoPadre(Integer codiOggettoPadre) {
        this.codiOggettoPadre = codiOggettoPadre;
    }

    public String getDescGerarchia() {
        return descGerarchia;
    }

    public void setDescGerarchia(String descGerarchia) {
        this.descGerarchia = descGerarchia;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

    public String getDescOggetto() {
        return descOggetto;
    }

    public void setDescOggetto(String descOggetto) {
        this.descOggetto = descOggetto;
    }

    public String getDescSigla() {
        return descSigla;
    }

    public void setDescSigla(String descSigla) {
        this.descSigla = descSigla;
    }

    public String getDescNote() {
        return descNote;
    }

    public void setDescNote(String descNote) {
        this.descNote = descNote;
    }


}
