package com.rextart.saw.rs.bean.request;

import java.util.List;



public class AnagraficaBeanRequest {
	private Integer codiUtente;
    private String descImei;
    private List<SyncAnagBean> listSyncAnagBean;
    
    
   	public List<SyncAnagBean> getListSyncAnagBean() {
   		return listSyncAnagBean;
   	}

   	public void setListSyncAnagBean(List<SyncAnagBean> listSyncAnagBean) {
   		this.listSyncAnagBean = listSyncAnagBean;
   	}

	public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public String getDescImei() {
        return descImei;
    }

    public void setDescImei(String descImei) {
        this.descImei = descImei;
    }
}
