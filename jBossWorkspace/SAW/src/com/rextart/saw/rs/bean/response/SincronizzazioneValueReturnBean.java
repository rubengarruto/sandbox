package com.rextart.saw.rs.bean.response;

public class SincronizzazioneValueReturnBean {

	private Integer codiSessione;
	
	private Integer codiOggetto;
	
	private VerbaleBean verbaleBean;

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public VerbaleBean getVerbaleBean() {
		return verbaleBean;
	}

	public void setVerbaleBean(VerbaleBean verbaleBean) {
		this.verbaleBean = verbaleBean;
	}

	
	
	
}
