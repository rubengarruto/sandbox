package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.rs.bean.request.RisultatoSurveyBean;
import com.rextart.saw.rs.bean.request.SurveyBean;

public class InitializationBean {

	private List<SessioniBean> listSessioni;
	
	private List<SchedaProggettoBean> listSchedaProggettoBean;
	
	private List<DocumentiDynBean> listDocumentiDynBean;
	
	private List<SurveyBean> listSurvey;
	
	private List<VerbaleBean> listVerbaleBean;
	
	private List<SchedaDynBean> listSchedaDynBean;
	
	private List<RisultatoSurveyBean> listRisultatoSurvey;

	
	
	public List<SessioniBean> getListSessioni() {
		return listSessioni;
	}

	public void setListSessioni(List<SessioniBean> listSessioni) {
		this.listSessioni = listSessioni;
	}

	public List<SurveyBean> getListSurvey() {
		return listSurvey;
	}

	public void setListSurvey(List<SurveyBean> listSurvey) {
		this.listSurvey = listSurvey;
	}

	public List<DocumentiDynBean> getListDocumentiDynBean() {
		return listDocumentiDynBean;
	}

	public void setListDocumentiDynBean(List<DocumentiDynBean> listDocumentiDynBean) {
		this.listDocumentiDynBean = listDocumentiDynBean;
	}

	public List<SchedaProggettoBean> getListSchedaProggettoBean() {
		return listSchedaProggettoBean;
	}

	public void setListSchedaProggettoBean(
			List<SchedaProggettoBean> listSchedaProggettoBean) {
		this.listSchedaProggettoBean = listSchedaProggettoBean;
	}

	public List<VerbaleBean> getListVerbaleBean() {
		return listVerbaleBean;
	}

	public void setListVerbaleBean(List<VerbaleBean> listVerbaleBean) {
		this.listVerbaleBean = listVerbaleBean;
	}

	public List<SchedaDynBean> getListSchedaDynBean() {
		return listSchedaDynBean;
	}

	public void setListSchedaDynBean(List<SchedaDynBean> listSchedaDynBean) {
		this.listSchedaDynBean = listSchedaDynBean;
	}

	public List<RisultatoSurveyBean> getListRisultatoSurvey() {
		return listRisultatoSurvey;
	}

	public void setListRisultatoSurvey(List<RisultatoSurveyBean> listRisultatoSurvey) {
		this.listRisultatoSurvey = listRisultatoSurvey;
	}

	

	

	
	
	
}
