package com.rextart.saw.rs.bean.response;

import java.util.List;



public class DocumentiDynBean {
	
	private List<RepositorySchedaRischiBean> schedaRischiList;
	
	public List<RepositorySchedaRischiBean> getSchedaRischiList() {
		return schedaRischiList;
	}
	public void setSchedaRischiList(List<RepositorySchedaRischiBean> schedaRischiList) {
		this.schedaRischiList = schedaRischiList;
	}
	


}
