package com.rextart.saw.rs.bean.request;


import java.io.Serializable;
import java.util.List;

/**
 * Created by Giovanni on 30/11/2015.
 */
public class SincronizzazioneBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private List<SurveyDettaglioBean> surveyDettaglioBeans;
    private List<RiferimentiFirmaBean> riferimentiFirmaBeans;
    private List<RisultatoSurveyBean> risultatoSurveyBeans;
    private List<NoteCaratteristicheBean> noteCaratteristicheBeans;
    private List<RelRitardiSessioniBean> relRitardiSessioniBeans;
    private List<NoteBean> listNoteBeans;
    private String descImei;
    private Integer codiUtente;
    private Integer codiSessione;
    private Integer numeVersioneApp;
    private Integer codiOggetto;
    private boolean flagLock;
    private boolean isLastCall;
    private List<Integer> listCodiSurveyAll;
    private boolean flgParziale;

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescImei() {
        return descImei;
    }

    public void setDescImei(String descImei) {
        this.descImei = descImei;
    }
	
   
	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getNumeVersioneApp() {
		return numeVersioneApp;
	}

	public void setNumeVersioneApp(Integer numeVersioneApp) {
		this.numeVersioneApp = numeVersioneApp;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public boolean isFlagLock() {
		return flagLock;
	}

	public void setFlagLock(boolean flagLock) {
		this.flagLock = flagLock;
	}

	public List<SurveyDettaglioBean> getSurveyDettaglioBeans() {
		return surveyDettaglioBeans;
	}

	public void setSurveyDettaglioBeans(
			List<SurveyDettaglioBean> surveyDettaglioBeans) {
		this.surveyDettaglioBeans = surveyDettaglioBeans;
	}

	public boolean isLastCall() {
		return isLastCall;
	}

	public void setLastCall(boolean isLastCall) {
		this.isLastCall = isLastCall;
	}

	public List<RiferimentiFirmaBean> getRiferimentiFirmaBeans() {
		return riferimentiFirmaBeans;
	}

	public void setRiferimentiFirmaBeans(List<RiferimentiFirmaBean> riferimentiFirmaBeans) {
		this.riferimentiFirmaBeans = riferimentiFirmaBeans;
	}

	public List<RisultatoSurveyBean> getRisultatoSurveyBeans() {
		return risultatoSurveyBeans;
	}

	public void setRisultatoSurveyBeans(
			List<RisultatoSurveyBean> risultatoSurveyBeans) {
		this.risultatoSurveyBeans = risultatoSurveyBeans;
	}

	public List<NoteCaratteristicheBean> getNoteCaratteristicheBeans() {
		return noteCaratteristicheBeans;
	}

	public void setNoteCaratteristicheBeans(
			List<NoteCaratteristicheBean> noteCaratteristicheBeans) {
		this.noteCaratteristicheBeans = noteCaratteristicheBeans;
	}

	public List<RelRitardiSessioniBean> getRelRitardiSessioniBeans() {
		return relRitardiSessioniBeans;
	}

	public void setRelRitardiSessioniBeans(
			List<RelRitardiSessioniBean> relRitardiSessioniBeans) {
		this.relRitardiSessioniBeans = relRitardiSessioniBeans;
	}

	public List<Integer> getListCodiSurveyAll() {
		return listCodiSurveyAll;
	}

	public void setListCodiSurveyAll(List<Integer> listCodiSurveyAll) {
		this.listCodiSurveyAll = listCodiSurveyAll;
	}

	public boolean isFlgParziale() {
		return flgParziale;
	}

	public void setFlgParziale(boolean flgParziale) {
		this.flgParziale = flgParziale;
	}

	public List<NoteBean> getListNoteBeans() {
		return listNoteBeans;
	}

	public void setListNoteBeans(List<NoteBean> listNoteBeans) {
		this.listNoteBeans = listNoteBeans;
	}

	
	
}
