package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.entity.TabNote;

public class VerbaleBean {
	

    private Integer codiSessione;

    private Integer codiOggetto;

    private Integer flgEsito;

    private Integer codiRisultato;

    private Integer flgCollaudoDittaTerminato;
    
    private String descOggetto;
    
    private List<TabNote> listTabNote;

	
	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getFlgEsito() {
		return flgEsito;
	}

	public void setFlgEsito(Integer flgEsito) {
		this.flgEsito = flgEsito;
	}

	public Integer getCodiRisultato() {
		return codiRisultato;
	}

	public void setCodiRisultato(Integer codiRisultato) {
		this.codiRisultato = codiRisultato;
	}

	public Integer getFlgCollaudoDittaTerminato() {
		return flgCollaudoDittaTerminato;
	}

	public void setFlgCollaudoDittaTerminato(Integer flgCollaudoDittaTerminato) {
		this.flgCollaudoDittaTerminato = flgCollaudoDittaTerminato;
	}

	public String getDescOggetto() {
		return descOggetto;
	}

	public void setDescOggetto(String descOggetto) {
		this.descOggetto = descOggetto;
	}

	public List<TabNote> getListTabNote() {
		return listTabNote;
	}

	public void setListTabNote(List<TabNote> listTabNote) {
		this.listTabNote = listTabNote;
	}

	
    

}
