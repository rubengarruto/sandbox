package com.rextart.saw.rs.bean.response;




public class SchedaDynBean {

	private Integer codiSessione;
	
	private String accessibilita;
	
    private String codiceMazzoChiavi;

	private String dataCreazioneScheda;
	
	private String disagiato;

	private String fasciaIntervento;
	
	private String itinerario;

	private String noteAccesso;

	private String noteAccessibilita;

	private String regime;

	private String tipoAccesso;
	
	private String versione;

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getAccessibilita() {
		return accessibilita;
	}

	public void setAccessibilita(String accessibilita) {
		this.accessibilita = accessibilita;
	}

	public String getCodiceMazzoChiavi() {
		return codiceMazzoChiavi;
	}

	public void setCodiceMazzoChiavi(String codiceMazzoChiavi) {
		this.codiceMazzoChiavi = codiceMazzoChiavi;
	}

	public String getDataCreazioneScheda() {
		return dataCreazioneScheda;
	}

	public void setDataCreazioneScheda(String dataCreazioneScheda) {
		this.dataCreazioneScheda = dataCreazioneScheda;
	}

	public String getDisagiato() {
		return disagiato;
	}

	public void setDisagiato(String disagiato) {
		this.disagiato = disagiato;
	}

	public String getFasciaIntervento() {
		return fasciaIntervento;
	}

	public void setFasciaIntervento(String fasciaIntervento) {
		this.fasciaIntervento = fasciaIntervento;
	}

	public String getItinerario() {
		return itinerario;
	}

	public void setItinerario(String itinerario) {
		this.itinerario = itinerario;
	}

	public String getNoteAccesso() {
		return noteAccesso;
	}

	public void setNoteAccesso(String noteAccesso) {
		this.noteAccesso = noteAccesso;
	}

	public String getNoteAccessibilita() {
		return noteAccessibilita;
	}

	public void setNoteAccessibilita(String noteAccessibilita) {
		this.noteAccessibilita = noteAccessibilita;
	}

	public String getRegime() {
		return regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public String getTipoAccesso() {
		return tipoAccesso;
	}

	public void setTipoAccesso(String tipoAccesso) {
		this.tipoAccesso = tipoAccesso;
	}

	public String getVersione() {
		return versione;
	}

	public void setVersione(String versione) {
		this.versione = versione;
	}
	
	
	
	
}
