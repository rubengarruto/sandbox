package com.rextart.saw.rs.bean.response;

public class BandeBean {
	private Integer codiBanda;
    private String descBanda;

    public Integer getCodiBanda() {
        return codiBanda;
    }

    public void setCodiBanda(Integer codiBanda) {
        this.codiBanda = codiBanda;
    }

    public String getDescBanda() {
        return descBanda;
    }

    public void setDescBanda(String descBanda) {
        this.descBanda = descBanda;
    }
}
