package com.rextart.saw.rs.bean.response;

public class SchedaProggettoBean {

	private int codiSessione;
    private byte[] file;
    private String descFile;
    private Integer codiAnagDocumenti;



    public int getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(int codiSessione) {
        this.codiSessione = codiSessione;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getDescFile() {
        return descFile;
    }

    public void setDescFile(String descFile) {
        this.descFile = descFile;
    }
	
    public int getCodiAnagDocumenti() {
        return codiAnagDocumenti;
    }

    public void setCodiAnagDocumenti(int codiAnagDocumenti) {
        this.codiAnagDocumenti = codiAnagDocumenti;
    }
}
