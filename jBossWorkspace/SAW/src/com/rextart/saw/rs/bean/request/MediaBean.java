package com.rextart.saw.rs.bean.request;

public class MediaBean {
	    private Integer codiSessione;
	    private Integer codiUtente;
	    private String fileName;
	    private String mimeType;
	    private String descMedia;
	    private String descSize;
	    private String descCategoria;

		
		public Integer getCodiSessione() {
			return codiSessione;
		}
		public void setCodiSessione(Integer codiSessione) {
			this.codiSessione = codiSessione;
		}
		public Integer getCodiUtente() {
			return codiUtente;
		}
		public void setCodiUtente(Integer codiUtente) {
			this.codiUtente = codiUtente;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getMimeType() {
			return mimeType;
		}
		public void setMimeType(String mimeType) {
			this.mimeType = mimeType;
		}
		public String getDescMedia() {
			return descMedia;
		}
		public void setDescMedia(String descMedia) {
			this.descMedia = descMedia;
		}
		public String getDescSize() {
			return descSize;
		}
		public void setDescSize(String descSize) {
			this.descSize = descSize;
		}
		public String getDescCategoria() {
			return descCategoria;
		}
		public void setDescCategoria(String descCategoria) {
			this.descCategoria = descCategoria;
		}
	
		
	
	
}
