package com.rextart.saw.rs.bean.request;


public class NoteCaratteristicheBean {

    private Integer codiNoteCaratt;
    private Integer codiCaratteristica;
    private Integer codiSessione;
    private Integer codiSurvey;
    private String descNote;
    private Integer codiOggetto;
    private Integer codiUtente;

    public Integer getCodiNoteCaratt() {
        return codiNoteCaratt;
    }

    public void setCodiNoteCaratt(Integer codiNoteCaratt) {
        this.codiNoteCaratt = codiNoteCaratt;
    }

    public Integer getCodiCaratteristica() {
        return codiCaratteristica;
    }

    public void setCodiCaratteristica(Integer codiCaratteristica) {
        this.codiCaratteristica = codiCaratteristica;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

    public String getDescNote() {
        return descNote;
    }

    public void setDescNote(String descNote) {
        this.descNote = descNote;
    }

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}
    
    

}
