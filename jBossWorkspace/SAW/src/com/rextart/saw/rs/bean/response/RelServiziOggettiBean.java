package com.rextart.saw.rs.bean.response;

public class RelServiziOggettiBean {

	private Integer codiRelServOggetto;

    private Integer codiOggetto;

    private Integer codiServizio;

	public Integer getCodiRelServOggetto() {
		return codiRelServOggetto;
	}

	public void setCodiRelServOggetto(Integer codiRelServOggetto) {
		this.codiRelServOggetto = codiRelServOggetto;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiServizio() {
		return codiServizio;
	}

	public void setCodiServizio(Integer codiServizio) {
		this.codiServizio = codiServizio;
	}
    
}
