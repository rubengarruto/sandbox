package com.rextart.saw.rs.bean.request;

public class NoteBean {
	
 	private Integer idNota;
 	private String descNota;
 	private Integer codiSessione;
 	private Boolean flgTablet;
 	private String descUsername;
 	private Integer codiSurvey;
 	private Integer codiOggetto;
 	
	public Integer getIdNota() {
		return idNota;
	}
	public void setIdNota(Integer idNota) {
		this.idNota = idNota;
	}
	public String getDescNota() {
		return descNota;
	}
	public void setDescNota(String descNota) {
		this.descNota = descNota;
	}
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public Boolean getFlgTablet() {
		return flgTablet;
	}
	public void setFlgTablet(Boolean flgTablet) {
		this.flgTablet = flgTablet;
	}
	public String getDescUsername() {
		return descUsername;
	}
	public void setDescUsername(String descUsername) {
		this.descUsername = descUsername;
	}
	public Integer getCodiSurvey() {
		return codiSurvey;
	}
	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}
	public Integer getCodiOggetto() {
		return codiOggetto;
	}
	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

}
