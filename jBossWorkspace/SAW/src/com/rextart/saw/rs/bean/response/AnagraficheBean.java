package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.rs.bean.request.SyncAnagBean;


public class AnagraficheBean {

    private List<AnagLovBean> arrLovList;
    private List<CaratteristicheBean> arrCaratteristiche;
    private List<ElementiBean> arrElementi;
    private List<OggettiBean> arrOggetti;
    private List <RelCaratteristicheLovBean> arrRelCaratteristicheLovBeans;
    private List <RelOggettiElementiBean> arrRelOggettiElementiBeans;
    private List <RelElementiCarattBean>  arrRelElementiCarattBeans;
    private List<TipologieOggettoBean> arrTipologieOggettoBeans;
    
    private List<ServiziBean> arrServiziBeans;
    private List<RelServiziOggettiBean> arrRelServiziOggettiBeans;
    private List<RelOggettiTipoBean> arrRelOggettiTipoBeans;
    private List<StrutturaBean> arrStrutturaBeans;
    private List<SyncAnagBean> listSyncAnagraficas;
    private List<AnagStatiSessioneBean> arrStatiSessione;
    
    private List<AnagAntenneBean> arrAntenneBeans;
    private List<AnagDiplexerBean> arrDiplexerBeans;
    private List<AnagTmaBean> arrTmaBeans;
    private List<AnagRisultatiBean> arrRisultatiBeans;
    private List<AnagRitardiBean> arrRitardiBeans;
    private List<BandeBean> arrBandeBeans;
    private List<SistemiBean> arrSistemiBeans;
    
	public List<AnagLovBean> getArrLovList() {
		return arrLovList;
	}
	public void setArrLovList(List<AnagLovBean> arrLovList) {
		this.arrLovList = arrLovList;
	}
	public List<CaratteristicheBean> getArrCaratteristiche() {
		return arrCaratteristiche;
	}
	public void setArrCaratteristiche(List<CaratteristicheBean> arrCaratteristiche) {
		this.arrCaratteristiche = arrCaratteristiche;
	}
	public List<ElementiBean> getArrElementi() {
		return arrElementi;
	}
	public void setArrElementi(List<ElementiBean> arrElementi) {
		this.arrElementi = arrElementi;
	}
	public List<OggettiBean> getArrOggetti() {
		return arrOggetti;
	}
	public void setArrOggetti(List<OggettiBean> arrOggetti) {
		this.arrOggetti = arrOggetti;
	}
	public List<RelCaratteristicheLovBean> getArrRelCaratteristicheLovBeans() {
		return arrRelCaratteristicheLovBeans;
	}
	public void setArrRelCaratteristicheLovBeans(
			List<RelCaratteristicheLovBean> arrRelCaratteristicheLovBeans) {
		this.arrRelCaratteristicheLovBeans = arrRelCaratteristicheLovBeans;
	}
	public List<RelOggettiElementiBean> getArrRelOggettiElementiBeans() {
		return arrRelOggettiElementiBeans;
	}
	public void setArrRelOggettiElementiBeans(
			List<RelOggettiElementiBean> arrRelOggettiElementiBeans) {
		this.arrRelOggettiElementiBeans = arrRelOggettiElementiBeans;
	}
	public List<RelElementiCarattBean> getArrRelElementiCarattBeans() {
		return arrRelElementiCarattBeans;
	}
	public void setArrRelElementiCarattBeans(
			List<RelElementiCarattBean> arrRelElementiCarattBeans) {
		this.arrRelElementiCarattBeans = arrRelElementiCarattBeans;
	}
	public List<TipologieOggettoBean> getArrTipologieOggettoBeans() {
		return arrTipologieOggettoBeans;
	}
	public void setArrTipologieOggettoBeans(
			List<TipologieOggettoBean> arrTipologieOggettoBeans) {
		this.arrTipologieOggettoBeans = arrTipologieOggettoBeans;
	}
	public List<ServiziBean> getArrServiziBeans() {
		return arrServiziBeans;
	}
	public void setArrServiziBeans(List<ServiziBean> arrServiziBeans) {
		this.arrServiziBeans = arrServiziBeans;
	}
	public List<RelServiziOggettiBean> getArrRelServiziOggettiBeans() {
		return arrRelServiziOggettiBeans;
	}
	public void setArrRelServiziOggettiBeans(
			List<RelServiziOggettiBean> arrRelServiziOggettiBeans) {
		this.arrRelServiziOggettiBeans = arrRelServiziOggettiBeans;
	}
	public List<RelOggettiTipoBean> getArrRelOggettiTipoBeans() {
		return arrRelOggettiTipoBeans;
	}
	public void setArrRelOggettiTipoBeans(
			List<RelOggettiTipoBean> arrRelOggettiTipoBeans) {
		this.arrRelOggettiTipoBeans = arrRelOggettiTipoBeans;
	}
	public List<StrutturaBean> getArrStrutturaBeans() {
		return arrStrutturaBeans;
	}
	public void setArrStrutturaBeans(List<StrutturaBean> arrStrutturaBeans) {
		this.arrStrutturaBeans = arrStrutturaBeans;
	}
	public List<SyncAnagBean> getListSyncAnagraficas() {
		return listSyncAnagraficas;
	}
	public void setListSyncAnagraficas(List<SyncAnagBean> listSyncAnagraficas) {
		this.listSyncAnagraficas = listSyncAnagraficas;
	}
	public List<AnagStatiSessioneBean> getArrStatiSessione() {
		return arrStatiSessione;
	}
	public void setArrStatiSessione(List<AnagStatiSessioneBean> arrStatiSessione) {
		this.arrStatiSessione = arrStatiSessione;
	}
	public List<AnagAntenneBean> getArrAntenneBeans() {
		return arrAntenneBeans;
	}
	public void setArrAntenneBeans(List<AnagAntenneBean> arrAntenneBeans) {
		this.arrAntenneBeans = arrAntenneBeans;
	}
	public List<AnagDiplexerBean> getArrDiplexerBeans() {
		return arrDiplexerBeans;
	}
	public void setArrDiplexerBeans(List<AnagDiplexerBean> arrDiplexerBeans) {
		this.arrDiplexerBeans = arrDiplexerBeans;
	}
	public List<AnagTmaBean> getArrTmaBeans() {
		return arrTmaBeans;
	}
	public void setArrTmaBeans(List<AnagTmaBean> arrTmaBeans) {
		this.arrTmaBeans = arrTmaBeans;
	}
	public List<AnagRisultatiBean> getArrRisultatiBeans() {
		return arrRisultatiBeans;
	}
	public void setArrRisultatiBeans(List<AnagRisultatiBean> arrRisultatiBeans) {
		this.arrRisultatiBeans = arrRisultatiBeans;
	}
	public List<AnagRitardiBean> getArrRitardiBeans() {
		return arrRitardiBeans;
	}
	public void setArrRitardiBeans(List<AnagRitardiBean> arrRitardiBeans) {
		this.arrRitardiBeans = arrRitardiBeans;
	}
	public List<BandeBean> getArrBandeBeans() {
		return arrBandeBeans;
	}
	public void setArrBandeBeans(List<BandeBean> arrBandeBeans) {
		this.arrBandeBeans = arrBandeBeans;
	}
	public List<SistemiBean> getArrSistemiBeans() {
		return arrSistemiBeans;
	}
	public void setArrSistemiBeans(List<SistemiBean> arrSistemiBeans) {
		this.arrSistemiBeans = arrSistemiBeans;
	}
   
	
    
}
