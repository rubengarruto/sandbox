package com.rextart.saw.rs.bean.response;



public class RepositorySchedaRischiBean {
	private Integer codiSessione;

	private long timeCreate;

	private String descMimeType;

	private String descNome;

	private String descPath;

	private Integer flagSchedaRischi;
	
	private String versione;

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public long getTimeCreate() {
		return timeCreate;
	}

	public void setTimeCreate(long timeCreate) {
		this.timeCreate = timeCreate;
	}

	public String getDescMimeType() {
		return descMimeType;
	}

	public void setDescMimeType(String descMimeType) {
		this.descMimeType = descMimeType;
	}

	public String getDescNome() {
		return descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPath() {
		return descPath;
	}

	public void setDescPath(String descPath) {
		this.descPath = descPath;
	}

	public Integer getFlagSchedaRischi() {
		return flagSchedaRischi;
	}

	public void setFlagSchedaRischi(Integer flagSchedaRischi) {
		this.flagSchedaRischi = flagSchedaRischi;
	}

	public String getVersione() {
		return versione;
	}

	public void setVersione(String versione) {
		this.versione = versione;
	}
	
	

}
