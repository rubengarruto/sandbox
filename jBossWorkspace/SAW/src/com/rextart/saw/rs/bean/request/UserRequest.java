package com.rextart.saw.rs.bean.request;

import java.util.HashMap;
import java.util.List;


public class UserRequest {

	private String username;
	private String password;
	private String imei;
	private Integer codiUtente;
    private HashMap<Integer, List<Integer>> hashCollaudiWithDocumentoPev;

	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public Integer getCodiUtente() {
		return codiUtente;
	}
	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}
	public HashMap<Integer, List<Integer>> getHashCollaudiWithDocumentoPev() {
		return hashCollaudiWithDocumentoPev;
	}
	public void setHashCollaudiWithDocumentoPev(
			HashMap<Integer, List<Integer>> hashCollaudiWithDocumentoPev) {
		this.hashCollaudiWithDocumentoPev = hashCollaudiWithDocumentoPev;
	}
	
	
	
	
	
	
}
