package com.rextart.saw.rs.bean.response;

public class DocumentoDynBean {

	private byte[] file;

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
