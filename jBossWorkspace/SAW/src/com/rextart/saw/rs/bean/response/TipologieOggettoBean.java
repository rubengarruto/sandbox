package com.rextart.saw.rs.bean.response;

public class TipologieOggettoBean {
	 private Integer codiTipoOggetto;
	    private String descTipoOggetto;
	    private String descNote;
	    private String descLabelFilterTipoOggetto;
	    private Integer flgDitta;

	    public Integer getCodiTipoOggetto() {
	        return codiTipoOggetto;
	    }

	    public void setCodiTipoOggetto(Integer codiTipoOggetto) {
	        this.codiTipoOggetto = codiTipoOggetto;
	    }

	    public String getDescTipoOggetto() {
	        return descTipoOggetto;
	    }

	    public void setDescTipoOggetto(String descTipoOggetto) {
	        this.descTipoOggetto = descTipoOggetto;
	    }

	    public String getDescNote() {
	        return descNote;
	    }

	    public void setDescNote(String descNote) {
	        this.descNote = descNote;
	    }

	    public String getDescLabelFilterTipoOggetto() {
	        return descLabelFilterTipoOggetto;
	    }

	    public void setDescLabelFilterTipoOggetto(String descLabelFilterTipoOggetto) {
	        this.descLabelFilterTipoOggetto = descLabelFilterTipoOggetto;
	    }

		public Integer getFlgDitta() {
			return flgDitta;
		}

		public void setFlgDitta(Integer flgDitta) {
			this.flgDitta = flgDitta;
		}
	
	    
}
