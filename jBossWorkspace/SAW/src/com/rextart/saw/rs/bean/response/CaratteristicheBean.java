package com.rextart.saw.rs.bean.response;


public class CaratteristicheBean {
	private Integer codiCaratteristica;
    private String descCaratteristica;
    private Integer numeSettore;
    private Integer numeVia;
    private String memoHelp;
    private Integer numeCella;
    private String descComponente;
    private Integer flagObbligatorio;
    private String descLabelCarat1;
    private String descLabelCarat2;
    private String descLabelCarat3;
    private String descLabelCarat4;
    private String descLabelCarat5;
    private Integer flagLov1;
    private Integer flagLov2;
    private Integer flagLov3;
    private Integer flagLov4;
    private Integer flagLov5;
    private Integer numeE;
    private Integer numeBranch;
    private Integer flagNote;
    
	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}
	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	
	public String getMemoHelp() {
		return memoHelp;
	}
	public void setMemoHelp(String memoHelp) {
		this.memoHelp = memoHelp;
	}
	
	public String getDescComponente() {
		return descComponente;
	}
	public void setDescComponente(String descComponente) {
		this.descComponente = descComponente;
	}
	public Integer getFlagObbligatorio() {
		return flagObbligatorio;
	}
	public void setFlagObbligatorio(Integer flagObbligatorio) {
		this.flagObbligatorio = flagObbligatorio;
	}
	public String getDescLabelCarat1() {
		return descLabelCarat1;
	}
	public void setDescLabelCarat1(String descLabelCarat1) {
		this.descLabelCarat1 = descLabelCarat1;
	}
	public String getDescLabelCarat2() {
		return descLabelCarat2;
	}
	public void setDescLabelCarat2(String descLabelCarat2) {
		this.descLabelCarat2 = descLabelCarat2;
	}
	public String getDescLabelCarat3() {
		return descLabelCarat3;
	}
	public void setDescLabelCarat3(String descLabelCarat3) {
		this.descLabelCarat3 = descLabelCarat3;
	}
	public String getDescLabelCarat4() {
		return descLabelCarat4;
	}
	public void setDescLabelCarat4(String descLabelCarat4) {
		this.descLabelCarat4 = descLabelCarat4;
	}
	public String getDescLabelCarat5() {
		return descLabelCarat5;
	}
	public void setDescLabelCarat5(String descLabelCarat5) {
		this.descLabelCarat5 = descLabelCarat5;
	}
	public Integer getFlagLov1() {
		return flagLov1;
	}
	public void setFlagLov1(Integer flagLov1) {
		this.flagLov1 = flagLov1;
	}
	public Integer getFlagLov2() {
		return flagLov2;
	}
	public void setFlagLov2(Integer flagLov2) {
		this.flagLov2 = flagLov2;
	}
	public Integer getFlagLov3() {
		return flagLov3;
	}
	public void setFlagLov3(Integer flagLov3) {
		this.flagLov3 = flagLov3;
	}
	public Integer getFlagLov4() {
		return flagLov4;
	}
	public void setFlagLov4(Integer flagLov4) {
		this.flagLov4 = flagLov4;
	}
	public Integer getFlagLov5() {
		return flagLov5;
	}
	public void setFlagLov5(Integer flagLov5) {
		this.flagLov5 = flagLov5;
	}
	public Integer getNumeSettore() {
		return numeSettore;
	}
	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}
	public Integer getNumeVia() {
		return numeVia;
	}
	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}
	public Integer getNumeCella() {
		return numeCella;
	}
	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}
	public Integer getNumeE() {
		return numeE;
	}
	public void setNumeE(Integer numeE) {
		this.numeE = numeE;
	}
	public Integer getNumeBranch() {
		return numeBranch;
	}
	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}
	public Integer getFlagNote() {
		return flagNote;
	}
	public void setFlagNote(Integer flagNote) {
		this.flagNote = flagNote;
	}
	
    
}
