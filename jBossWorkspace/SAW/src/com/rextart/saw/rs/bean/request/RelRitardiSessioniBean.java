package com.rextart.saw.rs.bean.request;

public class RelRitardiSessioniBean {
	
	 	private Integer codiRelRitardiSessioni;
	    private Integer codiSessione;
	    private Integer codiAnagRitardo;

	    public Integer getCodiRelRitardiSessioni() {
	        return codiRelRitardiSessioni;
	    }

	    public void setCodiRelRitardiSessioni(Integer codiRelRitardiSessioni) {
	        this.codiRelRitardiSessioni = codiRelRitardiSessioni;
	    }

	    public Integer getSessione() {
	        return codiSessione;
	    }

	    public void setCodiSessione(Integer codiRisultatoSessione) {
	        this.codiSessione = codiRisultatoSessione;
	    }

	    public Integer getCodiAnagRitardo() {
	        return codiAnagRitardo;
	    }

	    public void setCodiAnagRitardo(Integer codiAnagRitardo) {
	        this.codiAnagRitardo = codiAnagRitardo;
	    }

		public Integer getCodiSessione() {
			return codiSessione;
		}
	    
	    
}
