package com.rextart.saw.rs.bean.response;

public class StrutturaBean {
	
  	private Integer codiStruttura;
    private String descStruttura;

    public Integer getCodiStruttura() {
        return codiStruttura;
    }

    public void setCodiStruttura(Integer codiStruttura) {
        this.codiStruttura = codiStruttura;
    }

    public String getDescStruttura() {
        return descStruttura;
    }

    public void setDescStruttura(String descStruttura) {
        this.descStruttura = descStruttura;
    }

}
