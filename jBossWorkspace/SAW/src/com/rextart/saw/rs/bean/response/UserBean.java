package com.rextart.saw.rs.bean.response;


public class UserBean {

	private Integer codiUtente;

	private String descCgn;

	private String descEmail;

	private String descImei;

	private String descNome;

	private String descPwd;

	private String descUser;

	private String descNumeroSim;
	
	private String descImpresa;
	
	private boolean flgAbilitato = false;

	private boolean flgCessSosp;
	
	private boolean flgIsAdmin = false;

	private boolean flgReceiveEmail;

	private boolean flgReset = false;

	private boolean flgTablet = false;

	private long timeCeased;

	private long timeCreazione;

	private long timeDisable;

	private long timeEnable;

	private long timeSuspended;

	private long timeSuspension;
	
	private Integer codiAreaComp;
	
	private Integer codiStruttura;
	
	private Integer codiRole;
	
	private Integer flgImpresa;
	
	private Integer codiVendor;



	public int getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(int codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescCgn() {
		return descCgn;
	}

	public void setDescCgn(String descCgn) {
		this.descCgn = descCgn;
	}

	public String getDescEmail() {
		return descEmail;
	}

	public void setDescEmail(String descEmail) {
		this.descEmail = descEmail;
	}

	public String getDescImei() {
		return descImei;
	}

	public void setDescImei(String descImei) {
		this.descImei = descImei;
	}

	public String getDescNome() {
		return descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPwd() {
		return descPwd;
	}

	public void setDescPwd(String descPwd) {
		this.descPwd = descPwd;
	}

	public String getDescUser() {
		return descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public boolean isFlgAbilitato() {
		return flgAbilitato;
	}

	public void setFlgAbilitato(boolean flgAbilitato) {
		this.flgAbilitato = flgAbilitato;
	}

	public boolean isFlgCessSosp() {
		return flgCessSosp;
	}

	public void setFlgCessSosp(boolean flgCessSosp) {
		this.flgCessSosp = flgCessSosp;
	}

	public boolean isFlgIsAdmin() {
		return flgIsAdmin;
	}

	public void setFlgIsAdmin(boolean flgIsAdmin) {
		this.flgIsAdmin = flgIsAdmin;
	}

	public boolean isFlgReceiveEmail() {
		return flgReceiveEmail;
	}

	public void setFlgReceiveEmail(boolean flgReceiveEmail) {
		this.flgReceiveEmail = flgReceiveEmail;
	}

	public boolean isFlgReset() {
		return flgReset;
	}

	public void setFlgReset(boolean flgReset) {
		this.flgReset = flgReset;
	}

	public boolean isFlgTablet() {
		return flgTablet;
	}

	public void setFlgTablet(boolean flgTablet) {
		this.flgTablet = flgTablet;
	}

	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiStruttura() {
		return codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

	public Integer getCodiRole() {
		return codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public long getTimeCeased() {
		return timeCeased;
	}

	public void setTimeCeased(long timeCeased) {
		this.timeCeased = timeCeased;
	}

	public long getTimeCreazione() {
		return timeCreazione;
	}

	public void setTimeCreazione(long timeCreazione) {
		this.timeCreazione = timeCreazione;
	}

	public long getTimeDisable() {
		return timeDisable;
	}

	public void setTimeDisable(long timeDisable) {
		this.timeDisable = timeDisable;
	}

	public long getTimeEnable() {
		return timeEnable;
	}

	public void setTimeEnable(long timeEnable) {
		this.timeEnable = timeEnable;
	}

	public long getTimeSuspended() {
		return timeSuspended;
	}

	public void setTimeSuspended(long timeSuspended) {
		this.timeSuspended = timeSuspended;
	}

	public long getTimeSuspension() {
		return timeSuspension;
	}

	public void setTimeSuspension(long timeSuspension) {
		this.timeSuspension = timeSuspension;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescNumeroSim() {
		return descNumeroSim;
	}

	public void setDescNumeroSim(String descNumeroSim) {
		this.descNumeroSim = descNumeroSim;
	}

	public String getDescImpresa() {
		return descImpresa;
	}

	public void setDescImpresa(String descImpresa) {
		this.descImpresa = descImpresa;
	}

	public Integer getFlgImpresa() {
		return flgImpresa;
	}

	public void setFlgImpresa(Integer flgImpresa) {
		this.flgImpresa = flgImpresa;
	}

	public Integer getCodiVendor() {
		return codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}
	
	
}
