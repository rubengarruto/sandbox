package com.rextart.saw.rs.bean.request;


public class RiferimentiFirmaBean {

    private Integer codiSessione;

    private Integer codiOggetto;

    private Integer codiUtente;

    private byte[] objMediaFirma;
	
    private String descNomeDitta;

    private String descTelefonoDitta;

    private Integer codiSurvey;
    
    private Integer flgDitta;
    
    private String descDittaInstallatrice;


  

	public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

    public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public byte[] getObjMediaFirma() {
        return objMediaFirma;
    }

    public void setObjMediaFirma(byte[] objMediaFirma) {
        this.objMediaFirma = objMediaFirma;
    }

    public String getDescNomeDitta() {
        return descNomeDitta;
    }

    public void setDescNomeDitta(String descNomeDitta) {
        this.descNomeDitta = descNomeDitta;
    }

    public String getDescTelefonoDitta() {
        return descTelefonoDitta;
    }

    public void setDescTelefonoDitta(String descTelefonoDitta) {
        this.descTelefonoDitta = descTelefonoDitta;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

	public Integer getFlgDitta() {
		return flgDitta;
	}

	public void setFlgDitta(Integer flgDitta) {
		this.flgDitta = flgDitta;
	}

	public String getDescDittaInstallatrice() {
		return descDittaInstallatrice;
	}

	public void setDescDittaInstallatrice(String descDittaInstallatrice) {
		this.descDittaInstallatrice = descDittaInstallatrice;
	}
    
    
}
