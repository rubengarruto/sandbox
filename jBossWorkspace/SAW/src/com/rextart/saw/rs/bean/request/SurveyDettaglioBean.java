package com.rextart.saw.rs.bean.request;

import java.io.Serializable;


public class SurveyDettaglioBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer codiDettaglio;
    private Integer codiSurvey;
    private Integer codiElementi;
    private Integer codiCaratteristiche;
    private Integer codiAnagLov;
    private String descValore;
    private Integer codiGruppo;
    private Integer codiCatalogo;
    private String descNote;
	private String descLabel;
	private Integer numeCella;
	private Integer numeVia;
	private Integer numeBranch;
	private Integer numeSettore;
	private Integer numeLivelloE;

	public Integer getCodiDettaglio() {
        return codiDettaglio;
    }

    public void setCodiDettaglio(Integer codiDettaglio) {
        this.codiDettaglio = codiDettaglio;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

    public Integer getCodiElementi() {
        return codiElementi;
    }

    public void setCodiElementi(Integer codiElementi) {
        this.codiElementi = codiElementi;
    }

    public Integer getCodiCaratteristiche() {
        return codiCaratteristiche;
    }

    public void setCodiCaratteristiche(Integer codiCaratteristiche) {
        this.codiCaratteristiche = codiCaratteristiche;
    }

    public Integer getCodiAnagLov() {
        return codiAnagLov;
    }

    public void setCodiAnagLov(Integer codiAnagLov) {
        this.codiAnagLov = codiAnagLov;
    }

    public String getDescValore() {
        return descValore;
    }

    public void setDescValore(String descValore) {
        this.descValore = descValore;
    }

    public Integer getCodiGruppo() {
        return codiGruppo;
    }

    public void setCodiGruppo(Integer codiGruppo) {
        this.codiGruppo = codiGruppo;
    }

	public Integer getCodiCatalogo() {
		return codiCatalogo;
	}

	public void setCodiCatalogo(Integer codiCatalogo) {
		this.codiCatalogo = codiCatalogo;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public Integer getNumeCella() {
		return numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public Integer getNumeVia() {
		return numeVia;
	}

	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}

	public Integer getNumeBranch() {
		return numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getNumeSettore() {
		return numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeLivelloE() {
		return numeLivelloE;
	}

	public void setNumeLivelloE(Integer numeLivelloE) {
		this.numeLivelloE = numeLivelloE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codiCaratteristiche == null) ? 0 : codiCaratteristiche
						.hashCode());
		result = prime * result
				+ ((codiCatalogo == null) ? 0 : codiCatalogo.hashCode());
		result = prime * result
				+ ((codiElementi == null) ? 0 : codiElementi.hashCode());
		result = prime * result
				+ ((codiGruppo == null) ? 0 : codiGruppo.hashCode());
		result = prime * result
				+ ((codiSurvey == null) ? 0 : codiSurvey.hashCode());
		result = prime * result
				+ ((descLabel == null) ? 0 : descLabel.hashCode());
		result = prime * result
				+ ((numeBranch == null) ? 0 : numeBranch.hashCode());
		result = prime * result
				+ ((numeCella == null) ? 0 : numeCella.hashCode());
		result = prime * result
				+ ((numeLivelloE == null) ? 0 : numeLivelloE.hashCode());
		result = prime * result
				+ ((numeSettore == null) ? 0 : numeSettore.hashCode());
		result = prime * result + ((numeVia == null) ? 0 : numeVia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SurveyDettaglioBean)) {
			return false;
		}
		SurveyDettaglioBean other = (SurveyDettaglioBean) obj;
		if (codiCaratteristiche == null) {
			if (other.codiCaratteristiche != null) {
				return false;
			}
		} else if (!codiCaratteristiche.equals(other.codiCaratteristiche)) {
			return false;
		}
		if (codiCatalogo == null) {
			if (other.codiCatalogo != null) {
				return false;
			}
		} else if (!codiCatalogo.equals(other.codiCatalogo)) {
			return false;
		}
		if (codiElementi == null) {
			if (other.codiElementi != null) {
				return false;
			}
		} else if (!codiElementi.equals(other.codiElementi)) {
			return false;
		}
		if (codiGruppo == null) {
			if (other.codiGruppo != null) {
				return false;
			}
		} else if (!codiGruppo.equals(other.codiGruppo)) {
			return false;
		}
		if (codiSurvey == null) {
			if (other.codiSurvey != null) {
				return false;
			}
		} else if (!codiSurvey.equals(other.codiSurvey)) {
			return false;
		}
		if (descLabel == null) {
			if (other.descLabel != null) {
				return false;
			}
		} else if (!descLabel.equals(other.descLabel)) {
			return false;
		}
		if (numeBranch == null) {
			if (other.numeBranch != null) {
				return false;
			}
		} else if (!numeBranch.equals(other.numeBranch)) {
			return false;
		}
		if (numeCella == null) {
			if (other.numeCella != null) {
				return false;
			}
		} else if (!numeCella.equals(other.numeCella)) {
			return false;
		}
		if (numeLivelloE == null) {
			if (other.numeLivelloE != null) {
				return false;
			}
		} else if (!numeLivelloE.equals(other.numeLivelloE)) {
			return false;
		}
		if (numeSettore == null) {
			if (other.numeSettore != null) {
				return false;
			}
		} else if (!numeSettore.equals(other.numeSettore)) {
			return false;
		}
		if (numeVia == null) {
			if (other.numeVia != null) {
				return false;
			}
		} else if (!numeVia.equals(other.numeVia)) {
			return false;
		}
		return true;
	}
}
