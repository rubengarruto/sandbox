package com.rextart.saw.rs.bean.request;

public class CheckInOutBeanRequest {

	private Integer codiOggetto;
	private Integer codiSessione;
	private Integer codiUtente;
	private boolean flagLock;
	public Integer getCodiOggetto() {
		return codiOggetto;
	}
	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public Integer getCodiUtente() {
		return codiUtente;
	}
	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}
	public boolean isFlagLock() {
		return flagLock;
	}
	public void setFlagLock(boolean flagLock) {
		this.flagLock = flagLock;
	}
	
	
	
}
