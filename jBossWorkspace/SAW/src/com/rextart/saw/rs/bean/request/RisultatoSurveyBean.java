package com.rextart.saw.rs.bean.request;

public class RisultatoSurveyBean {

    private Integer codiValoreRisultatoSurvey;

    private String descRisultatoSurvey;

    private Integer codiSessione;

    private Integer codiOggetto;

    private Integer codiUtente;

    private Integer codiSurvey;
    
    private Integer codiAnagRisultato;
    
    private Integer codiRisultato;
    
    private String descReportFotografico;

    public Integer getCodiValoreRisultatoSurvey() {
        return codiValoreRisultatoSurvey;
    }

    public void setCodiValoreRisultatoSurvey(Integer codiValoreRisultatoSurvey) {
        this.codiValoreRisultatoSurvey = codiValoreRisultatoSurvey;
    }

    public String getDescRisultatoSurvey() {
        return descRisultatoSurvey;
    }

    public void setDescRisultatoSurvey(String descRisultatoSurvey) {
        this.descRisultatoSurvey = descRisultatoSurvey;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

    public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

	public Integer getCodiAnagRisultato() {
		return codiAnagRisultato;
	}

	public void setCodiAnagRisultato(Integer codiAnagRisultato) {
		this.codiAnagRisultato = codiAnagRisultato;
	}

	public Integer getCodiRisultato() {
		return codiRisultato;
	}

	public void setCodiRisultato(Integer codiRisultato) {
		this.codiRisultato = codiRisultato;
	}

	public String getDescReportFotografico() {
		return descReportFotografico;
	}

	public void setDescReportFotografico(String descReportFotografico) {
		this.descReportFotografico = descReportFotografico;
	}
    
}
