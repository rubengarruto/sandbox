package com.rextart.saw.rs.bean.response;

public class AnagRitardiBean {

    private Integer codiAnagRitardo;
    private String descRitardo;
    private Integer flagCheckOut;


    public Integer getCodiAnagRitardo() {
        return codiAnagRitardo;
    }

    public void setCodiAnagRitardo(Integer codiAnagRitardo) {
        this.codiAnagRitardo = codiAnagRitardo;
    }

    public String getDescRitardo() {
        return descRitardo;
    }

    public void setDescRitardo(String descRitardo) {
        this.descRitardo = descRitardo;
    }

    public Integer getFlagCheckOut() {
        return flagCheckOut;
    }

    public void setFlagCheckOut(Integer flagCheckOut) {
        this.flagCheckOut = flagCheckOut;
    }
}