package com.rextart.saw.rs.bean.response;

public class SistemiBean {
	    private Integer codiSistema;
	    private String descSistema;

	    public Integer getCodiSistema() {
	        return codiSistema;
	    }

	    public void setCodiSistema(Integer codiSistema) {
	        this.codiSistema = codiSistema;
	    }

	    public String getDescSistema() {
	        return descSistema;
	    }

	    public void setDescSistema(String descSistema) {
	        this.descSistema = descSistema;
	    }
}
