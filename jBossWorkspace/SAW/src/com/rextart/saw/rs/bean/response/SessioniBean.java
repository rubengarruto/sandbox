package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.rs.bean.request.SurveyBean;

public class SessioniBean {
	   private Integer codiSessione;
	    private long timeAcquisizione;
	    private Integer codiAreaComp;
	    private String descRegione;
	    private String descProvincia;
	    private String descComune;
	    private Integer codiUnitaTerr;
	    private String descNomeSito;
	    private String descCodice;
	    private Integer codiTipoImpianto;
	    private Integer codiSistema;
	    private Integer codiBanda;
	    private String descOperTim;
	    private String descProgettoRadio;
	    private Integer codiFinalitaColl;
	    private Integer codiDettaglioColl;
	    private Integer flgAllegatoA;
	    private Integer flgAllegatoB;
	    private Integer flgAllegatoC;
	    private Integer flgAllegatoD;
	    private Integer flgAllegatoE;
	    private Integer flgApparatoEricsson;
	    private Integer flgApparatoNsn;
	    private Integer flgApparatoHuawei;
	    private Integer flgLockA;
	    private Integer flgLockB;
	    private Integer flgLockC;
	    private Integer flgLockD;
	    private Integer flgLockE;
	    private Integer flgLockEricsson;
	    private Integer flgLockHuawei;
	    private Integer flgLockNsn;
	    private Integer codiLastStato;
	    private List<SurveyBean> listSurvey;
	    private String clliSito;
	    private Integer codiCollaudo;
	    private String codiceDbr;
	    private String descIndirizzo;
	    private String descLatitudine;
	    private String descLongitudine;
	    private long dataSchedaPrRaEs;
	    private String descNote;
	    private long timePrevistoCollaudo;
	    
	    
	    
	    
	    
	    

	   

		public Integer getCodiSessione() {
	        return codiSessione;
	    }

	    public void setCodiSessione(Integer codiSessione) {
	        this.codiSessione = codiSessione;
	    }

	    public Integer getCodiAreaComp() {
	        return codiAreaComp;
	    }

	    public void setCodiAreaComp(Integer codiAreaComp) {
	        this.codiAreaComp = codiAreaComp;
	    }

	    public long getTimeAcquisizione() {
			return timeAcquisizione;
		}

		public void setTimeAcquisizione(long timeAcquisizione) {
			this.timeAcquisizione = timeAcquisizione;
		}

		public String getDescRegione() {
			return descRegione;
		}

		public void setDescRegione(String descRegione) {
			this.descRegione = descRegione;
		}

		public String getDescProvincia() {
			return descProvincia;
		}

		public void setDescProvincia(String descProvincia) {
			this.descProvincia = descProvincia;
		}

		public String getDescComune() {
			return descComune;
		}

		public void setDescComune(String descComune) {
			this.descComune = descComune;
		}

		public Integer getCodiUnitaTerr() {
	        return codiUnitaTerr;
	    }

	    public void setCodiUnitaTerr(Integer codiUnitaTerr) {
	        this.codiUnitaTerr = codiUnitaTerr;
	    }

	    public String getDescNomeSito() {
	        return descNomeSito;
	    }

	    public void setDescNomeSito(String descNomeSito) {
	        this.descNomeSito = descNomeSito;
	    }

	    public String getDescCodice() {
	        return descCodice;
	    }

	    public void setDescCodice(String descCodice) {
	        this.descCodice = descCodice;
	    }

	    public Integer getCodiTipoImpianto() {
	        return codiTipoImpianto;
	    }

	    public void setCodiTipoImpianto(Integer codiTipoImpianto) {
	        this.codiTipoImpianto = codiTipoImpianto;
	    }

	    public Integer getCodiSistema() {
	        return codiSistema;
	    }

	    public void setCodiSistema(Integer codiSistema) {
	        this.codiSistema = codiSistema;
	    }

	    public Integer getCodiBanda() {
	        return codiBanda;
	    }

	    public void setCodiBanda(Integer codiBanda) {
	        this.codiBanda = codiBanda;
	    }

	    public String getDescOperTim() {
	        return descOperTim;
	    }

	    public void setDescOperTim(String descOperTim) {
	        this.descOperTim = descOperTim;
	    }

	    public String getDescProgettoRadio() {
	        return descProgettoRadio;
	    }

	    public void setDescProgettoRadio(String descProgettoRadio) {
	        this.descProgettoRadio = descProgettoRadio;
	    }

	    public Integer getCodiFinalitaColl() {
	        return codiFinalitaColl;
	    }

	    public void setCodiFinalitaColl(Integer codiFinalitaColl) {
	        this.codiFinalitaColl = codiFinalitaColl;
	    }

	    public Integer getCodiDettaglioColl() {
	        return codiDettaglioColl;
	    }

	    public void setCodiDettaglioColl(Integer codiDettaglioColl) {
	        this.codiDettaglioColl = codiDettaglioColl;
	    }

	    public Integer getFlgAllegatoA() {
	        return flgAllegatoA;
	    }

	    public void setFlgAllegatoA(Integer flgAllegatoA) {
	        this.flgAllegatoA = flgAllegatoA;
	    }

	    public Integer getFlgAllegatoB() {
	        return flgAllegatoB;
	    }

	    public void setFlgAllegatoB(Integer flgAllegatoB) {
	        this.flgAllegatoB = flgAllegatoB;
	    }

	    public Integer getFlgAllegatoC() {
	        return flgAllegatoC;
	    }

	    public void setFlgAllegatoC(Integer flgAllegatoC) {
	        this.flgAllegatoC = flgAllegatoC;
	    }

	    public Integer getFlgAllegatoD() {
	        return flgAllegatoD;
	    }

	    public void setFlgAllegatoD(Integer flgAllegatoD) {
	        this.flgAllegatoD = flgAllegatoD;
	    }

	    public Integer getFlgAllegatoE() {
	        return flgAllegatoE;
	    }

	    public void setFlgAllegatoE(Integer flgAllegatoE) {
	        this.flgAllegatoE = flgAllegatoE;
	    }

	    public Integer getFlgApparatoEricsson() {
	        return flgApparatoEricsson;
	    }

	    public void setFlgApparatoEricsson(Integer flgApparatoEricsson) {
	        this.flgApparatoEricsson = flgApparatoEricsson;
	    }

	    public Integer getFlgApparatoNsn() {
	        return flgApparatoNsn;
	    }

	    public void setFlgApparatoNsn(Integer flgApparatoNsn) {
	        this.flgApparatoNsn = flgApparatoNsn;
	    }

	    public Integer getFlgApparatoHuawei() {
	        return flgApparatoHuawei;
	    }

	    public void setFlgApparatoHuawei(Integer flgApparatoHuawei) {
	        this.flgApparatoHuawei = flgApparatoHuawei;
	    }

	    public Integer getFlgLockA() {
	        return flgLockA;
	    }

	    public void setFlgLockA(Integer flgLockA) {
	        this.flgLockA = flgLockA;
	    }

	    public Integer getFlgLockB() {
	        return flgLockB;
	    }

	    public void setFlgLockB(Integer flgLockB) {
	        this.flgLockB = flgLockB;
	    }

	    public Integer getFlgLockC() {
	        return flgLockC;
	    }

	    public void setFlgLockC(Integer flgLockC) {
	        this.flgLockC = flgLockC;
	    }

	    public Integer getFlgLockD() {
	        return flgLockD;
	    }

	    public void setFlgLockD(Integer flgLockD) {
	        this.flgLockD = flgLockD;
	    }

	    public Integer getFlgLockE() {
	        return flgLockE;
	    }

	    public void setFlgLockE(Integer flgLockE) {
	        this.flgLockE = flgLockE;
	    }

	    public Integer getFlgLockEricsson() {
	        return flgLockEricsson;
	    }

	    public void setFlgLockEricsson(Integer flgLockEricsson) {
	        this.flgLockEricsson = flgLockEricsson;
	    }

	    public Integer getFlgLockHuawei() {
	        return flgLockHuawei;
	    }

	    public void setFlgLockHuawei(Integer flgLockHuawei) {
	        this.flgLockHuawei = flgLockHuawei;
	    }

	    public Integer getFlgLockNsn() {
	        return flgLockNsn;
	    }

	    public void setFlgLockNsn(Integer flgLockNsn) {
	        this.flgLockNsn = flgLockNsn;
	    }

		public Integer getCodiLastStato() {
			return codiLastStato;
		}

		public void setCodiLastStato(Integer codiLastStato) {
			this.codiLastStato = codiLastStato;
		}

		public List<SurveyBean> getListSurvey() {
			return listSurvey;
		}

		public void setListSurvey(List<SurveyBean> listSurvey) {
			this.listSurvey = listSurvey;
		}

		public String getClliSito() {
			return clliSito;
		}

		public void setClliSito(String clliSito) {
			this.clliSito = clliSito;
		}

		public Integer getCodiCollaudo() {
			return codiCollaudo;
		}

		public void setCodiCollaudo(Integer codiCollaudo) {
			this.codiCollaudo = codiCollaudo;
		}

		public String getCodiceDbr() {
			return codiceDbr;
		}

		public void setCodiceDbr(String codiceDbr) {
			this.codiceDbr = codiceDbr;
		}

		public String getDescIndirizzo() {
			return descIndirizzo;
		}

		public void setDescIndirizzo(String descIndirizzo) {
			this.descIndirizzo = descIndirizzo;
		}

		public String getDescLatitudine() {
			return descLatitudine;
		}

		public void setDescLatitudine(String descLatitudine) {
			this.descLatitudine = descLatitudine;
		}

		public String getDescLongitudine() {
			return descLongitudine;
		}

		public void setDescLongitudine(String descLongitudine) {
			this.descLongitudine = descLongitudine;
		}

		public long getDataSchedaPrRaEs() {
			return dataSchedaPrRaEs;
		}

		public void setDataSchedaPrRaEs(long dataSchedaPrRaEs) {
			this.dataSchedaPrRaEs = dataSchedaPrRaEs;
		}

		public String getDescNote() {
			return descNote;
		}

		public void setDescNote(String descNote) {
			this.descNote = descNote;
		}

		public long getTimePrevistoCollaudo() {
			return timePrevistoCollaudo;
		}

		public void setTimePrevistoCollaudo(long timePrevistoCollaudo) {
			this.timePrevistoCollaudo = timePrevistoCollaudo;
		}
		
		
		
}
