package com.rextart.saw.rs.bean.response;

public class GeneralResponse {

	private Integer responseCode;
	private Object responseObject;

	public Object getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(Object responseObject) {
		this.responseObject = responseObject;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
}
