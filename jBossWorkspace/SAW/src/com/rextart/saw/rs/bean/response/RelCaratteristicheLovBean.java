package com.rextart.saw.rs.bean.response;


public class RelCaratteristicheLovBean {
	private Integer codiRelCarattLov;

	private Integer codiCaratteristica;

	private Integer codiAnagLov;

	private Integer codiNextCaratt;

	private Integer codiCatalogo;
	
	private Integer codiDescCaratt;

	private Integer codiPrecCaratt;
	
	private String descLabel;
	
	

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public Integer getCodiCatalogo() {
		return codiCatalogo;
	}

	public void setCodiCatalogo(Integer codiCatalogo) {
		this.codiCatalogo = codiCatalogo;
	}

	public Integer getCodiNextCaratt() {
		return codiNextCaratt;
	}

	public void setCodiNextCaratt(Integer codiNextCaratt) {
		this.codiNextCaratt = codiNextCaratt;
	}

	public Integer getCodiRelCarattLov() {
		return codiRelCarattLov;
	}

	public void setCodiRelCarattLov(Integer codiRelCarattLov) {
		this.codiRelCarattLov = codiRelCarattLov;
	}

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiAnagLov() {
		return codiAnagLov;
	}

	public void setCodiAnagLov(Integer codiAnagLov) {
		this.codiAnagLov = codiAnagLov;
	}

	public Integer getCodiDescCaratt() {
		return codiDescCaratt;
	}

	public void setCodiDescCaratt(Integer codiDescCaratt) {
		this.codiDescCaratt = codiDescCaratt;
	}

	public Integer getCodiPrecCaratt() {
		return codiPrecCaratt;
	}

	public void setCodiPrecCaratt(Integer codiPrecCaratt) {
		this.codiPrecCaratt = codiPrecCaratt;
	}
	
	
}
