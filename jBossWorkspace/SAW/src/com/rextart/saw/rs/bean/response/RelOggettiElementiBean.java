package com.rextart.saw.rs.bean.response;

public class RelOggettiElementiBean {
	private Integer codiRelOggElem;

    private Integer codiElemento;

    private Integer codiOggetto;

    private Integer codiTipoOggetto;
    
    private Integer numeOrder;

    public Integer getCodiTipoOggetto() {
        return codiTipoOggetto;
    }

    public void setCodiTipoOggetto(Integer codiTipoOggetto) {
        this.codiTipoOggetto = codiTipoOggetto;
    }

    public Integer getCodiRelOggElem() {
        return codiRelOggElem;
    }

    public void setCodiRelOggElem(Integer codiRelOggElem) {
        this.codiRelOggElem = codiRelOggElem;
    }

    public Integer getCodiElemento() {
        return codiElemento;
    }

    public void setCodiElemento(Integer codiElemento) {
        this.codiElemento = codiElemento;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

	public Integer getNumeOrder() {
		return numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}

    
}
