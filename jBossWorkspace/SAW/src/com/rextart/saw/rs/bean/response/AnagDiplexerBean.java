package com.rextart.saw.rs.bean.response;


public class AnagDiplexerBean {
	private Integer codiAnagDiplexer;
	
	private String descMarca;
	
	private String descModello;
	
	private Integer flagAttivo;


	public String getDescMarca() {
		return descMarca;
	}


	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescModello() {
		return descModello;
	}


	public void setDescModello(String descModello) {
		this.descModello = descModello;
	}



	public Integer getCodiAnagDiplexer() {
		return codiAnagDiplexer;
	}


	public void setCodiAnagDiplexer(Integer codiAnagDiplexer) {
		this.codiAnagDiplexer = codiAnagDiplexer;
	}


	public Integer getFlagAttivo() {
		return flagAttivo;
	}


	public void setFlagAttivo(Integer flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	
	
}
