package com.rextart.saw.rs.bean.response;


public class AnagRisultatiBean {
	
	private Integer codiAnagRisultati;
    private String descAnagRisultati;
    private Integer codiRisultato;
    private Integer codiStruttura;
    private Integer flagApparato;
    private Integer flagD1;
    private Integer flagE;


    public Integer getCodiAnagRisultati() {
        return codiAnagRisultati;
    }


    public void setCodiAnagRisultati(Integer codiAnagRisultati) {
        this.codiAnagRisultati = codiAnagRisultati;
    }


    public String getDescAnagRisultati() {
        return descAnagRisultati;
    }


    public void setDescAnagRisultati(String descAnagRisultati) {
        this.descAnagRisultati = descAnagRisultati;
    }


    public Integer getCodiRisultato() {
        return codiRisultato;
    }

    public void setCodiRisultato(Integer codiRisultato) {
        this.codiRisultato = codiRisultato;
    }


	public Integer getCodiStruttura() {
		return codiStruttura;
	}


	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}


	public Integer getFlagApparato() {
		return flagApparato;
	}


	public void setFlagApparato(Integer flagApparato) {
		this.flagApparato = flagApparato;
	}


	public Integer getFlagD1() {
		return flagD1;
	}


	public void setFlagD1(Integer flagD1) {
		this.flagD1 = flagD1;
	}


	public Integer getFlagE() {
		return flagE;
	}


	public void setFlagE(Integer flagE) {
		this.flagE = flagE;
	}
	
	
    
    
}
