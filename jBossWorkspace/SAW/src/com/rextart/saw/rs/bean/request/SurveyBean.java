package com.rextart.saw.rs.bean.request;

import java.io.Serializable;


public class SurveyBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer codiSurvey;

    private Integer codiSessione;

    private Integer codiOggetto;

    private Integer codiTipoOggetto;

    private String descNote;

    private long timeCreate;
    
    private Integer codiSurveyBackEnd;
    
    private String codiUid;
    
    private Integer flagEsito;


    public Integer getCodiTipoOggetto() {
        return codiTipoOggetto;
    }

    public void setCodiTipoOggetto(Integer codiTipoOggetto) {
        this.codiTipoOggetto = codiTipoOggetto;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }


    public String getDescNote() {
        return descNote;
    }

    public void setDescNote(String descNote) {
        this.descNote = descNote;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

	public Integer getCodiSurveyBackEnd() {
		return codiSurveyBackEnd;
	}

	public void setCodiSurveyBackEnd(Integer codiSurveyBackEnd) {
		this.codiSurveyBackEnd = codiSurveyBackEnd;
	}

	public String getCodiUid() {
		return codiUid;
	}

	public void setCodiUid(String codiUid) {
		this.codiUid = codiUid;
	}

	public Integer getFlagEsito() {
		return flagEsito;
	}

	public void setFlagEsito(Integer flagEsito) {
		this.flagEsito = flagEsito;
	}

	

    
}