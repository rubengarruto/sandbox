package com.rextart.saw.rs.bean.request;

import java.util.List;

public class DettaglioSurveyDownloadBeanRequest {

	private List<Integer> listCodiSurvey;
	private Integer codiSessione;
	private Integer codiUtente;
	
	
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public Integer getCodiUtente() {
		return codiUtente;
	}
	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}
	
	public List<Integer> getListCodiSurvey() {
        return listCodiSurvey;
    }

    public void setListCodiSurvey(List<Integer> listCodiSurvey) {
        this.listCodiSurvey = listCodiSurvey;
    }
}
