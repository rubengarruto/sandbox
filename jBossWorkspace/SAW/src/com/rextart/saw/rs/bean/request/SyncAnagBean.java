package com.rextart.saw.rs.bean.request;


public class SyncAnagBean {

	public static final String TABLE_TAB_ANAG_LOV = "TAB_ANAG_LOV";
	public static final String TABLE_TAB_CARATTERISTICHE = "TAB_CARATTERISTICHE";
	public static final String TABLE_TAB_ELEMENTI = "TAB_ELEMENTI";
	public static final String TABLE_TAB_OGGETTI = "TAB_OGGETTI";
	public static final String TABLE_TAB_REL_CARATTERISTICHE_LOV = "TAB_REL_CARATTERISTICHE_LOV";
	public static final String TABLE_TAB_REL_ELEMENTI_CARATT = "TAB_REL_ELEMENTI_CARATT";
	public static final String TABLE_TAB_REL_OGGETTI_ELEMENTI = "TAB_REL_OGGETTI_ELEMENTI";
	public static final String TABLE_TAB_REL_OGGETTI_TIPO = "TAB_REL_OGGETTI_TIPO";
	public static final String TABLE_TAB_REL_SERVIZI_OGGETTI = "TAB_REL_SERVIZI_OGGETTI";
	public static final String TABLE_TAB_SERVIZI = "TAB_SERVIZI";
	public static final String TABLE_TAB_TIPOLOGIE_OGGETTO = "TAB_TIPOLOGIE_OGGETTO";
	public static final String TABLE_TAB_SYNC_LOV = "TAB_SYNC_LOV";
	public static final String TABLE_TAB_STRUTTURA = "TAB_STRUTTURA";
	public static final String TABLE_TAB_ANAG_STATI_SESSIONE = "TAB_ANAG_STATI_SESSIONE";
	public static final String TABLE_TAB_ANAG_ANTENNE = "TAB_ANAG_ANTENNE";
	public static final String TABLE_TAB_ANAG_DIPLEXER = "TAB_ANAG_DIPLEXER";
	public static final String TABLE_TAB_ANAG_TMA = "TAB_ANAG_TMA";
	public static final String TABLE_TAB_ANAG_RISULTATI = "TAB_ANAG_RISULTATI";
	public static final String TABLE_TAB_ANAG_RITARDI = "TAB_ANAG_RITARDI";
	public static final String TABLE_TAB_SISTEMI = "TAB_SISTEMI";
	public static final String TABLE_TAB_BANDE = "TAB_BANDE";
	
	private Integer codiSyncAnag;
	private String descNameTable;
	private Integer numeVersione;
	public Integer getCodiSyncAnag() {
		return codiSyncAnag;
	}
	public void setCodiSyncAnag(Integer codiSyncAnag) {
		this.codiSyncAnag = codiSyncAnag;
	}
	public String getDescNameTable() {
		return descNameTable;
	}
	public void setDescNameTable(String descNameTable) {
		this.descNameTable = descNameTable;
	}
	public Integer getNumeVersione() {
		return numeVersione;
	}
	public void setNumeVersione(Integer numeVersione) {
		this.numeVersione = numeVersione;
	}
}
