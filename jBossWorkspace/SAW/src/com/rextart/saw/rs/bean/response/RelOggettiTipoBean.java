package com.rextart.saw.rs.bean.response;

public class RelOggettiTipoBean {
	 private Integer codiOggettoTipo;

	    private Integer codiOggetto;

	    private Integer codiTipoOggetto;

	    public Integer getCodiOggettoTipo() {
	        return codiOggettoTipo;
	    }

	    public void setCodiOggettoTipo(Integer codiOggettoTipo) {
	        this.codiOggettoTipo = codiOggettoTipo;
	    }

	    public Integer getCodiOggetto() {
	        return codiOggetto;
	    }

	    public void setCodiOggetto(Integer codiOggetto) {
	        this.codiOggetto = codiOggetto;
	    }

	    public Integer getCodiTipoOggetto() {
	        return codiTipoOggetto;
	    }

	    public void setCodiTipoOggetto(Integer codiTipoOggetto) {
	        this.codiTipoOggetto = codiTipoOggetto;
	    }
}
