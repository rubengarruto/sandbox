package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.rs.bean.request.SurveyBean;

public class CheckInOutBean {
	
	private boolean flagRitardo;
	
	private boolean flagDittaParziale=false;
	
	private List<SurveyBean> listSurveyBean;

	public boolean isFlagRitardo() {
		return flagRitardo;
	}

	public void setFlagRitardo(boolean flagRitardo) {
		this.flagRitardo = flagRitardo;
	}

	public boolean isFlagDittaParziale() {
		return flagDittaParziale;
	}

	public void setFlagDittaParziale(boolean flagDittaParziale) {
		this.flagDittaParziale = flagDittaParziale;
	}

	public List<SurveyBean> getListSurveyBean() {
		return listSurveyBean;
	}

	public void setListSurveyBean(List<SurveyBean> listSurveyBean) {
		this.listSurveyBean = listSurveyBean;
	}

	
	

     	    
}
