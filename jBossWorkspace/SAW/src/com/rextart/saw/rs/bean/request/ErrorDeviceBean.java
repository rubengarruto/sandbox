package com.rextart.saw.rs.bean.request;


public class ErrorDeviceBean {

	 private Integer codiErrorDevice;

	    private Integer codiUtente;

	    private Integer codiSessione;

	    private Integer codiCliente;

	    private String descErrore;

	    private String descTypeDevice;

	    private Integer numeLevelApiDevice;

	    private Long dateError;

	    private Integer numeVersionApp;

	    private String descImeiDevice;
	    
	    private String descTypeError;

	    public String getDescImeiDevice() {
	        return descImeiDevice;
	    }

	    public void setDescImeiDevice(String descImeiDevice) {
	        this.descImeiDevice = descImeiDevice;
	    }

	    public Integer getNumeVersionApp() {
	        return numeVersionApp;
	    }

	    public void setNumeVersionApp(Integer numeVersionApp) {
	        this.numeVersionApp = numeVersionApp;
	    }

	    public Long getDateError() {
	        return dateError;
	    }

	    public void setDateError(Long dateError) {
	        this.dateError = dateError;
	    }

	    public Integer getCodiErrorDevice() {
	        return codiErrorDevice;
	    }

	    public void setCodiErrorDevice(Integer codiErrorDevice) {
	        this.codiErrorDevice = codiErrorDevice;
	    }

	    public Integer getCodiUtente() {
	        return codiUtente;
	    }

	    public void setCodiUtente(Integer codiUtente) {
	        this.codiUtente = codiUtente;
	    }

	    public Integer getCodiSessione() {
	        return codiSessione;
	    }

	    public void setCodiSessione(Integer codiSessione) {
	        this.codiSessione = codiSessione;
	    }

	    public Integer getCodiCliente() {
	        return codiCliente;
	    }

	    public void setCodiCliente(Integer codiCliente) {
	        this.codiCliente = codiCliente;
	    }

	    public String getDescErrore() {
	        return descErrore;
	    }

	    public void setDescErrore(String descErrore) {
	        this.descErrore = descErrore;
	    }

	    public String getDescTypeDevice() {
	        return descTypeDevice;
	    }

	    public void setDescTypeDevice(String descTypeDevice) {
	        this.descTypeDevice = descTypeDevice;
	    }

	    public Integer getNumeLevelApiDevice() {
	        return numeLevelApiDevice;
	    }

	    public void setNumeLevelApiDevice(Integer numeLevelApiDevice) {
	        this.numeLevelApiDevice = numeLevelApiDevice;
	    }

		public String getDescTypeError() {
			return descTypeError;
		}

		public void setDescTypeError(String descTypeError) {
			this.descTypeError = descTypeError;
		}
	    
	    
}

