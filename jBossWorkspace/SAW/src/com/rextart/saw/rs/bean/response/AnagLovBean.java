package com.rextart.saw.rs.bean.response;

public class AnagLovBean {
	private Integer codiAnagLov;
    private String descLabel;
    private Integer flagAltro;

	public Integer getCodiAnagLov() {
		return codiAnagLov;
	}
	public void setCodiAnagLov(Integer codiAnagLov) {
		this.codiAnagLov = codiAnagLov;
	}
	public String getDescLabel() {
		return descLabel;
	}
	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}
	public Integer getFlagAltro() {
		return flagAltro;
	}
	public void setFlagAltro(Integer flagAltro) {
		this.flagAltro = flagAltro;
	}
}
