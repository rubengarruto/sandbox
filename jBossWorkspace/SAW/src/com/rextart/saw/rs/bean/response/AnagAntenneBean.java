package com.rextart.saw.rs.bean.response;


public class AnagAntenneBean{
	
	private Integer codiAnagAntenne;
	private String descMarca;
	private String descModello;
	private Integer flagAttivo;


	public Integer getCodiAnagAntenne() {
		return codiAnagAntenne;
	}


	public void setCodiAnagAntenne(Integer codiAnagAntenne) {
		this.codiAnagAntenne = codiAnagAntenne;
	}


	public String getDescMarca() {
		return descMarca;
	}


	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescModello() {
		return descModello;
	}


	public void setDescModello(String descModello) {
		this.descModello = descModello;
	}


	public Integer getFlagAttivo() {
		return flagAttivo;
	}


	public void setFlagAttivo(Integer flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	
}
