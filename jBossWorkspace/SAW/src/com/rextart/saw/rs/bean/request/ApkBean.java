package com.rextart.saw.rs.bean.request;


public class ApkBean {
	private Integer codiApk;

	private Integer numeVersione;

	private byte[] objtApk;

	private Integer flagAttiva;
	
	private String descNote;
	
	private Integer flagLast;
	
	private String descScriptDb;
	
	private boolean existNewVersion=false;
	
	private String descRelease;

    public String getDescRelease() {
        return descRelease;
    }

    public void setDescRelease(String descRelease) {
        this.descRelease = descRelease;
    }
	
	public boolean isExistNewVersion() {
		return existNewVersion;
	}

	public void setExistNewVersion(boolean existNewVersion) {
		this.existNewVersion = existNewVersion;
	}

	public Integer getCodiApk() {
		return codiApk;
	}

	public void setCodiApk(Integer codiApk) {
		this.codiApk = codiApk;
	}

	public Integer getNumeVersione() {
		return numeVersione;
	}

	public void setNumeVersione(Integer numeVersione) {
		this.numeVersione = numeVersione;
	}

	public byte[] getObjtApk() {
		return objtApk;
	}

	public void setObjtApk(byte[] objtApk) {
		this.objtApk = objtApk;
	}

	public Integer getFlagAttiva() {
		return flagAttiva;
	}

	public void setFlagAttiva(Integer flagAttiva) {
		this.flagAttiva = flagAttiva;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public Integer getFlagLast() {
		return flagLast;
	}

	public void setFlagLast(Integer flagLast) {
		this.flagLast = flagLast;
	}

	public String getDescScriptDb() {
		return descScriptDb;
	}

	public void setDescScriptDb(String descScriptDb) {
		this.descScriptDb = descScriptDb;
	}
	
	
}
