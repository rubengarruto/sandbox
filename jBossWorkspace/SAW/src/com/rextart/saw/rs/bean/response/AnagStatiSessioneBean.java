package com.rextart.saw.rs.bean.response;

public class AnagStatiSessioneBean {

    private Integer codiStato;
    private String descStato;
    private Integer flagInLavorazione = 0;
    private Integer flagChiuso = 0;
	public Integer getCodiStato() {
		return codiStato;
	}
	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}
	public String getDescStato() {
		return descStato;
	}
	public void setDescStato(String descStato) {
		this.descStato = descStato;
	}
	public Integer getFlagChiuso() {
		return flagChiuso;
	}
	public void setFlagChiuso(Integer flagChiuso) {
		this.flagChiuso = flagChiuso;
	}
	public Integer getFlagInLavorazione() {
		return flagInLavorazione;
	}
	public void setFlagInLavorazione(Integer flagInLavorazione) {
		this.flagInLavorazione = flagInLavorazione;
	}
    
    
}
