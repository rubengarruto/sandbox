package com.rextart.saw.rs.bean.response;

import java.util.List;

import com.rextart.saw.rs.bean.request.NoteBean;
import com.rextart.saw.rs.bean.request.NoteCaratteristicheBean;
import com.rextart.saw.rs.bean.request.RiferimentiFirmaBean;
import com.rextart.saw.rs.bean.request.RisultatoSurveyBean;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;


public class DettaglioDownloadBean {

	private List<SurveyDettaglioBean> listSurveyDettaglio;
	
	private List<NoteCaratteristicheBean> listNoteCaratteristiche;
	
	private List<RisultatoSurveyBean> listRisultatoSurvey;
	
	private List<RiferimentiFirmaBean> listRiferimentiFirma;
	
	private List<NoteBean> listNote;

	public List<SurveyDettaglioBean> getListSurveyDettaglio() {
		return listSurveyDettaglio;
	}

	public void setListSurveyDettaglio(List<SurveyDettaglioBean> listSurveyDettaglio) {
		this.listSurveyDettaglio = listSurveyDettaglio;
	}

	public List<NoteCaratteristicheBean> getListNoteCaratteristiche() {
		return listNoteCaratteristiche;
	}

	public void setListNoteCaratteristiche(
			List<NoteCaratteristicheBean> listNoteCaratteristiche) {
		this.listNoteCaratteristiche = listNoteCaratteristiche;
	}

	public List<RisultatoSurveyBean> getListRisultatoSurvey() {
		return listRisultatoSurvey;
	}

	public void setListRisultatoSurvey(List<RisultatoSurveyBean> listRisultatoSurvey) {
		this.listRisultatoSurvey = listRisultatoSurvey;
	}

	public List<RiferimentiFirmaBean> getListRiferimentiFirma() {
		return listRiferimentiFirma;
	}

	public void setListRiferimentiFirma(
			List<RiferimentiFirmaBean> listRiferimentiFirma) {
		this.listRiferimentiFirma = listRiferimentiFirma;
	}

	public List<NoteBean> getListNote() {
		return listNote;
	}

	public void setListNote(List<NoteBean> listNote) {
		this.listNote = listNote;
	}

	
	
	
	
}
