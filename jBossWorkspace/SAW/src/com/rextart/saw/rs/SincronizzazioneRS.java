package com.rextart.saw.rs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.openkm.sdk4j.bean.Document;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.openKM.OpenKmService;
import com.rextart.saw.rs.bean.request.MediaBean;
import com.rextart.saw.rs.bean.request.SincronizzazioneBean;
import com.rextart.saw.rs.bean.response.GeneralResponse;
import com.rextart.saw.rs.bean.response.SincronizzazioneValueReturnBean;
import com.rextart.saw.rs.bean.response.VerbaleBean;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SincronizzazioneService;
import com.rextart.saw.service.UserService;
import com.rextart.saw.service.UtilityService;
import com.rextart.saw.utility.Const;

@RequestMapping("/rest")
@Controller
public class SincronizzazioneRS {

	@Autowired
	SincronizzazioneService sincronizzazioneService;

	@Autowired
	UserService userService;

	@Autowired
	OpenKmService openKmService;


	@Autowired
	private UtilityService serviceUtility;

	@Autowired 
	private SessioniService sessioniService;

	@Autowired
	private RepositoryMediaService repositoryMediaService;

	public static String UPLOAD_FILES = "uploadFiles";
	public static String INPUT_DATA = "inputDati";


	@RequestMapping(value = "/sync/sincronizzaFigliSurvey", method=RequestMethod.POST, produces = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE}, consumes = {org.springframework.http.MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public GeneralResponse sincronizzaFigliSurvey(@RequestBody SincronizzazioneBean sincronizzazioneBean) {
		GeneralResponse gr = new GeneralResponse();
		sincronizzazioneService.logOperTrace(sincronizzazioneBean.getCodiUtente(),"DESC_IMEI:"+sincronizzazioneBean.getDescImei()+" CODI_SESSIONE:"+sincronizzazioneBean.getCodiSessione()+" RESPONSE:"+gr.getResponseCode());
		SincronizzazioneValueReturnBean sincronizzazioneValueReturnBean = new SincronizzazioneValueReturnBean();
		try {

			if(!sincronizzazioneService.isOggettoOwner(sincronizzazioneBean.getCodiSessione(), sincronizzazioneBean.getCodiOggetto(), sincronizzazioneBean.getCodiUtente())){
				gr.setResponseCode(Constants.ERROR_LOCK_OGGETTO_NO_OWNER);
				return gr;
			}
			
			if(!(sincronizzazioneService.isD1EsitoLibero(sincronizzazioneBean.getCodiSessione(), sincronizzazioneBean.getCodiOggetto()))){
				gr.setResponseCode(Constants.ERROR_ESITOD_ALREADY_EXIST);
				ViwSessioni selectedSessione = sessioniService.findSessione(sincronizzazioneBean.getCodiSessione());
				sessioniService.unlock(selectedSessione, sincronizzazioneBean.getCodiSessione(),Const.ALLEGATO_D);
				return gr;
			}
			

			if(sessioniService.isLockUnlockOggetto(sincronizzazioneBean.getCodiSessione(), sincronizzazioneBean.getCodiOggetto(), true)){
				if(sincronizzazioneBean.getSurveyDettaglioBeans()!=null){

					UtilLog.printCheckOutSync(sincronizzazioneBean);
				
					if(sincronizzazioneService.isSincronizzaAvaible(sincronizzazioneBean.getCodiSessione())){
						UtilLog.printSincronizzaFigliSurvey(sincronizzazioneBean);

						sincronizzazioneService.saveSincronizzazione(sincronizzazioneBean);

						//-----
						//devo aggiornare poi il tablet sullo stato del verbale se � l'ultima sincronizzazione
						//----
						VerbaleBean verbaleBean=null;
						if (sincronizzazioneBean.isLastCall()) {
							try {	 
								ViwVerbale viwVerbale= sincronizzazioneService.getViwVerbaleBySessioneOggetto(sincronizzazioneBean.getCodiSessione(),sincronizzazioneBean.getCodiOggetto());
								if (viwVerbale!=null) {
									verbaleBean= DataConverter.getVerbaleBean(viwVerbale);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							sincronizzazioneValueReturnBean.setCodiOggetto(sincronizzazioneBean.getCodiOggetto());
							sincronizzazioneValueReturnBean.setCodiSessione(sincronizzazioneBean.getCodiSessione());
						}
						sincronizzazioneValueReturnBean.setVerbaleBean(verbaleBean);


						gr.setResponseObject(sincronizzazioneValueReturnBean);
						gr.setResponseCode(Constants.RESPONSE_OK);

					}else{
						gr.setResponseCode(Constants.ERROR_SYNC_NO_MODIFY);
					}

				}else{
					gr.setResponseCode(Constants.ERROR_GENERIC);
					UtilLog.getLog().error("ERRORE SERVIZIO sincronizzaFigliSurvey - SincronizzazioneBean inviato dal device � NULL");
				}
			}else{
				gr.setResponseCode(Constants.ERROR_SYNC_OGGETTO_NO_LOCK);
			}


		} catch (Exception e) {
			UtilLog.getLog().error("ERRORE SERVIZIO sincronizzaFigliSurvey "+e.getMessage());
			gr.setResponseCode(Constants.ERROR_GENERIC);
			e.printStackTrace();
		}
		sincronizzazioneService.logOperTrace(sincronizzazioneBean.getCodiUtente(),"DESC_IMEI:"+sincronizzazioneBean.getDescImei()+" CODI_SESSIONE:"+sincronizzazioneBean.getCodiSessione()+" RESPONSE:"+gr.getResponseCode());

		return gr;
	}



	@RequestMapping(value = "/sync/syncMedia", method=RequestMethod.POST)
	@ResponseBody 
	public GeneralResponse syncMedia(@RequestParam("uploadFiles") MultipartFile[] files) {
		GeneralResponse gr = new GeneralResponse();
		gr.setResponseCode(Constants.RESPONSE_OK);
		String content=null;

		if (files != null && files.length >0) {
			for(int i =0 ;i< files.length; i++){
				Gson gson = new GsonBuilder().create();
				try {
					content= files[i].getContentType();
					StringBuilder builder = new StringBuilder();
					builder.append(content);
					MediaBean mb=gson.fromJson(builder.toString(), MediaBean.class);
					
					int provaIndex = mb.getFileName().indexOf(".");
					List<TabRepositoryMedia> listaBySessioneAndCategoria= repositoryMediaService.getAllBySessioneAndCategoria(mb.getCodiSessione(), mb.getDescCategoria());
					String counterOfMedia= String.valueOf(listaBySessioneAndCategoria.size() + 1 );
					String newFileName = mb.getFileName().substring(0,provaIndex-1) + counterOfMedia + mb.getFileName().substring(provaIndex);
					mb.setFileName(newFileName);
					UtilLog.printSincronizzaMedia(mb);

					TabUtente utente = userService.getUtenteById(mb.getCodiUtente());
					TabRepositoryMedia tabRepMedia = new TabRepositoryMedia();
					if(utente!=null){
						Document doc= openKmService.uploadSync(mb.getCodiSessione(), mb.getFileName(), utente.getDescUser(), files[i].getInputStream());

						tabRepMedia= DataConverter.getTabRepositoryMedia(mb, utente, doc);
						repositoryMediaService.saveRepositoryMedia(tabRepMedia);
						gr.setResponseCode(Constants.RESPONSE_OK);
					}
				} catch (Exception e) {
					UtilLog.getLog().error("ERRORE SERVIZIO sincronizzaMedia "+e.getMessage());
					gr.setResponseCode(Constants.ERROR_GENERIC);
					e.printStackTrace();
				}
			}

		} else {
			gr.setResponseCode(Constants.ERROR_GENERIC);
			UtilLog.getLog().error("ERRORE SERVIZIO sincronizzaMedia files inviato dal device � NULL oppure size 0");
		}

		return gr;
	}


}
