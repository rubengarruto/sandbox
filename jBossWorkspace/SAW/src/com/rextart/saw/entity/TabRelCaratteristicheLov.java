package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_CARATTERISTICHE_LOV database table.
 * 
 */
@Entity
@Table(name="TAB_REL_CARATTERISTICHE_LOV")
@Named("TabRelCaratteristicheLov")
@NamedQueries({
	@NamedQuery(name="TabRelCaratteristicheLov.findAll", query="SELECT t FROM TabRelCaratteristicheLov t order by t.codiRelCarattLov desc"),
	@NamedQuery(name="TabRelCaratteristicheLov.findRelAltro", query="SELECT t FROM TabRelCaratteristicheLov t, TabAnagLov l where t.codiAnaglov = l.codiAnagLov and l.flagAltro = true and t.codiCaratteristica = :codiCaratt")
})
public class TabRelCaratteristicheLov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_REL_CARATT_LOV")
	private Integer codiRelCarattLov;

	@Column(name="CODI_ANAG_LOV")
	private Integer codiAnaglov;
	
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;
	
	@Column(name="CODI_NEXT_CARATT")
	private Integer codiNextCaratt;
	
	@Column(name="CODI_CATALOGO")
	private Integer codiCatalogo;
	
	@Column(name="CODI_DESC_CARATT")
	private Integer codiDescCaratt;

	@Column(name="CODI_PREC_CARATT")
	private Integer codiPrecCaratt;
	
	@Column(name="DESC_LABEL")
	private String descLabel;
	
	public Integer getCodiCatalogo() {
		return codiCatalogo;
	}

	public void setCodiCatalogo(Integer codiCatalogo) {
		this.codiCatalogo = codiCatalogo;
	}

	public TabRelCaratteristicheLov() {
	}

	public Integer getCodiRelCarattLov() {
		return this.codiRelCarattLov;
	}

	public void setCodiRelCarattLov(Integer codiRelCarattLov) {
		this.codiRelCarattLov = codiRelCarattLov;
	}

	public Integer getCodiAnaglov() {
		return codiAnaglov;
	}

	public void setCodiAnaglov(Integer codiAnaglov) {
		this.codiAnaglov = codiAnaglov;
	}

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiNextCaratt() {
		return codiNextCaratt;
	}

	public void setCodiNextCaratt(Integer codiNextCaratt) {
		this.codiNextCaratt = codiNextCaratt;
	}

	public Integer getCodiDescCaratt() {
		return codiDescCaratt;
	}

	public void setCodiDescCaratt(Integer codiDescCaratt) {
		this.codiDescCaratt = codiDescCaratt;
	}

	public Integer getCodiPrecCaratt() {
		return codiPrecCaratt;
	}

	public void setCodiPrecCaratt(Integer codiPrecCaratt) {
		this.codiPrecCaratt = codiPrecCaratt;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	
}