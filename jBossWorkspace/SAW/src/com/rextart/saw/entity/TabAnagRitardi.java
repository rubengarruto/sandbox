package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_RITARDI database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_RITARDI")
@Named("TabAnagRitardi")
@NamedQueries({
	@NamedQuery(name="TabAnagRitardi.findAll", query="SELECT t FROM TabAnagRitardi t"),
	@NamedQuery(name="TabAnagRitardi.findByDescRitAndFlgCheck", query="SELECT t FROM TabAnagRitardi t where t.descRitardo=:descRitardo and t.flagCheckOut=:flagCheckOut"),
	@NamedQuery(name="TabAnagRitardi.findByCodiAnagRitardo", query="SELECT t FROM TabAnagRitardi t where t.codiAnagRitardo=:codiAnagRitardo")
})
public class TabAnagRitardi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_ANAG_RITARDI_CODI_ANAG_RITARDO_GENERATOR", sequenceName="SEQ_ANAG_RITARDI",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_ANAG_RITARDI_CODI_ANAG_RITARDO_GENERATOR")
	@Column(name="CODI_ANAG_RITARDO")
	private Integer codiAnagRitardo;

	@Column(name="DESC_RITARDO")
	private String descRitardo;

	@Column(name="FLAG_CHECK_OUT")
	private Boolean flagCheckOut = true;

	public TabAnagRitardi() {
	}

	public Integer getCodiAnagRitardo() {
		return this.codiAnagRitardo;
	}

	public void setCodiAnagRitardo(Integer codiAnagRitardo) {
		this.codiAnagRitardo = codiAnagRitardo;
	}

	public String getDescRitardo() {
		return this.descRitardo;
	}

	public void setDescRitardo(String descRitardo) {
		this.descRitardo = descRitardo;
	}

	public Boolean getFlagCheckOut() {
		return flagCheckOut;
	}

	public void setFlagCheckOut(Boolean flagCheckOut) {
		this.flagCheckOut = flagCheckOut;
	}

	
}