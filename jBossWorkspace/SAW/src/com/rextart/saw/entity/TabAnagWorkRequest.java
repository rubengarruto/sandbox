package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_WORK_REQUEST database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_WORK_REQUEST")
@Named("TabAnagWorkRequest")
@NamedQuery(name="TabAnagWorkRequest.findAll", query="SELECT t FROM TabAnagWorkRequest t")
public class TabAnagWorkRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ANAG_WRS")
	private Integer codiAnagWrs;

	@Column(name="DESC_ANAG_WRS")
	private String descAnagWrs;

	public TabAnagWorkRequest() {
	}

	public Integer getCodiAnagWrs() {
		return this.codiAnagWrs;
	}

	public void setCodiAnagWrs(Integer codiAnagWrs) {
		this.codiAnagWrs = codiAnagWrs;
	}

	public String getDescAnagWrs() {
		return this.descAnagWrs;
	}

	public void setDescAnagWrs(String descAnagWrs) {
		this.descAnagWrs = descAnagWrs;
	}

}