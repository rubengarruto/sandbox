package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_ZONE_REGIONI database table.
 * 
 */
@Entity
@Table(name="VIW_ZONE_REGIONI")
@NamedQueries({
	@NamedQuery(name="ViwZoneRegioni.findAll", query="SELECT v FROM ViwZoneRegioni v"),
	@NamedQuery(name="ViwZoneRegioni.findByCodiIdRegione", query="SELECT v FROM ViwZoneRegioni v where v.codiIdRegione IN (:selectedRegioni)"),
})
public class ViwZoneRegioni implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="UUID")
	private String uuid;
	
	@Column(name="CODI_ID_REGIONE")
	private Integer codiIdRegione;

	@Column(name="CODI_REL_ZONE_REGIONI")
	private Integer codiRelZoneRegioni;

	@Column(name="CODI_ZONA")
	private String codiZona;

	@Column(name="DESC_ZONA")
	private String descZona;

	public ViwZoneRegioni() {
	}

	public Integer getCodiIdRegione() {
		return codiIdRegione;
	}

	public void setCodiIdRegione(Integer codiIdRegione) {
		this.codiIdRegione = codiIdRegione;
	}

	public Integer getCodiRelZoneRegioni() {
		return codiRelZoneRegioni;
	}

	public void setCodiRelZoneRegioni(Integer codiRelZoneRegioni) {
		this.codiRelZoneRegioni = codiRelZoneRegioni;
	}

	public String getCodiZona() {
		return codiZona;
	}

	public void setCodiZona(String codiZona) {
		this.codiZona = codiZona;
	}

	public String getDescZona() {
		return descZona;
	}

	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	



}