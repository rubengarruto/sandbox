package com.rextart.saw.entity;

import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TAB_ERROR_DEVICE")
@Named("TabErrorDevice")
@NamedQueries({ 
@NamedQuery(name="TabErrorDevice.deleteByCodiUtenteImei", query="delete from TabErrorDevice t where t.codiUtente= :codiUtente and t.descImeiDevice= :descImeiDevice"),
@NamedQuery(name="TabErrorDevice.existError", query="select t from TabErrorDevice t where t.codiUtente= :codiUtente and t.numeVersionApp= :numeVersionApp and t.descErrore= :descErrore"),
@NamedQuery(name="TabErrorDevice.findAll", query="SELECT t FROM TabErrorDevice t"),
})
public class TabErrorDevice {
	@Id
	@Column(name = "CODI_ERROR_DEVICE")
	@SequenceGenerator(name = "TAB_ERROR_DEVICE_CODI_ERROR_DEVICE_GENERATOR", sequenceName = "SEQ_ERROR_DEVICE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAB_ERROR_DEVICE_CODI_ERROR_DEVICE_GENERATOR")
	private Integer codiErrorDevice;

	@Column(name = "CODI_UTENTE")
	private Integer codiUtente;

	@Column(name = "CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name = "CODI_CLIENTE")
	private Integer codiCliente;

	@Column(name = "DESC_ERRORE")
	private String descErrore;

	@Column(name = "DESC_TYPE_DEVICE")
	private String descTypeDevice;

	@Column(name = "NUME_LEVEL_API_DEVICE")
	private Integer numeLevelApiDevice;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_ERROR")
	private Date dateError;

	@Column(name = "NUME_VERSION_APP")
	private Integer numeVersionApp;

	@Column(name = "DESC_IMEI_DEVICE")
	private String descImeiDevice;
	
	@Column(name = "DESC_TYPE_ERROR")
	private String descTypeError;

	public String getDescImeiDevice() {
        return descImeiDevice;
    }

    public void setDescImeiDevice(String descImeiDevice) {
        this.descImeiDevice = descImeiDevice;
    }

    public Integer getNumeVersionApp() {
        return numeVersionApp;
    }

    public void setNumeVersionApp(Integer numeVersionApp) {
        this.numeVersionApp = numeVersionApp;
    }

    public Date getDateError() {
        return dateError;
    }

    public void setDateError(Date dateError) {
        this.dateError = dateError;
    }

    public Integer getCodiErrorDevice() {
        return codiErrorDevice;
    }

    public void setCodiErrorDevice(Integer codiErrorDevice) {
        this.codiErrorDevice = codiErrorDevice;
    }

    public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiCliente() {
        return codiCliente;
    }

    public void setCodiCliente(Integer codiCliente) {
        this.codiCliente = codiCliente;
    }

    public String getDescErrore() {
        return descErrore;
    }

    public void setDescErrore(String descErrore) {
        this.descErrore = descErrore;
    }

    public String getDescTypeDevice() {
        return descTypeDevice;
    }

    public void setDescTypeDevice(String descTypeDevice) {
        this.descTypeDevice = descTypeDevice;
    }

    public Integer getNumeLevelApiDevice() {
        return numeLevelApiDevice;
    }

    public void setNumeLevelApiDevice(Integer numeLevelApiDevice) {
        this.numeLevelApiDevice = numeLevelApiDevice;
    }

	public String getDescTypeError() {
		return descTypeError;
	}

	public void setDescTypeError(String descTypeError) {
		this.descTypeError = descTypeError;
	}
    
    
}
