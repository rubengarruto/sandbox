package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_LOGIN_FAILED database table.
 * 
 */
@Entity
@Table(name="TAB_LOGIN_FAILED")
@Named("TabLoginFailed")
@NamedQueries({
		@NamedQuery(name="TabLoginFailed.findAll", query="SELECT t FROM TabLoginFailed t"),
		@NamedQuery(name="TabLoginFailed.findByIp", query="SELECT t FROM TabLoginFailed t WHERE t.ipUser=:ipUser"),
		@NamedQuery(name="TabLoginFailed.deleteByIp", query="DELETE FROM TabLoginFailed t WHERE t.ipUser=:ipUser")
		})
public class TabLoginFailed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_LOGIN_FAILED_IDLOGINFAILED_GENERATOR", sequenceName="SEQ_LOGIN_FAILED",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_LOGIN_FAILED_IDLOGINFAILED_GENERATOR")
	@Column(name="ID_LOGIN_FAILED")
	private Integer idLoginFailed;

	@Column(name="DESC_USER")
	private String descUser;

	@Column(name="IP_USER")
	private String ipUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_LOGIN")
	private Calendar timeLogin;

	public TabLoginFailed() {
	}

	public Integer getIdLoginFailed() {
		return this.idLoginFailed;
	}

	public void setIdLoginFailed(Integer idLoginFailed) {
		this.idLoginFailed = idLoginFailed;
	}

	public String getDescUser() {
		return this.descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public String getIpUser() {
		return this.ipUser;
	}

	public void setIpUser(String ipUser) {
		this.ipUser = ipUser;
	}

	public Calendar getTimeLogin() {
		return this.timeLogin;
	}

	public void setTimeLogin(Calendar timeLogin) {
		this.timeLogin = timeLogin;
	}

}