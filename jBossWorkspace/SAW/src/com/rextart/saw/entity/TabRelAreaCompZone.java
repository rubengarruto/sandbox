package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_AREA_COMP_ZONE database table.
 * 
 */
@Entity
@Table(name="TAB_REL_AREA_COMP_ZONE")
@Named("TabRelAreaCompZone")
@NamedQuery(name="TabRelAreaCompZone.findAll", query="SELECT t FROM TabRelAreaCompZone t")
public class TabRelAreaCompZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_REL_AREA_COMP_ZONE")
	private Integer codiRelAreaCompZone;

	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	@Column(name="CODI_ZONA")
	private Integer codiZona;

	public TabRelAreaCompZone() {
	}

	public long getCodiRelAreaCompZone() {
		return this.codiRelAreaCompZone;
	}

	public void setCodiRelAreaCompZone(Integer codiRelAreaCompZone) {
		this.codiRelAreaCompZone = codiRelAreaCompZone;
	}

	public Integer getCodiAreaComp() {
		return this.codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiZona() {
		return this.codiZona;
	}

	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}

}