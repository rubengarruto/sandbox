package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_STATI_SESSIONE database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_STATI_SESSIONE")
@Named("TabAnagStatiSessione")
@NamedQueries({
	@NamedQuery(name="TabAnagStatiSessione.findAll", query="SELECT t FROM TabAnagStatiSessione t"),
	@NamedQuery(name="TabAnagStatiSessione.findStateAcquisito", query="SELECT t FROM TabAnagStatiSessione t where t.flagAcquisito=true"),
	@NamedQuery(name="TabAnagStatiSessione.findStateInLavorazione", query="SELECT t FROM TabAnagStatiSessione t where t.flagInLavorazione=true"),
	@NamedQuery(name="TabAnagStatiSessione.findStateDisponibile", query="SELECT t FROM TabAnagStatiSessione t where t.flagDisponibile=true"),
	@NamedQuery(name="TabAnagStatiSessione.findStateAnnullato", query="SELECT t FROM TabAnagStatiSessione t where t.flagAnnullato=true"),
	@NamedQuery(name="TabAnagStatiSessione.findStateChiuso", query="SELECT t FROM TabAnagStatiSessione t where t.flagChiuso=true"),
	@NamedQuery(name="TabAnagStatiSessione.findAllNotClosed", query="SELECT t FROM TabAnagStatiSessione t where t.flagChiuso=false"),
})
public class TabAnagStatiSessione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_STATO")
	private Integer codiStato;

	@Column(name="DESC_STATO", nullable=false, length=50)
	private String descStato;

	@Column(name="FLAG_ANNULLATO")
	private Boolean flagAnnullato;

	@Column(name="FLAG_CHIUSO")
	private Boolean flagChiuso;

	@Column(name="FLAG_ACQUISITO")
	private Boolean flagAcquisito;

	@Column(name="FLAG_IN_LAVORAZIONE")
	private Boolean flagInLavorazione;

	@Column(name="FLAG_DISPONIBILE")
	private Boolean flagDisponibile;

	@Column(name="NUME_PROG", nullable=false)
	private Boolean numeProg;

	public TabAnagStatiSessione() {
	}

	public Integer getCodiStato() {
		return codiStato;
	}

	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}

	public String getDescStato() {
		return descStato;
	}

	public void setDescStato(String descStato) {
		this.descStato = descStato;
	}

	public Boolean getFlagAnnullato() {
		return flagAnnullato;
	}

	public void setFlagAnnullato(Boolean flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	public Boolean getFlagChiuso() {
		return flagChiuso;
	}

	public void setFlagChiuso(Boolean flagChiuso) {
		this.flagChiuso = flagChiuso;
	}

	public Boolean getFlagAcquisito() {
		return flagAcquisito;
	}

	public void setFlagAcquisito(Boolean flagAcquisito) {
		this.flagAcquisito = flagAcquisito;
	}

	public Boolean getFlagInLavorazione() {
		return flagInLavorazione;
	}

	public void setFlagInLavorazione(Boolean flagInLavorazione) {
		this.flagInLavorazione = flagInLavorazione;
	}

	public Boolean getFlagDisponibile() {
		return flagDisponibile;
	}

	public void setFlagDisponibile(Boolean flagDisponibile) {
		this.flagDisponibile = flagDisponibile;
	}

	public Boolean getNumeProg() {
		return numeProg;
	}

	public void setNumeProg(Boolean numeProg) {
		this.numeProg = numeProg;
	}

	
}