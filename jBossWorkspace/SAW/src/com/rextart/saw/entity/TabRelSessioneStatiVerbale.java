package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_SESSIONE_STATI_VERBALE database table.
 * 
 */
@Entity
@Table(name="TAB_REL_SESSIONE_STATI_VERBALE")
@Named("TabRelSessioneStatiVerbale")
@NamedQueries({
	@NamedQuery(name="TabRelSessioneStatiVerbale.findAll", query="SELECT t FROM TabRelSessioneStatiVerbale t"),
	@NamedQuery(name="TabRelSessioneStatiVerbale.findByCodiOggettoCodiSess", query="SELECT t FROM TabRelSessioneStatiVerbale t where t.codiOggetto= :codiOggetto and t.codiSessione= :codiSessione"),
})


public class TabRelSessioneStatiVerbale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_SESSIONE_STATI_VERBALE", sequenceName="SEQ_SESSIONE_STATI_VERBALE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_SESSIONE_STATI_VERBALE")
	@Column(name="CODI_REL_SESS_STATO_VERBALE")
	private Integer codiRelSessStatoVerbale;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_STATO_VERBALE")
	private Integer codiStatoVerbale;

	public TabRelSessioneStatiVerbale() {
	}

	public Integer getCodiRelSessStatoVerbale() {
		return codiRelSessStatoVerbale;
	}

	public void setCodiRelSessStatoVerbale(Integer codiRelSessStatoVerbale) {
		this.codiRelSessStatoVerbale = codiRelSessStatoVerbale;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiStatoVerbale() {
		return codiStatoVerbale;
	}

	public void setCodiStatoVerbale(Integer codiStatoVerbale) {
		this.codiStatoVerbale = codiStatoVerbale;
	}

	

}