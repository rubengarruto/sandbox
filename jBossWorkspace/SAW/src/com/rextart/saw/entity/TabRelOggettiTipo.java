package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_OGGETTI_TIPO database table.
 * 
 */
@Entity
@Table(name="TAB_REL_OGGETTI_TIPO")
@Named("TabRelOggettiTipo")
@NamedQueries({ 
	@NamedQuery(name="TabRelOggettiTipo.findAll", query="SELECT t FROM TabRelOggettiTipo t"),
	@NamedQuery(name="TabRelOggettiTipo.findByListCodiOggetto", query="SELECT v FROM TabRelOggettiTipo v where v.codiOggetto IN (:listCodiOggetto)"),
})
public class TabRelOggettiTipo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_OGGETTI_TIPO_CODIOGGETTOTIPO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_OGGETTI_TIPO_CODIOGGETTOTIPO_GENERATOR")
	@Column(name="CODI_OGGETTO_TIPO")
	private int codiOggettoTipo;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	public TabRelOggettiTipo() {
	}

	public int getCodiOggettoTipo() {
		return this.codiOggettoTipo;
	}

	public void setCodiOggettoTipo(int codiOggettoTipo) {
		this.codiOggettoTipo = codiOggettoTipo;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	

}