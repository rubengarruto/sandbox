package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_BANDE database table.
 * 
 */
@Entity
@Table(name="TAB_BANDE")
@Named("TabBande")
@NamedQuery(name="TabBande.findAll", query="SELECT t FROM TabBande t")
public class TabBande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_BANDA")
	private Integer codiBanda;

	@Column(name="DESC_BANDA")
	private String descBanda;

	public TabBande() {
	}

	public Integer getCodiBanda() {
		return this.codiBanda;
	}

	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

}