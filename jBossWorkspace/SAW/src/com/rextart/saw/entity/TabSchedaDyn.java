package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_SCHEDA_DYN database table.
 * 
 */
@Entity
@Table(name="TAB_SCHEDA_DYN")
@NamedQueries({
	@NamedQuery(name="TabSchedaDyn.findAll", query="SELECT t FROM TabSchedaDyn t"),
	@NamedQuery(name="TabSchedaDyn.findByCodiSessione", query="SELECT t FROM TabSchedaDyn t where t.codiSessione= :codiSessione"),
	@NamedQuery(name="TabSchedaDyn.findByCodiSessioneList", query="SELECT t FROM TabSchedaDyn t where t.codiSessione IN (:codiSessioneList)"),
})
public class TabSchedaDyn implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;
	
	@Column(name="ACCESSIBILITA",columnDefinition="CLOB")
	private String accessibilita;
	
	@Lob
	@Column(name="CODICE_MAZZO_CHIAVI",columnDefinition="CLOB")
    private String codiceMazzoChiavi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE_SCHEDA")
	private Date dataCreazioneScheda;
	
	@Column(name="DISAGIATO",columnDefinition="CLOB")
	private String disagiato;

	@Column(name="FASCIA_INTERVENTO",columnDefinition="CLOB")
	private String fasciaIntervento;
	
	@Column(name="ITINERARIO",columnDefinition="CLOB")
	private String itinerario;

	@Column(name="NOTE_ACCESSO",columnDefinition="CLOB")
	private String noteAccesso;

	@Column(name="NOTE_ACCESSIBILITA",columnDefinition="CLOB")
	private String noteAccessibilita;

	@Column(name="REGIME",columnDefinition="CLOB")
	private String regime;

	@Column(name="TIPO_ACCESSO",columnDefinition="CLOB")
	private String tipoAccesso;
	
	@Column(name="VERSIONE")
	private String versione;

	public TabSchedaDyn() {
	}

	public String getAccessibilita() {
		return this.accessibilita;
	}

	public void setAccessibilita(String accessibilita) {
		this.accessibilita = accessibilita;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}


	public String getCodiceMazzoChiavi() {
		return codiceMazzoChiavi;
	}

	public void setCodiceMazzoChiavi(String codiceMazzoChiavi) {
		this.codiceMazzoChiavi = codiceMazzoChiavi;
	}

	public Date getDataCreazioneScheda() {
		return this.dataCreazioneScheda;
	}

	public void setDataCreazioneScheda(Date dataCreazioneScheda) {
		this.dataCreazioneScheda = dataCreazioneScheda;
	}

	public String getDisagiato() {
		return this.disagiato;
	}

	public void setDisagiato(String disagiato) {
		this.disagiato = disagiato;
	}

	public String getFasciaIntervento() {
		return this.fasciaIntervento;
	}

	public void setFasciaIntervento(String fasciaIntervento) {
		this.fasciaIntervento = fasciaIntervento;
	}

	public String getItinerario() {
		return this.itinerario;
	}

	public void setItinerario(String itinerario) {
		this.itinerario = itinerario;
	}

	public String getNoteAccesso() {
		return this.noteAccesso;
	}

	public void setNoteAccesso(String noteAccesso) {
		this.noteAccesso = noteAccesso;
	}

	public String getNoteAccessibilita() {
		return this.noteAccessibilita;
	}

	public void setNoteAccessibilita(String noteAccessibilita) {
		this.noteAccessibilita = noteAccessibilita;
	}

	public String getRegime() {
		return this.regime;
	}

	public void setRegime(String regime) {
		this.regime = regime;
	}

	public String getTipoAccesso() {
		return this.tipoAccesso;
	}

	public void setTipoAccesso(String tipoAccesso) {
		this.tipoAccesso = tipoAccesso;
	}

	public String getVersione() {
		return this.versione;
	}

	public void setVersione(String versione) {
		this.versione = versione;
	}

}