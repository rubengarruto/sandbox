package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_OGGETTI database table.
 * 
 */
@Entity
@Table(name="TAB_OGGETTI")
@Named("TabOggetti")
@NamedQueries({
	@NamedQuery(name="TabOggetti.findAll", query="SELECT t FROM TabOggetti t order by t.codiOggetto"),
	@NamedQuery(name="TabOggetti.findByCodiService", query="SELECT t FROM TabOggetti t, TabRelServiziOggetti r where t.codiOggetto=r.codiOggetto and r.codiServizio= :codiServizio"),
	@NamedQuery(name="TabOggetti.findByCodi", query="SELECT t FROM TabOggetti t where t.codiOggetto=:codiOggetto")
})
public class TabOggetti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_OGGETTI_CODIOGGETTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_OGGETTI_CODIOGGETTO_GENERATOR")
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_OGGETTO")
	private String descOggetto;

	@Column(name="DESC_SIGLA")
	private String descSigla;
	
	@Column(name="CODI_OGGETTO_PADRE")
	private Integer codiOggettoPadre;
	
	@Column(name="DESC_GERARCHIA")
	private String descGerarchia;


	
	public TabOggetti() {
	}

	public Integer getCodiOggetto() {
		return this.codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescOggetto() {
		return this.descOggetto;
	}

	public void setDescOggetto(String descOggetto) {
		this.descOggetto = descOggetto;
	}

	public String getDescSigla() {
		return this.descSigla;
	}

	public void setDescSigla(String descSigla) {
		this.descSigla = descSigla;
	}

	public Integer getCodiOggettoPadre() {
		return codiOggettoPadre;
	}

	public void setCodiOggettoPadre(Integer codiOggettoPadre) {
		this.codiOggettoPadre = codiOggettoPadre;
	}

	public String getDescGerarchia() {
		return descGerarchia;
	}

	public void setDescGerarchia(String descGerarchia) {
		this.descGerarchia = descGerarchia;
	}
	
	

}