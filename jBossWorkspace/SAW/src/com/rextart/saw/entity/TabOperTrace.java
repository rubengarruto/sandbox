package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_OPER_TRACE database table.
 * 
 */
@Entity
@Table(name="TAB_OPER_TRACE")
@Named("TabOperTrace")
@NamedQuery(name="TabOperTrace.findAll", query="SELECT t FROM TabOperTrace t")
public class TabOperTrace implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_OPER_TRACE_CODIOPERTRACE_GENERATOR",sequenceName="SEQ_OPER_TRACE",allocationSize=1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_OPER_TRACE_CODIOPERTRACE_GENERATOR")
	@Column(name="CODI_OPER_TRACE")
	private int codiOperTrace;

	@Column(name="CODI_ANAG_OPER_TRACE")
	private Integer codiAnagOperTrace;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;
	
	@Column(name="DESC_DETTAGLIO")
	private String descDettaglio;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_DATA_OPER")
	private Date timeDataOper;

	public TabOperTrace() {
	}

	public int getCodiOperTrace() {
		return this.codiOperTrace;
	}

	public void setCodiOperTrace(int codiOperTrace) {
		this.codiOperTrace = codiOperTrace;
	}

	public Integer getCodiAnagOperTrace() {
		return this.codiAnagOperTrace;
	}

	public void setCodiAnagOperTrace(Integer codiAnagOperTrace) {
		this.codiAnagOperTrace = codiAnagOperTrace;
	}

	public Integer getCodiUtente() {
		return this.codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Date getTimeDataOper() {
		return this.timeDataOper;
	}

	public void setTimeDataOper(Date timeDataOper) {
		this.timeDataOper = timeDataOper;
	}

	public String getDescDettaglio() {
		return descDettaglio;
	}

	public void setDescDettaglio(String descDettaglio) {
		this.descDettaglio = descDettaglio;
	}
	
	

}