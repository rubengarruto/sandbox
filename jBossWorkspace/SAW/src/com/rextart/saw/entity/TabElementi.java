package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ELEMENTI database table.
 * 
 */
@Entity
@Table(name="TAB_ELEMENTI")
@Named("TabElementi")
@NamedQueries({
	@NamedQuery(name="TabElementi.findAll", query="SELECT t FROM TabElementi t"),
	@NamedQuery(name="TabElementi.findByCodiTipoOggetto", query="select t from TabElementi t , TabRelOggettiElementi r where t.codiElemento=r.codiElemento and r.codiTipoOggetto= :codiTipoOggetto"),
	@NamedQuery(name="TabElementi.findByCodiElemento", query="select t from TabElementi t where t.codiElemento=:codiElemento"),
	
})
public class TabElementi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_ELEMENTI_CODIELEMENTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_ELEMENTI_CODIELEMENTO_GENERATOR")
	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_NOTE")
	private String descNote;


	public TabElementi() {
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

}