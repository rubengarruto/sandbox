package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_ROLE_STRUTTURA database table.
 * 
 */
@Entity
@Table(name="TAB_REL_ROLE_STRUTTURA")
@Named("TabRelRoleStruttura")
@NamedQuery(name="TabRelRoleStruttura.findAll", query="SELECT t FROM TabRelRoleStruttura t")
public class TabRelRoleStruttura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_REL_ROLE_STRUTTURA")
	private Integer codiRelRoleStruttura;

	@Column(name="CODI_ROLE")
	private Integer codiRole;

	@Column(name="CODI_STRUTTURA")
	private Integer codiStruttura;

	public TabRelRoleStruttura() {
	}

	public long getCodiRelRoleStruttura() {
		return this.codiRelRoleStruttura;
	}

	public void setCodiRelRoleStruttura(Integer codiRelRoleStruttura) {
		this.codiRelRoleStruttura = codiRelRoleStruttura;
	}

	public Integer getCodiRole() {
		return this.codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public Integer getCodiStruttura() {
		return this.codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

}