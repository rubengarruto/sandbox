package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_ANAG_LOV_CARATT database table.
 * 
 */
@Entity
@Table(name="VIW_ANAG_LOV_CARATT")
@Named("ViwAnagLovCaratt")
@NamedQueries({
		@NamedQuery(name="ViwAnagLovCaratt.findAll", query="SELECT v FROM ViwAnagLovCaratt v"),
		@NamedQuery(name="ViwAnagLovCaratt.findByCarattId", query="SELECT v FROM ViwAnagLovCaratt v WHERE v.codiCaratteristica = :codiCaratteristica order by v.descLabel"),
		@NamedQuery(name="ViwAnagLovCaratt.findByCaratt", query="SELECT v FROM ViwAnagLovCaratt v WHERE v.codiCaratteristica = :codiCaratteristica and v.descColumn = :descColumn order by v.descLabel"),
		@NamedQuery(name="ViwAnagLovCaratt.findAllCatalogo", query="SELECT v FROM ViwAnagLovCaratt v WHERE v.codiCatalogo is not null order by v.descLabel")
})
public class ViwAnagLovCaratt implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_ANAG_LOV")
	private Integer codiAnagLov;

	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_CATALOGO")
	private Integer codiCatalogo;

	@Column(name="CODI_DESC_CARATT")
	private Integer codiDescCaratt;

	@Column(name="CODI_NEXT_CARATT")
	private Integer codiNextCaratt;

	@Column(name="CODI_PREC_CARATT")
	private Integer codiPrecCaratt;

	@Column(name="DESC_COLUMN")
	private String descColumn;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="FLAG_ALTRO")
	private Integer flagAltro;

	public ViwAnagLovCaratt() {
	}

	public Integer getCodiAnagLov() {
		return this.codiAnagLov;
	}

	public void setCodiAnagLov(Integer codiAnagLov) {
		this.codiAnagLov = codiAnagLov;
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiCatalogo() {
		return this.codiCatalogo;
	}

	public void setCodiCatalogo(Integer codiCatalogo) {
		this.codiCatalogo = codiCatalogo;
	}

	public Integer getCodiDescCaratt() {
		return this.codiDescCaratt;
	}

	public void setCodiDescCaratt(Integer codiDescCaratt) {
		this.codiDescCaratt = codiDescCaratt;
	}

	public Integer getCodiNextCaratt() {
		return this.codiNextCaratt;
	}

	public void setCodiNextCaratt(Integer codiNextCaratt) {
		this.codiNextCaratt = codiNextCaratt;
	}

	public Integer getCodiPrecCaratt() {
		return this.codiPrecCaratt;
	}

	public void setCodiPrecCaratt(Integer codiPrecCaratt) {
		this.codiPrecCaratt = codiPrecCaratt;
	}

	public String getDescColumn() {
		return this.descColumn;
	}

	public void setDescColumn(String descColumn) {
		this.descColumn = descColumn;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public Integer getFlagAltro() {
		return this.flagAltro;
	}

	public void setFlagAltro(Integer flagAltro) {
		this.flagAltro = flagAltro;
	}

	public byte[] getUuid() {
		return uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

}