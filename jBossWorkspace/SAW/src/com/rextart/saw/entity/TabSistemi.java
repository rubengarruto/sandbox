package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_SISTEMI database table.
 * 
 */
@Entity
@Table(name="TAB_SISTEMI")
@Named("TabSistemi")
@NamedQuery(name="TabSistemi.findAll", query="SELECT t FROM TabSistemi t order by t.descSistema")
public class TabSistemi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_SISTEMA")
	private Integer codiSistema;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	public TabSistemi() {
	}

	public Integer getCodiSistema() {
		return this.codiSistema;
	}

	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

}