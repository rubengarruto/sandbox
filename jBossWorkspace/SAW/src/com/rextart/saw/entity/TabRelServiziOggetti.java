package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_SERVIZI_OGGETTI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_SERVIZI_OGGETTI")
@Named("TabRelServiziOggetti")
@NamedQuery(name="TabRelServiziOggetti.findAll", query="SELECT t FROM TabRelServiziOggetti t")
public class TabRelServiziOggetti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_SERVIZI_OGGETTI_CODIRELSERVOGGETTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_SERVIZI_OGGETTI_CODIRELSERVOGGETTO_GENERATOR")
	@Column(name="CODI_REL_SERV_OGGETTO")
	private int codiRelServOggetto;

	@Column(name="CODI_SERVIZIO")
	private Integer codiServizio;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	public TabRelServiziOggetti() {
	}

	public int getCodiRelServOggetto() {
		return this.codiRelServOggetto;
	}

	public void setCodiRelServOggetto(int codiRelServOggetto) {
		this.codiRelServOggetto = codiRelServOggetto;
	}

	public Integer getCodiServizio() {
		return this.codiServizio;
	}

	public void setCodiServizio(Integer codiServizio) {
		this.codiServizio = codiServizio;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	
}