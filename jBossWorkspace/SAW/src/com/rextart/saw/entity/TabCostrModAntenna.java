package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_COSTR_MOD_ANTENNA database table.
 * 
 */
@Entity
@Table(name="TAB_COSTR_MOD_ANTENNA")
@NamedQuery(name="TabCostrModAntenna.findAll", query="SELECT t FROM TabCostrModAntenna t order by t.descCostrMod")
public class TabCostrModAntenna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_COSTR_MOD")
	private Integer codiCostrMod;

	@Column(name="DESC_COSTR_MOD")
	private String descCostrMod;

	public TabCostrModAntenna() {
	}

	public Integer getCodiCostrMod() {
		return this.codiCostrMod;
	}

	public void setCodiCostrMod(Integer codiCostrMod) {
		this.codiCostrMod = codiCostrMod;
	}

	public String getDescCostrMod() {
		return this.descCostrMod;
	}

	public void setDescCostrMod(String descCostrMod) {
		this.descCostrMod = descCostrMod;
	}
}