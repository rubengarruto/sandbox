package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_GRUPPI database table.
 * 
 */
@Entity
@Table(name="TAB_GRUPPI")
@Named("TabGruppi")
@NamedQueries({
	@NamedQuery(name="TabGruppi.findAll", query="SELECT t FROM TabGruppi t"),
	@NamedQuery(name="TabGruppi.findByZona", query="SELECT g FROM TabGruppi g  where g.codiZona=:codiZona"),
	@NamedQuery(name="TabGruppi.findByDesc", query="SELECT g FROM TabGruppi g  where g.descGruppo=:descGruppo"),
})
public class TabGruppi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_GRUPPO")
	private Integer codiGruppo;

	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	@Column(name="CODI_ZONA")
	private Integer codiZona;

	@Column(name="DESC_GRUPPO")
	private String descGruppo;

	public TabGruppi() {
	}

	public Integer getCodiGruppo() {
		return this.codiGruppo;
	}

	public void setCodiGruppo(Integer codiGruppo) {
		this.codiGruppo = codiGruppo;
	}

	public Integer getCodiAreaComp() {
		return this.codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiZona() {
		return codiZona;
	}

	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}

	public String getDescGruppo() {
		return this.descGruppo;
	}

	public void setDescGruppo(String descGruppo) {
		this.descGruppo = descGruppo;
	}

}