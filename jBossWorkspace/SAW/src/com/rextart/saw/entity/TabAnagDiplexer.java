package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_ANAG_DIPLEXER")
@Named("TabAnagDiplexer")
@NamedQueries({
	@NamedQuery(name="TabAnagDiplexer.findAll", query="SELECT t FROM TabAnagDiplexer t"),
	@NamedQuery(name="TabAnagDiplexer.findByDescMarcaAndModello", query="SELECT t FROM TabAnagDiplexer t where t.descMarca=:descMarca and t.descModello=:descModello"),
	@NamedQuery(name="TabAnagDiplexer.findAllDistinctDescMarca", query="SELECT distinct descMarca FROM TabAnagDiplexer t"),
	@NamedQuery(name="TabAnagDiplexer.findByDescMarca", query="SELECT t FROM TabAnagDiplexer t where t.descMarca=:descMarca")
})
public class TabAnagDiplexer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TAB_ANAG_DIPLEXER_CODI_ANAG_DIPLEXER_GENERATOR", sequenceName="SEQ_ANAG_DIPLEXER",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_ANAG_DIPLEXER_CODI_ANAG_DIPLEXER_GENERATOR")
	@Column(name="CODI_ANAG_DIPLEXER")
	private Integer codiAnagDiplexer;
	
	
	@Column(name="DESC_MARCA")
	private String descMarca;
	
	
	@Column(name="DESC_MODELLO")
	private String descModello;
	
	
	@Column(name="FLAG_ATTIVO")
	private boolean flagAttivo;


	public String getDescMarca() {
		return descMarca;
	}


	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescModello() {
		return descModello;
	}


	public void setDescModello(String descModello) {
		this.descModello = descModello;
	}



	public Integer getCodiAnagDiplexer() {
		return codiAnagDiplexer;
	}


	public void setCodiAnagDiplexer(Integer codiAnagDiplexer) {
		this.codiAnagDiplexer = codiAnagDiplexer;
	}


	public boolean isFlagAttivo() {
		return flagAttivo;
	}


	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}
	
	
	
}
