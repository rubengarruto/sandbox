package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_SERVIZI database table.
 * 
 */
@Entity
@Table(name="TAB_SERVIZI")
@Named("TabServizi")
@NamedQuery(name="TabServizi.findAll", query="SELECT t FROM TabServizi t")
public class TabServizi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_SERVIZIO")
	private Integer codiServizio;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SERVIZIO")
	private String descServizio;

	@Column(name="DESC_SIGLA")
	private String descSigla;
	

	public TabServizi() {
	}

	public Integer getCodiServizio() {
		return this.codiServizio;
	}

	public void setCodiServizio(Integer codiServizio) {
		this.codiServizio = codiServizio;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescServizio() {
		return this.descServizio;
	}

	public void setDescServizio(String descServizio) {
		this.descServizio = descServizio;
	}

	public String getDescSigla() {
		return this.descSigla;
	}

	public void setDescSigla(String descSigla) {
		this.descSigla = descSigla;
	}

}