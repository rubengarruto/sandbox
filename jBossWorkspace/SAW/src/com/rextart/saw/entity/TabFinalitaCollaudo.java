package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_FINALITA_COLLAUDO database table.
 * 
 */
@Entity
@Table(name="TAB_FINALITA_COLLAUDO")
@Named("TabFinalitaCollaudo")
@NamedQuery(name="TabFinalitaCollaudo.findAll", query="SELECT t FROM TabFinalitaCollaudo t order by t.descFinalitaColl")
public class TabFinalitaCollaudo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_FINALITA_COLL")
	private Integer codiFinalitaColl;

	@Column(name="DESC_FINALITA_COLL")
	private String descFinalitaColl;

	public TabFinalitaCollaudo() {
	}

	public Integer getCodiFinalitaColl() {
		return this.codiFinalitaColl;
	}

	public void setCodiFinalitaColl(Integer codiFinalitaColl) {
		this.codiFinalitaColl = codiFinalitaColl;
	}

	public String getDescFinalitaColl() {
		return this.descFinalitaColl;
	}

	public void setDescFinalitaColl(String descFinalitaColl) {
		this.descFinalitaColl = descFinalitaColl;
	}

}