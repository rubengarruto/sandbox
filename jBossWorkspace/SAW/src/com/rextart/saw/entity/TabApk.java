package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="TAB_APK")
@Named("TabApk")
@NamedQueries({
	@NamedQuery(name="TabApk.getNumeLastVersion", query="SELECT t.numeVersione FROM TabApk t order by t.numeVersione desc"),
	@NamedQuery(name="TabApk.getLastVersion", query="SELECT t FROM TabApk t order by t.numeVersione desc"),
	@NamedQuery(name="TabApk.getFromVersion", query="SELECT t FROM TabApk t where t.numeVersione > :numeVersione"),
	@NamedQuery(name="TabApk.getScriptDbFromVersion", query="SELECT t.descScriptDb FROM TabApk t where t.numeVersione > :numeVersione"),
	@NamedQuery(name="TabApk.getApkLastVersion", query="SELECT t FROM TabApk t where t.numeVersione = :numeVersione"),
})
public class TabApk implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_APK")
	private Integer codiApk;

	@Column(name="NUME_VERSIONE")
	private Integer numeVersione;

	@Column(name="DATA_VERSIONE")
	private Date dataVersione;
	
	@Column(name="DESC_SCRIPT_DB")
	private String descScriptDb;
	
	@Column(name="DESC_RELEASE")
	private String descRelease;
	
    public String getDescRelease() {
        return descRelease;
    }

    public void setDescRelease(String descRelease) {
        this.descRelease = descRelease;
    }

	public Integer getCodiApk() {
		return codiApk;
	}

	public void setCodiApk(Integer codiApk) {
		this.codiApk = codiApk;
	}

	public Integer getNumeVersione() {
		return numeVersione;
	}

	public void setNumeVersione(Integer numeVersione) {
		this.numeVersione = numeVersione;
	}

	public Date getDataVersione() {
		return dataVersione;
	}

	public void setDataVersione(Date dataVersione) {
		this.dataVersione = dataVersione;
	}

	public String getDescScriptDb() {
		return descScriptDb;
	}

	public void setDescScriptDb(String descScriptDb) {
		this.descScriptDb = descScriptDb;
	}


}
