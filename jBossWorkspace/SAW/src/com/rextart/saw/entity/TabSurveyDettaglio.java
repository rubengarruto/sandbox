package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the TAB_SURVEY_DETTAGLIO database table.
 * 
 */
@Entity
@Table(name="TAB_SURVEY_DETTAGLIO")
@Named("TabSurveyDettaglio")
@NamedQueries({
	@NamedQuery(name="TabSurveyDettaglio.findAll", query="SELECT t FROM TabSurveyDettaglio t"),
	@NamedQuery(name="TabSurveyDettaglio.deleteByCodiSurvey", query="delete from TabSurveyDettaglio t where t.codiSurvey= :codiSurvey"),
	@NamedQuery(name="TabSurveyDettaglio.getListByCodiSurvey", query="SELECT t FROM TabSurveyDettaglio t where t.codiSurvey IN (:listCodiSurvey)"),
	@NamedQuery(name="TabSurveyDettaglio.findByCodiDettaglio", query="SELECT t FROM TabSurveyDettaglio t where t.codiDettaglio= :codiDettaglio"),
	@NamedQuery(name="TabSurveyDettaglio.findValorizedSurveyDettaglioByCodiCaratteristica", query="SELECT t FROM TabSurveyDettaglio t where t.codiSurvey = :codiSurvey and t.codiCaratteristica = :codiCaratteristica and (t.codiAnagLov is not null or t.descValore is not null)"),
	@NamedQuery(name="TabSurveyDettaglio.findByFormKey", query="SELECT t FROM TabSurveyDettaglio t where t.codiSurvey = :codiSurvey and t.codiElemento = :codiElemento and t.codiCaratteristica = :codiCaratteristica and t.descLabel = :descLabel and t.numeCella = :numeCella and t.numeVia = :numeVia and t.numeLivelloE = :numeLivelloE"),
	@NamedQuery(name="TabSurveyDettaglio.findByFormKeyWithoutDescLabel", query="SELECT t FROM TabSurveyDettaglio t where t.codiSurvey = :codiSurvey and t.codiElemento = :codiElemento and t.codiCaratteristica = :codiCaratteristica and t.numeCella = :numeCella and t.numeVia = :numeVia and t.numeLivelloE = :numeLivelloE and t.descLabel is null")
})
public class TabSurveyDettaglio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_SURVEY_DETTAGLIO_CODIDETTAGLIO_GENERATOR", sequenceName="SEQ_SURVEY_DETTAGLIO",allocationSize=1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_SURVEY_DETTAGLIO_CODIDETTAGLIO_GENERATOR")
	@Column(name="CODI_DETTAGLIO")
	private int codiDettaglio;

	@Column(name="CODI_ANAG_LOV")
	private Integer codiAnagLov;

	@Column(name="CODI_CARATTERISTICA", nullable=false)
	@NotNull(message="� richiesto un valore")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO", nullable=false)
	@NotNull(message="� richiesto un valore")
	private Integer codiElemento;

	@Column(name="CODI_GRUPPO")
	private Integer codiGruppo;

	@Column(name="CODI_SURVEY", nullable=false)
	@NotNull(message="� richiesto un valore")
	private Integer codiSurvey;
	
	@Column(name="DESC_VALORE")
	private String descValore;

	//bi-directional many-to-one association to TabSurvey
	@ManyToOne
	@JoinColumn(name="CODI_SURVEY",insertable= false, updatable=false)
	private TabSurvey tabSurvey;

	@Column(name="CODI_CATALOGO")
	private Integer codiCatalogo;
	
	@Column(name="DESC_NOTE")
	private String descNote;
	
	@Column(name="DESC_LABEL")
	private String descLabel;
	
	@Column(name="NUME_CELLA")
	private Integer numeCella;
	
	@Column(name="NUME_VIA")
	private Integer numeVia;
	
	@Column(name="NUME_BRANCH")
	private Integer numeBranch;
	
	@Column(name="NUME_SETTORE")
	private Integer numeSettore;
	
	@Column(name="NUME_LIVELLO_E")
	private Integer numeLivelloE;
	

	public TabSurveyDettaglio() {
	}

	public int getCodiDettaglio() {
		return this.codiDettaglio;
	}

	public String getDescValore() {
		return this.descValore;
	}

	public void setDescValore(String descValore) {
		this.descValore = descValore;
	}
	
	public Integer getCodiAnagLov() {
		return codiAnagLov;
	}

	public void setCodiAnagLov(Integer codiAnagLov) {
		this.codiAnagLov = codiAnagLov;
	}

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiGruppo() {
		return codiGruppo;
	}

	public void setCodiGruppo(Integer codiGruppo) {
		this.codiGruppo = codiGruppo;
	}

	public void setCodiDettaglio(int codiDettaglio) {
		this.codiDettaglio = codiDettaglio;
	}

	public TabSurvey getTabSurvey() {
		return this.tabSurvey;
	}

	public void setTabSurvey(TabSurvey tabSurvey) {
		this.tabSurvey = tabSurvey;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public Integer getCodiCatalogo() {
		return codiCatalogo;
	}

	public void setCodiCatalogo(Integer codiCatalogo) {
		this.codiCatalogo = codiCatalogo;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public Integer getNumeCella() {
		return numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public Integer getNumeVia() {
		return numeVia;
	}

	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}

	public Integer getNumeBranch() {
		return numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getNumeSettore() {
		return numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeLivelloE() {
		return numeLivelloE;
	}

	public void setNumeLivelloE(Integer numeLivelloE) {
		this.numeLivelloE = numeLivelloE;
	}
}