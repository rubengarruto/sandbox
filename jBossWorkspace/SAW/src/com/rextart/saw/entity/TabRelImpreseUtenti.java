package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_IMPRESE_UTENTI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_IMPRESE_UTENTI")
@Named("TabRelImpreseUtenti")
@NamedQueries({
	@NamedQuery(name="TabRelImpreseUtenti.findAll", query="SELECT t FROM TabRelImpreseUtenti t"),
	@NamedQuery(name="TabRelImpreseUtenti.findAllByCodiImp", query="SELECT t FROM TabRelImpreseUtenti t where t.codiImpresa= :codiImpresa "),
	@NamedQuery(name="TabRelImpreseUtenti.findByCodiUtente", query="SELECT t FROM TabRelImpreseUtenti t where t.codiUtente= :codiUtente "),
	@NamedQuery(name="TabRelImpreseUtenti.deleteByCodiUtente", query="delete from TabRelImpreseUtenti t where t.codiUtente= :codiUtente ")
})
public class TabRelImpreseUtenti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_IMPRESE_UTENTIE_CODIRELIMPRESAUTENTE_GENERATOR", sequenceName="SEQ_REL_IMPRESA_UTENTE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_IMPRESE_UTENTIE_CODIRELIMPRESAUTENTE_GENERATOR")
	@Column(name="CODI_REL_IMPRESA_UTENTE")
	private int codiRelImpresaUtente;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;
	
	@Column(name="CODI_IMPRESA")
	private Integer codiImpresa;

	public TabRelImpreseUtenti() {
	}

	public int getCodiRelImpresaUtente() {
		return this.codiRelImpresaUtente;
	}

	public void setCodiRelImpresaUtente(int codiRelImpresaUtente) {
		this.codiRelImpresaUtente = codiRelImpresaUtente;
	}

	public Integer getCodiUtente() {
		return this.codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Integer getCodiImpresa() {
		return codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

}