package com.rextart.saw.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the TAB_SURVEY database table.
 * 
 */
@Entity
@Table(name="TAB_SURVEY")
@Named("TabSurvey")
@NamedQueries({ 
@NamedQuery(name="TabSurvey.findAll", query="SELECT t FROM TabSurvey t"),
@NamedQuery(name="TabSurvey.findByCodiSessioni", query="SELECT t FROM TabSurvey t where t.codiSessione IN (:listCodiSessione)"),
@NamedQuery(name="TabSurvey.deleteByCodiSurvey", query="delete from TabSurvey t where t.codiSurvey= :codiSurvey"),
@NamedQuery(name="TabSurvey.findCountByCodiSessione", query = "SELECT COUNT(t) FROM TabSurvey t where t.codiSessione=:codiSessione"),
@NamedQuery(name="TabSurvey.findByCodiSessione", query="SELECT t FROM TabSurvey t where t.codiSessione=:codiSessione"),
@NamedQuery(name="TabSurvey.deleteByCodiSessione", query="delete from TabSurvey t where t.codiSessione= :codiSessione"),
@NamedQuery(name="TabSurvey.findByCodiSessioneCodiTipoOggetto", query="SELECT t FROM TabSurvey t where t.codiSessione=:codiSessione and t.codiTipoOggetto=:codiTipoOggetto"),
@NamedQuery(name="TabSurvey.findByCodiSessioneCodiOggetto", query="SELECT t FROM TabSurvey t where t.codiSessione=:codiSessione and t.codiOggetto=:codiOggetto"),

})
public class TabSurvey implements Serializable {
	private static final long serialVersionUID = 1L;
			
	@Id
	@SequenceGenerator(name="TAB_SURVEY_CODISURVEY_GENERATOR", sequenceName="SEQ_SURVEY",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_SURVEY_CODISURVEY_GENERATOR")
	@Column(name="CODI_SURVEY")
	private int codiSurvey;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;


	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREATE")
	private Date timeCreate= new Timestamp(System.currentTimeMillis());

	
	@ManyToOne
	@JoinColumn(name="CODI_TIPO_OGGETTO",insertable=false, updatable=false)
	private TabTipologieOggetto tabTipoOggetto;
	
	
	@Transient
	private boolean flagEsito=false;
	
	public TabSurvey() {
	}
	


	public int getCodiSurvey() {
		return this.codiSurvey;
	}

	
	
	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}


	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public void setCodiSurvey(int codiSurvey) {
		this.codiSurvey = codiSurvey;
	}


	public TabTipologieOggetto getTabTipoOggetto() {
		return tabTipoOggetto;
	}



	public void setTabTipoOggetto(TabTipologieOggetto tabTipoOggetto) {
		this.tabTipoOggetto = tabTipoOggetto;
	}



	public Date getTimeCreate() {
		return timeCreate;
	}



	public void setTimeCreate(Date timeCreate) {
		this.timeCreate = timeCreate;
	}



	public boolean isFlagEsito() {
		return flagEsito;
	}



	public void setFlagEsito(boolean flagEsito) {
		this.flagEsito = flagEsito;
	}

}