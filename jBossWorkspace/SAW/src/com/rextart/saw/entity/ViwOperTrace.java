package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the VIW_OPER_TRACE database table.
 * 
 */
@Entity
@Table(name="VIW_OPER_TRACE")
@NamedQuery(name="ViwOperTrace.findAll", query="SELECT v FROM ViwOperTrace v ORDER BY v.timeDataOper desc")
public class ViwOperTrace implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CODI_OPER_TRACE")
	private Integer codiOperTrace;

	@Column(name="DESC_DETTAGLIO")
	private String descDettaglio;

	@Column(name="DESC_OPER_TRACE")
	private String descOperTrace;

	@Column(name="DESC_USER")
	private String descUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_DATA_OPER")
	private Date timeDataOper;
	
	@Column(name="DESC_NOME")
	private String descNome;

	@Column(name="DESC_CGN")
	private String descCgn;


	public ViwOperTrace() {
	}

	public Integer getCodiOperTrace() {
		return codiOperTrace;
	}

	public void setCodiOperTrace(Integer codiOperTrace) {
		this.codiOperTrace = codiOperTrace;
	}

	public String getDescDettaglio() {
		return this.descDettaglio;
	}

	public void setDescDettaglio(String descDettaglio) {
		this.descDettaglio = descDettaglio;
	}

	public String getDescOperTrace() {
		return this.descOperTrace;
	}

	public void setDescOperTrace(String descOperTrace) {
		this.descOperTrace = descOperTrace;
	}

	public String getDescUser() {
		return this.descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public Date getTimeDataOper() {
		return this.timeDataOper;
	}

	public void setTimeDataOper(Date timeDataOper) {
		this.timeDataOper = timeDataOper;
	}

	public String getDescNome() {
		return descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescCgn() {
		return descCgn;
	}

	public void setDescCgn(String descCgn) {
		this.descCgn = descCgn;
	}
	
}