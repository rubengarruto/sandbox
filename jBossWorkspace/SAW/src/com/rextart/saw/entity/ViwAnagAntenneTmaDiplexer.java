package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_ANAG_ANTENNE_TMA_DIPLEXER database table.
 * 
 */
@Entity
@Table(name="VIW_ANAG_ANTENNE_TMA_DIPLEXER")
@NamedQueries({
	@NamedQuery(name="ViwAnagAntenneTmaDiplexer.findAll", query="SELECT v FROM ViwAnagAntenneTmaDiplexer v order by v.descMarca, v.descModello"),
	@NamedQuery(name="ViwAnagAntenneTmaDiplexer.findByCodiCatalogo", query="SELECT v FROM ViwAnagAntenneTmaDiplexer v WHERE v.codiCatalogo = :codiCatalogo order by v.descMarca, v.descModello"),
	@NamedQuery(name="ViwAnagAntenneTmaDiplexer.findDistinctDescMarcaByCodiCatalogo", query="SELECT DISTINCT(v.descMarca) FROM ViwAnagAntenneTmaDiplexer v WHERE v.codiCatalogo = :codiCatalogo order by v.descMarca"),
	@NamedQuery(name="ViwAnagAntenneTmaDiplexer.findByDescMarca", query="SELECT v FROM ViwAnagAntenneTmaDiplexer v where v.codiCatalogo = :codiCatalogo AND v.descMarca=:descMarca order by v.descMarca, v.descModello")})
	public class ViwAnagAntenneTmaDiplexer implements Serializable {
		private static final long serialVersionUID = 1L;

		@Column(name="CODI_ANAG")
		private Integer codiAnag;

		@Column(name="CODI_CATALOGO")
		private Integer codiCatalogo;

		@Column(name="DESC_MARCA")
		private String descMarca;

		@Column(name="DESC_MODELLO")
		private String descModello;

		@Column(name="FLAG_ATTIVO")
		private Integer flagAttivo;

		@Id
		@Column(name="\"UUID\"")
		private byte[] uuid;

		public ViwAnagAntenneTmaDiplexer() {
		}

		public Integer getCodiAnag() {
			return this.codiAnag;
		}

		public void setCodiAnag(Integer codiAnag) {
			this.codiAnag = codiAnag;
		}

		public Integer getCodiCatalogo() {
			return this.codiCatalogo;
		}

		public void setCodiCatalogo(Integer codiCatalogo) {
			this.codiCatalogo = codiCatalogo;
		}

		public String getDescMarca() {
			return this.descMarca;
		}

		public void setDescMarca(String descMarca) {
			this.descMarca = descMarca;
		}

		public String getDescModello() {
			return this.descModello;
		}

		public void setDescModello(String descModello) {
			this.descModello = descModello;
		}

		public Integer getFlagAttivo() {
			return this.flagAttivo;
		}

		public void setFlagAttivo(Integer flagAttivo) {
			this.flagAttivo = flagAttivo;
		}

		public byte[] getUuid() {
			return this.uuid;
		}

		public void setUuid(byte[] uuid) {
			this.uuid = uuid;
		}

	}