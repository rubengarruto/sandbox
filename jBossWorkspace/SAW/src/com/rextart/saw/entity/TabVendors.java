package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_VENDORS database table.
 * 
 */
@Entity
@Table(name="TAB_VENDORS")
@Named("TabVendors")
@NamedQueries({
@NamedQuery(name="TabVendors.findAll", query="SELECT t FROM TabVendors t"),
@NamedQuery(name="TabVendors.findByDesc", query="SELECT t FROM TabVendors t where t.descVendor =:descVendor"),
@NamedQuery(name="TabVendors.findByCodi", query="SELECT t FROM TabVendors t where t.codiVendor =:codiVendor")
})
public class TabVendors implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_VENDOR")
	private Integer codiVendor;

	@Column(name="DESC_VENDOR")
	private String descVendor;

	public TabVendors() {
	}

	public Integer getCodiVendor() {
		return this.codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}

	public String getDescVendor() {
		return this.descVendor;
	}

	public void setDescVendor(String descVendor) {
		this.descVendor = descVendor;
	}

}