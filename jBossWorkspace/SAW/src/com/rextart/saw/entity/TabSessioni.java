package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the TAB_SESSIONI database table.
 * 
 */
@Entity
@Table(name="TAB_SESSIONI")
@Named("TabSessioni")
@NamedQueries({
	@NamedQuery(name="TabSessioni.findAll", query="SELECT t FROM TabSessioni t "),
	@NamedQuery(name ="TabSessioni.findByCodiSessione", query="SELECT s FROM TabSessioni s where s.codiSessione= :codiSessione"),
})
public class TabSessioni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_SESSIONI_CODISESSIONE_GENERATOR", sequenceName="SEQ_SESSIONI", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_SESSIONI_CODISESSIONE_GENERATOR")
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CLLI_SITO", nullable=false)
	@NotNull(message="� richiesto un valore")
	private String clliSito;

	@Column(name="CODI_AREA_COMP",nullable=false)
	@NotNull(message="� richiesto un valore")
	private Integer codiAreaComp;

	@Column(name="CODI_BANDA")
	private Integer codiBanda;

	@Column(name="CODI_COLLAUDO",nullable=false)
	@NotNull(message="� richiesto un valore")
	private Integer codiCollaudo;

	@Column(name="CODI_DETTAGLIO_COLL")
	private Integer codiDettaglioColl;

	@Column(name="CODI_FINALITA_COLL")
	private Integer codiFinalitaColl;

	@Column(name="CODI_SISTEMA")
	private Integer codiSistema;

	@Column(name="CODI_TIPO_IMPIANTO")
	private Integer codiTipoImpianto;

	@Column(name="CODI_UNITA_TERR")
	private Integer codiUnitaTerr;

	@Column(name="CODICE_DBR",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String codiceDbr;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_SCHEDA_PR_RA_ES",nullable=false)
	@NotNull(message="� richiesto un valore")
	private Date dataSchedaPrRaEs;

	@Column(name="DESC_CODICE",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descCodice;

	@Column(name="DESC_COMUNE",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descComune;

	@Column(name="DESC_NOME_SITO",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_OPER_TIM")
	private String descOperTim;

	@Column(name="DESC_PROGETTO_RADIO")
	private String descProgettoRadio;

	@Column(name="DESC_PROVINCIA",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descProvincia;

	@Column(name="DESC_REGIONE",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descRegione;

	@Column(name="FLG_ALLEGATO_A")
	private Boolean flgAllegatoA=false;

	@Column(name="FLG_ALLEGATO_B")
	private Boolean flgAllegatoB=false;

	@Column(name="FLG_ALLEGATO_C")
	private Boolean flgAllegatoC=false;

	@Column(name="FLG_ALLEGATO_D")
	private Boolean flgAllegatoD=false;

	@Column(name="FLG_ALLEGATO_E")
	private Boolean flgAllegatoE=false;

	@Column(name="FLG_APPARATO_ERICSSON")
	private Boolean flgApparatoEricsson=false;

	@Column(name="FLG_APPARATO_HUAWEI")
	private Boolean flgApparatoHuawei=false;

	@Column(name="FLG_APPARATO_NSN")
	private Boolean flgApparatoNsn=false;

	@Column(name="FLG_LOCK_A")
	private Boolean flgLockA=false;

	@Column(name="FLG_LOCK_B")
	private Boolean flgLockB=false;

	@Column(name="FLG_LOCK_C")
	private Boolean flgLockC=false;

	@Column(name="FLG_LOCK_D")
	private Boolean flgLockD=false;

	@Column(name="FLG_LOCK_E")
	private Boolean flgLockE=false;

	@Column(name="FLG_LOCK_ERICSSON")
	private Boolean flgLockEricsson=false;

	@Column(name="FLG_LOCK_HUAWEI")
	private Boolean flgLockHuawei=false;

	@Column(name="FLG_LOCK_NSN")
	private Boolean flgLockNsn=false;

	@Column(name="DESC_INDIRIZZO",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descIndirizzo;

	@Column(name="DESC_LATITUDINE",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descLatitudine;

	@Column(name="DESC_LONGITUDINE",nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descLongitudine;
	
	@Column(name="FLG_ALLEGATO_D1_REMOTO")
	private Boolean flgAllegatoD1 = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_ACQUISIZIONE")
	private Date timeAcquisizione;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CHIUSO")
	private Date timeChiuso;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_PREVISTO_COLLAUDO")
	private Date timePrevistoCollaudo;
	
	
	@Column(name="FLG_SPEDITO_PEV")
	private Boolean flgSpeditoPev = false;

	@Transient
	private Integer codiVendor;
	

	public TabSessioni() {
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getClliSito() {
		return clliSito;
	}

	public void setClliSito(String clliSito) {
		this.clliSito = clliSito;
	}

	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiBanda() {
		return codiBanda;
	}

	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}

	public Integer getCodiCollaudo() {
		return codiCollaudo;
	}

	public void setCodiCollaudo(Integer codiCollaudo) {
		this.codiCollaudo = codiCollaudo;
	}

	public Integer getCodiDettaglioColl() {
		return codiDettaglioColl;
	}

	public void setCodiDettaglioColl(Integer codiDettaglioColl) {
		this.codiDettaglioColl = codiDettaglioColl;
	}

	public Integer getCodiFinalitaColl() {
		return codiFinalitaColl;
	}

	public void setCodiFinalitaColl(Integer codiFinalitaColl) {
		this.codiFinalitaColl = codiFinalitaColl;
	}

	public Integer getCodiSistema() {
		return codiSistema;
	}

	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}

	public Integer getCodiTipoImpianto() {
		return codiTipoImpianto;
	}

	public void setCodiTipoImpianto(Integer codiTipoImpianto) {
		this.codiTipoImpianto = codiTipoImpianto;
	}

	public Integer getCodiUnitaTerr() {
		return codiUnitaTerr;
	}

	public void setCodiUnitaTerr(Integer codiUnitaTerr) {
		this.codiUnitaTerr = codiUnitaTerr;
	}

	public String getCodiceDbr() {
		return codiceDbr;
	}

	public void setCodiceDbr(String codiceDbr) {
		this.codiceDbr = codiceDbr;
	}

	public Date getDataSchedaPrRaEs() {
		return dataSchedaPrRaEs;
	}

	public void setDataSchedaPrRaEs(Date dataSchedaPrRaEs) {
		this.dataSchedaPrRaEs = dataSchedaPrRaEs;
	}

	public String getDescCodice() {
		return descCodice;
	}

	public void setDescCodice(String descCodice) {
		this.descCodice = descCodice;
	}

	public String getDescComune() {
		return descComune;
	}

	public void setDescComune(String descComune) {
		this.descComune = descComune;
	}

	public String getDescNomeSito() {
		return descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescOperTim() {
		return descOperTim;
	}

	public void setDescOperTim(String descOperTim) {
		this.descOperTim = descOperTim;
	}

	public String getDescProgettoRadio() {
		return descProgettoRadio;
	}

	public void setDescProgettoRadio(String descProgettoRadio) {
		this.descProgettoRadio = descProgettoRadio;
	}

	public String getDescProvincia() {
		return descProvincia;
	}

	public void setDescProvincia(String descProvincia) {
		this.descProvincia = descProvincia;
	}

	public String getDescRegione() {
		return descRegione;
	}

	public void setDescRegione(String descRegione) {
		this.descRegione = descRegione;
	}

	public Boolean getFlgAllegatoA() {
		return flgAllegatoA;
	}

	public void setFlgAllegatoA(Boolean flgAllegatoA) {
		this.flgAllegatoA = flgAllegatoA;
	}

	public Boolean getFlgAllegatoB() {
		return flgAllegatoB;
	}

	public void setFlgAllegatoB(Boolean flgAllegatoB) {
		this.flgAllegatoB = flgAllegatoB;
	}

	public Boolean getFlgAllegatoC() {
		return flgAllegatoC;
	}

	public void setFlgAllegatoC(Boolean flgAllegatoC) {
		this.flgAllegatoC = flgAllegatoC;
	}

	public Boolean getFlgAllegatoD() {
		return flgAllegatoD;
	}

	public void setFlgAllegatoD(Boolean flgAllegatoD) {
		this.flgAllegatoD = flgAllegatoD;
	}

	public Boolean getFlgAllegatoE() {
		return flgAllegatoE;
	}

	public void setFlgAllegatoE(Boolean flgAllegatoE) {
		this.flgAllegatoE = flgAllegatoE;
	}

	public Boolean getFlgApparatoEricsson() {
		return flgApparatoEricsson;
	}

	public void setFlgApparatoEricsson(Boolean flgApparatoEricsson) {
		this.flgApparatoEricsson = flgApparatoEricsson;
	}

	public Boolean getFlgApparatoHuawei() {
		return flgApparatoHuawei;
	}

	public void setFlgApparatoHuawei(Boolean flgApparatoHuawei) {
		this.flgApparatoHuawei = flgApparatoHuawei;
	}

	public Boolean getFlgApparatoNsn() {
		return flgApparatoNsn;
	}

	public void setFlgApparatoNsn(Boolean flgApparatoNsn) {
		this.flgApparatoNsn = flgApparatoNsn;
	}

	public Boolean getFlgLockA() {
		return flgLockA;
	}

	public void setFlgLockA(Boolean flgLockA) {
		this.flgLockA = flgLockA;
	}

	public Boolean getFlgLockB() {
		return flgLockB;
	}

	public void setFlgLockB(Boolean flgLockB) {
		this.flgLockB = flgLockB;
	}

	public Boolean getFlgLockC() {
		return flgLockC;
	}

	public void setFlgLockC(Boolean flgLockC) {
		this.flgLockC = flgLockC;
	}

	public Boolean getFlgLockD() {
		return flgLockD;
	}

	public void setFlgLockD(Boolean flgLockD) {
		this.flgLockD = flgLockD;
	}

	public Boolean getFlgLockE() {
		return flgLockE;
	}

	public void setFlgLockE(Boolean flgLockE) {
		this.flgLockE = flgLockE;
	}

	public Boolean getFlgLockEricsson() {
		return flgLockEricsson;
	}

	public void setFlgLockEricsson(Boolean flgLockEricsson) {
		this.flgLockEricsson = flgLockEricsson;
	}

	public Boolean getFlgLockHuawei() {
		return flgLockHuawei;
	}

	public void setFlgLockHuawei(Boolean flgLockHuawei) {
		this.flgLockHuawei = flgLockHuawei;
	}

	public Boolean getFlgLockNsn() {
		return flgLockNsn;
	}

	public void setFlgLockNsn(Boolean flgLockNsn) {
		this.flgLockNsn = flgLockNsn;
	}

	public Date getTimeAcquisizione() {
		return timeAcquisizione;
	}

	public void setTimeAcquisizione(Date timeAcquisizione) {
		this.timeAcquisizione = timeAcquisizione;
	}

	public Date getTimeChiuso() {
		return timeChiuso;
	}

	public void setTimeChiuso(Date timeChiuso) {
		this.timeChiuso = timeChiuso;
	}

	public String getDescIndirizzo() {
		return descIndirizzo;
	}

	public void setDescIndirizzo(String descIndirizzo) {
		this.descIndirizzo = descIndirizzo;
	}

	public String getDescLatitudine() {
		return descLatitudine;
	}

	public void setDescLatitudine(String descLatitudine) {
		this.descLatitudine = descLatitudine;
	}

	public String getDescLongitudine() {
		return descLongitudine;
	}

	public void setDescLongitudine(String descLongitudine) {
		this.descLongitudine = descLongitudine;
	}

	public Date getTimePrevistoCollaudo() {
		return timePrevistoCollaudo;
	}

	public void setTimePrevistoCollaudo(Date timePrevistoCollaudo) {
		this.timePrevistoCollaudo = timePrevistoCollaudo;
	}

	public Boolean getFlgAllegatoD1() {
		return flgAllegatoD1;
	}

	public void setFlgAllegatoD1(Boolean flgAllegatoD1) {
		this.flgAllegatoD1 = flgAllegatoD1;
	}

	public Integer getCodiVendor() {
		return codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}

	public Boolean getFlgSpeditoPev() {
		return flgSpeditoPev;
	}

	public void setFlgSpeditoPev(Boolean flgSpeditoPev) {
		this.flgSpeditoPev = flgSpeditoPev;
	}
	
	
}