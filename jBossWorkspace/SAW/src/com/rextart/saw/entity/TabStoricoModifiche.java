package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_STORICO_MODIFICHE database table.
 * 
 */
@Entity
@Table(name="TAB_STORICO_MODIFICHE")
@Named("TabStoricoModifiche")
@NamedQuery(name="TabStoricoModifiche.findAll", query="SELECT t FROM TabStoricoModifiche t")
public class TabStoricoModifiche implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_STORICO_MODIFICHE_CODISTORICOMOD_GENERATOR", sequenceName="SEQ_STORICO_MODIFICHE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_STORICO_MODIFICHE_CODISTORICOMOD_GENERATOR")
	@Column(name="CODI_STORICO_MOD")
	private Integer codiStoricoMod;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	public TabStoricoModifiche() {
	}

	public Integer getCodiStoricoMod() {
		return codiStoricoMod;
	}

	public void setCodiStoricoMod(Integer codiStoricoMod) {
		this.codiStoricoMod = codiStoricoMod;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public TabStoricoModifiche(Integer codiSessione,
			Integer codiUtente) {
		super();
		this.codiSessione = codiSessione;
		this.codiUtente = codiUtente;
	}

}