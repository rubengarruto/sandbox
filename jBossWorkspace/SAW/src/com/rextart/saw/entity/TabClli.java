package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_CLLI database table.
 * 
 */
@Entity
@Table(name="TAB_CLLI")
@Named("TabClli")
@NamedQueries({
	@NamedQuery(name="TabClli.findAll", query="SELECT t FROM TabClli t"),
	@NamedQuery(name="TabClli.findByClliSito", query="SELECT c FROM TabClli c where c.codiClli=:clliSito"),
})
public class TabClli implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_CLLI")
	private String codiClli;

	@Column(name="CODI_ID_RETE")
	private String codiIdRete;

	@Column(name="DESC_COMUNE")
	private String descComune;

	@Column(name="DESC_LOCALITA")
	private String descLocalita;

	@Column(name="DESC_NOA")
	private String descNoa;

	@Column(name="DESC_NOL")
	private String descNol;

	@Column(name="DESC_NOU")
	private String descNou;

	@Column(name="DESC_PROV")
	private String descProv;

	@Column(name="DESC_SITO")
	private String descSito;

	@Column(name="DESC_STRADA")
	private String descStrada;

	public TabClli() {
	}

	public String getCodiClli() {
		return this.codiClli;
	}

	public void setCodiClli(String codiClli) {
		this.codiClli = codiClli;
	}

	public String getCodiIdRete() {
		return this.codiIdRete;
	}

	public void setCodiIdRete(String codiIdRete) {
		this.codiIdRete = codiIdRete;
	}

	public String getDescComune() {
		return this.descComune;
	}

	public void setDescComune(String descComune) {
		this.descComune = descComune;
	}

	public String getDescLocalita() {
		return this.descLocalita;
	}

	public void setDescLocalita(String descLocalita) {
		this.descLocalita = descLocalita;
	}

	public String getDescNoa() {
		return this.descNoa;
	}

	public void setDescNoa(String descNoa) {
		this.descNoa = descNoa;
	}

	public String getDescNol() {
		return this.descNol;
	}

	public void setDescNol(String descNol) {
		this.descNol = descNol;
	}

	public String getDescNou() {
		return this.descNou;
	}

	public void setDescNou(String descNou) {
		this.descNou = descNou;
	}

	public String getDescProv() {
		return this.descProv;
	}

	public void setDescProv(String descProv) {
		this.descProv = descProv;
	}

	public String getDescSito() {
		return this.descSito;
	}

	public void setDescSito(String descSito) {
		this.descSito = descSito;
	}

	public String getDescStrada() {
		return this.descStrada;
	}

	public void setDescStrada(String descStrada) {
		this.descStrada = descStrada;
	}

}