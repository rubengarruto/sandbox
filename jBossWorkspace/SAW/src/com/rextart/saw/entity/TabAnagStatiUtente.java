package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_STATI_UTENTE database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_STATI_UTENTE")
@NamedQuery(name="TabAnagStatiUtente.findAll", query="SELECT t FROM TabAnagStatiUtente t")
public class TabAnagStatiUtente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_STATO_UTENTE")
	private Integer codiStatoUtente;

	@Column(name="DESC_STATO_UTENTE")
	private String descStatoUtente;

	public TabAnagStatiUtente() {
	}

	public Integer getCodiStatoUtente() {
		return this.codiStatoUtente;
	}

	public void setCodiStatoUtente(Integer codiStatoUtente) {
		this.codiStatoUtente = codiStatoUtente;
	}

	public String getDescStatoUtente() {
		return this.descStatoUtente;
	}

	public void setDescStatoUtente(String descStatoUtente) {
		this.descStatoUtente = descStatoUtente;
	}

}