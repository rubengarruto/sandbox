package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_NOTE_CARATTERISTICHE")
@Named("TabNoteCaratteristiche")
@NamedQueries({
	@NamedQuery(name="TabNoteCaratteristiche.findAll", query="SELECT t FROM TabNoteCaratteristiche t"),
	@NamedQuery(name="TabNoteCaratteristiche.findByCodiSurveyCodiSess", query="SELECT t FROM TabNoteCaratteristiche t where t.codiSurvey= :codiSurvey and t.codiCaratteristica= :codiCaratteristica"),
	@NamedQuery(name="TabNoteCaratteristiche.deleteByCodiSurvey", query="DELETE FROM TabNoteCaratteristiche t where t.codiSurvey= :codiSurvey"),
	@NamedQuery(name="TabNoteCaratteristiche.getListByCodiSurvey", query="SELECT t FROM TabNoteCaratteristiche t where t.codiSurvey IN (:listCodiSurvey)"),
})
public class TabNoteCaratteristiche implements Serializable {

    private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_NOTE_CARATTERISTICHE_GENERATOR", sequenceName="SEQ_NOTE_CARATTERISTICHE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_NOTE_CARATTERISTICHE_GENERATOR")
    @Column(name="CODI_NOTE_CARATT")
    private Integer codiNoteCaratt;

    @Column(name="CODI_CARATTERISTICA")
    private Integer codiCaratteristica;

    @Column(name="CODI_SESSIONE")
    private Integer codiSessione;

    @Column(name="CODI_SURVEY")
    private Integer codiSurvey;

    @Column(name="DESC_NOTE")
    private String descNote;
    
    @Column(name="CODI_OGGETTO")
    private Integer codiOggetto;

    @Column(name="CODI_UTENTE")
    private Integer codiUtente;

    public Integer getCodiNoteCaratt() {
        return codiNoteCaratt;
    }

    public void setCodiNoteCaratt(Integer codiNoteCaratt) {
        this.codiNoteCaratt = codiNoteCaratt;
    }

    public Integer getCodiCaratteristica() {
        return codiCaratteristica;
    }

    public void setCodiCaratteristica(Integer codiCaratteristica) {
        this.codiCaratteristica = codiCaratteristica;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

    public String getDescNote() {
        return descNote;
    }

    public void setDescNote(String descNote) {
        this.descNote = descNote;
    }

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

    
}
