package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_MISURE_DI_PIM_RAD_D database table.
 * 
 */
@Entity
@Table(name="VIW_MISURE_DI_PIM_RAD_D")
@Named("ViwMisureDiPimRadD")
@NamedQueries({
	@NamedQuery(name="ViwMisureDiPimRadD.findAll", query="SELECT v FROM ViwMisureDiPimRadD v"),
	@NamedQuery(name="ViwMisureDiPimRadD.findByCodiSessione", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione order by v.codiElemento"),
	@NamedQuery(name="ViwMisureDiPimRadD.findMisuraPim", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione and (v.codiCaratteristica=828 or v.codiCaratteristica=758)"),
	@NamedQuery(name="ViwMisureDiPimRadD.findMisuraSistemaPim", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione and v.codiElemento=131"),
	@NamedQuery(name="ViwMisureDiPimRadD.findDtpValoreDiPim", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione and v.codiCaratteristica=762"),
	@NamedQuery(name="ViwMisureDiPimRadD.findDtpDistanza", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione and v.codiCaratteristica=763"),
	@NamedQuery(name="ViwMisureDiPimRadD.findBySessioneTipoOggettoCodiElemento", query="SELECT v FROM ViwMisureDiPimRadD v where v.codiSessione=:codiSessione and v.codiElemento=:codiElemento and v.codiTipoOggetto=:codiTipoOggetto"),

	
})
public class ViwMisureDiPimRadD implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="DESC_LABEL_LOV")
	private String descLabelLov;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;

	@Column(name="DESC_VALORE")
	private String descValore;

	@Column(name="NUME_BRANCH")
	private Integer numeBranch;

	@Column(name="NUME_CELLA")
	private Integer numeCella;

	@Column(name="NUME_ORDER")
	private Integer numeOrder;

	@Column(name="NUME_SETTORE")
	private Integer numeSettore;

	@Column(name="NUME_VIA")
	private Integer numeVia;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;
	
	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	public ViwMisureDiPimRadD() {
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCaratteristica() {
		return this.descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public String getDescLabelLov() {
		return this.descLabelLov;
	}

	public void setDescLabelLov(String descLabelLov) {
		this.descLabelLov = descLabelLov;
	}

	public String getDescNomeSito() {
		return this.descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public String getDescTipoOggetto() {
		return this.descTipoOggetto;
	}

	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}

	public String getDescValore() {
		return this.descValore;
	}

	public void setDescValore(String descValore) {
		this.descValore = descValore;
	}

	public Integer getNumeBranch() {
		return this.numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getNumeCella() {
		return this.numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public Integer getNumeOrder() {
		return this.numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}

	public Integer getNumeSettore() {
		return this.numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeVia() {
		return this.numeVia;
	}

	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}

	public byte[] getUuid() {
		return this.uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}
	
	

}