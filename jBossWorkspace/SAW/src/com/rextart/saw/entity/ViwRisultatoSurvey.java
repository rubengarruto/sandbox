package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_RISULTATO_SURVEY database table.
 * 
 */
@Entity
@Table(name="VIW_RISULTATO_SURVEY")
@NamedQueries({
	@NamedQuery(name="ViwRisultatoSurvey.findAll", query="SELECT v FROM ViwRisultatoSurvey v"),
	@NamedQuery(name="ViwRisultatoSurvey.findAllByCodiSessione", query="SELECT v FROM ViwRisultatoSurvey v WHERE v.codiSessione=:codiSessione"),
	@NamedQuery(name="ViwRisultatoSurvey.findAllByCodiSessioneAndCodiOggetto", query="SELECT v FROM ViwRisultatoSurvey v WHERE v.codiSessione=:codiSessione AND v.codiOggetto=:codiOggetto"),
	@NamedQuery(name="ViwRisultatoSurvey.findAllByCodiSessioneNegativoRiserva", query="SELECT v FROM ViwRisultatoSurvey v WHERE v.codiSessione=:codiSessione and v.codiAnagRisultato<>null order by v.codiOggetto"),
})
public class ViwRisultatoSurvey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_RISULTATO_SURVEY")
	private Integer codiRisultatoSurvey;
	
	@Column(name="CODI_ANAG_RISULTATO")
	private Integer codiAnagRisultato;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	@Column(name="CODI_VALORE_RISULTATO_SURVEY")
	private Integer codiValoreRisultatoSurvey;

	@Column(name="DESC_ANAG_RISULTATI")
	private String descAnagRisultati;

	@Column(name="DESC_OGGETTO")
	private String descOggetto;

	@Column(name="DESC_RISULTATO_SURVEY")
	private String descRisultatoSurvey;

	@Column(name="DESC_VALORE_RISULTATO_SURVEY")
	private String descValoreRisultatoSurvey;

	public ViwRisultatoSurvey() {
	}

	public Integer getCodiAnagRisultato() {
		return this.codiAnagRisultato;
	}

	public void setCodiAnagRisultato(Integer codiAnagRisultato) {
		this.codiAnagRisultato = codiAnagRisultato;
	}

	public Integer getCodiOggetto() {
		return this.codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiRisultatoSurvey() {
		return this.codiRisultatoSurvey;
	}

	public void setCodiRisultatoSurvey(Integer codiRisultatoSurvey) {
		this.codiRisultatoSurvey = codiRisultatoSurvey;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiSurvey() {
		return this.codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public Integer getCodiUtente() {
		return this.codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Integer getCodiValoreRisultatoSurvey() {
		return this.codiValoreRisultatoSurvey;
	}

	public void setCodiValoreRisultatoSurvey(Integer codiValoreRisultatoSurvey) {
		this.codiValoreRisultatoSurvey = codiValoreRisultatoSurvey;
	}

	public String getDescAnagRisultati() {
		return this.descAnagRisultati;
	}

	public void setDescAnagRisultati(String descAnagRisultati) {
		this.descAnagRisultati = descAnagRisultati;
	}

	public String getDescOggetto() {
		return this.descOggetto;
	}

	public void setDescOggetto(String descOggetto) {
		this.descOggetto = descOggetto;
	}

	public String getDescRisultatoSurvey() {
		return this.descRisultatoSurvey;
	}

	public void setDescRisultatoSurvey(String descRisultatoSurvey) {
		this.descRisultatoSurvey = descRisultatoSurvey;
	}

	public String getDescValoreRisultatoSurvey() {
		return this.descValoreRisultatoSurvey;
	}

	public void setDescValoreRisultatoSurvey(String descValoreRisultatoSurvey) {
		this.descValoreRisultatoSurvey = descValoreRisultatoSurvey;
	}

}