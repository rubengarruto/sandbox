package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REGIONI database table.
 * 
 */
@Entity
@Table(name="TAB_REGIONI")
@Named("TabRegioni")
@NamedQueries({
	@NamedQuery(name="TabRegioni.findAll", query="SELECT t FROM TabRegioni t order by t.nomeRegione"),
	@NamedQuery(name="TabRegioni.findRegioneByNome", query="SELECT t FROM TabRegioni t where t.nomeMaiuscolo=:nomeMaiuscolo"),
	@NamedQuery(name="TabRegioni.findIdRegioneByNome", query="SELECT t FROM TabRegioni t where t.nomeMaiuscolo=:nomeMaiuscolo"),
	//	@NamedQuery(name="TabRegioni.findDescRegioneByNome", query="SELECT t FROM TabRegioni t where t.nomeMaiuscolo=:nomeMaiuscolo"),
	
})
public class TabRegioni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_REGIONE")
	private int idRegione;

	@Column(name="CODI_REGIONE")
	private String codiRegione;

	@Column(name="DATA_SHAPE")
	private String dataShape;

	@Column(name="NOME_MAIUSCOLO")
	private String nomeMaiuscolo;

	@Column(name="NOME_REGIONE")
	private String nomeRegione;
	
	public TabRegioni() {
	}


	public int getIdRegione() {
		return this.idRegione;
	}

	public void setIdRegione(int idRegione) {
		this.idRegione = idRegione;
	}

	public String getCodiRegione() {
		return this.codiRegione;
	}

	public void setCodiRegione(String codiRegione) {
		this.codiRegione = codiRegione;
	}

	public String getDataShape() {
		return this.dataShape;
	}

	public void setDataShape(String dataShape) {
		this.dataShape = dataShape;
	}

	public String getNomeMaiuscolo() {
		return this.nomeMaiuscolo;
	}

	public void setNomeMaiuscolo(String nomeMaiuscolo) {
		this.nomeMaiuscolo = nomeMaiuscolo;
	}

	public String getNomeRegione() {
		return this.nomeRegione;
	}

	public void setNomeRegione(String nomeRegione) {
		this.nomeRegione = nomeRegione;
	}


}