package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_POLO_TECNOLOGICO database table.
 * 
 */
@Entity
@Table(name="TAB_POLO_TECNOLOGICO")
@Named("TabPoloTecnologico")
@NamedQueries({
	@NamedQuery(name="TabPoloTecnologico.findAll", query="SELECT t FROM TabPoloTecnologico t GROUP BY t.descPoloTecnologico"),
	@NamedQuery(name="TabPoloTecnologico.findByRegione", query="SELECT t FROM TabPoloTecnologico t where t.descRegione=:descRegione")
})
public class TabPoloTecnologico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_POLO_TECNO")
	private Integer idPoloTecno;

	@Column(name="CODI_VENDOR")
	private Integer codiVendor;

	@Column(name="DESC_POLO_TECNOLOGICO")
	private String descPoloTecnologico;

	@Column(name="ID_REGIONE")
	private Integer idRegione;
	
	@Column(name="DESC_REGIONE")
	private String descRegione;
	

	public TabPoloTecnologico() {
	}

	public Integer getIdPoloTecno() {
		return idPoloTecno;
	}

	public void setIdPoloTecno(Integer idPoloTecno) {
		this.idPoloTecno = idPoloTecno;
	}

	public Integer getCodiVendor() {
		return codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}

	public String getDescPoloTecnologico() {
		return descPoloTecnologico;
	}

	public void setDescPoloTecnologico(String descPoloTecnologico) {
		this.descPoloTecnologico = descPoloTecnologico;
	}

	public Integer getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(Integer idRegione) {
		this.idRegione = idRegione;
	}

	public String getDescRegione() {
		return descRegione;
	}

	public void setDescRegione(String descRegione) {
		this.descRegione = descRegione;
	}

	

}