package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_REL_SESSIONE_STATI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_SESSIONE_STATI")
@Named("TabRelSessioneStati")
@NamedQuery(name="TabRelSessioneStati.findAll", query="SELECT t FROM TabRelSessioneStati t")
public class TabRelSessioneStati implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_SESSIONE_STATI_CODIRELSESSSTATO_GENERATOR" , sequenceName="SEQ_TAB_REL_SESSIONE_STATI",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_SESSIONE_STATI_CODIRELSESSSTATO_GENERATOR")
	@Column(name="CODI_REL_SESS_STATO")
	private Integer codiRelSessStato;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_STATO")
	private Integer codiStato;

	@Temporal(TemporalType.DATE)
	@Column(name="TIME_DATA")
	private Date timeData;
	
	public TabRelSessioneStati() {
	}

	public Integer getCodiRelSessStato() {
		return this.codiRelSessStato;
	}

	public void setCodiRelSessStato(Integer codiRelSessStato) {
		this.codiRelSessStato = codiRelSessStato;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiStato() {
		return this.codiStato;
	}

	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}

	public Date getTimeData() {
		return this.timeData;
	}

	public void setTimeData(Date timeData) {
		this.timeData = timeData;
	}

	

	
}