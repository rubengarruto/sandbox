package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.rextart.saw.bean.WorkRequestBean;


/**
 * The persistent class for the TAB_WORK_REQUEST_COLL database table.
 * 
 */
@Entity
@Table(name="TAB_WORK_REQUEST_COLL")
@Named("TabWorkRequestColl")
@NamedQueries({
	@NamedQuery(name="TabWorkRequestColl.findAll", query="SELECT t FROM TabWorkRequestColl t"),
	@NamedQuery(name="TabWorkRequestColl.findByCodiSessione", query="SELECT t FROM TabWorkRequestColl t where t.codiSessione= :codiSessione"),
	@NamedQuery(name="TabWorkRequestColl.deleteByCodiSessione", query="DELETE FROM TabWorkRequestColl t where t.codiSessione= :codiSessione"),
	@NamedQuery(name="TabWorkRequestColl.findByCodiSessioneDescNumeroWrs", query="SELECT t FROM TabWorkRequestColl t where t.codiSessione= :codiSessione and t.descNumeroWrs= :descNumeroWrs"),
	@NamedQuery(name="TabWorkRequestColl.findLastWRByCodiSessione", query="SELECT t FROM TabWorkRequestColl t where t.codiSessione= :codiSessione AND t.codiAnagWrs=:codiAnagWrs ORDER BY t.dateTimeWrs DESC"),
})
public class TabWorkRequestColl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_WORK_REQUEST_COLL_CODIWRSCOLL_GENERATOR", sequenceName="SEQ_WORK_REQUEST_COLL")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_WORK_REQUEST_COLL_CODIWRSCOLL_GENERATOR")
	@Column(name="CODI_WRS_COLL")
	private Integer codiWrsColl;

	@Column(name="CODI_ANAG_WRS")
	private Integer codiAnagWrs;

	@Column(name="CODI_BANDA")
	private Integer codiBanda;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_SISTEMA")
	private Integer codiSistema;

	@Column(name="DESC_NUMERO_WRS")
	private String descNumeroWrs;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_TIME_WRS")
	private Date dateTimeWrs;

	
	@Transient
	private TabAnagWorkRequest tabAnagWorkRequest;
	
	
	public TabWorkRequestColl() {
	}

	public Integer getCodiWrsColl() {
		return this.codiWrsColl;
	}

	public void setCodiWrsColl(Integer codiWrsColl) {
		this.codiWrsColl = codiWrsColl;
	}

	public Integer getCodiAnagWrs() {
		return this.codiAnagWrs;
	}

	public void setCodiAnagWrs(Integer codiAnagWrs) {
		this.codiAnagWrs = codiAnagWrs;
	}

	public Integer getCodiBanda() {
		return this.codiBanda;
	}

	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}


	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiSistema() {
		return this.codiSistema;
	}

	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}

	public String getDescNumeroWrs() {
		return this.descNumeroWrs;
	}

	public void setDescNumeroWrs(String descNumeroWrs) {
		this.descNumeroWrs = descNumeroWrs;
	}


	public TabWorkRequestColl(WorkRequestBean workRequestBean) {
		super();
		this.codiAnagWrs = workRequestBean.getCodiAnagWrs();
		this.codiBanda = workRequestBean.getCodiBanda();
		this.codiOggetto = workRequestBean.getCodiOggetto();
		this.codiSessione = workRequestBean.getCodiSessione();
		this.codiSistema = workRequestBean.getCodiSistema();
		this.descNumeroWrs = workRequestBean.getDescNumeroWrs();
		this.dateTimeWrs = workRequestBean.getDataWorkRequest();
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public TabAnagWorkRequest getTabAnagWorkRequest() {
		return tabAnagWorkRequest;
	}

	public void setTabAnagWorkRequest(TabAnagWorkRequest tabAnagWorkRequest) {
		this.tabAnagWorkRequest = tabAnagWorkRequest;
	}

	public Date getDateTimeWrs() {
		return dateTimeWrs;
	}

	public void setDateTimeWrs(Date dateTimeWrs) {
		this.dateTimeWrs = dateTimeWrs;
	}


}