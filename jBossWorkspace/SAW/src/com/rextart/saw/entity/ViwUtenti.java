package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the VIW_UTENTI database table.
 * 
 */
@Entity
@Table(name="VIW_UTENTI")
@Named("ViwUtenti")
@NamedQueries({
	@NamedQuery(name="ViwUtenti.findByUser", query="SELECT t FROM ViwUtenti t where t.descUser=:username and t.codiStatoUtente=1"),
	@NamedQuery(name="ViwUtenti.findAll", query="SELECT v FROM ViwUtenti v"),
	@NamedQuery(name="ViwUtenti.findRefTerOPByCodiAreaComp", query="SELECT v FROM ViwUtenti v where  v.codiRole=:role and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findUserMOIByCodiAreaComp", query="SELECT v FROM ViwUtenti v where  v.codiImpresa=:codiImpresa and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findAllAssocSession", query="SELECT t FROM ViwUtenti t , TabRelSessioneUtenti r where t.codiUtente=r.codiUtente and r.codiSessione= :codiSessione and t.codiStruttura<>:struttura and t.codiRole<>:role and t.codiRole<>:codiVendor"),
	@NamedQuery(name="ViwUtenti.findUserByAreaComp", query="SELECT v FROM ViwUtenti v where v.codiAreaComp=:codiAreaComp"),
	@NamedQuery(name="ViwUtenti.findOperNoaByCodiAreaComp", query="SELECT v FROM ViwUtenti v where  v.codiRole=:role and v.codiStruttura=:struttura and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findAllAOUByAreaComp", query="SELECT v FROM ViwUtenti v where  v.codiStruttura=:struttura and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findOperAOUByAreaComp", query="SELECT v FROM ViwUtenti v where  v.codiStruttura=:struttura and  v.codiRole=:role and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findDGoperRToperByAreaComp", query="SELECT v FROM ViwUtenti v where  (v.codiRole=:roleDGOP or v.codiRole=:roleRTOP) and (v.codiAreaComp=:codiAreaComp or v.codiAreaComp=1) and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findUtentiVendorByCodiVendor", query="SELECT v FROM ViwUtenti v where v.codiVendor=:codiVendor   and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findByCodiUtenteAndCodiRole", query="SELECT v FROM ViwUtenti v where (v.codiUtente IN(:codiUtente)) and (v.codiRole=:roleRefTeROP or v.codiRole=:roleRefTer or v.codiRole=:roleCorOp or v.codiRole=:roleOp or v.codiRole=:roleVendor)"),
	@NamedQuery(name="ViwUtenti.findUserRefTerAndCordByAreaComp", query="SELECT v FROM ViwUtenti v where (v.codiRole=:roleRefTer or v.codiRole=:roleCorOp) and v.codiAreaComp=:codiAreaComp and v.flgCessSosp=false and v.flgReceiveEmail=true"),
	@NamedQuery(name="ViwUtenti.findUsersForMailDiSistema", query="SELECT v FROM ViwUtenti v where v.descAreaComp IN(:descAreaDiComp) and v.descRole IN(:descRoles) and v.flgCessSosp=false and v.flgReceiveEmail=true"),
	@NamedQuery(name="ViwUtenti.findAdmin", query="SELECT v FROM ViwUtenti v WHERE v.flgIsAdmin=true AND v.flgCessSosp=false AND v.flgReceiveEmail=true"),
	@NamedQuery(name="ViwUtenti.findRefTerOPByRegione", query="SELECT v FROM ViwUtenti v where  v.codiRole=:codiRole and v.codiUtente IN (:codiUtente) and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findOperAOUByRegione", query="SELECT v FROM ViwUtenti v where v.codiRole=:codiRole and v.codiStruttura=:struttura and v.codiUtente IN (:codiUtente) and v.flgCessSosp=false"),
	@NamedQuery(name="ViwUtenti.findUserRefTerAndCordByRegione", query="SELECT v FROM ViwUtenti v where (v.codiRole=:roleRefTer or v.codiRole=:roleCorOp) and v.codiUtente IN (:codiUtente) and v.flgCessSosp=false and v.flgReceiveEmail=true"),
	@NamedQuery(name="ViwUtenti.findUserInCodiUtente", query="SELECT v FROM ViwUtenti v where v.codiUtente IN (:codiUtente)"),
	@NamedQuery(name="ViwUtenti.findAllAssocSessionManuale", query="SELECT t FROM ViwUtenti t , TabRelSessioneUtenti r where t.codiUtente=r.codiUtente and r.codiSessione= :codiSessione and ((t.codiStruttura=:struttura and t.codiRole=:roleOper) or t.codiRole=:roleRefTerOp or t.codiRole=:roleVendor)"),

})
public class ViwUtenti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	@Column(name="CODI_ROLE")
	private Integer codiRole;

	@Column(name="CODI_STRUTTURA")
	private Integer codiStruttura;

	@Id
	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	@Column(name="DESC_AREA_COMP")
	private String descAreaComp;

	@Column(name="DESC_CGN")
	private String descCgn;

	@Column(name="DESC_CODI_ROLE")
	private String descCodiRole;

	@Column(name="DESC_EMAIL")
	private String descEmail;

	@Column(name="DESC_IMEI")
	private String descImei;

	@Column(name="DESC_NOME")
	private String descNome;

	@Column(name="DESC_PWD")
	private String descPwd;

	@Column(name="DESC_ROLE")
	private String descRole;

	@Column(name="DESC_STRUTTURA")
	private String descStruttura;

	@Column(name="DESC_USER")
	private String descUser;

	@Column(name="FLG_ABILITATO")
	private Boolean flgAbilitato;

	@Column(name="FLG_CESS_SOSP")
	private Boolean flgCessSosp;

	@Column(name="FLG_IS_ADMIN")
	private Boolean flgIsAdmin;

	@Column(name="FLG_NAZIONALE")
	private Boolean flgNazionale;

	@Column(name="FLG_OPERATIVO")
	private Boolean flgOperativo;

	@Column(name="FLG_RECEIVE_EMAIL")
	private Boolean flgReceiveEmail;

	@Column(name="FLG_RESET")
	private Boolean flgReset;

	@Column(name="FLG_TABLET")
	private Boolean flgTablet;
	
	@Column(name="FLG_IMPRESA")
	private Boolean flgImpresa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CEASED")
	private Date timeCeased;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREAZIONE")
	private Date timeCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_DISABLE")
	private Date timeDisable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_ENABLE")
	private Date timeEnable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_SUSPENDED")
	private Date timeSuspended;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_SUSPENSION")
	private Date timeSuspension;
	
	@Column(name="CODI_IMPRESA")
	private Integer codiImpresa;
	
	@Column(name="DESC_IMPRESA")
	private String descImpresa;
	
	@Column(name="DESC_EMAIL_IMPRESA")
	private String descEmaiImpresa;
	
	@Column(name="DESC_TEL_USER_IMP")
	private String descTelUserImp;
	
	@Column(name="CODI_STATO_UTENTE")
	private Integer codiStatoUtente;
	
	@Column(name="CODI_ZONA")
	private Integer codiZona;
	
	@Column(name="DESC_ZONA")
	private String descZona;
	
	@Column(name="CODI_GRUPPO")
	private Integer codiGruppo;
	
	@Column(name="DESC_GRUPPO")
	private String descGruppo;
	
	@Column(name="DESC_TELEFONO")
	private String descTelefono;
	
	@Column(name="CODI_VENDOR")
	private Integer codiVendor;
	
	@Column(name="FLG_CHANGE_PWD")
	private boolean flgChangePwd;
	
	@Transient
	private Boolean flagIsAssoc=false;

	@Column(name="DESC_VENDOR")
	private String descVendor;
	
	public ViwUtenti() {
	}

	public Integer getCodiUtente() {
		return this.codiUtente;
	}

	public String getDescAreaComp() {
		return this.descAreaComp;
	}

	public void setDescAreaComp(String descAreaComp) {
		this.descAreaComp = descAreaComp;
	}

	public String getDescCgn() {
		return this.descCgn;
	}

	public void setDescCgn(String descCgn) {
		this.descCgn = descCgn;
	}

	public String getDescCodiRole() {
		return this.descCodiRole;
	}

	public void setDescCodiRole(String descCodiRole) {
		this.descCodiRole = descCodiRole;
	}

	public String getDescEmail() {
		return this.descEmail;
	}

	public void setDescEmail(String descEmail) {
		this.descEmail = descEmail;
	}

	public String getDescImei() {
		return this.descImei;
	}

	public void setDescImei(String descImei) {
		this.descImei = descImei;
	}

	public String getDescNome() {
		return this.descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPwd() {
		return this.descPwd;
	}

	public void setDescPwd(String descPwd) {
		this.descPwd = descPwd;
	}

	public String getDescRole() {
		return this.descRole;
	}

	public void setDescRole(String descRole) {
		this.descRole = descRole;
	}

	public String getDescStruttura() {
		return this.descStruttura;
	}

	public void setDescStruttura(String descStruttura) {
		this.descStruttura = descStruttura;
	}

	public String getDescUser() {
		return this.descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public Boolean getFlgAbilitato() {
		return flgAbilitato;
	}

	public void setFlgAbilitato(Boolean flgAbilitato) {
		this.flgAbilitato = flgAbilitato;
	}

	public Boolean getFlgCessSosp() {
		return flgCessSosp;
	}

	public void setFlgCessSosp(Boolean flgCessSosp) {
		this.flgCessSosp = flgCessSosp;
	}

	public Boolean getFlgIsAdmin() {
		return flgIsAdmin;
	}

	public void setFlgIsAdmin(Boolean flgIsAdmin) {
		this.flgIsAdmin = flgIsAdmin;
	}

	public Boolean getFlgNazionale() {
		return flgNazionale;
	}

	public void setFlgNazionale(Boolean flgNazionale) {
		this.flgNazionale = flgNazionale;
	}

	public Boolean getFlgOperativo() {
		return flgOperativo;
	}

	public void setFlgOperativo(Boolean flgOperativo) {
		this.flgOperativo = flgOperativo;
	}

	public Boolean getFlgReceiveEmail() {
		return flgReceiveEmail;
	}

	public void setFlgReceiveEmail(Boolean flgReceiveEmail) {
		this.flgReceiveEmail = flgReceiveEmail;
	}

	public Boolean getFlgReset() {
		return flgReset;
	}

	public void setFlgReset(Boolean flgReset) {
		this.flgReset = flgReset;
	}

	public Boolean getFlgTablet() {
		return flgTablet;
	}

	public void setFlgTablet(Boolean flgTablet) {
		this.flgTablet = flgTablet;
	}

	public Date getTimeCeased() {
		return this.timeCeased;
	}

	public void setTimeCeased(Date timeCeased) {
		this.timeCeased = timeCeased;
	}

	public Date getTimeCreazione() {
		return this.timeCreazione;
	}

	public void setTimeCreazione(Date timeCreazione) {
		this.timeCreazione = timeCreazione;
	}

	public Date getTimeDisable() {
		return this.timeDisable;
	}

	public void setTimeDisable(Date timeDisable) {
		this.timeDisable = timeDisable;
	}

	public Date getTimeEnable() {
		return this.timeEnable;
	}

	public void setTimeEnable(Date timeEnable) {
		this.timeEnable = timeEnable;
	}

	public Date getTimeSuspended() {
		return this.timeSuspended;
	}

	public void setTimeSuspended(Date timeSuspended) {
		this.timeSuspended = timeSuspended;
	}

	public Date getTimeSuspension() {
		return this.timeSuspension;
	}

	public void setTimeSuspension(Date timeSuspension) {
		this.timeSuspension = timeSuspension;
	}

	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiRole() {
		return codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public Integer getCodiStruttura() {
		return codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Boolean getFlgImpresa() {
		return flgImpresa;
	}

	public void setFlgImpresa(Boolean flgImpresa) {
		this.flgImpresa = flgImpresa;
	}

	public String getDescTelUserImp() {
		return descTelUserImp;
	}

	public void setDescTelUserImp(String descTelUserImp) {
		this.descTelUserImp = descTelUserImp;
	}

	public Integer getCodiImpresa() {
		return codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

	public String getDescImpresa() {
		return descImpresa;
	}

	public void setDescImpresa(String descImpresa) {
		this.descImpresa = descImpresa;
	}

	public String getDescEmaiImpresa() {
		return descEmaiImpresa;
	}

	public void setDescEmaiImpresa(String descEmaiImpresa) {
		this.descEmaiImpresa = descEmaiImpresa;
	}

	public Integer getCodiStatoUtente() {
		return codiStatoUtente;
	}

	public void setCodiStatoUtente(Integer codiStatoUtente) {
		this.codiStatoUtente = codiStatoUtente;
	}

	public Integer getCodiZona() {
		return codiZona;
	}

	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}

	public String getDescZona() {
		return descZona;
	}

	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}

	public Integer getCodiGruppo() {
		return codiGruppo;
	}

	public void setCodiGruppo(Integer codiGruppo) {
		this.codiGruppo = codiGruppo;
	}

	public String getDescGruppo() {
		return descGruppo;
	}

	public void setDescGruppo(String descGruppo) {
		this.descGruppo = descGruppo;
	}

	public Boolean getFlagIsAssoc() {
		return flagIsAssoc;
	}

	public void setFlagIsAssoc(Boolean flagIsAssoc) {
		this.flagIsAssoc = flagIsAssoc;
	}

	public String getDescTelefono() {
		return descTelefono;
	}

	public void setDescTelefono(String descTelefono) {
		this.descTelefono = descTelefono;
	}

	public Integer getCodiVendor() {
		return codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}

	public boolean isFlgChangePwd() {
		return flgChangePwd;
	}

	public void setFlgChangePwd(boolean flgChangePwd) {
		this.flgChangePwd = flgChangePwd;
	}

	public String getDescVendor() {
		return descVendor;
	}

	public void setDescVendor(String descVendor) {
		this.descVendor = descVendor;
	}
	
}