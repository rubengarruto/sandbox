package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.*;



/**
 * The persistent class for the VIW_VER_FUNZ_ALLARMI database table.
 * 
 */
@Entity
@Table(name="VIW_VER_FUNZ_ALLARMI")
@NamedQueries({
	@NamedQuery(name="ViwVerFunzAllarmi.findAll", query="SELECT v FROM ViwVerFunzAllarmi v"),
	@NamedQuery(name="ViwVerFunzAllarmi.findBySessioneTipoOggettoCodiElemento", query="SELECT v FROM ViwVerFunzAllarmi v where v.codiSessione=:codiSessione and v.codiTipoOggetto=:codiTipoOggetto and v.codiElemento=:codiElemento order by v.codiCaratteristica"),
})
public class ViwVerFunzAllarmi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_SISTEMA")
	private String descSistema;
	
	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;
	
	@Column(name="DESC_NOTE")
	private String descNote;
	
	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;

	

	public ViwVerFunzAllarmi() {
	}

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescBanda() {
		return descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCaratteristica() {
		return descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public String getDescNomeSito() {
		return descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescSistema() {
		return descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public byte[] getUuid() {
		return uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	
}