package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the TAB_REL_ELEMENTI_CARATT database table.
 * 
 */
@Entity
@Table(name="TAB_REL_ELEMENTI_CARATT")
@Named("TabRelElementiCaratt")
@NamedQuery(name="TabRelElementiCaratt.findAll", query="SELECT t FROM TabRelElementiCaratt t")
public class TabRelElementiCaratt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_ELEMENTI_CARATT_CODIRELELEMCARATT_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_ELEMENTI_CARATT_CODIRELELEMCARATT_GENERATOR")
	@Column(name="CODI_REL_ELEM_CARATT")
	private int codiRelElemCaratt;

	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;
	
	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;
	
	@Column(name="FLAG_PRIMA_CARATTERISTICA",nullable=false)
	@NotNull(message="� richiesto un valore")
	private boolean flagPrimaCaratteristica;
	
	@Column(name="FLAG_ULTIMA_CARATTERISTICA")
	private boolean flagUltimaCaratteristica;
	

	public TabRelElementiCaratt() {
	}

	public int getCodiRelElemCaratt() {
		return this.codiRelElemCaratt;
	}

	public void setCodiRelElemCaratt(int codiRelElemCaratt) {
		this.codiRelElemCaratt = codiRelElemCaratt;
	}

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Boolean getFlagPrimaCaratteristica() {
		return flagPrimaCaratteristica;
	}

	public void setFlagPrimaCaratteristica(Boolean flagPrimaCaratteristica) {
		this.flagPrimaCaratteristica = flagPrimaCaratteristica;
	}

	public boolean isFlagUltimaCaratteristica() {
		return flagUltimaCaratteristica;
	}

	public void setFlagUltimaCaratteristica(boolean flagUltimaCaratteristica) {
		this.flagUltimaCaratteristica = flagUltimaCaratteristica;
	}

	public void setFlagPrimaCaratteristica(boolean flagPrimaCaratteristica) {
		this.flagPrimaCaratteristica = flagPrimaCaratteristica;
	}
	
	

}