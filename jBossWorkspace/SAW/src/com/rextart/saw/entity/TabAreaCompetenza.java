package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/**
 * The persistent class for the TAB_AREA_COMPETENZA database table.
 * 
 */
@Entity
@Table(name="TAB_AREA_COMPETENZA")
@Named("TabAreaCompetenza")
@NamedQueries({
@NamedQuery(name="TabAreaCompetenza.findAll", query="SELECT t FROM TabAreaCompetenza t"),
@NamedQuery(name="TabAreaCompetenza.findBySigla", query="SELECT t FROM TabAreaCompetenza t where t.descSiglaAreaComp =:descSiglaAreaComp"),
@NamedQuery(name="TabAreaCompetenza.findAllNotNazionale", query="SELECT t FROM TabAreaCompetenza t where t.flgNazionale=0")
})
public class TabAreaCompetenza implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	@Column(name="DESC_AREA_COMP", nullable=false, length=255)
	@NotNull
	@Length(min=1, max=255)
	private String descAreaComp;
	
	@Column(name="DESC_SIGLA_AREA_COMP", nullable=false, length=255)
	@NotNull
	@Length(min=1, max=20)
	private String descSiglaAreaComp;
	
	@Column(name="FLG_NAZIONALE", nullable=false)
	@NotNull
	private Integer flgNazionale;

	public TabAreaCompetenza() {}

	public Integer getCodiAreaComp() {
		return this.codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public String getDescAreaComp() {
		return this.descAreaComp;
	}

	public void setDescAreaComp(String descAreaComp) {
		this.descAreaComp = descAreaComp;
	}

	public Integer getFlgNazionale() {
		return flgNazionale;
	}

	public void setFlgNazionale(Integer flgNazionale) {
		this.flgNazionale = flgNazionale;
	}

	public String getDescSiglaAreaComp() {
		return descSiglaAreaComp;
	}

	public void setDescSiglaAreaComp(String descSiglaAreaComp) {
		this.descSiglaAreaComp = descSiglaAreaComp;
	}
	
	
}