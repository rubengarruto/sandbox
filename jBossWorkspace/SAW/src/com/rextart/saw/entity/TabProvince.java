package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_PROVINCE database table.
 * 
 */
@Entity
@Table(name="TAB_PROVINCE")
@Named("TabProvince")
@NamedQueries({
	@NamedQuery(name = "TabProvince.findByRegionId", query = "select p from TabProvince p where p.codiRegione=:codiRegione order by p.nomeProvincia"),
	@NamedQuery(name = "TabProvince.getProvinciaById", query = "select p from TabProvince p where p.idProvincia=:idProvincia"),
	@NamedQuery(name = "TabProvince.getProvinciaByCodiComune", query = "select p from TabProvince p, TabComuni c where p.codiProvincia=c.codiProvincia  and c.idComune = :idComune"),
	@NamedQuery(name=  "TabProvince.findProvinciaByNome", query="SELECT t FROM TabProvince t where t.nomeProvincia=:nomeProvincia"),
	
})
public class TabProvince implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PROVINCIA")
	private Integer idProvincia;

	@Column(name="CODI_PROVINCIA")
	private String codiProvincia;

	@Column(name="CODI_REGIONE")
	private String codiRegione;

	@Column(name="NOME_PROVINCIA")
	private String nomeProvincia;

    public TabProvince() {
    }

	public Integer getIdProvincia() {
		return this.idProvincia;
	}

	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getCodiProvincia() {
		return this.codiProvincia;
	}

	public void setCodiProvincia(String codiProvincia) {
		this.codiProvincia = codiProvincia;
	}

	public String getCodiRegione() {
		return this.codiRegione;
	}

	public void setCodiRegione(String codiRegione) {
		this.codiRegione = codiRegione;
	}

	public String getNomeProvincia() {
		return this.nomeProvincia;
	}

	public void setNomeProvincia(String nomeProvincia) {
		this.nomeProvincia = nomeProvincia;
	}
	/*
	public Set<TabDossier> getTabDossiers() {
		return this.tabDossiers;
	}

	public void setTabDossiers(Set<TabDossier> tabDossiers) {
		this.tabDossiers = tabDossiers;
	}*/
	
}