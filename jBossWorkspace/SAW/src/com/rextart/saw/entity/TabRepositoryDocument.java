package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_REPOSITORY_DOCUMENT database table.
 * 
 */
@Entity
@Table(name="TAB_REPOSITORY_DOCUMENT")
@Named("TabRepositoryDocument")
@NamedQueries({
	@NamedQuery(name="TabRepositoryDocument.findAll", query="SELECT t FROM TabRepositoryDocument t"),
	@NamedQuery(name="TabRepositoryDocument.findBySessioneNome", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione= :codiSessione and t.descNome= :descNome"),
	@NamedQuery(name="TabRepositoryDocument.findRepositoryDocumentByCodiSessione", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione=:codiSessione and t.flagD1=true"),
	@NamedQuery(name="TabRepositoryDocument.findVerbaleD1ByCodiSessione", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione=:codiSessione and t.flagD1=true"),
	@NamedQuery(name="TabRepositoryDocument.findByCodiSessione", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione=:codiSessione and t.flagD1=false"),
	@NamedQuery(name="TabRepositoryDocument.findBySessioneTipo", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione= :codiSessione and t.tipoFile= :tipoFile"),
	@NamedQuery(name="TabRepositoryDocument.findDocBySessioneAndNome", query="SELECT t FROM TabRepositoryDocument t where t.codiSessione= :codiSessione and t.descNome= :descNome"),

})
public class TabRepositoryDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REPOSITORY_DOCUMENT_CODIREPOSITORYDOCUMENT_GENERATOR", sequenceName="SEQ_REPOSITORY_DOCUMENT", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REPOSITORY_DOCUMENT_CODIREPOSITORYDOCUMENT_GENERATOR")
	@Column(name="CODI_REPOSITORY_DOCUMENT")
	private Integer codiRepositoryDocument;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_NOME")
	private String descNome;

	@Column(name="DESC_PATH")
	private String descPath;
	
	@Column(name="DESC_MIME_TYPE")
	private String descMimeType;
	
	@Column(name="FLAG_D1")
	private boolean flagD1=false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREATE")
	private Date timeCreate;
	
	@Column(name="TIPO_FILE")
	private Integer tipoFile;

	public TabRepositoryDocument() {
	}

	public long getCodiRepositoryDocument() {
		return this.codiRepositoryDocument;
	}

	public void setCodiRepositoryDocument(Integer codiRepositoryDocument) {
		this.codiRepositoryDocument = codiRepositoryDocument;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescNome() {
		return this.descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPath() {
		return this.descPath;
	}

	public void setDescPath(String descPath) {
		this.descPath = descPath;
	}

	public Date getTimeCreate() {
		return this.timeCreate;
	}

	public void setTimeCreate(Date timeCreate) {
		this.timeCreate = timeCreate;
	}

	public String getDescMimeType() {
		return descMimeType;
	}

	public void setDescMimeType(String descMimeType) {
		this.descMimeType = descMimeType;
	}

	public boolean isFlagD1() {
		return flagD1;
	}

	public void setFlagD1(boolean flagD1) {
		this.flagD1 = flagD1;
	}

	public Integer getTipoFile() {
		return tipoFile;
	}

	public void setTipoFile(Integer tipoFile) {
		this.tipoFile = tipoFile;
	}

}