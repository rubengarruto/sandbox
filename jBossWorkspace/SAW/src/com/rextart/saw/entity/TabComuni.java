package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_COMUNI database table.
 * 
 */
@Entity
@Table(name="TAB_COMUNI")
@Named("TabComuni")
@NamedQueries({
	@NamedQuery(name="TabComuni.findAll", query="SELECT t FROM TabComuni t"),
	@NamedQuery(name = "TabComuni.findByIdProvincia", query = "select t from TabComuni t where t.codiProvincia=:codiProvincia order by t.nomeComuneIt"),
})
public class TabComuni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_COMUNE")
	private Integer idComune;

	@Column(name="CODI_COD_ISTAT")
	private String codiCodIstat;

	@Column(name="CODI_COMUNE")
	private String codiComune;

	@Column(name="CODI_PROVINCIA")
	private String codiProvincia;

	@Column(name="CODI_REGIONE")
	private String codiRegione;

	@Column(name="FLAG_CAPOLUOGO")
	private  Boolean flagCapoluogo;

	@Column(name="NOME_COMUNE_IT")
	private String nomeComuneIt;

	@Column(name="NOME_COMUNE_IT_DE")
	private String nomeComuneItDe;

	@Column(name="NUME_ABITANTI")
	private Integer numeAbitanti;

	public TabComuni() {
	}

	public Integer getIdComune() {
		return this.idComune;
	}

	public void setIdComune(Integer idComune) {
		this.idComune = idComune;
	}

	public String getCodiCodIstat() {
		return this.codiCodIstat;
	}

	public void setCodiCodIstat(String codiCodIstat) {
		this.codiCodIstat = codiCodIstat;
	}

	public String getCodiComune() {
		return this.codiComune;
	}

	public void setCodiComune(String codiComune) {
		this.codiComune = codiComune;
	}

	public String getCodiProvincia() {
		return this.codiProvincia;
	}

	public void setCodiProvincia(String codiProvincia) {
		this.codiProvincia = codiProvincia;
	}

	public String getCodiRegione() {
		return this.codiRegione;
	}

	public void setCodiRegione(String codiRegione) {
		this.codiRegione = codiRegione;
	}

	public String getNomeComuneIt() {
		return this.nomeComuneIt;
	}

	public void setNomeComuneIt(String nomeComuneIt) {
		this.nomeComuneIt = nomeComuneIt;
	}

	public String getNomeComuneItDe() {
		return this.nomeComuneItDe;
	}

	public void setNomeComuneItDe(String nomeComuneItDe) {
		this.nomeComuneItDe = nomeComuneItDe;
	}

	public Boolean getFlagCapoluogo() {
		return flagCapoluogo;
	}

	public void setFlagCapoluogo(Boolean flagCapoluogo) {
		this.flagCapoluogo = flagCapoluogo;
	}

	public Integer getNumeAbitanti() {
		return numeAbitanti;
	}

	public void setNumeAbitanti(Integer numeAbitanti) {
		this.numeAbitanti = numeAbitanti;
	}

	

}