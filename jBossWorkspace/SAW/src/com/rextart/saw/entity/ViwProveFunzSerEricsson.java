package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_PROVE_FUNZ_SER_ERICSSON database table.
 * 
 */
@Entity
@Table(name="VIW_PROVE_FUNZ_SER_ERICSSON")
@Named("ViwProveFunzSerEricsson")
@NamedQueries({
	 @NamedQuery(name="ViwProveFunzSerEricsson.findAll", query="SELECT v FROM ViwProveFunzSerEricsson v"),
	 @NamedQuery(name="ViwProveFunzSerEricsson.findByCodiSessione", query="SELECT v FROM ViwProveFunzSerEricsson v where v.codiSessione=:codiSessione order by v.codiCaratteristica"),
	})
public class ViwProveFunzSerEricsson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_NOTE")
	private String descNote;
	
	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;
	

	public ViwProveFunzSerEricsson() {
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCaratteristica() {
		return this.descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public String getDescNomeSito() {
		return this.descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public byte[] getUuid() {
		return this.uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}
	
	

}