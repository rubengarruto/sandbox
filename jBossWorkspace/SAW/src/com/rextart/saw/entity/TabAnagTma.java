package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_ANAG_TMA")
@Named("TabAnagTma")
@NamedQueries({
	@NamedQuery(name="TabAnagTma.findAll", query="SELECT t FROM TabAnagTma t"),
	@NamedQuery(name="TabAnagTma.findByDescMarcaAndModello", query="SELECT t FROM TabAnagTma t where t.descMarca=:descMarca and t.descModello=:descModello"),
	@NamedQuery(name="TabAnagTma.findAllDistinctDescMarca", query="SELECT distinct descMarca FROM TabAnagTma t"),
	@NamedQuery(name="TabAnagTma.findByDescMarca", query="SELECT t FROM TabAnagTma t where t.descMarca=:descMarca")
})
public class TabAnagTma implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TAB_ANAG_TMA_CODI_ANAG_TMA_GENERATOR", sequenceName="SEQ_ANAG_TMA",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_ANAG_TMA_CODI_ANAG_TMA_GENERATOR")
	@Column(name="CODI_ANAG_TMA")
	private Integer codiAnagTma;
	
	
	@Column(name="DESC_MARCA")
	private String descMarca;
	
	
	@Column(name="DESC_MODELLO")
	private String descModello;
	
	
	@Column(name="FLAG_ATTIVO")
	private boolean flagAttivo;


	public String getDescMarca() {
		return descMarca;
	}


	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescModello() {
		return descModello;
	}


	public void setDescModello(String descModello) {
		this.descModello = descModello;
	}


	public boolean isFlagAttivo() {
		return flagAttivo;
	}


	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}


	public Integer getCodiAnagTma() {
		return codiAnagTma;
	}


	public void setCodiAnagTma(Integer codiAnagTma) {
		this.codiAnagTma = codiAnagTma;
	}
	
	

}
