package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_RIFERIMENTI_FIRMA")
@Named("TabRiferimentiFirma")
@NamedQueries({
	@NamedQuery(name="TabRiferimentiFirma.findByCodiSurvey", query="SELECT t FROM TabRiferimentiFirma t WHERE t.codiSurvey = :codiSurvey"),
	@NamedQuery(name="TabRiferimentiFirma.getListByCodiSurvey", query="SELECT t FROM TabRiferimentiFirma t where t.codiSurvey IN (:listCodiSurvey)"),
	@NamedQuery(name="TabRiferimentiFirma.findByCodiSurveyFlgDitta", query="SELECT t FROM TabRiferimentiFirma t where t.codiSurvey = :codiSurvey AND t.flgDitta=:flgDitta")
})
public class TabRiferimentiFirma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_RIFERIMENTI_FIRMA_CODI_OGGETTO_FIRMA_GENERATOR", sequenceName="SEQ_RIFERIMENTI_FIRMA", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_RIFERIMENTI_FIRMA_CODI_OGGETTO_FIRMA_GENERATOR")
	@Column(name="CODI_RIFERIMENTI_FIRMA")
    private Integer codiRiferimentiFirma;

	@Column(name="CODI_SESSIONE")
    private Integer codiSessione;

	@Column(name="CODI_OGGETTO")
    private Integer codiOggetto;

	@Column(name="CODI_UTENTE")
    private Integer codiUtente;

	@Lob
	@Column(name="OBJ_MEDIA_FIRMA")
    private byte[] objMediaFirma;
	
	@Column(name="DESC_NOME_DITTA")
    private String descNomeDitta;

	@Column(name="DESC_TELEFONO_DITTA")
    private String descTelefonoDitta;

	@Column(name="CODI_SURVEY")
    private Integer codiSurvey;
	
	@Column(name="FLG_DITTA")
    private Boolean flgDitta;
	
	@Column(name="DESC_DITTA_INSTALLATRICE")
    private String descDittaInstallatrice;

	
    public Integer getCodiRiferimentiFirma() {
		return codiRiferimentiFirma;
	}

	public void setCodiRiferimentiFirma(Integer codiRiferimentiFirma) {
		this.codiRiferimentiFirma = codiRiferimentiFirma;
	}

	public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

    public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public byte[] getObjMediaFirma() {
        return objMediaFirma;
    }

    public void setObjMediaFirma(byte[] objMediaFirma) {
        this.objMediaFirma = objMediaFirma;
    }

    public String getDescNomeDitta() {
        return descNomeDitta;
    }

    public void setDescNomeDitta(String descNomeDitta) {
        this.descNomeDitta = descNomeDitta;
    }

    public String getDescTelefonoDitta() {
        return descTelefonoDitta;
    }

    public void setDescTelefonoDitta(String descTelefonoDitta) {
        this.descTelefonoDitta = descTelefonoDitta;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

	public Boolean getFlgDitta() {
		return flgDitta;
	}

	public void setFlgDitta(Boolean flgDitta) {
		this.flgDitta = flgDitta;
	}

	public String getDescDittaInstallatrice() {
		return descDittaInstallatrice;
	}

	public void setDescDittaInstallatrice(String descDittaInstallatrice) {
		this.descDittaInstallatrice = descDittaInstallatrice;
	}
	
	
    
    
}
