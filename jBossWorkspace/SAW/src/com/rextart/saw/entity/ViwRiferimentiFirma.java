package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_RIFERIMENTI_FIRMA database table.
 * 
 */
@Entity
@Table(name="VIW_RIFERIMENTI_FIRMA")
@Named("ViwRiferimentiFirma")
@NamedQueries({
	@NamedQuery(name="ViwRiferimentiFirma.findAll", query="SELECT v FROM ViwRiferimentiFirma v"),
	@NamedQuery(name="ViwRiferimentiFirma.findByCodiSurvey", query="SELECT v FROM ViwRiferimentiFirma v WHERE v.codiSurvey = :codiSurvey")
})
public class ViwRiferimentiFirma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CODI_IMPRESA")
	private Integer codiImpresa;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Id
	@Column(name="CODI_RIFERIMENTI_FIRMA")
	private Integer codiRiferimentiFirma;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	@Column(name="DESC_IMPRESA")
	private String descImpresa;

	@Column(name="DESC_NOME_DITTA")
	private String descNomeDitta;

	@Column(name="DESC_TELEFONO_DITTA")
	private String descTelefonoDitta;

	@Column(name="FLG_DITTA")
	private Boolean flgDitta;
	
	@Column(name="DESC_DITTA_INSTALLATRICE")
	private String descDittaInstallatrice;

	@Lob
	@Column(name="OBJ_MEDIA_FIRMA")
	private byte[] objMediaFirma;

	public ViwRiferimentiFirma() {
	}

	public Integer getCodiImpresa() {
		return codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiRiferimentiFirma() {
		return codiRiferimentiFirma;
	}

	public void setCodiRiferimentiFirma(Integer codiRiferimentiFirma) {
		this.codiRiferimentiFirma = codiRiferimentiFirma;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescImpresa() {
		return descImpresa;
	}

	public void setDescImpresa(String descImpresa) {
		this.descImpresa = descImpresa;
	}

	public String getDescNomeDitta() {
		return descNomeDitta;
	}

	public void setDescNomeDitta(String descNomeDitta) {
		this.descNomeDitta = descNomeDitta;
	}

	public String getDescTelefonoDitta() {
		return descTelefonoDitta;
	}

	public void setDescTelefonoDitta(String descTelefonoDitta) {
		this.descTelefonoDitta = descTelefonoDitta;
	}

	public Boolean getFlgDitta() {
		return flgDitta;
	}

	public void setFlgDitta(Boolean flgDitta) {
		this.flgDitta = flgDitta;
	}

	public byte[] getObjMediaFirma() {
		return objMediaFirma;
	}

	public void setObjMediaFirma(byte[] objMediaFirma) {
		this.objMediaFirma = objMediaFirma;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDescDittaInstallatrice() {
		return descDittaInstallatrice;
	}

	public void setDescDittaInstallatrice(String descDittaInstallatrice) {
		this.descDittaInstallatrice = descDittaInstallatrice;
	}

	

}