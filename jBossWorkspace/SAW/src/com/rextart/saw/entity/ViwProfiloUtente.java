package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_PROFILO_UTENTE database table.
 * 
 */
@Entity
@Table(name="VIW_PROFILO_UTENTE")
@Named("ViwProfiloUtente")
@NamedQueries({
	@NamedQuery(name="ViwProfiloUtente.findAll", query="SELECT v FROM ViwProfiloUtente v"),
	@NamedQuery(name="ViwProfiloUtente.findStrutturaByRole", query="SELECT t FROM ViwProfiloUtente t where t.codiRole=:codiRole and t.codiStruttura<>:struttura"),
})
public class ViwProfiloUtente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CODI_REL_ROLE_STRUTTURA")
	private Integer codiRelRoleStruttura;
	
	@Column(name="CODI_ROLE")
	private Integer codiRole;

	@Column(name="CODI_STRUTTURA")
	private Integer codiStruttura;

	@Column(name="DESC_CODI_ROLE")
	private String descCodiRole;

	@Column(name="DESC_ROLE")
	private String descRole;

	@Column(name="DESC_STRUTTURA")
	private String descStruttura;

	@Column(name="FLG_OPERATIVO")
	private boolean flgOperativo;

	public ViwProfiloUtente() {
	}

	public Integer getCodiRole() {
		return this.codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public Integer getCodiStruttura() {
		return this.codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

	public String getDescCodiRole() {
		return this.descCodiRole;
	}

	public void setDescCodiRole(String descCodiRole) {
		this.descCodiRole = descCodiRole;
	}

	public String getDescRole() {
		return this.descRole;
	}

	public void setDescRole(String descRole) {
		this.descRole = descRole;
	}

	public String getDescStruttura() {
		return this.descStruttura;
	}

	public void setDescStruttura(String descStruttura) {
		this.descStruttura = descStruttura;
	}



}