package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_ANAG_ANTENNE")
@Named("TabAnagAntenne")
@NamedQueries({
	@NamedQuery(name="TabAnagAntenne.findAll", query="SELECT t FROM TabAnagAntenne t"),
	@NamedQuery(name="TabAnagAntenne.findByDescMarcaAndModello", query="SELECT t FROM TabAnagAntenne t where t.descMarca=:descMarca and t.descModello=:descModello"),
	@NamedQuery(name="TabAnagAntenne.findAllDistinctDescMarca", query="SELECT distinct descMarca FROM TabAnagAntenne t"),
	@NamedQuery(name="TabAnagAntenne.findByDescMarca", query="SELECT t FROM TabAnagAntenne t where t.descMarca=:descMarca")
})
public class TabAnagAntenne implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TAB_ANAG_ANTENNE_CODI_ANAG_ANTENNE_GENERATOR", sequenceName="SEQ_ANAG_ANTENNE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_ANAG_ANTENNE_CODI_ANAG_ANTENNE_GENERATOR")
	@Column(name="CODI_ANAG_ANTENNE")
	private Integer codiAnagAntenne;
	
	
	@Column(name="DESC_MARCA")
	private String descMarca;
	
	
	@Column(name="DESC_MODELLO")
	private String descModello;
	
	
	@Column(name="FLAG_ATTIVO")
	private boolean flagAttivo = false;


	public Integer getCodiAnagAntenne() {
		return codiAnagAntenne;
	}


	public void setCodiAnagAntenne(Integer codiAnagAntenne) {
		this.codiAnagAntenne = codiAnagAntenne;
	}


	public String getDescMarca() {
		return descMarca;
	}


	public void setDescMarca(String descMarca) {
		this.descMarca = descMarca;
	}


	public String getDescModello() {
		return descModello;
	}


	public void setDescModello(String descModello) {
		this.descModello = descModello;
	}


	public boolean isFlagAttivo() {
		return flagAttivo;
	}


	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	
}
