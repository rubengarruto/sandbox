package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_SESSIONI_NOTE database table.
 * 
 */
@Entity
@Table(name="VIW_SESSIONI_NOTE")
@NamedQueries({
	@NamedQuery(name="ViwSessioniNote.findAll", query="SELECT v FROM ViwSessioniNote v"),
	@NamedQuery(name="ViwSessioniNote.findSessioniNoteByListSessioni", query="SELECT v FROM ViwSessioniNote v where v.codiSessione IN(:listCodiSessioni)")
})
public class ViwSessioniNote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="FLG_NOTA_MOI")
	private Boolean flgNotaMoi;

	@Column(name="FLG_NOTA_MOS")
	private Boolean flgNotaMos;

	@Column(name="FLG_NOTA_WEB")
	private Boolean flgNotaWeb;

	public ViwSessioniNote() {
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Boolean getFlgNotaMoi() {
		return this.flgNotaMoi;
	}

	public void setFlgNotaMoi(Boolean flgNotaMoi) {
		this.flgNotaMoi = flgNotaMoi;
	}

	public Boolean getFlgNotaMos() {
		return this.flgNotaMos;
	}

	public void setFlgNotaMos(Boolean flgNotaMos) {
		this.flgNotaMos = flgNotaMos;
	}

	public Boolean getFlgNotaWeb() {
		return this.flgNotaWeb;
	}

	public void setFlgNotaWeb(Boolean flgNotaWeb) {
		this.flgNotaWeb = flgNotaWeb;
	}

}