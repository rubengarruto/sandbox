package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the TAB_REL_CASCADE_CARATT_LOV database table.
 * 
 */
@Entity
@Table(name="TAB_REL_CASCADE_CARATT_LOV")
@NamedQueries({
	@NamedQuery(name="TabRelCascadeCarattLov.findAll", query="SELECT t FROM TabRelCascadeCarattLov t"),
	@NamedQuery(name="TabRelCascadeCarattLov.findByCodiOggetto", query="SELECT t FROM TabRelCascadeCarattLov t where t.codiOggetto = :codiOggetto"),
	@NamedQuery(name="TabRelCascadeCarattLov.findPrimaryCarattByCodiOggetto", query="SELECT t FROM TabRelCascadeCarattLov t where t.codiOggetto = :codiOggetto"),
	@NamedQuery(name="TabRelCascadeCarattLov.findCarattByPrimaryCaratt", query="SELECT t FROM TabRelCascadeCarattLov t where t.codiOggetto = :codiOggetto and t.codiCarattPrimary = :codiCarattPrimary and t.codiAnagLovPrimary = :codiAnagLovPrimary"),
})
public class TabRelCascadeCarattLov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_CASCADE_CARATT_LOV_GENERATOR", sequenceName="SEQ_REL_CASCADE_CARATT_LOV", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_CASCADE_CARATT_LOV_GENERATOR")
	@Column(name="CODI_REL_CASCADE_CARATT_LOV")
	private Integer codiRelCascadeCarattLov;

	@Column(name="CODI_ANAG_LOV_PRIMARY")
	private Integer codiAnagLovPrimary;

	@Column(name="CODI_CARATT")
	private Integer codiCaratt;

	@Column(name="CODI_CARATT_PRIMARY")
	private Integer codiCarattPrimary;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;
	
	public TabRelCascadeCarattLov() {
	}

	public Integer getCodiRelCascadeCarattLov() {
		return this.codiRelCascadeCarattLov;
	}

	public void setCodiRelCascadeCarattLov(Integer codiRelCascadeCarattLov) {
		this.codiRelCascadeCarattLov = codiRelCascadeCarattLov;
	}

	public Integer getCodiAnagLovPrimary() {
		return this.codiAnagLovPrimary;
	}

	public void setCodiAnagLovPrimary(Integer codiAnagLovPrimary) {
		this.codiAnagLovPrimary = codiAnagLovPrimary;
	}

	public Integer getCodiCaratt() {
		return this.codiCaratt;
	}

	public void setCodiCaratt(Integer codiCaratt) {
		this.codiCaratt = codiCaratt;
	}

	public Integer getCodiCarattPrimary() {
		return this.codiCarattPrimary;
	}

	public void setCodiCarattPrimary(Integer codiCarattPrimary) {
		this.codiCarattPrimary = codiCarattPrimary;
	}

	public Integer getCodiOggetto() {
		return this.codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}
}