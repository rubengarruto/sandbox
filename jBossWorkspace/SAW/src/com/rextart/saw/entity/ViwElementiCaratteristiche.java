package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the VIW_ELEMENTI_CARATTERISTICHE database table.
 * 
 */
@Entity
@Table(name="VIW_ELEMENTI_CARATTERISTICHE")
@NamedQueries({
	@NamedQuery(name="ViwElementiCaratteristiche.findAll", query="SELECT v FROM ViwElementiCaratteristiche v"),
	@NamedQuery(name="ViwElementiCaratteristiche.findByElemento", query="SELECT v FROM ViwElementiCaratteristiche v where v.codiElemento=:codiElemento order by v.flagPrimaCaratteristica Desc, v.codiCaratteristica Asc")
})

public class ViwElementiCaratteristiche implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_CARATTERISTICA")

	private Integer codiCaratteristica;
	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_COMPONENTE")
	private String descComponente;

	@Column(name="DESC_LABEL_CARAT_1")
	private String descLabelCarat1;

	@Column(name="DESC_LABEL_CARAT_2")
	private String descLabelCarat2;

	@Column(name="DESC_LABEL_CARAT_3")
	private String descLabelCarat3;

	@Column(name="DESC_LABEL_CARAT_4")
	private String descLabelCarat4;

	@Column(name="DESC_LABEL_CARAT_5")
	private String descLabelCarat5;

	@Column(name="FLAG_LOV_1")
	private Integer flagLov1;

	@Column(name="FLAG_LOV_2")
	private Integer flagLov2;

	@Column(name="FLAG_LOV_3")
	private Integer flagLov3;

	@Column(name="FLAG_LOV_4")
	private Integer flagLov4;

	@Column(name="FLAG_LOV_5")
	private Integer flagLov5;

	@Column(name="FLAG_NOTE")
	private Integer flagNote;

	@Column(name="FLAG_OBBLIGATORIO")
	private Integer flagObbligatorio;

	@Column(name="FLAG_PRIMA_CARATTERISTICA")
	private Integer flagPrimaCaratteristica;

	@Column(name="FLAG_ULTIMA_CARATTERISTICA")
	private Integer flagUltimaCaratteristica;

	@Column(name="MEMO_HELP")
	private String memoHelp;

	@Column(name="NUME_BRANCH")
	private Integer numeBranch;

	@Column(name="NUME_CELLA")
	private Integer numeCella;

	@Column(name="NUME_E")
	private Integer numeE;

	@Column(name="NUME_SETTORE")
	private Integer numeSettore;

	@Column(name="NUME_VIA")
	private Integer numeVia;

	public ViwElementiCaratteristiche() {
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public String getDescCaratteristica() {
		return this.descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescComponente() {
		return this.descComponente;
	}

	public void setDescComponente(String descComponente) {
		this.descComponente = descComponente;
	}

	public String getDescLabelCarat1() {
		return this.descLabelCarat1;
	}

	public void setDescLabelCarat1(String descLabelCarat1) {
		this.descLabelCarat1 = descLabelCarat1;
	}

	public String getDescLabelCarat2() {
		return this.descLabelCarat2;
	}

	public void setDescLabelCarat2(String descLabelCarat2) {
		this.descLabelCarat2 = descLabelCarat2;
	}

	public String getDescLabelCarat3() {
		return this.descLabelCarat3;
	}

	public void setDescLabelCarat3(String descLabelCarat3) {
		this.descLabelCarat3 = descLabelCarat3;
	}

	public String getDescLabelCarat4() {
		return this.descLabelCarat4;
	}

	public void setDescLabelCarat4(String descLabelCarat4) {
		this.descLabelCarat4 = descLabelCarat4;
	}

	public String getDescLabelCarat5() {
		return this.descLabelCarat5;
	}

	public void setDescLabelCarat5(String descLabelCarat5) {
		this.descLabelCarat5 = descLabelCarat5;
	}

	public Integer getFlagLov1() {
		return this.flagLov1;
	}

	public void setFlagLov1(Integer flagLov1) {
		this.flagLov1 = flagLov1;
	}

	public Integer getFlagLov2() {
		return this.flagLov2;
	}

	public void setFlagLov2(Integer flagLov2) {
		this.flagLov2 = flagLov2;
	}

	public Integer getFlagLov3() {
		return this.flagLov3;
	}

	public void setFlagLov3(Integer flagLov3) {
		this.flagLov3 = flagLov3;
	}

	public Integer getFlagLov4() {
		return this.flagLov4;
	}

	public void setFlagLov4(Integer flagLov4) {
		this.flagLov4 = flagLov4;
	}

	public Integer getFlagLov5() {
		return this.flagLov5;
	}

	public void setFlagLov5(Integer flagLov5) {
		this.flagLov5 = flagLov5;
	}

	public Integer getFlagNote() {
		return this.flagNote;
	}

	public void setFlagNote(Integer flagNote) {
		this.flagNote = flagNote;
	}

	public Integer getFlagObbligatorio() {
		return this.flagObbligatorio;
	}

	public void setFlagObbligatorio(Integer flagObbligatorio) {
		this.flagObbligatorio = flagObbligatorio;
	}

	public Integer getFlagPrimaCaratteristica() {
		return this.flagPrimaCaratteristica;
	}

	public void setFlagPrimaCaratteristica(Integer flagPrimaCaratteristica) {
		this.flagPrimaCaratteristica = flagPrimaCaratteristica;
	}

	public Integer getFlagUltimaCaratteristica() {
		return this.flagUltimaCaratteristica;
	}

	public void setFlagUltimaCaratteristica(Integer flagUltimaCaratteristica) {
		this.flagUltimaCaratteristica = flagUltimaCaratteristica;
	}

	public String getMemoHelp() {
		return this.memoHelp;
	}

	public void setMemoHelp(String memoHelp) {
		this.memoHelp = memoHelp;
	}

	public Integer getNumeBranch() {
		return this.numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getNumeCella() {
		return this.numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public Integer getNumeE() {
		return this.numeE;
	}

	public void setNumeE(Integer numeE) {
		this.numeE = numeE;
	}

	public Integer getNumeSettore() {
		return this.numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeVia() {
		return this.numeVia;
	}

	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}

}