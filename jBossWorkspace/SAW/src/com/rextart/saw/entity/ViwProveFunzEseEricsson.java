package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_PROVE_FUNZ_ESE_ERICSSON database table.
 * 
 */
@Entity
@Table(name="VIW_PROVE_FUNZ_ESE_ERICSSON")
@Named("ViwProveFunzEseEricsson")
@NamedQueries({
	 @NamedQuery(name="ViwProveFunzEseEricsson.findAll", query="SELECT v FROM ViwProveFunzEseEricsson v"),
	 @NamedQuery(name="ViwProveFunzEseEricsson.findByCodiSessione", query="SELECT v FROM ViwProveFunzEseEricsson v where v.codiSessione=:codiSessione order by v.codiElemento"),
	})
public class ViwProveFunzEseEricsson implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;

	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;

	@Column(name="NUME_LIVELLO_E")
	private Integer numeLivelloE;

	@Column(name="NUME_ORDER")
	private Integer numeOrder;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;

	public ViwProveFunzEseEricsson() {
	}



	public byte[] getUuid() {
		return uuid;
	}



	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}



	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}



	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}



	public Integer getCodiElemento() {
		return codiElemento;
	}



	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}



	public Integer getCodiSessione() {
		return codiSessione;
	}



	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}



	public String getDescBanda() {
		return descBanda;
	}



	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}



	public String getDescCaratteristica() {
		return descCaratteristica;
	}



	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}



	public String getDescElemento() {
		return descElemento;
	}



	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}



	public String getDescLabel() {
		return descLabel;
	}



	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}



	public String getDescNomeSito() {
		return descNomeSito;
	}



	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}



	public String getDescNote() {
		return descNote;
	}



	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}



	public String getDescSistema() {
		return descSistema;
	}



	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}



	public String getDescTipoOggetto() {
		return descTipoOggetto;
	}



	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}



	public Integer getNumeLivelloE() {
		return numeLivelloE;
	}



	public void setNumeLivelloE(Integer numeLivelloE) {
		this.numeLivelloE = numeLivelloE;
	}



	public Integer getNumeOrder() {
		return numeOrder;
	}



	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}



	public Integer getCodiSurvey() {
		return codiSurvey;
	}



	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	

}