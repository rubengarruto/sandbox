package com.rextart.saw.entity;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TAB_RISULTATO_SURVEY")
@Named("TabRisultatoSurvey")
@NamedQueries({
	@NamedQuery(name="TabRisultatoSurvey.findByCodiSurvey", query="SELECT t FROM TabRisultatoSurvey t WHERE t.codiSurvey = :codiSurvey"),
	@NamedQuery(name="TabRisultatoSurvey.getListByCodiSurvey", query="SELECT t FROM TabRisultatoSurvey t where t.codiSurvey IN (:listCodiSurvey)"),
	@NamedQuery(name="TabRisultatoSurvey.findByCodiSessioneAndCodiOggetto", query="SELECT t FROM TabRisultatoSurvey t WHERE t.codiSessione= :codiSessione AND t.codiOggetto= :codiOggetto"),
})
public class TabRisultatoSurvey {

	@Id
	@SequenceGenerator(name="TAB_RISULTATO_SURVEY_GENERATOR", sequenceName="SEQ_RISULTATO_SURVEY", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_RISULTATO_SURVEY_GENERATOR")
	@Column(name="CODI_RISULTATO_SURVEY")
    private Integer codiRisultato;

	@Column(name="CODI_VALORE_RISULTATO_SURVEY")
    private Integer codiValoreRisultatoSurvey;

	@Column(name="DESC_RISULTATO_SURVEY")
    private String descRisultatoSurvey;

	@Column(name="CODI_SESSIONE")
    private Integer codiSessione;

	@Column(name="CODI_OGGETTO")
    private Integer codiOggetto;

	@Column(name="CODI_UTENTE")
    private Integer codiUtente;

	@Column(name="CODI_SURVEY")
    private Integer codiSurvey;
	
	@Column(name="DESC_VALORE_RISULTATO_SURVEY")
	private String descValoreRisultatoSurvey;
	
	@Column(name="CODI_ANAG_RISULTATO")
	private Integer codiAnagRisultato;

	@Column(name="DESC_REPORT_FOTOGRAFICO")
	private String descReportFotografico;
	
	public Integer getCodiRisultato() {
        return codiRisultato;
    }

    public void setCodiRisultato(Integer codiRisultato) {
        this.codiRisultato = codiRisultato;
    }

    public Integer getCodiValoreRisultatoSurvey() {
        return codiValoreRisultatoSurvey;
    }

    public void setCodiValoreRisultatoSurvey(Integer codiValoreRisultatoSurvey) {
        this.codiValoreRisultatoSurvey = codiValoreRisultatoSurvey;
    }

    public String getDescRisultatoSurvey() {
        return descRisultatoSurvey;
    }

    public void setDescRisultatoSurvey(String descRisultatoSurvey) {
        this.descRisultatoSurvey = descRisultatoSurvey;
    }

    public Integer getCodiSessione() {
        return codiSessione;
    }

    public void setCodiSessione(Integer codiSessione) {
        this.codiSessione = codiSessione;
    }

    public Integer getCodiOggetto() {
        return codiOggetto;
    }

    public void setCodiOggetto(Integer codiOggetto) {
        this.codiOggetto = codiOggetto;
    }

    public Integer getCodiUtente() {
        return codiUtente;
    }

    public void setCodiUtente(Integer codiUtente) {
        this.codiUtente = codiUtente;
    }

    public Integer getCodiSurvey() {
        return codiSurvey;
    }

    public void setCodiSurvey(Integer codiSurvey) {
        this.codiSurvey = codiSurvey;
    }

	public String getDescValoreRisultatoSurvey() {
		return descValoreRisultatoSurvey;
	}

	public void setDescValoreRisultatoSurvey(String descValoreRisultatoSurvey) {
		this.descValoreRisultatoSurvey = descValoreRisultatoSurvey;
	}

	public Integer getCodiAnagRisultato() {
		return codiAnagRisultato;
	}

	public void setCodiAnagRisultato(Integer codiAnagRisultato) {
		this.codiAnagRisultato = codiAnagRisultato;
	}
    
    public String getDescReportFotografico() {
		return descReportFotografico;
	}

	public void setDescReportFotografico(String descReportFotografico) {
		this.descReportFotografico = descReportFotografico;
	}

}
