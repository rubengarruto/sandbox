package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="TAB_OGGETTO_LOCK")
@Named("TabOggettoLock")
@NamedQueries({
	@NamedQuery(name="TabOggettoLock.findAll", query="SELECT t FROM TabOggettoLock t"),
	@NamedQuery(name="TabOggettoLock.findBySessOgg", query="SELECT t FROM TabOggettoLock t where t.codiOggetto= :codiOggetto and t.codiSessione= :codiSessione and t.flagLock=true order by t.timeLastOperation desc")
})
public class TabOggettoLock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_OGGETTO_LOCK_GENERATOR", sequenceName="SEQ_OGGETTO_LOCK",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_OGGETTO_LOCK_GENERATOR")
	@Column(name="CODI_OGGETTO_LOCK")
	private Integer codiOggettoLock;
	
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;
	
	@Column(name="CODI_UTENTE")
	private Integer codiUtente;
	
	@Column(name="FLAG_LOCK")
	private boolean flagLock;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_LAST_OPERATION", nullable=false)
	private Date timeLastOperation;

	
	public Integer getCodiOggettoLock() {
		return codiOggettoLock;
	}

	public void setCodiOggettoLock(Integer codiOggettoLock) {
		this.codiOggettoLock = codiOggettoLock;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public boolean isFlagLock() {
		return flagLock;
	}

	public void setFlagLock(boolean flagLock) {
		this.flagLock = flagLock;
	}

	public Date getTimeLastOperation() {
		return timeLastOperation;
	}

	public void setTimeLastOperation(Date timeLastOperation) {
		this.timeLastOperation = timeLastOperation;
	}
	
}
