package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_DOCUMENTI database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_DOCUMENTI")
@NamedQueries({
	@NamedQuery(name="TabAnagDocumenti.findAll", query="SELECT t FROM TabAnagDocumenti t WHERE t.codiAnagDocumento <>3 AND t.codiAnagDocumento <>4"),
	@NamedQuery(name="TabAnagDocumenti.findAllWithoutD1", query="SELECT t FROM TabAnagDocumenti t WHERE t.codiAnagDocumento <> 1 AND t.codiAnagDocumento <>3 AND t.codiAnagDocumento <>4")
})
public class TabAnagDocumenti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ANAG_DOCUMENTO")
	private Integer codiAnagDocumento;

	@Column(name="DESC_DOCUMENTO")
	private String descDocumento;

	public TabAnagDocumenti() {
	}

	public Integer getCodiAnagDocumento() {
		return this.codiAnagDocumento;
	}

	public void setCodiAnagDocumento(Integer codiAnagDocumento) {
		this.codiAnagDocumento = codiAnagDocumento;
	}

	public String getDescDocumento() {
		return this.descDocumento;
	}

	public void setDescDocumento(String descDocumento) {
		this.descDocumento = descDocumento;
	}

}