package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.*;



/**
 * The persistent class for the VIW_VERBALE database table.
 * 
 */
@Entity
@Table(name="VIW_VERBALE")
@NamedQueries({
	@NamedQuery(name="ViwVerbale.findAll", query="SELECT v FROM ViwVerbale v"),
	@NamedQuery(name="ViwVerbale.findByCodiSessioneList", query="SELECT t FROM ViwVerbale t where t.codiSessione in :codiSessioneList"),
	@NamedQuery(name="ViwVerbale.findBySessioneOggetto", query="SELECT t FROM ViwVerbale t where t.codiSessione = :codiSessione and t.codiOggetto=:codiOggetto"),
	@NamedQuery(name="ViwVerbale.findByCodiSessione", query="SELECT t FROM ViwVerbale t where t.codiSessione = :codiSessione"),
})

public class ViwVerbale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_STATO_VERBALE")
	private Integer codiStatoVerbale;

	@Column(name="FLG_PARZIALE")
	private Integer flgParziale;
	
	@Column(name="DESC_OGGETTO")
	private String descOggetto;

	public ViwVerbale() {
	}

	public Integer getCodiOggetto() {
		return this.codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiStatoVerbale() {
		return this.codiStatoVerbale;
	}

	public void setCodiStatoVerbale(Integer codiStatoVerbale) {
		this.codiStatoVerbale = codiStatoVerbale;
	}

	public Integer getFlgParziale() {
		return this.flgParziale;
	}

	public void setFlgParziale(Integer flgParziale) {
		this.flgParziale = flgParziale;
	}

	public String getDescOggetto() {
		return descOggetto;
	}

	public void setDescOggetto(String descOggetto) {
		this.descOggetto = descOggetto;
	}

}