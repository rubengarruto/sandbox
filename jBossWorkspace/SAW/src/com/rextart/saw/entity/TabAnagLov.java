package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_LOV database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_LOV")
@Named("TabAnagLov")
@NamedQueries({
	@NamedQuery(name="TabAnagLov.findAll", query="SELECT t FROM TabAnagLov t order by t.codiAnagLov desc")
})
public class TabAnagLov implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ANAG_LOV")
	private Integer codiAnagLov;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="FLAG_ALTRO")
	private Boolean flagAltro=false;


	public TabAnagLov() {
	}

	public Integer getCodiAnagLov() {
		return this.codiAnagLov;
	}

	public void setCodiAnagLov(Integer codiAnagLov) {
		this.codiAnagLov = codiAnagLov;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public Boolean getFlagAltro() {
		return this.flagAltro;
	}

	public void setFlagAltro(Boolean flagAltro) {
		this.flagAltro = flagAltro;
	}

	

}