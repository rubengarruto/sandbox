package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_UTE_REGIONI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_UTE_REGIONI")
@NamedQueries({
	@NamedQuery(name="TabRelUteRegioni.findAll", query="SELECT t FROM TabRelUteRegioni t"),
	@NamedQuery(name="TabRelUteRegioni.findSelectedRegioni", query="SELECT t.codiIdRegione FROM TabRelUteRegioni t WHERE t.codiUtente = :codiUtente"),
	@NamedQuery(name="TabRelUteRegioni.deleteByCodiUtente", query="delete from TabRelUteRegioni t where t.codiUtente= :codiUtente"),
	@NamedQuery(name="TabRelUteRegioni.findUtentiByRegione", query="SELECT t.codiUtente FROM TabRelUteRegioni t WHERE t.codiIdRegione = :codiIdRegione")
})


public class TabRelUteRegioni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_UTE_REGIONI_CODI_UTENTE_GENERATOR", sequenceName="SEQ_REL_UTE_REGIONI")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_UTE_REGIONI_CODI_UTENTE_GENERATOR")	
	@Column(name="CODI_REL_UTE_REG")
	private Integer codiRelUteReg;
	
	@Column(name="CODI_UTENTE")
	private Integer codiUtente;
	
	@Column(name="CODI_ID_REGIONE")
	private Integer codiIdRegione;
	
	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	public TabRelUteRegioni() {
	}

	public Integer getCodiAreaComp() {
		return this.codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Integer getCodiIdRegione() {
		return codiIdRegione;
	}

	public void setCodiIdRegione(Integer codiIdRegione) {
		this.codiIdRegione = codiIdRegione;
	}

	public Integer getCodiRelUteReg() {
		return codiRelUteReg;
	}

	public void setCodiRelUteReg(Integer codiRelUteReg) {
		this.codiRelUteReg = codiRelUteReg;
	}

}