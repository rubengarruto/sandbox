package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_SESSIONE_UTENTI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_SESSIONE_UTENTI")
@Named("TabRelSessioneUtenti")
@NamedQueries({
	@NamedQuery(name="TabRelSessioneUtenti.findByIdSessione", query="SELECT s FROM TabRelSessioneUtenti s where s.codiSessione= :codiSessione"),
	@NamedQuery(name="TabRelSessioneUtenti.findByIdSessioneDistinct", query="SELECT s FROM TabRelSessioneUtenti s where s.codiSessione= :codiSessione"),
	@NamedQuery(name="TabRelSessioneUtenti.findAll", query="SELECT t FROM TabRelSessioneUtenti t"),
	@NamedQuery(name="TabRelSessioneUtenti.findByIdUserIdSessione", query="SELECT s FROM TabRelSessioneUtenti s where s.codiSessione= :codiSessione and s.codiUtente= :codiUtente"),
	@NamedQuery(name="TabRelSessioneUtenti.deleteByCodiSessione", query="delete from TabRelSessioneUtenti t where t.codiSessione= :codiSessione"),
	@NamedQuery(name="TabRelSessioneUtenti.deleteByCodiUser", query="delete from TabRelSessioneUtenti t where t.codiUtente= :codiUtente"),
	@NamedQuery(name="TabRelSessioneUtenti.findByCodiUserCodiAreaComp", query="SELECT rs FROM TabRelSessioneUtenti rs,TabSessioni s where s.codiSessione= rs.codiSessione and rs.codiUtente=:codiUtente and s.codiAreaComp<>:codiAreaComp"),
	@NamedQuery(name="TabRelSessioneUtenti.findByIdUser", query="SELECT s FROM TabRelSessioneUtenti s where  s.codiUtente= :codiUtente"),
	@NamedQuery(name="TabRelSessioneUtenti.deleteByCodisUser", query="delete from TabRelSessioneUtenti t where t.codiUtente IN(:codiUtente)"),
	@NamedQuery(name="TabRelSessioneUtenti.deleteByCodisUserAndSessione", query="delete from TabRelSessioneUtenti t where t.codiUtente IN(:codiUtente) and t.codiSessione= :codiSessione"),
})
public class TabRelSessioneUtenti implements Serializable {
	private static final long serialVersionUID = 1L; 

	@Id
	@SequenceGenerator(name="TAB_REL_SESSIONE_UTENTI_CODIRELSESSUTENTI_GENERATOR", sequenceName="SEQ_TAB_REL_SESSIONE_UTENTI", allocationSize=1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_SESSIONE_UTENTI_CODIRELSESSUTENTI_GENERATOR")
	@Column(name="CODI_REL_SESS_UTENTI")
	private int codiRelSessUtenti;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;
	
	@Column(name="CODI_UTENTE")
	private Integer codiUtente;
	
	@ManyToOne
	@JoinColumn(name="CODI_UTENTE",insertable= false, updatable=false)
	private TabUtente tabUser;


	public TabRelSessioneUtenti() {
	}

	public int getCodiRelSessUtenti() {
		return codiRelSessUtenti;
	}

	public void setCodiRelSessUtenti(int codiRelSessUtenti) {
		this.codiRelSessUtenti = codiRelSessUtenti;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}


}