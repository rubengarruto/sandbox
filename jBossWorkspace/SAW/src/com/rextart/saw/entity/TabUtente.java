package com.rextart.saw.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;


/**
 * The persistent class for the TAB_UTENTE database table.
 * 
 */
@Entity
@Table(name="TAB_UTENTE")
@Named("TabUtente")
@NamedQueries({
	@NamedQuery(name="TabUtente.authenticationUser", query="SELECT t FROM TabUtente t where t.descUser=:descUser and t.descPwd=:descPwd"),
	@NamedQuery(name="TabUtente.findAll", query="SELECT t FROM TabUtente t"),
	@NamedQuery(name="TabUtente.findByUser", query="SELECT t FROM TabUtente t where t.descUser=:username and t.flgAbilitato=true"),
	@NamedQuery(name="TabUtente.findUserbyUsername", query="SELECT t FROM TabUtente t where t.descUser=:descUser"),
	@NamedQuery(name="TabUtente.findByEmailAndImei", query="SELECT t FROM TabUtente t where t.descEmail=:descEmail and t.descImei=:descImei and t.flgAbilitato=true"),
	@NamedQuery(name="TabUtente.findUserByRole", query="SELECT t FROM TabUtente t  where t.codiRole=:role and t.codiAreaComp=:areaComp and t.flgAbilitato=true and t.flgReceiveEmail=true"),
	@NamedQuery(name="TabUtente.authenticationByUserName", query="SELECT t FROM TabUtente t where t.descUser=:descUser"),
	@NamedQuery(name="TabUtente.findByCodiUtente", query="SELECT t FROM TabUtente t where t.codiUtente=:codiUtente"),
})
public class TabUtente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_UTENTE_CODIUTENTE_GENERATOR", sequenceName="SEQ_UTENTE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_UTENTE_CODIUTENTE_GENERATOR")
	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	@Column(name="DESC_CGN", nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descCgn;

	@Column(name="DESC_EMAIL", nullable=false, length=255)
	@NotNull(message="� richiesto un valore")
	@Length(max=255)
	@Pattern(regexp="^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", message="Indirizzo mail non corretto.")
	private String descEmail;

	@Column(name="DESC_IMEI", length=20)
	@Length(min=1, max=20)
	private String descImei;

	@Column(name="DESC_NOME", nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descNome;

	@Column(name="DESC_PWD", length=50)
	@Length(min=1, max=50)
	private String descPwd;

	@Column(name="DESC_USER", length=8)
	@Length(min=0, max=8, message="� richiesto un valore di massimo {max} caratteri")
	private String descUser;

	@Column(name="FLG_ABILITATO", nullable=false)
	@NotNull
	private boolean flgAbilitato = false;

	@Column(name="FLG_CESS_SOSP", nullable=false)
	private boolean flgCessSosp = true;
	
	@Column(name="FLG_IS_ADMIN", nullable=false)
	@NotNull
	private boolean flgIsAdmin = false;

	@Column(name="FLG_RECEIVE_EMAIL", nullable=false)
	@NotNull
	private boolean flgReceiveEmail = true;

	@Column(name="FLG_RESET", nullable=false)
	@NotNull
	private boolean flgReset = false;

	@Column(name="FLG_TABLET", nullable=false)
	@NotNull
	private boolean flgTablet = false;
	
	@Column(name="FLG_IMPRESA", nullable=false)
	@NotNull
	private boolean flgImpresa = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CEASED")
	private Date timeCeased;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREAZIONE", nullable=false)
	@NotNull
	private Date timeCreazione = new Timestamp(System.currentTimeMillis());

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_DISABLE")
	private Date timeDisable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_ENABLE")
	private Date timeEnable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_SUSPENDED")
	private Date timeSuspended;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_SUSPENSION")
	private Date timeSuspension;
	
	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;
	
	@Column(name="CODI_STRUTTURA")
	private Integer codiStruttura;
	
	@Column(name="CODI_ROLE")
	private Integer codiRole;
	
	@Column(name="CODI_STATO_UTENTE")
	private Integer codiStatoUtente;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CODI_AREA_COMP",insertable=false,updatable=false)
	private TabAreaCompetenza tabAreaCompetenza;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CODI_STRUTTURA",insertable=false,updatable=false)
	private TabStruttura tabStruttura;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CODI_ROLE",insertable=false,updatable=false)
	private TabRole tabRole;
	
	@Column(name="DESC_TEL_USER_IMP", nullable=true, length=30)
	@Length(min=0, max=30)
	private String descTelUserImp;
	
	@Column(name="CODI_ZONA")
	private Integer codiZona;
	
	@Column(name="CODI_GRUPPO")
	private Integer codiGruppo;
	
	@Column(name="CODI_VENDOR")
	private Integer codiVendor;
	
	@Column(name="DESC_TELEFONO", nullable=false)
	@NotNull(message="� richiesto un valore")
	private String descTelefono;
	
	@Column(name="FLG_CHANGE_PWD", nullable=false)
	@NotNull
	private boolean flgChangePwd = false;

	public TabUtente() {}

	public int getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(int codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescCgn() {
		return descCgn;
	}

	public void setDescCgn(String descCgn) {
		this.descCgn = descCgn;
	}

	public String getDescEmail() {
		return descEmail;
	}

	public void setDescEmail(String descEmail) {
		this.descEmail = descEmail;
	}

	public String getDescImei() {
		return descImei;
	}

	public void setDescImei(String descImei) {
		this.descImei = descImei;
	}

	public String getDescNome() {
		return descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPwd() {
		return descPwd;
	}

	public void setDescPwd(String descPwd) {
		this.descPwd = descPwd;
	}

	public String getDescUser() {
		return descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public boolean isFlgAbilitato() {
		return flgAbilitato;
	}

	public void setFlgAbilitato(boolean flgAbilitato) {
		this.flgAbilitato = flgAbilitato;
	}

	public boolean isFlgCessSosp() {
		return flgCessSosp;
	}

	public void setFlgCessSosp(boolean flgCessSosp) {
		this.flgCessSosp = flgCessSosp;
	}

	public boolean isFlgIsAdmin() {
		return flgIsAdmin;
	}

	public void setFlgIsAdmin(boolean flgIsAdmin) {
		this.flgIsAdmin = flgIsAdmin;
	}

	public boolean isFlgReceiveEmail() {
		return flgReceiveEmail;
	}

	public void setFlgReceiveEmail(boolean flgReceiveEmail) {
		this.flgReceiveEmail = flgReceiveEmail;
	}

	public boolean isFlgReset() {
		return flgReset;
	}

	public void setFlgReset(boolean flgReset) {
		this.flgReset = flgReset;
	}

	public boolean isFlgTablet() {
		return flgTablet;
	}

	public void setFlgTablet(boolean flgTablet) {
		this.flgTablet = flgTablet;
	}

	public Date getTimeCeased() {
		return timeCeased;
	}

	public void setTimeCeased(Date timeCeased) {
		this.timeCeased = timeCeased;
	}

	public Date getTimeCreazione() {
		return timeCreazione;
	}

	public void setTimeCreazione(Date timeCreazione) {
		this.timeCreazione = timeCreazione;
	}

	public Date getTimeDisable() {
		return timeDisable;
	}

	public void setTimeDisable(Date timeDisable) {
		this.timeDisable = timeDisable;
	}

	public Date getTimeEnable() {
		return timeEnable;
	}

	public void setTimeEnable(Date timeEnable) {
		this.timeEnable = timeEnable;
	}

	public Date getTimeSuspended() {
		return timeSuspended;
	}

	public void setTimeSuspended(Date timeSuspended) {
		this.timeSuspended = timeSuspended;
	}

	public Date getTimeSuspension() {
		return timeSuspension;
	}

	public void setTimeSuspension(Date timeSuspension) {
		this.timeSuspension = timeSuspension;
	}

	public TabStruttura getTabStruttura() {
		return tabStruttura;
	}

	public void setTabStruttura(TabStruttura tabStruttura) {
		this.tabStruttura = tabStruttura;
	}

	public TabRole getTabRole() {
		return tabRole;
	}

	public void setTabRole(TabRole tabRole) {
		this.tabRole = tabRole;
	}

	public TabAreaCompetenza getTabAreaCompetenza() {
		return tabAreaCompetenza;
	}

	public void setTabAreaCompetenza(TabAreaCompetenza tabAreaCompetenza) {
		this.tabAreaCompetenza = tabAreaCompetenza;
	}

	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiStruttura() {
		return codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

	public Integer getCodiRole() {
		return codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public boolean isFlgImpresa() {
		return flgImpresa;
	}

	public void setFlgImpresa(boolean flgImpresa) {
		this.flgImpresa = flgImpresa;
	}

	public String getDescTelUserImp() {
		return descTelUserImp;
	}

	public void setDescTelUserImp(String descTelUserImp) {
		this.descTelUserImp = descTelUserImp;
	}

	public Integer getCodiStatoUtente() {
		return codiStatoUtente;
	}

	public void setCodiStatoUtente(Integer codiStatoUtente) {
		this.codiStatoUtente = codiStatoUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public Integer getCodiZona() {
		return codiZona;
	}

	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}

	public Integer getCodiGruppo() {
		return codiGruppo;
	}

	public void setCodiGruppo(Integer codiGruppo) {
		this.codiGruppo = codiGruppo;
	}


	public String getDescTelefono() {
		return descTelefono;
	}

	public void setDescTelefono(String descTelefono) {
		this.descTelefono = descTelefono;
	}

	public Integer getCodiVendor() {
		return codiVendor;
	}

	public void setCodiVendor(Integer codiVendor) {
		this.codiVendor = codiVendor;
	}

	public boolean isFlgChangePwd() {
		return flgChangePwd;
	}

	public void setFlgChangePwd(boolean flgChangePwd) {
		this.flgChangePwd = flgChangePwd;
	}

	
}