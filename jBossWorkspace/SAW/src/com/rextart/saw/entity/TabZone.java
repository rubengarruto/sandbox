package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ZONE database table.
 * 
 */
@Entity
@Table(name="TAB_ZONE")
@Named("TabZone")
@NamedQueries({
	@NamedQuery(name="TabZone.findAll", query="SELECT t FROM TabZone t"),
	@NamedQuery(name="TabZone.findByAreaComp", query="SELECT t FROM TabZone t ,TabRelAreaCompZone az where t.codiZona=az.codiZona and az.codiAreaComp=:codiAreaComp"),
})
public class TabZone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ZONA")
	private Integer codiZona;

	@Column(name="DESC_ZONA")
	private String descZona;

	public TabZone() {
	}

	public Integer getCodiZona() {
		return this.codiZona;
	}

	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}

	public String getDescZona() {
		return this.descZona;
	}

	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}

}