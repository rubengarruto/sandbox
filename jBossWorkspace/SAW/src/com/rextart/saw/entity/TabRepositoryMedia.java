package com.rextart.saw.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the TAB_REPOSITORY_MEDIA database table.
 * 
 */
@Entity
@Table(name="TAB_REPOSITORY_MEDIA")
@Named("TabRepositoryMedia")
@NamedQueries({
	@NamedQuery(name="TabRepositoryMedia.findAll", query="SELECT t FROM TabRepositoryMedia t"),
	@NamedQuery(name="TabRepositoryMedia.findRepMediaByCodiSessione", query="SELECT t FROM TabRepositoryMedia t where t.codiSessione= :codiSessione order by t.descCategoria asc ,t.descName asc"),
	@NamedQuery(name="TabRepositoryMedia.findRepMediaByFilePath", query="SELECT t FROM TabRepositoryMedia t where t.descPath= :descPath"),
	@NamedQuery(name="TabRepositoryMedia.findAllBySessioneAndCategoria", query="SELECT t FROM TabRepositoryMedia t WHERE t.codiSessione=:codiSessione AND t.descCategoria=:descCategoria"),
	@NamedQuery(name="TabRepositoryMedia.findFiveRepMediaByCodiSessione", query="SELECT t FROM TabRepositoryMedia t WHERE t.codiSessione=:codiSessione ORDER BY t.codiRepositoryMedia DESC")
})
public class TabRepositoryMedia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REPOSITORY_MEDIA_CODIREPOSITORYMEDIA_GENERATOR", sequenceName="SEQ_REPOSITORY_MEDIA", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REPOSITORY_MEDIA_CODIREPOSITORYMEDIA_GENERATOR")
	@Column(name="CODI_REPOSITORY_MEDIA")
	private int codiRepositoryMedia;

	@Column(name="CODI_SESSIONE")
	@NotNull(message="� richiesto un valore")
	private Integer codiSessione;

	@Column(name="CODI_UTENTE")
	@NotNull(message="� richiesto un valore")
	private Integer codiUtente;

	@Column(name="DESC_MIME_TYPE",nullable=true)
	private String descMimeType;

	@Column(name="DESC_NAME")
	@NotNull(message="� richiesto un valore")
	private String descName;

	@Column(name="DESC_OWNER")
	@NotNull(message="� richiesto un valore")
	private String descOwner;

	@Column(name="DESC_PATH")
	@NotNull(message="� richiesto un valore")
	private String descPath;
	
	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SIZE",nullable=true)
	private String descSize;
	
	@Column(name="DESC_CATEGORIA",nullable=false)
	private String descCategoria;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREATE")
	@NotNull
	private Date timeCreate= new Timestamp(System.currentTimeMillis());
	

	public TabRepositoryMedia() {
	}

	public int getCodiRepositoryMedia() {
		return codiRepositoryMedia;
	}

	public void setCodiRepositoryMedia(int codiRepositoryMedia) {
		this.codiRepositoryMedia = codiRepositoryMedia;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescMimeType() {
		return descMimeType;
	}

	public void setDescMimeType(String descMimeType) {
		this.descMimeType = descMimeType;
	}

	public String getDescName() {
		return descName;
	}

	public void setDescName(String descName) {
		this.descName = descName;
	}

	public String getDescOwner() {
		return descOwner;
	}

	public void setDescOwner(String descOwner) {
		this.descOwner = descOwner;
	}

	public String getDescPath() {
		return descPath;
	}

	public void setDescPath(String descPath) {
		this.descPath = descPath;
	}

	public String getDescSize() {
		return descSize;
	}

	public void setDescSize(String descSize) {
		this.descSize = descSize;
	}

	public Date getTimeCreate() {
		return timeCreate;
	}

	public void setTimeCreate(Date timeCreate) {
		this.timeCreate = timeCreate;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescCategoria() {
		return descCategoria;
	}

	public void setDescCategoria(String descCategoria) {
		this.descCategoria = descCategoria;
	}

	

}