package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;



/**
 * The persistent class for the TAB_ROLE database table.
 * 
 */
@Entity
@Table(name="TAB_ROLE")
@Named("TabRole")
@NamedQueries({
	@NamedQuery(name="TabRole.findAll", query="SELECT t FROM TabRole t"),
	@NamedQuery(name = "TabRole.findAllNoSupervisorAndImp", query = "select s from TabRole s where s.codiRole<>:ruolo")
})

public class TabRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ROLE")
	private Integer codiRole;

	@Column(name="DESC_CODI_ROLE")
	@NotNull
	@NotBlank
	@Length(min=1, max=50)
	private String descCodiRole;

	@Column(name="DESC_ROLE")
	private String descRole;
	
	
	@Column(name="FLG_OPERATIVO", nullable=false)
	@NotNull
	private boolean flgOperativo = false;

	public TabRole() {
	}

	public Integer getCodiRole() {
		return codiRole;
	}

	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}

	public String getDescCodiRole() {
		return this.descCodiRole;
	}

	public void setDescCodiRole(String descCodiRole) {
		this.descCodiRole = descCodiRole;
	}

	public String getDescRole() {
		return this.descRole;
	}

	public void setDescRole(String descRole) {
		this.descRole = descRole;
	}

}