package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_VER_FUNZ_MISURE_ERICSSON database table.
 * 
 */
@Entity
@Table(name="VIW_VER_FUNZ_MISURE_ERICSSON")
@Named("ViwVerFunzMisureEricsson")
@NamedQueries({
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findAll", query="SELECT v FROM ViwVerFunzMisureEricsson v"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureStrumentiByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=55 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOffsetByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=56 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureCanaliSettoreByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=57 and v.codiCaratteristica<>662 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureCanaliSettoreCellaByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=57 and v.codiCaratteristica=662"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureVerTestReportEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=99 and v.codiCaratteristica=848"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureTratteInFibraEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=58"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOutPowValRifByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=59 and v.codiCaratteristica=669"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureGsmOutPowByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=59 and v.codiCaratteristica=670"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureUmtsOutPowByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=59 and v.codiCaratteristica=671"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureLteOutPowByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=59 and v.codiCaratteristica=672"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=60 and v.codiCaratteristica<>676 and v.codiCaratteristica<>677 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreCellaByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=60 and v.codiCaratteristica=676 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureFrequencyErrorSettoreBranchByCodiSessione", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=60 and v.codiCaratteristica=677 order by v.codiCaratteristica"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureCPICHValRifEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=61 and v.codiCaratteristica=678"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureCPICHSettoreCellaEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=61 and v.codiCaratteristica=679"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDValRifEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=62 and v.codiCaratteristica=680"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDSettoreCellaEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=62 and v.codiCaratteristica=681"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDValRifLTEEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=62 and v.codiCaratteristica=682"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureOCCUPIEDsettoriBranchEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=62 and v.codiCaratteristica=683"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureEVMValRifEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=63 and v.codiCaratteristica=684"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureEVMSettoreCellaEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=63 and v.codiCaratteristica=685"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureEVMValRifLTEEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=63 and v.codiCaratteristica=686"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureEVMsettoriBranchEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=63 and v.codiCaratteristica=687"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureVerTMAEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=64 and (v.codiCaratteristica=688 or v.codiCaratteristica=689)"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureVerRETEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=65 and (v.codiCaratteristica=692 or v.codiCaratteristica=693)"),
	 @NamedQuery(name="ViwVerFunzMisureEricsson.findFunzMisureVerGestioneRETEricsson", query="SELECT v FROM ViwVerFunzMisureEricsson v where v.codiSessione=:codiSessione and v.codiElemento=65 and (v.codiCaratteristica=690 or v.codiCaratteristica=691)"),


	 
})
public class ViwVerFunzMisureEricsson implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;


	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;

	@Column(name="DESC_LABEL_LOV")
	private String descLabelLov;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;

	@Column(name="DESC_VALORE")
	private String descValore;

	@Column(name="NUME_ORDER")
	private Integer numeOrder;
	
	@Column(name="NUME_SETTORE")
	private Integer numeSettore;
	
	@Column(name="NUME_CELLA")
	private Integer numeCella;
	
	@Column(name="NUME_BRANCH")
	private Integer numeBranch;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;
	
	public ViwVerFunzMisureEricsson() {
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCaratteristica() {
		return this.descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public String getDescLabelLov() {
		return this.descLabelLov;
	}

	public void setDescLabelLov(String descLabelLov) {
		this.descLabelLov = descLabelLov;
	}

	public String getDescNomeSito() {
		return this.descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public String getDescTipoOggetto() {
		return this.descTipoOggetto;
	}

	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}

	public String getDescValore() {
		return this.descValore;
	}

	public void setDescValore(String descValore) {
		this.descValore = descValore;
	}

	public Integer getNumeOrder() {
		return this.numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}

	public byte[] getUuid() {
		return this.uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public Integer getNumeSettore() {
		return numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeCella() {
		return numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public Integer getNumeBranch() {
		return numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}
	
	

}