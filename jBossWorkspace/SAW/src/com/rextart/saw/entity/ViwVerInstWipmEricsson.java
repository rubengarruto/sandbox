package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_VER_INST_WIPM_ERICSSON database table.
 * 
 */
@Entity
@Table(name="VIW_VER_INST_WIPM_ERICSSON")
@Named("ViwVerInstWipmEricsson")
@NamedQueries({
	 @NamedQuery(name="ViwVerInstWipmEricsson.findAll", query="SELECT v FROM ViwVerInstWipmEricsson v"),
	 @NamedQuery(name="ViwVerInstWipmEricsson.findByCodiSessione", query="SELECT v FROM ViwVerInstWipmEricsson v where v.codiSessione=:codiSessione order by v.codiElemento"),
	})
public class ViwVerInstWipmEricsson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CARATTERISTICA")
	private String descCaratteristica;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_LABEL")
	private String descLabel;
	
	@Column(name="DESC_LABEL_LOV")
	private String descLabelLov;
	
	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;

	@Column(name="NUME_LIVELLO_E")
	private Integer numeLivelloE;

	@Column(name="NUME_ORDER")
	private Integer numeOrder;

	@Column(name="DESC_VALORE")
	private String descValore;
	
	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;
	

	public ViwVerInstWipmEricsson() {
	}

	public Integer getCodiCaratteristica() {
		return this.codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public Integer getCodiElemento() {
		return this.codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCaratteristica() {
		return this.descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescLabel() {
		return this.descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public String getDescNomeSito() {
		return this.descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public String getDescTipoOggetto() {
		return this.descTipoOggetto;
	}

	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}

	public Integer getNumeLivelloE() {
		return this.numeLivelloE;
	}

	public void setNumeLivelloE(Integer numeLivelloE) {
		this.numeLivelloE = numeLivelloE;
	}

	public Integer getNumeOrder() {
		return this.numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}

	public byte[] getUuid() {
		return this.uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public String getDescLabelLov() {
		return descLabelLov;
	}

	public void setDescLabelLov(String descLabelLov) {
		this.descLabelLov = descLabelLov;
	}

	public String getDescValore() {
		return descValore;
	}

	public void setDescValore(String descValore) {
		this.descValore = descValore;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}
	
	

}