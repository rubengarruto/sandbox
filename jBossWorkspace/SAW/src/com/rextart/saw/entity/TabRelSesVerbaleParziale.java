package com.rextart.saw.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_REL_SES_VERBALE_PARZIALE database table.
 * 
 */
@Entity
@Table(name="TAB_REL_SES_VERBALE_PARZIALE")
@Named("TabRelSesVerbaleParziale")
@NamedQueries({
	@NamedQuery(name="TabRelSesVerbaleParziale.findAll", query="SELECT t FROM TabRelSesVerbaleParziale t"),
	@NamedQuery(name="TabRelSesVerbaleParziale.findByOggettoSessione", query="SELECT t FROM TabRelSesVerbaleParziale t where t.codiOggetto=:codiOggetto and t.codiSessione=:codiSessione order by t.data desc"),
})


public class TabRelSesVerbaleParziale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_SES_VERBALE_PARZIALE", sequenceName="SEQ_TAB_REL_SES_VER_PARZIALE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_SES_VERBALE_PARZIALE")
	@Column(name="CODI_REL_SES_VERBALE_PARZIALE")
	private Integer codiRelSesVerbaleParziale;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA", nullable=false)
	private Date data= new Timestamp(System.currentTimeMillis());;

	@Column(name="FLG_PARZIALE")
	private boolean flgParziale = false;

	public TabRelSesVerbaleParziale() {
	}

	public Integer getCodiRelSesVerbaleParziale() {
		return codiRelSesVerbaleParziale;
	}

	public void setCodiRelSesVerbaleParziale(Integer codiRelSesVerbaleParziale) {
		this.codiRelSesVerbaleParziale = codiRelSesVerbaleParziale;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public boolean isFlgParziale() {
		return flgParziale;
	}

	public void setFlgParziale(boolean flgParziale) {
		this.flgParziale = flgParziale;
	}

	
}