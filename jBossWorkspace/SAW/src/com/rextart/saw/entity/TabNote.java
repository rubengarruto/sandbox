package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_NOTE database table.
 * 
 */
@Entity
@Table(name="TAB_NOTE")
@NamedQueries ({
@NamedQuery(name="TabNote.findByCodiSessione", query="SELECT t FROM TabNote t where t.codiSessione = :codiSessione and t.flgTablet = 0"),
@NamedQuery(name="TabNote.findNoteDittaByCodiSessione", query="SELECT t FROM TabNote t where t.codiSessione = :codiSessione and t.flgTablet = 1"),
@NamedQuery(name="TabNote.findUserbyUsername", query="SELECT t FROM TabNote t where t.descUser=:username and t.codiSessione=:codiSessione"),
@NamedQuery(name="TabNote.getListByCodiSurvey", query="SELECT t FROM TabNote t where t.codiSurvey IN (:listCodiSurvey) and t.flgTablet = 1"),
@NamedQuery(name="TabNote.findByCodiSurveyAndCodiSess", query="SELECT t FROM TabNote t where t.codiSurvey=:codiSurvey)")})
public class TabNote implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@SequenceGenerator(name="TAB_NOTE_WEB_ID_NOTA", sequenceName="SEQ_NOTE_WEB", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_NOTE_WEB_ID_NOTA")	
	@Column(name="ID_NOTA")
	private Integer idNota;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="DESC_NOTA")
	private String descNota;
	
	@Column(name="FLG_TABLET")
	private Boolean flgTablet;
	
	@Column(name="DESC_USER")
	private String descUser;
	
	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;


	public TabNote() {
	}

	public Integer getIdNota() {
		return this.idNota;
	}

	public void setIdNota(Integer idNota) {
		this.idNota = idNota;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescNota() {
		return this.descNota;
	}

	public void setDescNota(String descNota) {
		this.descNota = descNota;
	}

	public Boolean getFlgTablet() {
		return flgTablet;
	}

	public void setFlgTablet(Boolean flgTablet) {
		this.flgTablet = flgTablet;
	}

	public String getDescUser() {
		return descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

}