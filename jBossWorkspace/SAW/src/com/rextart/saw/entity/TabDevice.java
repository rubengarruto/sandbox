package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/**
 * The persistent class for the TAB_DEVICE database table.
 * 
 */
@Entity
@Table(name="TAB_DEVICE")
@Named("TabDevice")
@NamedQueries({
	@NamedQuery(name="TabDevice.findAll", query="SELECT t FROM TabDevice t"),
	@NamedQuery(name="TabDevice.findByImei", query="SELECT t FROM TabDevice t where t.descImei = :descImei"),
	@NamedQuery(name="TabDevice.findByCodiDevice", query="SELECT t FROM TabDevice t where t.codiDevice not in (:codiciDevice)"),
	
})
public class TabDevice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_DEVICE_CODIDEVICE_GENERATOR", sequenceName="SEQ_TAB_DEVICE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_DEVICE_CODIDEVICE_GENERATOR")
	@Column(name="CODI_DEVICE")
	private int codiDevice;

	@Column(name="DESC_IMEI", unique=true,length=20)
	@NotNull(message="� richiesto un valore")
	@Length(min=1, max=20, message="� richiesto un valore di massimo {max} caratteri")
	private String descImei;


	@Column(name="FLAG_IMPRESA")
	private Integer flagImpresa;


	
	public TabDevice() {
	}

	public int getCodiDevice() {
		return this.codiDevice;
	}

	public void setCodiDevice(int codiDevice) {
		this.codiDevice = codiDevice;
	}

	public String getDescImei() {
		return this.descImei;
	}

	public void setDescImei(String descImei) {
		this.descImei = descImei;
	}

	public Integer getFlagImpresa() {
		return flagImpresa;
	}

	public void setFlagImpresa(Integer flagImpresa) {
		this.flagImpresa = flagImpresa;
	}

	

}