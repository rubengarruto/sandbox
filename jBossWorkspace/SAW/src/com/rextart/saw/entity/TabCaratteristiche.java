package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_CARATTERISTICHE database table.
 * 
 */
@Entity
@Table(name="TAB_CARATTERISTICHE")
@Named("TabCaratteristiche")
@NamedQueries({
	@NamedQuery(name="TabCaratteristiche.findAll", query="SELECT t FROM TabCaratteristiche t")
})
public class TabCaratteristiche implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_CARATTERISTICHE_CODICARATTERISTICA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_CARATTERISTICHE_CODICARATTERISTICA_GENERATOR")
	@Column(name="CODI_CARATTERISTICA")
	private Integer codiCaratteristica;

	@Column(name="DESC_CARATTERISTICA")
    private String descCaratteristica;
	
	@Column(name="MEMO_HELP")
    private String memoHelp;
	
	
	@Column(name="DESC_COMPONENTE")
    private String descComponente;
	
	@Column(name="FLAG_OBBLIGATORIO")
    private boolean flagObbligatorio;
	
	@Column(name="DESC_LABEL_CARAT_1")
    private String descLabelCarat1;
	
	@Column(name="DESC_LABEL_CARAT_2")
    private String descLabelCarat2;
	
	@Column(name="DESC_LABEL_CARAT_3")
    private String descLabelCarat3;
	
	@Column(name="DESC_LABEL_CARAT_4")
    private String descLabelCarat4;
	
	@Column(name="DESC_LABEL_CARAT_5")
    private String descLabelCarat5;
	
	@Column(name="FLAG_LOV_1")
    private boolean flagLov1;
	
	@Column(name="FLAG_LOV_2")
    private boolean flagLov2;
	
	@Column(name="FLAG_LOV_3")
    private boolean flagLov3;
	
	@Column(name="FLAG_LOV_4")
    private boolean flagLov4;
	
	@Column(name="FLAG_LOV_5")
    private boolean flagLov5;
	
	@Column(name="NUME_E")
    private Integer numeE;
	
	@Column(name="NUME_BRANCH")
    private Integer numeBranch;
	
	@Column(name="NUME_SETTORE")
    private Integer numeSettore;
	
	@Column(name="NUME_VIA")
    private Integer numeVia;
	
	@Column(name="NUME_CELLA")
    private Integer numeCella;
	
	@Column(name="FLAG_NOTE")
	private boolean flagNote;

	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}

	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}

	public String getDescCaratteristica() {
		return descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getMemoHelp() {
		return memoHelp;
	}

	public void setMemoHelp(String memoHelp) {
		this.memoHelp = memoHelp;
	}

	public String getDescComponente() {
		return descComponente;
	}

	public void setDescComponente(String descComponente) {
		this.descComponente = descComponente;
	}

	public boolean isFlagObbligatorio() {
		return flagObbligatorio;
	}

	public void setFlagObbligatorio(boolean flagObbligatorio) {
		this.flagObbligatorio = flagObbligatorio;
	}

	public String getDescLabelCarat1() {
		return descLabelCarat1;
	}

	public void setDescLabelCarat1(String descLabelCarat1) {
		this.descLabelCarat1 = descLabelCarat1;
	}

	public String getDescLabelCarat2() {
		return descLabelCarat2;
	}

	public void setDescLabelCarat2(String descLabelCarat2) {
		this.descLabelCarat2 = descLabelCarat2;
	}

	public String getDescLabelCarat3() {
		return descLabelCarat3;
	}

	public void setDescLabelCarat3(String descLabelCarat3) {
		this.descLabelCarat3 = descLabelCarat3;
	}

	public String getDescLabelCarat4() {
		return descLabelCarat4;
	}

	public void setDescLabelCarat4(String descLabelCarat4) {
		this.descLabelCarat4 = descLabelCarat4;
	}

	public String getDescLabelCarat5() {
		return descLabelCarat5;
	}

	public void setDescLabelCarat5(String descLabelCarat5) {
		this.descLabelCarat5 = descLabelCarat5;
	}

	public boolean isFlagLov1() {
		return flagLov1;
	}

	public void setFlagLov1(boolean flagLov1) {
		this.flagLov1 = flagLov1;
	}

	public boolean isFlagLov2() {
		return flagLov2;
	}

	public void setFlagLov2(boolean flagLov2) {
		this.flagLov2 = flagLov2;
	}

	public boolean isFlagLov3() {
		return flagLov3;
	}

	public void setFlagLov3(boolean flagLov3) {
		this.flagLov3 = flagLov3;
	}

	public boolean isFlagLov4() {
		return flagLov4;
	}

	public void setFlagLov4(boolean flagLov4) {
		this.flagLov4 = flagLov4;
	}

	public boolean isFlagLov5() {
		return flagLov5;
	}

	public void setFlagLov5(boolean flagLov5) {
		this.flagLov5 = flagLov5;
	}

	public Integer getNumeE() {
		return numeE;
	}

	public void setNumeE(Integer numeE) {
		this.numeE = numeE;
	}

	public Integer getNumeBranch() {
		return numeBranch;
	}

	public void setNumeBranch(Integer numeBranch) {
		this.numeBranch = numeBranch;
	}

	public Integer getNumeSettore() {
		return numeSettore;
	}

	public void setNumeSettore(Integer numeSettore) {
		this.numeSettore = numeSettore;
	}

	public Integer getNumeVia() {
		return numeVia;
	}

	public void setNumeVia(Integer numeVia) {
		this.numeVia = numeVia;
	}

	public Integer getNumeCella() {
		return numeCella;
	}

	public void setNumeCella(Integer numeCella) {
		this.numeCella = numeCella;
	}

	public boolean isFlagNote() {
		return flagNote;
	}

	public void setFlagNote(boolean flagNote) {
		this.flagNote = flagNote;
	}

	
}