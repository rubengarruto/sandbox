package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_SESS_IMP_USER database table.
 * 
 */
@Entity
@Table(name="VIW_SESS_IMP_USER")
@NamedQueries({
@NamedQuery(name="ViwSessImpUser.findAll", query="SELECT v FROM ViwSessImpUser v"),
@NamedQuery(name="ViwSessImpUser.findDittaByCodiSessione", query="SELECT v FROM ViwSessImpUser v WHERE v.codiSessione=:codiSessione"),
@NamedQuery(name="ViwSessImpUser.findUsersByCodiImpresaAndCodiSessione", query="SELECT v FROM ViwSessImpUser v WHERE v.codiImpresa=:codiImpresa AND v.codiSessione=:codiSessione"),
})
public class ViwSessImpUser implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_IMPRESA")
	private Integer codiImpresa;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_UTENTE")
	private Integer codiUtente;

	@Column(name="DESC_IMPRESA")
	private String descImpresa;

	@Column(name="DESC_USER")
	private String descUser;
	
	@Column(name="DESC_NOME")
	private String descNome;

	@Column(name="DESC_CGN")
	private String descCgn;
	

	public ViwSessImpUser() {
	}

	public byte[] getUuid() {
		return uuid;
	}

	public void setUuid(byte[] uuid) {
		this.uuid = uuid;
	}

	public Integer getCodiImpresa() {
		return this.codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiUtente() {
		return this.codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getDescImpresa() {
		return this.descImpresa;
	}

	public void setDescImpresa(String descImpresa) {
		this.descImpresa = descImpresa;
	}

	public String getDescUser() {
		return this.descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

	public String getDescNome() {
		return descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescCgn() {
		return descCgn;
	}

	public void setDescCgn(String descCgn) {
		this.descCgn = descCgn;
	}

}