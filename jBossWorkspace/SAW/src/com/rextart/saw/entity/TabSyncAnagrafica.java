package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="TAB_SYNC_ANAGRAFICA")
@Named("TabSyncAnagrafica")
@NamedQueries({
	@NamedQuery(name="TabSyncAnagrafica.findAll", query="SELECT t FROM TabSyncAnagrafica t"),
	@NamedQuery(name="TabSyncAnagrafica.getByNameTable", query="select t FROM TabSyncAnagrafica t where t.descNameTable=:descNameTable"),
})
public class TabSyncAnagrafica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_SYNC_ANAG")
	private Integer codiSyncAnag;
	
	@Column(name="DESC_NAME_TABLE")
	private String descNameTable;
	
	@Column(name="NUME_VERSIONE")
	private Integer numeVersione;
	
	public Integer getCodiSyncAnag() {
		return codiSyncAnag;
	}
	public void setCodiSyncAnag(Integer codiSyncAnag) {
		this.codiSyncAnag = codiSyncAnag;
	}
	public String getDescNameTable() {
		return descNameTable;
	}
	public void setDescNameTable(String descNameTable) {
		this.descNameTable = descNameTable;
	}
	public Integer getNumeVersione() {
		return numeVersione;
	}
	public void setNumeVersione(Integer numeVersione) {
		this.numeVersione = numeVersione;
	}
}
