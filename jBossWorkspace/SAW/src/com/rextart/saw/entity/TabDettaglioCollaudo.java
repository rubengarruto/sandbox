package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_DETTAGLIO_COLLAUDO database table.
 * 
 */
@Entity
@Table(name="TAB_DETTAGLIO_COLLAUDO")
@Named("TabDettaglioCollaudo")
@NamedQuery(name="TabDettaglioCollaudo.findAll", query="SELECT t FROM TabDettaglioCollaudo t order by t.descDettaglioColl")
public class TabDettaglioCollaudo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_DETTAGLIO_COLL")
	private Integer codiDettaglioColl;

	@Column(name="DESC_DETTAGLIO_COLL")
	private String descDettaglioColl;

	public TabDettaglioCollaudo() {
	}

	public Integer getCodiDettaglioColl() {
		return this.codiDettaglioColl;
	}

	public void setCodiDettaglioColl(Integer codiDettaglioColl) {
		this.codiDettaglioColl = codiDettaglioColl;
	}

	public String getDescDettaglioColl() {
		return this.descDettaglioColl;
	}

	public void setDescDettaglioColl(String descDettaglioColl) {
		this.descDettaglioColl = descDettaglioColl;
	}

}