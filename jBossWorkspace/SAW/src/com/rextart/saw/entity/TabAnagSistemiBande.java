package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_SISTEMI_BANDE database table.
 * 
 */
@Entity
@Table(name="TAB_ANAG_SISTEMI_BANDE")
@Named("TabAnagSistemiBande")
@NamedQueries({
	@NamedQuery(name="TabAnagSistemiBande.findAll", query="SELECT t FROM TabAnagSistemiBande t"),
	@NamedQuery(name="TabAnagSistemiBande.findBandaSistemaByCodiImpianto", query="SELECT t FROM TabAnagSistemiBande t where t.descIdentificativo=:descIdentificativo"),
	@NamedQuery(name="TabAnagSistemiBande.findIdentificativoByBandaAndSistema", query="SELECT t FROM TabAnagSistemiBande t where t.codiBanda=:codiBanda and t.codiSistema=:codiSistema"),
})
public class TabAnagSistemiBande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ANAG_SISTEMI_BANDE")
	private Integer codiAnagSistemiBande;

	@Column(name="CODI_BANDA")
	private Integer codiBanda;

	@Column(name="CODI_SISTEMA")
	private Integer codiSistema;

	@Column(name="DESC_IDENTIFICATIVO")
	private String descIdentificativo;

	public TabAnagSistemiBande() {
	}

	public long getCodiAnagSistemiBande() {
		return this.codiAnagSistemiBande;
	}

	public Integer getCodiBanda() {
		return codiBanda;
	}

	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}

	public Integer getCodiSistema() {
		return codiSistema;
	}

	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}

	public void setCodiAnagSistemiBande(Integer codiAnagSistemiBande) {
		this.codiAnagSistemiBande = codiAnagSistemiBande;
	}

	public String getDescIdentificativo() {
		return this.descIdentificativo;
	}

	public void setDescIdentificativo(String descIdentificativo) {
		this.descIdentificativo = descIdentificativo;
	}

}