package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/**
 * The persistent class for the TAB_STRUTTURA database table.
 * 
 */
@Entity
@Table(name="TAB_STRUTTURA")
@Named("TabStruttura")
@NamedQueries({
	@NamedQuery(name = "TabStruttura.findAll", query = "select s from TabStruttura s"),
})
public class TabStruttura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_STRUTTURA")
	private Integer codiStruttura;

	@Column(name="DESC_STRUTTURA", nullable=false, length=50)
	@NotNull
	@Length(min=1, max=50)
	private String descStruttura;

	public TabStruttura() {}

	public Integer getCodiStruttura() {
		return this.codiStruttura;
	}

	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}

	public String getDescStruttura() {
		return descStruttura;
	}

	public void setDescStruttura(String descStruttura) {
		this.descStruttura = descStruttura;
	}
	
}