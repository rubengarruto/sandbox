package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_IMPIANTI database table.
 * 
 */
@Entity
@Table(name="TAB_IMPIANTI")
@Named("TabImpianti")
@NamedQuery(name="TabImpianti.findAll", query="SELECT t FROM TabImpianti t order by t.descImpianto")
public class TabImpianti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_TIPO_IMPIANTO")
	private Integer codiTipoImpianto;

	@Column(name="DESC_IMPIANTO")
	private String descImpianto;

	public TabImpianti() {
	}

	public Integer getCodiTipoImpianto() {
		return this.codiTipoImpianto;
	}

	public void setCodiTipoImpianto(Integer codiTipoImpianto) {
		this.codiTipoImpianto = codiTipoImpianto;
	}

	public String getDescImpianto() {
		return this.descImpianto;
	}

	public void setDescImpianto(String descImpianto) {
		this.descImpianto = descImpianto;
	}

}