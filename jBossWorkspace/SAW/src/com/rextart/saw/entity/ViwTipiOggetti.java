package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_TIPI_OGGETTI database table.
 * 
 */
@Entity
@Table(name="VIW_TIPI_OGGETTI")
@NamedQueries({
	@NamedQuery(name="ViwTipiOggetti.findAll", query="SELECT v FROM ViwTipiOggetti v"),
	@NamedQuery(name="ViwTipiOggetti.findByOggetto", query="SELECT v FROM ViwTipiOggetti v WHERE v.codiOggetto = :codiOggetto order by v.codiTipoOggetto desc")
})
public class ViwTipiOggetti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="DESC_LABEL_FILTER_TIPO_OGGETTO")
	private String descLabelFilterTipoOggetto;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;

	@Column(name="FLG_DATI_GENERALI")
	private Integer flgDatiGenerali;

	@Column(name="FLG_DITTA")
	private Integer flgDitta;

	public ViwTipiOggetti() {
	}

	public Integer getCodiTipoOggetto() {
		return this.codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public String getDescLabelFilterTipoOggetto() {
		return this.descLabelFilterTipoOggetto;
	}

	public void setDescLabelFilterTipoOggetto(String descLabelFilterTipoOggetto) {
		this.descLabelFilterTipoOggetto = descLabelFilterTipoOggetto;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescTipoOggetto() {
		return this.descTipoOggetto;
	}

	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}

	public Integer getFlgDatiGenerali() {
		return this.flgDatiGenerali;
	}

	public void setFlgDatiGenerali(Integer flgDatiGenerali) {
		this.flgDatiGenerali = flgDatiGenerali;
	}

	public Integer getFlgDitta() {
		return this.flgDitta;
	}

	public void setFlgDitta(Integer flgDitta) {
		this.flgDitta = flgDitta;
	}
	
	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}
}