package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_OGGETTI_ELEMENTI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_OGGETTI_ELEMENTI")
@Named("TabRelOggettiElementi")
@NamedQuery(name="TabRelOggettiElementi.findAll", query="SELECT t FROM TabRelOggettiElementi t")
public class TabRelOggettiElementi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REL_OGGETTI_ELEMENTI_CODIRELOGGELEM_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REL_OGGETTI_ELEMENTI_CODIRELOGGELEM_GENERATOR")
	@Column(name="CODI_REL_OGG_ELEM")
	private int codiRelOggElem;

	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;
	
	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;
	
	@Column(name="NUME_ORDER")
	private Integer numeOrder;
	
	public TabRelOggettiElementi() {
	}

	public int getCodiRelOggElem() {
		return this.codiRelOggElem;
	}

	public void setCodiRelOggElem(int codiRelOggElem) {
		this.codiRelOggElem = codiRelOggElem;
	}

	public Integer getCodiElemento() {
		return codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public Integer getNumeOrder() {
		return numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}

	
	
}