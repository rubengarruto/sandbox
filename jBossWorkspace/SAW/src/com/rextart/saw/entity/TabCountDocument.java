package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_COUNT_DOCUMENT database table.
 * 
 */
@Entity
@Table(name="TAB_COUNT_DOCUMENT")
@NamedQueries({
@NamedQuery(name="TabCountDocument.findAll", query="SELECT t FROM TabCountDocument t"),
@NamedQuery(name="TabCountDocument.findBySessioneCodiDoc", query="SELECT t FROM TabCountDocument t where t.codiSessione =:codiSessione and  t.codiAnagDocumento=:codiAnagDocumento")
})
public class TabCountDocument implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="TAB_COUNT_DOCUMENT_CODI_COUNT_DOCUMENT_GENERATOR", sequenceName="SEQ_COUNT_DOCUMENT", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_COUNT_DOCUMENT_CODI_COUNT_DOCUMENT_GENERATOR")
	@Column(name="CODI_COUNT_DOCUMENT")
	private Integer codiCountDocument;

	@Column(name="CODI_ANAG_DOCUMENTO")
	private Integer codiAnagDocumento;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="COUNT_DOCUMENT")
	private Integer countDocument;

	public TabCountDocument() {
	}

	public Integer getCodiCountDocument() {
		return codiCountDocument;
	}

	public void setCodiCountDocument(Integer codiCountDocument) {
		this.codiCountDocument = codiCountDocument;
	}

	public Integer getCodiAnagDocumento() {
		return codiAnagDocumento;
	}

	public void setCodiAnagDocumento(Integer codiAnagDocumento) {
		this.codiAnagDocumento = codiAnagDocumento;
	}

	public Integer getCodiSessione() {
		return codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCountDocument() {
		return countDocument;
	}

	public void setCountDocument(Integer countDocument) {
		this.countDocument = countDocument;
	}

	

}