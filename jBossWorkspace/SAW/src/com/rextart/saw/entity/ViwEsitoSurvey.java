package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_ESITO_SURVEY database table.
 * 
 */
@Entity
@Table(name="VIW_ESITO_SURVEY")
@Named("ViwEsitoSurvey")
@NamedQueries({
	@NamedQuery(name="ViwEsitoSurvey.findAll", query="SELECT v FROM ViwEsitoSurvey v"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVeriInstEricsson", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=12"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerFunzionaliApparati", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=13"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoProveFunzEseApparati", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=15"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoProveFunzSerEricsson", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=16"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerificheInstWIPM", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=17"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerbaleRadiantiA", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=1"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerbaleRadiantiB", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=2"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerbaleRadiantiC", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=3"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerbaleRadiantiD", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=5"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoVerbaleRadiantiE", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=4"),
	@NamedQuery(name="ViwEsitoSurvey.findEsitoByTipoOggetto", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.codiTipoOggetto=:codiTipoOggetto"),
	@NamedQuery(name="ViwEsitoSurvey.findNoteEsitoByCodiSessione", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.descRisultatoSurvey is not null"),
	@NamedQuery(name="ViwEsitoSurvey.findNoteDittaByCodiSessione", query="SELECT v FROM ViwEsitoSurvey v where v.codiSessione=:codiSessione and v.descNota is not null"),

})
public class ViwEsitoSurvey implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="\"UUID\"")
	private byte[] uuid;
	
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_SURVEY")
	private Integer codiSurvey;

	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_RISULTATO_SURVEY")
	private String descRisultatoSurvey;

	@Column(name="DESC_SISTEMA")
	private String descSistema;
	
	@Column(name="DESC_NOTA")
	private String descNota;

	@Column(name="RISULTATO")
	private String risultato;
	
	@Column(name="DESC_REPORT_FOTOGRAFICO")
	private String descReportFotografico;
	
	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;
	
	@Column(name="DESC_USER")
	private String descUser;

	
	public ViwEsitoSurvey() {
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescRisultatoSurvey() {
		return this.descRisultatoSurvey;
	}

	public void setDescRisultatoSurvey(String descRisultatoSurvey) {
		this.descRisultatoSurvey = descRisultatoSurvey;
	}

	public String getRisultato() {
		return this.risultato;
	}

	public void setRisultato(String risultato) {
		this.risultato = risultato;
	}

	public String getDescSistema() {
		return descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public String getDescNomeSito() {
		return descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescBanda() {
		return descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public Integer getCodiSurvey() {
		return codiSurvey;
	}

	public void setCodiSurvey(Integer codiSurvey) {
		this.codiSurvey = codiSurvey;
	}

	public String getDescNota() {
		return descNota;
	}

	public void setDescNota(String descNota) {
		this.descNota = descNota;
	}

	public String getDescReportFotografico() {
		return descReportFotografico;
	}

	public void setDescReportFotografico(String descReportFotografico) {
		this.descReportFotografico = descReportFotografico;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public String getDescUser() {
		return descUser;
	}

	public void setDescUser(String descUser) {
		this.descUser = descUser;
	}

}