package com.rextart.saw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIW_ELEMENTI_OGGETTI database table.
 * 
 */
@Entity
@Table(name="VIW_ELEMENTI_OGGETTI")
@NamedQueries({
	@NamedQuery(name="ViwElementiOggetti.findAll", query="SELECT v FROM ViwElementiOggetti v"),
	@NamedQuery(name="ViwElementiOggetti.findByTipo", query="SELECT v FROM ViwElementiOggetti v WHERE v.codiTipoOggetto = :codiTipoOggetto order by v.numeOrder")
})
public class ViwElementiOggetti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_ELEMENTO")
	private Integer codiElemento;

	@Column(name="CODI_OGGETTO")
	private Integer codiOggetto;

	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	@Column(name="DESC_ELEMENTO")
	private String descElemento;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="NUME_ORDER")
	private Integer numeOrder;

	public ViwElementiOggetti() {
	}

	public String getDescElemento() {
		return this.descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public Integer getCodiElemento() {
		return codiElemento;
	}

	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public Integer getNumeOrder() {
		return numeOrder;
	}

	public void setNumeOrder(Integer numeOrder) {
		this.numeOrder = numeOrder;
	}
}