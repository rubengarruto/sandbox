package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/**
 * The persistent class for the VIW_SESSIONI database table.
 * 
 */
@Entity
@Table(name="VIW_SESSIONI")
@Named("ViwSessioni")
@NamedQueries({
 @NamedQuery(name="ViwSessioni.findAll", query="SELECT v FROM ViwSessioni v"),
 @NamedQuery(name="ViwSessioni.findCollaudiInLavorazione", query="SELECT v FROM ViwSessioni v where v.codiStato<>:stato and (v.codiStato=:statoFiltro  or :statoFiltro IS NULL) order by v.timeAcquisizione asc"),
 @NamedQuery(name="ViwSessioni.findCollaudiInLavorazioneByAreaComp", query="SELECT v FROM ViwSessioni v where v.codiStato<>:stato and v.codiAreaComp=:codiAreaComp and (v.codiStato=:statoFiltro  or :statoFiltro IS NULL) order by v.timeAcquisizione asc"),
 @NamedQuery(name="ViwSessioni.findCollaudiArchiviati", query="SELECT v FROM ViwSessioni v where v.codiStato=:stato order by v.timeChiuso desc"),
 @NamedQuery(name="ViwSessioni.findCollaudiArchiviatiByAreaComp", query="SELECT v FROM ViwSessioni v where v.codiStato=:stato and v.codiAreaComp=:codiAreaComp order by v.timeChiuso desc"),
 @NamedQuery(name="ViwSessioni.findByCodiSessione", query="SELECT v FROM ViwSessioni v where v.codiSessione=:codiSessione"),
 @NamedQuery(name="ViwSessioni.findCollaudiInLavorazioneByUtenteVendor", query="SELECT v FROM ViwSessioni v,TabPoloTecnologico t where v.codiStato<>:stato and v.descRegione=t.descRegione and t.codiVendor=:codiVendor and (v.codiStato=:statoFiltro  or :statoFiltro IS NULL) order by v.timeAcquisizione asc"),
 @NamedQuery(name="ViwSessioni.findCollaudiArchiviatiByUtenteVendor", query="SELECT v FROM ViwSessioni v,TabPoloTecnologico t where v.codiStato=:stato and v.descRegione=t.descRegione and t.codiVendor=:codiVendor order by v.timeChiuso desc"),
 @NamedQuery(name="ViwSessioni.findCollaudiInLavorazioneByMoiAssoc", query="SELECT v FROM ViwSessioni v ,TabRelSessioneUtenti u where v.codiStato<>:stato and v.codiSessione=u.codiSessione and u.codiUtente=:codiUtente and (v.codiStato=:statoFiltro  or :statoFiltro IS NULL) order by v.codiSessione desc"),
 @NamedQuery(name="ViwSessioni.findCollaudiArchiviatiByMoiAssoc", query="SELECT v FROM ViwSessioni v ,TabRelSessioneUtenti u where v.codiStato=:stato and v.codiSessione=u.codiSessione and u.codiUtente=:codiUtente order by v.codiSessione desc"),
 @NamedQuery(name="ViwSessioni.findCollaudiInLavorazioneByMoi", query="SELECT v FROM ViwSessioni v,TabRelSessioneUtenti u ,TabRelImpreseUtenti iu,TabImprese i where v.codiStato<>:stato and v.codiSessione=u.codiSessione and u.codiUtente=iu.codiUtente and iu.codiImpresa=i.codiImpresa and i.codiImpresa=:codiImpresa and (v.codiStato=:statoFiltro  or :statoFiltro IS NULL) order by v.timeAcquisizione asc"),
 @NamedQuery(name="ViwSessioni.findCollaudiArchiviatiByMoi", query="SELECT v FROM ViwSessioni v,TabRelSessioneUtenti u ,TabRelImpreseUtenti iu,TabImprese i where v.codiStato=:stato and v.codiSessione=u.codiSessione and u.codiUtente=iu.codiUtente and iu.codiImpresa=i.codiImpresa and i.codiImpresa=:codiImpresa order by v.timeChiuso desc"),
 @NamedQuery(name="ViwSessioni.findViwSessioniToClone", query="SELECT v FROM ViwSessioni v where v.versione=(Select max(vs.versione)from ViwSessioni vs where vs.codiCollaudo=:codiCollaudo) and v.codiCollaudo=:codiCollaudo"),
 @NamedQuery(name="ViwSessioni.findByCodiCollaudo", query="SELECT v FROM ViwSessioni v where v.codiCollaudo=:codiCollaudo"),
})
public class ViwSessioni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CLLI_SITO")
	private String clliSito;

	@Column(name="CODI_AREA_COMP")
	private Integer codiAreaComp;

	@Column(name="CODI_BANDA")
	private Integer codiBanda;

	@Column(name="CODI_COLLAUDO")
	private Integer codiCollaudo;

	@Column(name="CODI_DETTAGLIO_COLL")
	private Integer codiDettaglioColl;

	@Column(name="CODI_FINALITA_COLL")
	private Integer codiFinalitaColl;

	@Id
	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Column(name="CODI_SISTEMA")
	private Integer codiSistema;

	@Column(name="CODI_LAST_STATO")
	private Integer codiStato;

	@Column(name="CODI_TIPO_IMPIANTO")
	private Integer codiTipoImpianto;

	@Column(name="CODI_UNITA_TERR")
	private Integer codiUnitaTerr;

	@Column(name="CODICE_DBR")
	private String codiceDbr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_SCHEDA_PR_RA_ES")
	private Date dataSchedaPrRaEs;

	@Column(name="DESC_AREA_COMP")
	private String descAreaComp;

	@Column(name="DESC_BANDA")
	private String descBanda;

	@Column(name="DESC_CODICE")
	private String descCodice;

	@Column(name="DESC_COMUNE")
	private String descComune;

	@Column(name="DESC_DETTAGLIO_COLL")
	private String descDettaglioColl;

	@Column(name="DESC_FINALITA_COLL")
	private String descFinalitaColl;

	@Column(name="DESC_IMPIANTO")
	private String descImpianto;

	@Column(name="DESC_INDIRIZZO")
	private String descIndirizzo;

	@Column(name="DESC_LATITUDINE")
	private String descLatitudine;

	@Column(name="DESC_LONGITUDINE")
	private String descLongitudine;

	@Column(name="DESC_NOME_SITO")
	private String descNomeSito;

	@Column(name="DESC_NOTE")
	private String descNote;

	@Column(name="DESC_OPER_TIM")
	private String descOperTim;

	@Column(name="DESC_PROGETTO_RADIO")
	private String descProgettoRadio;

	@Column(name="DESC_PROVINCIA")
	private String descProvincia;

	@Column(name="DESC_REGIONE")
	private String descRegione;

	@Column(name="DESC_SISTEMA")
	private String descSistema;

	@Column(name="DESC_STATO")
	private String descStato;

	@Column(name="DESC_ZONA")
	private String descZona;

	@Column(name="FLG_ALLEGATO_A")
	private Boolean flgAllegatoA;

	@Column(name="FLG_ALLEGATO_B")
	private Boolean flgAllegatoB;

	@Column(name="FLG_ALLEGATO_C")
	private Boolean flgAllegatoC;

	@Column(name="FLG_ALLEGATO_D")
	private Boolean flgAllegatoD;
	
	@Column(name="FLG_ALLEGATO_D1")
	private Boolean flgAllegatoD1;

	@Column(name="FLG_ALLEGATO_E")
	private Boolean flgAllegatoE;

	@Column(name="FLG_APPARATO_ERICSSON")
	private Boolean flgApparatoEricsson;

	@Column(name="FLG_APPARATO_HUAWEI")
	private Boolean flgApparatoHuawei;

	@Column(name="FLG_APPARATO_NSN")
	private Boolean flgApparatoNsn;

	@Column(name="FLG_LOCK_A")
	private Boolean flgLockA;

	@Column(name="FLG_LOCK_B")
	private Boolean flgLockB;

	@Column(name="FLG_LOCK_C")
	private Boolean flgLockC;

	@Column(name="FLG_LOCK_D")
	private Boolean flgLockD;

	@Column(name="FLG_LOCK_E")
	private Boolean flgLockE;

	@Column(name="FLG_LOCK_ERICSSON")
	private Boolean flgLockEricsson;

	@Column(name="FLG_LOCK_HUAWEI")
	private Boolean flgLockHuawei;

	@Column(name="FLG_LOCK_NSN")
	private Boolean flgLockNsn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_ACQUISIZIONE")
	private Date timeAcquisizione;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_PREVISTO_COLLAUDO")
	private Date timePrevistoCollaudo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CHIUSO")
	private Date timeChiuso;
	
	@Column(name="DESC_UNITA_TERR")
	private String descUnitaTerr;
	
	@Column(name="CODI_STATO_A")
	private Integer codiStatoA;
	
	@Column(name="CODI_STATO_B")
	private Integer codiStatoB;
	
	@Column(name="CODI_STATO_C")
	private Integer codiStatoC;
	
	@Column(name="CODI_STATO_D1")
	private Integer codiStatoD1;
	
	@Column(name="CODI_STATO_D2")
	private Integer codiStatoD2;
	
	@Column(name="CODI_STATO_E")
	private Integer codiStatoE;
	
	@Column(name="CODI_STATO_ERICSSON")
	private Integer codiStatoEricsson;
	
	@Column(name="CODI_STATO_HUAWEI")
	private Integer codiStatoHuawei;
	
	@Column(name="CODI_STATO_NOKIA")
	private Integer codiStatoNokia;
	
//	@Column(name="CODI_IMPRESA")
//	private Integer codiImpresa;
	
	//@Column(name="CODI_WORK_REQUEST")
	@Transient
	private Integer codiWorkRequest;
	
	@Temporal(TemporalType.TIMESTAMP)
	//@Column(name="DATA_WORK_REQUEST")
	@Transient
	private Date dataWorkRequest;
	
	@Transient
	private String descNumeroWrs;
	
	@Column(name="FLG_SPEDITO_PEV")
	private Boolean flgSpeditoPev;
	
	@Column(name="VERSIONE")
	private Integer versione;

	public ViwSessioni() {
	}

	public String getClliSito() {
		return this.clliSito;
	}

	public void setClliSito(String clliSito) {
		this.clliSito = clliSito;
	}

	public Integer getCodiAreaComp() {
		return this.codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiBanda() {
		return this.codiBanda;
	}

	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}

	public Integer getCodiCollaudo() {
		return this.codiCollaudo;
	}

	public void setCodiCollaudo(Integer codiCollaudo) {
		this.codiCollaudo = codiCollaudo;
	}

	public Integer getCodiDettaglioColl() {
		return this.codiDettaglioColl;
	}

	public void setCodiDettaglioColl(Integer codiDettaglioColl) {
		this.codiDettaglioColl = codiDettaglioColl;
	}

	public Integer getCodiFinalitaColl() {
		return this.codiFinalitaColl;
	}

	public void setCodiFinalitaColl(Integer codiFinalitaColl) {
		this.codiFinalitaColl = codiFinalitaColl;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public Integer getCodiSistema() {
		return this.codiSistema;
	}

	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}

	public Integer getCodiStato() {
		return this.codiStato;
	}

	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}

	public Integer getCodiTipoImpianto() {
		return this.codiTipoImpianto;
	}

	public void setCodiTipoImpianto(Integer codiTipoImpianto) {
		this.codiTipoImpianto = codiTipoImpianto;
	}

	public Integer getCodiUnitaTerr() {
		return this.codiUnitaTerr;
	}

	public void setCodiUnitaTerr(Integer codiUnitaTerr) {
		this.codiUnitaTerr = codiUnitaTerr;
	}

	public String getCodiceDbr() {
		return this.codiceDbr;
	}

	public void setCodiceDbr(String codiceDbr) {
		this.codiceDbr = codiceDbr;
	}

	public Date getDataSchedaPrRaEs() {
		return this.dataSchedaPrRaEs;
	}

	public void setDataSchedaPrRaEs(Date dataSchedaPrRaEs) {
		this.dataSchedaPrRaEs = dataSchedaPrRaEs;
	}

	public String getDescAreaComp() {
		return this.descAreaComp;
	}

	public void setDescAreaComp(String descAreaComp) {
		this.descAreaComp = descAreaComp;
	}

	public String getDescBanda() {
		return this.descBanda;
	}

	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}

	public String getDescCodice() {
		return this.descCodice;
	}

	public void setDescCodice(String descCodice) {
		this.descCodice = descCodice;
	}

	public String getDescComune() {
		return this.descComune;
	}

	public void setDescComune(String descComune) {
		this.descComune = descComune;
	}

	public String getDescDettaglioColl() {
		return this.descDettaglioColl;
	}

	public void setDescDettaglioColl(String descDettaglioColl) {
		this.descDettaglioColl = descDettaglioColl;
	}

	public String getDescFinalitaColl() {
		return this.descFinalitaColl;
	}

	public void setDescFinalitaColl(String descFinalitaColl) {
		this.descFinalitaColl = descFinalitaColl;
	}

	public String getDescImpianto() {
		return this.descImpianto;
	}

	public void setDescImpianto(String descImpianto) {
		this.descImpianto = descImpianto;
	}

	public String getDescIndirizzo() {
		return this.descIndirizzo;
	}

	public void setDescIndirizzo(String descIndirizzo) {
		this.descIndirizzo = descIndirizzo;
	}

	public String getDescLatitudine() {
		return this.descLatitudine;
	}

	public void setDescLatitudine(String descLatitudine) {
		this.descLatitudine = descLatitudine;
	}

	public String getDescLongitudine() {
		return this.descLongitudine;
	}

	public void setDescLongitudine(String descLongitudine) {
		this.descLongitudine = descLongitudine;
	}

	public String getDescNomeSito() {
		return this.descNomeSito;
	}

	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}

	public String getDescNote() {
		return this.descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescOperTim() {
		return this.descOperTim;
	}

	public void setDescOperTim(String descOperTim) {
		this.descOperTim = descOperTim;
	}

	public String getDescProgettoRadio() {
		return this.descProgettoRadio;
	}

	public void setDescProgettoRadio(String descProgettoRadio) {
		this.descProgettoRadio = descProgettoRadio;
	}

	public String getDescProvincia() {
		return this.descProvincia;
	}

	public void setDescProvincia(String descProvincia) {
		this.descProvincia = descProvincia;
	}

	public String getDescRegione() {
		return this.descRegione;
	}

	public void setDescRegione(String descRegione) {
		this.descRegione = descRegione;
	}

	public String getDescSistema() {
		return this.descSistema;
	}

	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}

	public String getDescStato() {
		return this.descStato;
	}

	public void setDescStato(String descStato) {
		this.descStato = descStato;
	}

	public String getDescZona() {
		return this.descZona;
	}

	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}

	public Date getTimeAcquisizione() {
		return timeAcquisizione;
	}

	public void setTimeAcquisizione(Date timeAcquisizione) {
		this.timeAcquisizione = timeAcquisizione;
	}

	public Boolean getFlgAllegatoA() {
		return flgAllegatoA;
	}

	public void setFlgAllegatoA(Boolean flgAllegatoA) {
		this.flgAllegatoA = flgAllegatoA;
	}

	public Boolean getFlgAllegatoB() {
		return flgAllegatoB;
	}

	public void setFlgAllegatoB(Boolean flgAllegatoB) {
		this.flgAllegatoB = flgAllegatoB;
	}

	public Boolean getFlgAllegatoC() {
		return flgAllegatoC;
	}

	public void setFlgAllegatoC(Boolean flgAllegatoC) {
		this.flgAllegatoC = flgAllegatoC;
	}

	public Boolean getFlgAllegatoD() {
		return flgAllegatoD;
	}

	public void setFlgAllegatoD(Boolean flgAllegatoD) {
		this.flgAllegatoD = flgAllegatoD;
	}

	public Boolean getFlgAllegatoE() {
		return flgAllegatoE;
	}

	public void setFlgAllegatoE(Boolean flgAllegatoE) {
		this.flgAllegatoE = flgAllegatoE;
	}

	public Boolean getFlgApparatoEricsson() {
		return flgApparatoEricsson;
	}

	public void setFlgApparatoEricsson(Boolean flgApparatoEricsson) {
		this.flgApparatoEricsson = flgApparatoEricsson;
	}

	public Boolean getFlgApparatoHuawei() {
		return flgApparatoHuawei;
	}

	public void setFlgApparatoHuawei(Boolean flgApparatoHuawei) {
		this.flgApparatoHuawei = flgApparatoHuawei;
	}

	public Boolean getFlgApparatoNsn() {
		return flgApparatoNsn;
	}

	public void setFlgApparatoNsn(Boolean flgApparatoNsn) {
		this.flgApparatoNsn = flgApparatoNsn;
	}

	public Boolean getFlgLockA() {
		return flgLockA;
	}

	public void setFlgLockA(Boolean flgLockA) {
		this.flgLockA = flgLockA;
	}

	public Boolean getFlgLockB() {
		return flgLockB;
	}

	public void setFlgLockB(Boolean flgLockB) {
		this.flgLockB = flgLockB;
	}

	public Boolean getFlgLockC() {
		return flgLockC;
	}

	public void setFlgLockC(Boolean flgLockC) {
		this.flgLockC = flgLockC;
	}

	public Boolean getFlgLockD() {
		return flgLockD;
	}

	public void setFlgLockD(Boolean flgLockD) {
		this.flgLockD = flgLockD;
	}

	public Boolean getFlgLockE() {
		return flgLockE;
	}

	public void setFlgLockE(Boolean flgLockE) {
		this.flgLockE = flgLockE;
	}

	public Boolean getFlgLockEricsson() {
		return flgLockEricsson;
	}

	public void setFlgLockEricsson(Boolean flgLockEricsson) {
		this.flgLockEricsson = flgLockEricsson;
	}

	public Boolean getFlgLockHuawei() {
		return flgLockHuawei;
	}

	public void setFlgLockHuawei(Boolean flgLockHuawei) {
		this.flgLockHuawei = flgLockHuawei;
	}

	public Boolean getFlgLockNsn() {
		return flgLockNsn;
	}

	public void setFlgLockNsn(Boolean flgLockNsn) {
		this.flgLockNsn = flgLockNsn;
	}

	public Date getTimePrevistoCollaudo() {
		return timePrevistoCollaudo;
	}

	public void setTimePrevistoCollaudo(Date timePrevistoCollaudo) {
		this.timePrevistoCollaudo = timePrevistoCollaudo;
	}
	
	public Date getTimeChiuso() {
		return timeChiuso;
	}

	public void setTimeChiuso(Date timeChiuso) {
		this.timeChiuso = timeChiuso;
	}


	public String getDescUnitaTerr() {
		return descUnitaTerr;
	}

	public void setDescUnitaTerr(String descUnitaTerr) {
		this.descUnitaTerr = descUnitaTerr;
	}

	public Boolean getFlgAllegatoD1() {
		return flgAllegatoD1;
	}

	public void setFlgAllegatoD1(Boolean flgAllegatoD1) {
		this.flgAllegatoD1 = flgAllegatoD1;
	}

	public Integer getCodiWorkRequest() {
		return codiWorkRequest;
	}

	public void setCodiWorkRequest(Integer codiWorkRequest) {
		this.codiWorkRequest = codiWorkRequest;
	}

	public Date getDataWorkRequest() {
		return dataWorkRequest;
	}

	public void setDataWorkRequest(Date dataWorkRequest) {
		this.dataWorkRequest = dataWorkRequest;
	}

	public Integer getCodiStatoA() {
		return codiStatoA;
	}

	public void setCodiStatoA(Integer codiStatoA) {
		this.codiStatoA = codiStatoA;
	}

	public Integer getCodiStatoB() {
		return codiStatoB;
	}

	public void setCodiStatoB(Integer codiStatoB) {
		this.codiStatoB = codiStatoB;
	}

	public Integer getCodiStatoC() {
		return codiStatoC;
	}

	public void setCodiStatoC(Integer codiStatoC) {
		this.codiStatoC = codiStatoC;
	}

	public Integer getCodiStatoD1() {
		return codiStatoD1;
	}

	public void setCodiStatoD1(Integer codiStatoD1) {
		this.codiStatoD1 = codiStatoD1;
	}

	public Integer getCodiStatoD2() {
		return codiStatoD2;
	}

	public void setCodiStatoD2(Integer codiStatoD2) {
		this.codiStatoD2 = codiStatoD2;
	}

	public Integer getCodiStatoE() {
		return codiStatoE;
	}

	public void setCodiStatoE(Integer codiStatoE) {
		this.codiStatoE = codiStatoE;
	}

	public Integer getCodiStatoEricsson() {
		return codiStatoEricsson;
	}

	public void setCodiStatoEricsson(Integer codiStatoEricsson) {
		this.codiStatoEricsson = codiStatoEricsson;
	}

	public Integer getCodiStatoHuawei() {
		return codiStatoHuawei;
	}

	public void setCodiStatoHuawei(Integer codiStatoHuawei) {
		this.codiStatoHuawei = codiStatoHuawei;
	}

	public Integer getCodiStatoNokia() {
		return codiStatoNokia;
	}

	public void setCodiStatoNokia(Integer codiStatoNokia) {
		this.codiStatoNokia = codiStatoNokia;
	}

//	public Integer getCodiImpresa() {
//		return codiImpresa;
//	}
//
//	public void setCodiImpresa(Integer codiImpresa) {
//		this.codiImpresa = codiImpresa;
//	}
	
	public Boolean getFlgSpeditoPev() {
		return flgSpeditoPev;
	}

	public void setFlgSpeditoPev(Boolean flgSpeditoPev) {
		this.flgSpeditoPev = flgSpeditoPev;
	}

	public Integer getVersione() {
		return versione;
	}

	public void setVersione(Integer versione) {
		this.versione = versione;
	}

	public String getDescNumeroWrs() {
		return descNumeroWrs;
	}

	public void setDescNumeroWrs(String descNumeroWrs) {
		this.descNumeroWrs = descNumeroWrs;
	}

}