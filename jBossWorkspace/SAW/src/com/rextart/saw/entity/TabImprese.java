package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the TAB_IMPRESE database table.
 * 
 */
@Entity
@Table(name="TAB_IMPRESE")
@Named("TabImprese")
@NamedQueries({
	@NamedQuery(name="TabImprese.findAll", query="SELECT t FROM TabImprese t"),
	@NamedQuery(name="TabImprese.findByDescCf", query="SELECT t FROM TabImprese t where t.descCf=:descCf"),
//	@NamedQuery(name="TabImprese.getImpresaByCfAndByCodiImpresa", query="SELECT t FROM TabImprese t where t.descCf=:descCf and t.codiImpresa=:codiImpresa")
	@NamedQuery(name="TabImprese.getImpresaByCfAndByCodiImpresa", query="SELECT t FROM TabImprese t where t.descCf=:descCf and t.codiImpresa<>:codiImpresa")

})
public class TabImprese implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_IMPRESE_CODI_IMPRESA_GENERATOR", sequenceName="SEQ_IMPRESE",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_IMPRESE_CODI_IMPRESA_GENERATOR")
	@Column(name="CODI_IMPRESA")
	private Integer codiImpresa;

	@Column(name="DESC_CAP", nullable=true, length=5)
	private String descCap;

	@Column(name="DESC_CF", nullable=true, length=16)
	@Length(min=0, max=16, message="� richiesto un valore di massimo {max} caratteri")
	private String descCf;

	@Column(name="DESC_EMAIL", nullable=false, length=50)
	@NotNull(message="� richiesto un valore")
	@NotBlank(message="deve contenere almeno un carattere valido")
	@Length(min=1, max=50, message="� richiesto un valore di massimo {max} caratteri")
	@Pattern(regexp="^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$", message="Indirizzo mail non corretto.")
	private String descEmail;

	@Column(name="DESC_FAX",nullable=true, length=100)
	@Length(min=0, max=100, message="� richiesto un valore di massimo {max} caratteri")
	private String descFax;

	@Column(name="DESC_IMPRESA",nullable=false, length=100)
	@NotNull(message="� richiesto un valore")
	@NotBlank(message="deve contenere almeno un carattere valido")
	@Length(min=1, max=100, message="� richiesto un valore di massimo {max} caratteri")
	private String descImpresa;

	@Column(name="DESC_INDIRIZZO",nullable=true, length= 255)
	@Length(min=0, max=255, message="� richiesto un valore di massimo {max} caratteri")
	private String descIndirizzo;

	@Column(name="DESC_LOCALITA",nullable=true, length= 255)
	@Length(min=0, max=255, message="� richiesto un valore di massimo {max} caratteri")
	private String descLocalita;

	@Column(name="DESC_TEL",nullable=false,length=100)
	@NotNull(message="� richiesto un valore")
	@Length(min=0, max=100)
	private String descTel;
	
	@Column(name="FLG_IS_CERTIFIED", nullable=false)
	@NotNull
	private boolean flgIsCertified = false;

	public TabImprese() {
	}

	public Integer getCodiImpresa() {
		return this.codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

	public String getDescCap() {
		return this.descCap;
	}

	public void setDescCap(String descCap) {
		this.descCap = descCap;
	}

	public String getDescCf() {
		return this.descCf;
	}

	public void setDescCf(String descCf) {
		this.descCf = descCf;
	}

	public String getDescEmail() {
		return this.descEmail;
	}

	public void setDescEmail(String descEmail) {
		this.descEmail = descEmail;
	}

	public String getDescFax() {
		return this.descFax;
	}

	public void setDescFax(String descFax) {
		this.descFax = descFax;
	}

	public String getDescImpresa() {
		return this.descImpresa;
	}

	public void setDescImpresa(String descImpresa) {
		this.descImpresa = descImpresa;
	}

	public String getDescIndirizzo() {
		return this.descIndirizzo;
	}

	public void setDescIndirizzo(String descIndirizzo) {
		this.descIndirizzo = descIndirizzo;
	}

	public String getDescLocalita() {
		return this.descLocalita;
	}

	public void setDescLocalita(String descLocalita) {
		this.descLocalita = descLocalita;
	}

	public String getDescTel() {
		return this.descTel;
	}

	public void setDescTel(String descTel) {
		this.descTel = descTel;
	}

	public boolean isFlgIsCertified() {
		return flgIsCertified;
	}

	public void setFlgIsCertified(boolean flgIsCertified) {
		this.flgIsCertified = flgIsCertified;
	}

}