package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_REL_RITARDI_SESSIONI database table.
 * 
 */
@Entity
@Table(name="TAB_REL_RITARDI_SESSIONI")
@Named("TabRelRitardiSessioni")
@NamedQueries({
	@NamedQuery(name="TabRelRitardiSessioni.findAll", query="SELECT t FROM TabRelRitardiSessioni t"),
	@NamedQuery(name="TabRelRitardiSessioni.findByCodiSessione", query="SELECT t FROM TabRelRitardiSessioni t where t.codiSessione = :codiSessione order by t.codiRelRitardiSessioni desc")
})


public class TabRelRitardiSessioni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CODI_REL_RITARDI_SESSIONI_GENERATOR" , sequenceName="SEQ_TAB_REL_RITARDI_SESSIONI",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CODI_REL_RITARDI_SESSIONI_GENERATOR")
	@Column(name="CODI_REL_RITARDI_SESSIONI")
	private Integer codiRelRitardiSessioni;

	@Column(name="CODI_ANAG_RITARDO")
	private Integer codiAnagRitardo;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;
	
	@ManyToOne
	@JoinColumn(name="CODI_ANAG_RITARDO",insertable=false, updatable=false)
	private TabAnagRitardi tabAnagRitardi; 

	public TabRelRitardiSessioni() {
	}

	public Integer getCodiRelRitardiSessioni() {
		return this.codiRelRitardiSessioni;
	}

	public void setCodiRelRitardiSessioni(Integer codiRelRitardiSessioni) {
		this.codiRelRitardiSessioni = codiRelRitardiSessioni;
	}

	public Integer getCodiAnagRitardo() {
		return this.codiAnagRitardo;
	}

	public void setCodiAnagRitardo(Integer codiAnagRitardo) {
		this.codiAnagRitardo = codiAnagRitardo;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public TabAnagRitardi getTabAnagRitardi() {
		return tabAnagRitardi;
	}

	public void setTabAnagRitardi(TabAnagRitardi tabAnagRitardi) {
		this.tabAnagRitardi = tabAnagRitardi;
	}

	
}