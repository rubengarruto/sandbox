package com.rextart.saw.entity;

import java.io.Serializable;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TAB_ANAG_ATTIVITA database table.
 * 
 */
@Entity
@Table(name="TAB_TIPOLOGIE_OGGETTO")
@Named("TabTipologieOggetto")
@NamedQueries({
	@NamedQuery(name="TabTipologieOggetto.findAll", query="SELECT t FROM TabTipologieOggetto t order by t.descNote"),
	@NamedQuery(name="TabTipologieOggetto.findAllByCodiObj", query="select t FROM TabTipologieOggetto t, TabRelOggettiTipo r where t.codiTipoOggetto=r.codiTipoOggetto and r.codiOggetto= :codiOggetto order by t.descNote"),
})
public class TabTipologieOggetto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODI_TIPO_OGGETTO")
	private Integer codiTipoOggetto;

	@Column(name="DESC_TIPO_OGGETTO")
	private String descTipoOggetto;
	
	@Column(name="DESC_NOTE")
	private String descNote;
	
	@Column(name="DESC_LABEL_FILTER_TIPO_OGGETTO")
	private String descLabelFilterTipoOggetto;
	
	@Column(name="FLG_DATI_GENERALI")
	private boolean flgDatiGenerali;
	
	@Column(name="FLG_DITTA")
	private Integer flgDitta;

	public Integer getCodiTipoOggetto() {
		return codiTipoOggetto;
	}

	public void setCodiTipoOggetto(Integer codiTipoOggetto) {
		this.codiTipoOggetto = codiTipoOggetto;
	}

	public String getDescTipoOggetto() {
		return descTipoOggetto;
	}

	public void setDescTipoOggetto(String descTipoOggetto) {
		this.descTipoOggetto = descTipoOggetto;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}

	public String getDescLabelFilterTipoOggetto() {
		return descLabelFilterTipoOggetto;
	}

	public void setDescLabelFilterTipoOggetto(String descLabelFilterTipoOggetto) {
		this.descLabelFilterTipoOggetto = descLabelFilterTipoOggetto;
	}

	public boolean isFlgDatiGenerali() {
		return flgDatiGenerali;
	}

	public void setFlgDatiGenerali(boolean flgDatiGenerali) {
		this.flgDatiGenerali = flgDatiGenerali;
	}

	public Integer getFlgDitta() {
		return flgDitta;
	}

	public void setFlgDitta(Integer flgDitta) {
		this.flgDitta = flgDitta;
	}

	
	

	
}