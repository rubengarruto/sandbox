package com.rextart.saw.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TAB_REPOSITORY_SCHEDA_RISCHI database table.
 * 
 */
@Entity
@Table(name="TAB_REPOSITORY_SCHEDA_RISCHI")
@NamedQueries({
	@NamedQuery(name="TabRepositorySchedaRischi.findAll", query="SELECT t FROM TabRepositorySchedaRischi t"),
	@NamedQuery(name="TabRepositorySchedaRischi.findBySessioneNome", query="SELECT t FROM TabRepositorySchedaRischi t where t.codiSessione= :codiSessione and t.descNome= :descNome"),
	@NamedQuery(name="TabRepositorySchedaRischi.findByCodiSessione", query="SELECT t FROM TabRepositorySchedaRischi t where t.codiSessione= :codiSessione"),
	
})
public class TabRepositorySchedaRischi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAB_REPOSITORY_SCHEDA_RISCHI_CODIREPOSCHEDARISCHI_GENERATOR", sequenceName="SEQ_REPOSITORY_SCHEDA_RISCHI", allocationSize= 1  )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAB_REPOSITORY_SCHEDA_RISCHI_CODIREPOSCHEDARISCHI_GENERATOR")
	@Column(name="CODI_REPO_SCHEDA_RISCHI")
	private Integer codiRepoSchedaRischi;

	@Column(name="CODI_SESSIONE")
	private Integer codiSessione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME_CREATE")
	private Date timeCreate;

	@Column(name="DESC_MIME_TYPE")
	private String descMimeType;

	@Column(name="DESC_NOME")
	private String descNome;

	@Column(name="DESC_PATH")
	private String descPath;

	@Column(name="FLAG_SCHEDA_RISCHI")
	private boolean flagSchedaRischi;
	
	@Column(name="VERSIONE")
	private String versione;

	public TabRepositorySchedaRischi() {
	}

	public Integer getCodiRepoSchedaRischi() {
		return this.codiRepoSchedaRischi;
	}

	public void setCodiRepoSchedaRischi(Integer codiRepoSchedaRischi) {
		this.codiRepoSchedaRischi = codiRepoSchedaRischi;
	}

	public Integer getCodiSessione() {
		return this.codiSessione;
	}

	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}

	public String getDescMimeType() {
		return this.descMimeType;
	}

	public void setDescMimeType(String descMimeType) {
		this.descMimeType = descMimeType;
	}

	public String getDescNome() {
		return this.descNome;
	}

	public void setDescNome(String descNome) {
		this.descNome = descNome;
	}

	public String getDescPath() {
		return this.descPath;
	}

	public void setDescPath(String descPath) {
		this.descPath = descPath;
	}

	public Date getTimeCreate() {
		return timeCreate;
	}

	public void setTimeCreate(Date timeCreate) {
		this.timeCreate = timeCreate;
	}

	public boolean isFlagSchedaRischi() {
		return flagSchedaRischi;
	}

	public void setFlagSchedaRischi(boolean flagSchedaRischi) {
		this.flagSchedaRischi = flagSchedaRischi;
	}

	public String getVersione() {
		return versione;
	}

	public void setVersione(String versione) {
		this.versione = versione;
	}
	
	

}