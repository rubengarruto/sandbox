
package com.rextart.saw.ws.collaudoToSaw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoA" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoB" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoC" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoD1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoD1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoD2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoD2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameMisurazioniApogeo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="misurazioniApogeo1" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameMisurazioniApogeo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="misurazioniApogeo2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameMisurazioniApogeo3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="misurazioniApogeo3" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoE" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameAllegatoApparati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="allegatoApparati" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="causaliChiusura">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="SI"/>
 *               &lt;enumeration value="NO"/>
 *               &lt;enumeration value="SI_CON_RISERVA"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="descrizioneRegolaEsito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileNameFotoSala" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fotoSala" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameFotoSupporto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fotoSupporto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameFotoCella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fotoCella" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fileNameFotoLocalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fotoLocalita" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idCo",
    "note",
    "fileNameAllegatoA",
    "allegatoA",
    "fileNameAllegatoB",
    "allegatoB",
    "fileNameAllegatoC",
    "allegatoC",
    "fileNameAllegatoD1",
    "allegatoD1",
    "fileNameAllegatoD2",
    "allegatoD2",
    "fileNameMisurazioniApogeo1",
    "misurazioniApogeo1",
    "fileNameMisurazioniApogeo2",
    "misurazioniApogeo2",
    "fileNameMisurazioniApogeo3",
    "misurazioniApogeo3",
    "fileNameAllegatoE",
    "allegatoE",
    "fileNameAllegatoApparati",
    "allegatoApparati",
    "causaliChiusura",
    "descrizioneRegolaEsito",
    "fileNameFotoSala",
    "fotoSala",
    "fileNameFotoSupporto",
    "fotoSupporto",
    "fileNameFotoCella",
    "fotoCella",
    "fileNameFotoLocalita",
    "fotoLocalita"
})
@XmlRootElement(name = "CompletamentoCollaudoApparatiRequest")
public class CompletamentoCollaudoApparatiRequest {

    protected int idCo;
    protected String note;
    protected String fileNameAllegatoA;
    protected byte[] allegatoA;
    protected String fileNameAllegatoB;
    protected byte[] allegatoB;
    protected String fileNameAllegatoC;
    protected byte[] allegatoC;
    protected String fileNameAllegatoD1;
    protected byte[] allegatoD1;
    protected String fileNameAllegatoD2;
    protected byte[] allegatoD2;
    protected String fileNameMisurazioniApogeo1;
    protected byte[] misurazioniApogeo1;
    protected String fileNameMisurazioniApogeo2;
    protected byte[] misurazioniApogeo2;
    protected String fileNameMisurazioniApogeo3;
    protected byte[] misurazioniApogeo3;
    protected String fileNameAllegatoE;
    protected byte[] allegatoE;
    protected String fileNameAllegatoApparati;
    protected byte[] allegatoApparati;
    @XmlElement(required = true)
    protected String causaliChiusura;
    protected String descrizioneRegolaEsito;
    protected String fileNameFotoSala;
    protected byte[] fotoSala;
    protected String fileNameFotoSupporto;
    protected byte[] fotoSupporto;
    protected String fileNameFotoCella;
    protected byte[] fotoCella;
    protected String fileNameFotoLocalita;
    protected byte[] fotoLocalita;

    /**
     * Recupera il valore della proprietÓ idCo.
     * 
     */
    public int getIdCo() {
        return idCo;
    }

    /**
     * Imposta il valore della proprietÓ idCo.
     * 
     */
    public void setIdCo(int value) {
        this.idCo = value;
    }

    /**
     * Recupera il valore della proprietÓ note.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Imposta il valore della proprietÓ note.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoA() {
        return fileNameAllegatoA;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoA(String value) {
        this.fileNameAllegatoA = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoA.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoA() {
        return allegatoA;
    }

    /**
     * Imposta il valore della proprietÓ allegatoA.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoA(byte[] value) {
        this.allegatoA = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoB() {
        return fileNameAllegatoB;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoB(String value) {
        this.fileNameAllegatoB = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoB.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoB() {
        return allegatoB;
    }

    /**
     * Imposta il valore della proprietÓ allegatoB.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoB(byte[] value) {
        this.allegatoB = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoC() {
        return fileNameAllegatoC;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoC(String value) {
        this.fileNameAllegatoC = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoC.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoC() {
        return allegatoC;
    }

    /**
     * Imposta il valore della proprietÓ allegatoC.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoC(byte[] value) {
        this.allegatoC = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoD1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoD1() {
        return fileNameAllegatoD1;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoD1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoD1(String value) {
        this.fileNameAllegatoD1 = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoD1.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoD1() {
        return allegatoD1;
    }

    /**
     * Imposta il valore della proprietÓ allegatoD1.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoD1(byte[] value) {
        this.allegatoD1 = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoD2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoD2() {
        return fileNameAllegatoD2;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoD2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoD2(String value) {
        this.fileNameAllegatoD2 = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoD2.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoD2() {
        return allegatoD2;
    }

    /**
     * Imposta il valore della proprietÓ allegatoD2.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoD2(byte[] value) {
        this.allegatoD2 = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameMisurazioniApogeo1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameMisurazioniApogeo1() {
        return fileNameMisurazioniApogeo1;
    }

    /**
     * Imposta il valore della proprietÓ fileNameMisurazioniApogeo1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameMisurazioniApogeo1(String value) {
        this.fileNameMisurazioniApogeo1 = value;
    }

    /**
     * Recupera il valore della proprietÓ misurazioniApogeo1.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMisurazioniApogeo1() {
        return misurazioniApogeo1;
    }

    /**
     * Imposta il valore della proprietÓ misurazioniApogeo1.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMisurazioniApogeo1(byte[] value) {
        this.misurazioniApogeo1 = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameMisurazioniApogeo2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameMisurazioniApogeo2() {
        return fileNameMisurazioniApogeo2;
    }

    /**
     * Imposta il valore della proprietÓ fileNameMisurazioniApogeo2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameMisurazioniApogeo2(String value) {
        this.fileNameMisurazioniApogeo2 = value;
    }

    /**
     * Recupera il valore della proprietÓ misurazioniApogeo2.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMisurazioniApogeo2() {
        return misurazioniApogeo2;
    }

    /**
     * Imposta il valore della proprietÓ misurazioniApogeo2.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMisurazioniApogeo2(byte[] value) {
        this.misurazioniApogeo2 = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameMisurazioniApogeo3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameMisurazioniApogeo3() {
        return fileNameMisurazioniApogeo3;
    }

    /**
     * Imposta il valore della proprietÓ fileNameMisurazioniApogeo3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameMisurazioniApogeo3(String value) {
        this.fileNameMisurazioniApogeo3 = value;
    }

    /**
     * Recupera il valore della proprietÓ misurazioniApogeo3.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMisurazioniApogeo3() {
        return misurazioniApogeo3;
    }

    /**
     * Imposta il valore della proprietÓ misurazioniApogeo3.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMisurazioniApogeo3(byte[] value) {
        this.misurazioniApogeo3 = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoE() {
        return fileNameAllegatoE;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoE(String value) {
        this.fileNameAllegatoE = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoE.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoE() {
        return allegatoE;
    }

    /**
     * Imposta il valore della proprietÓ allegatoE.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoE(byte[] value) {
        this.allegatoE = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameAllegatoApparati.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameAllegatoApparati() {
        return fileNameAllegatoApparati;
    }

    /**
     * Imposta il valore della proprietÓ fileNameAllegatoApparati.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameAllegatoApparati(String value) {
        this.fileNameAllegatoApparati = value;
    }

    /**
     * Recupera il valore della proprietÓ allegatoApparati.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegatoApparati() {
        return allegatoApparati;
    }

    /**
     * Imposta il valore della proprietÓ allegatoApparati.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegatoApparati(byte[] value) {
        this.allegatoApparati = value;
    }

    /**
     * Recupera il valore della proprietÓ causaliChiusura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCausaliChiusura() {
        return causaliChiusura;
    }

    /**
     * Imposta il valore della proprietÓ causaliChiusura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCausaliChiusura(String value) {
        this.causaliChiusura = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizioneRegolaEsito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneRegolaEsito() {
        return descrizioneRegolaEsito;
    }

    /**
     * Imposta il valore della proprietÓ descrizioneRegolaEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneRegolaEsito(String value) {
        this.descrizioneRegolaEsito = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameFotoSala.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameFotoSala() {
        return fileNameFotoSala;
    }

    /**
     * Imposta il valore della proprietÓ fileNameFotoSala.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameFotoSala(String value) {
        this.fileNameFotoSala = value;
    }

    /**
     * Recupera il valore della proprietÓ fotoSala.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFotoSala() {
        return fotoSala;
    }

    /**
     * Imposta il valore della proprietÓ fotoSala.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFotoSala(byte[] value) {
        this.fotoSala = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameFotoSupporto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameFotoSupporto() {
        return fileNameFotoSupporto;
    }

    /**
     * Imposta il valore della proprietÓ fileNameFotoSupporto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameFotoSupporto(String value) {
        this.fileNameFotoSupporto = value;
    }

    /**
     * Recupera il valore della proprietÓ fotoSupporto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFotoSupporto() {
        return fotoSupporto;
    }

    /**
     * Imposta il valore della proprietÓ fotoSupporto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFotoSupporto(byte[] value) {
        this.fotoSupporto = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameFotoCella.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameFotoCella() {
        return fileNameFotoCella;
    }

    /**
     * Imposta il valore della proprietÓ fileNameFotoCella.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameFotoCella(String value) {
        this.fileNameFotoCella = value;
    }

    /**
     * Recupera il valore della proprietÓ fotoCella.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFotoCella() {
        return fotoCella;
    }

    /**
     * Imposta il valore della proprietÓ fotoCella.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFotoCella(byte[] value) {
        this.fotoCella = value;
    }

    /**
     * Recupera il valore della proprietÓ fileNameFotoLocalita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameFotoLocalita() {
        return fileNameFotoLocalita;
    }

    /**
     * Imposta il valore della proprietÓ fileNameFotoLocalita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameFotoLocalita(String value) {
        this.fileNameFotoLocalita = value;
    }

    /**
     * Recupera il valore della proprietÓ fotoLocalita.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFotoLocalita() {
        return fotoLocalita;
    }

    /**
     * Imposta il valore della proprietÓ fotoLocalita.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFotoLocalita(byte[] value) {
        this.fotoLocalita = value;
    }

}
