package com.rextart.saw.ws.bean;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collaudoBean", propOrder = {
    "codiCollaudo",
    "areaCompetenza",
    "nomeSito",
    "codiceImpianto",
    "clliSito",
    "codiceDBR",
    "regione",
    "provincia",
    "comune",
    "indirizzo",
    "longitudine",
    "latitudine",
    "schedaProgettoRadioEsecutiva",
    "vendor",
    "inputFile"
})


public class CollaudoBean {
	
	/**
	 * 
	 */

	@XmlElement(required = true, nillable = false)
	protected Integer codiCollaudo;
	@XmlElement(required = true, nillable = false)
	protected String areaCompetenza;
	@XmlElement(required = true, nillable = false)
	protected String nomeSito;
	@XmlElement(required = true, nillable = false)
	protected String codiceImpianto;
	@XmlElement(required = true, nillable = false)
	protected String clliSito;
	@XmlElement(required = true, nillable = false)
	protected String codiceDBR;
	@XmlElement(required = true, nillable = false)
	protected String regione;
	@XmlElement(required = true, nillable = false)
	protected String provincia;
	@XmlElement(required = true, nillable = false)
	protected String comune;
	@XmlElement(required = true, nillable = false)
	protected String indirizzo;
	@XmlElement(required = true, nillable = false)
	protected String latitudine;
	@XmlElement(required = true, nillable = false)
	protected String longitudine;
	@XmlElement(required = true, nillable = false)
	protected String schedaProgettoRadioEsecutiva;
	
	protected String vendor;
	@XmlElement(required = true, nillable = false)
	protected ArrayList<FileInputCollaudo> inputFile;
	
	public Integer getCodiCollaudo() {
		return codiCollaudo;
	}
	public void setCodiCollaudo(Integer codiCollaudo) {
		this.codiCollaudo = codiCollaudo;
	}
	public String getAreaCompetenza() {
		return areaCompetenza;
	}
	public void setAreaCompetenza(String areaCompetenza) {
		this.areaCompetenza = areaCompetenza;
	}
	public String getNomeSito() {
		return nomeSito;
	}
	public void setNomeSito(String nomeSito) {
		this.nomeSito = nomeSito;
	}
	public String getCodiceImpianto() {
		return codiceImpianto;
	}
	public void setCodiceImpianto(String codiceImpianto) {
		this.codiceImpianto = codiceImpianto;
	}
	public String getClliSito() {
		return clliSito;
	}
	public void setClliSito(String clliSito) {
		this.clliSito = clliSito;
	}
	public String getRegione() {
		return regione;
	}
	public void setRegione(String regione) {
		this.regione = regione;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getComune() {
		return comune;
	}
	public void setComune(String comune) {
		this.comune = comune;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(String longitudine) {
		this.longitudine = longitudine;
	}
	public String getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(String latitudine) {
		this.latitudine = latitudine;
	}
	public ArrayList<FileInputCollaudo> getInputFile() {
		return inputFile;
	}
	public void setInputFile(ArrayList<FileInputCollaudo> inputFile) {
		this.inputFile = inputFile;
	}
	public String getCodiceDBR() {
		return codiceDBR;
	}
	public void setCodiceDBR(String codiceDBR) {
		this.codiceDBR = codiceDBR;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getSchedaProgettoRadioEsecutiva() {
		return schedaProgettoRadioEsecutiva;
	}
	public void setSchedaProgettoRadioEsecutiva(String schedaProgettoRadioEsecutiva) {
		this.schedaProgettoRadioEsecutiva = schedaProgettoRadioEsecutiva;
	}
	
	
	
}
