package com.rextart.saw.ws.bean;

import java.io.Serializable;

public class ResponseWsCollaudo implements Serializable {

	private static final long serialVersionUID = 765339734133731531L;
	
	private ResponseStatusCode statusCode;
	private String statuMsg;
	
	public ResponseWsCollaudo() {
		setStatusCode(null);
		setStatuMsg(null);
	}
	
	public ResponseWsCollaudo(ResponseStatusCode statusCode) {		
		setStatusCode(statusCode);
		switch (statusCode){		
		case RES_COLLAUDO_200:
			setStatuMsg("Collaudo inserito correttamente");
			break;
		case RES_CODI_COLLAUDO_201:
			setStatuMsg("ID CO non pu� essere null,oppure gi� presente");
			break;
		case RES_DESC_AREA_COMPETENZA_202:
			setStatuMsg("Area di competenza non pu� essere null o Stringa vuota");
			break;
		case RES_DESC_NOME_SITO_203:
			setStatuMsg("Nome Sito non pu� essere null o Stringa vuota");
			break;
		case RES_DESC_CODICE_204:
			setStatuMsg("Codice impianto non pu� essere null o Stringa vuota e di lunghezza =5");
			break;
		case RES_CLLI_SITO_205:
			setStatuMsg("CLLI Sito non pu� essere null o Stringa vuota");
			break;
		case RES_REGIONE_206:
			setStatuMsg("Regione non pu� essere null o Stringa vuota");
			break;
		case RES_PROVINCIA_207:
			setStatuMsg("Provincia non pu� essere null o Stringa vuota");
			break;
		case RES_COMUNE_208:
			setStatuMsg("Comune non pu� essere null o Stringa vuota");
			break;
		case RES_INDIRIZZO_209:
			setStatuMsg("Indirizzo non pu� essere null o Stringa vuota");
			break;
		case RES_LATITUDINE_210:
			setStatuMsg("Latitudine (WGS84) non pu� essere null o Stringa vuota");
			break;
		case RES_LONGITUDINE_211:
			setStatuMsg("Longitudine (WGS84) non pu� essere null o Stringa vuota");
			break;
		case RES_SCHEDA_PR_RADIO_ESECUTIVA_212:
			setStatuMsg("Scheda progetto radio esecutiva non pu� essere null o con formato diverso da aaaa/mm/gg");
			break;
		case RES_FILE_213:
			setStatuMsg("Il File non pu� essere null,il nome del File non pu� essere null o Stringa vuota,il tipo non pu� essere null o diverso da 1,2,3,4");
			break;
		case RES_AREA_COMPETENZA_NON_TROVATA_214:
			setStatuMsg("Area Di Competenza non trovata");
			break;
		case RES_CODI_DBR_215:
			setStatuMsg("Il codice DBR non pu� essere null e deve essere di lunghezza massima 6");
			break;
		case RES_DESC_CODICE_216:
			setStatuMsg("Ultimo carattere del codice Impianto non valido: (Sistema/Banda) non trovati");
			break;
			
		case RES_COLLAUDO_500:
			setStatuMsg("System error");
			break;
		}
	
	}

	public ResponseStatusCode getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(ResponseStatusCode statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatuMsg() {
		return statuMsg;
	}
	public void setStatuMsg(String statuMsg) {
		this.statuMsg = statuMsg;
	}
	
}
