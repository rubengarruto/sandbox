package com.rextart.saw.ws.bean;

public enum ResponseStatusCode {
	
	RES_COLLAUDO_200 ("Collaudo inserito correttamente"),
	RES_CODI_COLLAUDO_201("ID CO non pu� essere null,oppure gi� presente"),
	RES_DESC_AREA_COMPETENZA_202("Area di competenza non pu� essere null o Stringa vuota"),
	RES_DESC_NOME_SITO_203("Nome Sito non pu� essere null o Stringa vuota"),
	RES_DESC_CODICE_204("Codice impianto non pu� essere null o Stringa vuota e di lunghezza =5"),
	RES_CLLI_SITO_205("CLLI Sito non pu� essere null o Stringa vuota"),
	RES_REGIONE_206("Regione non pu� essere null  o Stringa vuota"),
	RES_PROVINCIA_207("Provincia non pu� essere null  o Stringa vuota"),
	RES_COMUNE_208("Comune non pu� essere null o Stringa vuota"),
	RES_INDIRIZZO_209("Indirizzo non pu� essere null o Stringa vuota"),
	RES_LATITUDINE_210("Latitudine (WGS84) non pu� essere null o Stringa vuota"),
	RES_LONGITUDINE_211("Longitudine (WGS84) non pu� essere null o Stringa vuota"),
	RES_SCHEDA_PR_RADIO_ESECUTIVA_212("Scheda progetto radio esecutiva non pu� essere null o con formato diverso da aaaa/mm/gg"),
	RES_FILE_213("Il File non pu� essere null,il nome del File non pu� essere null o Stringa vuota,il tipo non pu� essere null o diverso da 1,2,3,4"),
	RES_AREA_COMPETENZA_NON_TROVATA_214("Area Di Competenza non trovata"),
	RES_CODI_DBR_215("Il codice DBR non pu� essere null e deve essere di lunghezza massima 6"),
	RES_DESC_CODICE_216("Ultimo carattere del codice Impianto non valido: (Sistema/Banda) non trovati"),
	RES_COLLAUDO_500  ("System error");

	private String responseStatusCode;
	
	public String getResponseStatusCode() {
		return responseStatusCode;
	}

	public void setResponseStatusCode(String responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

	private ResponseStatusCode(String responseStatusCode){
		this.responseStatusCode=responseStatusCode;
	}

}
