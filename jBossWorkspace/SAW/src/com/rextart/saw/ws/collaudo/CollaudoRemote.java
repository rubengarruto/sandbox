package com.rextart.saw.ws.collaudo;

import javax.ejb.Remote;
import javax.jws.WebMethod;

import com.rextart.saw.ws.bean.CollaudoBean;
import com.rextart.saw.ws.bean.ResponseWsCollaudo;

@Remote
public interface CollaudoRemote {

	@WebMethod(operationName="addCollaudo")
	public ResponseWsCollaudo  addColludo(CollaudoBean collaudoBean);
	
}
