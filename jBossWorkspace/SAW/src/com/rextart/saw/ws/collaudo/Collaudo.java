package com.rextart.saw.ws.collaudo;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.jws.WebService;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.openkm.sdk4j.bean.Document;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabPoloTecnologico;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.openKM.OpenKmService;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.ws.bean.CollaudoBean;
import com.rextart.saw.ws.bean.FileInputCollaudo;
import com.rextart.saw.ws.bean.ResponseStatusCode;
import com.rextart.saw.ws.bean.ResponseWsCollaudo;

	@WebService(serviceName="CollaudoService")
	
	public class Collaudo extends SpringBeanAutowiringSupport implements CollaudoRemote,Serializable{
		
	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;


		@Autowired
		SessioniService sessioneService;
		@Autowired
		RepositoryMediaService repositoryMediaService;
		@Autowired
		OpenKmService openKmService;
		boolean flagEricson=false;
		boolean flagHuawei=false;
		boolean flagNokia=false;
		ViwSessioni sessione;
 
		@Override
		public ResponseWsCollaudo addColludo(CollaudoBean collaudoBean) {
			try {
				UtilLog.printCallAddCollaudo();
				System.out.println("REQUEST: " + "CODI COLLAUDO: " + collaudoBean.getCodiCollaudo() 
									+"\nAREA COMPETENZA: "+collaudoBean.getAreaCompetenza()
									+"\nNOME SITO: "+collaudoBean.getNomeSito()
									+"\nCODICE IMPIANTO: "+collaudoBean.getCodiceImpianto()
									+"\nCLLI: "+collaudoBean.getClliSito()
									+"\nDBR: "+collaudoBean.getCodiceDBR()
									+"\nREGIONE: "+collaudoBean.getRegione()
									+"\nPROVINCIA: "+collaudoBean.getProvincia()
									+"\nCOMUNE: "+collaudoBean.getComune()
									+"\nINDIRIZZO: "+collaudoBean.getIndirizzo()
									+"\nLAT: "+collaudoBean.getLatitudine()
									+"\nLONG: "+collaudoBean.getLongitudine()
									+"\nDATA PROGETTO RADIO: "+collaudoBean.getSchedaProgettoRadioEsecutiva()
									+"\nVENDOR: "+collaudoBean.getVendor()
					);
				if(collaudoBean.getCodiCollaudo()==null || collaudoBean.getCodiCollaudo().equals(Const.EMPTY_VALUE) ){
					System.out.println("ID CO non pu� essere null");
					return new ResponseWsCollaudo(ResponseStatusCode.RES_CODI_COLLAUDO_201);
				}else{
					sessione= sessioneService.findViwSessioniToClone(collaudoBean.getCodiCollaudo());
				}
				if(sessione!=null && !sessione.getFlgSpeditoPev()){
					return new ResponseWsCollaudo(ResponseStatusCode.RES_CODI_COLLAUDO_201);
					
				}else if(sessione!=null && sessione.getFlgSpeditoPev()){
					/*Chiamo la funzioneper clonare il collaudo */
					Integer resultsClone=sessioneService.clonaCollaudo(sessione.getCodiCollaudo(), sessione.getCodiSessione(), sessione.getCodiAreaComp());
					if(resultsClone==Const.RESULT_CLONE_OK){
						return new ResponseWsCollaudo(ResponseStatusCode.RES_COLLAUDO_200);
					}else{
						return new ResponseWsCollaudo(ResponseStatusCode.RES_COLLAUDO_500);
					}
					
				}else{

					if(collaudoBean.getAreaCompetenza()==null || collaudoBean.getAreaCompetenza().equals(Const.EMPTY_STRING)){
						System.out.println("Area di competenza non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_DESC_AREA_COMPETENZA_202);
					}
					Integer codiAreaComp=sessioneService.getCodiAreaCompetenza(collaudoBean.getAreaCompetenza().toUpperCase().trim());
					if(codiAreaComp==null){
						System.out.println("Area Di Competenza non trovata");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_AREA_COMPETENZA_NON_TROVATA_214);
					}
					if(collaudoBean.getNomeSito()==null || collaudoBean.getNomeSito().equals(Const.EMPTY_STRING)){
						System.out.println("Nome Sito non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_DESC_NOME_SITO_203);
					}
					TabAnagSistemiBande tabAnagSistemiBande;
					if(collaudoBean.getCodiceImpianto()==null || collaudoBean.getCodiceImpianto().length()!=5){
						System.out.println("Codice impianto non pu� essere null o Stringa vuota e di lunghezza =5");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_DESC_CODICE_204);
					}else{
						String  descIdentificativo=Character.toString(collaudoBean.getCodiceImpianto().toUpperCase().trim().charAt(4));
						tabAnagSistemiBande=sessioneService.findBandaSistemaByCodiImpianto(descIdentificativo);
						if(tabAnagSistemiBande==null){
							System.out.println("Ultimo carattere del codice Impianto non valido: (Sistema/Banda) non trovati");
							return new ResponseWsCollaudo(ResponseStatusCode.RES_DESC_CODICE_216);
						}
					}
					if(collaudoBean.getClliSito()==null || collaudoBean.getClliSito().equals(Const.EMPTY_STRING)){
						System.out.println("CLLI Sito non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_CLLI_SITO_205);
					}
					if(collaudoBean.getRegione()==null || collaudoBean.getRegione().equals(Const.EMPTY_STRING)){
						System.out.println("Regione non pu� essere null  o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_REGIONE_206);
					}
					if(collaudoBean.getProvincia()==null || collaudoBean.getProvincia().equals(Const.EMPTY_STRING)){
						System.out.println("Provincia non pu� essere null  o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_PROVINCIA_207);
					}
					if(collaudoBean.getComune()==null || collaudoBean.getComune().equals(Const.EMPTY_STRING)){
						System.out.println("Comune non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_COMUNE_208);
					}
					if(collaudoBean.getIndirizzo()==null || collaudoBean.getIndirizzo().equals(Const.EMPTY_STRING)){
						System.out.println("Indirizzo non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_INDIRIZZO_209);
					}
					if(collaudoBean.getLatitudine()==null || collaudoBean.getLatitudine().equals(Const.EMPTY_STRING)){
						System.out.println("Latitudine (WGS84) non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_LATITUDINE_210);
					}
					if(collaudoBean.getLongitudine()==null || collaudoBean.getLongitudine().equals(Const.EMPTY_STRING)){
						System.out.println("Longitudine (WGS84) non pu� essere null o Stringa vuota");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_LONGITUDINE_211);
					}
					if(collaudoBean.getSchedaProgettoRadioEsecutiva()!=null && !collaudoBean.getSchedaProgettoRadioEsecutiva().equalsIgnoreCase(Const.EMPTY_STRING) ){
						try {
							new Date(collaudoBean.getSchedaProgettoRadioEsecutiva());
							System.out.println("data: " + new Date(collaudoBean.getSchedaProgettoRadioEsecutiva()));
						} catch (Exception e) {
							System.out.println("Scheda progetto radio esecutiva non pu� essere null o con formato diverso da aaaa/mm/gg");
							return new ResponseWsCollaudo(ResponseStatusCode.RES_SCHEDA_PR_RADIO_ESECUTIVA_212);
						}
					}else{
						System.out.println("Scheda progetto radio esecutiva non pu� essere null o con formato diverso da aaaa/mm/gg");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_SCHEDA_PR_RADIO_ESECUTIVA_212);
					}
					if(collaudoBean.getCodiceDBR()!=null && ! collaudoBean.getCodiceDBR().equals(Const.EMPTY_STRING)){
						if(collaudoBean.getCodiceDBR().length()>6){
							System.out.println("Il codice DBR non pu� essere null e deve essere di lunghezza massima 6");
							return new ResponseWsCollaudo(ResponseStatusCode.RES_CODI_DBR_215);
						}
					}else{
						System.out.println("Il codice DBR non pu� essere null e deve essere di lunghezza massima 6");
						return new ResponseWsCollaudo(ResponseStatusCode.RES_CODI_DBR_215);
					}
					String descNou  = sessioneService.getDescGruppoByCLLI(collaudoBean.getClliSito());
					TabGruppi gruppo= new TabGruppi();
					if(descNou!=null && !descNou.equalsIgnoreCase(Const.EMPTY_STRING)){
						gruppo= sessioneService.getGruppoByDescGruppo(descNou);
					}
					boolean flagEricson=false;
					boolean flagHuawei=false;
					boolean flagNokia=false;
					Integer codiVendor= null;
					if(collaudoBean.getVendor()!=null && !collaudoBean.getVendor().equalsIgnoreCase(Const.EMPTY_STRING)){
						codiVendor=sessioneService.getCodiVendor(collaudoBean.getVendor().toUpperCase().trim());

						if(codiVendor!=null){
							switch (codiVendor) {
							case 1:
								flagEricson=true;
								break;
							case 2:
								flagHuawei=true;
								break;
							case 3:
								flagNokia=true;
								break;
							}
						}
					}else{
						String regione =sessioneService.getDescRegioneByNome(collaudoBean.getRegione());
						TabPoloTecnologico tabPoloTecnologico= new TabPoloTecnologico();
						tabPoloTecnologico =sessioneService.findPoloTecnologicoByRegione(regione);
						codiVendor=tabPoloTecnologico.getCodiVendor();
						if(codiVendor!=null){
							switch (codiVendor) {
							case 1:
								flagEricson=true;
								break;
							case 2:
								flagHuawei=true;
								break;
							case 3:
								flagNokia=true;
								break;
							}
						}
					}
					if(collaudoBean.getInputFile()!=null){
						for (int i = 0; i < collaudoBean.getInputFile().size(); i++) {
							if(collaudoBean.getInputFile().get(i)==null)
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);	
							else if(collaudoBean.getInputFile().get(i).getFile()==null)
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							else if(collaudoBean.getInputFile().get(i).getFile().length==0){
								System.out.println("file null");
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							}
							else if(collaudoBean.getInputFile().get(i).getNomeFile()==null){
								System.out.println("Nome file null");
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							}
							else if(collaudoBean.getInputFile().get(i).getNomeFile().length()==0){
								System.out.println("Nome file VUOTO");
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							}
							else if(collaudoBean.getInputFile().get(i).getTipoFile()==null){
								System.out.println("Tipo file null");
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							}
							else if(collaudoBean.getInputFile().get(i).getTipoFile()<=0 || collaudoBean.getInputFile().get(i).getTipoFile()>4){
								System.out.println("TIPO FILE MINORE DI 0 O MAGGIORE DI 4");
								return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
							}
						}
					}
//					else{
//						System.out.println("Almeno un file � obbligatorio");
//						return new ResponseWsCollaudo(ResponseStatusCode.RES_FILE_213);
//					}
					TabSessioni tabSessioni= new TabSessioni();
					if(gruppo!=null)
						tabSessioni=DataConverter.getTabSessioni(collaudoBean,codiAreaComp,tabAnagSistemiBande,flagEricson,flagHuawei,flagNokia,gruppo.getCodiZona());
					else
						tabSessioni=DataConverter.getTabSessioni(collaudoBean,codiAreaComp,tabAnagSistemiBande,flagEricson,flagHuawei,flagNokia,null);

					Object [] sessione=	sessioneService.saveCollaudo(tabSessioni);
					if(collaudoBean.getInputFile()!=null){
						for (FileInputCollaudo file : collaudoBean.getInputFile()) {
							Document doc= openKmService.uploadDocumentiCollaudo((Integer) sessione[2], file.getNomeFile(), file.getFile());
							TabRepositoryDocument tabRepDoc= new TabRepositoryDocument();
							tabRepDoc.setCodiSessione((Integer) sessione[2]);
							tabRepDoc.setDescNome(file.getNomeFile());
							tabRepDoc.setDescPath(doc.getPath());
							tabRepDoc.setTimeCreate(new Timestamp(System.currentTimeMillis()));
							tabRepDoc.setTipoFile(file.getTipoFile());
							repositoryMediaService.saveRepositoryDocument(tabRepDoc);
						}
					}

				}
				
			} catch (Exception e) {
				return new ResponseWsCollaudo(ResponseStatusCode.RES_COLLAUDO_500);
			}
			UtilLog.printCambioStatoSessione(UtilLog.COLLAUDO_ACQUISITO);
			return new ResponseWsCollaudo(ResponseStatusCode.RES_COLLAUDO_200);
		}
		private void StoreFile(FileInputCollaudo f) throws Exception {
			FileUtils.writeByteArrayToFile(new File("C:/ProvaServiziFile/"+f.getNomeFile()), f.getFile());
		}
		
	
		
		
	}

