
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rextart.saw.ws.dyn package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rextart.saw.ws.dyn
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetScheda }
     * 
     */
    public GetScheda createGetScheda() {
        return new GetScheda();
    }

    /**
     * Create an instance of {@link GetSchedaResponse }
     * 
     */
    public GetSchedaResponse createGetSchedaResponse() {
        return new GetSchedaResponse();
    }

    /**
     * Create an instance of {@link OutputScheda }
     * 
     */
    public OutputScheda createOutputScheda() {
        return new OutputScheda();
    }

    /**
     * Create an instance of {@link GetStatoSchedaRischi }
     * 
     */
    public GetStatoSchedaRischi createGetStatoSchedaRischi() {
        return new GetStatoSchedaRischi();
    }

    /**
     * Create an instance of {@link GetStatoSchedaRischiResponse }
     * 
     */
    public GetStatoSchedaRischiResponse createGetStatoSchedaRischiResponse() {
        return new GetStatoSchedaRischiResponse();
    }

    /**
     * Create an instance of {@link OutputStatoScheda }
     * 
     */
    public OutputStatoScheda createOutputStatoScheda() {
        return new OutputStatoScheda();
    }

    /**
     * Create an instance of {@link ArrayOfTns1Allegato }
     * 
     */
    public ArrayOfTns1Allegato createArrayOfTns1Allegato() {
        return new ArrayOfTns1Allegato();
    }

    /**
     * Create an instance of {@link Allegato }
     * 
     */
    public Allegato createAllegato() {
        return new Allegato();
    }

}
