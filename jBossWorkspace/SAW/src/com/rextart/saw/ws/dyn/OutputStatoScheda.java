
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per OutputStatoScheda complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="OutputStatoScheda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiceEsito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataCreazioneScheda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msgEsito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="versione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutputStatoScheda", namespace = "http://bean.di.telecomitalia.it", propOrder = {
    "codiceEsito",
    "dataCreazioneScheda",
    "msgEsito",
    "versione"
})
public class OutputStatoScheda {

    @XmlElement(required = true, nillable = true)
    protected String codiceEsito;
    @XmlElement(required = true, nillable = true)
    protected String dataCreazioneScheda;
    @XmlElement(required = true, nillable = true)
    protected String msgEsito;
    @XmlElement(required = true, nillable = true)
    protected String versione;

    /**
     * Recupera il valore della proprietÓ codiceEsito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceEsito() {
        return codiceEsito;
    }

    /**
     * Imposta il valore della proprietÓ codiceEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceEsito(String value) {
        this.codiceEsito = value;
    }

    /**
     * Recupera il valore della proprietÓ dataCreazioneScheda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCreazioneScheda() {
        return dataCreazioneScheda;
    }

    /**
     * Imposta il valore della proprietÓ dataCreazioneScheda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCreazioneScheda(String value) {
        this.dataCreazioneScheda = value;
    }

    /**
     * Recupera il valore della proprietÓ msgEsito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgEsito() {
        return msgEsito;
    }

    /**
     * Imposta il valore della proprietÓ msgEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgEsito(String value) {
        this.msgEsito = value;
    }

    /**
     * Recupera il valore della proprietÓ versione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersione() {
        return versione;
    }

    /**
     * Imposta il valore della proprietÓ versione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersione(String value) {
        this.versione = value;
    }

}
