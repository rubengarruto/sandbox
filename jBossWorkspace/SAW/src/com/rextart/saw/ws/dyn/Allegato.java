
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Allegato complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Allegato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegato" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="nomeAllegato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Allegato", namespace = "http://bean.di.telecomitalia.it", propOrder = {
    "allegato",
    "nomeAllegato"
})
public class Allegato {

    @XmlElement(required = true, nillable = true)
    protected byte[] allegato;
    @XmlElement(required = true, nillable = true)
    protected String nomeAllegato;

    /**
     * Recupera il valore della proprietÓ allegato.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegato() {
        return allegato;
    }

    /**
     * Imposta il valore della proprietÓ allegato.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegato(byte[] value) {
        this.allegato = value;
    }

    /**
     * Recupera il valore della proprietÓ nomeAllegato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeAllegato() {
        return nomeAllegato;
    }

    /**
     * Imposta il valore della proprietÓ nomeAllegato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeAllegato(String value) {
        this.nomeAllegato = value;
    }

}
