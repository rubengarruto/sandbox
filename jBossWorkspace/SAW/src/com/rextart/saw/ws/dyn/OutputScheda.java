
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per OutputScheda complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="OutputScheda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessibilita" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="allegati" type="{http://service.di.telecomitalia.it}ArrayOf_tns1_Allegato"/>
 *         &lt;element name="codiceEsito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codiceMazzoChiavi" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataCreazioneSchedaRischi" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="disagiato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fasciaIntervento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="itinerario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="msgEsito" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="noteAccessibilita" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="noteAccesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="regime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="schedaRischi" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="tipoAccesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="versione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutputScheda", namespace = "http://bean.di.telecomitalia.it", propOrder = {
    "accessibilita",
    "allegati",
    "codiceEsito",
    "codiceMazzoChiavi",
    "dataCreazioneSchedaRischi",
    "disagiato",
    "fasciaIntervento",
    "itinerario",
    "msgEsito",
    "noteAccessibilita",
    "noteAccesso",
    "regime",
    "schedaRischi",
    "tipoAccesso",
    "versione"
})
public class OutputScheda {

    @XmlElement(required = true, nillable = true)
    protected String accessibilita;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfTns1Allegato allegati;
    @XmlElement(required = true, nillable = true)
    protected String codiceEsito;
    @XmlElement(required = true, nillable = true)
    protected String codiceMazzoChiavi;
    @XmlElement(required = true, nillable = true)
    protected String dataCreazioneSchedaRischi;
    @XmlElement(required = true, nillable = true)
    protected String disagiato;
    @XmlElement(required = true, nillable = true)
    protected String fasciaIntervento;
    @XmlElement(required = true, nillable = true)
    protected String itinerario;
    @XmlElement(required = true, nillable = true)
    protected String msgEsito;
    @XmlElement(required = true, nillable = true)
    protected String noteAccessibilita;
    @XmlElement(required = true, nillable = true)
    protected String noteAccesso;
    @XmlElement(required = true, nillable = true)
    protected String regime;
    @XmlElement(required = true, nillable = true)
    protected byte[] schedaRischi;
    @XmlElement(required = true, nillable = true)
    protected String tipoAccesso;
    @XmlElement(required = true, nillable = true)
    protected String versione;

    /**
     * Recupera il valore della proprietÓ accessibilita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessibilita() {
        return accessibilita;
    }

    /**
     * Imposta il valore della proprietÓ accessibilita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessibilita(String value) {
        this.accessibilita = value;
    }

    /**
     * Recupera il valore della proprietÓ allegati.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTns1Allegato }
     *     
     */
    public ArrayOfTns1Allegato getAllegati() {
        return allegati;
    }

    /**
     * Imposta il valore della proprietÓ allegati.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTns1Allegato }
     *     
     */
    public void setAllegati(ArrayOfTns1Allegato value) {
        this.allegati = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceEsito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceEsito() {
        return codiceEsito;
    }

    /**
     * Imposta il valore della proprietÓ codiceEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceEsito(String value) {
        this.codiceEsito = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceMazzoChiavi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceMazzoChiavi() {
        return codiceMazzoChiavi;
    }

    /**
     * Imposta il valore della proprietÓ codiceMazzoChiavi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceMazzoChiavi(String value) {
        this.codiceMazzoChiavi = value;
    }

    /**
     * Recupera il valore della proprietÓ dataCreazioneSchedaRischi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCreazioneSchedaRischi() {
        return dataCreazioneSchedaRischi;
    }

    /**
     * Imposta il valore della proprietÓ dataCreazioneSchedaRischi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCreazioneSchedaRischi(String value) {
        this.dataCreazioneSchedaRischi = value;
    }

    /**
     * Recupera il valore della proprietÓ disagiato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisagiato() {
        return disagiato;
    }

    /**
     * Imposta il valore della proprietÓ disagiato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisagiato(String value) {
        this.disagiato = value;
    }

    /**
     * Recupera il valore della proprietÓ fasciaIntervento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFasciaIntervento() {
        return fasciaIntervento;
    }

    /**
     * Imposta il valore della proprietÓ fasciaIntervento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFasciaIntervento(String value) {
        this.fasciaIntervento = value;
    }

    /**
     * Recupera il valore della proprietÓ itinerario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItinerario() {
        return itinerario;
    }

    /**
     * Imposta il valore della proprietÓ itinerario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItinerario(String value) {
        this.itinerario = value;
    }

    /**
     * Recupera il valore della proprietÓ msgEsito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgEsito() {
        return msgEsito;
    }

    /**
     * Imposta il valore della proprietÓ msgEsito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgEsito(String value) {
        this.msgEsito = value;
    }

    /**
     * Recupera il valore della proprietÓ noteAccessibilita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoteAccessibilita() {
        return noteAccessibilita;
    }

    /**
     * Imposta il valore della proprietÓ noteAccessibilita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoteAccessibilita(String value) {
        this.noteAccessibilita = value;
    }

    /**
     * Recupera il valore della proprietÓ noteAccesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoteAccesso() {
        return noteAccesso;
    }

    /**
     * Imposta il valore della proprietÓ noteAccesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoteAccesso(String value) {
        this.noteAccesso = value;
    }

    /**
     * Recupera il valore della proprietÓ regime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegime() {
        return regime;
    }

    /**
     * Imposta il valore della proprietÓ regime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegime(String value) {
        this.regime = value;
    }

    /**
     * Recupera il valore della proprietÓ schedaRischi.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSchedaRischi() {
        return schedaRischi;
    }

    /**
     * Imposta il valore della proprietÓ schedaRischi.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSchedaRischi(byte[] value) {
        this.schedaRischi = value;
    }

    /**
     * Recupera il valore della proprietÓ tipoAccesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAccesso() {
        return tipoAccesso;
    }

    /**
     * Imposta il valore della proprietÓ tipoAccesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAccesso(String value) {
        this.tipoAccesso = value;
    }

    /**
     * Recupera il valore della proprietÓ versione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersione() {
        return versione;
    }

    /**
     * Imposta il valore della proprietÓ versione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersione(String value) {
        this.versione = value;
    }

}
