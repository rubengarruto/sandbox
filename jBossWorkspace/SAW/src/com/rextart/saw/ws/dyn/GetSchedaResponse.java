
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getSchedaReturn" type="{http://bean.di.telecomitalia.it}OutputScheda"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSchedaReturn"
})
@XmlRootElement(name = "getSchedaResponse")
public class GetSchedaResponse {

    @XmlElement(required = true)
    protected OutputScheda getSchedaReturn;

    /**
     * Recupera il valore della proprietÓ getSchedaReturn.
     * 
     * @return
     *     possible object is
     *     {@link OutputScheda }
     *     
     */
    public OutputScheda getGetSchedaReturn() {
        return getSchedaReturn;
    }

    /**
     * Imposta il valore della proprietÓ getSchedaReturn.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputScheda }
     *     
     */
    public void setGetSchedaReturn(OutputScheda value) {
        this.getSchedaReturn = value;
    }

}
