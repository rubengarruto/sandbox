
package com.rextart.saw.ws.dyn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiceDbr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codiceDbr"
})
@XmlRootElement(name = "getStatoSchedaRischi")
public class GetStatoSchedaRischi {

    @XmlElement(required = true)
    protected String codiceDbr;

    /**
     * Recupera il valore della proprietÓ codiceDbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceDbr() {
        return codiceDbr;
    }

    /**
     * Imposta il valore della proprietÓ codiceDbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceDbr(String value) {
        this.codiceDbr = value;
    }

}
