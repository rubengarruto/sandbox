package com.rextart.saw.report.bean;

public class RiferimentiBean {

	private String descNomeTim;
	private String descNomeDitta;
	private String telefonoTim;
	private String telefonoDitta;
	private String descDitta;
	public String getDescNomeTim() {
		return descNomeTim;
	}
	public void setDescNomeTim(String descNomeTim) {
		this.descNomeTim = descNomeTim;
	}
	public String getDescNomeDitta() {
		return descNomeDitta;
	}
	public void setDescNomeDitta(String descNomeDitta) {
		this.descNomeDitta = descNomeDitta;
	}
	public String getTelefonoTim() {
		return telefonoTim;
	}
	public void setTelefonoTim(String telefonoTim) {
		this.telefonoTim = telefonoTim;
	}
	public String getTelefonoDitta() {
		return telefonoDitta;
	}
	public void setTelefonoDitta(String telefonoDitta) {
		this.telefonoDitta = telefonoDitta;
	}
	public String getDescDitta() {
		return descDitta;
	}
	public void setDescDitta(String descDitta) {
		this.descDitta = descDitta;
	}
	
	
}
