package com.rextart.saw.report.bean;

public class AllarmiEsterni2GBean {

	private String nr;
	private String severita;
	private String sloganAllarme;
	private String esito;
	private String configurazione;
	private String competenzaAllarmi;
	
	
	public String getNr() {
		return nr;
	}
	public void setNr(String nr) {
		this.nr = nr;
	}
	public String getSeverita() {
		return severita;
	}
	public void setSeverita(String severita) {
		this.severita = severita;
	}
	public String getSloganAllarme() {
		return sloganAllarme;
	}
	public void setSloganAllarme(String sloganAllarme) {
		this.sloganAllarme = sloganAllarme;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getConfigurazione() {
		return configurazione;
	}
	public void setConfigurazione(String configurazione) {
		this.configurazione = configurazione;
	}
	public String getCompetenzaAllarmi() {
		return competenzaAllarmi;
	}
	public void setCompetenzaAllarmi(String competenzaAllarmi) {
		this.competenzaAllarmi = competenzaAllarmi;
	}
	
	
	
	
	
	
	
}
