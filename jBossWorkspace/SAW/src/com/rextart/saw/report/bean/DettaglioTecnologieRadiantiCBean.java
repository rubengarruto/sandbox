package com.rextart.saw.report.bean;

public class DettaglioTecnologieRadiantiCBean {
	
	private String descCaratteristica;
	private String sharingRF;
	private String retInCascata;
	private String mimo4;
	private String master;
	private String serieRetCella1;
	private String checkCella1;
	private String serieRetCella2;
	private String checkCella2;
	private String serieRetCella3;
	private String checkCella3;
	private String serieRetCella4;
	private String checkCella4;
	private String serieRetCella5;
	private String checkCella5;
	private String serieRetCella6;
	private String checkCella6;
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getSharingRF() {
		return sharingRF;
	}
	public void setSharingRF(String sharingRF) {
		this.sharingRF = sharingRF;
	}
	public String getRetInCascata() {
		return retInCascata;
	}
	public void setRetInCascata(String retInCascata) {
		this.retInCascata = retInCascata;
	}
	public String getMimo4() {
		return mimo4;
	}
	public void setMimo4(String mimo4) {
		this.mimo4 = mimo4;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getSerieRetCella1() {
		return serieRetCella1;
	}
	public void setSerieRetCella1(String serieRetCella1) {
		this.serieRetCella1 = serieRetCella1;
	}
	public String getCheckCella1() {
		return checkCella1;
	}
	public void setCheckCella1(String checkCella1) {
		this.checkCella1 = checkCella1;
	}
	public String getSerieRetCella2() {
		return serieRetCella2;
	}
	public void setSerieRetCella2(String serieRetCella2) {
		this.serieRetCella2 = serieRetCella2;
	}
	public String getCheckCella2() {
		return checkCella2;
	}
	public void setCheckCella2(String checkCella2) {
		this.checkCella2 = checkCella2;
	}
	public String getSerieRetCella3() {
		return serieRetCella3;
	}
	public void setSerieRetCella3(String serieRetCella3) {
		this.serieRetCella3 = serieRetCella3;
	}
	public String getCheckCella3() {
		return checkCella3;
	}
	public void setCheckCella3(String checkCella3) {
		this.checkCella3 = checkCella3;
	}
	public String getSerieRetCella4() {
		return serieRetCella4;
	}
	public void setSerieRetCella4(String serieRetCella4) {
		this.serieRetCella4 = serieRetCella4;
	}
	public String getCheckCella4() {
		return checkCella4;
	}
	public void setCheckCella4(String checkCella4) {
		this.checkCella4 = checkCella4;
	}
	public String getSerieRetCella5() {
		return serieRetCella5;
	}
	public void setSerieRetCella5(String serieRetCella5) {
		this.serieRetCella5 = serieRetCella5;
	}
	public String getCheckCella5() {
		return checkCella5;
	}
	public void setCheckCella5(String checkCella5) {
		this.checkCella5 = checkCella5;
	}
	public String getSerieRetCella6() {
		return serieRetCella6;
	}
	public void setSerieRetCella6(String serieRetCella6) {
		this.serieRetCella6 = serieRetCella6;
	}
	public String getCheckCella6() {
		return checkCella6;
	}
	public void setCheckCella6(String checkCella6) {
		this.checkCella6 = checkCella6;
	}

}
