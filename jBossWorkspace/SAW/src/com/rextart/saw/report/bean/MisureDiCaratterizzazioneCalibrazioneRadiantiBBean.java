package com.rextart.saw.report.bean;

public class MisureDiCaratterizzazioneCalibrazioneRadiantiBBean {

	private String descCaratteristica;
	private String descElemento;
	private String flgCheck;
	private String descLabel;
	private Integer codiElemento;
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getFlgCheck() {
		return flgCheck;
	}
	public void setFlgCheck(String flgCheck) {
		this.flgCheck = flgCheck;
	}
	public String getDescLabel() {
		return descLabel;
	}
	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}
		public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	
	
	
}
