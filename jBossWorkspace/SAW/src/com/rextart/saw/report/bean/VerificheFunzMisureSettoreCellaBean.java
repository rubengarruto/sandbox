package com.rextart.saw.report.bean;

public class VerificheFunzMisureSettoreCellaBean {
	
	private String descCaratteristica;
	private String descElemento;
	private String descSettore1Cella1;
	private String descSettore2Cella1;
	private String descSettore3Cella1;
	private String descSettore4Cella1;
	private String descSettore5Cella1;
	private String descSettore6Cella1;
	private String descSettore1Cella2;
	private String descSettore2Cella2;
	private String descSettore3Cella2;
	private String descSettore4Cella2;
	private String descSettore5Cella2;
	private String descSettore6Cella2;
	private String descSettore1Cella3;
	private String descSettore2Cella3;
	private String descSettore3Cella3;
	private String descSettore4Cella3;
	private String descSettore5Cella3;
	private String descSettore6Cella3;
	
	private String descNote;

	public String getDescCaratteristica() {
		return descCaratteristica;
	}

	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}

	public String getDescElemento() {
		return descElemento;
	}

	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescSettore1Cella1() {
		return descSettore1Cella1;
	}

	public void setDescSettore1Cella1(String descSettore1Cella1) {
		this.descSettore1Cella1 = descSettore1Cella1;
	}

	public String getDescSettore2Cella1() {
		return descSettore2Cella1;
	}

	public void setDescSettore2Cella1(String descSettore2Cella1) {
		this.descSettore2Cella1 = descSettore2Cella1;
	}

	public String getDescSettore3Cella1() {
		return descSettore3Cella1;
	}

	public void setDescSettore3Cella1(String descSettore3Cella1) {
		this.descSettore3Cella1 = descSettore3Cella1;
	}

	public String getDescSettore4Cella1() {
		return descSettore4Cella1;
	}

	public void setDescSettore4Cella1(String descSettore4Cella1) {
		this.descSettore4Cella1 = descSettore4Cella1;
	}

	public String getDescSettore5Cella1() {
		return descSettore5Cella1;
	}

	public void setDescSettore5Cella1(String descSettore5Cella1) {
		this.descSettore5Cella1 = descSettore5Cella1;
	}

	public String getDescSettore6Cella1() {
		return descSettore6Cella1;
	}

	public void setDescSettore6Cella1(String descSettore6Cella1) {
		this.descSettore6Cella1 = descSettore6Cella1;
	}

	public String getDescSettore1Cella2() {
		return descSettore1Cella2;
	}

	public void setDescSettore1Cella2(String descSettore1Cella2) {
		this.descSettore1Cella2 = descSettore1Cella2;
	}

	public String getDescSettore2Cella2() {
		return descSettore2Cella2;
	}

	public void setDescSettore2Cella2(String descSettore2Cella2) {
		this.descSettore2Cella2 = descSettore2Cella2;
	}

	public String getDescSettore3Cella2() {
		return descSettore3Cella2;
	}

	public void setDescSettore3Cella2(String descSettore3Cella2) {
		this.descSettore3Cella2 = descSettore3Cella2;
	}

	public String getDescSettore4Cella2() {
		return descSettore4Cella2;
	}

	public void setDescSettore4Cella2(String descSettore4Cella2) {
		this.descSettore4Cella2 = descSettore4Cella2;
	}

	public String getDescSettore5Cella2() {
		return descSettore5Cella2;
	}

	public void setDescSettore5Cella2(String descSettore5Cella2) {
		this.descSettore5Cella2 = descSettore5Cella2;
	}

	public String getDescSettore6Cella2() {
		return descSettore6Cella2;
	}

	public void setDescSettore6Cella2(String descSettore6Cella2) {
		this.descSettore6Cella2 = descSettore6Cella2;
	}

	public String getDescSettore1Cella3() {
		return descSettore1Cella3;
	}

	public void setDescSettore1Cella3(String descSettore1Cella3) {
		this.descSettore1Cella3 = descSettore1Cella3;
	}

	public String getDescSettore2Cella3() {
		return descSettore2Cella3;
	}

	public void setDescSettore2Cella3(String descSettore2Cella3) {
		this.descSettore2Cella3 = descSettore2Cella3;
	}

	public String getDescSettore3Cella3() {
		return descSettore3Cella3;
	}

	public void setDescSettore3Cella3(String descSettore3Cella3) {
		this.descSettore3Cella3 = descSettore3Cella3;
	}

	public String getDescSettore4Cella3() {
		return descSettore4Cella3;
	}

	public void setDescSettore4Cella3(String descSettore4Cella3) {
		this.descSettore4Cella3 = descSettore4Cella3;
	}

	public String getDescSettore5Cella3() {
		return descSettore5Cella3;
	}

	public void setDescSettore5Cella3(String descSettore5Cella3) {
		this.descSettore5Cella3 = descSettore5Cella3;
	}

	public String getDescSettore6Cella3() {
		return descSettore6Cella3;
	}

	public void setDescSettore6Cella3(String descSettore6Cella3) {
		this.descSettore6Cella3 = descSettore6Cella3;
	}

	public String getDescNote() {
		return descNote;
	}

	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
	

	
}
