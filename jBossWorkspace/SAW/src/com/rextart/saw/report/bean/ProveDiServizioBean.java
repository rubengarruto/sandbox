package com.rextart.saw.report.bean;

public class ProveDiServizioBean {
	
	private String sistema;
	private String nr;
	private String descrizioneTest;
	private String esito;
	private String note;
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getNr() {
		return nr;
	}
	public void setNr(String nr) {
		this.nr = nr;
	}
	public String getDescrizioneTest() {
		return descrizioneTest;
	}
	public void setDescrizioneTest(String descrizioneTest) {
		this.descrizioneTest = descrizioneTest;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	

}
