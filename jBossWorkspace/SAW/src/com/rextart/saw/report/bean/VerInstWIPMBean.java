package com.rextart.saw.report.bean;

public class VerInstWIPMBean {

	private String descCaratteristica;
	private String valoreMisurato;
	private String esito;
	private String note;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getValoreMisurato() {
		return valoreMisurato;
	}
	public void setValoreMisurato(String valoreMisurato) {
		this.valoreMisurato = valoreMisurato;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
	
	
}
