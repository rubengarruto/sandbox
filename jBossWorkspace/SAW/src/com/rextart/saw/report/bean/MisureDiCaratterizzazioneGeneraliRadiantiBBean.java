package com.rextart.saw.report.bean;

public class MisureDiCaratterizzazioneGeneraliRadiantiBBean {
	
	private String flgCheck;
	private String via1Cella1;
	private String via2Cella1;
	private String via3Cella1;
	private String via4Cella1;
	private String conforme1;
	private String via1Cella2;
	private String via2Cella2;
	private String via3Cella2;
	private String via4Cella2;
	private String conforme2;
	private String via1Cella3;
	private String via2Cella3;
	private String via3Cella3;
	private String via4Cella3;
	private String conforme3;
	private String via1Cella4;
	private String via2Cella4;
	private String via3Cella4;
	private String via4Cella4;
	private String conforme4;
	private String via1Cella5;
	private String via2Cella5;
	private String via3Cella5;
	private String via4Cella5;
	private String conforme5;
	private String via1Cella6;
	private String via2Cella6;
	private String via3Cella6;
	private String via4Cella6;
	private String conforme6;
	private String descElemento;
	private String descCaratteristica;
	private Integer codiElemento;
	private String descLabel;
	public String getDescLabel() {
		return descLabel;
	}
	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}
	public String getFlgCheck() {
		return flgCheck;
	}
	public void setFlgCheck(String flgCheck) {
		this.flgCheck = flgCheck;
	}
	public String getVia1Cella1() {
		return via1Cella1;
	}
	public void setVia1Cella1(String via1Cella1) {
		this.via1Cella1 = via1Cella1;
	}
	public String getVia2Cella1() {
		return via2Cella1;
	}
	public void setVia2Cella1(String via2Cella1) {
		this.via2Cella1 = via2Cella1;
	}
	public String getVia3Cella1() {
		return via3Cella1;
	}
	public void setVia3Cella1(String via3Cella1) {
		this.via3Cella1 = via3Cella1;
	}
	public String getVia4Cella1() {
		return via4Cella1;
	}
	public void setVia4Cella1(String via4Cella1) {
		this.via4Cella1 = via4Cella1;
	}
	public String getConforme1() {
		return conforme1;
	}
	public void setConforme1(String conforme1) {
		this.conforme1 = conforme1;
	}
	public String getVia1Cella2() {
		return via1Cella2;
	}
	public void setVia1Cella2(String via1Cella2) {
		this.via1Cella2 = via1Cella2;
	}
	public String getVia2Cella2() {
		return via2Cella2;
	}
	public void setVia2Cella2(String via2Cella2) {
		this.via2Cella2 = via2Cella2;
	}
	public String getVia3Cella2() {
		return via3Cella2;
	}
	public void setVia3Cella2(String via3Cella2) {
		this.via3Cella2 = via3Cella2;
	}
	public String getVia4Cella2() {
		return via4Cella2;
	}
	public void setVia4Cella2(String via4Cella2) {
		this.via4Cella2 = via4Cella2;
	}
	public String getConforme2() {
		return conforme2;
	}
	public void setConforme2(String conforme2) {
		this.conforme2 = conforme2;
	}
	public String getVia1Cella3() {
		return via1Cella3;
	}
	public void setVia1Cella3(String via1Cella3) {
		this.via1Cella3 = via1Cella3;
	}
	public String getVia2Cella3() {
		return via2Cella3;
	}
	public void setVia2Cella3(String via2Cella3) {
		this.via2Cella3 = via2Cella3;
	}
	public String getVia3Cella3() {
		return via3Cella3;
	}
	public void setVia3Cella3(String via3Cella3) {
		this.via3Cella3 = via3Cella3;
	}
	public String getVia4Cella3() {
		return via4Cella3;
	}
	public void setVia4Cella3(String via4Cella3) {
		this.via4Cella3 = via4Cella3;
	}
	public String getConforme3() {
		return conforme3;
	}
	public void setConforme3(String vonforme3) {
		this.conforme3 = vonforme3;
	}
	public String getVia1Cella4() {
		return via1Cella4;
	}
	public void setVia1Cella4(String via1Cella4) {
		this.via1Cella4 = via1Cella4;
	}
	public String getVia2Cella4() {
		return via2Cella4;
	}
	public void setVia2Cella4(String via2Cella4) {
		this.via2Cella4 = via2Cella4;
	}
	public String getVia3Cella4() {
		return via3Cella4;
	}
	public void setVia3Cella4(String via3Cella4) {
		this.via3Cella4 = via3Cella4;
	}
	public String getVia4Cella4() {
		return via4Cella4;
	}
	public void setVia4Cella4(String via4Cella4) {
		this.via4Cella4 = via4Cella4;
	}
	public String getConforme4() {
		return conforme4;
	}
	public void setConforme4(String conforme4) {
		this.conforme4 = conforme4;
	}
	public String getVia1Cella5() {
		return via1Cella5;
	}
	public void setVia1Cella5(String via1Cella5) {
		this.via1Cella5 = via1Cella5;
	}
	public String getVia2Cella5() {
		return via2Cella5;
	}
	public void setVia2Cella5(String via2Cella5) {
		this.via2Cella5 = via2Cella5;
	}
	public String getVia3Cella5() {
		return via3Cella5;
	}
	public void setVia3Cella5(String via3Cella5) {
		this.via3Cella5 = via3Cella5;
	}
	public String getVia4Cella5() {
		return via4Cella5;
	}
	public void setVia4Cella5(String via4Cella5) {
		this.via4Cella5 = via4Cella5;
	}
	public String getConforme5() {
		return conforme5;
	}
	public void setConforme5(String conforme5) {
		this.conforme5 = conforme5;
	}
	public String getVia1Cella6() {
		return via1Cella6;
	}
	public void setVia1Cella6(String via1Cella6) {
		this.via1Cella6 = via1Cella6;
	}
	public String getVia2Cella6() {
		return via2Cella6;
	}
	public void setVia2Cella6(String via2Cella6) {
		this.via2Cella6 = via2Cella6;
	}
	public String getVia3Cella6() {
		return via3Cella6;
	}
	public void setVia3Cella6(String via3Cella6) {
		this.via3Cella6 = via3Cella6;
	}
	public String getVia4Cella6() {
		return via4Cella6;
	}
	public void setVia4Cella6(String via4Cella6) {
		this.via4Cella6 = via4Cella6;
	}
	public String getConforme6() {
		return conforme6;
	}
	public void setConforme6(String conforme6) {
		this.conforme6 = conforme6;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}
	
	
	
	
}
