package com.rextart.saw.report.bean;

public class VerificheFunzMisureCelleDoppiaLabelBean {
	
	private String descCaratteristica;
	private String descElemento;
	private String descCella1Label1;
	private String descCella2Label1;
	private String descCella3Label1;
	private String descCella4Label1;
	private String descCella5Label1;
	private String descCella6Label1;
	private String descCella1Label2;
	private String descCella2Label2;
	private String descCella3Label2;
	private String descCella4Label2;
	private String descCella5Label2;
	private String descCella6Label2;
	private String descNote;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	public String getDescCella1Label1() {
		return descCella1Label1;
	}
	public void setDescCella1Label1(String descCella1Label1) {
		this.descCella1Label1 = descCella1Label1;
	}
	public String getDescCella2Label1() {
		return descCella2Label1;
	}
	public void setDescCella2Label1(String descCella2Label1) {
		this.descCella2Label1 = descCella2Label1;
	}
	public String getDescCella3Label1() {
		return descCella3Label1;
	}
	public void setDescCella3Label1(String descCella3Label1) {
		this.descCella3Label1 = descCella3Label1;
	}
	public String getDescCella4Label1() {
		return descCella4Label1;
	}
	public void setDescCella4Label1(String descCella4Label1) {
		this.descCella4Label1 = descCella4Label1;
	}
	public String getDescCella5Label1() {
		return descCella5Label1;
	}
	public void setDescCella5Label1(String descCella5Label1) {
		this.descCella5Label1 = descCella5Label1;
	}
	public String getDescCella6Label1() {
		return descCella6Label1;
	}
	public void setDescCella6Label1(String descCella6Label1) {
		this.descCella6Label1 = descCella6Label1;
	}
	public String getDescCella1Label2() {
		return descCella1Label2;
	}
	public void setDescCella1Label2(String descCella1Label2) {
		this.descCella1Label2 = descCella1Label2;
	}
	public String getDescCella2Label2() {
		return descCella2Label2;
	}
	public void setDescCella2Label2(String descCella2Label2) {
		this.descCella2Label2 = descCella2Label2;
	}
	public String getDescCella3Label2() {
		return descCella3Label2;
	}
	public void setDescCella3Label2(String descCella3Label2) {
		this.descCella3Label2 = descCella3Label2;
	}
	public String getDescCella4Label2() {
		return descCella4Label2;
	}
	public void setDescCella4Label2(String descCella4Label2) {
		this.descCella4Label2 = descCella4Label2;
	}
	public String getDescCella5Label2() {
		return descCella5Label2;
	}
	public void setDescCella5Label2(String descCella5Label2) {
		this.descCella5Label2 = descCella5Label2;
	}
	public String getDescCella6Label2() {
		return descCella6Label2;
	}
	public void setDescCella6Label2(String descCella6Label2) {
		this.descCella6Label2 = descCella6Label2;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
	
	
	

	
}
