package com.rextart.saw.report.bean;

public class DatiGeneraliTabletRadiantiABean {
	private String descCaratt1;
	private String field1;
	private String descCaratt2;
	private String field2;
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getDescCaratt1() {
		return descCaratt1;
	}
	public void setDescCaratt1(String descCaratt1) {
		this.descCaratt1 = descCaratt1;
	}
	public String getDescCaratt2() {
		return descCaratt2;
	}
	public void setDescCaratt2(String descCaratt2) {
		this.descCaratt2 = descCaratt2;
	}
	
	
}
