package com.rextart.saw.report.bean;

public class VerificheFunzMisureCelleBean {
	
	private String descCaratteristica;
	private String descElemento;
	private String descCella1;
	private String descCella2;
	private String descCella3;
	private String descCella4;
	private String descCella5;
	private String descCella6;
	private String descNote;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getDescCella1() {
		return descCella1;
	}
	public void setDescCella1(String descCella1) {
		this.descCella1 = descCella1;
	}
	public String getDescCella2() {
		return descCella2;
	}
	public void setDescCella2(String descCella2) {
		this.descCella2 = descCella2;
	}
	public String getDescCella3() {
		return descCella3;
	}
	public void setDescCella3(String descCella3) {
		this.descCella3 = descCella3;
	}
	public String getDescCella4() {
		return descCella4;
	}
	public void setDescCella4(String descCella4) {
		this.descCella4 = descCella4;
	}
	public String getDescCella5() {
		return descCella5;
	}
	public void setDescCella5(String descCella5) {
		this.descCella5 = descCella5;
	}
	public String getDescCella6() {
		return descCella6;
	}
	public void setDescCella6(String descCella6) {
		this.descCella6 = descCella6;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
	
	
	

	
}
