package com.rextart.saw.report.bean;

public class ConfigurazioneCelleRadiantiCBean {
	private String checkCella1;
	private String checkCella2;
	private String checkCella3;
	private String checkCella4;
	private String checkCella5;
	private String checkCella6;
	public String getCheckCella1() {
		return checkCella1;
	}
	public void setCheckCella1(String checkCella1) {
		this.checkCella1 = checkCella1;
	}
	public String getCheckCella2() {
		return checkCella2;
	}
	public void setCheckCella2(String checkCella2) {
		this.checkCella2 = checkCella2;
	}
	public String getCheckCella3() {
		return checkCella3;
	}
	public void setCheckCella3(String checkCella3) {
		this.checkCella3 = checkCella3;
	}
	public String getCheckCella4() {
		return checkCella4;
	}
	public void setCheckCella4(String checkCella4) {
		this.checkCella4 = checkCella4;
	}
	public String getCheckCella5() {
		return checkCella5;
	}
	public void setCheckCella5(String checkCella5) {
		this.checkCella5 = checkCella5;
	}
	public String getCheckCella6() {
		return checkCella6;
	}
	public void setCheckCella6(String checkCella6) {
		this.checkCella6 = checkCella6;
	}
	
	
}
