package com.rextart.saw.report.bean;

public class VerificheFunzMisureStrumentiBean {
	
	private String descCaratteristica;
	private String descNumeroSerie;
	private String descScadenzaTaratura;
	private String descElemento;
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescNumeroSerie() {
		return descNumeroSerie;
	}
	public void setDescNumeroSerie(String descNumeroSerie) {
		this.descNumeroSerie = descNumeroSerie;
	}
	public String getDescScadenzaTaratura() {
		return descScadenzaTaratura;
	}
	public void setDescScadenzaTaratura(String descScadenzaTaratura) {
		this.descScadenzaTaratura = descScadenzaTaratura;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}

	
}
