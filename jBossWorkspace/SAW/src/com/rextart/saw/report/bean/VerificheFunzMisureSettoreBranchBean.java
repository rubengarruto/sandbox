package com.rextart.saw.report.bean;

public class VerificheFunzMisureSettoreBranchBean {

	private String descCaratteristica;
	private String descElemento;
	private String descSettore1Branch1;
	private String descSettore2Branch1;
	private String descSettore3Branch1;
	private String descSettore4Branch1;
	private String descSettore5Branch1;
	private String descSettore6Branch1;
	private String descSettore1Branch2;
	private String descSettore2Branch2;
	private String descSettore3Branch2;
	private String descSettore4Branch2;
	private String descSettore5Branch2;
	private String descSettore6Branch2;
	private String descSettore1Branch3;
	private String descSettore2Branch3;
	private String descSettore3Branch3;
	private String descSettore4Branch3;
	private String descSettore5Branch3;
	private String descSettore6Branch3;
	private String descSettore1Branch4;
	private String descSettore2Branch4;
	private String descSettore3Branch4;
	private String descSettore4Branch4;
	private String descSettore5Branch4;
	private String descSettore6Branch4;
	private String descNote;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getDescSettore1Branch1() {
		return descSettore1Branch1;
	}
	public void setDescSettore1Branch1(String descSettore1Branch1) {
		this.descSettore1Branch1 = descSettore1Branch1;
	}
	public String getDescSettore2Branch1() {
		return descSettore2Branch1;
	}
	public void setDescSettore2Branch1(String descSettore2Branch1) {
		this.descSettore2Branch1 = descSettore2Branch1;
	}
	public String getDescSettore3Branch1() {
		return descSettore3Branch1;
	}
	public void setDescSettore3Branch1(String descSettore3Branch1) {
		this.descSettore3Branch1 = descSettore3Branch1;
	}
	public String getDescSettore4Branch1() {
		return descSettore4Branch1;
	}
	public void setDescSettore4Branch1(String descSettore4Branch1) {
		this.descSettore4Branch1 = descSettore4Branch1;
	}
	public String getDescSettore5Branch1() {
		return descSettore5Branch1;
	}
	public void setDescSettore5Branch1(String descSettore5Branch1) {
		this.descSettore5Branch1 = descSettore5Branch1;
	}
	public String getDescSettore6Branch1() {
		return descSettore6Branch1;
	}
	public void setDescSettore6Branch1(String descSettore6Branch1) {
		this.descSettore6Branch1 = descSettore6Branch1;
	}
	public String getDescSettore1Branch2() {
		return descSettore1Branch2;
	}
	public void setDescSettore1Branch2(String descSettore1Branch2) {
		this.descSettore1Branch2 = descSettore1Branch2;
	}
	public String getDescSettore2Branch2() {
		return descSettore2Branch2;
	}
	public void setDescSettore2Branch2(String descSettore2Branch2) {
		this.descSettore2Branch2 = descSettore2Branch2;
	}
	public String getDescSettore3Branch2() {
		return descSettore3Branch2;
	}
	public void setDescSettore3Branch2(String descSettore3Branch2) {
		this.descSettore3Branch2 = descSettore3Branch2;
	}
	public String getDescSettore4Branch2() {
		return descSettore4Branch2;
	}
	public void setDescSettore4Branch2(String descSettore4Branch2) {
		this.descSettore4Branch2 = descSettore4Branch2;
	}
	public String getDescSettore5Branch2() {
		return descSettore5Branch2;
	}
	public void setDescSettore5Branch2(String descSettore5Branch2) {
		this.descSettore5Branch2 = descSettore5Branch2;
	}
	public String getDescSettore6Branch2() {
		return descSettore6Branch2;
	}
	public void setDescSettore6Branch2(String descSettore6Branch2) {
		this.descSettore6Branch2 = descSettore6Branch2;
	}
	public String getDescSettore1Branch3() {
		return descSettore1Branch3;
	}
	public void setDescSettore1Branch3(String descSettore1Branch3) {
		this.descSettore1Branch3 = descSettore1Branch3;
	}
	public String getDescSettore2Branch3() {
		return descSettore2Branch3;
	}
	public void setDescSettore2Branch3(String descSettore2Branch3) {
		this.descSettore2Branch3 = descSettore2Branch3;
	}
	public String getDescSettore3Branch3() {
		return descSettore3Branch3;
	}
	public void setDescSettore3Branch3(String descSettore3Branch3) {
		this.descSettore3Branch3 = descSettore3Branch3;
	}
	public String getDescSettore4Branch3() {
		return descSettore4Branch3;
	}
	public void setDescSettore4Branch3(String descSettore4Branch3) {
		this.descSettore4Branch3 = descSettore4Branch3;
	}
	public String getDescSettore5Branch3() {
		return descSettore5Branch3;
	}
	public void setDescSettore5Branch3(String descSettore5Branch3) {
		this.descSettore5Branch3 = descSettore5Branch3;
	}
	public String getDescSettore6Branch3() {
		return descSettore6Branch3;
	}
	public void setDescSettore6Branch3(String descSettore6Branch3) {
		this.descSettore6Branch3 = descSettore6Branch3;
	}
	public String getDescSettore1Branch4() {
		return descSettore1Branch4;
	}
	public void setDescSettore1Branch4(String descSettore1Branch4) {
		this.descSettore1Branch4 = descSettore1Branch4;
	}
	public String getDescSettore2Branch4() {
		return descSettore2Branch4;
	}
	public void setDescSettore2Branch4(String descSettore2Branch4) {
		this.descSettore2Branch4 = descSettore2Branch4;
	}
	public String getDescSettore3Branch4() {
		return descSettore3Branch4;
	}
	public void setDescSettore3Branch4(String descSettore3Branch4) {
		this.descSettore3Branch4 = descSettore3Branch4;
	}
	public String getDescSettore4Branch4() {
		return descSettore4Branch4;
	}
	public void setDescSettore4Branch4(String descSettore4Branch4) {
		this.descSettore4Branch4 = descSettore4Branch4;
	}
	public String getDescSettore5Branch4() {
		return descSettore5Branch4;
	}
	public void setDescSettore5Branch4(String descSettore5Branch4) {
		this.descSettore5Branch4 = descSettore5Branch4;
	}
	public String getDescSettore6Branch4() {
		return descSettore6Branch4;
	}
	public void setDescSettore6Branch4(String descSettore6Branch4) {
		this.descSettore6Branch4 = descSettore6Branch4;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
}
