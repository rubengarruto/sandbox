package com.rextart.saw.report.bean;

public class MisureApogeoCelleBean {

	private String duranteInstallazione;
	private String dopoInstallazione;
	private String checkCella1;
	private String macroCella1;
	private String splitCella1;
	private String checkCella2;
	private String macroCella2;
	private String splitCella2;
	private String checkCella3;
	private String macroCella3;
	private String splitCella3;
	private String checkCella4;
	private String macroCella4;
	private String splitCella4;
	private String checkCella5;
	private String macroCella5;
	private String splitCella5;
	private String checkCella6;
	private String macroCella6;
	private String splitCella6;
	
	
	public String getDuranteInstallazione() {
		return duranteInstallazione;
	}
	public void setDuranteInstallazione(String duranteInstallazione) {
		this.duranteInstallazione = duranteInstallazione;
	}
	public String getDopoInstallazione() {
		return dopoInstallazione;
	}
	public void setDopoInstallazione(String dopoInstallazione) {
		this.dopoInstallazione = dopoInstallazione;
	}
	public String getCheckCella1() {
		return checkCella1;
	}
	public void setCheckCella1(String checkCella1) {
		this.checkCella1 = checkCella1;
	}
	public String getMacroCella1() {
		return macroCella1;
	}
	public void setMacroCella1(String macroCella1) {
		this.macroCella1 = macroCella1;
	}
	public String getSplitCella1() {
		return splitCella1;
	}
	public void setSplitCella1(String splitCella1) {
		this.splitCella1 = splitCella1;
	}
	public String getCheckCella2() {
		return checkCella2;
	}
	public void setCheckCella2(String checkCella2) {
		this.checkCella2 = checkCella2;
	}
	public String getMacroCella2() {
		return macroCella2;
	}
	public void setMacroCella2(String macroCella2) {
		this.macroCella2 = macroCella2;
	}
	public String getSplitCella2() {
		return splitCella2;
	}
	public void setSplitCella2(String splitCella2) {
		this.splitCella2 = splitCella2;
	}
	public String getCheckCella3() {
		return checkCella3;
	}
	public void setCheckCella3(String checkCella3) {
		this.checkCella3 = checkCella3;
	}
	public String getMacroCella3() {
		return macroCella3;
	}
	public void setMacroCella3(String macroCella3) {
		this.macroCella3 = macroCella3;
	}
	public String getSplitCella3() {
		return splitCella3;
	}
	public void setSplitCella3(String splitCella3) {
		this.splitCella3 = splitCella3;
	}
	public String getCheckCella4() {
		return checkCella4;
	}
	public void setCheckCella4(String checkCella4) {
		this.checkCella4 = checkCella4;
	}
	public String getMacroCella4() {
		return macroCella4;
	}
	public void setMacroCella4(String macroCella4) {
		this.macroCella4 = macroCella4;
	}
	public String getSplitCella4() {
		return splitCella4;
	}
	public void setSplitCella4(String splitCella4) {
		this.splitCella4 = splitCella4;
	}
	public String getCheckCella5() {
		return checkCella5;
	}
	public void setCheckCella5(String checkCella5) {
		this.checkCella5 = checkCella5;
	}
	public String getMacroCella5() {
		return macroCella5;
	}
	public void setMacroCella5(String macroCella5) {
		this.macroCella5 = macroCella5;
	}
	public String getSplitCella5() {
		return splitCella5;
	}
	public void setSplitCella5(String splitCella5) {
		this.splitCella5 = splitCella5;
	}
	public String getCheckCella6() {
		return checkCella6;
	}
	public void setCheckCella6(String checkCella6) {
		this.checkCella6 = checkCella6;
	}
	public String getMacroCella6() {
		return macroCella6;
	}
	public void setMacroCella6(String macroCella6) {
		this.macroCella6 = macroCella6;
	}
	public String getSplitCella6() {
		return splitCella6;
	}
	public void setSplitCella6(String splitCella6) {
		this.splitCella6 = splitCella6;
	}
	
	

	
	
	
	
}
