package com.rextart.saw.report.bean;

public class TestFlussiBean {
	private String descCaratteristica;
	private Integer codiCaratteristica;
	private String descElemento;
	private Integer codiElemento;
	private String descNote;
	private String descE1;
	private String descE2;
	private String descE3;
	private String descE4;
	private String descE5;
	private String descE6;
	private String descE7;
	private String descE8;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	public String getDescE1() {
		return descE1;
	}
	public void setDescE1(String descE1) {
		this.descE1 = descE1;
	}
	public String getDescE2() {
		return descE2;
	}
	public void setDescE2(String descE2) {
		this.descE2 = descE2;
	}
	public String getDescE3() {
		return descE3;
	}
	public void setDescE3(String descE3) {
		this.descE3 = descE3;
	}
	public String getDescE4() {
		return descE4;
	}
	public void setDescE4(String descE4) {
		this.descE4 = descE4;
	}
	public String getDescE5() {
		return descE5;
	}
	public void setDescE5(String descE5) {
		this.descE5 = descE5;
	}
	public String getDescE6() {
		return descE6;
	}
	public void setDescE6(String descE6) {
		this.descE6 = descE6;
	}
	public String getDescE7() {
		return descE7;
	}
	public void setDescE7(String descE7) {
		this.descE7 = descE7;
	}
	public String getDescE8() {
		return descE8;
	}
	public void setDescE8(String descE8) {
		this.descE8 = descE8;
	}
	public Integer getCodiCaratteristica() {
		return codiCaratteristica;
	}
	public void setCodiCaratteristica(Integer codiCaratteristica) {
		this.codiCaratteristica = codiCaratteristica;
	}
	
	
	
	
}
