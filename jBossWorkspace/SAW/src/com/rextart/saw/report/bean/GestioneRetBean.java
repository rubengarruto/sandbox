package com.rextart.saw.report.bean;

public class GestioneRetBean {

	private String modalitaGesioneRET;
	private String retGestitoDa;
	public String getModalitaGesioneRET() {
		return modalitaGesioneRET;
	}
	public void setModalitaGesioneRET(String modalitaGesioneRET) {
		this.modalitaGesioneRET = modalitaGesioneRET;
	}
	public String getRetGestitoDa() {
		return retGestitoDa;
	}
	public void setRetGestitoDa(String retGestitoDa) {
		this.retGestitoDa = retGestitoDa;
	}
	
	
}
