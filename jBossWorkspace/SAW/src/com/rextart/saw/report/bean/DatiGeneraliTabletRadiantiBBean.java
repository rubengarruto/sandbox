package com.rextart.saw.report.bean;

public class DatiGeneraliTabletRadiantiBBean {
	private String tipoMisura;
	private String costruttoreStrumento;
	private String modelloStrumento;
	private String dataTaratura;
	public String getTipoMisura() {
		return tipoMisura;
	}
	public void setTipoMisura(String tipoMisura) {
		this.tipoMisura = tipoMisura;
	}
	public String getCostruttoreStrumento() {
		return costruttoreStrumento;
	}
	public void setCostruttoreStrumento(String costruttoreStrumento) {
		this.costruttoreStrumento = costruttoreStrumento;
	}
	public String getModelloStrumento() {
		return modelloStrumento;
	}
	public void setModelloStrumento(String modelloStrumento) {
		this.modelloStrumento = modelloStrumento;
	}
	public String getDataTaratura() {
		return dataTaratura;
	}
	public void setDataTaratura(String dataTaratura) {
		this.dataTaratura = dataTaratura;
	}
	
	
}
