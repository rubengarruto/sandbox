package com.rextart.saw.report.bean;

public class MisureDiCaratterizzazioneCondivisioneCalataBean {
	private Boolean cella1flgCheck=false;
	private String cella1Via01;
	private String cella1Via23;
	private Boolean cella2flgCheck=false;
	private String cella2Via01;
	private String cella2Via23;
	private Boolean cella3flgCheck=false;
	private String cella3Via01;
	private String cella3Via23;
	private Boolean cella4flgCheck=false;
	private String cella4Via01;
	private String cella4Via23;
	private Boolean cella5flgCheck=false;
	private String cella5Via01;
	private String cella5Via23;
	private Boolean cella6flgCheck=false;
	private String cella6Via01;
	private String cella6Via23;
	public Boolean getCella1flgCheck() {
		return cella1flgCheck;
	}
	public void setCella1flgCheck(Boolean cella1flgCheck) {
		this.cella1flgCheck = cella1flgCheck;
	}
	public String getCella1Via01() {
		return cella1Via01;
	}
	public void setCella1Via01(String cella1Via01) {
		this.cella1Via01 = cella1Via01;
	}
	public String getCella1Via23() {
		return cella1Via23;
	}
	public void setCella1Via23(String cella1Via23) {
		this.cella1Via23 = cella1Via23;
	}
	public Boolean getCella2flgCheck() {
		return cella2flgCheck;
	}
	public void setCella2flgCheck(Boolean cella2flgCheck) {
		this.cella2flgCheck = cella2flgCheck;
	}
	public String getCella2Via01() {
		return cella2Via01;
	}
	public void setCella2Via01(String cella2Via01) {
		this.cella2Via01 = cella2Via01;
	}
	public String getCella2Via23() {
		return cella2Via23;
	}
	public void setCella2Via23(String cella2Via23) {
		this.cella2Via23 = cella2Via23;
	}
	public Boolean getCella3flgCheck() {
		return cella3flgCheck;
	}
	public void setCella3flgCheck(Boolean cella3flgCheck) {
		this.cella3flgCheck = cella3flgCheck;
	}
	public String getCella3Via01() {
		return cella3Via01;
	}
	public void setCella3Via01(String cella3Via01) {
		this.cella3Via01 = cella3Via01;
	}
	public String getCella3Via23() {
		return cella3Via23;
	}
	public void setCella3Via23(String cella3Via23) {
		this.cella3Via23 = cella3Via23;
	}
	public Boolean getCella4flgCheck() {
		return cella4flgCheck;
	}
	public void setCella4flgCheck(Boolean cella4flgCheck) {
		this.cella4flgCheck = cella4flgCheck;
	}
	public String getCella4Via01() {
		return cella4Via01;
	}
	public void setCella4Via01(String cella4Via01) {
		this.cella4Via01 = cella4Via01;
	}
	public String getCella4Via23() {
		return cella4Via23;
	}
	public void setCella4Via23(String cella4Via23) {
		this.cella4Via23 = cella4Via23;
	}
	public Boolean getCella5flgCheck() {
		return cella5flgCheck;
	}
	public void setCella5flgCheck(Boolean cella5flgCheck) {
		this.cella5flgCheck = cella5flgCheck;
	}
	public String getCella5Via01() {
		return cella5Via01;
	}
	public void setCella5Via01(String cella5Via01) {
		this.cella5Via01 = cella5Via01;
	}
	public String getCella5Via23() {
		return cella5Via23;
	}
	public void setCella5Via23(String cella5Via23) {
		this.cella5Via23 = cella5Via23;
	}
	public Boolean getCella6flgCheck() {
		return cella6flgCheck;
	}
	public void setCella6flgCheck(Boolean cella6flgCheck) {
		this.cella6flgCheck = cella6flgCheck;
	}
	public String getCella6Via01() {
		return cella6Via01;
	}
	public void setCella6Via01(String cella6Via01) {
		this.cella6Via01 = cella6Via01;
	}
	public String getCella6Via23() {
		return cella6Via23;
	}
	public void setCella6Via23(String cella6Via23) {
		this.cella6Via23 = cella6Via23;
	}
	
	
}
