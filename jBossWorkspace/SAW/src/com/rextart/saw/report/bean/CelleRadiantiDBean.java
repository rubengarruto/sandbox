package com.rextart.saw.report.bean;

public class CelleRadiantiDBean {

	private String checkCella1;
	private String tipoCella1;
	private String statoCella1;
	private String via01Cella1;
	private String via23Cella1;
	private String checkCella2;
	private String tipoCella2;
	private String statoCella2;
	private String via01Cella2;
	private String via23Cella2;
	private String checkCella3;
	private String tipoCella3;
	private String statoCella3;
	private String via01Cella3;
	private String via23Cella3;
	private String checkCella4;
	private String tipoCella4;
	private String statoCella4;
	private String via01Cella4;
	private String via23Cella4;
	private String checkCella5;
	private String tipoCella5;
	private String statoCella5;
	private String via01Cella5;
	private String via23Cella5;
	private String checkCella6;
	private String tipoCella6;
	private String statoCella6;
	private String via01Cella6;
	private String via23Cella6;
	public String getCheckCella1() {
		return checkCella1;
	}
	public void setCheckCella1(String checkCella1) {
		this.checkCella1 = checkCella1;
	}
	public String getTipoCella1() {
		return tipoCella1;
	}
	public void setTipoCella1(String tipoCella1) {
		this.tipoCella1 = tipoCella1;
	}
	public String getStatoCella1() {
		return statoCella1;
	}
	public void setStatoCella1(String statoCella1) {
		this.statoCella1 = statoCella1;
	}
	public String getVia01Cella1() {
		return via01Cella1;
	}
	public void setVia01Cella1(String via01Cella1) {
		this.via01Cella1 = via01Cella1;
	}
	public String getVia23Cella1() {
		return via23Cella1;
	}
	public void setVia23Cella1(String via23Cella1) {
		this.via23Cella1 = via23Cella1;
	}
	public String getCheckCella2() {
		return checkCella2;
	}
	public void setCheckCella2(String checkCella2) {
		this.checkCella2 = checkCella2;
	}
	public String getTipoCella2() {
		return tipoCella2;
	}
	public void setTipoCella2(String tipoCella2) {
		this.tipoCella2 = tipoCella2;
	}
	public String getStatoCella2() {
		return statoCella2;
	}
	public void setStatoCella2(String statoCella2) {
		this.statoCella2 = statoCella2;
	}
	public String getVia01Cella2() {
		return via01Cella2;
	}
	public void setVia01Cella2(String via01Cella2) {
		this.via01Cella2 = via01Cella2;
	}
	public String getVia23Cella2() {
		return via23Cella2;
	}
	public void setVia23Cella2(String via23Cella2) {
		this.via23Cella2 = via23Cella2;
	}
	public String getCheckCella3() {
		return checkCella3;
	}
	public void setCheckCella3(String checkCella3) {
		this.checkCella3 = checkCella3;
	}
	public String getTipoCella3() {
		return tipoCella3;
	}
	public void setTipoCella3(String tipoCella3) {
		this.tipoCella3 = tipoCella3;
	}
	public String getStatoCella3() {
		return statoCella3;
	}
	public void setStatoCella3(String statoCella3) {
		this.statoCella3 = statoCella3;
	}
	public String getVia01Cella3() {
		return via01Cella3;
	}
	public void setVia01Cella3(String via01Cella3) {
		this.via01Cella3 = via01Cella3;
	}
	public String getVia23Cella3() {
		return via23Cella3;
	}
	public void setVia23Cella3(String via23Cella3) {
		this.via23Cella3 = via23Cella3;
	}
	public String getCheckCella4() {
		return checkCella4;
	}
	public void setCheckCella4(String checkCella4) {
		this.checkCella4 = checkCella4;
	}
	public String getTipoCella4() {
		return tipoCella4;
	}
	public void setTipoCella4(String tipoCella4) {
		this.tipoCella4 = tipoCella4;
	}
	public String getStatoCella4() {
		return statoCella4;
	}
	public void setStatoCella4(String statoCella4) {
		this.statoCella4 = statoCella4;
	}
	public String getVia01Cella4() {
		return via01Cella4;
	}
	public void setVia01Cella4(String via01Cella4) {
		this.via01Cella4 = via01Cella4;
	}
	public String getVia23Cella4() {
		return via23Cella4;
	}
	public void setVia23Cella4(String via23Cella4) {
		this.via23Cella4 = via23Cella4;
	}
	public String getCheckCella5() {
		return checkCella5;
	}
	public void setCheckCella5(String checkCella5) {
		this.checkCella5 = checkCella5;
	}
	public String getTipoCella5() {
		return tipoCella5;
	}
	public void setTipoCella5(String tipoCella5) {
		this.tipoCella5 = tipoCella5;
	}
	public String getStatoCella5() {
		return statoCella5;
	}
	public void setStatoCella5(String statoCella5) {
		this.statoCella5 = statoCella5;
	}
	public String getVia01Cella5() {
		return via01Cella5;
	}
	public void setVia01Cella5(String via01Cella5) {
		this.via01Cella5 = via01Cella5;
	}
	public String getVia23Cella5() {
		return via23Cella5;
	}
	public void setVia23Cella5(String via23Cella5) {
		this.via23Cella5 = via23Cella5;
	}
	public String getCheckCella6() {
		return checkCella6;
	}
	public void setCheckCella6(String checkCella6) {
		this.checkCella6 = checkCella6;
	}
	public String getTipoCella6() {
		return tipoCella6;
	}
	public void setTipoCella6(String tipoCella6) {
		this.tipoCella6 = tipoCella6;
	}
	public String getVia01Cella6() {
		return via01Cella6;
	}
	public void setVia01Cella6(String via01Cella6) {
		this.via01Cella6 = via01Cella6;
	}
	public String getVia23Cella6() {
		return via23Cella6;
	}
	public void setVia23Cella6(String via23Cella6) {
		this.via23Cella6 = via23Cella6;
	}
	public String getStatoCella6() {
		return statoCella6;
	}
	public void setStatoCella6(String statoCella6) {
		this.statoCella6 = statoCella6;
	}
	
	
}
