package com.rextart.saw.report.bean;

public class VerificheFunzMisureOutPowValRifBean {
	private String descCaratteristica;
	private String descElemento;
	private Integer codiElemento;
	private String descNote;
	private String descLabel1;
	private String descLabel2;
	private String descLabel3;
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	public String getDescLabel1() {
		return descLabel1;
	}
	public void setDescLabel1(String descLabel1) {
		this.descLabel1 = descLabel1;
	}
	public String getDescLabel2() {
		return descLabel2;
	}
	public void setDescLabel2(String descLabel2) {
		this.descLabel2 = descLabel2;
	}
	public String getDescLabel3() {
		return descLabel3;
	}
	public void setDescLabel3(String descLabel3) {
		this.descLabel3 = descLabel3;
	}
	public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}
	
	
}
