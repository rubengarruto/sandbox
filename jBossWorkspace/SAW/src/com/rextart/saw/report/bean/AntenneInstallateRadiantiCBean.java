package com.rextart.saw.report.bean;

public class AntenneInstallateRadiantiCBean {

	private String cella1;
	private String cella2;
	private String cella3;
	private String cella4;
	private String cella5;
	private String cella6;
	
	public String getCella1() {
		return cella1;
	}
	public void setCella1(String cella1) {
		this.cella1 = cella1;
	}
	public String getCella2() {
		return cella2;
	}
	public void setCella2(String cella2) {
		this.cella2 = cella2;
	}
	public String getCella3() {
		return cella3;
	}
	public void setCella3(String cella3) {
		this.cella3 = cella3;
	}
	public String getCella4() {
		return cella4;
	}
	public void setCella4(String cella4) {
		this.cella4 = cella4;
	}
	public String getCella5() {
		return cella5;
	}
	public void setCella5(String cella5) {
		this.cella5 = cella5;
	}
	public String getCella6() {
		return cella6;
	}
	public void setCella6(String cella6) {
		this.cella6 = cella6;
	}
	
	
}
