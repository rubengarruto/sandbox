package com.rextart.saw.report.bean;

public class VerificheInstallazioneTensioniWIPMBean {
	private String descCaratteristica;
	private String descElemento;
	private Integer codiElemento;
	private String descNote;
	private String descValoreMisurato;
	private String descEsito;
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	public String getDescValoreMisurato() {
		return descValoreMisurato;
	}
	public void setDescValoreMisurato(String descValoreMisurato) {
		this.descValoreMisurato = descValoreMisurato;
	}
	public String getDescEsito() {
		return descEsito;
	}
	public void setDescEsito(String descEsito) {
		this.descEsito = descEsito;
	}
	
	
}
