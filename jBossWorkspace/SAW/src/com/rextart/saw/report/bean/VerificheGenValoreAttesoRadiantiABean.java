package com.rextart.saw.report.bean;

public class VerificheGenValoreAttesoRadiantiABean {

	private String descCaratteristica;
	private String descElemento;
	private Boolean flagCheck;
	private Boolean flagNotIsertable;
	private String descCella1Label1;
	private String descCella1Label2;
	private String descCella1Label3;
	private String descCella2Label1;
	private String descCella2Label2;
	private String descCella2Label3;
	private String descCella3Label1;
	private String descCella3Label2;
	private String descCella3Label3;
	private String descCella4Label1;
	private String descCella4Label2;
	private String descCella4Label3;
	private String descCella5Label1;
	private String descCella5Label2;
	private String descCella5Label3;
	private String descCella6Label1;
	private String descCella6Label2;
	private String descCella6Label3;
	private Integer codiElemento;
	
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public Boolean getFlagCheck() {
		return flagCheck;
	}
	public void setFlagCheck(Boolean flagCheck) {
		this.flagCheck = flagCheck;
	}
	public Boolean getFlagNotIsertable() {
		return flagNotIsertable;
	}
	public void setFlagNotIsertable(Boolean flagNotIsertable) {
		this.flagNotIsertable = flagNotIsertable;
	}
	public String getDescCella1Label1() {
		return descCella1Label1;
	}
	public void setDescCella1Label1(String descCella1Label1) {
		this.descCella1Label1 = descCella1Label1;
	}
	public String getDescCella1Label2() {
		return descCella1Label2;
	}
	public void setDescCella1Label2(String descCella1Label2) {
		this.descCella1Label2 = descCella1Label2;
	}
	public String getDescCella2Label1() {
		return descCella2Label1;
	}
	public void setDescCella2Label1(String descCella2Label1) {
		this.descCella2Label1 = descCella2Label1;
	}
	public String getDescCella2Label2() {
		return descCella2Label2;
	}
	public void setDescCella2Label2(String descCella2Label2) {
		this.descCella2Label2 = descCella2Label2;
	}
	public String getDescCella3Label1() {
		return descCella3Label1;
	}
	public void setDescCella3Label1(String descCella3Label1) {
		this.descCella3Label1 = descCella3Label1;
	}
	public String getDescCella3Label2() {
		return descCella3Label2;
	}
	public void setDescCella3Label2(String descCella3Label2) {
		this.descCella3Label2 = descCella3Label2;
	}
	public String getDescCella4Label1() {
		return descCella4Label1;
	}
	public void setDescCella4Label1(String descCella4Label1) {
		this.descCella4Label1 = descCella4Label1;
	}
	public String getDescCella4Label2() {
		return descCella4Label2;
	}
	public void setDescCella4Label2(String descCella4Label2) {
		this.descCella4Label2 = descCella4Label2;
	}
	public String getDescCella5Label1() {
		return descCella5Label1;
	}
	public void setDescCella5Label1(String descCella5Label1) {
		this.descCella5Label1 = descCella5Label1;
	}
	public String getDescCella5Label2() {
		return descCella5Label2;
	}
	public void setDescCella5Label2(String descCella5Label2) {
		this.descCella5Label2 = descCella5Label2;
	}
	public String getDescCella6Label1() {
		return descCella6Label1;
	}
	public void setDescCella6Label1(String descCella6Label1) {
		this.descCella6Label1 = descCella6Label1;
	}
	public String getDescCella6Label2() {
		return descCella6Label2;
	}
	public void setDescCella6Label2(String descCella6Label2) {
		this.descCella6Label2 = descCella6Label2;
	}
	public String getDescCella1Label3() {
		return descCella1Label3;
	}
	public void setDescCella1Label3(String descCella1Label3) {
		this.descCella1Label3 = descCella1Label3;
	}
	public String getDescCella2Label3() {
		return descCella2Label3;
	}
	public void setDescCella2Label3(String descCella2Label3) {
		this.descCella2Label3 = descCella2Label3;
	}
	public String getDescCella3Label3() {
		return descCella3Label3;
	}
	public void setDescCella3Label3(String descCella3Label3) {
		this.descCella3Label3 = descCella3Label3;
	}
	public String getDescCella4Label3() {
		return descCella4Label3;
	}
	public void setDescCella4Label3(String descCella4Label3) {
		this.descCella4Label3 = descCella4Label3;
	}
	public String getDescCella5Label3() {
		return descCella5Label3;
	}
	public void setDescCella5Label3(String descCella5Label3) {
		this.descCella5Label3 = descCella5Label3;
	}
	public String getDescCella6Label3() {
		return descCella6Label3;
	}
	public void setDescCella6Label3(String descCella6Label3) {
		this.descCella6Label3 = descCella6Label3;
	}
	public Integer getCodiElemento() {
		return codiElemento;
	}
	public void setCodiElemento(Integer codiElemento) {
		this.codiElemento = codiElemento;
	}

	
	
	
}
