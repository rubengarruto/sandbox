package com.rextart.saw.report.bean;

public class VerFunzAllarmiEsterniBean {
	private String tipoTelaio;
	private String competenzaAllarmi;
	private String allarme;
	private String alarmIdM2000;
	private String criterio;
	private String severita;
	private String sloganAllarme;
	private String esito;
	private String note;
	public String getTipoTelaio() {
		return tipoTelaio;
	}
	public void setTipoTelaio(String tipoTelaio) {
		this.tipoTelaio = tipoTelaio;
	}
	public String getCompetenzaAllarmi() {
		return competenzaAllarmi;
	}
	public void setCompetenzaAllarmi(String competenzaAllarmi) {
		this.competenzaAllarmi = competenzaAllarmi;
	}
	public String getAllarme() {
		return allarme;
	}
	public void setAllarme(String allarme) {
		this.allarme = allarme;
	}
	public String getAlarmIdM2000() {
		return alarmIdM2000;
	}
	public void setAlarmIdM2000(String alarmiIdM2000) {
		this.alarmIdM2000 = alarmiIdM2000;
	}
	public String getCriterio() {
		return criterio;
	}
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}
	public String getSeverita() {
		return severita;
	}
	public void setSeverita(String severita) {
		this.severita = severita;
	}
	public String getSloganAllarme() {
		return sloganAllarme;
	}
	public void setSloganAllarme(String sloganAllarme) {
		this.sloganAllarme = sloganAllarme;
	}
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}
