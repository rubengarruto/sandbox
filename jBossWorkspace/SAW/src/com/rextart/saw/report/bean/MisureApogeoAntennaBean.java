package com.rextart.saw.report.bean;

public class MisureApogeoAntennaBean {

	public String descCaratteristica;
	public String check;
	public String valoreAttesoCella1;
	public String valoreRilevatoCella1;
	public String conformeCella1;
	public String valoreAttesoCella2;
	public String valoreRilevatoCella2;
	public String conformeCella2;
	public String valoreAttesoCella3;
	public String valoreRilevatoCella3;
	public String conformeCella3;
	public String valoreAttesoCella4;
	public String valoreRilevatoCella4;
	public String conformeCella4;
	public String valoreAttesoCella5;
	public String valoreRilevatoCella5;
	public String conformeCella5;
	public String valoreAttesoCella6;
	public String valoreRilevatoCella6;
	public String conformeCella6;
	
	
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public String getValoreAttesoCella1() {
		return valoreAttesoCella1;
	}
	public void setValoreAttesoCella1(String valoreAttesoCella1) {
		this.valoreAttesoCella1 = valoreAttesoCella1;
	}
	public String getValoreRilevatoCella1() {
		return valoreRilevatoCella1;
	}
	public void setValoreRilevatoCella1(String valoreRilevatoCella1) {
		this.valoreRilevatoCella1 = valoreRilevatoCella1;
	}
	public String getConformeCella1() {
		return conformeCella1;
	}
	public void setConformeCella1(String conformeCella1) {
		this.conformeCella1 = conformeCella1;
	}
	public String getValoreAttesoCella2() {
		return valoreAttesoCella2;
	}
	public void setValoreAttesoCella2(String valoreAttesoCella2) {
		this.valoreAttesoCella2 = valoreAttesoCella2;
	}
	public String getValoreRilevatoCella2() {
		return valoreRilevatoCella2;
	}
	public void setValoreRilevatoCella2(String valoreRilevatoCella2) {
		this.valoreRilevatoCella2 = valoreRilevatoCella2;
	}
	public String getConformeCella2() {
		return conformeCella2;
	}
	public void setConformeCella2(String conformeCella2) {
		this.conformeCella2 = conformeCella2;
	}
	public String getValoreAttesoCella3() {
		return valoreAttesoCella3;
	}
	public void setValoreAttesoCella3(String valoreAttesoCella3) {
		this.valoreAttesoCella3 = valoreAttesoCella3;
	}
	public String getValoreRilevatoCella3() {
		return valoreRilevatoCella3;
	}
	public void setValoreRilevatoCella3(String valoreRilevatoCella3) {
		this.valoreRilevatoCella3 = valoreRilevatoCella3;
	}
	public String getConformeCella3() {
		return conformeCella3;
	}
	public void setConformeCella3(String conformeCella3) {
		this.conformeCella3 = conformeCella3;
	}
	public String getValoreAttesoCella4() {
		return valoreAttesoCella4;
	}
	public void setValoreAttesoCella4(String valoreAttesoCella4) {
		this.valoreAttesoCella4 = valoreAttesoCella4;
	}
	public String getValoreRilevatoCella4() {
		return valoreRilevatoCella4;
	}
	public void setValoreRilevatoCella4(String valoreRilevatoCella4) {
		this.valoreRilevatoCella4 = valoreRilevatoCella4;
	}
	public String getConformeCella4() {
		return conformeCella4;
	}
	public void setConformeCella4(String conformeCella4) {
		this.conformeCella4 = conformeCella4;
	}
	public String getValoreAttesoCella5() {
		return valoreAttesoCella5;
	}
	public void setValoreAttesoCella5(String valoreAttesoCella5) {
		this.valoreAttesoCella5 = valoreAttesoCella5;
	}
	public String getValoreRilevatoCella5() {
		return valoreRilevatoCella5;
	}
	public void setValoreRilevatoCella5(String valoreRilevatoCella5) {
		this.valoreRilevatoCella5 = valoreRilevatoCella5;
	}
	public String getConformeCella5() {
		return conformeCella5;
	}
	public void setConformeCella5(String conformeCella5) {
		this.conformeCella5 = conformeCella5;
	}
	public String getValoreAttesoCella6() {
		return valoreAttesoCella6;
	}
	public void setValoreAttesoCella6(String valoreAttesoCella6) {
		this.valoreAttesoCella6 = valoreAttesoCella6;
	}
	public String getValoreRilevatoCella6() {
		return valoreRilevatoCella6;
	}
	public void setValoreRilevatoCella6(String valoreRilevatoCella6) {
		this.valoreRilevatoCella6 = valoreRilevatoCella6;
	}
	public String getConformeCella6() {
		return conformeCella6;
	}
	public void setConformeCella6(String conformeCella6) {
		this.conformeCella6 = conformeCella6;
	}
	
	
	
}
