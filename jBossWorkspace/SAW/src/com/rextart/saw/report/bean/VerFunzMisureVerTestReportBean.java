package com.rextart.saw.report.bean;

public class VerFunzMisureVerTestReportBean {

	private String descCella;
	private String descEsito;
	private String descNote;
	public String getDescCella() {
		return descCella;
	}
	public void setDescCella(String descCella) {
		this.descCella = descCella;
	}
	public String getDescEsito() {
		return descEsito;
	}
	public void setDescEsito(String descEsito) {
		this.descEsito = descEsito;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
	
}
