package com.rextart.saw.report.bean;

public class VerificheFunzMisureSettoreBean {
	
	private String descCaratteristica;
	private String descElemento;
	private String descSettore1;
	private String descSettore2;
	private String descSettore3;
	private String descSettore4;
	private String descSettore5;
	private String descSettore6;
	private String descNote;
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	public String getDescElemento() {
		return descElemento;
	}
	public void setDescElemento(String descElemento) {
		this.descElemento = descElemento;
	}
	public String getDescSettore1() {
		return descSettore1;
	}
	public void setDescSettore1(String descSettore1) {
		this.descSettore1 = descSettore1;
	}
	public String getDescSettore2() {
		return descSettore2;
	}
	public void setDescSettore2(String descSettore2) {
		this.descSettore2 = descSettore2;
	}
	public String getDescSettore3() {
		return descSettore3;
	}
	public void setDescSettore3(String descSettore3) {
		this.descSettore3 = descSettore3;
	}
	public String getDescSettore4() {
		return descSettore4;
	}
	public void setDescSettore4(String descSettore4) {
		this.descSettore4 = descSettore4;
	}
	public String getDescSettore5() {
		return descSettore5;
	}
	public void setDescSettore5(String descSettore5) {
		this.descSettore5 = descSettore5;
	}
	public String getDescSettore6() {
		return descSettore6;
	}
	public void setDescSettore6(String descSettore6) {
		this.descSettore6 = descSettore6;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	
	
	
	

	
}
