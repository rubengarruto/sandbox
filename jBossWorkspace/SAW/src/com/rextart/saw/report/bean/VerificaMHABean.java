package com.rextart.saw.report.bean;

public class VerificaMHABean {

	private String cella1Via01;
	private String cella2Via01;
	private String cella3Via01;
	private String cella4Via01;
	private String cella5Via01;
	private String cella6Via01;
	private String descNoteVia01;
	private String cella1Via02;
	private String cella2Via02;
	private String cella3Via02;
	private String cella4Via02;
	private String cella5Via02;
	private String cella6Via02;
	private String descNoteVia02;
	private String descCaratteristica;
	
	public String getCella1Via01() {
		return cella1Via01;
	}
	public void setCella1Via01(String cella1Via01) {
		this.cella1Via01 = cella1Via01;
	}
	public String getCella2Via01() {
		return cella2Via01;
	}
	public void setCella2Via01(String cella2Via01) {
		this.cella2Via01 = cella2Via01;
	}
	public String getCella3Via01() {
		return cella3Via01;
	}
	public void setCella3Via01(String cella3Via01) {
		this.cella3Via01 = cella3Via01;
	}
	public String getCella4Via01() {
		return cella4Via01;
	}
	public void setCella4Via01(String cella4Via01) {
		this.cella4Via01 = cella4Via01;
	}
	public String getCella5Via01() {
		return cella5Via01;
	}
	public void setCella5Via01(String cella5Via01) {
		this.cella5Via01 = cella5Via01;
	}
	public String getCella6Via01() {
		return cella6Via01;
	}
	public void setCella6Via01(String cella6Via01) {
		this.cella6Via01 = cella6Via01;
	}
	public String getDescNoteVia01() {
		return descNoteVia01;
	}
	public void setDescNoteVia01(String descNoteVia01) {
		this.descNoteVia01 = descNoteVia01;
	}
	public String getCella1Via02() {
		return cella1Via02;
	}
	public void setCella1Via02(String cella1Via02) {
		this.cella1Via02 = cella1Via02;
	}
	public String getCella2Via02() {
		return cella2Via02;
	}
	public void setCella2Via02(String cella2Via02) {
		this.cella2Via02 = cella2Via02;
	}
	public String getCella3Via02() {
		return cella3Via02;
	}
	public void setCella3Via02(String cella3Via02) {
		this.cella3Via02 = cella3Via02;
	}
	public String getCella4Via02() {
		return cella4Via02;
	}
	public void setCella4Via02(String cella4Via02) {
		this.cella4Via02 = cella4Via02;
	}
	public String getCella5Via02() {
		return cella5Via02;
	}
	public void setCella5Via02(String cella5Via02) {
		this.cella5Via02 = cella5Via02;
	}
	public String getCella6Via02() {
		return cella6Via02;
	}
	public void setCella6Via02(String cella6Via02) {
		this.cella6Via02 = cella6Via02;
	}
	public String getDescNoteVia02() {
		return descNoteVia02;
	}
	public void setDescNoteVia02(String descNoteVia02) {
		this.descNoteVia02 = descNoteVia02;
	}
	public String getDescCaratteristica() {
		return descCaratteristica;
	}
	public void setDescCaratteristica(String descCaratteristica) {
		this.descCaratteristica = descCaratteristica;
	}
	
	
	
	

	
}
