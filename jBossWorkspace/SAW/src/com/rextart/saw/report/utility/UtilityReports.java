package com.rextart.saw.report.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang.ArrayUtils;

import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.dao.SurveyDao;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.ViwApparatoEricsson;
import com.rextart.saw.entity.ViwApparatoHuawei;
import com.rextart.saw.entity.ViwApparatoNokia;
import com.rextart.saw.entity.ViwConfGenRadC;
import com.rextart.saw.entity.ViwMisureDiPimRadD;
import com.rextart.saw.entity.ViwMisureGenRadB;
import com.rextart.saw.entity.ViwProveFunzSerEricsson;
import com.rextart.saw.entity.ViwRadiantiE;
import com.rextart.saw.entity.ViwRiferimentiFirma;
import com.rextart.saw.entity.ViwVerFunzAllarmi;
import com.rextart.saw.entity.ViwVerFunzAllarmiEricsson;
import com.rextart.saw.entity.ViwVerFunzMisureEricsson;
import com.rextart.saw.entity.ViwVerGenRadA;
import com.rextart.saw.report.bean.AllarmiEsterni2GBean;
import com.rextart.saw.report.bean.AntenneInstallateRadiantiCBean;
import com.rextart.saw.report.bean.CelleRadiantiDBean;
import com.rextart.saw.report.bean.ConfigurazioneCelleRadiantiCBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletApparatoBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiABean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiBBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiDBean;
import com.rextart.saw.report.bean.DettaglioTecnologieRadiantiCBean;
import com.rextart.saw.report.bean.GestioneRetBean;
import com.rextart.saw.report.bean.MisureApogeoAntennaBean;
import com.rextart.saw.report.bean.MisureApogeoCelleBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneCalibrazioneRadiantiBBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneCondivisioneCalataBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneGeneraliRadiantiBBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneTipoCavoRadiantiBBean;
import com.rextart.saw.report.bean.MisureDtpValorePimSistemaRadiantiDBean;
import com.rextart.saw.report.bean.MisurePimSistemaRadiantiDBean;
import com.rextart.saw.report.bean.ProveDiServizioBean;
import com.rextart.saw.report.bean.RiferimentiBean;
import com.rextart.saw.report.bean.TestFlussiBean;
import com.rextart.saw.report.bean.VerFunzAllarmiEsterniBean;
import com.rextart.saw.report.bean.VerFunzMisureVerTestReportBean;
import com.rextart.saw.report.bean.VerInstWIPMBean;
import com.rextart.saw.report.bean.VerificaMHABean;
import com.rextart.saw.report.bean.VerificaRETBean;
import com.rextart.saw.report.bean.VerificaTMABean;
import com.rextart.saw.report.bean.VerificheFunzMisureCelleBean;
import com.rextart.saw.report.bean.VerificheFunzMisureCelleDoppiaLabelBean;
import com.rextart.saw.report.bean.VerificheFunzMisureSettoreBranchBean;
import com.rextart.saw.report.bean.VerificheFunzMisureSettoreCellaBean;
import com.rextart.saw.report.bean.VerificheGenCelleDoppiaLabelBean;
import com.rextart.saw.report.bean.VerificheGenChainingRETRadiantiABean;
import com.rextart.saw.report.bean.VerificheGenRadiantiABean;
import com.rextart.saw.report.bean.VerificheGenValoreAttesoRadiantiABean;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.ConstReport;

public class UtilityReports {
	private static BufferedImage firmaTim;
	private static BufferedImage firmaDitta;


	public static List<VerificheFunzMisureCelleDoppiaLabelBean> getArrayEricssonMisureCelleDoppiaLabelBean(List<ViwVerFunzMisureEricsson> arrayCelleLabel) {
		List<VerificheFunzMisureCelleDoppiaLabelBean> arrayCelleLabelBean = new ArrayList<VerificheFunzMisureCelleDoppiaLabelBean>();

		ViwVerFunzMisureEricsson  viwVerFunzMisureEricsson;
		for (int i = 0; i < arrayCelleLabel.size(); i++) {
			viwVerFunzMisureEricsson= arrayCelleLabel.get(i);
			VerificheFunzMisureCelleDoppiaLabelBean verificheFunzMisureCanaliCelleLabelBean = new VerificheFunzMisureCelleDoppiaLabelBean();
			verificheFunzMisureCanaliCelleLabelBean.setDescElemento(viwVerFunzMisureEricsson.getDescElemento());
			verificheFunzMisureCanaliCelleLabelBean.setDescCaratteristica(viwVerFunzMisureEricsson.getDescCaratteristica());
			verificheFunzMisureCanaliCelleLabelBean.setDescNote(viwVerFunzMisureEricsson.getDescNote());
			Integer codiCaratteristicaAppo=viwVerFunzMisureEricsson.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerFunzMisureEricsson.getCodiCaratteristica())&& i<arrayCelleLabel.size()) {
				if(viwVerFunzMisureEricsson.getNumeCella()!=null)
					switch (viwVerFunzMisureEricsson.getNumeCella()) {
					case 1:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella1Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella1Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella1Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella1Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 2:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella2Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella2Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella2Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella2Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 3:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella3Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella3Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella3Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella3Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 4:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella4Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella4Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella4Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella4Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 5:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella5Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella5Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella5Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella5Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 6:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null){
							if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
								verificheFunzMisureCanaliCelleLabelBean.setDescCella6Label1(viwVerFunzMisureEricsson.getDescLabelLov());
							else
								verificheFunzMisureCanaliCelleLabelBean.setDescCella6Label2(viwVerFunzMisureEricsson.getDescLabelLov());
						}else if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Via RF 0-1"))
							verificheFunzMisureCanaliCelleLabelBean.setDescCella6Label1(viwVerFunzMisureEricsson.getDescValore());
						else
							verificheFunzMisureCanaliCelleLabelBean.setDescCella6Label2(viwVerFunzMisureEricsson.getDescValore());
						break;
					}
				i++;
				if(i<arrayCelleLabel.size())
					viwVerFunzMisureEricsson= arrayCelleLabel.get(i);
			}
			arrayCelleLabelBean.add(verificheFunzMisureCanaliCelleLabelBean);
			i--;
		}

		return arrayCelleLabelBean;

	}

	public static List<VerificheFunzMisureSettoreBranchBean> getArrayEricssonMisureSettoreBranchBean(List<ViwVerFunzMisureEricsson> arraySettoreBranch) {
		VerificheFunzMisureSettoreBranchBean verificheFunzMisureSettoreBranchBean = new VerificheFunzMisureSettoreBranchBean();
		if(arraySettoreBranch.size()>0){
			verificheFunzMisureSettoreBranchBean.setDescCaratteristica(arraySettoreBranch.get(0).getDescCaratteristica());
			verificheFunzMisureSettoreBranchBean.setDescNote(arraySettoreBranch.get(0).getDescNote());
			verificheFunzMisureSettoreBranchBean.setDescElemento(arraySettoreBranch.get(0).getDescElemento());
		} 
		String settoreBranch;

		for (ViwVerFunzMisureEricsson viwVerFunzMisureEricsson2 : arraySettoreBranch) {
			if(viwVerFunzMisureEricsson2.getNumeSettore()!=null && viwVerFunzMisureEricsson2.getNumeBranch()!=null){
				settoreBranch=viwVerFunzMisureEricsson2.getNumeSettore().toString()+viwVerFunzMisureEricsson2.getNumeBranch().toString();

				switch (settoreBranch) {
				case "11":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "21":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "31":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "41":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "51":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "61":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "12":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "22":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "32":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "42":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "52":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "62":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "13":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "23":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "33":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "43":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "53":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch3(viwVerFunzMisureEricsson2.getDescValore());	
					break;
				case "63":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch3(viwVerFunzMisureEricsson2.getDescValore());
					break;

				case "14":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore1Branch4(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "24":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore2Branch4(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "34":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore3Branch4(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "44":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore4Branch4(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "54":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore5Branch4(viwVerFunzMisureEricsson2.getDescValore());	
					break;
				case "64":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch4(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreBranchBean.setDescSettore6Branch4(viwVerFunzMisureEricsson2.getDescValore());
					break;

				}
			}
		}

		List<VerificheFunzMisureSettoreBranchBean> arrayEricssonMisureSettoreBranchBean = new ArrayList<VerificheFunzMisureSettoreBranchBean>();
		arrayEricssonMisureSettoreBranchBean.add(verificheFunzMisureSettoreBranchBean);


		return arrayEricssonMisureSettoreBranchBean;

	}

	public static List<VerFunzMisureVerTestReportBean> getArrayVerFunzMisureVerTestReportBean (List<ViwVerFunzMisureEricsson> arrayViwVerFunzMisureEricsson) {

		HashMap<Integer , VerFunzMisureVerTestReportBean> hashMap = new HashMap<>();

		VerFunzMisureVerTestReportBean verFunzMisureVerTestReportBean;

		for (int i=1 ; i<=6; i++) {

			verFunzMisureVerTestReportBean = new VerFunzMisureVerTestReportBean();

			verFunzMisureVerTestReportBean.setDescCella("Cella "+i);

			hashMap.put (i,verFunzMisureVerTestReportBean);

		}

		for (ViwVerFunzMisureEricsson viwVerFunzMisureEricsson : arrayViwVerFunzMisureEricsson) {

			if (viwVerFunzMisureEricsson.getNumeCella()!=null) {

				hashMap.get(viwVerFunzMisureEricsson.getNumeCella()).setDescEsito(viwVerFunzMisureEricsson.getDescLabelLov());


				hashMap.get(viwVerFunzMisureEricsson.getNumeCella()).setDescNote(viwVerFunzMisureEricsson.getDescNote());

			}

		}

		ArrayList<VerFunzMisureVerTestReportBean> arrayFunzMisureVerTestReportBeans = new ArrayList<>(hashMap.values());

		return arrayFunzMisureVerTestReportBeans;

	}

	public static List<VerificheFunzMisureCelleBean> getArrayEricssonMisureCelleBean(List<ViwVerFunzMisureEricsson> arrayCelle) {
		List<VerificheFunzMisureCelleBean> arrayCelleBean = new ArrayList<VerificheFunzMisureCelleBean>();

		ViwVerFunzMisureEricsson  viwVerFunzMisureEricsson;
		for (int i = 0; i < arrayCelle.size(); i++) {
			viwVerFunzMisureEricsson= arrayCelle.get(i);
			VerificheFunzMisureCelleBean verificheFunzMisureCanaliCelleBean = new VerificheFunzMisureCelleBean();
			verificheFunzMisureCanaliCelleBean.setDescElemento(viwVerFunzMisureEricsson.getDescElemento());
			verificheFunzMisureCanaliCelleBean.setDescCaratteristica(viwVerFunzMisureEricsson.getDescCaratteristica());
			verificheFunzMisureCanaliCelleBean.setDescNote(viwVerFunzMisureEricsson.getDescNote());
			Integer codiCaratteristicaAppo=viwVerFunzMisureEricsson.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerFunzMisureEricsson.getCodiCaratteristica())&& i<arrayCelle.size()) {
				if(viwVerFunzMisureEricsson.getNumeCella()!=null)
					switch (viwVerFunzMisureEricsson.getNumeCella()) {
					case 1:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella1(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella1(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 2:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella2(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella2(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 3:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella3(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella3(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 4:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella4(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella4(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 5:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella5(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella5(viwVerFunzMisureEricsson.getDescValore());
						break;
					case 6:
						if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
							verificheFunzMisureCanaliCelleBean.setDescCella6(viwVerFunzMisureEricsson.getDescLabelLov());
						else
							verificheFunzMisureCanaliCelleBean.setDescCella6(viwVerFunzMisureEricsson.getDescValore());
						break;
					}
				i++;
				if(i<arrayCelle.size())
					viwVerFunzMisureEricsson= arrayCelle.get(i);
			}
			arrayCelleBean.add(verificheFunzMisureCanaliCelleBean);
			i--;
		}

		return arrayCelleBean;

	}

	public static List<VerificheFunzMisureSettoreCellaBean> getArrayEricssonMisureSettoreCellaBean(List<ViwVerFunzMisureEricsson> arraySettoreCella) {
		VerificheFunzMisureSettoreCellaBean verificheFunzMisureSettoreCellaBean = new VerificheFunzMisureSettoreCellaBean();
		if(arraySettoreCella.size()>0){
			verificheFunzMisureSettoreCellaBean.setDescCaratteristica(arraySettoreCella.get(0).getDescCaratteristica());
			verificheFunzMisureSettoreCellaBean.setDescNote(arraySettoreCella.get(0).getDescNote());
			verificheFunzMisureSettoreCellaBean.setDescElemento(arraySettoreCella.get(0).getDescElemento());
		} 
		String settoreCella;

		for (ViwVerFunzMisureEricsson viwVerFunzMisureEricsson2 : arraySettoreCella) {
			if(viwVerFunzMisureEricsson2.getNumeSettore()!=null && viwVerFunzMisureEricsson2.getNumeCella()!=null){
				settoreCella=viwVerFunzMisureEricsson2.getNumeSettore().toString()+viwVerFunzMisureEricsson2.getNumeCella().toString();

				switch (settoreCella) {
				case "11":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "21":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "31":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "41":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "51":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "61":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella1(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella1(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "12":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "22":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "32":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "42":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "52":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "62":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella2(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella2(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "13":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore1Cella3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "23":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore2Cella3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "33":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore3Cella3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "43":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore4Cella3(viwVerFunzMisureEricsson2.getDescValore());
					break;
				case "53":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore5Cella3(viwVerFunzMisureEricsson2.getDescValore());	
					break;
				case "63":
					if(viwVerFunzMisureEricsson2.getDescLabelLov()!=null)
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella3(viwVerFunzMisureEricsson2.getDescLabelLov());
					else
						verificheFunzMisureSettoreCellaBean.setDescSettore6Cella3(viwVerFunzMisureEricsson2.getDescValore());
					break;


				}
			}
		}

		List<VerificheFunzMisureSettoreCellaBean> arrayEricssonMisureSettoreCellaBean = new ArrayList<VerificheFunzMisureSettoreCellaBean>();
		arrayEricssonMisureSettoreCellaBean.add(verificheFunzMisureSettoreCellaBean);


		return arrayEricssonMisureSettoreCellaBean;

	}

	//	 public static List<VerificheFunzMisureSettoreBean> getArrayEricssonMisureSettoreBean(List<ViwVerFunzMisureEricsson> arraySettore) {
	//		 List<VerificheFunzMisureSettoreBean> arraySettoreBean = new ArrayList<VerificheFunzMisureSettoreBean>();
	//		 
	//		 ViwVerFunzMisureEricsson  viwVerFunzMisureEricsson;
	//		 for (int i = 0; i < arraySettore.size(); i++) {
	//			 viwVerFunzMisureEricsson= arraySettore.get(i);
	//			 VerificheFunzMisureSettoreBean verificheFunzMisureCanaliSettoreBean = new VerificheFunzMisureSettoreBean();
	//			 verificheFunzMisureCanaliSettoreBean.setDescElemento(viwVerFunzMisureEricsson.getDescElemento());
	//			 verificheFunzMisureCanaliSettoreBean.setDescCaratteristica(viwVerFunzMisureEricsson.getDescCaratteristica());
	//			 verificheFunzMisureCanaliSettoreBean.setDescNote(viwVerFunzMisureEricsson.getDescNote());
	//			 Integer codiCaratteristicaAppo=viwVerFunzMisureEricsson.getCodiCaratteristica();
	//			 while (codiCaratteristicaAppo.equals(viwVerFunzMisureEricsson.getCodiCaratteristica())&& i<arraySettore.size()) {
	//				if(viwVerFunzMisureEricsson.getNumeSettore()!=null)
	//				 switch (viwVerFunzMisureEricsson.getNumeSettore()) {
	//				case 1:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore1(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore1(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				case 2:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore2(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore2(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				case 3:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore3(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore3(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				case 4:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore4(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore4(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				case 5:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore5(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore5(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				case 6:
	//					if(viwVerFunzMisureEricsson.getDescLabelLov()!=null)
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore6(viwVerFunzMisureEricsson.getDescLabelLov());
	//					else
	//						verificheFunzMisureCanaliSettoreBean.setDescSettore6(viwVerFunzMisureEricsson.getDescValore());
	//					break;
	//				}
	//				 i++;
	//				 if(i<arraySettore.size())
	//					 viwVerFunzMisureEricsson= arraySettore.get(i);
	//			}
	//			 arraySettoreBean.add(verificheFunzMisureCanaliSettoreBean);
	//			 i--;
	//		 }
	//		 
	//		 return arraySettoreBean;
	//		
	//	}

	public static List<VerificheGenCelleDoppiaLabelBean> getArrayVerificheGenCelleDoppiaLabel(List<ViwVerGenRadA> arrayCelleLabel){

		List<VerificheGenCelleDoppiaLabelBean> arrayCelleLabelBean = new ArrayList<VerificheGenCelleDoppiaLabelBean>();

		ViwVerGenRadA  viwVerGenRadA;
		for (int i = 0; i < arrayCelleLabel.size(); i++) {
			viwVerGenRadA= arrayCelleLabel.get(i);
			VerificheGenCelleDoppiaLabelBean verificheGenCelleDoppiaLabelBean = new VerificheGenCelleDoppiaLabelBean();
			verificheGenCelleDoppiaLabelBean.setDescElemento(viwVerGenRadA.getDescElemento());
			verificheGenCelleDoppiaLabelBean.setDescCaratteristica(viwVerGenRadA.getDescCaratteristica());
			Integer codiCaratteristicaAppo=viwVerGenRadA.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerGenRadA.getCodiCaratteristica())&& i<arrayCelleLabel.size()) {
				if(viwVerGenRadA.getNumeCella()!=null)
					switch (viwVerGenRadA.getNumeCella()) {
					case 1:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella1Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella1Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella1Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella1Label2(viwVerGenRadA.getDescValore());
						break;
					case 2:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella2Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella2Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella2Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella2Label2(viwVerGenRadA.getDescValore());
						break;
					case 3:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella3Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella3Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella3Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella3Label2(viwVerGenRadA.getDescValore());
						break;
					case 4:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella4Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella4Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella4Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella4Label2(viwVerGenRadA.getDescValore());
						break;
					case 5:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella5Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella5Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella5Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella5Label2(viwVerGenRadA.getDescValore());
						break;
					case 6:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
								verificheGenCelleDoppiaLabelBean.setDescCella6Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenCelleDoppiaLabelBean.setDescCella6Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Check"))
							verificheGenCelleDoppiaLabelBean.setDescCella6Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenCelleDoppiaLabelBean.setDescCella6Label2(viwVerGenRadA.getDescValore());
						break;
					}
				i++;
				if(i<arrayCelleLabel.size())
					viwVerGenRadA= arrayCelleLabel.get(i);
			}
			arrayCelleLabelBean.add(verificheGenCelleDoppiaLabelBean);
			i--;
		}

		return arrayCelleLabelBean;



	}

	public static List<VerificheGenRadiantiABean> getArrayVerificheGenRadiantiA(List<ViwVerGenRadA> arrayCelleLabel){

		List<VerificheGenRadiantiABean> arraylBean = new ArrayList<VerificheGenRadiantiABean>();
		HashMap<Integer, String> hashMapCheck = new HashMap<>();
		ArrayList<ViwVerGenRadA> viwVerGenRadAAppo = new ArrayList<>();
		for (ViwVerGenRadA viwVerGenRadA : arrayCelleLabel) {
			if(viwVerGenRadA.getDescCaratteristica().equalsIgnoreCase("Check")){
				hashMapCheck.put(viwVerGenRadA.getCodiElemento(), viwVerGenRadA.getDescLabelLov());
				viwVerGenRadAAppo.add(viwVerGenRadA);
			}
		}
		arrayCelleLabel.removeAll(viwVerGenRadAAppo);
		ViwVerGenRadA  viwVerGenRadA;
		for (int i = 0; i < arrayCelleLabel.size(); i++) {
			viwVerGenRadA= arrayCelleLabel.get(i);
			VerificheGenRadiantiABean verificheGenRadiantiABean = new VerificheGenRadiantiABean();
			verificheGenRadiantiABean.setDescElemento(viwVerGenRadA.getDescElemento());
			verificheGenRadiantiABean.setDescCaratteristica(viwVerGenRadA.getDescCaratteristica());
			verificheGenRadiantiABean.setCodiElemento(viwVerGenRadA.getCodiElemento());
			boolean flagCheck=false;
			if(hashMapCheck.get(arrayCelleLabel.get(i).getCodiElemento())!=null && hashMapCheck.get(arrayCelleLabel.get(i).getCodiElemento()).equals("SI")){
				flagCheck=true;
			}
			verificheGenRadiantiABean.setFlagCheck(flagCheck);

			Integer codiCaratteristicaAppo=viwVerGenRadA.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerGenRadA.getCodiCaratteristica())&& i<arrayCelleLabel.size()) {
				if(viwVerGenRadA.getNumeCella()!=null)
					switch (viwVerGenRadA.getNumeCella()) {
					case 1:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella1Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella1Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella1Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella1Label2(viwVerGenRadA.getDescValore());
						break;
					case 2:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella2Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella2Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella2Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella2Label2(viwVerGenRadA.getDescValore());
						break;
					case 3:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella3Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella3Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella3Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella3Label2(viwVerGenRadA.getDescValore());
						break;
					case 4:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella4Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella4Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella4Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella4Label2(viwVerGenRadA.getDescValore());
						break;
					case 5:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella5Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella5Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella5Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella5Label2(viwVerGenRadA.getDescValore());
						break;
					case 6:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenRadiantiABean.setDescCella6Label1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenRadiantiABean.setDescCella6Label2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenRadiantiABean.setDescCella6Label1(viwVerGenRadA.getDescValore());
						else
							verificheGenRadiantiABean.setDescCella6Label2(viwVerGenRadA.getDescValore());
						break;
					}
				i++;
				if(i<arrayCelleLabel.size())
					viwVerGenRadA= arrayCelleLabel.get(i);
			}
			arraylBean.add(verificheGenRadiantiABean);
			i--;
		}

		return arraylBean;



	}

	public static List<VerificheGenValoreAttesoRadiantiABean> getArrayVerificheGenValoreAttesoRadiantiA(List<ViwVerGenRadA> arrayCelleLabel){

		List<VerificheGenValoreAttesoRadiantiABean> arraylBean = new ArrayList<VerificheGenValoreAttesoRadiantiABean>();
		HashMap<Integer, String> hashMapCheck = new HashMap<>();
		ArrayList<ViwVerGenRadA> viwVerGenRadAAppo = new ArrayList<>();
		for (ViwVerGenRadA viwVerGenRadA : arrayCelleLabel) {
			if(viwVerGenRadA.getDescCaratteristica().equalsIgnoreCase("Check")){
				hashMapCheck.put(viwVerGenRadA.getCodiElemento(), viwVerGenRadA.getDescLabelLov());
				viwVerGenRadAAppo.add(viwVerGenRadA);
			}
		}
		arrayCelleLabel.removeAll(viwVerGenRadAAppo);
		ViwVerGenRadA  viwVerGenRadA;
		for (int i = 0; i < arrayCelleLabel.size(); i++) {
			viwVerGenRadA= arrayCelleLabel.get(i);
			VerificheGenValoreAttesoRadiantiABean verificheGenValoreAttesoRadiantiABean = new VerificheGenValoreAttesoRadiantiABean();
			verificheGenValoreAttesoRadiantiABean.setDescElemento(viwVerGenRadA.getDescElemento());
			verificheGenValoreAttesoRadiantiABean.setDescCaratteristica(viwVerGenRadA.getDescCaratteristica());
			verificheGenValoreAttesoRadiantiABean.setCodiElemento(viwVerGenRadA.getCodiElemento());
			boolean flagCheck=false;
			if(hashMapCheck.get(arrayCelleLabel.get(i).getCodiElemento())!=null && hashMapCheck.get(arrayCelleLabel.get(i).getCodiElemento()).equals("SI")){
				flagCheck=true;
			}
			verificheGenValoreAttesoRadiantiABean.setFlagCheck(flagCheck);

			Integer codiCaratteristicaAppo=viwVerGenRadA.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerGenRadA.getCodiCaratteristica())&& i<arrayCelleLabel.size()) {
				if(viwVerGenRadA.getNumeCella()!=null)
					switch (viwVerGenRadA.getNumeCella()) {
					case 1:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella1Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella1Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella1Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella1Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella1Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella1Label3(viwVerGenRadA.getDescValore());
						break;
					case 2:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella2Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella2Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella2Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella2Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella2Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella2Label3(viwVerGenRadA.getDescValore());

						break;
					case 3:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella3Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella3Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella3Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella3Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella3Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella3Label3(viwVerGenRadA.getDescValore());

						break;
					case 4:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella4Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella4Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella4Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella4Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella4Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella4Label3(viwVerGenRadA.getDescValore());

						break;
					case 5:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella5Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella5Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella5Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella5Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella5Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella5Label3(viwVerGenRadA.getDescValore());

						break;
					case 6:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
								verificheGenValoreAttesoRadiantiABean.setDescCella6Label1(viwVerGenRadA.getDescLabelLov());
							else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
								verificheGenValoreAttesoRadiantiABean.setDescCella6Label2(viwVerGenRadA.getDescLabelLov());
							else 
								verificheGenValoreAttesoRadiantiABean.setDescCella6Label3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore rilevato"))
							verificheGenValoreAttesoRadiantiABean.setDescCella6Label1(viwVerGenRadA.getDescValore());
						else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Valore atteso"))
							verificheGenValoreAttesoRadiantiABean.setDescCella6Label2(viwVerGenRadA.getDescValore());
						else  verificheGenValoreAttesoRadiantiABean.setDescCella6Label3(viwVerGenRadA.getDescValore());

						break;
					}
				i++;
				if(i<arrayCelleLabel.size())
					viwVerGenRadA= arrayCelleLabel.get(i);
			}
			arraylBean.add(verificheGenValoreAttesoRadiantiABean);
			i--;
		}

		return arraylBean;



	}



	public static List<VerificheGenChainingRETRadiantiABean> getArrayChainingRETRadiantiBean(List<ViwVerGenRadA> arrayCelle){

		List<VerificheGenChainingRETRadiantiABean> arraylBean = new ArrayList<VerificheGenChainingRETRadiantiABean>();
		HashMap<Integer, String> hashMapCheck = new HashMap<>();
		ArrayList<ViwVerGenRadA> viwVerGenRadAAppo = new ArrayList<>();
		for (ViwVerGenRadA viwVerGenRadA : arrayCelle) {
			if(viwVerGenRadA.getDescCaratteristica().equalsIgnoreCase("Check")){
				hashMapCheck.put(viwVerGenRadA.getCodiElemento(), viwVerGenRadA.getDescLabelLov());
				viwVerGenRadAAppo.add(viwVerGenRadA);
			}
		}
		arrayCelle.removeAll(viwVerGenRadAAppo);
		ViwVerGenRadA  viwVerGenRadA;
		for (int i = 0; i < arrayCelle.size(); i++) {
			viwVerGenRadA= arrayCelle.get(i);
			VerificheGenChainingRETRadiantiABean verificheGenChainingRETRadiantiABean  = new VerificheGenChainingRETRadiantiABean();
			verificheGenChainingRETRadiantiABean.setDescElemento(viwVerGenRadA.getDescElemento());
			verificheGenChainingRETRadiantiABean.setDescCaratteristica(viwVerGenRadA.getDescCaratteristica());
			verificheGenChainingRETRadiantiABean.setCodiElemento(viwVerGenRadA.getCodiElemento());
			boolean flagCheck=false;
			if(hashMapCheck.get(arrayCelle.get(i).getCodiElemento())!=null && hashMapCheck.get(arrayCelle.get(i).getCodiElemento()).equals("SI")){
				flagCheck=true;
			}
			verificheGenChainingRETRadiantiABean.setFlagCheck(flagCheck);

			Integer codiCaratteristicaAppo=viwVerGenRadA.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwVerGenRadA.getCodiCaratteristica())&& i<arrayCelle.size()) {
				if(viwVerGenRadA.getNumeCella()!=null)
					switch (viwVerGenRadA.getNumeCella()) {
					case 1:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella1(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella1(viwVerGenRadA.getDescValore());
						break;
					case 2:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella2(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella2(viwVerGenRadA.getDescValore());
						break;
					case 3:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella3(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella3(viwVerGenRadA.getDescValore());
						break;
					case 4:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella4(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella4(viwVerGenRadA.getDescValore());
						break;
					case 5:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella5(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella5(viwVerGenRadA.getDescValore());
						break;
					case 6:
						if(viwVerGenRadA.getDescLabelLov()!=null){
							if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
								verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescLabelLov());
							else
								verificheGenChainingRETRadiantiABean.setCella6(viwVerGenRadA.getDescLabelLov());
						}else if(viwVerGenRadA.getDescLabel().equalsIgnoreCase("Sistema"))
							verificheGenChainingRETRadiantiABean.setDescLabel1(viwVerGenRadA.getDescValore());
						else
							verificheGenChainingRETRadiantiABean.setCella6(viwVerGenRadA.getDescValore());
						break;
					}
				i++;
				if(i<arrayCelle.size())
					viwVerGenRadA= arrayCelle.get(i);
			}
			arraylBean.add(verificheGenChainingRETRadiantiABean);
			i--;
		}

		return arraylBean;

	}

	public static List<MisureDiCaratterizzazioneCondivisioneCalataBean> getArrayRadiantiBCondivisioneCalataBean(List<ViwMisureGenRadB> arrayCelle){

		MisureDiCaratterizzazioneCondivisioneCalataBean calataBean = new MisureDiCaratterizzazioneCondivisioneCalataBean();
		ArrayList<MisureDiCaratterizzazioneCondivisioneCalataBean> arrayCalataBean = new ArrayList<>();
		for (ViwMisureGenRadB viwMisureGenRadB : arrayCelle) {
			if(viwMisureGenRadB.getNumeCella()!=null){
				if(viwMisureGenRadB.getCodiCaratteristica()==ConstReport.InfoCelleB){

					switch (viwMisureGenRadB.getNumeCella()) {
					case 1:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella1flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					case 2:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella2flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					case 3:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella3flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					case 4:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella4flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					case 5:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella5flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					case 6:
						if(viwMisureGenRadB.getDescLabelLov()!=null)
							calataBean.setCella6flgCheck(viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI"));
						break;
					}

				}else{
					switch (viwMisureGenRadB.getNumeCella()) {
					case 1:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella1Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella1Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella1Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella1Via23(viwMisureGenRadB.getDescValore());
						break;
					case 2:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella2Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella2Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella2Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella2Via23(viwMisureGenRadB.getDescValore());
						break;
					case 3:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella3Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella3Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella3Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella3Via23(viwMisureGenRadB.getDescValore());
						break;
					case 4:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella4Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella4Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella4Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella4Via23(viwMisureGenRadB.getDescValore());
						break;
					case 5:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella5Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella5Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella5Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella5Via23(viwMisureGenRadB.getDescValore());
						break;

					case 6:
						if(viwMisureGenRadB.getDescLabelLov()!=null){
							if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
								calataBean.setCella6Via01(viwMisureGenRadB.getDescLabelLov());
							else
								calataBean.setCella6Via23(viwMisureGenRadB.getDescLabelLov());
						}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Via 0-1"))
							calataBean.setCella6Via01(viwMisureGenRadB.getDescValore());
						else
							calataBean.setCella6Via23(viwMisureGenRadB.getDescValore());
						break;
					}
				}

			}
		}
		arrayCalataBean.add(calataBean);
		return arrayCalataBean;

	}

	public static List<MisureDiCaratterizzazioneGeneraliRadiantiBBean> getArrayRadiantiBMisureGeneraliBean(List<ViwMisureGenRadB> arrayCelle){
		ArrayList<MisureDiCaratterizzazioneGeneraliRadiantiBBean> arrayMisureBean = new ArrayList<>();
		HashMap<Integer, String> hashMapCheck = new HashMap<>();
		ArrayList<ViwMisureGenRadB> viwMisureGenAppo = new ArrayList<>();
		for (ViwMisureGenRadB viwMisureGenRadB : arrayCelle) {
			if(viwMisureGenRadB.getDescCaratteristica().equalsIgnoreCase("Check")){
				// if(viwMisureGenRadB.getDescLabelLov()!=null){
				hashMapCheck.put(viwMisureGenRadB.getCodiElemento(), viwMisureGenRadB.getDescLabelLov());
				viwMisureGenAppo.add(viwMisureGenRadB);
				// }
			}
		}
		arrayCelle.removeAll(viwMisureGenAppo);
		String viaCella;
		ViwMisureGenRadB viwMisureGenRadB;
		for (int i=0 ;i<arrayCelle.size() ;i++) {
			viwMisureGenRadB=arrayCelle.get(i);
			MisureDiCaratterizzazioneGeneraliRadiantiBBean misurePimSistemaRadiantiDBean = new MisureDiCaratterizzazioneGeneraliRadiantiBBean();
			misurePimSistemaRadiantiDBean.setDescCaratteristica(viwMisureGenRadB.getDescCaratteristica());
			misurePimSistemaRadiantiDBean.setDescElemento(viwMisureGenRadB.getDescElemento());
			misurePimSistemaRadiantiDBean.setCodiElemento(viwMisureGenRadB.getCodiElemento());
			if(hashMapCheck.get(viwMisureGenRadB.getCodiElemento())!=null && hashMapCheck.get(viwMisureGenRadB.getCodiElemento()).equalsIgnoreCase("SI") ){
				misurePimSistemaRadiantiDBean.setFlgCheck("X");
			}else{
				misurePimSistemaRadiantiDBean.setFlgCheck("");
			}
			Integer codiCaratteristicaAppo=viwMisureGenRadB.getCodiCaratteristica();
			if(viwMisureGenRadB.getDescLabel()!=null && viwMisureGenRadB.getCodiElemento().equals(155)){
				if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
					misurePimSistemaRadiantiDBean.setDescLabel(viwMisureGenRadB.getDescValore());
			}
			while (codiCaratteristicaAppo.equals(viwMisureGenRadB.getCodiCaratteristica())&& i<arrayCelle.size()) {
				if(viwMisureGenRadB.getNumeVia()!=null && viwMisureGenRadB.getNumeCella()!=null){
					viaCella=viwMisureGenRadB.getNumeVia().toString()+viwMisureGenRadB.getNumeCella().toString();

					switch (viaCella) {
					case "11":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella1(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella1(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme1(viwMisureGenRadB.getDescLabelLov());
						break;
					case "21":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella1(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella1(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureGenRadB.getDescLabelLov());
						break;
					case "31":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella1(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella1(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureGenRadB.getDescLabelLov());
						break;
					case "41":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella1(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella1(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureGenRadB.getDescLabelLov());
						break;
					case "12":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella2(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella2(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme2(viwMisureGenRadB.getDescLabelLov());
						break;
					case "22":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella2(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella2(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureGenRadB.getDescLabelLov());
						break;
					case "32":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella2(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella2(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureGenRadB.getDescLabelLov());
						break;
					case "42":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella2(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella2(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureGenRadB.getDescLabelLov());
						break;
					case "13":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella3(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella3(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme3(viwMisureGenRadB.getDescLabelLov());
						break;
					case "23":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella3(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella3(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureGenRadB.getDescLabelLov());
						break;
					case "33":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella3(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella3(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureGenRadB.getDescLabelLov());
						break;
					case "43":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella3(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella3(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureGenRadB.getDescLabelLov());
						break;
					case "14":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella4(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella4(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme4(viwMisureGenRadB.getDescLabelLov());
						break;
					case "24":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella4(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella4(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureGenRadB.getDescLabelLov());
						break;
					case "34":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella4(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella4(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureGenRadB.getDescLabelLov());
						break;
					case "44":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella4(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella4(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureGenRadB.getDescLabelLov());
						break;
					case "15":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella5(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella5(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme5(viwMisureGenRadB.getDescLabelLov());
						break;
					case "25":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella5(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella5(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureGenRadB.getDescLabelLov());
						break;
					case "35":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella5(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella5(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureGenRadB.getDescLabelLov());
						break;
					case "45":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella5(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella5(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureGenRadB.getDescLabelLov());
						break;
					case "16":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella6(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella6(viwMisureGenRadB.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme6(viwMisureGenRadB.getDescLabelLov());
						break;
					case "26":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella6(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella6(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureGenRadB.getDescLabelLov());
						break;
					case "36":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella6(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella6(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureGenRadB.getDescLabelLov());
						break;
					case "46":
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureGenRadB.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella6(viwMisureGenRadB.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella6(viwMisureGenRadB.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureGenRadB.getDescLabelLov());
						break;
					}

				}
				i++;
				if(i<arrayCelle.size())
					viwMisureGenRadB= arrayCelle.get(i);
			}
			arrayMisureBean.add(misurePimSistemaRadiantiDBean);
			i--;
		}
		return arrayMisureBean;

	}


	public static List<MisureDiCaratterizzazioneTipoCavoRadiantiBBean> getArrayRadiantiBMisureTipoCavoBean(List<ViwMisureGenRadB> arrayCelleLabel){

		List<MisureDiCaratterizzazioneTipoCavoRadiantiBBean> arrayCelleLabelBean = new ArrayList<MisureDiCaratterizzazioneTipoCavoRadiantiBBean>();
		String check="";
		ViwMisureGenRadB viwMisureGenRadBAppo= new ViwMisureGenRadB();
		for (ViwMisureGenRadB viwMisureGenRadB : arrayCelleLabel) {
			if(viwMisureGenRadB.getDescCaratteristica().equalsIgnoreCase("Check") && viwMisureGenRadB.getDescLabelLov()!=null && viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI")){
				viwMisureGenRadBAppo=viwMisureGenRadB;
				check="X";
			}
		}
		arrayCelleLabel.remove(viwMisureGenRadBAppo);
		ViwMisureGenRadB  viwMisureGenRadB=arrayCelleLabel.get(0);
		MisureDiCaratterizzazioneTipoCavoRadiantiBBean misureDiCaratterizzazioneTipoCavoRadiantiBBean = new MisureDiCaratterizzazioneTipoCavoRadiantiBBean();
		misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescElemento(viwMisureGenRadB.getDescElemento());
		misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCaratteristica(viwMisureGenRadB.getDescCaratteristica());
		misureDiCaratterizzazioneTipoCavoRadiantiBBean.setCodiElemento(viwMisureGenRadB.getCodiElemento());
		misureDiCaratterizzazioneTipoCavoRadiantiBBean.setFlgCheck(check);

		for (int i = 0; i < arrayCelleLabel.size(); i++) {
			viwMisureGenRadB= arrayCelleLabel.get(i);

			if(viwMisureGenRadB.getNumeCella()!=null)
				switch (viwMisureGenRadB.getNumeCella()) {
				case 1:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella1Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella1Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella1Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella1Label2(viwMisureGenRadB.getDescValore());
					break;
				case 2:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella2Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella2Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella2Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella2Label2(viwMisureGenRadB.getDescValore());
					break;
				case 3:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella3Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella3Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella3Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella3Label2(viwMisureGenRadB.getDescValore());
					break;
				case 4:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella4Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella4Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella4Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella4Label2(viwMisureGenRadB.getDescValore());
					break;
				case 5:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella5Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella5Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella5Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella5Label2(viwMisureGenRadB.getDescValore());
					break;
				case 6:
					if(viwMisureGenRadB.getDescLabelLov()!=null){
						if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella6Label1(viwMisureGenRadB.getDescLabelLov());
						else
							misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella6Label2(viwMisureGenRadB.getDescLabelLov());
					}else if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella6Label1(viwMisureGenRadB.getDescValore());
					else
						misureDiCaratterizzazioneTipoCavoRadiantiBBean.setDescCella6Label2(viwMisureGenRadB.getDescValore());
					break;
				}




		}
		arrayCelleLabelBean.add(misureDiCaratterizzazioneTipoCavoRadiantiBBean);
		return arrayCelleLabelBean;



	}

	public static List<ConfigurazioneCelleRadiantiCBean> getArrayConfCelleBean(List<ViwConfGenRadC> arrayCelleLabel) {
		ArrayList<ConfigurazioneCelleRadiantiCBean> arrayListBean = new ArrayList<>();
		ConfigurazioneCelleRadiantiCBean bean = new ConfigurazioneCelleRadiantiCBean();
		for (ViwConfGenRadC viwConfGenRadC : arrayCelleLabel) {
			if(viwConfGenRadC.getNumeCella()!=null){
				String check="";
				if(viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI"))
					check="X";
				switch (viwConfGenRadC.getNumeCella()) {
				case 1:
					bean.setCheckCella1(check);
					break;
				case 2:
					bean.setCheckCella2(check);
					break;
				case 3:
					bean.setCheckCella3(check);
					break;
				case 4:
					bean.setCheckCella4(check);
					break;
				case 5:
					bean.setCheckCella5(check);
					break;
				case 6:
					bean.setCheckCella6(check);
					break;
				}

			}
		}
		arrayListBean.add(bean);
		return arrayListBean;

	}


	public static List<AntenneInstallateRadiantiCBean> getArrayAntenneInstallateBean(List<ViwConfGenRadC> arrayAntenneInstallate) {



		AntenneInstallateRadiantiCBean modello1Bean = new AntenneInstallateRadiantiCBean();
		AntenneInstallateRadiantiCBean modello2Bean = new AntenneInstallateRadiantiCBean();
		AntenneInstallateRadiantiCBean modello3Bean = new AntenneInstallateRadiantiCBean();
		AntenneInstallateRadiantiCBean modello4Bean = new AntenneInstallateRadiantiCBean();


		for (ViwConfGenRadC viwConfGenRadC : arrayAntenneInstallate) {


			if(viwConfGenRadC.getNumeCella()!=null && viwConfGenRadC.getDescLabel()!=null){


				switch (viwConfGenRadC.getNumeCella()) {

				case 1:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella1(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella1(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella1(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella1(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella1(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella1(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella1(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella1(viwConfGenRadC.getDescValore());	
					}
					break;

				case 2:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella2(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella2(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella2(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella2(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella2(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella2(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella2(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella2(viwConfGenRadC.getDescValore());	
					}
					break;

				case 3:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella3(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella3(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella3(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella3(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella3(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella3(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella3(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella3(viwConfGenRadC.getDescValore());	
					}
					break;

				case 4:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella4(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella4(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella4(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella4(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella4(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella4(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella4(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella4(viwConfGenRadC.getDescValore());	
					}
					break;

				case 5:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella5(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella5(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella5(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella5(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella5(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella5(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella5(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella5(viwConfGenRadC.getDescValore());	
					}
					break;

				case 6:
					if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 1")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello1Bean.setCella6(viwConfGenRadC.getDescLabelLov());
						else
							modello1Bean.setCella6(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 2")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello2Bean.setCella6(viwConfGenRadC.getDescLabelLov());
						else
							modello2Bean.setCella6(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 3")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello3Bean.setCella6(viwConfGenRadC.getDescLabelLov());
						else
							modello3Bean.setCella6(viwConfGenRadC.getDescValore());
					}else if(viwConfGenRadC.getDescLabel().equalsIgnoreCase("Modello 4")){
						if(viwConfGenRadC.getDescLabelLov()!=null)
							modello4Bean.setCella6(viwConfGenRadC.getDescLabelLov());
						else
							modello4Bean.setCella6(viwConfGenRadC.getDescValore());	
					}
					break;
				}

			}
		}
		ArrayList<AntenneInstallateRadiantiCBean> arrayListBean = new ArrayList<>();
		arrayListBean.add(modello1Bean);
		arrayListBean.add(modello2Bean);
		arrayListBean.add(modello3Bean);
		arrayListBean.add(modello4Bean);

		return arrayListBean;

	}

	public static List<DettaglioTecnologieRadiantiCBean> getArrayDettaglioTecnologieBean(List<ViwConfGenRadC> arrayDettaglioTecnologie) {


		//divido i record della vista in due array: uno con le caratteristiche di Dettaglio tecnologie installate e uno con le caratteristiche di configurazione ret in cascata
		ArrayList<ViwConfGenRadC> arrayDettaglioTecnologieInstallate =  new ArrayList<>(); 
		ArrayList<ViwConfGenRadC> arrayDettaglioTecnologieInCascata =  new ArrayList<>(); 
		for (ViwConfGenRadC viwConfGenRadC : arrayDettaglioTecnologie) {

			if (viwConfGenRadC.getCodiElemento()==ConstReport.ElementoDettagliTecnologieInstallate) {
				arrayDettaglioTecnologieInstallate.add(viwConfGenRadC);
			} else {
				arrayDettaglioTecnologieInCascata.add(viwConfGenRadC);
			}

		}

		//creo una hash map con chiave il nome della caratteristica e valore il bean relativo a questa
		HashMap<String, DettaglioTecnologieRadiantiCBean> hashMap = new HashMap<>();
		for (ViwConfGenRadC viwConfGenRadC : arrayDettaglioTecnologieInstallate) {
			if (!hashMap.containsKey(viwConfGenRadC.getDescCaratteristica())) {
				hashMap.put(viwConfGenRadC.getDescCaratteristica(),new DettaglioTecnologieRadiantiCBean());
				hashMap.get(viwConfGenRadC.getDescCaratteristica()).setDescCaratteristica(viwConfGenRadC.getDescCaratteristica());
			}
		}

		//ora che ho l'hash map con chiavi le caratteristiche vado a settare i campi del bean. Prima mi giro i record di dettaglio tecnologie installate
		for (ViwConfGenRadC viwConfGenRadC : arrayDettaglioTecnologieInstallate) {
			if (viwConfGenRadC.getDescLabel()!=null) {
				switch (viwConfGenRadC.getDescLabel()) {
				case "Sharing RF":
					if (viwConfGenRadC.getDescLabelLov()!=null) {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSharingRF(viwConfGenRadC.getDescLabelLov());
					} else {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSharingRF(viwConfGenRadC.getDescValore());
					}
					break;
				case "RET in cascata":
					if (viwConfGenRadC.getDescLabelLov()!=null) {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setRetInCascata(viwConfGenRadC.getDescLabelLov());
					} else {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setRetInCascata(viwConfGenRadC.getDescValore());
					}
					break;
				case "MIMO 4":
					if (viwConfGenRadC.getDescLabelLov()!=null) {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setMimo4(viwConfGenRadC.getDescLabelLov());
					} else {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setMimo4(viwConfGenRadC.getDescValore());
					}
					break;
				case "Master":
					if (viwConfGenRadC.getDescLabelLov()!=null) {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setMaster(viwConfGenRadC.getDescLabelLov());
					} else {
						hashMap.get(viwConfGenRadC.getDescCaratteristica()).setMaster(viwConfGenRadC.getDescValore());
					}
					break;
				}

			}
		}

		//ora metto i record ret in cascata
		for (ViwConfGenRadC viwConfGenRadC : arrayDettaglioTecnologieInCascata) {
			if (viwConfGenRadC.getNumeCella()!=null && viwConfGenRadC.getDescLabel()!=null) {
				switch (viwConfGenRadC.getNumeCella()) {
				case 1:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella1("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella1(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella1(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella1(viwConfGenRadC.getDescValore());
						}
					}
					break;
				case 2:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella2("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella2(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella2(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella2(viwConfGenRadC.getDescValore());
						}
					}
					break;
				case 3:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella3("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella3(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella3(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella3(viwConfGenRadC.getDescValore());
						}
					}
					break;
				case 4:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella4("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella4(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella4(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella4(viwConfGenRadC.getDescValore());
						}
					}
					break;
				case 5:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella5("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella5(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella5(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella5(viwConfGenRadC.getDescValore());
						}
					}
					break;
				case 6:
					if (viwConfGenRadC.getDescLabel().equalsIgnoreCase("Check")) {
						if (viwConfGenRadC.getDescLabelLov()!=null && viwConfGenRadC.getDescLabelLov().equalsIgnoreCase("SI")) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella6("X");
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setCheckCella6(" ");
						}
					} else {
						if (viwConfGenRadC.getDescLabelLov()!=null) {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella6(viwConfGenRadC.getDescLabelLov());
						} else {
							hashMap.get(viwConfGenRadC.getDescCaratteristica()).setSerieRetCella6(viwConfGenRadC.getDescValore());
						}
					}
					break;
				}
			}
		}

		ArrayList<DettaglioTecnologieRadiantiCBean> arrayListBean = new ArrayList<>(hashMap.values());
		return arrayListBean;


	}

	public static List<CelleRadiantiDBean> getArrayCelleRadiantiDBean(List<ViwMisureDiPimRadD> arrayCelleRadD) {

		//Inizializzo l'unico bean che popoler� i campi del report
		CelleRadiantiDBean celleRadiantiBean = new CelleRadiantiDBean();

		//ora devo settare i campi del bean, ci saranno due casi: uno quando a caratteristica ha codice 828 che ha determinati campi (check, tipo,stato), l'altro quando la
		//caratteristica ha codice 758 e prendo altri campi (condivisione calata).
		for (ViwMisureDiPimRadD viwMisureDiPimRadD:  arrayCelleRadD) {

			if (viwMisureDiPimRadD.getDescLabel()!=null && viwMisureDiPimRadD.getNumeCella()!=null && viwMisureDiPimRadD.getNumeCella()!=null && viwMisureDiPimRadD.getDescLabelLov()!=null) {

				if (viwMisureDiPimRadD.getCodiCaratteristica()==828) {

					switch (viwMisureDiPimRadD.getNumeCella()) {
					case 1:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella1("X");
							} else {
								celleRadiantiBean.setCheckCella1("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella1(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella1(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella1(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella1(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 2:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella2("X");
							} else {
								celleRadiantiBean.setCheckCella2("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella2(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella2(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella2(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella2(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 3:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella3("X");
							} else {
								celleRadiantiBean.setCheckCella3("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella3(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella3(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella3(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella3(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 4:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella4("X");
							} else {
								celleRadiantiBean.setCheckCella4("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella4(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella4(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella4(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella4(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 5:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella5("X");
							} else {
								celleRadiantiBean.setCheckCella5("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella5(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella5(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella5(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella5(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 6:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Check")) { 
							if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
								celleRadiantiBean.setCheckCella6("X");
							} else {
								celleRadiantiBean.setCheckCella6("");
							}
						} else if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Tipo")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setTipoCella6(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setTipoCella6(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setStatoCella6(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setStatoCella6(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;

					}		
				} else  { //altrimenti se la codi caratteristica � la 758
					switch (viwMisureDiPimRadD.getNumeCella()) {

					case 1:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella1(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella1(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella1(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella1(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 2:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella2(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella2(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella2(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella2(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 3:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella3(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella3(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella3(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella3(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 4:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella4(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella4(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella4(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella4(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 5:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella5(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella5(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella5(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella5(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;
					case 6:
						if (viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Via 0-1")) {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia01Cella6(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia01Cella6(viwMisureDiPimRadD.getDescValore());
							}
						} else {
							if (viwMisureDiPimRadD.getDescLabelLov()!=null) {
								celleRadiantiBean.setVia23Cella6(viwMisureDiPimRadD.getDescLabelLov());
							} else {
								celleRadiantiBean.setVia23Cella6(viwMisureDiPimRadD.getDescValore());
							}
						}
						break;


					}
				} 

			}
		}

		ArrayList<CelleRadiantiDBean> arrayCelle = new ArrayList<>();
		arrayCelle.add(celleRadiantiBean);
		return   arrayCelle;



	}


	public static List<MisurePimSistemaRadiantiDBean> getArrayMisurePimSistemaRadiantiDBean(List<ViwMisureDiPimRadD> arrayCelle){
		ArrayList<MisurePimSistemaRadiantiDBean> arrayMisureBean = new ArrayList<>();
		HashMap<Integer, String> hashMapCheck = new HashMap<>();
		ArrayList<ViwMisureDiPimRadD> viwMisureDiPimRadDAppo = new ArrayList<>();
		for (ViwMisureDiPimRadD viwMisureDiPimRadD : arrayCelle) {
			if(viwMisureDiPimRadD.getDescCaratteristica().equalsIgnoreCase("Check")){
				// if(viwMisureGenRadB.getDescLabelLov()!=null){
				hashMapCheck.put(viwMisureDiPimRadD.getCodiElemento(), viwMisureDiPimRadD.getDescLabelLov());
				viwMisureDiPimRadDAppo.add(viwMisureDiPimRadD);
				// }
			}
		}
		arrayCelle.removeAll(viwMisureDiPimRadDAppo);
		String viaCella;
		ViwMisureDiPimRadD viwMisureDiPimRadD;
		for (int i=0 ;i<arrayCelle.size() ;i++) {
			viwMisureDiPimRadD=arrayCelle.get(i);
			MisurePimSistemaRadiantiDBean misurePimSistemaRadiantiDBean = new MisurePimSistemaRadiantiDBean();
			misurePimSistemaRadiantiDBean.setDescCaratteristica(viwMisureDiPimRadD.getDescCaratteristica());
			//			 misurePimSistemaRadiantiDBean.setDescElemento(viwMisureDiPimRadD.getDescElemento());
			misurePimSistemaRadiantiDBean.setCodiElemento(viwMisureDiPimRadD.getCodiElemento());
			if(hashMapCheck.get(viwMisureDiPimRadD.getCodiElemento())!=null && hashMapCheck.get(viwMisureDiPimRadD.getCodiElemento()).equalsIgnoreCase("SI") ){
				misurePimSistemaRadiantiDBean.setFlgCheck("X");
			}else{
				misurePimSistemaRadiantiDBean.setFlgCheck("");
			}
			Integer codiCaratteristicaAppo=viwMisureDiPimRadD.getCodiCaratteristica();
			while (codiCaratteristicaAppo.equals(viwMisureDiPimRadD.getCodiCaratteristica())&& i<arrayCelle.size()) {
				if(viwMisureDiPimRadD.getNumeVia()!=null && viwMisureDiPimRadD.getNumeCella()!=null){
					viaCella=viwMisureDiPimRadD.getNumeVia().toString()+viwMisureDiPimRadD.getNumeCella().toString();

					switch (viaCella) {
					case "11":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella1(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella1(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme1(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "21":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella1(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella1(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "31":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella1(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella1(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "41":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella1(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella1(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme1(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "12":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella2(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella2(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme2(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "22":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella2(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella2(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "32":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella2(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella2(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "42":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella2(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella2(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme2(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "13":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella3(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella3(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme3(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "23":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella3(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella3(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "33":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella3(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella3(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "43":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella3(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella3(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme3(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "14":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella4(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella4(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme4(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "24":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella4(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella4(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "34":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella4(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella4(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "44":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella4(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella4(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme4(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "15":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella5(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella5(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme5(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "25":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella5(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella5(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "35":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella5(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella5(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "45":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella5(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella5(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme5(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "16":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia1Cella6(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia1Cella6(viwMisureDiPimRadD.getDescValore());
						}else
							misurePimSistemaRadiantiDBean.setConforme6(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "26":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia2Cella6(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia2Cella6(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "36":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia3Cella6(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia3Cella6(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureDiPimRadD.getDescLabelLov());
						break;
					case "46":
						if(viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Valore")){
							if(viwMisureDiPimRadD.getDescLabelLov()!=null)
								misurePimSistemaRadiantiDBean.setVia4Cella6(viwMisureDiPimRadD.getDescLabelLov());
							else
								misurePimSistemaRadiantiDBean.setVia4Cella6(viwMisureDiPimRadD.getDescValore());
						}//else
//							misurePimSistemaRadiantiDBean.setConforme6(viwMisureDiPimRadD.getDescLabelLov());
						break;
					}

				}
				i++;
				if(i<arrayCelle.size())
					viwMisureDiPimRadD= arrayCelle.get(i);
			}
			arrayMisureBean.add(misurePimSistemaRadiantiDBean);
			i--;
		}
		return arrayMisureBean;

	}

	public static List<MisureDtpValorePimSistemaRadiantiDBean> getArrayMisuraDtpRadiantiDBean(List<ViwMisureDiPimRadD> arrayMisuraDtpValorePim) {

		//le chavi saranno picco 1, picco 2, picco 3 e le trovo nella colonna desc label dei record della vista
		HashMap<String, MisureDtpValorePimSistemaRadiantiDBean> hashMap = new HashMap<>(); 


		hashMap.put("Picco n 1",new MisureDtpValorePimSistemaRadiantiDBean());
		hashMap.get("Picco n 1").setDescPicco("Picco n 1");

		hashMap.put("Picco n 2",new MisureDtpValorePimSistemaRadiantiDBean());
		hashMap.get("Picco n 2").setDescPicco("Picco n 2");

		hashMap.put("Picco n 3",new MisureDtpValorePimSistemaRadiantiDBean());
		hashMap.get("Picco n 3").setDescPicco("Picco n 3");


		//ora che ho l'hash map con i bean e il picco come chiave setto i valori reltivi a celle e vie

		String viaCella;


		for (ViwMisureDiPimRadD viwMisureDiPimRadD: arrayMisuraDtpValorePim) {

			if (viwMisureDiPimRadD.getNumeCella() != null && viwMisureDiPimRadD.getNumeVia()!=null && !viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Eseguita")) {

				viaCella= viwMisureDiPimRadD.getNumeVia().toString() + viwMisureDiPimRadD.getNumeCella().toString();


				switch (viaCella) {

				case "11": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella1(viwMisureDiPimRadD.getDescValore());
					break;
				case "21": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella1(viwMisureDiPimRadD.getDescValore());
					break;
				case "31": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella1(viwMisureDiPimRadD.getDescValore());
					break;
				case "41": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella1(viwMisureDiPimRadD.getDescValore());
					break;
				case "12": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella2(viwMisureDiPimRadD.getDescValore());
					break;
				case "22": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella2(viwMisureDiPimRadD.getDescValore());
					break;
				case "32": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella2(viwMisureDiPimRadD.getDescValore());
					break;
				case "42": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella2(viwMisureDiPimRadD.getDescValore());
					break;
				case "13": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella3(viwMisureDiPimRadD.getDescValore());
					break;
				case "23": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella3(viwMisureDiPimRadD.getDescValore());
					break;
				case "33": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella3(viwMisureDiPimRadD.getDescValore());
					break;
				case "43": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella3(viwMisureDiPimRadD.getDescValore());
					break;
				case "14": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella4(viwMisureDiPimRadD.getDescValore());
					break;
				case "24": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella4(viwMisureDiPimRadD.getDescValore());
					break;
				case "34": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella4(viwMisureDiPimRadD.getDescValore());
					break;
				case "44": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella4(viwMisureDiPimRadD.getDescValore());
					break;
				case "15": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella5(viwMisureDiPimRadD.getDescValore());
					break;
				case "25": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella5(viwMisureDiPimRadD.getDescValore());
					break;
				case "35": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella5(viwMisureDiPimRadD.getDescValore());
					break;
				case "45": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella5(viwMisureDiPimRadD.getDescValore());
					break;
				case "16": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia1Cella6(viwMisureDiPimRadD.getDescValore());
					break;
				case "26": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia2Cella6(viwMisureDiPimRadD.getDescValore());
					break;
				case "36": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia3Cella6(viwMisureDiPimRadD.getDescValore());
					break;
				case "46": 
					hashMap.get(viwMisureDiPimRadD.getDescLabel()).setVia4Cella6(viwMisureDiPimRadD.getDescValore());
					break;

				}

			}  else if (viwMisureDiPimRadD.getNumeVia() != null && viwMisureDiPimRadD.getDescLabelLov()!=null && viwMisureDiPimRadD.getNumeVia()!=null && viwMisureDiPimRadD.getDescLabel().equalsIgnoreCase("Eseguita") && viwMisureDiPimRadD.getNumeVia()==ConstReport.Via0) {

				for (MisureDtpValorePimSistemaRadiantiDBean bean : hashMap.values()) {
					if (viwMisureDiPimRadD.getDescLabelLov().equalsIgnoreCase("SI")) {
						bean.setFlgCheck("X");
					}


				}


			}
		}
		return new ArrayList<MisureDtpValorePimSistemaRadiantiDBean> (hashMap.values());


	}

	public static List<RiferimentiBean> getArrayRiferimentiBean(List<ViwRiferimentiFirma> arrayRiferimenti) {
		ArrayList<RiferimentiBean> arrayRiferimentiBean = new ArrayList<>();
		RiferimentiBean riferimentiBean = new RiferimentiBean();
		firmaTim=null;
		firmaDitta=null;
		for (ViwRiferimentiFirma viwRiferimentiFirma : arrayRiferimenti) {

			if(viwRiferimentiFirma.getFlgDitta()){
				try {
					if(viwRiferimentiFirma.getObjMediaFirma()!=null){
						setFirmaDitta(ImageIO.read(new ByteArrayInputStream(viwRiferimentiFirma.getObjMediaFirma())));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				riferimentiBean.setDescDitta(viwRiferimentiFirma.getDescDittaInstallatrice());
				riferimentiBean.setDescNomeDitta(viwRiferimentiFirma.getDescNomeDitta());
				riferimentiBean.setTelefonoDitta(viwRiferimentiFirma.getDescTelefonoDitta());
			}else{
				riferimentiBean.setDescNomeTim(viwRiferimentiFirma.getDescNomeDitta());
				riferimentiBean.setTelefonoTim(viwRiferimentiFirma.getDescTelefonoDitta());
				try {
					if(viwRiferimentiFirma.getObjMediaFirma()!=null){
						setFirmaTim(ImageIO.read(new ByteArrayInputStream(viwRiferimentiFirma.getObjMediaFirma())));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		arrayRiferimentiBean.add(riferimentiBean);
		return arrayRiferimentiBean;
	}

	public static BufferedImage getFirmaTim() {
		return firmaTim;
	}

	public static void setFirmaTim(BufferedImage firmaTim) {
		UtilityReports.firmaTim = firmaTim;
	}

	public static BufferedImage getFirmaDitta() {
		return firmaDitta;
	}

	public static void setFirmaDitta(BufferedImage firmaDitta) {
		UtilityReports.firmaDitta = firmaDitta;
	}

	public static List<MisureApogeoCelleBean> getArrayMisureApogeoCelleBean(List<ViwRadiantiE> arrayViwRadiantiE) {

		MisureApogeoCelleBean misureApogeoCelleBean = new MisureApogeoCelleBean(); 


		for (ViwRadiantiE viewRadiantiE : arrayViwRadiantiE ) {

			if ( viewRadiantiE.getCodiCaratteristica()==ConstReport.InfoCelleRadiantiE ) { //se la caratteristica � InfoCelle, setteremo  il check, il macro e lo split

				if (viewRadiantiE.getNumeCella()!=null && viewRadiantiE.getDescLabel()!=null) {

					switch (viewRadiantiE.getNumeCella()) {

					case 1: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella1(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella1(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella1(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;


					case 2: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella2(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella2(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella2(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;


					case 3: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella3(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella3(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella3(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;

					case 4: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella4(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella4(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella4(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;

					case 5: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella5(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella5(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella5(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;

					case 6: 

						if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Check")) {

							misureApogeoCelleBean.setCheckCella6(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else if (viewRadiantiE.getDescLabel().equalsIgnoreCase("Macro")) {

							misureApogeoCelleBean.setMacroCella6(getCheckSign(viewRadiantiE.getDescLabelLov()));

						} else {

							misureApogeoCelleBean.setSplitCella6(getCheckSign(viewRadiantiE.getDescLabelLov()));

						}
						break;
					}


				}

			} else { //altrimenti se la caratteristica � tempo di esecuzione

				if (viewRadiantiE.getNumeCella()!=null && viewRadiantiE.getDescLabel()!=null && viewRadiantiE.getDescLabelLov()!=null) {

					if (viewRadiantiE.getDescLabelLov().equalsIgnoreCase("Durante l'installazione")) {

						misureApogeoCelleBean.setDuranteInstallazione("X");

					}  else {

						misureApogeoCelleBean.setDopoInstallazione("X");

					}


				}


			}


		}

		ArrayList<MisureApogeoCelleBean> listMisureApogeoCelleBean = new ArrayList<>();

		listMisureApogeoCelleBean.add(misureApogeoCelleBean);

		return listMisureApogeoCelleBean;


	}



	private static String getCheckSign (String yesOrNoCheck) {

		if (yesOrNoCheck!=null && yesOrNoCheck.equalsIgnoreCase("SI")) {

			return "X";

		}

		return null;
	}

	public static List<MisureApogeoAntennaBean> getArrayMisureApogeoAntennaBean (List<ViwRadiantiE> arrayViwRadiantiE) {

		ArrayList<MisureApogeoAntennaBean> arrayMisureApogeoAntenna = new ArrayList<> ();

		String check = null;

		ViwRadiantiE viwRadiantiEAppo= null;

		//mi ricavo il check
		for (ViwRadiantiE viwRadiantiE : arrayViwRadiantiE) {

			if (viwRadiantiE.getDescCaratteristica()!=null && viwRadiantiE.getDescCaratteristica().equalsIgnoreCase("Check")) {

				check = getCheckSign(viwRadiantiE.getDescLabelLov());

				viwRadiantiEAppo = viwRadiantiE;
			}
		}
		arrayViwRadiantiE.remove(viwRadiantiEAppo);

		int codiCaratteristicaAppo;

		MisureApogeoAntennaBean misureApogeoAntennaBean;

		ViwRadiantiE viwRadiantiE;

		for (int i=0 ; i< arrayViwRadiantiE.size(); i++) {

			viwRadiantiE = arrayViwRadiantiE.get(i);

			misureApogeoAntennaBean = new MisureApogeoAntennaBean();

			misureApogeoAntennaBean.setCheck(check);

			misureApogeoAntennaBean.setDescCaratteristica(viwRadiantiE.getDescCaratteristica());

			codiCaratteristicaAppo = viwRadiantiE.getCodiCaratteristica();

			while (viwRadiantiE.getCodiCaratteristica()==codiCaratteristicaAppo && i<arrayViwRadiantiE.size()) {

				if (viwRadiantiE.getDescLabel()!=null && viwRadiantiE.getNumeCella()!=null) {

					switch (viwRadiantiE.getNumeCella()) {

					case 1: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella1(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella1(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella1(viwRadiantiE.getDescLabelLov());

						}
						break;

					case 2: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella2(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella2(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella2(viwRadiantiE.getDescLabelLov());

						}
						break;

					case 3: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella3(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella3(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella3(viwRadiantiE.getDescLabelLov());

						}
						break;


					case 4: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella4(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella4(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella4(viwRadiantiE.getDescLabelLov());

						}
						break;

					case 5: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella5(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella5(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella5(viwRadiantiE.getDescLabelLov());

						}
						break;


					case 6: 

						if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore rilevato")) {

							misureApogeoAntennaBean.setValoreRilevatoCella6(viwRadiantiE.getDescValore());

						} else if (viwRadiantiE.getDescLabel().equalsIgnoreCase("Valore atteso")) {

							misureApogeoAntennaBean.setValoreAttesoCella6(viwRadiantiE.getDescValore());

						} else {

							misureApogeoAntennaBean.setConformeCella6(viwRadiantiE.getDescLabelLov());

						}
						break;
					}

				}

				i++;
				if (i<arrayViwRadiantiE.size()) {
					viwRadiantiE = arrayViwRadiantiE.get(i);
				}

			}
			arrayMisureApogeoAntenna.add(misureApogeoAntennaBean);
			i--;
		}

		return arrayMisureApogeoAntenna;

	}

	public static String getRisultatoAllegatoByCodiSessioneCodiOggetto(int codiSessione, int codiOggetto, SincronizzazioneDao sincronizzazioneDao) {
		try {

			TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(codiOggetto, codiSessione);

			if (tabRelSessioneStatiVerbale!=null) {

					switch (tabRelSessioneStatiVerbale.getCodiStatoVerbale()) {

					case Const.NEGATIVO_INT: 
						return Const.NEGATIVO;
					case Const.POSITIVO_CON_RISERVA_INT:
						return Const.POSITIVO_CON_RISERVA;
					case Const.POSITIVO_INT:
						return Const.POSITIVO;
					default: return Const.NON_APPLICABILE;


					}

				} else {
					return Const.NON_APPLICABILE;
				}
		} catch (Exception e) {
			e.printStackTrace();
			return Const.NON_APPLICABILE;
		}


	}

	public static  Integer getRisultatoSurveyByCodiSurvey(Integer codiSurvey,SurveyDao surveyDao) {
		ArrayList<Integer> listCodiSurvey = new ArrayList<>();
		listCodiSurvey.add(codiSurvey);
		List<TabRisultatoSurvey> listRisultatoSurvey = surveyDao.getListRisultatiSurvey(listCodiSurvey);
		if (listRisultatoSurvey!=null && listRisultatoSurvey.size()>0) {
			return listRisultatoSurvey.get(0).getCodiValoreRisultatoSurvey();
		}
		return null;
	}
	

	//Il seguente metodo rimuove la caratteristica flussi E1 dall'array e ritorna la lista delle viw associate ad essa in un altro array. (Flussi E1 va gestita a parte)
	public static ArrayList<ViwApparatoNokia> getNokiaArrayVerinst(List<ViwApparatoNokia> arrayVerInst) {
		ArrayList<ViwApparatoNokia> apparatoNokiaArrayAppo=new ArrayList<>();
		for (ViwApparatoNokia viwApparatoNokia : arrayVerInst) {
			if (viwApparatoNokia.getCodiCaratteristica().equals(ConstReport.TestFlussiNokia)) {
				apparatoNokiaArrayAppo.add(viwApparatoNokia);
			}
		}
		arrayVerInst.removeAll(apparatoNokiaArrayAppo);
		return apparatoNokiaArrayAppo;
	}
	
	//Il seguente metodo rimuove la caratteristica flussi E1 dall'array e ritorna la lista delle viw associate ad essa in un altro array. (Flussi E1 va gestita a parte)
		public static ArrayList<ViwApparatoHuawei> getHuaweiArrayVerinst(List<ViwApparatoHuawei> arrayVerInst) {
			ArrayList<ViwApparatoHuawei> apparatoHuaweiArrayAppo=new ArrayList<>();
			for (ViwApparatoHuawei viwApparatoHuawei : arrayVerInst) {
				if (viwApparatoHuawei.getCodiCaratteristica().equals(ConstReport.TestFlussiHuawei)) {
					apparatoHuaweiArrayAppo.add(viwApparatoHuawei);
				}
			}
			arrayVerInst.removeAll(apparatoHuaweiArrayAppo);
			return apparatoHuaweiArrayAppo;
		}
	
	//Il seguente metodo definisce il bean per la caratteristica test flussi
	public static ArrayList<TestFlussiBean> getArrayTestFlussiBean(List<ViwApparatoNokia> arrayTestFlussi) {
		
		ArrayList<TestFlussiBean> flussiBeans = new ArrayList<>();
		TestFlussiBean bean = new TestFlussiBean();
		if (arrayTestFlussi!=null && arrayTestFlussi.size()>0) {
			bean.setDescCaratteristica(arrayTestFlussi.get(0).getDescCaratteristica());
			bean.setCodiCaratteristica(arrayTestFlussi.get(0).getCodiCaratteristica());
			bean.setDescNote(arrayTestFlussi.get(0).getDescNote());
		}
		for (ViwApparatoNokia viwApparatoNokia : arrayTestFlussi) {
			if (viwApparatoNokia.getNumeLivelloE()!=null) {
				switch (viwApparatoNokia.getNumeLivelloE()) {
					case 1:  
						bean.setDescE1(viwApparatoNokia.getDescLabelLov());
						break;
					case 2:  
						bean.setDescE2(viwApparatoNokia.getDescLabelLov());
						break;
					case 3:  
						bean.setDescE3(viwApparatoNokia.getDescLabelLov());
						break;
					case 4:  
						bean.setDescE4(viwApparatoNokia.getDescLabelLov());
						break;
					case 5:  
						bean.setDescE5(viwApparatoNokia.getDescLabelLov());
						break;
					case 6:  
						bean.setDescE6(viwApparatoNokia.getDescLabelLov());
						break;
					case 7:  
						bean.setDescE7(viwApparatoNokia.getDescLabelLov());
						break;
					case 8:  
						bean.setDescE8(viwApparatoNokia.getDescLabelLov());
						break;
						
				}
			}
		}
		flussiBeans.add(bean);
		return flussiBeans;
	}

	//Questo metodo crea i bean per allarmi esterni nokia in verichi funzionali d'apparato in commisining
	//data la struttura dei nomi delle caratteristiche (vedi ad esempio caratt. 851) 
	//si splotta il nome in Nr, Severit� e slogan allarme per essere fedeli all'excel
	public static List<AllarmiEsterni2GBean> getArrayAllarmiNokia(List<ViwVerFunzAllarmi> arrayAllarmi2G) {
		
		String configurazione= null;
		String competenzaAllarmi = null;
		AllarmiEsterni2GBean allarmiEsterni2GBean;
		ArrayList<AllarmiEsterni2GBean> arrayToReturn= new ArrayList<>();
		
		for (ViwVerFunzAllarmi viwVerFunzAllarmi : arrayAllarmi2G) {
			if (viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.CompetenzaAllarmiNokia2G) || viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.CompetenzaAllarmiNokia3G)) {
				competenzaAllarmi = viwVerFunzAllarmi.getDescLabel();
				continue;
			}
			if (viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.ConfigurazioneNokia2G) || viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.ConfigurazioneNokia3G)) {
				configurazione = viwVerFunzAllarmi.getDescLabel();
				continue;
			}
			allarmiEsterni2GBean = new AllarmiEsterni2GBean();
			String[] parts = viwVerFunzAllarmi.getDescCaratteristica().split("/");
			allarmiEsterni2GBean.setCompetenzaAllarmi(competenzaAllarmi);
			allarmiEsterni2GBean.setConfigurazione(configurazione);
			if (parts.length==3) {
			allarmiEsterni2GBean.setNr(parts[0]);
			allarmiEsterni2GBean.setSeverita(parts[1]);
			allarmiEsterni2GBean.setSloganAllarme(parts[2]);
			} else if (parts.length==2) {
				allarmiEsterni2GBean.setNr(parts[0]);
				allarmiEsterni2GBean.setSloganAllarme(parts[1]);
			}
			allarmiEsterni2GBean.setEsito(viwVerFunzAllarmi.getDescLabel());
			arrayToReturn.add(allarmiEsterni2GBean);
		}
		
		//se non ho fatto nessuna caratteristica ci metto 24 righe vuote
		if (arrayToReturn.size()==0) {
			for (int i=0; i<24;i++) {
				allarmiEsterni2GBean = new AllarmiEsterni2GBean();
				allarmiEsterni2GBean.setCompetenzaAllarmi(competenzaAllarmi);
				allarmiEsterni2GBean.setConfigurazione(configurazione);
				arrayToReturn.add(allarmiEsterni2GBean);
			}
		}
		
		return arrayToReturn;
	}

	//Questo metodo prepara i bean Verifica MHA.
	//Una hash map conterra alla fine due bean ognuno salvato con chiave il
	//codice caratteristica di Tipo MHA e Funzionalit� MHA
	public static List<VerificaMHABean> getArrayVerificaMHABean(List<ViwApparatoNokia> arrayVerificaMHA) {

		HashMap<Integer, VerificaMHABean> hashMap = new HashMap<>();

		for (ViwApparatoNokia viwApparatoNokia : arrayVerificaMHA) {

			if (! hashMap.containsKey(viwApparatoNokia.getCodiCaratteristica())) {//se non ho ancora creato il bean
				hashMap.put(viwApparatoNokia.getCodiCaratteristica(), new VerificaMHABean()); //lo creo e lo metto nella hash map
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescCaratteristica(viwApparatoNokia.getDescCaratteristica());
			}

			if (viwApparatoNokia.getDescLabel()!=null && viwApparatoNokia.getNumeCella()!=null) { //"se ho messo i dati"
				if (viwApparatoNokia.getDescLabel().equalsIgnoreCase("Via RF 0-1")) {
					hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescNoteVia01(viwApparatoNokia.getDescNote());
					switch (viwApparatoNokia.getNumeCella()) {
						case 1: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella1Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 2: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella2Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 3: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella3Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 4: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella4Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 5: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella5Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 6: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella6Via01(viwApparatoNokia.getDescLabelLov());
							break;		
					}
					
				} else { //se � Via RF 0-2
					hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescNoteVia02(viwApparatoNokia.getDescNote());
					switch (viwApparatoNokia.getNumeCella()) {
					
					case 1: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella1Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 2: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella2Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 3: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella3Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 4: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella4Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 5: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella5Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 6: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella6Via02(viwApparatoNokia.getDescLabelLov());
						break;
					
					}
				}
				
			} 

		}

		return 	 new ArrayList<>(hashMap.values());

	}

	public static List<VerificaRETBean> getArrayVerificaRETBean(List<ViwApparatoNokia> arrayVerificaRET) {
		
		String modalitaGestioneRET=null;
		String gestoreRET=null;
		HashMap<Integer, VerificaRETBean> hashMap = new HashMap<>();
		
		for (ViwApparatoNokia viwApparatoNokia : arrayVerificaRET) {
			
			if (viwApparatoNokia.getCodiCaratteristica().equals(ConstReport.ModalitaGestioneRetNokia)) {
				modalitaGestioneRET = viwApparatoNokia.getDescLabelLov();
				continue;
			}
			
			if (viwApparatoNokia.getCodiCaratteristica().equals(ConstReport.RetGestitoDaNokia)) {
				gestoreRET=viwApparatoNokia.getDescLabelLov();
				continue;
			}

			if (! hashMap.containsKey(viwApparatoNokia.getCodiCaratteristica())) {//se non ho ancora creato il bean
				hashMap.put(viwApparatoNokia.getCodiCaratteristica(), new VerificaRETBean()); //lo creo e lo metto nella hash map
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescCaratteristica(viwApparatoNokia.getDescCaratteristica());
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setModalit�GestioneRET(modalitaGestioneRET);
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setGestoreRet(gestoreRET);
			}
			

			if (viwApparatoNokia.getDescLabel()!=null && viwApparatoNokia.getNumeCella()!=null) { //"se ho messo i dati"
				if (viwApparatoNokia.getDescLabel().equalsIgnoreCase("Via RF 0-1")) {
					hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescNoteVia01(viwApparatoNokia.getDescNote());
					switch (viwApparatoNokia.getNumeCella()) {
						case 1: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella1Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 2: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella2Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 3: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella3Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 4: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella4Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 5: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella5Via01(viwApparatoNokia.getDescLabelLov());
							break;
						case 6: 
							hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella6Via01(viwApparatoNokia.getDescLabelLov());
							break;		
					}
					
				} else { //se � Via RF 0-2
					hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescNoteVia02(viwApparatoNokia.getDescNote());
					switch (viwApparatoNokia.getNumeCella()) {
					
					case 1: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella1Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 2: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella2Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 3: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella3Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 4: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella4Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 5: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella5Via02(viwApparatoNokia.getDescLabelLov());
						break;
					case 6: 
						hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setCella6Via02(viwApparatoNokia.getDescLabelLov());
						break;
					
					}
				}
				
			} 

		}
		
		return 	 new ArrayList<>(hashMap.values());
	}

	public static List<ProveDiServizioBean> getArrayProveDiServizio(List<ViwVerFunzAllarmi> arrayProveDiServizio) {
		String sistema=null;
		int i=1;
		ArrayList<ProveDiServizioBean> arrayToReturn = new ArrayList<>();
		for (ViwVerFunzAllarmi viwVerFunzAllarmi : arrayProveDiServizio) {
			if (viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.SistemaProveDiServizioNokia)) {
				sistema =viwVerFunzAllarmi.getDescLabel();
				continue;
			}
			ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
			proveDiServizioBean.setDescrizioneTest(viwVerFunzAllarmi.getDescCaratteristica());
			proveDiServizioBean.setNr("#"+(i++));
			proveDiServizioBean.setNote(viwVerFunzAllarmi.getDescNote());
			proveDiServizioBean.setEsito(viwVerFunzAllarmi.getDescLabel());
			proveDiServizioBean.setSistema(sistema);
			arrayToReturn.add(proveDiServizioBean);
		}
		
		if (arrayToReturn.size()==0) { //se "non sono stati inseriti i dati"
			//aggiungo 15 fittizi elementi vuoti
			for (int j=0;j<15;j++) {
				ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
				proveDiServizioBean.setNr("#"+(j+1));
				proveDiServizioBean.setSistema(sistema);
				arrayToReturn.add(proveDiServizioBean);
			}
		}
		return arrayToReturn;
		
	}

	//questo metodo elimina dall'array di caratteristiche quelle che hanno un layout diverso,
	//i record eliminati vengono eaggrupti nell'array ritornato (i "Valori misurati")
	public static List<VerInstWIPMBean> getListValoriMisuratiWIPM(List<ViwApparatoNokia> arrayVerificheInstWIPMBean) {
		
		ArrayList<ViwApparatoNokia> arrayAppo = new ArrayList<>();
		for (ViwApparatoNokia viwApparatoNokia : arrayVerificheInstWIPMBean) {
			if (ArrayUtils.contains(ConstReport.ValoriMisuratiWIPMNokia,viwApparatoNokia.getCodiCaratteristica())) {
				arrayAppo.add(viwApparatoNokia);
			}
		}
		arrayVerificheInstWIPMBean.removeAll(arrayAppo);//rimuovo quelle diverse
		
		//costruisco i bean con quelle diverse
		HashMap<Integer,VerInstWIPMBean> hashMap = new HashMap<>(); 
		for (ViwApparatoNokia viwApparatoNokia:arrayAppo) {
			if (!hashMap.containsKey(viwApparatoNokia.getCodiCaratteristica())) {
				hashMap.put(viwApparatoNokia.getCodiCaratteristica(), new VerInstWIPMBean());
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setDescCaratteristica(viwApparatoNokia.getDescCaratteristica());
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setNote(viwApparatoNokia.getDescNote());

			}
			if (viwApparatoNokia.getDescLabel()!=null && viwApparatoNokia.getDescLabel().equalsIgnoreCase("Valore misurato")) {
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setValoreMisurato(viwApparatoNokia.getDescValore());
			} else {
				hashMap.get(viwApparatoNokia.getCodiCaratteristica()).setEsito(viwApparatoNokia.getDescLabelLov());
			}
		}
		
		return new ArrayList<VerInstWIPMBean> (hashMap.values());
		
				
	}

	//Il seguente metodo definisce il bean per la caratteristica test flussi
	public static ArrayList<TestFlussiBean> getArrayTestFlussiHuaweiBean(List<ViwApparatoHuawei> arrayTestFlussi) {
		
		ArrayList<TestFlussiBean> flussiBeans = new ArrayList<>();
		TestFlussiBean bean = new TestFlussiBean();
		if (arrayTestFlussi!=null && arrayTestFlussi.size()>0) {
			bean.setDescCaratteristica(arrayTestFlussi.get(0).getDescCaratteristica());
			bean.setCodiCaratteristica(arrayTestFlussi.get(0).getCodiCaratteristica());
			bean.setDescNote(arrayTestFlussi.get(0).getDescNote());
		}
		for (ViwApparatoHuawei viwApparatoHuawei : arrayTestFlussi) {
			if (viwApparatoHuawei.getNumeLivelloE()!=null) {
				switch (viwApparatoHuawei.getNumeLivelloE()) {
					case 1:  
						bean.setDescE1(viwApparatoHuawei.getDescLabelLov());
						break;
					case 2:  
						bean.setDescE2(viwApparatoHuawei.getDescLabelLov());
						break;
					case 3:  
						bean.setDescE3(viwApparatoHuawei.getDescLabelLov());
						break;
					case 4:  
						bean.setDescE4(viwApparatoHuawei.getDescLabelLov());
						break;
					case 5:  
						bean.setDescE5(viwApparatoHuawei.getDescLabelLov());
						break;
					case 6:  
						bean.setDescE6(viwApparatoHuawei.getDescLabelLov());
						break;
					case 7:  
						bean.setDescE7(viwApparatoHuawei.getDescLabelLov());
						break;
					case 8:  
						bean.setDescE8(viwApparatoHuawei.getDescLabelLov());
						break;
						
				}
			}
		}
		flussiBeans.add(bean);
		return flussiBeans;
	}

	//il seguente metodo definisce il bean per gli allarmi esterni di huawei. Prima togloie il tipo telaio e la competenza allarmi per poi 
	//settarle a tutti i record
	public static List<VerFunzAllarmiEsterniBean> getArrayAllarmiHuawei(List<ViwVerFunzAllarmi> arrayAllarmi) {
		String tipoTelaio = null;
		String competenzaAllarmi=null;
		List<ViwVerFunzAllarmi> listToRemove = new ArrayList<>();
		for (ViwVerFunzAllarmi verFunzAllarmi : arrayAllarmi) {
			if (verFunzAllarmi.getCodiCaratteristica()==ConstReport.TipoTelaioAllarmiEsterniHuawei) {
				tipoTelaio = verFunzAllarmi.getDescLabel();
				listToRemove.add(verFunzAllarmi);
				continue;
			}
			if (verFunzAllarmi.getCodiCaratteristica()==ConstReport.CompetenzaAllarmiEsterniHuawei) {
				competenzaAllarmi= verFunzAllarmi.getDescLabel();
				listToRemove.add(verFunzAllarmi);
			}
		}
		arrayAllarmi.removeAll(listToRemove);
		
		List<VerFunzAllarmiEsterniBean> listToReturn = new ArrayList<>();
		VerFunzAllarmiEsterniBean verFunzAllarmiEsterniBean=null;
		String[] descCaratteristicaParti=null;
		//sul nome della caratteristica eseguo prima lo split per "/" e poi per ":" per estrarre i campi  
		for (ViwVerFunzAllarmi verFunzAllarmi : arrayAllarmi) {
			verFunzAllarmiEsterniBean = new VerFunzAllarmiEsterniBean();
			verFunzAllarmiEsterniBean.setTipoTelaio(tipoTelaio);
			verFunzAllarmiEsterniBean.setCompetenzaAllarmi(competenzaAllarmi);
			verFunzAllarmiEsterniBean.setEsito(verFunzAllarmi.getDescLabel());
			verFunzAllarmiEsterniBean.setNote(verFunzAllarmi.getDescNote());
			descCaratteristicaParti = verFunzAllarmi.getDescCaratteristica().split("/");
			for (int i=0;i<descCaratteristicaParti.length;i++) {
				//il valore � dopo i ":"
				String value = descCaratteristicaParti[i].split(":")[1];
				switch (i) {
					case 0:
						verFunzAllarmiEsterniBean.setAllarme(value);
						break;
					case 1:
						verFunzAllarmiEsterniBean.setAlarmIdM2000(value);
						break;
					case 2:
						verFunzAllarmiEsterniBean.setCriterio(value);
						break;
					case 3:
						verFunzAllarmiEsterniBean.setSeverita(value);
						break;
					case 4:
						verFunzAllarmiEsterniBean.setSloganAllarme(value);
						break;
					
				}
			}
			listToReturn.add(verFunzAllarmiEsterniBean);
		}
		if (listToReturn.size()==0) {
			for (int i=0;i<16;i++) {
				verFunzAllarmiEsterniBean = new VerFunzAllarmiEsterniBean();
				verFunzAllarmiEsterniBean.setAllarme(String.valueOf(i));
				listToReturn.add(verFunzAllarmiEsterniBean);
			}
		}
		return listToReturn;
	}

	public static List<VerificaTMABean> getArrayVerificaTMABean(List<ViwApparatoHuawei> arrayVerificaTMA) {
		HashMap<Integer, VerificaTMABean> hashMap = new HashMap<>();

		for (ViwApparatoHuawei viwApparatoHuawei : arrayVerificaTMA) {

			if (! hashMap.containsKey(viwApparatoHuawei.getCodiCaratteristica())) {//se non ho ancora creato il bean
				hashMap.put(viwApparatoHuawei.getCodiCaratteristica(), new VerificaTMABean()); //lo creo e lo metto nella hash map
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescCaratteristica(viwApparatoHuawei.getDescCaratteristica());
			}

			if (viwApparatoHuawei.getDescLabel()!=null && viwApparatoHuawei.getNumeCella()!=null) { //"se ho messo i dati"
				if (viwApparatoHuawei.getDescLabel().equalsIgnoreCase("Via RF 0-1")) {
					hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescNoteVia01(viwApparatoHuawei.getDescNote());
					switch (viwApparatoHuawei.getNumeCella()) {
						case 1: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella1Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 2: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella2Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 3: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella3Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 4: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella4Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 5: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella5Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 6: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella6Via01(viwApparatoHuawei.getDescLabelLov());
							break;		
					}
					
				} else { //se � Via RF 0-2
					hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescNoteVia02(viwApparatoHuawei.getDescNote());
					switch (viwApparatoHuawei.getNumeCella()) {
					
					case 1: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella1Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 2: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella2Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 3: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella3Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 4: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella4Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 5: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella5Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 6: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella6Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					
					}
				}
				
			} 

		}

		return 	 new ArrayList<>(hashMap.values());
		
	}

	public static List<VerificaRETBean> getArrayVerificaRETHuaweiBean(List<ViwApparatoHuawei> arrayVerificaRET) {
		
		String modalitaGestioneRET=null;
		String gestoreRET=null;
		HashMap<Integer, VerificaRETBean> hashMap = new HashMap<>();
		
		for (ViwApparatoHuawei viwApparatoHuawei : arrayVerificaRET) {
			
			if (viwApparatoHuawei.getCodiCaratteristica().equals(ConstReport.ModalitaGestioneRetHuawei)) {
				modalitaGestioneRET = viwApparatoHuawei.getDescLabelLov();
				continue;
			}
			
			if (viwApparatoHuawei.getCodiCaratteristica().equals(ConstReport.RetGestitoDaHuawei)) {
				gestoreRET=viwApparatoHuawei.getDescLabelLov();
				continue;
			}

			if (! hashMap.containsKey(viwApparatoHuawei.getCodiCaratteristica())) {//se non ho ancora creato il bean
				hashMap.put(viwApparatoHuawei.getCodiCaratteristica(), new VerificaRETBean()); //lo creo e lo metto nella hash map
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescCaratteristica(viwApparatoHuawei.getDescCaratteristica());
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setModalit�GestioneRET(modalitaGestioneRET);
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setGestoreRet(gestoreRET);
			}
			

			if (viwApparatoHuawei.getDescLabel()!=null && viwApparatoHuawei.getNumeCella()!=null) { //"se ho messo i dati"
				if (viwApparatoHuawei.getDescLabel().equalsIgnoreCase("Via RF 0-1")) {
					hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescNoteVia01(viwApparatoHuawei.getDescNote());
					switch (viwApparatoHuawei.getNumeCella()) {
						case 1: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella1Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 2: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella2Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 3: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella3Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 4: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella4Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 5: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella5Via01(viwApparatoHuawei.getDescLabelLov());
							break;
						case 6: 
							hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella6Via01(viwApparatoHuawei.getDescLabelLov());
							break;		
					}
					
				} else { //se � Via RF 0-2
					hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescNoteVia02(viwApparatoHuawei.getDescNote());
					switch (viwApparatoHuawei.getNumeCella()) {
					
					case 1: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella1Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 2: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella2Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 3: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella3Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 4: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella4Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 5: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella5Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					case 6: 
						hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setCella6Via02(viwApparatoHuawei.getDescLabelLov());
						break;
					
					}
				}
				
			} 

		}
		
		return 	 new ArrayList<>(hashMap.values());
	
		
	}
	
	
	public static List<ProveDiServizioBean> getArrayProveDiServizioHuawei(List<ViwVerFunzAllarmi> arrayProveDiServizio) {
		String sistema=null;
		int i=1;
		ArrayList<ProveDiServizioBean> arrayToReturn = new ArrayList<>();
		for (ViwVerFunzAllarmi viwVerFunzAllarmi : arrayProveDiServizio) {
			if ( viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.SistemaProveDiServizioHuawei)) {
				sistema =viwVerFunzAllarmi.getDescLabel();
				continue;
			}
			ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
			proveDiServizioBean.setDescrizioneTest(viwVerFunzAllarmi.getDescCaratteristica().split("/")[1]);
			proveDiServizioBean.setNr("#"+(i++));
			proveDiServizioBean.setNote(viwVerFunzAllarmi.getDescNote());
			proveDiServizioBean.setEsito(viwVerFunzAllarmi.getDescLabel());
			proveDiServizioBean.setSistema(sistema);
			arrayToReturn.add(proveDiServizioBean);
		}
		
		if (arrayToReturn.size()==0) { //se "non sono stati inseriti i dati"
			//aggiungo 15 fittizi elementi vuoti
			for (int j=0;j<15;j++) {
				ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
				proveDiServizioBean.setNr("#"+(j+1));
				proveDiServizioBean.setSistema(sistema);
				arrayToReturn.add(proveDiServizioBean);
			}
		}
		return arrayToReturn;
		
	}

	public static List<VerInstWIPMBean> getListValoriMisuratiWIPMHuawei(List<ViwApparatoHuawei> arrayVerificheInstWIPMBean) {

		ArrayList<ViwApparatoHuawei> arrayAppo = new ArrayList<>();
		for (ViwApparatoHuawei viwApparatoHuawei : arrayVerificheInstWIPMBean) {
			if (ArrayUtils.contains(ConstReport.ValoriMisuratiWIPMHuawei
					,viwApparatoHuawei.getCodiCaratteristica())) {
				arrayAppo.add(viwApparatoHuawei);
			}
		}
		arrayVerificheInstWIPMBean.removeAll(arrayAppo);//rimuovo quelle diverse
		
		//costruisco i bean con quelle diverse
		HashMap<Integer,VerInstWIPMBean> hashMap = new HashMap<>(); 
		for (ViwApparatoHuawei viwApparatoHuawei:arrayAppo) {
			if (!hashMap.containsKey(viwApparatoHuawei.getCodiCaratteristica())) {
				hashMap.put(viwApparatoHuawei.getCodiCaratteristica(), new VerInstWIPMBean());
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setDescCaratteristica(viwApparatoHuawei.getDescCaratteristica());
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setNote(viwApparatoHuawei.getDescNote());

			}
			if (viwApparatoHuawei.getDescLabel()!=null && viwApparatoHuawei.getDescLabel().equalsIgnoreCase("Valore misurato")) {
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setValoreMisurato(viwApparatoHuawei.getDescValore());
			} else {
				hashMap.get(viwApparatoHuawei.getCodiCaratteristica()).setEsito(viwApparatoHuawei.getDescLabelLov());
			}
		}
		
		return new ArrayList<VerInstWIPMBean> (hashMap.values());
	}

	public static List<VerFunzAllarmiEsterniBean> getArrayAllarmiEriccson(List<ViwVerFunzAllarmiEricsson> arrayAllarmi) {
		String tipoTelaio = null;
		String competenzaAllarmi=null;
		List<ViwVerFunzAllarmiEricsson> listToRemove = new ArrayList<>();
		for (ViwVerFunzAllarmiEricsson verFunzAllarmi : arrayAllarmi) {
			if (verFunzAllarmi.getCodiCaratteristica()==ConstReport.TipoTelaioAllarmiEsterniEricsson) {
				tipoTelaio = verFunzAllarmi.getDescLabel();
				listToRemove.add(verFunzAllarmi);
				continue;
			}
			if (verFunzAllarmi.getCodiCaratteristica()==ConstReport.CompetenzaAllarmiEsterniEricsson) {
				competenzaAllarmi= verFunzAllarmi.getDescLabel();
				listToRemove.add(verFunzAllarmi);
			}
		}
		arrayAllarmi.removeAll(listToRemove);
		
		List<VerFunzAllarmiEsterniBean> listToReturn = new ArrayList<>();
		VerFunzAllarmiEsterniBean verFunzAllarmiEsterniBean=null;
		String[] descCaratteristicaParti=null;
		//sul nome della caratteristica eseguo prima lo split per "/" e poi per ":" per estrarre i campi  
		for (ViwVerFunzAllarmiEricsson verFunzAllarmi : arrayAllarmi) {
			verFunzAllarmiEsterniBean = new VerFunzAllarmiEsterniBean();
			verFunzAllarmiEsterniBean.setTipoTelaio(tipoTelaio);
			verFunzAllarmiEsterniBean.setCompetenzaAllarmi(competenzaAllarmi);
			verFunzAllarmiEsterniBean.setEsito(verFunzAllarmi.getDescLabel());
			descCaratteristicaParti = verFunzAllarmi.getDescCaratteristica().split("/");
			for (int i=0;i<descCaratteristicaParti.length;i++) {
				switch (i) {
					case 0:
						verFunzAllarmiEsterniBean.setAllarme(descCaratteristicaParti[0]);
						break;
					case 1:
						verFunzAllarmiEsterniBean.setSloganAllarme(descCaratteristicaParti[1]);
				}
			}
			listToReturn.add(verFunzAllarmiEsterniBean);
		}
		
		for (int i=listToReturn.size()+1;i<=10;i++) {
			verFunzAllarmiEsterniBean = new VerFunzAllarmiEsterniBean();
			verFunzAllarmiEsterniBean.setAllarme(String.valueOf(i));
			listToReturn.add(verFunzAllarmiEsterniBean);
			
		}
		return listToReturn;
	}

	public static List<ProveDiServizioBean> getListProveFunzionaliServizioBean(List<ViwProveFunzSerEricsson> arrayProveFunzionaliServizio) {
		String sistema=null;
		int i=1;
		ArrayList<ProveDiServizioBean> arrayToReturn = new ArrayList<>();
		for (ViwProveFunzSerEricsson viwVerFunzAllarmi : arrayProveFunzionaliServizio) {
			if ( viwVerFunzAllarmi.getCodiCaratteristica().equals(ConstReport.SistemaProveDiServizioEricsson)) {
				sistema =viwVerFunzAllarmi.getDescLabel();
				continue;
			}
			ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
			proveDiServizioBean.setDescrizioneTest(viwVerFunzAllarmi.getDescCaratteristica().split("/")[1]);
			proveDiServizioBean.setNr("#"+(i++));
			proveDiServizioBean.setNote(viwVerFunzAllarmi.getDescNote());
			proveDiServizioBean.setEsito(viwVerFunzAllarmi.getDescLabel());
			proveDiServizioBean.setSistema(sistema);
			arrayToReturn.add(proveDiServizioBean);
		}
		
		if (arrayToReturn.size()==0) { //se "non sono stati inseriti i dati"
			//aggiungo 15 fittizi elementi vuoti
			for (int j=0;j<15;j++) {
				ProveDiServizioBean proveDiServizioBean = new ProveDiServizioBean();
				proveDiServizioBean.setNr("#"+(j+1));
				proveDiServizioBean.setSistema(sistema);
				arrayToReturn.add(proveDiServizioBean);
			}
		}
		return arrayToReturn;
		
	}

	public static List<GestioneRetBean> getArrayGestioneRetbean(List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureVerGestioneRETBean) {
		List<GestioneRetBean> listToReturn = new ArrayList<>();
		GestioneRetBean gestioneRetBean = new GestioneRetBean();
		for (ViwVerFunzMisureEricsson verFunzMisureEricsson:arrayVeriFunzMisureVerGestioneRETBean) {
			if (verFunzMisureEricsson.getCodiCaratteristica()==ConstReport.retGestitoDaEricsson) {
				gestioneRetBean.setRetGestitoDa(verFunzMisureEricsson.getDescLabelLov());
			} else {
				gestioneRetBean.setModalitaGesioneRET(verFunzMisureEricsson.getDescLabelLov());
			}
		}
		listToReturn.add(gestioneRetBean);
		return listToReturn;
	}

	//Questo metodo crea il bean pr i dati generali di rad A nel quale a copie di due le viwVerGenRadA sono messe nel bean
	public static List<DatiGeneraliTabletRadiantiABean> getArrayDatiGeneraliRadABean(List<ViwVerGenRadA> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletRadiantiABean> arrayDatiGeneraliTabletBean = new ArrayList<>();
		DatiGeneraliTabletRadiantiABean datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		datiGeneraliTabletRadiantiABean= new DatiGeneraliTabletRadiantiABean();
		arrayDatiGeneraliTabletBean.add(datiGeneraliTabletRadiantiABean);
		
		
		for (int i=0;i<arrayDatiGeneraliTablet.size();i++) {
			if (i%2==0) {
				arrayDatiGeneraliTabletBean.get(i/2).setDescCaratt1(arrayDatiGeneraliTablet.get(i).getDescCaratteristica());
				arrayDatiGeneraliTabletBean.get(i/2).setField1(arrayDatiGeneraliTablet.get(i).getDescValore());
			} else {
				arrayDatiGeneraliTabletBean.get(i/2).setDescCaratt2(arrayDatiGeneraliTablet.get(i).getDescCaratteristica());
				arrayDatiGeneraliTabletBean.get(i/2).setField2(arrayDatiGeneraliTablet.get(i).getDescValore());
			}
			
		}
		return arrayDatiGeneraliTabletBean;
	}

	public static List<DatiGeneraliTabletRadiantiBBean> getArrayTabletDatiGeneraliRadBBean(List<ViwMisureGenRadB> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletRadiantiBBean> listToReturn = new ArrayList<>();
		DatiGeneraliTabletRadiantiBBean datiGeneraliTabletRadiantiBBean = new DatiGeneraliTabletRadiantiBBean();
		for (ViwMisureGenRadB viwMisureGenRadB: arrayDatiGeneraliTablet) {
			if (viwMisureGenRadB.getCodiCaratteristica()==ConstReport.TipoMisuraRadB) {
				datiGeneraliTabletRadiantiBBean.setTipoMisura(viwMisureGenRadB.getDescLabelLov());
				continue;
			}
			if (viwMisureGenRadB.getCodiCaratteristica()==ConstReport.StrumentoUsatoRadB ) {
				if (viwMisureGenRadB.getDescLabel()!=null && viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Costruttore")  ){
					datiGeneraliTabletRadiantiBBean.setCostruttoreStrumento(viwMisureGenRadB.getDescValore());
				} else {
					datiGeneraliTabletRadiantiBBean.setModelloStrumento(viwMisureGenRadB.getDescValore());
				}
				continue;
			}
			datiGeneraliTabletRadiantiBBean.setDataTaratura(viwMisureGenRadB.getDescValore());
			
		}
		listToReturn.add(datiGeneraliTabletRadiantiBBean);
		return listToReturn;
	}

	public static List<DatiGeneraliTabletRadiantiDBean> getArrayTabletDatiGeneraliRadDBean(List<ViwMisureDiPimRadD> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletRadiantiDBean> listToReturn = new ArrayList<>();
		DatiGeneraliTabletRadiantiDBean datiGeneraliTabletRadiantiDBean = new DatiGeneraliTabletRadiantiDBean();
		for (ViwMisureDiPimRadD viwMisureGenRadD: arrayDatiGeneraliTablet) {
			if (viwMisureGenRadD.getCodiCaratteristica()==ConstReport.TipoMisuraRadD) {
				datiGeneraliTabletRadiantiDBean.setTipoMisura(viwMisureGenRadD.getDescLabelLov());
				continue;
			}
			if (viwMisureGenRadD.getCodiCaratteristica()==ConstReport.StrumentoUsatoRadD ) {
				if (viwMisureGenRadD.getDescLabel()!=null && viwMisureGenRadD.getDescLabel().equalsIgnoreCase("Costruttore") ){
					datiGeneraliTabletRadiantiDBean.setCostruttoreStrumento(viwMisureGenRadD.getDescValore());
				} else {
					datiGeneraliTabletRadiantiDBean.setModelloStrumento(viwMisureGenRadD.getDescValore());
				}
				continue;
			}
			datiGeneraliTabletRadiantiDBean.setDataTaratura(viwMisureGenRadD.getDescValore());
			
		}
		listToReturn.add(datiGeneraliTabletRadiantiDBean);
		return listToReturn;
	}

	public static List<DatiGeneraliTabletApparatoBean> getArrayTabletDatiGeneraliApparatoBean(List<ViwApparatoHuawei> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletApparatoBean> arrayToReturn = new ArrayList<>();
		DatiGeneraliTabletApparatoBean datiGeneraliTabletApparatoBean = new DatiGeneraliTabletApparatoBean();
		for (ViwApparatoHuawei apparatoHuawei : arrayDatiGeneraliTablet) {
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Tipo apparato")) {
				datiGeneraliTabletApparatoBean.setTipoApparato(apparatoHuawei.getDescLabelLov());
				continue;
			}
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Tipo telaio")) {
				datiGeneraliTabletApparatoBean.setTipoTelaio(apparatoHuawei.getDescLabelLov());
				continue;
			}
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Posizione Moduli RF")) {
				datiGeneraliTabletApparatoBean.setPosizioneModuliRF(apparatoHuawei.getDescLabelLov());
				continue;
			}
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Alimentazione primaria")) {
				datiGeneraliTabletApparatoBean.setAlimentazionePrimaria(apparatoHuawei.getDescLabelLov());
				continue;
			}
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Tipo trasporto")) {
				datiGeneraliTabletApparatoBean.setTipoTrasporto(apparatoHuawei.getDescLabelLov());
				continue;
			}
			if (apparatoHuawei.getDescCaratteristica().equalsIgnoreCase("Note")) {
				datiGeneraliTabletApparatoBean.setNote(apparatoHuawei.getDescValore());
				continue;
			}
			datiGeneraliTabletApparatoBean.setTipoFO(apparatoHuawei.getDescValore());
		}
		arrayToReturn.add(datiGeneraliTabletApparatoBean);
		return arrayToReturn;
	}
	
	public static List<DatiGeneraliTabletApparatoBean> getArrayTabletDatiGeneraliApparatoEricssonBean(List<ViwApparatoEricsson> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletApparatoBean> arrayToReturn = new ArrayList<>();
		DatiGeneraliTabletApparatoBean datiGeneraliTabletApparatoBean = new DatiGeneraliTabletApparatoBean();
		for (ViwApparatoEricsson apparatoEricsson : arrayDatiGeneraliTablet) {
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Tipo apparato")) {
				datiGeneraliTabletApparatoBean.setTipoApparato(apparatoEricsson.getDescLabelLov());
				continue;
			}
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Tipo telaio")) {
				datiGeneraliTabletApparatoBean.setTipoTelaio(apparatoEricsson.getDescLabelLov());
				continue;
			}
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Posizione Moduli RF")) {
				datiGeneraliTabletApparatoBean.setPosizioneModuliRF(apparatoEricsson.getDescLabelLov());
				continue;
			}
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Alimentazione primaria")) {
				datiGeneraliTabletApparatoBean.setAlimentazionePrimaria(apparatoEricsson.getDescLabelLov());
				continue;
			}
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Tipo trasporto")) {
				datiGeneraliTabletApparatoBean.setTipoTrasporto(apparatoEricsson.getDescLabelLov());
				continue;
			}
			if (apparatoEricsson.getDescCaratteristica().equalsIgnoreCase("Note")) {
				datiGeneraliTabletApparatoBean.setNote(apparatoEricsson.getDescValore());
				continue;
			}
			datiGeneraliTabletApparatoBean.setTipoFO(apparatoEricsson.getDescValore());
		}
		arrayToReturn.add(datiGeneraliTabletApparatoBean);
		return arrayToReturn;
	}

	public static List<DatiGeneraliTabletApparatoBean> getArrayTabletDatiGeneraliApparatoNokiaBean(List<ViwApparatoNokia> arrayDatiGeneraliTablet) {
		List<DatiGeneraliTabletApparatoBean> arrayToReturn = new ArrayList<>();
		DatiGeneraliTabletApparatoBean datiGeneraliTabletApparatoBean = new DatiGeneraliTabletApparatoBean();
		for (ViwApparatoNokia apparatoNokia : arrayDatiGeneraliTablet) {
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Tipo apparato")) {
				datiGeneraliTabletApparatoBean.setTipoApparato(apparatoNokia.getDescLabelLov());
				continue;
			}
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Tipo telaio")) {
				datiGeneraliTabletApparatoBean.setTipoTelaio(apparatoNokia.getDescLabelLov());
				continue;
			}
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Posizione Moduli RF")) {
				datiGeneraliTabletApparatoBean.setPosizioneModuliRF(apparatoNokia.getDescLabelLov());
				continue;
			}
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Alimentazione primaria")) {
				datiGeneraliTabletApparatoBean.setAlimentazionePrimaria(apparatoNokia.getDescLabelLov());
				continue;
			}
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Tipo trasporto")) {
				datiGeneraliTabletApparatoBean.setTipoTrasporto(apparatoNokia.getDescLabelLov());
				continue;
			}
			if (apparatoNokia.getDescCaratteristica().equalsIgnoreCase("Note")) {
				datiGeneraliTabletApparatoBean.setNote(apparatoNokia.getDescValore());
				continue;
			}
			datiGeneraliTabletApparatoBean.setTipoFO(apparatoNokia.getDescLabelLov());
		}
		arrayToReturn.add(datiGeneraliTabletApparatoBean);
		return arrayToReturn;
	}
	
	
	public static List<MisureDiCaratterizzazioneCalibrazioneRadiantiBBean> getArrayRadiantiBMisureCalibrazioneBean(List<ViwMisureGenRadB> arrayCelleLabel){

		List<MisureDiCaratterizzazioneCalibrazioneRadiantiBBean> arrayCelleLabelBean = new ArrayList<MisureDiCaratterizzazioneCalibrazioneRadiantiBBean>();
		String check="";
		ViwMisureGenRadB viwMisureGenRadBAppo= new ViwMisureGenRadB();
		MisureDiCaratterizzazioneCalibrazioneRadiantiBBean misureDiCaratterizzazioneCalibrazioneRadiantiBBean = new MisureDiCaratterizzazioneCalibrazioneRadiantiBBean();
		for (ViwMisureGenRadB viwMisureGenRadB : arrayCelleLabel) {
			if(viwMisureGenRadB.getDescCaratteristica().equalsIgnoreCase("Check") && viwMisureGenRadB.getDescLabelLov()!=null && viwMisureGenRadB.getDescLabelLov().equalsIgnoreCase("SI")){
				viwMisureGenRadBAppo=viwMisureGenRadB;
				check="X";
				misureDiCaratterizzazioneCalibrazioneRadiantiBBean.setFlgCheck(check);
			}
			if(viwMisureGenRadB.getDescLabel()!=null){
				if(viwMisureGenRadB.getDescLabel().equalsIgnoreCase("Valore"))
					misureDiCaratterizzazioneCalibrazioneRadiantiBBean.setDescLabel(viwMisureGenRadB.getDescValore());
			}
		}
		arrayCelleLabel.remove(viwMisureGenRadBAppo);
		ViwMisureGenRadB  viwMisureGenRadB=arrayCelleLabel.get(0);
		misureDiCaratterizzazioneCalibrazioneRadiantiBBean.setDescElemento(viwMisureGenRadB.getDescElemento());
		misureDiCaratterizzazioneCalibrazioneRadiantiBBean.setDescCaratteristica(viwMisureGenRadB.getDescCaratteristica());
		misureDiCaratterizzazioneCalibrazioneRadiantiBBean.setCodiElemento(viwMisureGenRadB.getCodiElemento());

		
		
		arrayCelleLabelBean.add(misureDiCaratterizzazioneCalibrazioneRadiantiBBean);
		return arrayCelleLabelBean;



	}

	
	
	
}
