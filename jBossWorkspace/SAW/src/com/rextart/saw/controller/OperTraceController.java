package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabAnagOperTrace;
import com.rextart.saw.entity.ViwOperTrace;
import com.rextart.saw.service.OperationLogService;

@ManagedBean(name = "operTraceController")
@ViewScoped
public class OperTraceController implements Serializable, Controller {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{operationLogService}")
	private OperationLogService operationLogService;
	List<ViwOperTrace> listaOperTrace = new ArrayList<ViwOperTrace>();
	private CurrentUser currentUser;
	private List<TabAnagOperTrace> listaAnagOperTrace;
	private String descr;

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	// -----------------------------------------------------------------------------------
	public OperationLogService getOperationLogService() {
		return operationLogService;
	}

	public void setOperationLogService(OperationLogService operationLogService) {
		this.operationLogService = operationLogService;
	}

	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		listaOperTrace = operationLogService.getAllOperTrace();
		listaAnagOperTrace = operationLogService.getAllAnagOperTrace();
	}

	@Override
	public void openEdit() {
		// TODO Auto-generated method stub

	}

	@Override
	public void openNew() {
		// TODO Auto-generated method stub

	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub

	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub

	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub

	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	public List<ViwOperTrace> getListaOperTrace() {
		return listaOperTrace;
	}

	public void setListaOperTrace(List<ViwOperTrace> listaOperTrace) {
		this.listaOperTrace = listaOperTrace;
	}

	public List<TabAnagOperTrace> getListaAnagOperTrace() {
		return listaAnagOperTrace;
	}

	public void setListaAnagOperTrace(List<TabAnagOperTrace> listaAnagOperTrace) {
		this.listaAnagOperTrace = listaAnagOperTrace;
	}

	public List<String> CompleteDescr(String query) {
		String queryLowerCase = query.toUpperCase();
		List<TabAnagOperTrace> allDescr = listaAnagOperTrace;
		List<String> descrList = new ArrayList<>();
		allDescr = allDescr.stream().filter(t -> t.getDescOperTrace().toUpperCase().contains(queryLowerCase))
				.collect(Collectors.toList());
		for (int i = 0; i < allDescr.size(); i++) {
			descrList.add(allDescr.get(i).getDescOperTrace());
		}
		return descrList;

	}

}
