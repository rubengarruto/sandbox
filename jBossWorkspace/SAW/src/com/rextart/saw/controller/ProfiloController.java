package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabRole;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwProfiloUtente;
import com.rextart.saw.entity.ViwZoneRegioni;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;

@ManagedBean(name = "profiloController")
@ViewScoped
public class ProfiloController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	@ManagedProperty(value="#{currentUser}")
	private CurrentUser currentUser;
	private List<ViwAreaCompRegioni> regioni = new ArrayList<ViwAreaCompRegioni>();
	private List<Integer> selectedRegioni;
	private List<ViwZoneRegioni> listZone;
	private List<TabGruppi> listGruppi;
	private List<TabRole> listRoles;
	private List<ViwAreaCompRegioni> listRegioni;
	private List<ViwZoneRegioni> listNewZone;
	private List<String> selectedListDescRegione;
	private List<ViwProfiloUtente> listStrutture;
	private List<TabVendors> listVendor;
	private boolean renderedZona=false;
	private boolean renderedGruppo=false;
	private boolean renderedAreaComp=true;
	private boolean renderedVendor=false;
	private boolean renderedRegione=false;
	private boolean cessato=false;
	private boolean renderedImpresa=false;
	private boolean renderedTelImpresa = false;
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		//setNewUser(userService.getUtenteById(currentUser.getCodiUtente()));
		setListStrutture(userService.getStruttureByRole(currentUser.getCodiRole()));
		//setListNewZone(userService.getViwZoneRegioni(selectedRegioni));
		setListGruppi(userService.getTabGruppiByZona(currentUser.getCodiZona()));
		setRegioni(userService.getViwRegioniByAreaComp(currentUser.getCodiAreaComp()));
		selectedRegioni = userService.getRegioniSelectedByCodiUtente(currentUser.getCodiUtente());
		if(selectedRegioni != null && selectedRegioni.size()>0) {
			listZone = userService.getViwZoneRegioni(selectedRegioni);
			listGruppi = userService.getTabGruppiByZona(currentUser.getCodiZona());
			} 
		
		gestioneRuoli();
		

	}
	
	private void gestioneRuoli () {
		// VENDOR
		if (currentUser.getCodiRole() == Const.UTENTE_VENDOR) {
			setListVendor(userService.getAllVendor());
			renderedAreaComp=false;
			renderedZona=false;
			renderedGruppo=false;
			renderedVendor=true;
			renderedRegione=false;
			renderedImpresa = false;
			renderedTelImpresa = false;
			selectedRegioni = new ArrayList<Integer>();
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
		}
		else if((currentUser.getCodiRole()== Const.Referente_Territoriale_OP) || (currentUser.getCodiRole()==Const.Referente_Territoriale) || (currentUser.getCodiRole()==Const.Coordinatore_Operativo) || (currentUser.getCodiRole()==Const.Operatore)) {
			renderedAreaComp=true;
			renderedVendor=false;
			renderedImpresa = false;
			renderedTelImpresa = false;
			if(currentUser.getCodiStruttura() == Const.Ditte_MOI) {
				renderedRegione=false;
				renderedImpresa = true;
				renderedTelImpresa = true;
				selectedRegioni = new ArrayList<Integer>();
				setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			}
			else {
				renderedRegione=true;
			}
			if(currentUser.getCodiStruttura() == ConfigurationController.STRUTTURA_AOL || currentUser.getCodiStruttura() == ConfigurationController.STRUTTURA_AOU) {
				renderedZona=true;
				renderedGruppo=true;
				renderedImpresa = false;
				renderedTelImpresa = false;
			} else {
				renderedZona=false;
				renderedGruppo=false;
//				renderedImpresa = false;
//				renderedTelImpresa = false;
			}		
		}
		else if((currentUser.getCodiRole()==Const.DG_OP)){
			//setListAreeComp(userService.getAllAreeCompNotNazionale());
			renderedAreaComp=true;
			renderedVendor=false;
			renderedRegione=false;
			renderedImpresa = false;
			renderedTelImpresa = false;
			
		} else if ((currentUser.getCodiRole()==Const.Ditte_MOI)) {
			renderedAreaComp=true;
			renderedVendor=false;
			renderedRegione=false;
			renderedZona = false;
			renderedGruppo=false;
			renderedImpresa = true;
			renderedTelImpresa = true;
		}
		else{
			renderedAreaComp=true;
			renderedVendor=false;
			renderedGruppo=false;
			renderedRegione=false;
			renderedZona = false;
		}
	}
	
	
	public void listnerChangeRegione() {
		try {
			 listZone = new ArrayList<ViwZoneRegioni>();
			 currentUser.setCodiZona(null);
			 setListZone(userService.getViwZoneRegioni(selectedRegioni));
			 currentUser.setCodiGruppo(null);
			 setListGruppi(new ArrayList<TabGruppi>());
			} catch (Exception e) {
		}
	}
	
	public void listnerChangeZone() {
		try {
			setListGruppi(userService.getTabGruppiByZona(currentUser.getCodiZona()));
			} catch (Exception e) {
		}
	}
	
	public void update() {
		try {
			userService.saveProfile(currentUser, currentUser.getViwUtente().getCodiImpresa(), selectedRegioni, currentUser.getCodiZona(), currentUser.getViwUtente().getCodiGruppo());
			//reloadTableUtenti();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Salvataggio riuscito"));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}

	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save() {
//		try{
//			
//			if(""){
//				
//			}else{
//				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"I dati inseriti sono gi� presenti"));	
//			}		
//		}catch(Exception e){
//			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
//			e.printStackTrace();
//		}
		
	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public void updateAnagrafica(){
//		TabSyncAnagrafica tabSyncAnagrafica = anagService.getSyncAnagByNameTable(Const.SYNC_ANTENNE);
//		Integer addNumeroVersione = tabSyncAnagrafica.getNumeVersione() + 1;
//		tabSyncAnagrafica.setNumeVersione(addNumeroVersione);
//		anagService.updateSyncAnagrafica(tabSyncAnagrafica);
	}

	@Override
	public void openNew() {
		// TODO Auto-generated method stub
		
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<ViwAreaCompRegioni> getRegioni() {
		return regioni;
	}

	public void setRegioni(List<ViwAreaCompRegioni> regioni) {
		this.regioni = regioni;
	}

	public List<Integer> getSelectedRegioni() {
		return selectedRegioni;
	}

	public void setSelectedRegioni(List<Integer> selectedRegioni) {
		this.selectedRegioni = selectedRegioni;
	}

	public List<ViwZoneRegioni> getListZone() {
		return listZone;
	}

	public void setListZone(List<ViwZoneRegioni> listZone) {
		this.listZone = listZone;
	}

	public List<TabGruppi> getListGruppi() {
		return listGruppi;
	}

	public void setListGruppi(List<TabGruppi> listGruppi) {
		this.listGruppi = listGruppi;
	}

	public List<TabRole> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<TabRole> listRoles) {
		this.listRoles = listRoles;
	}

	public List<ViwAreaCompRegioni> getListRegioni() {
		return listRegioni;
	}

	public void setListRegioni(List<ViwAreaCompRegioni> listRegioni) {
		this.listRegioni = listRegioni;
	}

	public List<ViwZoneRegioni> getListNewZone() {
		return listNewZone;
	}

	public void setListNewZone(List<ViwZoneRegioni> listNewZone) {
		this.listNewZone = listNewZone;
	}

	public List<String> getSelectedListDescRegione() {
		return selectedListDescRegione;
	}

	public void setSelectedListDescRegione(List<String> selectedListDescRegione) {
		this.selectedListDescRegione = selectedListDescRegione;
	}

	public List<ViwProfiloUtente> getListStrutture() {
		return listStrutture;
	}

	public void setListStrutture(List<ViwProfiloUtente> listStrutture) {
		this.listStrutture = listStrutture;
	}

	public boolean isRenderedZona() {
		return renderedZona;
	}

	public void setRenderedZona(boolean renderedZona) {
		this.renderedZona = renderedZona;
	}

	public boolean isRenderedGruppo() {
		return renderedGruppo;
	}

	public void setRenderedGruppo(boolean renderedGruppo) {
		this.renderedGruppo = renderedGruppo;
	}

	public boolean isRenderedAreaComp() {
		return renderedAreaComp;
	}

	public void setRenderedAreaComp(boolean renderedAreaComp) {
		this.renderedAreaComp = renderedAreaComp;
	}

	public boolean isRenderedVendor() {
		return renderedVendor;
	}

	public void setRenderedVendor(boolean renderedVendor) {
		this.renderedVendor = renderedVendor;
	}

	public boolean isRenderedRegione() {
		return renderedRegione;
	}

	public void setRenderedRegione(boolean renderedRegione) {
		this.renderedRegione = renderedRegione;
	}

	public List<TabVendors> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<TabVendors> listVendor) {
		this.listVendor = listVendor;
	}

	public boolean isCessato() {
		return cessato;
	}

	public void setCessato(boolean cessato) {
		this.cessato = cessato;
	}

	public boolean isRenderedImpresa() {
		return renderedImpresa;
	}

	public void setRenderedImpresa(boolean renderedImpresa) {
		this.renderedImpresa = renderedImpresa;
	}

	public boolean isRenderedTelImpresa() {
		return renderedTelImpresa;
	}

	public void setRenderedTelImpresa(boolean renderedTelImpresa) {
		this.renderedTelImpresa = renderedTelImpresa;
	}
	
}
