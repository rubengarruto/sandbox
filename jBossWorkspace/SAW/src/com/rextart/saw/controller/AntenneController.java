package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.utility.Const;

@ManagedBean(name = "antenneController")
@ViewScoped
public class AntenneController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;
	
	@ManagedProperty(value="#{arrAntenne}")
	private List<TabAnagAntenne> arrAntenne;
	private TabAnagAntenne selectedAntenne;
	private TabAnagAntenne newAntenne;
	private CurrentUser currentUser;
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		arrAntenne=anagService.getAllAnagAntenne();
		setNewAntenne(new TabAnagAntenne());
	}

	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openNew() {
		setNewAntenne(new TabAnagAntenne());
	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save() {
		try{
			TabAnagAntenne tab = anagService.getAnagAntenneUnique(newAntenne.getDescMarca(), newAntenne.getDescModello());
			if(tab==null){
				anagService.saveAntenne(newAntenne);
				updateAnagrafica();
				setNewAntenne(new TabAnagAntenne());
				reloadTableAntenne();
				RequestContext.getCurrentInstance().execute("PF('newAntenneDialog').hide()");
			}else{
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"I dati inseriti sono gi� presenti"));	
			}		
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
		
	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	private void reloadTableAntenne() {
		setArrAntenne(anagService.getAllAnagAntenne());
	}
	
	public AnagService getAnagService() {
		return anagService;
	}
	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}
	public List<TabAnagAntenne> getArrAntenne() {
		return arrAntenne;
	}
	public void setArrAntenne(List<TabAnagAntenne> arrAntenne) {
		this.arrAntenne = arrAntenne;
	}
	public TabAnagAntenne getSelectedAntenne() {
		return selectedAntenne;
	}
	public void setSelectedAntenne(TabAnagAntenne selectedAntenne) {
		this.selectedAntenne = selectedAntenne;
	}
	
	public TabAnagAntenne getNewAntenne(){
		return newAntenne;
	}
	
	public void setNewAntenne(TabAnagAntenne newAntenne){
		this.newAntenne = newAntenne;
	}
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public void updateAnagrafica(){
		TabSyncAnagrafica tabSyncAnagrafica = anagService.getSyncAnagByNameTable(Const.SYNC_ANTENNE);
		Integer addNumeroVersione = tabSyncAnagrafica.getNumeVersione() + 1;
		tabSyncAnagrafica.setNumeVersione(addNumeroVersione);
		anagService.updateSyncAnagrafica(tabSyncAnagrafica);
	}
	
}
