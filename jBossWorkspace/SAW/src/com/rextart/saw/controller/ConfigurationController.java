package com.rextart.saw.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.rextart.saw.utility.ConstInterfaces;

@ManagedBean(name = "configurationController")
@ViewScoped
public class ConfigurationController implements ConstInterfaces{

	@Override
	public Integer getSupervisor() {
		return SUPERVISOR;
	}

	@Override
	public Integer getRpa() {
		return RPA;
	}

	@Override
	public Integer getGgu() {
		return GGU;
	}
	
	@Override
	public Integer getDg() {
		return DG;
	}

	@Override
	public Integer getDgOp() {
		return DG_OP;
	}

	@Override
	public Integer getRefTerOp() {
		return Referente_Territoriale_OP;
	}

	@Override
	public Integer getRefTer() {
		return Referente_Territoriale;
	}

	@Override
	public Integer getCorOp() {
		return Coordinatore_Operativo;
	}

	@Override
	public Integer getOperatore() {
		return OPERATORE;
	}
	
	@Override
	public Integer getUserVendor() {
		return UTENTE_VENDOR;
	}
	

	@Override
	public Integer getUserCreato() {
		return UTENTE_CREATO;
	}

	@Override
	public Integer getUserAbilitato() {
		return UTENTE_ABILITATO;
	}

	@Override
	public Integer getUserSospeso() {
		return UTENTE_SOSPESO;
	}

	@Override
	public Integer getUserCessato() {
		return UTENTE_CESSATO;
	}

	@Override
	public Integer getStrutturaAOL() {
		return STRUTTURA_AOL;
	}

	@Override
	public Integer getStrutturaAOU() {
		return STRUTTURA_AOU;
	}

	@Override
	public String getDescCodiRoleSupervisor() {
		return DESC_CODIROLE_SUPERVISOR;
	}

	@Override
	public String getDescCodiRoleAdmin() {
		return DESC_CODIROLE_ADMIN;
	}

	@Override
	public String getDescCodiRoleOperativo() {
		return DESC_CODIROLE_OPRETATIVO;
	}

	@Override
	public String getDescCodiRoleAdminOper() {
		return DESC_CODIROLE_ADMIN_OPER;
	}
	
	@Override
	public String getDescCodiRoleVendor() {
		return DESC_CODIROLE_VENDOR;
	}
	
	@Override
	public Integer getAcquisito() {
		return ACQUISITO;
	}

	@Override
	public Integer getDisponibile() {
		return DISPONIBILE;
	}

	@Override
	public Integer getInLavorazione() {
		return IN_LAVORAZIONE;
	}

	@Override
	public Integer getChiuso() {
		return CHIUSO;
	}

	@Override
	public Integer getStrutturaVendor() {
		return STRUTTURA_VENDOR;
	}
	
	
}
