/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rextart.saw.controller;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.LoginBean;
import com.rextart.saw.entity.TabLoginFailed;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.service.OperationLogService;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;


@SuppressWarnings("deprecation")
@ManagedBean(name="menuController")
@SessionScoped
@Controller
public class MenuController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean badCredential= false;
	
	@ManagedProperty(value="#{authenticationManager}")
    private AuthenticationManager authenticationManager = null;

	@ManagedProperty(value="#{currentUser}")
	private CurrentUser currentUser;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	@ManagedProperty(value="#{operationLogService}")
	private OperationLogService operationLogService;
	
//	private StreamedContent file = null;
	
//	String urlAdmin = Const.REPO_ADMIN_URL;
//	String urlSupervisor = Const.REPO_SUPERVISOR_URL;
	
	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	private String email;
	private String userName;
	private StreamedContent file=null;

	@Autowired
	private LoginBean loginBean = new LoginBean();
	
	
	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	
	public String buttonCallPageLogin() {
		  return "goToLogin";
		 }
	
	public String buttonCallPageHome() {
		  return "goToHome";
		 }
	

	public String buttonCallPageUser() {
		  return "goToUser";
		 }
	public String buttonCallPageImprerse() {
		  return "goToImprese";
		 }

	public String buttonCallPageCollaudiInLavorazione() {
		  return "goToCollaudiInLavorazione";
		 }
	
	public String buttonCallPageCollaudiArchiviati() {
		  return "goToCollaudiArchiviati";
		 }
	
	public String buttonCallPageNewCollaudo() {
		  return "goToNewCollaudo";
		 }
	
	public String buttonCallPageMyHome() {
		  return "goToMyHome";
		 }

	public String buttonCallRegister() {
		  return "goToRegister";
	}
	
	public String buttonCallPageAntenne(){
		return "goToGestioneAntenne";
	}
	
	public String buttonCallPageDiplexer(){
		return "goToGestioneDiplexer";
	}
	
	public String buttonCallPageTma(){
		return "goToGestioneTma";
	}
	
	public String buttonCallPageRitardi(){
		return "goToGestioneRitardi";
	}
	
	public String buttonCallPageMenuDiSistema(){
		return "goToMailDiSistema";
	}
	
	public String buttonCallPageOperTrace(){
		return "goToOperTrace";
	}
	
	public String buttonCallMyProfile() {
		return "goToMyProfile";
	}

	public String buttonCallMiaSezione() {
		  return "goToMiaSezione";
		 }
	
	

	public String buttonCallCarManager(){
		  return "goToCarManager";
		 }
	
	

	
//	private boolean switchPassword= false;
	private String password;
	private String passwordConfirm;

	
	@RequestMapping("/")
	public String index(org.springframework.ui.Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		    return "pages/myHome.xhtml";
		}else
			return "index.xhtml";
	}
		
	public String buttonCallMappa() {
	  return "goToMappa";
	}
	@SuppressWarnings("unchecked")
	public String login() {
		String url = Const.SAW_LDAP_URL;
		String base = Const.SAW_LDAP_BASE;
		String userDn = Const.SAW_LDAP_USER;
		String password = Const.SAW_LDAP_PWD;
		String filterOrganizzation=Const.SAW_LDAP_FILTER_ORGANIZZATION;
		String filterPerson=Const.SAW_LDAP_FILTER_PERSON;
		String filterUid=Const.SAW_LDAP_FILTER_UID;
		try {

			TabUtente utente =userService.authenticationByUserName(loginBean.getUserName());
			if(utente!=null){
				if(!utente.isFlgImpresa() && Const.SAW_LDAP_ON && !utente.isFlgIsAdmin() && utente.getCodiVendor()==Const.EMPTY_VALUE){
					boolean autenticate=false;
					try {
						LdapContextSource ctxSrc = new LdapContextSource();
						ctxSrc.setUrl(url);
						ctxSrc.setBase(base);
						ctxSrc.setUserDn(userDn);
						ctxSrc.setPassword(password);
						ctxSrc.afterPropertiesSet();
						LdapTemplate lt = new LdapTemplate(ctxSrc);

						/* Test Query */
						AndFilter filter = new AndFilter();
						filter.and(new EqualsFilter(filterOrganizzation, filterPerson)).and(new EqualsFilter(filterUid, loginBean.getUserName()));
						
//						List<JSONObject> list = lt.search("", filter.encode(), new ContactAttributeMapperJSON());
//						  if(list.size()>0){
//							  JSONObject obj =list.get(0);
//							  obj.get("uid");
//							  obj.get("objectClass");
//							  obj.get("sn");
//							  obj.get("cn");
//						  } 
						autenticate = lt.authenticate(DistinguishedName.EMPTY_PATH,filter.toString(),  loginBean.getPassword());

					} catch (Exception e) {
						e.printStackTrace();
					}
					if(autenticate){
						Authentication request = new UsernamePasswordAuthenticationToken(
								loginBean.getUserName(), utente.getDescPwd());
						Authentication result = authenticationManager.authenticate(request);
						SecurityContextHolder.getContext().setAuthentication(result);
						setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal());
						setBadCredential(false);
						
						String ipEmptyForClean = "";
						cleanTabLoginFailed(ipEmptyForClean);
						
						Timestamp data = new Timestamp(System.currentTimeMillis());
						if(data.getHours()>=Const.HOUR_FOB_MAX || data.getHours()<Const.HOUR_FOB_MIN){
							operationLogService.logOperation(Const.LOGIN_FOB,data , utente.getCodiUtente(),loginBean.getUserName());
							operationLogService.sendMailFOB(loginBean.getUserName(), data);
						}else{
							operationLogService.logOperation(Const.LOGIN,data , utente.getCodiUtente(),loginBean.getUserName());
						}
						System.out.println("UTENTE TELECOM: "+loginBean.getUserName()+" LOGGATO CON SUCCESSO");
						return "correct";

					}
					else{
						
						userLoginFailed();
						
						setBadCredential(true);
						setCurrentUser(null);
						return "incorrect";
					}
				}else{
					Authentication request = new UsernamePasswordAuthenticationToken(
							loginBean.getUserName(), loginBean.getPassword());
					Authentication result = authenticationManager.authenticate(request);
					SecurityContextHolder.getContext().setAuthentication(result);
					
					String ipEmptyForClean = "";
					cleanTabLoginFailed(ipEmptyForClean);

						if(!utente.isFlgChangePwd() && (utente.isFlgImpresa()||utente.getCodiVendor()!=Const.EMPTY_VALUE)){
							return"changePwd";
						}else{
							setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
									.getAuthentication().getPrincipal());
							setBadCredential(false);
							Timestamp data = new Timestamp(System.currentTimeMillis());

							if(data.getHours()>=Const.HOUR_FOB_MAX || data.getHours()<Const.HOUR_FOB_MIN){
								operationLogService.logOperation(Const.LOGIN_FOB,data, utente.getCodiUtente(),loginBean.getUserName());
								operationLogService.sendMailFOB(loginBean.getUserName(), data);
							}else{
							operationLogService.logOperation(Const.LOGIN,data , currentUser.getCodiUtente(),loginBean.getUserName());
							}
							
							System.out.println("UTENTE DITTA O VENDOR : "+loginBean.getUserName()+" LOGGATO CON SUCCESSO");

							return "correct";
						}
					}
				
			}else{
				userLoginFailed();

				setBadCredential(true);
				return "incorrect";
			}
		} catch (BadCredentialsException badCredentialsException) {
		} catch (AuthenticationException e) {
		}
		userLoginFailed();

		setBadCredential(true);
		return "incorrect";
	}
	 
	public String logout(){
		SecurityContextHolder.clearContext();
		setCurrentUser(null);
		setPassword(null);
		setPasswordConfirm(null);
		return "loggedOut";
	}
	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
    

	public void update() {
		Boolean result;
		try { 
		 if(password!=null && password.equals(passwordConfirm)){
				result = userService.savePassword(password ,loginBean.getUserName());
				if(result){
					setPassword(null);
					setPasswordConfirm(null);
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Salvataggio effettuato correttamente tornare nella pagina di Login per effettuare l'accesso con la nuova password "));
					
				}else
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
				}else{
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Password non coincide con Conferma Password."));
				}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}
//
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
//	
//	public void recoveryPwd() {
//		Boolean result;
//		try {
//			result = userService.getPwdByEmail(email,userName);
//			if(result){
//				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Credenziali inviate all'indirizzo specificato"));
//			}else
//				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_INFO,"Credenziali non valide"));
//			email = null;
//			userName = null;
//		} catch (Exception e) {
//			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_INFO,"Credenziali non valide"));
//			e.printStackTrace();
//		}
//		
//		
//	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isBadCredential() {
		return badCredential;
	}

	public void setBadCredential(boolean badCredential) {
		this.badCredential = badCredential;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public OperationLogService getOperationLogService() {
		return operationLogService;
	}

	public void setOperationLogService(OperationLogService operationLogService) {
		this.operationLogService = operationLogService;
	}
	
	public void userLoginFailed(){
		HttpServletRequest requestServlet = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress=Const.EMPTY_STRING;
		TabLoginFailed firstTabLoginFailed = new TabLoginFailed();
		if(requestServlet!=null){
			ipAddress = requestServlet.getRemoteAddr();
		}
		List<TabLoginFailed> listLoginFailedByIp = new ArrayList<TabLoginFailed>();
		try {
			listLoginFailedByIp = operationLogService.getAllLoginFailedByIp(ipAddress);
		} catch (Exception e) {
		}
		TabLoginFailed tabLoginFailed = new TabLoginFailed();
		tabLoginFailed.setDescUser(loginBean.getUserName());
		tabLoginFailed.setIpUser(ipAddress);
//		Timestamp data = new Timestamp(System.currentTimeMillis());
		tabLoginFailed.setTimeLogin(Calendar.getInstance());
		try {
			operationLogService.saveLoginFailed(tabLoginFailed);
			listLoginFailedByIp.add(tabLoginFailed);
		} catch (Exception e) {
		}
		if(listLoginFailedByIp.size()== Const.ACCESS_FAILED_MAX){
//			Date appoDate =new Date();
			Calendar appoDate = Calendar.getInstance();
			appoDate.setTimeZone(TimeZone.getTimeZone("UTC"));
//			Date firstDate = new Date();
			Calendar firstDate = Calendar.getInstance();
			firstDate.setTimeZone(TimeZone.getTimeZone("UTC"));

//			Date tabDate = tabLoginFailed.getTimeLogin();
//			Calendar tabDate = Calendar.getInstance();
//			tabDate.setTimeZone(TimeZone.getTimeZone("UTC"));
//			tabDate.setTime(tabLoginFailed.getTimeLogin());
			Calendar tabDate = tabLoginFailed.getTimeLogin();
			int firstDateMin = firstDate.get(Calendar.MINUTE);
			int tabDateMin = tabDate.get(Calendar.MINUTE);
			int appoMin = appoDate.get(Calendar.MINUTE);

			for (TabLoginFailed appoTabLoginFailed : listLoginFailedByIp) {
//				appoDate = appoTabLoginFailed.getTimeLogin();
				appoDate = (appoTabLoginFailed.getTimeLogin());
				appoMin = appoDate.get(Calendar.MINUTE);
				if(firstTabLoginFailed.getTimeLogin()==null){
					firstTabLoginFailed = appoTabLoginFailed;
//				    firstDate = firstTabLoginFailed.getTimeLogin();
				    firstDate= (appoTabLoginFailed.getTimeLogin());
				    firstDateMin = firstDate.get(Calendar.MINUTE);
				}else if(firstDate.after(appoDate)){
					firstTabLoginFailed = appoTabLoginFailed;
//				    firstDate = firstTabLoginFailed.getTimeLogin();
				    firstDate = (appoTabLoginFailed.getTimeLogin());
				    firstDateMin = firstDate.get(Calendar.MINUTE);
				}
			}	
			if(firstDate.get(Calendar.HOUR_OF_DAY)==tabDate.get(Calendar.HOUR_OF_DAY)){
				if((tabDateMin-firstDateMin)<Const.MINUTE_ACCESS_POSSIBLE){
					operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
				}
			}else if(tabDate.get(Calendar.HOUR_OF_DAY)-firstDate.get(Calendar.HOUR_OF_DAY)== Const.GIORNO_CONSECUTIVO || tabDate.get(Calendar.HOUR_OF_DAY)+firstDate.get(Calendar.HOUR_OF_DAY)==Const.GIORNO_CONSECUTIVO){
				switch(firstDateMin){
				case 55:
					if(tabDateMin<1){
						operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
					}
					break;
				case 56:
					if(tabDateMin<2){
						operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
					}
					break;
				case 57:
					if(tabDateMin<3){
						operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
					}
					break;
				case 58:
					if(tabDateMin<4){
						operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
						}
					break;
				case 59:
					if(tabDateMin<5){
						operationLogService.sendMail(ipAddress, tabLoginFailed.getDescUser());
					}
					break;
				}
			}
			cleanTabLoginFailed(ipAddress);
		}

	}
	
	public void cleanTabLoginFailed(String ipAddress){
		if(ipAddress==null || ipAddress.isEmpty()){
			HttpServletRequest requestServlet = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			ipAddress = Const.EMPTY_STRING;
			if(requestServlet!=null){
				ipAddress = requestServlet.getRemoteAddr();
			}
		}
		try {
			operationLogService.removeTabLoginFailed(ipAddress);
		} catch (Exception e) {
		}
	}

	public StreamedContent getFile() {
		try {
			String nameExtention= Const.EXTENTION_PDF;
			String nameFile=Const.NAME_PDF;
			FileSystemResource fileResource = new FileSystemResource(new File(Const.PATH_MANUALE_UTENTE+nameExtention)); 
			InputStream stream = fileResource.getInputStream();
			file = new DefaultStreamedContent(stream,Const.MINETYPE_PDF, nameFile+nameExtention);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return file;
	}

	public StreamedContent getApk() {
		try {
			String filePathToBeServed = Const.PATH_APK + Const.NAME_FILE_APK + Const.EXTENTION_ZIP;
			FileSystemResource fileResource = new FileSystemResource(new File(filePathToBeServed)); 
			InputStream stream = fileResource.getInputStream();
			file = new DefaultStreamedContent(stream,Const.MINETYPE_APK,  Const.NAME_FILE_APK + Const.EXTENTION_ZIP);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
	
	public StreamedContent getManualeAndroid() {
		try {
			String nameExtention= Const.EXTENTION_PDF;
			String nameFile=Const.NAME_ANDROID_PDF;
			FileSystemResource fileResource = new FileSystemResource(new File(Const.PATH_MANUALE_ANDROID+nameExtention)); 
			InputStream stream = fileResource.getInputStream();
			file = new DefaultStreamedContent(stream,Const.MINETYPE_PDF, nameFile+nameExtention);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return file;
	}
	
	}
	
	
	
	

