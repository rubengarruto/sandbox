package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.service.ImpreseService;
import com.rextart.saw.utility.Const;


@ManagedBean(name = "impreseController")
@ViewScoped
public class ImpreseController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;


	@ManagedProperty(value="#{impreseService}")
	private ImpreseService impreseService;
	
//	@ManagedProperty(value="#{userService}")
//	private UserService userService;
	
	@ManagedProperty(value="#{arrImprese}")
	private List<TabImprese> arrImprese ;
	private TabImprese selectedImprese;
	private TabImprese newImprese;
	private CurrentUser currentUser;
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		arrImprese=impreseService.getTableImpreseList();
		setNewImprese(new TabImprese());
	}


	@Override
	public void openNew() {
		setNewImprese(new TabImprese());
	}


	@Override
	public void save() {
		try {
			TabImprese tab = impreseService.getImpresaByCF(newImprese.getDescCf());
			 if(tab == null){
				impreseService.saveImpresa(newImprese,currentUser);
				setNewImprese(new TabImprese());
				reloadTableImprese();
				RequestContext.getCurrentInstance().execute("PF('newImpresaDialog').hide()");
			 }else {
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"L'impresa con il Codice Fiscale/P.I. � gi� presente"));
			 }
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}


	public void updateImpresa() {
		try {
			TabImprese tabCfAndCodi = impreseService.getImpresaByCFAndByCodiImpresa(newImprese.getDescCf(), newImprese.getCodiImpresa());
			if(tabCfAndCodi == null){
			impreseService.updateImpresa(newImprese,currentUser);
			reloadTableImprese();
			RequestContext.getCurrentInstance().execute("PF('impresaDialog').hide()");
			}else {
				reloadTableImprese();
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"L'impresa con il Codice Fiscale/P.I. � gi� presente"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}

	@Override
	public void rimuovi() {
		if(selectedImprese==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un impresa della lista"));
			return;
		}
		try {
			setNewImprese(impreseService.getImpresaById(selectedImprese.getCodiImpresa()));
			Object[] obj = impreseService.removeImpresa(newImprese,currentUser);
			if((boolean) obj[0]){
				getArrImprese().remove(selectedImprese);
//				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, Const.SEVERITY_INFO, "Impresa rimossa correttamente"));
			}else{
				RequestContext.getCurrentInstance().showMessageInDialog( new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Cancellazione Fallita"));
			}
		} catch (Exception e) {
			RequestContext.getCurrentInstance().showMessageInDialog( new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Non � possibile rimuovere questa impresa perch� legata a dati relazionati"));
			e.printStackTrace();
		}

	}

	@Override
	public void openEdit() {
		if(selectedImprese==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un impresa della lista"));
			return;
		}
		setNewImprese(selectedImprese);
		RequestContext.getCurrentInstance().execute("PF('impresaDialog').show()");
	}

	@Override
	public void filter() {
	}

	public void filterClear() {
	}

	private void reloadTableImprese() {
		setArrImprese(impreseService.getTableImpreseList());
	}

	public ImpreseService getImpreseService() {
		return impreseService;
	}
	public void setImpreseService(ImpreseService impreseService) {
		this.impreseService = impreseService;
	}
	public List<TabImprese> getArrImprese() {
		return arrImprese;
	}
	public void setArrImprese(List<TabImprese> arrImprese) {
		this.arrImprese = arrImprese;
	}
	public TabImprese getSelectedImprese() {
		return selectedImprese;
	}
	public void setSelectedImprese(TabImprese selectedImprese) {
		this.selectedImprese = selectedImprese;
	}
	public TabImprese getNewImprese() {
		return newImprese;
	}
	public void setNewImprese(TabImprese newImprese) {
		this.newImprese = newImprese;
	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

}



