package com.rextart.saw.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabCostrModAntenna;
import com.rextart.saw.entity.TabRelCascadeCarattLov;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwAnagAntenneTmaDiplexer;
import com.rextart.saw.entity.ViwAnagLovCaratt;
import com.rextart.saw.entity.ViwElemCarattWOrder;
import com.rextart.saw.entity.ViwElementiOggetti;
import com.rextart.saw.entity.ViwRisultatoSurvey;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwTipiOggetti;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.service.FormService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SincronizzazioneService;
import com.rextart.saw.service.SurveyService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.FormUtility;
import com.rextart.saw.utility.MethodUtilities;

@ManagedBean(name = "formController")
@SessionScoped
public class FormController implements Serializable
{	
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{formService}")
	private FormService formService;

	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;

	@ManagedProperty(value="#{surveyService}")
	private SurveyService surveyService;

	@ManagedProperty(value="#{selectedSessione}")
	private ViwSessioni selectedSessione;

	@ManagedProperty(value="#{sincronizzazioneService}")
	private SincronizzazioneService sincronizzazioneService;

	@ManagedProperty(value="#{sessioniService}")
	private SessioniService sessioniService;

	private List<ViwTipiOggetti> tipologieOggetto;
	private HashMap<Integer, List<ViwElementiOggetti>> elementiTipologieOggetto;
	private HashMap<Integer, List<ViwElemCarattWOrder>> elementiCaratteristiche;
	private ArrayList<ViwElemCarattWOrder> tmpCarattOrderedLst;
	
	private List<TabCostrModAntenna> anagStrumentazioneAntenne;
	private List<ViwAnagLovCaratt> carattLovFromCatalogo;

	//	variabili per gestione casistiche fornitore\modello
	private HashMap<Integer, HashMap<String, List<ViwAnagAntenneTmaDiplexer>>> anagFornitoreModelloFromCatalogo;
	private ArrayList<String> modelliLst;
	private ArrayList<String> fornitoriLst;
	private HashMap<String, List<ViwAnagAntenneTmaDiplexer>> modelliMap;
	private HashMap<String, FornitoreBean> fornitoriUploadedMap;
	private HashMap<Integer, Visibility> visFornitoreMap;
	private HashMap<Integer, Visibility> visModelloMap;
	
	//	variabili di appoggio per intercettare il valore dei campi all'evento onChange (o blur)
	private String selectedFornitore;
	private String selectedModello;
	private String selectedFormValue;
	
	// costanti, utili per essere richiamate dal front-end 
	private String modelloDescCaratt = Constants.MODELLO;
	private String fornitoreDescCaratt = Constants.FORNITORE;
	private String sistemaDescCaratt = Constants.SISTEMA;
	private String strumentazioniAntenna = Constants.STRUMENTAZIONI_ANTENNA;
	private int codiOggettoAllegatoA = Constants.OGGETTO_A;
	private int codiOggettoAllegatoB = Constants.OGGETTO_B;
	private int codiOggettoAllegatoC = Constants.OGGETTO_C;
	private int codiOggettoAllegatoD2 = Constants.OGGETTO_D;
	private int codiOggettoAllegatoE = Constants.OGGETTO_E;
	private int codiOggettoApparatoHauwei = Constants.OGGETTO_HUAWEI;
	private int codiOggettoApparatoNokia= Constants.OGGETTO_NOKIA;
	private int codiOggettoApparatoEricsson = Constants.OGGETTO_ERICSSON;
	private int maxColumnDescLabel = Constants.MAX_COLUMN_DESC_LABEL;
	private String conformeDescLabel = Constants.CONFORME;

	private HashMap<Integer, Integer> codiSurveyMap;
	private HashMap<String, SurveyDettaglioBean> surveyDettaglioBeanMap;
	private HashMap<Integer, ArrayList<SurveyDettaglioBean>> surveyDettaglioBeanMapByCaratteristica;

	private CurrentUser currentUser;

	private Integer codiOggetto;

	private TabView tabViewWizard;

	private String descOggetto;

	private List<ViwRisultatoSurvey> listaRisultati;

	private HashMap<Integer, CascadeCaratteristicaBean> cascadeCarattLovMap;
	private HashMap<Integer, Integer> cascadeCarattPrimaryMap;
	private HashMap<Integer, Integer> cascadeElemPrimaryMap;
	private HashMap<Integer, String> selectedCascadeCarattLovMap;
	
/*		TODO unificare front-end tra gli allegati/apparati (xhtml unico) 
		Schema attuale:
		- allegati A = C = E
		- allegati B = D2
	 	- apparati Nokia = Hauwei = Ericsson*/

	//	entry point allegato
	public void buttonCallCompilazioneAllegato(ViwSessioni selectedSessione, Integer codiOggetto)
	{		
		if(FormUtility.controlloStatoVerbale(codiOggetto, selectedSessione)){

			currentUser = (CurrentUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if(FormUtility.verificaUtenti(codiOggetto, currentUser)){
				if(FormUtility.isUnlocked(codiOggetto, selectedSessione))
				{
					this.selectedSessione = selectedSessione;

					if(selectedSessione.getCodiStato().equals(Const.DISPONIBILE)){
						sessioniService.changeStatoSessione(selectedSessione.getCodiSessione(), Const.IN_LAVORAZIONE);
					}
					this.codiOggetto = codiOggetto;
					descOggetto = sincronizzazioneService.findDescOggettoByCodiOggetto(codiOggetto);
					sessioniService.lockUnLockDoc(selectedSessione.getCodiSessione(), codiOggetto, currentUser.getCodiUtente(), true);

					selectedFornitore = null;
					selectedFormValue = null;

					elementiTipologieOggetto = new HashMap<>();
					elementiCaratteristiche = new HashMap<>();
					modelliLst = new ArrayList<>();
					fornitoriLst = new ArrayList<>();
					modelliMap = new HashMap<>();
					fornitoriUploadedMap = new HashMap<>();
					visFornitoreMap = new HashMap<>();
					visModelloMap = new HashMap<>();

					loadCascadeCaratt();

					tipologieOggetto = getTipologieByOggetto(codiOggetto);
//					segue logica di ordine della versione mobile
//					TODO aggiungere colonna in ViwTipiOggetti per gestione pi� efficente dell'ordine degli elementi
					Collections.sort(tipologieOggetto, new Comparator<ViwTipiOggetti>()
							{
						@Override
						public int compare(ViwTipiOggetti t1, ViwTipiOggetti t2)
						{
							return t2.getDescLabelFilterTipoOggetto().compareTo(t1.getDescLabelFilterTipoOggetto());
						}
							});

					codiSurveyMap = new HashMap<>();
					for(ViwTipiOggetti tipoOggetto : tipologieOggetto)
					{
						Integer codiTipoOggetto = tipoOggetto.getCodiTipoOggetto();
						TabSurvey survey = surveyService.getSurveyBySessioneTipoOggetto(selectedSessione.getCodiSessione(), codiTipoOggetto);
						codiSurveyMap.put(codiTipoOggetto, survey.getCodiSurvey());
					}

					surveyDettaglioBeanMap = new HashMap<>();
					surveyDettaglioBeanMapByCaratteristica = new HashMap<>();

					carattLovFromCatalogo = formService.getAllAnagLovCarattCatalogo();
					anagStrumentazioneAntenne = anagService.getAllCostrModAntenna();
					anagFornitoreModelloFromCatalogo = new HashMap<>();

					for(ViwTipiOggetti sez : tipologieOggetto)
					{
						Integer sezId = sez.getCodiTipoOggetto();
						ArrayList<ViwElementiOggetti> listElementi = getElementiByTipo(sezId);		
						elementiTipologieOggetto.put(sezId, listElementi);

						for(ViwElementiOggetti elem : listElementi)
						{
							Integer elemId = elem.getCodiElemento();
							
//							gestione ordine caratteristiche secondo codiNextCarr
							
							List<ViwElemCarattWOrder> carattLst = formService.getCaratteristicheWithOrderByElemento(elemId);
							
//							costruzione mappa: codice caratteristica --> oggetto relativo
							HashMap<Integer, ViwElemCarattWOrder> codiCarattMap = new HashMap<>();
							for(ViwElemCarattWOrder caratt : carattLst)
								codiCarattMap.put(caratt.getCodiCaratteristica(), caratt);
									
//							costruzione mappa: codice caratteristica --> lista oggetti caratteristica che la seguono in modo diretto (attributo codiNextCaratt)
							HashMap<Integer, ArrayList<ViwElemCarattWOrder>> codiCarattSeqMap = new HashMap<>();
							for(ViwElemCarattWOrder caratt : carattLst)
							{
								ArrayList<ViwElemCarattWOrder> nextCarattLst = codiCarattSeqMap.get(caratt.getCodiCaratteristica());
								if(nextCarattLst == null) nextCarattLst = new ArrayList<ViwElemCarattWOrder>();

								ViwElemCarattWOrder nextCaratt = codiCarattMap.get(caratt.getCodiNextCaratt());
								nextCarattLst.add(nextCaratt);
								codiCarattSeqMap.put(caratt.getCodiCaratteristica(), nextCarattLst);
							}
							
							tmpCarattOrderedLst = new ArrayList<>();
							ViwElemCarattWOrder firstCaratt = carattLst.get(0); // la prima caratteristica � sempre la prima restituita dalla query perch� l'unica con flagPrimaCaratteristica = 1, unica per ogni elemento (v. query)
							sortCarattSeq(firstCaratt, codiCarattSeqMap);
							
							elementiCaratteristiche.put(elemId, tmpCarattOrderedLst);
						}
					}

					listaRisultati = sessioniService.getAllRisultatiByCodiSessioneAndCodiOggetto(selectedSessione.getCodiSessione(), codiOggetto);

					try {
						FacesContext.getCurrentInstance().getExternalContext().redirect(FormUtility.getPageNameByCodiOggetto(codiOggetto));

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				// TODO parametrizzare messages
				// formService.getMessage("erroreUtenteNonAbilitato")
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione allegato gi� preso in carico da un altro utente"));
				return;
			}
			// TODO parametrizzare messages
			// formService.getMessage("erroreAllegatoPresoInCarico")
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione utente non abilitato alla compilazione di questo allegato"));
			return;
		}
		// TODO parametrizzare messages
		// formService.getMessage("erroreEsitoAllegatoPresente")
		RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione esito dell'allegato gi� presente"));
		return;
	}
	
//	TODO ottimizzare evitando di utilizzare variabili globali
	private void sortCarattSeq(ViwElemCarattWOrder caratt, final HashMap<Integer, ArrayList<ViwElemCarattWOrder>> codiCarattSeqMap)
	{
		try
		{
			if(caratt != null) tmpCarattOrderedLst.add(caratt);
			else return;
			
			ArrayList<ViwElemCarattWOrder> nextCarattLst = codiCarattSeqMap.get(caratt.getCodiCaratteristica());
			
	//		ci si trova sull'ultima caratteristica
			if(nextCarattLst == null || nextCarattLst.isEmpty()) return;
			
			for(ViwElemCarattWOrder nextCaratt : nextCarattLst)
			{
				sortCarattSeq(nextCaratt, codiCarattSeqMap);
			}
		}
		catch(Exception e)
		{
		}
	}
	
	//	TODO gestire casistiche fornitore/modello come le caratteristiche a cascata?
	// gestione caratteristiche a cascata
	public void loadCascadeCaratt()
	{
		List<TabRelCascadeCarattLov> cascadeCarattLovLst = anagService.getRelCascadeCarattLovByCodiOggetto(codiOggetto);

		selectedCascadeCarattLovMap = new HashMap<>();
		cascadeCarattLovMap = new HashMap<>();
		cascadeCarattPrimaryMap = new HashMap<>();
		cascadeElemPrimaryMap = new HashMap<>();

		if(cascadeCarattLovLst != null & !cascadeCarattLovLst.isEmpty())
		{
			for(TabRelCascadeCarattLov cascadeCarattLov : cascadeCarattLovLst)
			{
				Integer codiCarattPrimary = cascadeCarattLov.getCodiCarattPrimary();
				String codiAnagLovPrimary = Integer.toString(cascadeCarattLov.getCodiAnagLovPrimary());
				Integer codiCaratt = cascadeCarattLov.getCodiCaratt();

				cascadeCarattPrimaryMap.put(codiCaratt, codiCarattPrimary);

				CascadeCaratteristicaBean cascadeCaratteristicaBean = cascadeCarattLovMap.get(codiCarattPrimary);

				if(cascadeCaratteristicaBean == null) cascadeCaratteristicaBean = new CascadeCaratteristicaBean();

				cascadeCaratteristicaBean.addCaratt(codiAnagLovPrimary, codiCaratt);

				cascadeCarattLovMap.put(codiCarattPrimary, cascadeCaratteristicaBean);

				selectedCascadeCarattLovMap.put(codiCarattPrimary, null);
			}
		}
	}

	public List<ViwAnagAntenneTmaDiplexer> getFromModelliMap(Integer codiCaratteristica, int colIndex, int cellIndex, int viaIndex, int eIndex)
	{
		String k = createId(codiCaratteristica, colIndex, cellIndex, viaIndex, eIndex);
		List<ViwAnagAntenneTmaDiplexer> lst = modelliMap.get(k);
		if(lst == null) return new ArrayList<>();
		else return lst;
	}

	public ArrayList<ViwElementiOggetti> getElementiByTipo(Integer idTipo)
	{
		return (ArrayList<ViwElementiOggetti>) formService.getElementiByTipo(idTipo);
	}

	public List<ViwTipiOggetti> getTipologieByOggetto(Integer idOggetto)
	{
		return formService.getTipologieByOggetto(idOggetto);
	}

	public List<ViwAnagLovCaratt> getAnagLovByCaratteristica(Integer idCaratteristica)
	{
		return formService.getAnagLovByIdCaratteristica(idCaratteristica);
	}

	public List<ViwAnagLovCaratt> getAnagLovByCaratteristica(Integer idCaratteristica, String descLabel)
	{		
		if(MethodUtilities.isNullOrEmpty(descLabel))
			return getAnagLovByCaratteristica(idCaratteristica);
		return formService.getAnagLovByCaratteristica(idCaratteristica, descLabel);
	}

	public boolean hasAnagLov(ViwElemCarattWOrder caratteristica, Integer flagIndex)
	{
		return FormUtility.hasAnagLov(caratteristica, flagIndex);
	}

	public String getDescLabelCaratByIndex(ViwElemCarattWOrder viwElemCarattWOrder, int index)
	{
		return FormUtility.getDescLabelCarat(viwElemCarattWOrder, index);
	}

	public String capitalize(String toCapitalize)
	{
		return MethodUtilities.capitalizeWords(toCapitalize);
	}

	public String createId(Object ... k)
	{
		return FormUtility.createId(k);
	}

	public ViwSessioni getSelectedSessione() {
		return selectedSessione;
	}

	public FormService getFormService() {
		return formService;
	}

	public void setFormService(FormService formService) {
		this.formService = formService;
	}

	public void setSelectedSessione(ViwSessioni selectedSessione) {
		this.selectedSessione = selectedSessione;
	}

	public List<ViwTipiOggetti> getTipologieOggetto() {
		return tipologieOggetto;
	}

	public void setTipologieOggetto(List<ViwTipiOggetti> tipologieOggetto) {
		this.tipologieOggetto = tipologieOggetto;
	}

	public HashMap<Integer, List<ViwElementiOggetti>> getElementiTipologieOggetto() {
		return elementiTipologieOggetto;
	}

	public void setElementiTipologieOggetto(HashMap<Integer, List<ViwElementiOggetti>> elementiTipologieOggetto) {
		this.elementiTipologieOggetto = elementiTipologieOggetto;
	}
	public HashMap<Integer, List<ViwElemCarattWOrder>> getElementiCaratteristiche() {
		return elementiCaratteristiche;
	}

	public void setElementiCaratteristiche(HashMap<Integer, List<ViwElemCarattWOrder>> elementiCaratteristiche) {
		this.elementiCaratteristiche = elementiCaratteristiche;
	}

	public List<ViwAnagLovCaratt> getCarattLovFromCatalogo() {
		return carattLovFromCatalogo;
	}

	public void setCarattLovFromCatalogo(
			List<ViwAnagLovCaratt> carattLovFromCatalogo) {
		this.carattLovFromCatalogo = carattLovFromCatalogo;
	}

	public boolean hasAnagLovFromCatalogo(Integer carattId, String descColumn)
	{				
		for(ViwAnagLovCaratt anagLovCaratt : carattLovFromCatalogo)
		{
			if(anagLovCaratt.getCodiCaratteristica().equals(carattId) && anagLovCaratt.getDescColumn().equals(descColumn)) 
				return true;
		}
		return false;
	}

	public List<String> getAnagFornitoreFromCatalogo(Integer codiCatalogo)
	{
//		tale gestione evita di interrogare il db pi� volte per gli stessi criteri
		
		if(codiCatalogo == null) return new ArrayList<>();
		
		HashMap<String, List<ViwAnagAntenneTmaDiplexer>> anagFornitoreModelloMap = anagFornitoreModelloFromCatalogo.get(codiCatalogo);
		if(anagFornitoreModelloMap == null)
		{
			anagFornitoreModelloMap = new HashMap<>();
			List<String> anagFornitoreLst = formService.getDistinctDescMarcaByCodiCatalogo(codiCatalogo);
			
			for(String anag : anagFornitoreLst)
				anagFornitoreModelloMap.put(anag, null);
			
			anagFornitoreModelloFromCatalogo.put(codiCatalogo, anagFornitoreModelloMap);
			
			return anagFornitoreLst;
		}
		
		return new ArrayList<>(anagFornitoreModelloMap.keySet());
	}

	private List<ViwAnagAntenneTmaDiplexer> getAnagModelloByFornitoreFromCatalogo(Integer codiCatalogo, String fornitore)
	{
//		tale gestione evita di interrogare il db pi� volte per gli stessi criteri
		
		if(codiCatalogo == null || fornitore == null) return new ArrayList<>();
		
		HashMap<String, List<ViwAnagAntenneTmaDiplexer>> anagFornitoreModelloMap = anagFornitoreModelloFromCatalogo.get(codiCatalogo);
		if(anagFornitoreModelloMap == null) anagFornitoreModelloMap = new HashMap<>();
		
		List<ViwAnagAntenneTmaDiplexer> anagModelloLst = anagFornitoreModelloMap.get(fornitore);
		if(anagModelloLst == null)
		{
			anagModelloLst = formService.getModelloByDescMarca(codiCatalogo, fornitore);
			anagFornitoreModelloMap.put(fornitore, anagModelloLst);
			anagFornitoreModelloFromCatalogo.put(codiCatalogo, anagFornitoreModelloMap);
		}

		return anagModelloLst;
	}

	public AnagService getAnagService() {
		return anagService;
	}

	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}

	public String getSelectedFornitore() {
		return selectedFornitore;
	}

	public void setSelectedFornitore(String selectedFornitore) {
		this.selectedFornitore = selectedFornitore;
	}

	public ArrayList<String> getModelli() {
		return modelliLst;
	}

	public String getModelloFromFornitore(Integer codiCaratteristica, int colIndex, int cellIndex, int viaIndex, int eIndex)
	{
		int index = fornitoriLst.indexOf(createId(codiCaratteristica, colIndex, cellIndex, viaIndex, eIndex));

		if(index == -1 || modelliLst.isEmpty() || modelliLst.size() != fornitoriLst.size()) return null;
		else return modelliLst.get(index);
	}

	public void setModelli(ArrayList<String> modelli) {
		this.modelliLst = modelli;
	}

	public ArrayList<String> getFornitori() {
		return fornitoriLst;
	}

	public void setFornitori(ArrayList<String> fornitori) {
		this.fornitoriLst = fornitori;
	}

	public void addToModelliLst(Integer codiCaratteristica, int colIndex, int cellIndex, int viaIndex, int eIndex)
	{
		modelliLst.add(createId(codiCaratteristica, colIndex, cellIndex, viaIndex, eIndex));
	}

	public void addToFornitoreLst(Integer codiCaratteristica, int colIndex, int cellIndex, int viaIndex, int eIndex)
	{
		fornitoriLst.add(createId(codiCaratteristica, colIndex, cellIndex, viaIndex, eIndex));
	}

	public String getModelloDescCaratt() {
		return modelloDescCaratt;
	}

	public void setModelloDescCaratt(String modelloDescCaratt) {
		this.modelloDescCaratt = modelloDescCaratt;
	}

	public String getFornitoreDescCaratt() {
		return fornitoreDescCaratt;
	}

	public void setFornitoreDescCaratt(String fornitoreDescCaratt) {
		this.fornitoreDescCaratt = fornitoreDescCaratt;
	}

	//	gestione delle strumentazioni antenna separata rispetto alle altre anagrafiche in quanto censite su tabelle diverse del db
	public List<TabCostrModAntenna> getAnagStrumentazioneAntenne() {
		return anagStrumentazioneAntenne;
	}

	public void setAnagStrumentazioneAntenne(
			List<TabCostrModAntenna> anagStrumentazioneAntenne) {
		this.anagStrumentazioneAntenne = anagStrumentazioneAntenne;
	}
	
	public boolean isAnagStrumentazioneAntenna(String value)
	{
		if(MethodUtilities.isNullOrEmpty(value)) return true;
		
		for(TabCostrModAntenna tabCostrModAntenna : anagStrumentazioneAntenne)
		{
			if(tabCostrModAntenna.getDescCostrMod().equals(value)) return true;
		}
		
		return false;
	}
	
	public boolean isAnagFornitore(Integer codiCatalogo, String value)
	{
		if(MethodUtilities.isNullOrEmpty(value)) return true;
		
		for(String anag : getAnagFornitoreFromCatalogo(codiCatalogo))
		{
			if(anag.equals(value)) return true;
		}
		
		return false;
	}
	
	public boolean isAnagModello(Integer codiCaratteristica, int colIndex, int cellIndex, int viaIndex, int eIndex, String value)
	{
		if(MethodUtilities.isNullOrEmpty(value)) return true;
		
		for(ViwAnagAntenneTmaDiplexer anag : getFromModelliMap(codiCaratteristica, colIndex, cellIndex, viaIndex, eIndex))
		{
			if(anag.getDescModello().equals(value)) return true;
		}
		
		return false;
	}

	public String getStrumentazioniAntenna() {
		return strumentazioniAntenna;
	}

	public void setStrumentazioniAntenna(String strumentazioniAntenna) {
		this.strumentazioniAntenna = strumentazioniAntenna;
	}

	public int getNumeSubTabToBuild(Integer numeCelle){
		if(numeCelle==null || numeCelle.equals(0)){
			return Const.CELLA_VUOTA;
		}
		return numeCelle;
	}

	public void updateSelectedValue(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE, Integer codiAnagLov, String descValore)
	{
		// TODO migliorare gestione della conversione automatica da parte di JSF per valori null in 0
		if(codiAnagLov != null && codiAnagLov.equals(0)) codiAnagLov = null;
		
		// TODO migliorare gestione della conversione automatica da parte di JSF per valori null in ""
		if(descValore != null && descValore.isEmpty()) descValore = null;

		String id = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		SurveyDettaglioBean surveyDettaglioBean = surveyDettaglioBeanMap.get(id);
		surveyDettaglioBean.setCodiAnagLov(codiAnagLov);
		surveyDettaglioBean.setDescValore(descValore);

		surveyDettaglioBeanMap.put(id, surveyDettaglioBean);

		ArrayList<SurveyDettaglioBean> surveyDettaglioBeanLst = surveyDettaglioBeanMapByCaratteristica.get(codiCaratteristica);
		surveyDettaglioBeanLst.remove(surveyDettaglioBean); // rimuove dalla lista il veccchio bean essendo equals e hashCode basati sugli attributi chiave (codi_dettaglio, codi_elemento, codi_caratteristica, desc_label, nume_cell, ecc.)

		surveyDettaglioBeanLst.add(surveyDettaglioBean); // aggiunge alla lista il nuovo bean con desc_valore e/o codi_anag_lov aggiornati
		surveyDettaglioBeanMapByCaratteristica.put(codiCaratteristica, surveyDettaglioBeanLst);
	}

	public void updateSelectedAnagLov(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE, Integer codiAnagLov)
	{
		updateSelectedValue(codiCaratteristica, numeCol, numeCella, numeVia, numeE, codiAnagLov, null);
	}

	public void updateSelectedDescValore(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE, String descValore)
	{
		updateSelectedValue(codiCaratteristica, numeCol, numeCella, numeVia, numeE, null, descValore);
	}

	public String getSelectedFormValue() {
		return selectedFormValue;
	}

	private void setSelectedFornitore(Integer codiCatalogo, Integer codiCaratteristica, int numeCol, int numeCella, int numeVia, int numeE, String value)
	{
		int index = fornitoriLst.indexOf(createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE));
		String modelloId = modelliLst.get(index);
		modelliMap.put(modelloId, getAnagModelloByFornitoreFromCatalogo(codiCatalogo, value));
	}

	//	metodo invocato dal listener all'event onChange della select box della caratteristica "Fornitore"
	public void updateSelectedFornitore(Integer codiTipoOggetto, Integer codiCatalogo, Integer codiCaratteristica, int numeCol, int numeCella, int numeVia, int numeE, String value)
	{
		// TODO migliorare gestione della conversione automatica da parte di JSF per valori null in ""
		if(value != null && value.isEmpty()) value = null;
		
		setSelectedFornitore(codiCatalogo, codiCaratteristica, numeCol, numeCella, numeVia, numeE, value);

		// ogni volta che il campo fornitore viene cambiato il relativo campo modello viene resettato
		String modelloId = getModelloFromFornitore(codiCaratteristica, numeCol, numeCella, numeVia, numeE);
		resetSelectedModello(codiTipoOggetto, Integer.parseInt(FormUtility.getIds(modelloId)[0]), Integer.parseInt(FormUtility.getIds(modelloId)[1]), Integer.parseInt(FormUtility.getIds(modelloId)[2]), Integer.parseInt(FormUtility.getIds(modelloId)[3]), Integer.parseInt(FormUtility.getIds(modelloId)[4]));
	}
	
	public void resetSelectedDescValore(Integer codiTipoOggetto, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		updateSelectedDescValore(codiCaratteristica, numeCol, numeCella, numeVia, numeE, null);
		selectedFormValue = null;
		
		String idLayout = createId("formCompilazioneCollaudo:wizard:accordionPanel", codiTipoOggetto + ":tabViewCaratt", codiCaratteristica + ":field", codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		RequestContext.getCurrentInstance().update(idLayout);
	}
	
//	reset del fornitore e del relativo modello
	public void resetSelectedFornitore(Integer codiTipoOggetto, Integer codiCatalogo, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		updateSelectedDescValore(codiCaratteristica, numeCol, numeCella, numeVia, numeE, null);
		
		updateSelectedFornitore(codiTipoOggetto, codiCatalogo, codiCaratteristica, numeCol, numeCella, numeVia, numeE, null);
		
		selectedFornitore = null;
		refreshFornitoreLayout(codiTipoOggetto, codiCaratteristica, numeCol, numeCella, numeVia, numeE); // refresh del relativo campo fornitore per ricalcolare la lista dei nuovi valori della drop-down list
	}

	public void resetSelectedModello(Integer codiTipoOggetto, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		updateSelectedDescValore(codiCaratteristica, numeCol, numeCella, numeVia, numeE, null);

		selectedModello = null;
		refreshModelloLayout(codiTipoOggetto, codiCaratteristica, numeCol, numeCella, numeVia, numeE); // refresh del relativo campo modello per ricalcolare la lista dei nuovi valori della drop-down list
	}
	
	public void refreshFornitoreLayout(Integer codiTipoOggetto, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		//	in caso di utilizzo per pi� allegati tenere in considerazione l'id che potrebbe cambiare per la presenza di nuovi componenti layout sul front-end
		String idLayout = createId("formCompilazioneCollaudo:wizard:accordionPanel", codiTipoOggetto + ":tabViewCaratt", codiCaratteristica + ":fornitore", codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		RequestContext.getCurrentInstance().update(idLayout);
	}

	public void refreshModelloLayout(Integer codiTipoOggetto, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		//	in caso di utilizzo per pi� allegati tenere in considerazione l'id che potrebbe cambiare per la presenza di nuovi componenti layout sul front-end
		String idLayout = createId("formCompilazioneCollaudo:wizard:accordionPanel", codiTipoOggetto + ":tabViewCaratt", codiCaratteristica + ":modello", codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		RequestContext.getCurrentInstance().update(idLayout);
	}

	public void uploadPrevioudAnagLov(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		String formFieldId = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		selectedFormValue = null;
		SurveyDettaglioBean surveyDettaglioBean = surveyDettaglioBeanMap.get(formFieldId);
		if(surveyDettaglioBean != null)
		{
			Integer codiAnagLov = surveyDettaglioBean.getCodiAnagLov();
			if(codiAnagLov != null)
				selectedFormValue = Integer.toString(codiAnagLov);
		}
	}

	public void uploadPreviousDescValore(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		String formFieldId = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		selectedFormValue = null;
		SurveyDettaglioBean surveyDettaglioBean = surveyDettaglioBeanMap.get(formFieldId);
		if(surveyDettaglioBean != null)
		{
			String descValore = surveyDettaglioBean.getDescValore();
			if(descValore != null)
				selectedFormValue = descValore;
		}
	}
	
	public void uploadPreviousFornitore(Integer codiTipoOggetto, Integer codiCatalogo, Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{
		String fornitoreId = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		selectedFornitore = null;
		SurveyDettaglioBean surveyDettaglioBean = surveyDettaglioBeanMap.get(fornitoreId);
		if(surveyDettaglioBean != null)
		{
			String descValore = surveyDettaglioBean.getDescValore();
			if(descValore != null)
			{
				selectedFornitore = descValore;

				FornitoreBean fornitoreBean = new FornitoreBean();
				fornitoreBean.setCodiTipoOggetto(codiTipoOggetto);
				fornitoreBean.setCodiCatalogo(codiCatalogo);
				fornitoreBean.setCodiCaratteristica(codiCaratteristica);
				fornitoreBean.setNumeCol(numeCol);
				fornitoreBean.setNumeCella(numeCella);
				fornitoreBean.setNumeVia(numeVia);
				fornitoreBean.setValue(descValore);
				fornitoreBean.setNumeE(numeE);

				fornitoriUploadedMap.put(fornitoreId, fornitoreBean);
			}
		}
	}

	//	TODO verificare se riutilizzabile in altri contesti o se da gestire come le caratteristiche a cascata
	public class FornitoreBean
	{
		private Integer codiTipoOggetto;
		private Integer codiCatalogo;
		private Integer codiCaratteristica;
		private Integer numeCol;
		private Integer numeCella;
		private Integer numeVia;
		private Integer numeE;
		private String value;

		public Integer getCodiTipoOggetto() {
			return codiTipoOggetto;
		}
		public void setCodiTipoOggetto(Integer codiTipoOggetto) {
			this.codiTipoOggetto = codiTipoOggetto;
		}
		public Integer getCodiCatalogo() {
			return codiCatalogo;
		}
		public void setCodiCatalogo(Integer codiCatalogo) {
			this.codiCatalogo = codiCatalogo;
		}
		public Integer getCodiCaratteristica() {
			return codiCaratteristica;
		}
		public void setCodiCaratteristica(Integer codiCaratteristica) {
			this.codiCaratteristica = codiCaratteristica;
		}
		public Integer getNumeCol() {
			return numeCol;
		}
		public void setNumeCol(Integer numeCol) {
			this.numeCol = numeCol;
		}
		public Integer getNumeCella() {
			return numeCella;
		}
		public void setNumeCella(Integer numeCella) {
			this.numeCella = numeCella;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public Integer getNumeVia() {
			return numeVia;
		}
		public void setNumeVia(Integer numeVia) {
			this.numeVia = numeVia;
		}
		public Integer getNumeE() {
			return numeE;
		}
		public void setNumeE(Integer numeE) {
			this.numeE = numeE;
		}
	}

	public class CascadeCaratteristicaBean
	{
		private HashMap<String, ArrayList<Integer>> anagLovCarattMap;

		//	memorizza la caratteristica subordinata al valore della LOV ed il relativo elemento a cui appartiene
		public void addCaratt(String codiAnagLovPrimary, Integer codiCaratt)
		{
			if(anagLovCarattMap == null) anagLovCarattMap = new HashMap<>();

			ArrayList<Integer> codiCarattLst = anagLovCarattMap.get(codiAnagLovPrimary);
			if(codiCarattLst == null) codiCarattLst = new ArrayList<>();

			codiCarattLst.add(codiCaratt);
			anagLovCarattMap.put(codiAnagLovPrimary, codiCarattLst);
		}

		public HashMap<String, ArrayList<Integer>> getAnagLovCarattMap() {
			return anagLovCarattMap;
		}

		public void setAnagLovCarattMap(HashMap<String, ArrayList<Integer>> anagLovCarattMap) {
			this.anagLovCarattMap = anagLovCarattMap;
		}
	}

	public void uploadPreviousModello(Integer codiCaratteristica, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeE)
	{	
		String modelloId = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeE);

		selectedModello = null;

		int index = modelliLst.indexOf(modelloId);
		FornitoreBean fornitore = fornitoriUploadedMap.get(fornitoriLst.get(index));
		// se il relativo campo fornitore � stato valorizzato allora genera la lista di valori disponibili per il modello, in caso contrario nemmeno il valore del modello � valorizzato
		if(fornitore != null && isAnagFornitore(fornitore.getCodiCatalogo(), fornitore.getValue()))
			modelliMap.put(modelloId, getAnagModelloByFornitoreFromCatalogo(fornitore.getCodiCatalogo(), fornitore.getValue()));
		else if(fornitore == null) return;

		SurveyDettaglioBean surveyDettaglioBean = surveyDettaglioBeanMap.get(modelloId);
		if(surveyDettaglioBean != null)
		{
			String descValore = surveyDettaglioBean.getDescValore();
			if(descValore != null)
			{
				selectedModello = descValore;
			}
		}
	}

	public String getSelectedModello() {
		return selectedModello;
	}

	public void setSelectedModello(String selectedModello) {
		this.selectedModello = selectedModello;
	}

	//	TODO gestire note
	public void addField(Integer codiTipoOggetto, Integer codiElemento, Integer codiCaratteristica, String descLabel, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeBranch, Integer numeSettore, Integer numeLivelloE, String descNote)
	{
		String id = createId(codiCaratteristica, numeCol, numeCella, numeVia, numeLivelloE);

		// se il campo � gi� stato mappato non deve essere registrato nuovamente. evita problemi di campi duplicati a seguito di possibili update di layout.
		if(surveyDettaglioBeanMap.containsKey(id)) return;

		// TODO migliorare gestione della conversione automatica da parte di JSF per valori null in ""
		if(descLabel != null && descLabel.isEmpty()) descLabel = null;

		Integer codiSurvey = codiSurveyMap.get(codiTipoOggetto);


		SurveyDettaglioBean surveyDettaglioBean = new SurveyDettaglioBean();

		// TODO ottimizzare query per non fare accessi al db per ogni campo
		TabSurveyDettaglio tabSurveyDettaglio = surveyService.getSurveyDettaglioByFormKey(codiSurvey, codiElemento, codiCaratteristica, descLabel, numeCella, numeVia, numeLivelloE);
		if(tabSurveyDettaglio != null)
		{
			surveyDettaglioBean.setCodiDettaglio(tabSurveyDettaglio.getCodiDettaglio());
			surveyDettaglioBean.setDescValore(tabSurveyDettaglio.getDescValore());
			surveyDettaglioBean.setCodiAnagLov(tabSurveyDettaglio.getCodiAnagLov());
		}

		surveyDettaglioBean.setCodiSurvey(codiSurvey);
		surveyDettaglioBean.setCodiElementi(codiElemento);
		surveyDettaglioBean.setCodiCaratteristiche(codiCaratteristica);
		surveyDettaglioBean.setCodiGruppo(1); // in tab_survey_dettaglio viene sempre valorizzato ad 1
		surveyDettaglioBean.setDescLabel(descLabel);
		surveyDettaglioBean.setNumeCella(numeCella);
		surveyDettaglioBean.setNumeVia(numeVia);
		surveyDettaglioBean.setNumeBranch(numeBranch);
		surveyDettaglioBean.setNumeSettore(numeSettore);
		surveyDettaglioBean.setNumeLivelloE(numeLivelloE);
		surveyDettaglioBean.setDescNote(descNote);

		surveyDettaglioBeanMap.put(id, surveyDettaglioBean);

		ArrayList<SurveyDettaglioBean> surveyDettaglioBeanLst = surveyDettaglioBeanMapByCaratteristica.get(codiCaratteristica);
		if(surveyDettaglioBeanLst == null) surveyDettaglioBeanLst = new ArrayList<>();
		surveyDettaglioBeanLst.add(surveyDettaglioBean);
		surveyDettaglioBeanMapByCaratteristica.put(codiCaratteristica, surveyDettaglioBeanLst);
	}

	public void addField(Integer codiTipoOggetto, Integer codiElemento, Integer codiCaratteristica, String descLabel, Integer numeCol, Integer numeCella, Integer numeVia, Integer numeBranch, Integer numeSettore, Integer numeLivelloE)
	{
		addField(codiTipoOggetto, codiElemento, codiCaratteristica, descLabel, numeCol, numeCella, numeVia, numeBranch, numeSettore, numeLivelloE, null);
	}

	public SurveyService getSurveyService() {
		return surveyService;
	}

	public void setSurveyService(SurveyService surveyService) {
		this.surveyService = surveyService;
	}

	public void save()
	{
		for(Integer codiCaratteristica : surveyDettaglioBeanMapByCaratteristica.keySet())
		{
			ArrayList<SurveyDettaglioBean> surveyDettaglioBeanLst = surveyDettaglioBeanMapByCaratteristica.get(codiCaratteristica);

			for(SurveyDettaglioBean surveyDettaglioBean : surveyDettaglioBeanLst)
			{
				if(surveyDettaglioBean.getCodiDettaglio() == null && surveyDettaglioBean.getCodiAnagLov() == null && surveyDettaglioBean.getDescValore() == null) continue;

				// se un solo campo � valorizzato allora salva tutti i campi della caratteristica (per garantire corretto funzionamento app mobile)
				sincronizzazioneService.saveSurveyDettaglioLst(surveyDettaglioBeanLst);
				break;
			}
		}
		unlockAllegato();
	}

	public SincronizzazioneService getSincronizzazioneService() {
		return sincronizzazioneService;
	}

	public void setSincronizzazioneService(
			SincronizzazioneService sincronizzazioneService) {
		this.sincronizzazioneService = sincronizzazioneService;
	}

	public HashMap<String, SurveyDettaglioBean> getSurveyDettaglioBeanMap() {
		return surveyDettaglioBeanMap;
	}

	public void setSurveyDettaglioBeanMap(
			HashMap<String, SurveyDettaglioBean> surveyDettaglioBeanMap) {
		this.surveyDettaglioBeanMap = surveyDettaglioBeanMap;
	}

	public void setSelectedFormValue(String selectedFormValue) {
		this.selectedFormValue = selectedFormValue;
	}

	public TabView getTabViewWizard() {
		return tabViewWizard;
	}

	public void setTabViewWizard(TabView tabViewWizard) {
		this.tabViewWizard = tabViewWizard;
	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	public SessioniService getSessioniService() {
		return sessioniService;
	}

	public void setSessioniService(SessioniService sessioniService) {
		this.sessioniService = sessioniService;
	}

	public Integer getCodiOggetto() {
		return codiOggetto;
	}

	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public String getCellaTitle(int cella)
	{
		return cella == 0 ? "" : "C " + cella;
	}

	public String getDescOggetto() {
		return descOggetto;
	}

	public void setDescOggetto(String descOggetto) {
		this.descOggetto = descOggetto;
	}

	public String getViaTitle(int via)
	{
		return via == 0 ? "" : "Via " + Integer.toString(via-1);
	}

	public String getETitle(int e)
	{
		return e == 0 ? "" : "E " + e;
	}

	public HashMap<String, List<ViwAnagAntenneTmaDiplexer>> getModelliMap() {
		return modelliMap;
	}

	public void setModelliMap(
			HashMap<String, List<ViwAnagAntenneTmaDiplexer>> modelliMap) {
		this.modelliMap = modelliMap;
	}

	public int getCodiOggettoAllegatoA() {
		return codiOggettoAllegatoA;
	}

	public void setCodiOggettoAllegatoA(int codiOggettoAllegatoA) {
		this.codiOggettoAllegatoA = codiOggettoAllegatoA;
	}

	public int getCodiOggettoAllegatoB() {
		return codiOggettoAllegatoB;
	}

	public void setCodiOggettoAllegatoB(int codiOggettoAllegatoB) {
		this.codiOggettoAllegatoB = codiOggettoAllegatoB;
	}

	public int getCodiOggettoAllegatoC() {
		return codiOggettoAllegatoC;
	}

	public void setCodiOggettoAllegatoC(int codiOggettoAllegatoC) {
		this.codiOggettoAllegatoC = codiOggettoAllegatoC;
	}

	public int getMaxColumnDescLabel() {
		return maxColumnDescLabel;
	}

	public void setMaxColumnDescLabel(int maxColumnDescLabel) {
		this.maxColumnDescLabel = maxColumnDescLabel;
	}

	public int getCodiOggettoAllegatoD2() {
		return codiOggettoAllegatoD2;
	}

	public void setCodiOggettoAllegatoD2(int codiOggettoAllegatoD2) {
		this.codiOggettoAllegatoD2 = codiOggettoAllegatoD2;
	}

	public int getCodiOggettoAllegatoE() {
		return codiOggettoAllegatoE;
	}

	public void setCodiOggettoAllegatoE(int codiOggettoAllegatoE) {
		this.codiOggettoAllegatoE = codiOggettoAllegatoE;
	}

	public int getCodiOggettoApparatoHauwei() {
		return codiOggettoApparatoHauwei;
	}

	public void setCodiOggettoApparatoHauwei(int codiOggettoApparatoHauwei) {
		this.codiOggettoApparatoHauwei = codiOggettoApparatoHauwei;
	}

	public int getCodiOggettoApparatoNokia() {
		return codiOggettoApparatoNokia;
	}

	public void setCodiOggettoApparatoNokia(int codiOggettoApparatoNokia) {
		this.codiOggettoApparatoNokia = codiOggettoApparatoNokia;
	}

	public int getCodiOggettoApparatoEricsson() {
		return codiOggettoApparatoEricsson;
	}

	public void setCodiOggettoApparatoEricsson(int codiOggettoApparatoEricsson) {
		this.codiOggettoApparatoEricsson = codiOggettoApparatoEricsson;
	}

	public String getLayoutVisibilityByCaratteristica(Integer codiCaratteristica)
	{
		Integer primaryCascadeCaratt = getPrimaryCascadeCarattByCaratt(codiCaratteristica);
		if(primaryCascadeCaratt != null)
		{
			String selectedPrimaryAnagLov = selectedCascadeCarattLovMap.get(primaryCascadeCaratt);
			if(selectedPrimaryAnagLov != null)
			{
				if(cascadeCarattLovMap.get(primaryCascadeCaratt).getAnagLovCarattMap().get(selectedPrimaryAnagLov).contains(codiCaratteristica)) return "inline";
			}
			return "none";
		}

		return "inline";
	}

	public String getLayoutPaddingByCaratteristica(Integer codiCaratteristica)
	{
		Integer primaryCascadeCaratt = getPrimaryCascadeCarattByCaratt(codiCaratteristica);
		if(primaryCascadeCaratt != null)
		{
			String selectedPrimaryAnagLov = selectedCascadeCarattLovMap.get(primaryCascadeCaratt);
			if(selectedPrimaryAnagLov != null)
			{
				if(cascadeCarattLovMap.get(primaryCascadeCaratt).getAnagLovCarattMap().get(selectedPrimaryAnagLov).contains(codiCaratteristica)) return "0.5em;";
			}
			return "0em;";
		}

		return "0.5em;";
	}

	public Integer getPrimaryCascadeCarattByCaratt(Integer codiCaratteristica)
	{
		return cascadeCarattPrimaryMap.get(codiCaratteristica);
	}

	public boolean isPrimaryCascadeCaratt(Integer codiPrimaryCaratt)
	{
		return cascadeCarattLovMap.containsKey(codiPrimaryCaratt);
	}

	public Integer getPrimaryCascadeCarattByElem(Integer codiElemento)
	{
		return cascadeElemPrimaryMap.get(codiElemento);
	}

	public void updateSelectedCascadeCaratt(Integer codiTipoOggetto, Integer codiElemento, Integer codiPrimaryCaratt, String selectedPrimaryAnagLov)
	{
		String oldSelectedPrimaryAnagLov = selectedCascadeCarattLovMap.get(codiPrimaryCaratt);

		// TODO migliorare gestione della conversione automatica da parte di JSF per valori null in ""
		String newSelectedPrimaryAnagLov = selectedPrimaryAnagLov != null && selectedPrimaryAnagLov.isEmpty() ? null : selectedPrimaryAnagLov;

		selectedCascadeCarattLovMap.put(codiPrimaryCaratt, newSelectedPrimaryAnagLov);

		try
		{
			// per evitare casi in cui l'evento onChange viene chiamato due volte consecutivamente
			if((oldSelectedPrimaryAnagLov == null && newSelectedPrimaryAnagLov == null) || (oldSelectedPrimaryAnagLov.equals(newSelectedPrimaryAnagLov))) return;
		}
		catch(NullPointerException e)
		{
			// ritorna eccezione solo nel caso in cui il vecchio valore sia null ed il nuovo no (e viceversa): in tal caso si prosegue in quanto diversi
		}

		CascadeCaratteristicaBean cascadeCaratteristicaBean = cascadeCarattLovMap.get(codiPrimaryCaratt);

		// clean & hide dei valori dei campi relativi al precedente sistema selezionato
		if(oldSelectedPrimaryAnagLov != null)
		{
			for(Integer codiCaratt : cascadeCaratteristicaBean.getAnagLovCarattMap().get(oldSelectedPrimaryAnagLov))
			{
				updateSelectedAnagLov(codiCaratt, 1, 0, 0, 0, null);

				String idLayout = createId("formCompilazioneCollaudo:wizard:accordionPanel", codiTipoOggetto + ":outputPanel", codiElemento, codiCaratt);
				selectedFormValue = null;
				RequestContext.getCurrentInstance().update(idLayout);
			}
		}

		// show dei campi relativi al nuovo sistema selezionato
		if(newSelectedPrimaryAnagLov != null)
		{
			for(Integer codiCaratt : cascadeCaratteristicaBean.getAnagLovCarattMap().get(newSelectedPrimaryAnagLov))
			{
				String idLayout = createId("formCompilazioneCollaudo:wizard:accordionPanel", codiTipoOggetto + ":outputPanel", codiElemento, codiCaratt);
				RequestContext.getCurrentInstance().update(idLayout);
			}
		}
	}

	public String getSistemaDescCaratt() {
		return sistemaDescCaratt;
	}

	public void setSistemaDescCaratt(String sistemaDescCaratt) {
		this.sistemaDescCaratt = sistemaDescCaratt;
	}

	public void setSelectedCascadeCaratt(Integer codiPrimaryCaratt, String selectedPrimaryAnagLov)
	{
		if(selectedPrimaryAnagLov != null && selectedPrimaryAnagLov.isEmpty()) selectedPrimaryAnagLov = null;
		selectedCascadeCarattLovMap.put(codiPrimaryCaratt, selectedPrimaryAnagLov);
	}

	public void showDialog(){
		RequestContext.getCurrentInstance().execute("PF('dialogAfterSave').show()");
	}

	public Boolean isRoleUserAble(Integer codiTipoOggetto){
		//Se l'utente � impresa (MOI) o Vendor non pu� visualizzare le prove funzionali negli apparati
		//TODO gestione non cablata dei codiTipoOggetto
		switch(codiTipoOggetto){
		case 10: case 20 : case 15: case 11 : case 16: case 21 :
			if(currentUser.getViwUtente().getFlgImpresa() || !currentUser.getViwUtente().getCodiVendor().equals(0)){
				return false;
			}else  
				return true;
		default : return true;
		}
	}

	public Boolean isSurveyWithState(Integer codiTipoOggetto){
		Integer codiSurvey = codiSurveyMap.get(codiTipoOggetto);
		for(ViwRisultatoSurvey survey : getListaRisultati()){
			if(survey.getCodiSurvey().equals(codiSurvey)){
				if(survey.getCodiValoreRisultatoSurvey()>=1){
					return true;
				}
			}
		}
		return false;
	}

	public List<ViwRisultatoSurvey> getListaRisultati() {
		return listaRisultati;
	}

	public void setListaRisultati(List<ViwRisultatoSurvey> listaRisultati) {
		this.listaRisultati = listaRisultati;
	}

	//TODO migliorare soluzione e rivedere la selezione del fornitore partendo dal modello
	public void onFornitoreCellaChange(TabChangeEvent event){
		try{
			Integer codiElemento = Integer.parseInt(FormUtility.getIds(event.getTab().getParent().getParent().getClientId())[2]);
			Integer codiCaratteristica = Integer.parseInt(FormUtility.getIds( event.getTab().getParent().getId().toString())[1]);
			Integer numeCella = Integer.parseInt(FormUtility.getIds(event.getTab().getClientId().toString())[4]);
			Integer indexFornitore = fornitoriLst.indexOf(createId(codiCaratteristica, 1, numeCella,0,0));
			if(indexFornitore!=null && indexFornitore>=0){
				String idModello = modelliLst.get(indexFornitore);
				Integer codiCarattMod = Integer.parseInt(FormUtility.getIds(idModello)[0]);
				Integer cellaInArray = numeCella - 1;
				if(!visFornitoreMap.get(codiCaratteristica).equals(visModelloMap.get(codiCarattMod))){
					RequestContext.getCurrentInstance().execute("PF('fieldset_"+ codiElemento+ "_" + codiCarattMod+"').toggle()");
				}
				RequestContext.getCurrentInstance().execute("PF('tabViewCaratt_" + codiCarattMod +"').select(" + cellaInArray +")");
			}
		}catch(Exception e){
		}
	}

	public void toggleEventFornitore(ToggleEvent event){
		Integer codiElemento = Integer.parseInt(FormUtility.getIds(event.getComponent().getClientId().toString())[2]);
		Integer codiCaratteristica = Integer.parseInt(FormUtility.getIds(event.getComponent().getClientId().toString())[3]);
		Integer indexForModello = fornitoriLst.indexOf(createId(codiCaratteristica, 1, 1,0,0));
		if(indexForModello!=null && indexForModello>=0){
			visFornitoreMap.put(codiCaratteristica, event.getVisibility());
			String idModello = modelliLst.get(indexForModello);
			Integer codiCarattMod = Integer.parseInt(FormUtility.getIds(idModello)[0]);
			if(visModelloMap.get(codiCarattMod)==null){
				visModelloMap.put(codiCarattMod, Visibility.HIDDEN);
			}
			if(!visFornitoreMap.get(codiCaratteristica).equals(visModelloMap.get(codiCarattMod))){
				visModelloMap.put(codiCarattMod,visFornitoreMap.get(codiCaratteristica));
				RequestContext.getCurrentInstance().execute("PF('fieldset_" +codiElemento +"_" + codiCarattMod+"').toggle()");
			}
		}
	}

	public void toggleEventModello(ToggleEvent event){
		Integer codiCaratteristica = Integer.parseInt(FormUtility.getIds(event.getComponent().getClientId().toString())[3]);
		Integer indexOfModello = modelliLst.indexOf(createId(codiCaratteristica, 1, 1,0,0));
		if(indexOfModello!=null && indexOfModello>=0){
			visModelloMap.put(codiCaratteristica, event.getVisibility());
		}
	}

	public void unlockAllegato(){
		sessioniService.lockUnLockDoc(selectedSessione.getCodiSessione(), getCodiOggetto(), currentUser.getCodiUtente(), false);
	}

	public String getTipoOggettoTitle(ViwTipiOggetti tipoOggetto)
	{
		return tipoOggetto.getDescTipoOggetto().concat(" [").concat(tipoOggetto.getDescLabelFilterTipoOggetto()).concat("]");
	}

	public String getConformeDescLabel() {
		return conformeDescLabel;
	}

	public void setConformeDescLabel(String conformeDescLabel) {
		this.conformeDescLabel = conformeDescLabel;
	}

}