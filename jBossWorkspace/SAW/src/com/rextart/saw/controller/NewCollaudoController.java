package com.rextart.saw.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.springframework.security.core.context.SecurityContextHolder;

import com.openkm.sdk4j.bean.Document;
import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.UploadFileSawBean;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabComuni;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabProvince;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.openKM.OpenKmService;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.utility.Const;


@ManagedBean(name = "newCollaudoController")
@ViewScoped
public class NewCollaudoController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{sessioniService}")
	private SessioniService sessioniService;
	private CurrentUser currentUser;
	private TabSessioni newSessione;
	private List<TabAreaCompetenza> listTabAreaCompetenza;
	private List<ViwAreaCompRegioni> listRegioni;
	private List<TabProvince> listProvincie;
	private List<TabComuni> listComuni;
	private List<TabVendors> listVendors;
	private boolean flagEricson=false;
	private boolean flagHuawei=false;
	private boolean flagNokia=false;
	private TabAnagSistemiBande tabAnagSistemiBande;
	private UploadFileSawBean uploadfileSawBean= new UploadFileSawBean();
	@ManagedProperty(value="#{openKmService}")
	OpenKmService openKmService;
	
	@ManagedProperty(value="#{repositoryMediaService}")
	private RepositoryMediaService repositoryMediaService;
	
	Object [] sessione=null;
	 
	
	@PostConstruct
	@Override
	public void init() {
		newSessione = new TabSessioni();
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		
		setListTabAreaCompetenza(sessioniService.getAllAreaCompetenza());
		setListVendors(sessioniService.getAllVendors());
		
	} 

	@Override
	public void openNew() {}

	@Override
	public void save() {
		try {
			List<ViwSessioni> sessioneExist= sessioniService.findViwSessioniByCodiCollaudo(newSessione.getCodiCollaudo());
			if(sessioneExist==null||sessioneExist.size()==Const.EMPTY_VALUE){
				String  descIdentificativo=Character.toString(newSessione.getDescCodice().toUpperCase().trim().charAt(4));
				tabAnagSistemiBande=sessioniService.findBandaSistemaByCodiImpianto(descIdentificativo);
				if(tabAnagSistemiBande==null){
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, Const.SEVERITY_ERROR,"Salvataggio fallito.Codice  non valido"));
					return;
				}
				String descNou=sessioniService.getDescGruppoByCLLI(newSessione.getClliSito());
				TabGruppi gruppo = new TabGruppi();
				if(descNou!=null && !descNou.equalsIgnoreCase(Const.EMPTY_STRING)){
					gruppo= sessioniService.getGruppoByDescGruppo(descNou);
				}
				switch (newSessione.getCodiVendor()) {
				case 1:
					flagEricson=true;
					flagHuawei=false;
					flagNokia=false;
					break;
				case 2:
					flagHuawei=true;
					flagEricson=false;
					flagNokia=false;
					break;
				case 3:
					flagNokia=true;
					flagEricson=false;
					flagHuawei=false;
					break;
				}
				newSessione.setFlgApparatoEricsson(flagEricson);
				newSessione.setFlgApparatoHuawei(flagHuawei);
				newSessione.setFlgApparatoNsn(flagNokia);
				newSessione.setCodiSistema(tabAnagSistemiBande.getCodiSistema());
				newSessione.setCodiBanda(tabAnagSistemiBande.getCodiBanda());
				if(gruppo!=null)
					newSessione.setCodiUnitaTerr(gruppo.getCodiZona());
				else
					newSessione.setCodiUnitaTerr(null);
				Object[] sessione = sessioniService.saveCollaudo(newSessione);
				if((boolean)sessione[0]){
					saveSchedaProgetto((Integer) sessione[2]);
					saveCheckList((Integer) sessione[2]);
					saveSchedaRadio((Integer) sessione[2]);
				}
				newSessione = new TabSessioni();
				uploadfileSawBean= new UploadFileSawBean();
			}else{
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Attenzione Eseste un collaudo con lo stesso ID CO"));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	public void resetSchedaProgetto(){
		uploadfileSawBean.setNomeFileSchedaProgetto(null);
		uploadfileSawBean.setFileSchedaProgetto(null);
	}
	public void uploadSchedaProgetto(FileUploadEvent event) {
		uploadfileSawBean.setNomeFileSchedaProgetto(event.getFile().getFileName());
		uploadfileSawBean.setFileSchedaProgetto(event.getFile().getContents());
    }
	public void saveSchedaProgetto(Integer codiSessione) {
		Document doc=null;
		if(uploadfileSawBean.getFileSchedaProgetto()!=null && uploadfileSawBean.getFileSchedaProgetto().length!=Const.EMPTY_VALUE){
			try {
				doc = openKmService.uploadDocumentiCollaudo(codiSessione, uploadfileSawBean.getNomeFileSchedaProgetto(), uploadfileSawBean.getFileSchedaProgetto());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TabRepositoryDocument tabRepDoc= new TabRepositoryDocument();
			tabRepDoc.setCodiSessione(codiSessione);
			tabRepDoc.setDescNome(uploadfileSawBean.getNomeFileSchedaProgetto());
			tabRepDoc.setDescPath(doc.getPath());
			tabRepDoc.setTimeCreate(new Timestamp(System.currentTimeMillis()));
			tabRepDoc.setTipoFile(Const.SCHEDA_PROGETTO);
			repositoryMediaService.saveRepositoryDocument(tabRepDoc);
		}
		
    }
	public void resetCheckList(){
		uploadfileSawBean.setNomeFileCheckList(null);
		uploadfileSawBean.setFileCheckList(null);
	}
	public void uploadCheckList(FileUploadEvent event) {
		uploadfileSawBean.setNomeFileCheckList(event.getFile().getFileName());
		uploadfileSawBean.setFileCheckList(event.getFile().getContents());
    }
	
	public void saveCheckList(Integer codiSessione) {
		Document doc=null;
		if(uploadfileSawBean.getFileCheckList()!=null && uploadfileSawBean.getFileCheckList().length!=Const.EMPTY_VALUE){
			try {
				doc = openKmService.uploadDocumentiCollaudo(codiSessione, uploadfileSawBean.getNomeFileCheckList(), uploadfileSawBean.getFileCheckList());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TabRepositoryDocument tabRepDoc= new TabRepositoryDocument();
			tabRepDoc.setCodiSessione(codiSessione);
			tabRepDoc.setDescNome(uploadfileSawBean.getNomeFileCheckList());
			tabRepDoc.setDescPath(doc.getPath());
			tabRepDoc.setTimeCreate(new Timestamp(System.currentTimeMillis()));
			tabRepDoc.setTipoFile(Const.CHECK_LIST);
			repositoryMediaService.saveRepositoryDocument(tabRepDoc);
		}
		
    }
	
	public void resetSchedaRadio(){
		uploadfileSawBean.setNomeFileSchedaRadio(null);
		uploadfileSawBean.setFileSchedaRadio(null);
	}
	public void uploadSchedaRadio(FileUploadEvent event) {
		uploadfileSawBean.setNomeFileSchedaRadio(event.getFile().getFileName());
		uploadfileSawBean.setFileSchedaRadio(event.getFile().getContents());

    }
	public void saveSchedaRadio(Integer codiSessione) {
		Document doc=null;
		if(uploadfileSawBean.getFileSchedaRadio()!=null && uploadfileSawBean.getFileSchedaRadio().length!=Const.EMPTY_VALUE){
			try {
				doc = openKmService.uploadDocumentiCollaudo(codiSessione, uploadfileSawBean.getNomeFileSchedaRadio(), uploadfileSawBean.getFileSchedaRadio());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TabRepositoryDocument tabRepDoc= new TabRepositoryDocument();
			tabRepDoc.setCodiSessione(codiSessione);
			tabRepDoc.setDescNome(uploadfileSawBean.getNomeFileSchedaRadio());
			tabRepDoc.setDescPath(doc.getPath());
			tabRepDoc.setTimeCreate(new Timestamp(System.currentTimeMillis()));
			tabRepDoc.setTipoFile(Const.SCHEDA_RADIO_ESECUTIVA);
			repositoryMediaService.saveRepositoryDocument(tabRepDoc);
		}
		
    }
	
	
	@Override
	public void openEdit() {}
	
	@Override
	public void rimuovi() {}

	@Override
	public void filter() {}

	@Override
	public void filterClear() {}


	public void listnerChangeAreaComp() {
		setListRegioni(sessioniService.getRegioneByAreaComp(newSessione.getCodiAreaComp()));
		setListProvincie(new ArrayList<TabProvince>());
		setListComuni(new ArrayList<TabComuni>());	
	}
	
	public void listnerChangeRegione() {
		String  codiRegione = sessioniService.getRegioneByNome(newSessione.getDescRegione());
		setListProvincie(sessioniService.getProvinceByIdRegione(codiRegione));
		setListComuni(new ArrayList<TabComuni>());	
	}
	
	public void listnerChangeProvincia() {
		String codiProvincia= sessioniService.getProvinciaByNome(newSessione.getDescProvincia());
		setListComuni(sessioniService.getComuneByIdProvincia(codiProvincia));	
	}
	
	public SessioniService getSessioniService() {
		return sessioniService;
	}

	public void setSessioniService(SessioniService sessioniService) {
		this.sessioniService = sessioniService;
	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	public TabSessioni getNewSessione() {
		return newSessione;
	}

	public void setNewSessione(TabSessioni newSessione) {
		this.newSessione = newSessione;
	}

	public List<TabAreaCompetenza> getListTabAreaCompetenza() {
		return listTabAreaCompetenza;
	}

	public void setListTabAreaCompetenza(
			List<TabAreaCompetenza> listTabAreaCompetenza) {
		this.listTabAreaCompetenza = listTabAreaCompetenza;
	}

	public List<ViwAreaCompRegioni> getListRegioni() {
		return listRegioni;
	}

	public void setListRegioni(List<ViwAreaCompRegioni> listRegioni) {
		this.listRegioni = listRegioni;
	}

	public List<TabProvince> getListProvincie() {
		return listProvincie;
	}

	public void setListProvincie(List<TabProvince> listProvincie) {
		this.listProvincie = listProvincie;
	}

	public List<TabComuni> getListComuni() {
		return listComuni;
	}

	public void setListComuni(List<TabComuni> listComuni) {
		this.listComuni = listComuni;
	}

	public List<TabVendors> getListVendors() {
		return listVendors;
	}

	public void setListVendors(List<TabVendors> listVendors) {
		this.listVendors = listVendors;
	}

	public boolean isFlagEricson() {
		return flagEricson;
	}

	public void setFlagEricson(boolean flagEricson) {
		this.flagEricson = flagEricson;
	}

	public boolean isFlagHuawei() {
		return flagHuawei;
	}

	public void setFlagHuawei(boolean flagHuawei) {
		this.flagHuawei = flagHuawei;
	}

	public boolean isFlagNokia() {
		return flagNokia;
	}

	public void setFlagNokia(boolean flagNokia) {
		this.flagNokia = flagNokia;
	}

	public TabAnagSistemiBande getTabAnagSistemiBande() {
		return tabAnagSistemiBande;
	}

	public void setTabAnagSistemiBande(TabAnagSistemiBande tabAnagSistemiBande) {
		this.tabAnagSistemiBande = tabAnagSistemiBande;
	}

	public UploadFileSawBean getUploadfileSawBean() {
		return uploadfileSawBean;
	}

	public void setUploadfileSawBean(UploadFileSawBean uploadfileSawBean) {
		this.uploadfileSawBean = uploadfileSawBean;
	}

	public OpenKmService getOpenKmService() {
		return openKmService;
	}

	public void setOpenKmService(OpenKmService openKmService) {
		this.openKmService = openKmService;
	}

	public RepositoryMediaService getRepositoryMediaService() {
		return repositoryMediaService;
	}

	public void setRepositoryMediaService(
			RepositoryMediaService repositoryMediaService) {
		this.repositoryMediaService = repositoryMediaService;
	}
	
}



