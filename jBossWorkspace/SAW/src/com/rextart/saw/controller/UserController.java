package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.UserFilter;
import com.rextart.saw.entity.TabAnagStatiUtente;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabRole;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwProfiloUtente;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwZoneRegioni;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.ConstInterfaces;

@ManagedBean(name = "userController")
@ViewScoped
public class UserController implements Serializable, Controller{
	
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{userService}")
	private UserService userService;
	@ManagedProperty(value="#{arrUtenti}")
	private List<ViwUtenti> arrUtenti;
	private TabUtente newUser;
    private ViwUtenti selectedUser;
	private CurrentUser currentUser;
	private UserFilter userFilter;
	private List<TabRole> listRoles;
	private List<TabAreaCompetenza> listAreeComp;
	private List<ViwProfiloUtente> listStrutture;
	private List<TabZone>listZone;
	private List<ViwAreaCompRegioni> listRegioni;
	//nuove zone
	private List<ViwZoneRegioni> listNewZone;
	private List<String> selectedListDescRegione;
	private List<Integer> selectedRegioni;
	private List<TabGruppi>listGruppi;
	private List<TabStruttura> listStruttureFilter;
	private Integer codiImpSelected;
	private List<TabImprese> listImprese;
	private List<TabVendors>listVendor;
	private boolean cessato=false;
	private boolean renderedZona=false;
	private boolean renderedGruppo=false;
	private boolean renderedAreaComp=true;
	private boolean renderedVendor=false;
	private boolean renderedRegione=false;
	private List<TabAnagStatiUtente> statiUtente;
	
	@PostConstruct
	@Override
	public void init() {
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof CurrentUser){
			setCurrentUser((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		}
		userFilter= new UserFilter();
		setArrUtenti(userService.getFilterViwUser(userFilter));
		setListZone(new ArrayList<TabZone>());
		setListNewZone(new ArrayList<ViwZoneRegioni>());
		setListGruppi(new ArrayList<TabGruppi>());
		setListAreeComp(userService.getAllAreeComp());
		setListRoles(userService.getAllRoles());
		setListStrutture(new ArrayList<ViwProfiloUtente>());
		setListStruttureFilter(userService.getAllStrutture());
		setListImprese(userService.getAllImprese());
		setListVendor(new ArrayList<TabVendors>());
		setNewUser(new TabUtente());
		setSelectedUser(new ViwUtenti());
		setStatiUtente(userService.getAllStatiUtente());
	}
	
	@Override
	public void openNew() {
		try {
			setNewUser(new TabUtente());
			renderedZona=false;
			renderedGruppo=false;
			renderedAreaComp=true;
			renderedVendor=false;
			renderedRegione=false;
		} catch (Exception e) {
			
		}
	}
	
	
	public void listnerChangeRoles() {
		try {
			newUser.setCodiStruttura(null);
			setListStrutture(userService.getStruttureByRole(newUser.getCodiRole()));
			newUser.setCodiAreaComp(null);
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			selectedListDescRegione = new ArrayList<String>();
			if(newUser.getCodiRole()==ConstInterfaces.UTENTE_VENDOR){
				setListVendor(userService.getAllVendor());
				renderedAreaComp=false;
				selectedRegioni = new ArrayList<Integer>();
				renderedRegione=false;
				renderedVendor=true;
			}else if((newUser.getCodiRole()==Const.Referente_Territoriale_OP) || (newUser.getCodiRole()==Const.Referente_Territoriale) || (newUser.getCodiRole()==Const.Coordinatore_Operativo) || (newUser.getCodiRole()==Const.Operatore)){
				setListAreeComp(userService.getAllAreeCompNotNazionale());
				renderedAreaComp=true;
				renderedVendor=false;
				renderedRegione=true;
				newUser.setCodiVendor(Const.EMPTY_VALUE);
			}else if((newUser.getCodiRole()==Const.DG_OP)){
				setListAreeComp(userService.getAllAreeCompNotNazionale());
				renderedAreaComp=true;
				renderedVendor=false;
				renderedRegione=false;
				newUser.setCodiVendor(Const.EMPTY_VALUE);
			}else{
				setListAreeComp(userService.getAllAreeComp());
				renderedAreaComp=true;
				renderedVendor=false;
				renderedRegione=false;
				newUser.setCodiVendor(Const.EMPTY_VALUE);
			}
			newUser.setCodiZona(null);
			setListZone(new ArrayList<TabZone>());
			newUser.setCodiGruppo(null);
			setListGruppi(new ArrayList<TabGruppi>());
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());		
			renderedZona=false;
			renderedGruppo=false;
			} catch (Exception e) {
		}
	}
	
	public void listnerChangeAreaCompetenza() {
		try {
			newUser.setCodiZona(null);
			//setListZone(userService.getTabZoneByAreaComp(newUser.getCodiAreaComp()));
			setListRegioni(userService.getViwRegioniByAreaComp(newUser.getCodiAreaComp()));
			loadElencoRegioneTooltip();
			newUser.setCodiGruppo(null);
			setListGruppi(new ArrayList<TabGruppi>());
			selectedListDescRegione = new ArrayList<String>();
			selectedRegioni = new ArrayList<Integer>();
			listNewZone = new ArrayList<ViwZoneRegioni>();
			} catch (Exception e) {
		}
	}
	
	public void listnerChangeRegione() {
		try {
			//newUser.setCodiZona(null);
			 listNewZone = new ArrayList<ViwZoneRegioni>();
			 newUser.setCodiZona(null);
			 setListNewZone(userService.getViwZoneRegioni(selectedRegioni));
			 listGruppi= new ArrayList<TabGruppi>();
			 newUser.setCodiGruppo(null);
			} catch (Exception e) {
		}
	}
	
	public void loadElencoRegioneTooltip() {
		selectedListDescRegione = new ArrayList<String>();
		
		for (Integer codiRegione : selectedRegioni) {
			ViwAreaCompRegioni regioni = userService.getRegioneByCodi(codiRegione);
			selectedListDescRegione.add(regioni.getNomeMaiuscolo());
		}
	}
	
	public void listnerChangeStruttura() {
		try {
			if(newUser.getCodiRole().equals(Const.UTENTE_VENDOR)){
				newUser.setCodiAreaComp(ConstInterfaces.NAZIONALE);
			}else{
				newUser.setCodiAreaComp(null);
				newUser.setCodiVendor(Const.EMPTY_VALUE);
			}
			if((newUser.getCodiRole()==Const.DG_OP) || (newUser.getCodiRole()==Const.Referente_Territoriale_OP) || (newUser.getCodiRole()==Const.Referente_Territoriale) || (newUser.getCodiRole()==Const.Coordinatore_Operativo) || (newUser.getCodiRole()==Const.Operatore))
				setListAreeComp(userService.getAllAreeCompNotNazionale());
			else
				setListAreeComp(userService.getAllAreeComp());
			newUser.setCodiZona(null);
			setListZone(userService.getTabZoneByAreaComp(newUser.getCodiAreaComp()));
			newUser.setCodiGruppo(null);
			setListGruppi(new ArrayList<TabGruppi>());
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			listNewZone = new ArrayList<ViwZoneRegioni>();
			selectedListDescRegione = new ArrayList<String>();
			if(newUser.getCodiStruttura()== ConfigurationController.STRUTTURA_AOL || newUser.getCodiStruttura()== ConfigurationController.STRUTTURA_AOU){
				renderedZona=true;
			}else{
				renderedZona=false;
				newUser.setCodiZona(null);
				newUser.setCodiGruppo(null);
				renderedGruppo=false;
			}
			if(newUser.getCodiStruttura()== ConfigurationController.STRUTTURA_AOU){
				renderedGruppo=true;
			}else{
				renderedGruppo=false;
				newUser.setCodiGruppo(null);
			}
		} catch (Exception e) {
		}
	}
	
	public void listnerChangeZone() {
		try {
			setListGruppi(userService.getTabGruppiByZona(newUser.getCodiZona()));
			} catch (Exception e) {
		}
	}
	
	public void listnerFlagImpresa(){
		if(newUser.isFlgImpresa()){
			newUser.setCodiAreaComp(null);
			setListAreeComp(userService.getAllAreeCompNotNazionale());
			renderedAreaComp=true;
			renderedRegione = false;
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			setListNewZone(new ArrayList<ViwZoneRegioni>());
			selectedRegioni = new ArrayList<Integer>();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_INFO,"L'username dell'Utente MOI sar� assegnato da sistema!"));
		}else{
			setListAreeComp(userService.getAllAreeComp());
			renderedVendor=false;
			renderedZona = false;;
			renderedRegione = false;
			renderedGruppo = false;
			setNewUser(new TabUtente());
			
		}
			
	}
	
	@Override
	public void openEdit() {
		if(selectedUser==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un utente della lista"));
			setSelectedUser(new ViwUtenti());
			return;
		}
		setCessato(false);
		setNewUser(userService.getUtenteById(selectedUser.getCodiUtente()));
		setListStrutture(userService.getStruttureByRole(selectedUser.getCodiRole()));
		// carico regioni
		setListRegioni(userService.getViwRegioniByAreaComp(selectedUser.getCodiAreaComp()));
		selectedRegioni = userService.getRegioniSelectedByCodiUtente(selectedUser.getCodiUtente());
		if(selectedRegioni.size()>0 && selectedRegioni != null) {
		setListNewZone(userService.getViwZoneRegioni(selectedRegioni));
		setListGruppi(userService.getTabGruppiByZona(selectedUser.getCodiZona()));
		} 
		if((selectedUser.getCodiRole()== Const.Referente_Territoriale_OP) || (selectedUser.getCodiRole()==Const.Referente_Territoriale) || (selectedUser.getCodiRole()==Const.Coordinatore_Operativo) || (selectedUser.getCodiRole()==Const.Operatore)) {
			renderedAreaComp=true;
			renderedVendor=false;
			if(selectedUser.getCodiStruttura() == Const.Ditte_MOI) {
				renderedRegione=false;
				selectedRegioni = new ArrayList<Integer>();
				setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			}
			else {
			renderedRegione=true;
			}
			if(selectedUser.getCodiStruttura() == ConfigurationController.STRUTTURA_AOL || selectedUser.getCodiStruttura() == ConfigurationController.STRUTTURA_AOU) {
				renderedZona=true;
				renderedGruppo=true;
			} else {
				renderedZona=false;
				renderedGruppo=false;
			}		
		}else if((selectedUser.getCodiRole()==Const.DG_OP)){
			//setListAreeComp(userService.getAllAreeCompNotNazionale());
			renderedAreaComp=true;
			renderedVendor=false;
			renderedRegione=false;
		} else if ((selectedUser.getCodiRole()==Const.Ditte_MOI)) {
			renderedAreaComp=true;
			renderedVendor=false;
			renderedRegione=false;
			selectedRegioni = new ArrayList<Integer>();
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
		}
		else{
			renderedAreaComp=true;
			renderedVendor=false;
			renderedGruppo=false;
			renderedRegione=false;
			renderedZona = false;
		}
		if(selectedUser.getCodiRole()== ConfigurationController.UTENTE_VENDOR){
			setListVendor(userService.getAllVendor());
			selectedRegioni = new ArrayList<Integer>();
			setListRegioni(new ArrayList<ViwAreaCompRegioni>());
			renderedAreaComp=false;
			renderedZona=false;
			renderedGruppo=false;
			renderedVendor=true;
			renderedRegione=false;
		}
			
		RequestContext.getCurrentInstance().execute("PF('userDialog').show()");
	}

	private void reloadTableUtenti() {
		RequestContext.getCurrentInstance().execute("PF('userTableWV').clearFilters()");
			setArrUtenti(userService.getFilterViwUser(userFilter));

	}
	
	
	@Override
	public void save() {
		try {
			Object[] result;
			result = userService.saveUser(newUser,codiImpSelected,currentUser, selectedRegioni);
			if((Integer)result[1]==1){
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Utente gi� presente in archivio"));
			}else{
				reloadTableUtenti();
				RequestContext.getCurrentInstance().execute("PF('newUserDialog').hide()");
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
		
	public void update() {
		try {
			userService.updateUser(newUser,selectedUser.getCodiImpresa(),cessato,currentUser, selectedRegioni);
			reloadTableUtenti();
			RequestContext.getCurrentInstance().execute("PF('userDialog').hide()");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
	
	public void resetPassword() {
		try {
			userService.resetPassword(newUser);
			reloadTableUtenti();
			RequestContext.getCurrentInstance().execute("PF('userDialog').hide()");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
	public void abilitaUtente() {
		try {
			setNewUser(userService.getUserById(selectedUser.getCodiUtente()));
			userService.abilitaUtente(newUser,currentUser);
			reloadTableUtenti();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
	
	public void disabilitaUtente() {
		try {
			setNewUser(userService.getUserById(selectedUser.getCodiUtente()));
			userService.disabilitaUtente(newUser,currentUser);
			reloadTableUtenti();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
	
	
	@Override
	public void rimuovi() {}




	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<ViwUtenti> getArrUtenti() {
		return arrUtenti;
	}

	public void setArrUtenti(List<ViwUtenti> arrUtenti) {
		this.arrUtenti = arrUtenti;
	}

	public TabUtente getNewUser() {
		return newUser;
	}

	public void setNewUser(TabUtente newUser) {
		this.newUser = newUser;
	}

	public ViwUtenti getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(ViwUtenti selectedUser) {
		this.selectedUser = selectedUser;
	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	@Override
	public void filter() {
		RequestContext.getCurrentInstance().execute("PF('userTableWV').clearFilters()");
		setArrUtenti(userService.getFilterViwUser(userFilter));
		
	}

	@Override
	public void filterClear() {
		RequestContext.getCurrentInstance().execute("PF('userTableWV').clearFilters()");
		userFilter = new UserFilter();
		setArrUtenti(userService.getFilterViwUser(userFilter));
		
	}

	public void registerUser() {
		try {
			Object[] result;
			result = userService.registerUser(newUser,codiImpSelected,selectedRegioni);
			newUser = new TabUtente();
			if((Integer)result[1]==1){
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Utente gi� presente in archivio"));
			}
			if((boolean)result[0]){
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Credenziali create, attendere abilitazione utente"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
		}
	}
	
	public UserFilter getUserFilter() {
		return userFilter;
	}

	public void setUserFilter(UserFilter userFilter) {
		this.userFilter = userFilter;
	}

	public List<TabRole> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<TabRole> listRoles) {
		this.listRoles = listRoles;
	}

	public List<TabAreaCompetenza> getListAreeComp() {
		return listAreeComp;
	}
	
	public void setListAreeComp(List<TabAreaCompetenza> listAreeComp) {
		this.listAreeComp = listAreeComp;
	}

	public List<ViwProfiloUtente> getListStrutture() {
		return listStrutture;
	}

	public void setListStrutture(List<ViwProfiloUtente> listStrutture) {
		this.listStrutture = listStrutture;
	}

	public List<TabImprese> getListImprese() {
		return listImprese;
	}

	public void setListImprese(List<TabImprese> listImprese) {
		this.listImprese = listImprese;
	}

	public Integer getCodiImpSelected() {
		return codiImpSelected;
	}

	public void setCodiImpSelected(Integer codiImpSelected) {
		this.codiImpSelected = codiImpSelected;
	}

	public boolean isCessato() {
		return cessato;
	}

	public void setCessato(boolean cessato) {
		this.cessato = cessato;
	}

	public List<TabStruttura> getListStruttureFilter() {
		return listStruttureFilter;
	}

	public void setListStruttureFilter(List<TabStruttura> listStruttureFilter) {
		this.listStruttureFilter = listStruttureFilter;
	}

	public List<TabZone> getListZone() {
		return listZone;
	}

	public void setListZone(List<TabZone> listZone) {
		this.listZone = listZone;
	}

	public List<TabGruppi> getListGruppi() {
		return listGruppi;
	}

	public void setListGruppi(List<TabGruppi> listGruppi) {
		this.listGruppi = listGruppi;
	}

	public boolean isRenderedZona() {
		return renderedZona;
	}

	public void setRenderedZona(boolean renderedZona) {
		this.renderedZona = renderedZona;
	}

	public boolean isRenderedGruppo() {
		return renderedGruppo;
	}

	public void setRenderedGruppo(boolean renderedGruppo) {
		this.renderedGruppo = renderedGruppo;
	}

	public boolean isRenderedAreaComp() {
		return renderedAreaComp;
	}

	public void setRenderedAreaComp(boolean renderedAreaComp) {
		this.renderedAreaComp = renderedAreaComp;
	}

	public boolean isRenderedVendor() {
		return renderedVendor;
	}

	public void setRenderedVendor(boolean renderedVendor) {
		this.renderedVendor = renderedVendor;
	}

	public List<TabVendors> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<TabVendors> listVendor) {
		this.listVendor = listVendor;
	}
	
	public List<TabAnagStatiUtente> getStatiUtente() {
		return statiUtente;
	}

	public void setStatiUtente(List<TabAnagStatiUtente> statiUtente) {
		this.statiUtente = statiUtente;
	}

	public List<ViwAreaCompRegioni> getListRegioni() {
		return listRegioni;
	}

	public void setListRegioni(List<ViwAreaCompRegioni> listRegioni) {
		this.listRegioni = listRegioni;
	}

	public List<Integer> getSelectedRegioni() {
		return selectedRegioni;
	}

	public void setSelectedRegioni(List<Integer> selectedRegioni) {
		this.selectedRegioni = selectedRegioni;
	}

	public List<ViwZoneRegioni> getListNewZone() {
		return listNewZone;
	}

	public void setListNewZone(List<ViwZoneRegioni> listNewZone) {
		this.listNewZone = listNewZone;
	}

	public boolean isRenderedRegione() {
		return renderedRegione;
	}

	public void setRenderedRegione(boolean renderedRegione) {
		this.renderedRegione = renderedRegione;
	}

	public List<String> getSelectedListDescRegione() {
		return selectedListDescRegione;
	}

	public void setSelectedListDescRegione(List<String> selectedListDescRegione) {
		this.selectedListDescRegione = selectedListDescRegione;
	}
	
}
