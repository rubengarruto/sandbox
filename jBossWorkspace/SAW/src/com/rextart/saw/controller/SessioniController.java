package com.rextart.saw.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.ConnectException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.springframework.security.core.context.SecurityContextHolder;

import com.openkm.sdk4j.bean.Document;
import com.rextart.saw.bean.AllegatoD1Bean;
import com.rextart.saw.bean.CollaudoDatiGeneraliBean;
import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.SessioniFilter;
import com.rextart.saw.bean.WorkRequestBean;
import com.rextart.saw.entity.TabAnagDocumenti;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagWorkRequest;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCountDocument;
import com.rextart.saw.entity.TabDettaglioCollaudo;
import com.rextart.saw.entity.TabFinalitaCollaudo;
import com.rextart.saw.entity.TabImpianti;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabPoloTecnologico;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSchedaDyn;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabWorkRequestColl;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwRisultatoSurvey;
import com.rextart.saw.entity.ViwSbloccoSezioni;
import com.rextart.saw.entity.ViwSessImpUser;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwSessioniNote;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.openKM.OpenKmService;
import com.rextart.saw.openKM.ServiceRepositoryImpl;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.service.ReportService;
import com.rextart.saw.service.RepositoryMediaService;
import com.rextart.saw.service.SessioniService;
import com.rextart.saw.service.SincronizzazioneService;
import com.rextart.saw.service.SurveyService;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.ws.collaudoToSaw.CompletamentoCollaudoApparatiRequest;
import com.rextart.saw.ws.collaudoToSaw.CompletamentoCollaudoApparatiResponse;
import com.rextart.saw.ws.collaudoToSaw.SawIn;
import com.rextart.saw.ws.collaudoToSaw.SawIn_Service;
import com.rextart.saw.ws.dyn.Allegato;
import com.rextart.saw.ws.dyn.OutputScheda;
import com.rextart.saw.ws.dyn.OutputStatoScheda;
import com.rextart.saw.ws.dyn.ServiceDynToSaw;
import com.rextart.saw.ws.dyn.ServiceDynToSawService;


@ManagedBean(name = "sessioniController")
@ViewScoped
public class SessioniController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{sessioniService}")
	private SessioniService sessioniService;

	@ManagedProperty(value="#{reportService}")
	private ReportService reportService;
	
	@ManagedProperty(value="#{arrCollaudiInLavorazione}")
	private List<ViwSessioni> arrCollaudiInLavorazione ;

	@ManagedProperty(value="#{arrCollaudiArchiviati}")
	private List<ViwSessioni> arrCollaudiArchiviati ;
		
	@ManagedProperty(value="#{openKmService}")
	OpenKmService openKmService;
	
	@ManagedProperty(value="#{repositoryMediaService}")
	private RepositoryMediaService repositoryMediaService;
	
	@ManagedProperty(value="#{surveyService}")
	private SurveyService surveyService;
	
	@ManagedProperty(value="#{sincronizzazioneService}")
	private SincronizzazioneService sincronizzazioneService;
	
	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;

	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	private ViwSessioni selectedSessione;
	private ViwSbloccoSezioni selectedViwSbloccoSezioni;
	private CollaudoDatiGeneraliBean selectedBean;
	private WorkRequestBean workRequestBean;
	private List<TabImpianti> impiantiList;
	private List<TabSistemi> sistemiList;
	private List<TabBande> bandeList;
	private List<TabFinalitaCollaudo> finalitaCollList;
	private List<TabDettaglioCollaudo> dettagliCollList;
	private List<TabZone> unitaTerrList;
	private List<TabAnagWorkRequest> anagWorkRequestList;
	private List<TabImprese> impreseList;
	private DualListModel<String> utentiToAssoc;
	private HashMap<String,ViwUtenti> utentiHashMap = new HashMap<>();
	private Integer telecomOrImpUser;
	private Integer codiImpresa;
	private List<TabWorkRequestColl> workRequestCollList;
	private TabWorkRequestColl selectedWorkRequest;
	private List<TabRelRitardiSessioni> listRitardi;

	private CurrentUser currentUser;
	private Boolean flgProcessed=false;
	private Boolean flagAllegati=false;
	private Boolean flagApparati=false;
	private Boolean flagWorkRequest=false;
	
	private AllegatoD1Bean allegatoD1Bean;
	private List<TabRepositoryDocument> listDocument;
	private List<TabRepositorySchedaRischi>listDocumentDyn;
	private TabRepositoryDocument selectedDocument;
	private List<TabRepositoryMedia> listFoto;
	private TabRepositoryMedia selectedFoto;
	private Boolean flagVerbaleD1=false;
	private TabSchedaDyn tabSchedaDin;
	private List<ViwSbloccoSezioni> viwSbloccoSezioniList; 
	private List<TabAnagDocumenti> listAnagDocumenti;
	private TabAnagDocumenti anagDocumenti;
	private boolean uploadPanelVisible=false;
	private boolean sessioniUserImp=false;
	private boolean sessioniArchiviateUserImp=false;
	private List<ViwSessImpUser> arrDitte=null;
	private List<ViwSessImpUser> arrUser=null;
	private ViwSessImpUser selectedImpresa;
	private TreeNode rootSessione;
	private TreeNode rootMotivazioneEsiti= new DefaultTreeNode();
	private List<TreeNode> nodeDitte = new ArrayList<>();
	private List<ViwVerbale> listVerbale;
	private SessioniFilter sessioniFilter;
	private List<TabAnagStatiSessione> listStati;
	private List<ViwRisultatoSurvey> listRisultati;
	private List<TreeNode> nodeRisultati = new ArrayList<>();
	private List<ViwEsitoSurvey> listNoteEsito = new ArrayList<ViwEsitoSurvey>();
	private List<TabNote> listNoteDitta = new ArrayList<TabNote>();
	private TabNote tabNote;
	private List<TabNote> listTabNote = new ArrayList<TabNote>();
	private String descOggettiTabRisSurvey;
	private TabNote selectedNota;
	private ViwSessioni sessioneSelezionata = new ViwSessioni();
	private List<Integer> codiUtentiByRegione = new ArrayList<Integer>();
	private HashMap<Integer, String> oggettiMap = new HashMap<Integer,String>();
	private HashMap<Integer, ViwSessioniNote> sessioniNoteMap= new HashMap<Integer, ViwSessioniNote>();

	@PostConstruct
	@Override
	public void init() {
		
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		if(currentUser.getViwUtente().getCodiAreaComp()!=1 && !currentUser.getViwUtente().getFlgImpresa() && !(currentUser.getViwUtente().getCodiRole().equals(Const.DG_OP))){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByAreaComp(currentUser.getViwUtente().getCodiAreaComp(),null);
		}else if(currentUser.getViwUtente().getCodiVendor()!=Const.EMPTY_VALUE){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByUserVendor(currentUser.getViwUtente().getCodiVendor(),null);
		}else if(currentUser.getViwUtente().getFlgImpresa()){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoi(currentUser.getViwUtente().getCodiImpresa(),null);
		}else{
			arrCollaudiInLavorazione=sessioniService.getAllCollaudiInLavorazione(null);
		}
		if(currentUser.getViwUtente().getCodiAreaComp()!=1 && !currentUser.getViwUtente().getFlgImpresa()  && !(currentUser.getViwUtente().getCodiRole().equals(Const.DG_OP))){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByAreaComp(currentUser.getViwUtente().getCodiAreaComp());
		}else if(currentUser.getViwUtente().getCodiVendor()!=Const.EMPTY_VALUE){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByUserVendor(currentUser.getViwUtente().getCodiVendor());
		}else if(currentUser.getViwUtente().getFlgImpresa()){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByMoi(currentUser.getViwUtente().getCodiImpresa());
		}else{
			arrCollaudiArchiviati=sessioniService.getAllCollaudiArchiviati();
		}
		
		fillSessioniNoteMap(arrCollaudiInLavorazione, arrCollaudiArchiviati);
		setListStati(anagService.getAllAnagStatiSessioneNotClosed());
		setImpiantiList(sessioniService.getAllImpianti());
		setSistemiList(sessioniService.getAllSistemi());
		setDettagliCollList(sessioniService.getAllDettagliColl());
		setFinalitaCollList(sessioniService.getAllFinalitaColl());
		setUnitaTerrList(new ArrayList<TabZone>());
		setSelectedBean(new CollaudoDatiGeneraliBean());
		setBandeList(sessioniService.getAllBande());
		setUtentiToAssoc(new DualListModel<>(new ArrayList<String>(), new ArrayList<String>()));
		setAnagWorkRequestList(new ArrayList<TabAnagWorkRequest>());
		setWorkRequestCollList(new ArrayList<TabWorkRequestColl>());
		reloadComboRisultati();
		if(arrCollaudiInLavorazione!=null && arrCollaudiInLavorazione.size()>0){
			listVerbale = sessioniService.getListVerbaliBySessioniInLavorazione(arrCollaudiInLavorazione);
		}else{
			listVerbale = new ArrayList<ViwVerbale>();
		}
		setSessioniFilter(new SessioniFilter());
		
		tabNote = new TabNote();
		selectedNota = new TabNote();
		fillOggettiMap();
		}

	public Integer isFlgVerbaleParziale(Integer codiOggetto, Integer codiSessione){
		for(ViwVerbale viwVerbale : listVerbale){
			if(viwVerbale.getCodiSessione().equals(codiSessione) && viwVerbale.getCodiOggetto().equals(codiOggetto)){
				return viwVerbale.getFlgParziale();
			}
		}
		return Const.EMPTY_VALUE;		
	}
	
	public void arrCollaudiInLavorazioneMoiCheck(){
		if(!sessioniUserImp){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoi(currentUser.getViwUtente().getCodiImpresa(),sessioniFilter.getCodiStato());
		}else{
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoiAssoc(currentUser.getViwUtente().getCodiUtente(),sessioniFilter.getCodiStato());
		}
		RequestContext.getCurrentInstance().execute("PF('sessioniTableWV').clearFilters()");
	}
	
	public void arrCollaudiArchiviatiMoiCheck(){
		if(!sessioniArchiviateUserImp){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByMoi(currentUser.getViwUtente().getCodiImpresa());
		}else{
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByMoiAssoc(currentUser.getViwUtente().getCodiUtente());
		}
	}
	
	private void reloadComboRisultati(){
		HashMap<Integer,String> mapRisultatoSurvey = new HashMap<Integer,String>();
		mapRisultatoSurvey.put(1, "POSITIVO");
    	mapRisultatoSurvey.put(2, "POSITIVO_CON_RISERVA");
    	mapRisultatoSurvey.put(3, "NEGATIVO");
    	allegatoD1Bean = new AllegatoD1Bean();
    	allegatoD1Bean.setMapRisultatoSurvey(mapRisultatoSurvey);
	}

	@Override
	public void openNew() {}

	@Override
	public void save() {
		Boolean saveOk = sessioniService.mergeCollaudo(selectedBean,currentUser.getCodiUtente());
		if(saveOk){
			RequestContext.getCurrentInstance().execute("PF('collaudiEditInLavorazioneDialog').hide()");
			reloadCollaudiInLavorazione();
		}else
			FacesContext.getCurrentInstance().addMessage("formCollaudiInLavorazione:messagesEdit", new FacesMessage(FacesMessage.SEVERITY_ERROR, Const.SEVERITY_ERROR,"Salvataggio fallito. Riprovare."));

	}
	
	public void saveWorkRequest() {
		TabWorkRequestColl savedWorkRequestColl = sessioniService.saveWorkRequest(workRequestBean,currentUser.getCodiUtente());
		if(savedWorkRequestColl!=null){
			setWorkRequestBean(new WorkRequestBean(selectedSessione.getCodiBanda(), selectedSessione.getCodiSessione(), selectedSessione.getCodiSistema()));
			workRequestCollList.add(savedWorkRequestColl);
		}else{
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione Numero Work Request gi� presente per il collaudo selezionato"));
		}
	}

	
	/**
	 * Elimina WorkRequest collaudo selezionato**/
	@Override
	public void rimuovi() {
		Boolean removeOk = sessioniService.removeWorkRequest(selectedWorkRequest, currentUser.getCodiUtente());
		if(removeOk){
			setWorkRequestCollList(sessioniService.getWorkRequestBySessione(selectedSessione.getCodiSessione()));
		}
	}
			 

	@Override
	public void openEdit() {}
	
	
	public void openWorkRequestPanel() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		if(selectedSessione.getCodiBanda()==null || selectedSessione.getCodiSistema()==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione questo collaudo non � ancora associato ad un sistema o ad una rete"));
			return;			
		}
		setWorkRequestCollList(sessioniService.getWorkRequestBySessione(selectedSessione.getCodiSessione()));
		setAnagWorkRequestList(sessioniService.getAllAnagWorkRequest());
		setWorkRequestBean(new WorkRequestBean(selectedSessione.getCodiBanda(), selectedSessione.getCodiSessione(), selectedSessione.getCodiSistema()));
		RequestContext.getCurrentInstance().execute("PF('workRequestDialog').show()");
	}

	public void changeApparatoEricsson() {
		if(selectedBean.getFlgApparatoEricsson()){
			selectedBean.setFlgApparatoHuawei(false);
			selectedBean.setFlgApparatoNsn(false);
		}
	}

	public void changeApparatoHuawei() {
		if(selectedBean.getFlgApparatoHuawei()){
			selectedBean.setFlgApparatoEricsson(false);
			selectedBean.setFlgApparatoNsn(false);
		}
	}

	public void changeApparatoNsn() {
		if(selectedBean.getFlgApparatoNsn()){
			selectedBean.setFlgApparatoEricsson(false);
			selectedBean.setFlgApparatoHuawei(false);
		}
	}

	public void openDetailCollaudiInLavorazione() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		selectedBean = new CollaudoDatiGeneraliBean(selectedSessione);
		selectedBean.setFlgCollDisponibile(sessioniService.isSessioneStatoDisponibile(selectedSessione.getCodiSessione()) || sessioniService.isSessioneStatoAcquisito(selectedSessione.getCodiSessione()));
		RequestContext.getCurrentInstance().execute("PF('collaudiInLavorazioneDialog').show()");
	}

	public void openEditCollaudiInLavorazione() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		
			if(controlloPerDisponibilitaCollaudo(selectedSessione.getFlgAllegatoA(),selectedSessione.getFlgAllegatoB(),selectedSessione.getFlgAllegatoC(),selectedSessione.getFlgAllegatoD(),selectedSessione.getFlgAllegatoD1(),selectedSessione.getFlgAllegatoE(),selectedSessione.getFlgApparatoEricsson(),selectedSessione.getFlgApparatoHuawei(),selectedSessione.getFlgApparatoNsn(),selectedSessione.getCodiSessione(),selectedSessione.getTimePrevistoCollaudo(),selectedSessione.getCodiStato())){
				setFlgProcessed(true);
			}else
				setFlgProcessed(false);
		
		
			setUnitaTerrList(sessioniService.getAllUnitaTerr(selectedSessione.getCodiAreaComp()));
		ViwSessioni sessioneRefresh = sessioniService.findSessione(selectedSessione.getCodiSessione());
		selectedBean = new CollaudoDatiGeneraliBean(sessioneRefresh);
		selectedBean.setFlgCollDisponibile(sessioniService.isSessioneStatoDisponibile(selectedSessione.getCodiSessione()) || sessioniService.isSessioneStatoAcquisito(selectedSessione.getCodiSessione()));
		RequestContext.getCurrentInstance().execute("PF('collaudiEditInLavorazioneDialog').show()");
		
	}
	
	
	public void openSbloccaSezioneAllegato() {
		
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		reloadViwSbloccoSezioniList();
		
		RequestContext.getCurrentInstance().execute("PF('sbloccaSezioneAllegatoDialog').show()");

	}
	
	public void rimuoviEsitoSurvey() {
		
		try {
			
			if(selectedViwSbloccoSezioni.getCodiOggetto() == Const.ALLEGATO_D1){
					List<TabRepositoryDocument> listDoc = new ArrayList<TabRepositoryDocument>();
					listDoc = repositoryMediaService.getRepDocD1Exist(selectedViwSbloccoSezioni.getCodiSessione());
						if(listDoc.size()>0){
						openKmService.deleteExistD1(selectedViwSbloccoSezioni.getCodiSessione(),listDoc.get(0).getDescNome());
						repositoryMediaService.removeDocumentD1(selectedViwSbloccoSezioni.getCodiSessione());
					}
			}		
			
			surveyService.rimuoviEsitoSurvey(selectedViwSbloccoSezioni);
			
			
			
			reloadViwSbloccoSezioniList();
			reloadCollaudiInLavorazione();
			
		} catch (Exception e) {
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Errore nell'eseguire l'operazione"));
			e.printStackTrace();
		}
		
	}

	private void reloadViwSbloccoSezioniList(){
		viwSbloccoSezioniList = surveyService.getListViwSbloccoSezioneByCodiSessione(selectedSessione.getCodiSessione());
	}

	public void openEditCollaudiArchiviati() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		ViwSessioni sessioneRefresh = sessioniService.findSessione(selectedSessione.getCodiSessione());
		selectedBean = new CollaudoDatiGeneraliBean(sessioneRefresh);
		RequestContext.getCurrentInstance().execute("PF('collaudiArchiviatiDialog').show()");
	}

	private void reloadCollaudiInLavorazione() {
		RequestContext.getCurrentInstance().execute("PF('sessioniTableWV').clearFilters()");
		if(currentUser.getViwUtente().getCodiAreaComp()!=1 && !currentUser.getViwUtente().getFlgImpresa() && !(currentUser.getViwUtente().getCodiRole().equals(Const.DG_OP))){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByAreaComp(currentUser.getViwUtente().getCodiAreaComp(),null);
		}else if(currentUser.getViwUtente().getCodiVendor()!=Const.EMPTY_VALUE){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByUserVendor(currentUser.getViwUtente().getCodiVendor(),null);
		}else if(currentUser.getViwUtente().getFlgImpresa()){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoi(currentUser.getViwUtente().getCodiImpresa(),null);
		}else{
			arrCollaudiInLavorazione=sessioniService.getAllCollaudiInLavorazione(null);
		}
		
	}
	
	private void reloadCollaudiArchiviati() {
		if(currentUser.getViwUtente().getCodiAreaComp()!=1 && !currentUser.getViwUtente().getFlgImpresa()  && !(currentUser.getViwUtente().getCodiRole().equals(Const.DG_OP))){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByAreaComp(currentUser.getViwUtente().getCodiAreaComp());
		}else if(currentUser.getViwUtente().getCodiVendor()!=Const.EMPTY_VALUE){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByUserVendor(currentUser.getViwUtente().getCodiVendor());
		}else if(currentUser.getViwUtente().getFlgImpresa()){
			arrCollaudiArchiviati=sessioniService.getCollaudiArchiviatiByMoi(currentUser.getViwUtente().getCodiImpresa());
		}else{
			arrCollaudiArchiviati=sessioniService.getAllCollaudiArchiviati();
		}
		
	}
	public void openAssocUsers(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		setCodiImpresa(null);
		setTelecomOrImpUser(null);
//		String descGruppo = sessioniService.getDescGruppoByCLLI(selectedSessione.getClliSito());
//		TabGruppi tabGruppi= sessioniService.getTabGruppiDescGruppo(descGruppo);

		//		LISTA DI TUTTI GLI UTENTI associati e no   DI QUEL AREA DI COMPETENZA

		List<ViwUtenti> userlistForbuiltHashMap = sessioniService.getUtentiListByAreaComp(selectedSessione.getCodiAreaComp());
		for (ViwUtenti anagUtenti : userlistForbuiltHashMap) {
			utentiHashMap.put(setInfoListPickList(anagUtenti), anagUtenti);
		}
		/* Mi serve per non rimandare l' EMAIL agli utenti che erano gi� associati*/
		List<ViwUtenti> utentiAssociati=sessioniService.getUtentiListAssoc(selectedSessione.getCodiSessione());
		for (ViwUtenti viwUtenti : utentiAssociati) {
			viwUtenti.setFlagIsAssoc(true);
			utentiHashMap.put(setInfoListPickList(viwUtenti), viwUtenti);
		}
		
		List<String> utentiTarget =	listUtentiToString(utentiAssociati);
		
		setUtentiToAssoc(new DualListModel<>(new ArrayList<String>(), utentiTarget));
		RequestContext.getCurrentInstance().execute("PF('assocUsersDialog').show()");
	}

	private  DualListModel<String> getDualListModelAssocUtenti(List<String> utentiAssocOnDb, List<ViwUtenti> arrUtenti){
		List<String> utentiSources = new ArrayList<String>();
		if(utentiAssocOnDb!=null && utentiAssocOnDb.size()>0){
			for (ViwUtenti anagUtenti : arrUtenti) {
				String descUserForPicklist = setInfoListPickList(anagUtenti);
				if(!utentiAssocOnDb.contains(descUserForPicklist)){
					utentiSources.add(descUserForPicklist);
				}
			}
		}else
			utentiSources = listUtentiToString(arrUtenti);
		return new DualListModel<String>(utentiSources, utentiAssocOnDb);
	}
	
	private List<String> listUtentiToString(List<ViwUtenti> arrUtenti) {
		List<String> arrUtentiString = new ArrayList<String>();
		for (ViwUtenti anagUtenti : arrUtenti) {
			arrUtentiString.add(setInfoListPickList(anagUtenti));
		}
		return arrUtentiString;
	}
	
	private String setInfoListPickList(ViwUtenti anagUtenti) {
		String impOrNot="";
		if(anagUtenti.getFlgImpresa())
			impOrNot = Const.UTENTE_MOI;
		else if (anagUtenti.getCodiVendor().equals(Const.EMPTY_VALUE))
			impOrNot =  Const.UTENTE_TELECOM;
		else 
			impOrNot = Const.DESC_UTENTE_VENDOR;
		
		return anagUtenti.getDescUser()+" ("+anagUtenti.getDescNome()+" "+anagUtenti.getDescCgn()+"-"+impOrNot+")";
	}
	
	/**
	 * Popola la lista source della picklist associa sessione utente 
	 * **/
	public void listenerBodyTipoTecnico(){
		List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
		try {
			
				if(telecomOrImpUser==1 && codiImpresa==null)
				listaUtenti= sessioniService.getOperNoaByCodiAreaComp(selectedSessione.getCodiAreaComp());
			else
				listaUtenti= sessioniService.getUserMOIByAreaComp(codiImpresa, selectedSessione.getCodiAreaComp());

			
		} catch (Exception e) {
			
		}
		setUtentiToAssoc(getDualListModelAssocUtenti(utentiToAssoc.getTarget(),listaUtenti));
		
		
		
	}
	/**
	 * Metodo per popolare la picklist di tecnico enel (telecomOrImpUser=1) o la combo box delle imprese
	 * **/
	public void listenerTipoTecnico() {
		if(telecomOrImpUser==null){
			setCodiImpresa(null);
			return;}
		if(telecomOrImpUser==1){
			setCodiImpresa(null);
			listenerBodyTipoTecnico();
			
		}else{
			List<String> utentiTarget= utentiToAssoc.getTarget();
			setUtentiToAssoc(new DualListModel<>(new ArrayList<String>(), utentiTarget));
			try {
					setImpreseList(sessioniService.getTableImpreseList());
				
			} catch (Exception e) {
			}
		}
	}
		
	public void saveAssocUtente() {
		try {
			List<ViwUtenti> utentiAssociati=sessioniService.getUtentiListAssoc(selectedSessione.getCodiSessione());
			List<Integer> codiUserAssoc= new ArrayList<Integer>();
			for (ViwUtenti viwUtenti : utentiAssociati) {
				codiUserAssoc.add(viwUtenti.getCodiUtente());
			}
			List<String> target= utentiToAssoc.getTarget();
			List<ViwUtenti> utenti= new ArrayList<>();
			for (String string : target) {
				utenti.add(utentiHashMap.get(string));
			}
			Boolean result = sessioniService.saveAssocUtente(utenti,selectedSessione,codiUserAssoc);
			if(result){
				reloadCollaudiInLavorazione();
					RequestContext.getCurrentInstance().execute("PF('assocUsersDialog').hide()");
			}else
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Associazione Fallita"));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Associazione Fallita"));
			e.printStackTrace();
		}
	}
	
	private Boolean  controlloPerDisponibilitaCollaudo(Boolean flgAllegatoA,Boolean flgAllegatoB,Boolean flgAllegatoC,Boolean flgAllegatoD,Boolean flgAllegatoD1,Boolean flgAllegatoE,Boolean flgApparatoEricsson,Boolean flgApparatoHuawei,Boolean flgApparatoNsn ,Integer codiSessione,Date dataPrevistoCollaudo,Integer codiStato) {
		if (flgAllegatoA) {
			setFlagAllegati(true);
		}else if (flgAllegatoB) {
			setFlagAllegati(true);
		}else if (flgAllegatoC) {
			setFlagAllegati(true);
		}else if (flgAllegatoD) {
			setFlagAllegati(true);
		}else if (flgAllegatoE) {
			setFlagAllegati(true);
		}else if (flgAllegatoD1) {
			setFlagAllegati(true);
		}
		else setFlagAllegati(false);
		
		if (flgApparatoEricsson) {
			setFlagApparati(true);
		}else if (flgApparatoHuawei) {
			setFlagApparati(true);
		}else if (flgApparatoNsn) {
			setFlagApparati(true);
		}else setFlagApparati(false);
		
		List<TabWorkRequestColl> list =sessioniService.getWorkRequestBySessione(codiSessione);
	    Integer codiStatoAcquisito= sessioniService.getStateAcquisito();
		
		if(flagAllegati && flagApparati && list.size()>0 && dataPrevistoCollaudo!=null && codiStatoAcquisito==codiStato)
			return true;
		else 
			return false;
	}
	
	

	public void rendiDisponibile() {
		if(controlloPerDisponibilitaCollaudo(selectedBean.getFlgAllegatoA(),selectedBean.getFlgAllegatoB(),selectedBean.getFlgAllegatoC(),selectedBean.getFlgAllegatoD(),selectedBean.getFlgAllegatoD1(),selectedBean.getFlgAllegatoE(),selectedBean.getFlgApparatoEricsson(),selectedBean.getFlgApparatoHuawei(),selectedBean.getFlgApparatoNsn(),selectedBean.getCodiSessione(),selectedBean.getTimePrevistoCollaudo(),sessioniService.getStateAcquisito())){
			try {
//				String descGruppo = sessioniService.getDescGruppoByCLLI(selectedSessione.getClliSito());
//				TabGruppi tabGruppi= sessioniService.getTabGruppiDescGruppo(descGruppo);
				sessioniService.rendiDiponibileCollaudo(selectedBean, sessioniService.getCodiStateDisponibile());
				setFlgProcessed(false);
				reloadCollaudiInLavorazione();
				RequestContext.getCurrentInstance().execute("PF('collaudiEditInLavorazioneDialog').hide()");
			} catch (Exception e) {
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Cambio di stato fallito controllare se c'� almeno un allegato fleggato ,un verbale fleggato e la data previsto collaudo "));
			}
			
			
		}else{
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Cambio di stato fallito controllare se c'� almeno un allegato fleggato ,un verbale fleggato e la data previsto collaudo "));
			setFlgProcessed(true);
		}
	}
	
	public void forcedUnlockA() {
			sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_A);
			reloadCollaudiInLavorazione();
	}
	public void forcedUnlockB() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_B);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockC() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_C);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockD() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_D);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockE() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_E);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockEricsson() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_ERICSSON);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockNokia() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_NOKIA);
		reloadCollaudiInLavorazione();
	}
	public void forcedUnlockHuawei() {
		sessioniService.unlock(selectedSessione, currentUser.getCodiUtente(),Const.ALLEGATO_HUAWEI);
		reloadCollaudiInLavorazione();
	}
	
	public void rollbackCollaudo() {
		boolean valRet = false;
		if(currentUser.getViwUtente().getCodiRole() == Const.SUPERVISOR){
			valRet = sessioniService.rollbackCollaudo(selectedSessione.getCodiSessione());
		}
		if(valRet){
			RequestContext.getCurrentInstance().execute("PF('collaudiInLavorazioneDialog').hide()");
			reloadCollaudiInLavorazione();
		}else
		FacesContext.getCurrentInstance().addMessage("formCollaudiInLavorazione:messagesDetail", new FacesMessage(FacesMessage.SEVERITY_ERROR, Const.SEVERITY_ERROR,"Salvataggio fallito. Riprovare."));

	}
	
	public void detailParziali(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		List<TabRelRitardiSessioni> listRitardi = sessioniService.getRitardiBySessione(selectedSessione.getCodiSessione());
		setListRitardi(listRitardi);
		RequestContext.getCurrentInstance().execute("PF('parzialiDialog').show()");
	}
	
	public void openReportAllegati() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		flagVerbaleD1=false;
		TabRepositoryDocument verbaleD1 = new TabRepositoryDocument();
		verbaleD1=repositoryMediaService.getTabRepositoryDocumentVerbaleD1(selectedSessione.getCodiSessione());
		if(verbaleD1!=null)
			flagVerbaleD1=true;
		RequestContext.getCurrentInstance().execute("PF('allegatiSessioneDialog').show()");

	}
	
	public void openCompilazioneAllegati() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista!"));
			return;
		}
		if(selectedSessione.getCodiStato().equals(Const.DISPONIBILE) || selectedSessione.getCodiStato().equals(Const.IN_LAVORAZIONE)){
			if(sessioniService.isUserAssocSessione(currentUser.getCodiUtente(), selectedSessione.getCodiSessione())){
				//selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
				RequestContext.getCurrentInstance().execute("PF('compilazioneAllegati').show()");
			} else {
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione utente non associato alla sessione"));
				return;
			}
		}else{
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione stato del collaudo errato"));
			return;
		}
	}
	
	
	public void openUpload(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		//Possibilit� di inserire il D1 nel caso in cui non sia stato inserito il D2
		if((selectedSessione.getCodiStatoD2() == null || selectedSessione.getCodiStatoD2() == Const.VERBALE_NON_LAVORATO) && selectedSessione.getCodiStato() != Const.CHIUSO){
			setListAnagDocumenti(sessioniService.getAllAnagDocumenti());
		}else{
			//Nel caso in cui � stato dato un esito a D2 non si avr� la possibilit� di vedere e quindi di inserire il D1
			setListAnagDocumenti(sessioniService.getAllAnagDocumentiWithoutD1());
		}
		
		setAnagDocumenti(new TabAnagDocumenti());
		
		setUploadPanelVisible(false);
		reloadComboRisultati();
		
		
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		
		RequestContext.getCurrentInstance().execute("PF('uploadDialog').show()");
	}
	
	public void openDownload(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		setListDocument(repositoryMediaService.getRepositoryDocumentByCodiSessione(selectedSessione.getCodiSessione()));
		setListFoto(repositoryMediaService.getRepositoryMediaByCodiSessione(selectedSessione.getCodiSessione()));;
		
		RequestContext.getCurrentInstance().execute("PF('downloadDialog').show()");
	}
	public void listnerDocument(){
		if(anagDocumenti.getCodiAnagDocumento()!=null)
			setUploadPanelVisible(true);
		else
			setUploadPanelVisible(false);
	}
		
	public void uploadFile(FileUploadEvent event){
		try {
			
			//Se esiste gi� un D1 sul repository lo cancello prima di inserirne un altro
			Integer tipoDocumento=anagDocumenti.getCodiAnagDocumento();
			try {
				if(tipoDocumento==1){
				List<TabRepositoryDocument> listDoc = new ArrayList<TabRepositoryDocument>();
					listDoc = repositoryMediaService.getRepDocD1Exist(selectedSessione.getCodiSessione());
					if(listDoc.size()>0)
					openKmService.deleteExistD1(selectedSessione.getCodiSessione(),listDoc.get(0).getDescNome());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String nomeFile = event.getFile().getFileName();
			
			TabRepositoryDocument tabRepDoc= new TabRepositoryDocument();
			tabRepDoc.setCodiSessione(selectedSessione.getCodiSessione());
			tabRepDoc.setDescNome(nomeFile);
//			tabRepDoc.setTipoFile(4);
			
			tabRepDoc.setTimeCreate(new Timestamp(System.currentTimeMillis()));
			
			String[] nomeFileSplit = nomeFile.split("_");
			String[] nomeFileSplitExt = nomeFile.split("\\.");
			String codiceImpiantoFile = "";
			String codiceImpiantoSessione = selectedSessione.getDescCodice().substring(4, 5);
			String fileExt = "";
			
			try {
				if(nomeFileSplitExt.length>0){
					fileExt = "." + nomeFileSplitExt[1];
					tabRepDoc.setDescMimeType("."+nomeFileSplitExt[1]);
					
					codiceImpiantoFile = nomeFileSplit[1].substring(5, 6);
				}
			} catch (Exception e) {
			}
			
			
			switch (tipoDocumento) {
			case 1:
				if((selectedSessione.getFlgAllegatoD1() &&( sessioniService.isSessioneStatoDisponibile(selectedSessione.getCodiSessione())||sessioniService.isSessioneStatoInLavorazione(selectedSessione.getCodiSessione())))){
					if(sessioniService.isSessioneStatoDisponibile(selectedSessione.getCodiSessione())){
						sessioniService.changeStatoSessione(selectedSessione.getCodiSessione(), Const.IN_LAVORAZIONE);
					}if(!codiceImpiantoFile.equalsIgnoreCase("")&&(nomeFileSplit.length > 0 && nomeFileSplit.length==4) &&  nomeFileSplit[0].equals(selectedSessione.getCodiceDbr()) && nomeFileSplit[1].equals("VPIM1"+codiceImpiantoFile)
							&& codiceImpiantoFile.equals(codiceImpiantoSessione) && (nomeFileSplit[3].equals("P"+fileExt) || nomeFileSplit[3].equals("N"+fileExt) || nomeFileSplit[3].equals("R"+fileExt)) && (fileExt.equalsIgnoreCase(".pdf") || fileExt.equalsIgnoreCase(".xls") || fileExt.equalsIgnoreCase(".xlsx"))){

						allegatoD1Bean.setVisibleForm(true);
						tabRepDoc.setFlagD1(true);
						tabRepDoc.setTipoFile(Const.ALLEGATO_D1_REMOTO);

						Document doc= openKmService.uploadSync(selectedSessione.getCodiSessione(), nomeFile, String.valueOf(currentUser.getCodiUtente()), event.getFile().getInputstream());
						tabRepDoc.setDescPath(doc.getPath());
						repositoryMediaService.saveRepositoryDocument(tabRepDoc);
					}else{
						FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Tipologia file non supportata, controllare nome file"));
					}
				}else {
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Il collaudo non prevedere il D1, oppure si trova in uno stato non disponbile per l'operazione"));
				}
				
				break;
			case 2:
				if(!codiceImpiantoFile.equalsIgnoreCase("")&&(nomeFileSplit.length > 0 && nomeFileSplit.length==4) && nomeFileSplit[0].equals(selectedSessione.getCodiceDbr()) && nomeFileSplit[1].equals("FILAP"+codiceImpiantoFile)
						&& codiceImpiantoFile.equals(codiceImpiantoSessione) && (nomeFileSplit[3].equals("P"+fileExt) || nomeFileSplit[3].equals("N"+fileExt) || nomeFileSplit[3].equals("R"+fileExt))){
					
					allegatoD1Bean.setVisibleForm(false); 
					tabRepDoc.setFlagD1(false);
					tabRepDoc.setTipoFile(Const.MISURE_PER_APOGEO);
					
					Document doc= openKmService.uploadSync(selectedSessione.getCodiSessione(), nomeFile, String.valueOf(currentUser.getCodiUtente()), event.getFile().getInputstream());
					tabRepDoc.setDescPath(doc.getPath());
					repositoryMediaService.saveRepositoryDocument(tabRepDoc);
					//aggiungere mail file per misurazioni
					sessioniService.invioMailDocUpload(selectedSessione, tipoDocumento, nomeFile , currentUser.getCodiUtente());
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"File caricato correttamente"));
				}else{
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Tipologia file non supportata, controllare nome file"));
				}
				break;
				// NON � POSSIBILE CANCELLARLI PERCH� SUL DB CI SAREBBERO DATI INCONSISTENTI
//			case 3: case 4:
//			
//				if(fileExt.equalsIgnoreCase(".zip")){
//					allegatoD1Bean.setVisibleForm(false); 
//					tabRepDoc.setFlagD1(false);
//					TabCountDocument tab=null;
//					tab=sessioniService.findProgessiovoDocument(selectedSessione.getCodiSessione(), tipoDocumento);
//					String inizioNome=null;
//					if(tipoDocumento==3){
//						inizioNome="misure_";
//						tabRepDoc.setTipoFile(Const.MISURE_ZIP);
//					}else{
//						inizioNome="media_";
//						tabRepDoc.setTipoFile(Const.MEDIA_ZIP);
//					}
//					nomeFile=inizioNome+selectedSessione.getDescCodice()+"_"+tab.getCountDocument()+".zip";
//					Document doc= openKmService.uploadSync(selectedSessione.getCodiSessione(), nomeFile, String.valueOf(currentUser.getCodiUtente()), event.getFile().getInputstream());
//					tabRepDoc.setDescPath(doc.getPath());
//					tabRepDoc.setDescNome(nomeFile);
//					repositoryMediaService.saveRepositoryDocument(tabRepDoc);
//					//aggiungere mail per misurazioni e media
//					sessioniService.invioMailDocUpload(selectedSessione, tipoDocumento, nomeFile, currentUser.getCodiUtente());
//					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"File caricato correttamente"));
//				}else{
//					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Tipologia file non supportata, controllare nome file"));
//				}
//				break;
			case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12:
				if(fileExt.equalsIgnoreCase(".zip")){
					allegatoD1Bean.setVisibleForm(false);
					tabRepDoc.setFlagD1(false);
					TabCountDocument tab=null;
					tab=sessioniService.findProgessiovoDocument(selectedSessione.getCodiSessione(), tipoDocumento);
					String progressivo = String.valueOf(tab.getCountDocument());
					if(tab.getCountDocument()>= Const.CHAR_A && tab.getCountDocument()<= Const.CHAR_Z){
						progressivo = String.valueOf(sessioniService.countChar(tab.getCountDocument()- Const.CHAR_A));
					}
					String identificativo = sessioniService.findIdentificativoByBandaAndSistema(selectedSessione.getCodiBanda(), selectedSessione.getCodiSistema());
					Date data = tabRepDoc.getTimeCreate();
					String dataString = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(data));
					switch(tipoDocumento){
					case 5:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_MISURE_PIM+identificativo+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.MISURE_PIM_ZIP);
						break;
					case 6:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_MISURE_ROS+identificativo+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.MISURE_ROS_ZIP);
						break;
					case 7:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_EXPORT_NODO+identificativo+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.EXPORT_NODO_ZIP);
						break;
					case 8:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_DOCUMENTO_TRR+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.DOCUMENTO_TRR_ZIP);
						break;
					case 9:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_FOTO_SUPPORTO+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.FOTO_SUPPORTO_ZIP);
						break;
					case 10:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_FOTO_CELLA+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.FOTO_CELLA_ZIP);
						break;
					case 11:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_FOTO_SALA+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.FOTO_SALA_ZIP);
						break;
					case 12:
						nomeFile = selectedSessione.getCodiceDbr()+"_"+Const.CODIFICA_FOTO_LOCALITA+"_"+dataString+"_"+progressivo+".zip";
						tabRepDoc.setTipoFile(Const.FOTO_LOCALITA_ZIP);
						break;
					}
					sessioniService.invioMailDocUpload(selectedSessione, tipoDocumento, nomeFile , currentUser.getCodiUtente());
					Document doc= openKmService.uploadSync(selectedSessione.getCodiSessione(), nomeFile, String.valueOf(currentUser.getCodiUtente()), event.getFile().getInputstream());
					tabRepDoc.setDescPath(doc.getPath());
					tabRepDoc.setDescNome(nomeFile);
					repositoryMediaService.saveRepositoryDocument(tabRepDoc);
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"File caricato correttamente"));
				}else{
					FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Tipologia file non supportata, controllare nome file"));
				}
				break;
			}		
		}catch(Exception ex){
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Errore durante l'upload, controllare il nome del file "));
		}
	}
	
	public void saveUploadForm(){
		try{
			if(selectedSessione.getFlgAllegatoD1() && sessioniService.isSessioneStatoInLavorazione(selectedSessione.getCodiSessione())){
				TabRisultatoSurvey tabRisultatoSurvey = new TabRisultatoSurvey();
				tabRisultatoSurvey.setCodiOggetto(Constants.OGGETTO_D1_REMOTO);
				tabRisultatoSurvey.setCodiUtente(currentUser.getCodiUtente());
				tabRisultatoSurvey.setCodiValoreRisultatoSurvey(allegatoD1Bean.getCodiEsito());
				tabRisultatoSurvey.setDescValoreRisultatoSurvey(Constants.mapRisultatoSurvey.get(allegatoD1Bean.getCodiEsito()));
				if(allegatoD1Bean.getCodiEsito().equals(Const.CODI_POSITIVO_CON_RISERVA)){
					tabRisultatoSurvey.setCodiAnagRisultato(Const.D1_POSITIVO_CON_RISERVA);
				}else if(allegatoD1Bean.getCodiEsito().equals(Const.CODI_NEGATIVO)){
					tabRisultatoSurvey.setCodiAnagRisultato(Const.D1_NEGATIVO);
				}else{
					tabRisultatoSurvey.setCodiAnagRisultato(null);
				}
				tabRisultatoSurvey.setCodiSessione(selectedSessione.getCodiSessione());
				
				surveyService.saveSurveyForD1( tabRisultatoSurvey, selectedSessione.getCodiSessione(), Constants.OGGETTO_TIPO_CODI_D1_REMOTO);
				//Se esiste un risultato in TabRelSessioneStatiVerbale del file D1 lo cancello prima di inserire il nuovo
				List<TabRelSessioneStatiVerbale> listTabRelSessioneStatiVerbale = surveyService.getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(), Constants.OGGETTO_D1_REMOTO);
				if(listTabRelSessioneStatiVerbale.size()>0){
					TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale=listTabRelSessioneStatiVerbale.get(0);
					surveyService.deleteRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(tabRelSessioneStatiVerbale.getCodiSessione(),tabRelSessioneStatiVerbale.getCodiOggetto());
				}
				
//				//salvo il risultato del verbale D1
				TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = new TabRelSessioneStatiVerbale();
				tabRelSessioneStatiVerbale.setCodiOggetto(Constants.OGGETTO_D1_REMOTO);
				tabRelSessioneStatiVerbale.setCodiSessione(selectedSessione.getCodiSessione());
				tabRelSessioneStatiVerbale.setCodiStatoVerbale(allegatoD1Bean.getCodiEsito());
				surveyService.saveRelSessioneStatiVerbale(tabRelSessioneStatiVerbale);
				 if(sincronizzazioneService.isExistCollaudoAllResults(selectedSessione.getCodiSessione())){
					 sincronizzazioneService.changeStatoSessione(selectedSessione.getCodiSessione(), surveyService.loadStatoChiuso().getCodiStato());
						
						UtilLog.printControllaSurveyRisultati(selectedSessione.getCodiSessione(), true);
						surveyService.invioMailArchiviatoInD1(selectedSessione.getCodiSessione());
					}else{
						UtilLog.printControllaSurveyRisultati(selectedSessione.getCodiSessione(), false);
					}
				TabRepositoryDocument rep = new TabRepositoryDocument();
				rep=repositoryMediaService.getTabRepositoryDocumentVerbaleD1(selectedSessione.getCodiSessione());
				sessioniService.invioMailD1(selectedSessione, "Allegato D1", rep.getDescNome(), currentUser.getCodiUtente(), allegatoD1Bean.getCodiEsito());
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"File caricato correttamente"));
				
				RequestContext.getCurrentInstance().execute("PF('uploadDialog').hide()");
			}else{
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Allegato D1 non previsto per il collaudo o stato collaudo non idoneo"));
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Errore durante il salvataggio "));
		}
		
	}
	
	public void uploadSchedaRischiDyn(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		ServiceDynToSawService service1 = new ServiceDynToSawService();
		OutputStatoScheda stato = null;
		try {
			ServiceDynToSaw port2=null;
			try {
				port2 = service1.getServiceDynToSaw();
			} catch (ConnectException e) {
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Connessione assente"));
			}
			stato = port2.getStatoSchedaRischi(selectedSessione.getCodiceDbr());

			if(stato.getCodiceEsito()!=null  && stato.getCodiceEsito().equalsIgnoreCase("001")){
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Upload documenti DYN:" +stato.getMsgEsito()));
				return;
			}
			if(stato.getVersione()==null  || stato.getVersione().equalsIgnoreCase("")){
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione la versione non pu� essere null"));
				return;
			}

			OutputScheda scheda = port2.getScheda(selectedSessione.getCodiceDbr());
			TabSchedaDyn tabSchedaExist = repositoryMediaService.getTabSchedaDyn(selectedSessione.getCodiSessione());
			if(tabSchedaExist!=null && tabSchedaExist.getVersione().equalsIgnoreCase(stato.getVersione())){
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"La scheda � gi� aggiornata all'ultima versione"));
				return;
			}else{

				TabSchedaDyn tabScheda = new TabSchedaDyn();

				tabScheda.setCodiSessione(selectedSessione.getCodiSessione());
				tabScheda.setAccessibilita(scheda.getAccessibilita());
				if(scheda.getCodiceMazzoChiavi()!=null)
					tabScheda.setCodiceMazzoChiavi(scheda.getCodiceMazzoChiavi());
				try {
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					if(scheda.getDataCreazioneSchedaRischi()!=null && (!scheda.getDataCreazioneSchedaRischi().equalsIgnoreCase(""))){
						tabScheda.setDataCreazioneScheda(formatter.parse(scheda.getDataCreazioneSchedaRischi()));
					}
				} catch (Exception e) {
					RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Il formato della data non � corretto"));
					return;
				}
				
				tabScheda.setDisagiato(scheda.getDisagiato());
				tabScheda.setFasciaIntervento(scheda.getFasciaIntervento());
				tabScheda.setItinerario(scheda.getItinerario());
				tabScheda.setNoteAccessibilita(scheda.getNoteAccessibilita());
				tabScheda.setNoteAccesso(scheda.getNoteAccesso());
				tabScheda.setRegime(scheda.getRegime());
				tabScheda.setTipoAccesso(scheda.getTipoAccesso());
				tabScheda.setVersione(scheda.getVersione());
				if(tabSchedaExist!=null)	
					repositoryMediaService.updateSchedaDyn(tabScheda);
				else
					repositoryMediaService.saveSchedaDyn(tabScheda);
			}

			try {
				openKmService.createFolderDocumentDyn(selectedSessione.getCodiSessione());
			} catch (Exception e) {
				e.printStackTrace();	
				return;
			}
			
			repositoryMediaService.removeDocumentDyn(selectedSessione.getCodiSessione());
			if(scheda.getAllegati()!=null){
				for (Allegato file: scheda.getAllegati().getItem()) {
					try {
						Document doc= openKmService.uploadDocumentDyn(selectedSessione.getCodiSessione(), file.getNomeAllegato(), file.getAllegato());
						TabRepositorySchedaRischi tabRepoSchedaRischi= new TabRepositorySchedaRischi();
						tabRepoSchedaRischi.setCodiSessione(selectedSessione.getCodiSessione());
						tabRepoSchedaRischi.setDescNome(file.getNomeAllegato());
						tabRepoSchedaRischi.setDescPath(doc.getPath());
						tabRepoSchedaRischi.setVersione(scheda.getVersione());
						try {
							SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
							if(scheda.getDataCreazioneSchedaRischi()!=null && (!scheda.getDataCreazioneSchedaRischi().equalsIgnoreCase(""))){
								tabRepoSchedaRischi.setTimeCreate(formatter.parse(scheda.getDataCreazioneSchedaRischi()));
							}
						} catch (Exception e) {
							RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Il formato della data non � corretto"));
							return;
						}
						repositoryMediaService.saveRepositorySchedaRischi(tabRepoSchedaRischi);
					} catch (Exception e) {
					}
	
				}
			}
			if(scheda.getSchedaRischi()!=null && scheda.getSchedaRischi().length>0){
				TabRepositorySchedaRischi tabRepoSchedaRischi= new TabRepositorySchedaRischi();
				tabRepoSchedaRischi.setCodiSessione(selectedSessione.getCodiSessione());
				Document schedaRischi= openKmService.uploadDocumentDyn(selectedSessione.getCodiSessione(),"SchedaRischi.pdf" , scheda.getSchedaRischi());
				tabRepoSchedaRischi.setDescNome("SchedaRischi.pdf");
				tabRepoSchedaRischi.setDescPath(schedaRischi.getPath());
				tabRepoSchedaRischi.setFlagSchedaRischi(true);
				tabRepoSchedaRischi.setVersione(scheda.getVersione());
				try {
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					if(scheda.getDataCreazioneSchedaRischi()!=null && (!scheda.getDataCreazioneSchedaRischi().equalsIgnoreCase(""))){
						tabRepoSchedaRischi.setTimeCreate(formatter.parse(scheda.getDataCreazioneSchedaRischi()));
					}
				} catch (Exception e) {
					RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Il formato della data non � corretto"));
					return;
				}
				repositoryMediaService.saveRepositorySchedaRischi(tabRepoSchedaRischi);
			}
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Upload documenti DYN:" +stato.getMsgEsito()));
			return;
		
		} catch (WebServiceException e){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Connessione assente"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
			
	}
	
	public void openDownloadDyn(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		
		selectedSessione = sessioniService.findSessione(selectedSessione.getCodiSessione());
		setListDocumentDyn(repositoryMediaService.getRepositoryDocumentDynByCodiSessione(selectedSessione.getCodiSessione()));
		setTabSchedaDin(repositoryMediaService.getTabSchedaDyn(selectedSessione.getCodiSessione()));
		RequestContext.getCurrentInstance().execute("PF('downloadDocumentDynDialog').show()");
	}
	
	public void printApparatoEricsson() {
		reportService.printApparatoEricsson(selectedSessione);
	}
	public void printApparatoNokia() {
		reportService.printApparatoNokia(selectedSessione);
	}
	public void printApparatoHuawei() {
		reportService.printApparatoHuawei(selectedSessione);
	}
	public void printVerbaleRadiantiA() {
		reportService.printVerbaleRadiantiA(selectedSessione);
	}
	public void printVerbaleRadiantiB() {
		reportService.printVerbaleRadiantiB(selectedSessione);
	}
	public void printVerbaleRadiantiC() {
		reportService.printVerbaleRadiantiC(selectedSessione);
	}
	public void printVerbaleRadiantiD() {
		reportService.printVerbaleRadiantiD(selectedSessione);
	}
	public void printVerbaleRadiantiE() {
		reportService.printVerbaleRadiantiE(selectedSessione);
	}
	
	public StreamedContent downloadVerbaleD1(){
		TabRepositoryDocument rep = new TabRepositoryDocument();
		rep=repositoryMediaService.getTabRepositoryDocumentVerbaleD1(selectedSessione.getCodiSessione());
		StreamedContent file = null;
		try {
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.download(rep) ;
			if(is!=null)
			file = new DefaultStreamedContent(is, rep.getDescMimeType(), rep.getDescNome());
			else
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione problemi di connessione con il repository. Riprovare pi� tardi"));
		} catch (Exception e) {}
		return file;
	}
	
	public StreamedContent downloadDoc(TabRepositoryDocument rep){
		StreamedContent file = null;
		try {
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.download(rep) ;
			if(is!=null)
				file = new DefaultStreamedContent(is, rep.getDescMimeType(), rep.getDescNome());
			else
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione problemi di connessione con il repository. Riprovare pi� tardi"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
	
	public StreamedContent downloadMedia(TabRepositoryMedia rep){
		StreamedContent file = null;
		try {
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.downloadMedia(rep) ;
			if(is!=null)
			file = new DefaultStreamedContent(is, rep.getDescMimeType(), rep.getDescName());
			else
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione problemi di connessione con il repository. Riprovare pi� tardi"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
	
	public StreamedContent downloadDocDyn(TabRepositorySchedaRischi rep){
		StreamedContent file = null;
		try {
			ServiceRepositoryImpl srI = new ServiceRepositoryImpl();
			InputStream is = srI.downloadDocumentiDyn(rep) ;
			if(is!=null)
			file = new DefaultStreamedContent(is, rep.getDescMimeType(), rep.getDescNome());
			else
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione problemi di connessione con il repository. Riprovare pi� tardi"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}
	
	/*
	 * Questo metodo serve a far vedere le imprese associate indirettamente alla sessione tramite gli utenti
	 */
	
	public void openAssocDitte(){
		arrDitte = sessioniService.getDitteByCodiSessione(selectedSessione.getCodiSessione());
		if(arrDitte.size()>0){
			RequestContext.getCurrentInstance().execute("PF('assocDitteDialog').show()");
		}else{
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Non ci sono imprese associate"));
		}
		rootSessione = new DefaultTreeNode("Sessione: "+selectedSessione.getDescNomeSito(), null);
		List<ViwSessImpUser> appoArrDitte= new ArrayList<>();
		for(ViwSessImpUser ditta : arrDitte){
			boolean dittaPresente = false;
			if(appoArrDitte.isEmpty()){
				appoArrDitte.add(ditta);
			} else {
				for(ViwSessImpUser appoDitta : appoArrDitte){
					if(appoDitta.getDescImpresa().equals(ditta.getDescImpresa())){
						dittaPresente = true;
					}
				}
				if(!dittaPresente){
					appoArrDitte.add(ditta);
				}
			}
		}
		createNodes(rootSessione, appoArrDitte);
	}
	
	/*
	 * Metodo di supporto al metodo openAssocDitte che, ricevuta la rootSessione e l'array delle ditte filtrate,
	 * genera tutti i nodes della root, sia quelli delle ditte sia quelli degli utenti.
	 */
	public void createNodes (TreeNode rootSessione, List<ViwSessImpUser> appoArrDitte) {
		for (ViwSessImpUser appoDitta : appoArrDitte) {
			TreeNode nodeDitta = new DefaultTreeNode("Impresa: "+appoDitta.getDescImpresa(), rootSessione);
			nodeDitte.add(nodeDitta);
			arrUser = sessioniService.getUsersByCodiImpresaAndCodiSessione(appoDitta.getCodiImpresa(), appoDitta.getCodiSessione());
			for(ViwSessImpUser user : arrUser){
				nodeDitta.getChildren().add(new DefaultTreeNode(user.getDescNome()+ " " + user.getDescCgn()));
			}
		}		
	}
	
	
	


	@Override
	public void filter() {
		RequestContext.getCurrentInstance().execute("PF('sessioniTableWV').clearFilters()");
		if(currentUser.getViwUtente().getCodiAreaComp()!=1 && !currentUser.getViwUtente().getFlgImpresa() && !(currentUser.getViwUtente().getCodiRole().equals(Const.DG_OP))){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByAreaComp(currentUser.getViwUtente().getCodiAreaComp(),sessioniFilter.getCodiStato());
		}else if(currentUser.getViwUtente().getCodiVendor()!=Const.EMPTY_VALUE){
			arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByUserVendor(currentUser.getViwUtente().getCodiVendor(),sessioniFilter.getCodiStato());
		}else if(currentUser.getViwUtente().getFlgImpresa()){
			if(!sessioniUserImp){
				arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoi(currentUser.getViwUtente().getCodiImpresa(),sessioniFilter.getCodiStato());
			}else{
				arrCollaudiInLavorazione=sessioniService.getCollaudiInLavorazioneByMoiAssoc(currentUser.getViwUtente().getCodiUtente(),sessioniFilter.getCodiStato());
			}
		}else{
			arrCollaudiInLavorazione=sessioniService.getAllCollaudiInLavorazione(sessioniFilter.getCodiStato());
		}
		
	}
//
	@Override
	public void filterClear() {
		sessioniFilter = new SessioniFilter();
		sessioniUserImp=false;
		reloadCollaudiInLavorazione();

	}

	public CurrentUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	public SessioniService getSessioniService() {
		return sessioniService;
	}

	public void setSessioniService(SessioniService sessioniService) {
		this.sessioniService = sessioniService;
	}

	public List<ViwSessioni> getArrCollaudiInLavorazione() {
		return arrCollaudiInLavorazione;
	}

	public void setArrCollaudiInLavorazione(
			List<ViwSessioni> arrCollaudiInLavorazione) {
		this.arrCollaudiInLavorazione = arrCollaudiInLavorazione;
	}

	public List<ViwSessioni> getArrCollaudiArchiviati() {
		return arrCollaudiArchiviati;
	}

	public void setArrCollaudiArchiviati(List<ViwSessioni> arrCollaudiArchiviati) {
		this.arrCollaudiArchiviati = arrCollaudiArchiviati;
	}

	public ViwSessioni getSelectedSessione() {
		return selectedSessione;
	}

	public void setSelectedSessione(ViwSessioni selectedSessione) {
		this.selectedSessione = selectedSessione;
	}

	public CollaudoDatiGeneraliBean getSelectedBean() {
		return selectedBean;
	}

	public void setSelectedBean(CollaudoDatiGeneraliBean selectedBean) {
		this.selectedBean = selectedBean;
	}

	public List<TabImpianti> getImpiantiList() {
		return impiantiList;
	}

	public void setImpiantiList(List<TabImpianti> impiantiList) {
		this.impiantiList = impiantiList;
	}

	public List<TabSistemi> getSistemiList() {
		return sistemiList;
	}

	public void setSistemiList(List<TabSistemi> sistemiList) {
		this.sistemiList = sistemiList;
	}

	public List<TabBande> getBandeList() {
		return bandeList;
	}

	public void setBandeList(List<TabBande> bandeList) {
		this.bandeList = bandeList;
	}

	public List<TabFinalitaCollaudo> getFinalitaCollList() {
		return finalitaCollList;
	}

	public void setFinalitaCollList(List<TabFinalitaCollaudo> finalitaCollList) {
		this.finalitaCollList = finalitaCollList;
	}

	public List<TabDettaglioCollaudo> getDettagliCollList() {
		return dettagliCollList;
	}

	public void setDettagliCollList(List<TabDettaglioCollaudo> dettagliCollList) {
		this.dettagliCollList = dettagliCollList;
	}

	public List<TabZone> getUnitaTerrList() {
		return unitaTerrList;
	}

	public void setUnitaTerrList(List<TabZone> unitaTerrList) {
		this.unitaTerrList = unitaTerrList;
	}

	public DualListModel<String> getUtentiToAssoc() {
		return utentiToAssoc;
	}

	public void setUtentiToAssoc(DualListModel<String> utentiToAssoc) {
		this.utentiToAssoc = utentiToAssoc;
	}

	public Integer getTelecomOrImpUser() {
		return telecomOrImpUser;
	}

	public void setTelecomOrImpUser(Integer telecomOrImpUser) {
		this.telecomOrImpUser = telecomOrImpUser;
	}

	public Integer getCodiImpresa() {
		return codiImpresa;
	}

	public void setCodiImpresa(Integer codiImpresa) {
		this.codiImpresa = codiImpresa;
	}

	public List<TabImprese> getImpreseList() {
		return impreseList;
	}

	public void setImpreseList(List<TabImprese> impreseList) {
		this.impreseList = impreseList;
	}

	
	
	public WorkRequestBean getWorkRequestBean() {
		return workRequestBean;
	}

	public void setWorkRequestBean(WorkRequestBean workRequestBean) {
		this.workRequestBean = workRequestBean;
	}

	public List<TabAnagWorkRequest> getAnagWorkRequestList() {
		return anagWorkRequestList;
	}

	public void setAnagWorkRequestList(List<TabAnagWorkRequest> anagWorkRequestList) {
		this.anagWorkRequestList = anagWorkRequestList;
	}

	public List<TabWorkRequestColl> getWorkRequestCollList() {
		return workRequestCollList;
	}

	public void setWorkRequestCollList(List<TabWorkRequestColl> workRequestCollList) {
		this.workRequestCollList = workRequestCollList;
	}

	public TabWorkRequestColl getSelectedWorkRequest() {
		return selectedWorkRequest;
	}

	public void setSelectedWorkRequest(TabWorkRequestColl selectedWorkRequest) {
		this.selectedWorkRequest = selectedWorkRequest;
	}

	public Boolean getFlgProcessed() {
		return flgProcessed;
	}

	public void setFlgProcessed(Boolean flgProcessed) {
		this.flgProcessed = flgProcessed;
	}

	public HashMap<String, ViwUtenti> getUtentiHashMap() {
		return utentiHashMap;
	}

	public void setUtentiHashMap(HashMap<String, ViwUtenti> utentiHashMap) {
		this.utentiHashMap = utentiHashMap;
	}

	public Boolean getFlagAllegati() {
		return flagAllegati;
	}

	public void setFlagAllegati(Boolean flagAllegati) {
		this.flagAllegati = flagAllegati;
	}

	public Boolean getFlagApparati() {
		return flagApparati;
	}

	public void setFlagApparati(Boolean flagApparati) {
		this.flagApparati = flagApparati;
	}

	public Boolean getFlagWorkRequest() {
		return flagWorkRequest;
	}

	public void setFlagWorkRequest(Boolean flagWorkRequest) {
		this.flagWorkRequest = flagWorkRequest;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
	
	public List<TabRelRitardiSessioni> getListRitardi() {
		return listRitardi;
	}

	public void setListRitardi(List<TabRelRitardiSessioni> listRitardi) {
		this.listRitardi = listRitardi;
	}

	public OpenKmService getOpenKmService() {
		return openKmService;
	}

	public void setOpenKmService(OpenKmService openKmService) {
		this.openKmService = openKmService;
	}

	public RepositoryMediaService getRepositoryMediaService() {
		return repositoryMediaService;
	}

	public void setRepositoryMediaService(
			RepositoryMediaService repositoryMediaService) {
		this.repositoryMediaService = repositoryMediaService;
	}

	public AllegatoD1Bean getAllegatoD1Bean() {
		return allegatoD1Bean;
	}

	public void setAllegatoD1Bean(AllegatoD1Bean allegatoD1Bean) {
		this.allegatoD1Bean = allegatoD1Bean;
	}

	public SurveyService getSurveyService() {
		return surveyService;
	}

	public void setSurveyService(SurveyService surveyService) {
		this.surveyService = surveyService;
	}

	public List<TabRepositoryDocument> getListDocument() {
		return listDocument;
	}

	public void setListDocument(List<TabRepositoryDocument> listDocument) {
		this.listDocument = listDocument;
	}

	public List<TabRepositoryMedia> getListFoto() {
		return listFoto;
	}

	public void setListFoto(List<TabRepositoryMedia> listFoto) {
		this.listFoto = listFoto;
	}

	public TabRepositoryDocument getSelectedDocument() {
		return selectedDocument;
	}

	public void setSelectedDocument(TabRepositoryDocument selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public TabRepositoryMedia getSelectedFoto() {
		return selectedFoto;
	}

	public void setSelectedFoto(TabRepositoryMedia selectedFoto) {
		this.selectedFoto = selectedFoto;
	}

	public Boolean getFlagVerbaleD1() {
		return flagVerbaleD1;
	}

	public void setFlagVerbaleD1(Boolean flagVerbaleD1) {
		this.flagVerbaleD1 = flagVerbaleD1;
	}

	public List<TabRepositorySchedaRischi> getListDocumentDyn() {
		return listDocumentDyn;
	}

	public void setListDocumentDyn(List<TabRepositorySchedaRischi> listDocumentDyn) {
		this.listDocumentDyn = listDocumentDyn;
	}

	public TabSchedaDyn getTabSchedaDin() {
		return tabSchedaDin;
	}

	public void setTabSchedaDin(TabSchedaDyn tabSchedaDin) {
		this.tabSchedaDin = tabSchedaDin;
	}

	public List<ViwSbloccoSezioni> getViwSbloccoSezioniList() {
		return viwSbloccoSezioniList;
	}

	public void setViwSbloccoSezioniList(
			List<ViwSbloccoSezioni> viwSbloccoSezioniList) {
		this.viwSbloccoSezioniList = viwSbloccoSezioniList;
	}

	public ViwSbloccoSezioni getSelectedViwSbloccoSezioni() {
		return selectedViwSbloccoSezioni;
	}

	public void setSelectedViwSbloccoSezioni(
			ViwSbloccoSezioni selectedViwSbloccoSezioni) {
		this.selectedViwSbloccoSezioni = selectedViwSbloccoSezioni;
	}

	public List<TabAnagDocumenti> getListAnagDocumenti() {
		return listAnagDocumenti;
	}

	public void setListAnagDocumenti(List<TabAnagDocumenti> listAnagDocumenti) {
		this.listAnagDocumenti = listAnagDocumenti;
	}

	public TabAnagDocumenti getAnagDocumenti() {
		return anagDocumenti;
	}

	public void setAnagDocumenti(TabAnagDocumenti anagDocumenti) {
		this.anagDocumenti = anagDocumenti;
	}

	public boolean isUploadPanelVisible() {
		return uploadPanelVisible;
	}

	public void setUploadPanelVisible(boolean uploadPanelVisible) {
		this.uploadPanelVisible = uploadPanelVisible;
	}
	
	public List<ViwSessImpUser> getArrDitte() {
		return arrDitte;
	}

	public void setArrDitte(List<ViwSessImpUser> arrDitte) {
		this.arrDitte = arrDitte;
	}
	
	public List<ViwSessImpUser> getArrUser() {
		return arrUser;
	}

	public void setArrUser(List<ViwSessImpUser> arrUser) {
		this.arrUser = arrUser;
	}

	public ViwSessImpUser getSelectedImpresa() {
		return selectedImpresa;
	}

	public void setSelectedImpresa(ViwSessImpUser selectedImpresa) {
		this.selectedImpresa = selectedImpresa;
	}

	public List<TreeNode> getNodeDitte() {
		return nodeDitte;
	}

	public void setNodeDitte(List<TreeNode> nodeDitte) {
		this.nodeDitte = nodeDitte;
	}
	
	public TreeNode getRootSessione() {
		return rootSessione;
	}

	public void setRootSessione(TreeNode rootSessione) {
		this.rootSessione = rootSessione;
	}

	public boolean isSessioniUserImp() {
		return sessioniUserImp;
	}

	public void setSessioniUserImp(boolean sessioniUserImp) {
		this.sessioniUserImp = sessioniUserImp;
	}

	public boolean isSessioniArchiviateUserImp() {
		return sessioniArchiviateUserImp;
	}

	public void setSessioniArchiviateUserImp(boolean sessioniArchiviateUserImp) {
		this.sessioniArchiviateUserImp = sessioniArchiviateUserImp;
	}
	
	public List<TabAnagStatiSessione> getListStati() {
		return listStati;
	}

	public void setListStati(List<TabAnagStatiSessione> listStati) {
		this.listStati = listStati;
	}

	public AnagService getAnagService() {
		return anagService;
	}

	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}

	///*Chiamata al servizio per la spedizione a Pev*/
//	
	public void completamentoCollaudo(){
		Base64 codec = new Base64();
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		SawIn_Service service = new SawIn_Service();
		CompletamentoCollaudoApparatiResponse resp = null;
		CompletamentoCollaudoApparatiRequest req = new CompletamentoCollaudoApparatiRequest();
		
		try {
			SawIn port = null;
			try {
				port = service.getSawInSOAP();
			} catch (Exception e) {
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Connessione assente"));
			}
			int idco =selectedSessione.getCodiCollaudo().intValue();
			req.setIdCo(idco);
			
			/*Prende da OpenKm i file archiviati e genera quelli non archiviati, poi cifrati in base64*/
			
			if(selectedSessione.getFlgAllegatoA()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_A);
				byte[] allegatoA = reportService.generaRadiantiA(selectedSessione);
				byte[] encodedAllegatoA = codec.encode(allegatoA);
				req.setFileNameAllegatoA(selectedSessione.getCodiceDbr()+"_"+"VRISP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
				req.setAllegatoA(encodedAllegatoA);
			}
			if(selectedSessione.getFlgAllegatoB()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_B);
				byte[] allegatoB = reportService.generaRadiantiB(selectedSessione);
				byte[] encodedAllegatoB = codec.encode(allegatoB);
				req.setAllegatoB(encodedAllegatoB);
				req.setFileNameAllegatoB(selectedSessione.getCodiceDbr()+"_"+"VRROS"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			if(selectedSessione.getFlgAllegatoC()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_C);
				byte[] allegatoC = reportService.generaRadiantiC(selectedSessione);
				byte[] encodedAllegatoC = codec.encode(allegatoC);
				req.setAllegatoC(encodedAllegatoC);
				req.setFileNameAllegatoC(selectedSessione.getCodiceDbr()+"_"+"VRRET"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			if(selectedSessione.getFlgAllegatoD() && selectedSessione.getCodiStatoD2()!=null) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_D);
				byte[] allegatoD = reportService.generaRadiantiD(selectedSessione);
				byte[] encodedAllegatoD = codec.encode(allegatoD);
				req.setAllegatoD2(encodedAllegatoD);
				
				req.setFileNameAllegatoD2(selectedSessione.getCodiceDbr()+"_"+"VPIM2"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			if(selectedSessione.getFlgAllegatoD1()&& selectedSessione.getCodiStatoD1()!=null) {
				repositoryMediaService.getTabRepositoryDocumentVerbaleD1(selectedSessione.getCodiSessione()).getDescNome();
				req.setFileNameAllegatoD1(repositoryMediaService.getTabRepositoryDocumentVerbaleD1(selectedSessione.getCodiSessione()).getDescNome());
				byte[] allegatoD1 = IOUtils.toByteArray(downloadVerbaleD1().getStream());
				byte[] encodedAllegatoD1 = codec.encode(allegatoD1);
				req.setAllegatoD1(encodedAllegatoD1);
			}
			if(selectedSessione.getFlgAllegatoE()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_E);
				byte[] allegatoE = reportService.generaRadiantiE(selectedSessione);
				byte[] encodedAllegatoE = codec.encode(allegatoE);
				req.setAllegatoE(encodedAllegatoE);
				req.setFileNameAllegatoE(selectedSessione.getCodiceDbr()+"_"+"RLVAP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
		
			
			if(selectedSessione.getFlgApparatoEricsson()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_ERICSSON);
				byte[] apparatoEricsson= reportService.generaEricsson(selectedSessione);
				byte[] encodedApparatoEricsson = codec.encode(apparatoEricsson);
				req.setAllegatoApparati(encodedApparatoEricsson);
				req.setFileNameAllegatoApparati(selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			else if(selectedSessione.getFlgApparatoHuawei()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_HUAWEI);
				byte[] apparatoHuawei= reportService.generaHuawei(selectedSessione);
				byte[] encodedApparatoHuawei = codec.encode(apparatoHuawei);
				req.setAllegatoApparati(encodedApparatoHuawei);
				req.setFileNameAllegatoApparati(selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			else if(selectedSessione.getFlgApparatoNsn()) {
				String esitoAllegato = surveyService.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_NOKIA);
				byte[] apparatoNsn= reportService.generaNokia(selectedSessione);
				byte[] encodedApparatoNsn = codec.encode(apparatoNsn);
				req.setAllegatoApparati(encodedApparatoNsn);
				req.setFileNameAllegatoApparati(selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			}
			
			
			/*Crea cartelle zip con le foto divise per categoria */
//			List<TabRepositoryMedia> mediaSessione = repositoryMediaService.getRepositoryMediaByCodiSessione(selectedSessione.getCodiSessione()); 
			
			List<TabRepositoryMedia> mediaSessione = repositoryMediaService.getFiveRepMediaBySessione(selectedSessione.getCodiSessione());
			
			if(mediaSessione!=null && mediaSessione.size()>0){
				ByteArrayOutputStream baosCella = new ByteArrayOutputStream();
			    ZipOutputStream zosCella = new ZipOutputStream(baosCella);
			    boolean existFotoCella=false;
				
				ByteArrayOutputStream baosSala = new ByteArrayOutputStream();
			    ZipOutputStream zosSala = new ZipOutputStream(baosSala);
			    boolean existFotoSala=false;
			    
			    ByteArrayOutputStream baosSupporto = new ByteArrayOutputStream();
			    ZipOutputStream zosSupporto = new ZipOutputStream(baosSupporto);
			    boolean existFotosupporto=false;
			    
			    ByteArrayOutputStream baosLocalita = new ByteArrayOutputStream();
			    ZipOutputStream zosLocalita = new ZipOutputStream(baosLocalita);
			    boolean existFotoLocalita=false;
			    
			    
				for (TabRepositoryMedia tabRepositoryMedia : mediaSessione) {
					
				    ZipEntry entry = new ZipEntry(tabRepositoryMedia.getDescName());
				    entry.setSize(IOUtils.toByteArray(downloadMedia(tabRepositoryMedia).getStream()).length);
					
					if(tabRepositoryMedia.getDescCategoria().equalsIgnoreCase(Const.DESC_CELLA)) { 
					    zosCella.putNextEntry(entry);
					    zosCella.write(IOUtils.toByteArray(downloadMedia(tabRepositoryMedia).getStream()));
					    zosCella.closeEntry();
					    existFotoCella=true;
					}
					else if(tabRepositoryMedia.getDescCategoria().equalsIgnoreCase(Const.DESC_SALA)) { 
					    zosSala.putNextEntry(entry);
					    zosSala.write(IOUtils.toByteArray(downloadMedia(tabRepositoryMedia).getStream()));
					    zosSala.closeEntry();
					    existFotoSala=true;
					}
					else if(tabRepositoryMedia.getDescCategoria().equalsIgnoreCase(Const.DESC_SUPPORTO)) { 
					    zosSupporto.putNextEntry(entry);
					    zosSupporto.write(IOUtils.toByteArray(downloadMedia(tabRepositoryMedia).getStream()));
					    zosSupporto.closeEntry();
						existFotosupporto=true;
					}
					else if(tabRepositoryMedia.getDescCategoria().equalsIgnoreCase(Const.DESC_LOCALITA)) { 
					    zosLocalita.putNextEntry(entry);
					    zosLocalita.write(IOUtils.toByteArray(downloadMedia(tabRepositoryMedia).getStream()));
					    zosLocalita.closeEntry();
					    existFotoLocalita=true;
					}
				}	
				zosCella.close();
				if(existFotoCella){
					byte[] allegatoFotoCella = baosCella.toByteArray();
					byte[] encodedFotoCella = codec.encode(allegatoFotoCella);
					req.setFotoCella(encodedFotoCella);
				    req.setFileNameFotoCella("Foto Cella - "+ selectedSessione.getDescNomeSito()+".zip");
				}
			    zosSala.close();
			    if(existFotoSala){
					byte[] allegatoFotoSala = baosSala.toByteArray();
					byte[] encodedFotoSala = codec.encode(allegatoFotoSala);
					req.setFotoSala(encodedFotoSala);
				    req.setFileNameFotoSala("Foto Sala - "+ selectedSessione.getDescNomeSito()+".zip");
			    }
			    zosSupporto.close();
			    if(existFotosupporto){
				    byte[] allegatoFotoSupporto = baosSupporto.toByteArray();
					byte[] encodedFotoSupporto = codec.encode(allegatoFotoSupporto);
					req.setFotoSupporto(encodedFotoSupporto);
				    req.setFileNameFotoSupporto("Foto Supporto - "+ selectedSessione.getDescNomeSito()+".zip");
			    }
			    zosLocalita.close();
			    if(existFotoLocalita){
				    byte[] allegatoFotoLocalita = baosLocalita.toByteArray();
					byte[] encodedFotoLocalita = codec.encode(allegatoFotoLocalita);
					req.setFotoLocalita(encodedFotoLocalita);
				    req.setFileNameFotoLocalita("Foto Localit� - "+ selectedSessione.getDescNomeSito()+".zip");
			    }
		    }
			List<TabRepositoryDocument> rep = repositoryMediaService.getRepositoryDocumentByCodiSessione(selectedSessione.getCodiSessione());
			if(rep!=null && rep.size()>0){	
					for (TabRepositoryDocument tabRepositoryDocument : rep) {
								if(tabRepositoryDocument.getTipoFile()==Const.MISURE_PER_APOGEO) {
									if(req.getMisurazioniApogeo1()==null){
										byte[] misurApogeo1 = IOUtils.toByteArray(downloadDoc(tabRepositoryDocument).getStream());
										byte[] encodedMisurApogeo1 = codec.encode(misurApogeo1);
										req.setMisurazioniApogeo1(encodedMisurApogeo1);
										req.setFileNameMisurazioniApogeo1(tabRepositoryDocument.getDescNome());
									}
									else if(req.getMisurazioniApogeo2()==null){
										byte[] misurApogeo2 = IOUtils.toByteArray(downloadDoc(tabRepositoryDocument).getStream());
										byte[] encodedMisurApogeo2 = codec.encode(misurApogeo2);
										req.setMisurazioniApogeo2(encodedMisurApogeo2);
										req.setFileNameMisurazioniApogeo2(tabRepositoryDocument.getDescNome());
									}
									else if(req.getMisurazioniApogeo3()==null){
										byte[] misurApogeo3 = IOUtils.toByteArray(downloadDoc(tabRepositoryDocument).getStream());
										byte[] encodedMisurApogeo3 = codec.encode(misurApogeo3);
										req.setMisurazioniApogeo3(encodedMisurApogeo3);
										req.setFileNameMisurazioniApogeo3(tabRepositoryDocument.getDescNome());
									}
									else break;
					}
				}
			} 
			/*regola per l'esito chiusura*/
			List<ViwVerbale> verbali = sessioniService.getListVerbaliByCodiSessione(selectedSessione.getCodiSessione());
			Integer esitoSessione = 1;
			for (ViwVerbale viwVerbale : verbali) {
				if(viwVerbale.getCodiStatoVerbale()==3) {
					esitoSessione = 3;
					break;
				}
				if(viwVerbale.getCodiStatoVerbale()==2) {
					esitoSessione=2;
				}
			}
			req.setCausaliChiusura(Constants.mapRisultatoSurveySawIn.get(esitoSessione));
			
			//attributi da prendere dalla form che si dovrebbe aprire prima di inviare la req.
			req.setNote(Const.EMPTY_STRING);
			req.setDescrizioneRegolaEsito(descrizioneRegolaEsito(selectedSessione.getCodiSessione()));
			try {
				resp = port.completamentoCollaudoApparati(req);
			} catch (Exception e) {
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Connessione assente"));
				return;
			}
			
			
		
			String descrEsito = null;
			if(resp!=null && resp.getEsito()!=null && !resp.getEsito().equals(Const.EMPTY_STRING)){
				String esito = resp.getEsito();
				
				switch (esito) {
		         case Const.RES_OK:
		        	 sessioniService.setSentToPev(selectedSessione.getCodiSessione());
		        	//mail PEV
					 sessioniService.invioMailPev(selectedSessione, esitoSessione);
		        	  break;
		         case Const.RES_KEY_NOT_FOUND:
		        	 descrEsito = "Key not found";
		        	 System.out.println(descrEsito+resp.getDescrizioneEsito());
		        	 RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Spedizione documenti a PEV: " +descrEsito));
		             break;
		         case Const.RES_MISSING_FILENAME:
		        	 descrEsito = "Missing filename";
		        	 System.out.println(descrEsito+resp.getDescrizioneEsito());
		        	 RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Spedizione documenti a PEV: " +descrEsito));
		             break;
		         case Const.RES_MISSING_MANDATORY_FIELD:
		        	 descrEsito = "Missing mandatory field"; 
		        	 System.out.println(descrEsito+resp.getDescrizioneEsito());
		        	 RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Spedizione documenti a PEV: " +descrEsito));
		             break;
		         case Const.RES_GENERIC_ERROR:
		        	 descrEsito = "Errore";
		        	 System.out.println(descrEsito+resp.getDescrizioneEsito());
		        	 RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Spedizione documenti a PEV: " +descrEsito));
		             break;
		         default:
		             throw new IllegalArgumentException("Invalid : " + esito);
		     }
				
				
				reloadCollaudiArchiviati();
				return;
			}
			else {
				 descrEsito = "Errore";
	        	 RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Spedizione documenti a PEV: " +descrEsito));
			}
		} catch (WebServiceException e){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Connessione assente"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private String getData(Date data){
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		return df2.format(data);
	}

	public SessioniFilter getSessioniFilter() {
		return sessioniFilter;
	}

	public void setSessioniFilter(SessioniFilter sessioniFilter) {
		this.sessioniFilter = sessioniFilter;
	}
	
	public List<ViwRisultatoSurvey> getListRisultati() {
		return listRisultati;
	}

	public void setListRisultati(List<ViwRisultatoSurvey> listRisultati) {
		this.listRisultati = listRisultati;
	}

	public void openInfoEsiti(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		rootMotivazioneEsiti = new DefaultTreeNode("Sessione: "+selectedSessione.getDescNomeSito(), null);
		listRisultati = sessioniService.getAllRisultatiByCodiSessione(selectedSessione.getCodiSessione());
		if(listRisultati.size()>0){
			List<ViwRisultatoSurvey> listAppoRisultati = new ArrayList<ViwRisultatoSurvey>();
			for(ViwRisultatoSurvey risultato : listRisultati){
				boolean risultatoPresente = false; 
				for(ViwRisultatoSurvey risultatoAppo : listAppoRisultati){
					if(risultatoAppo.getCodiOggetto().equals(risultato.getCodiOggetto())){
						risultatoPresente = true;
					}
				}

				if(!risultatoPresente && risultato.getDescValoreRisultatoSurvey()!=null && (risultato.getCodiValoreRisultatoSurvey()==Const.VERBALE_POSITIVO_CON_RISERVA || risultato.getCodiValoreRisultatoSurvey()==Const.VERBALE_NEGATIVO)){
					listAppoRisultati.add(risultato);
				}
			}
				createNodesRisultati(rootMotivazioneEsiti, listAppoRisultati);
		}else{
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Non sono stati dati esiti agli allegati associati alla sessione"));
		}
	}
	
	/* metodo note web e tablet */
	public void openNoteWebAndTablet() {
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un collaudo della lista"));
			return;
		}
		sessioneSelezionata = sessioniService.findSessione(selectedSessione.getCodiSessione());
		HashMap<Integer, ViwEsitoSurvey> esitiMap = new HashMap<Integer, ViwEsitoSurvey>();
		for(ViwEsitoSurvey viwEsitoSurvey : sessioniService.getNoteTabletByCodiSessione(sessioneSelezionata.getCodiSessione())){
			esitiMap.put(viwEsitoSurvey.getCodiSurvey(), viwEsitoSurvey);
		}
		listNoteEsito = new ArrayList<ViwEsitoSurvey>(esitiMap.values());
		listNoteDitta = sessioniService.getNoteDittaBySessione(sessioneSelezionata.getCodiSessione());
		listTabNote = sessioniService.getNote(sessioneSelezionata.getCodiSessione());
		
		callDialog();

		
	}
	
	
	public void addNoteWeb() {
        tabNote.setDescNota("");
		RequestContext.getCurrentInstance().execute("PF('aggiungiNotaDialog').show()");
		
	}
	
	public void saveNotaWeb() {
		if (tabNote.getDescNota() != null && !tabNote.getDescNota().trim().isEmpty()) {
			sessioniService.saveNotaWeb(tabNote.getDescNota(), selectedSessione.getCodiSessione(), currentUser.getUsername());
			RequestContext.getCurrentInstance().execute("PF('aggiungiNotaDialog').hide()");
			listTabNote=sessioniService.getNote(selectedSessione.getCodiSessione());
		}
		else {
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione la nota non pu� essere vuota"));

		}
		
	}
	
	public void editNotaWeb() {
		if(selectedNota == null) {
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare la nota da modificare"));
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('editNotaDialog').show()");
			
//			sessioniService.updateNotaWeb(selectedNota.getDescNota(), currentUser.getUsername());
		}
	}
	
	public void updateNotaWeb() {
		if(selectedNota != null && selectedNota.getDescNota() != null && !selectedNota.getDescNota().trim().isEmpty()) {
			selectedNota.setDescUser(currentUser.getUsername());
			sessioniService.updateNotaWeb(selectedNota);
			RequestContext.getCurrentInstance().execute("PF('editNotaDialog').hide()");
		}else {
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione la nota non pu� essere vuota"));
		}
	}
	
	public void createNodesRisultati (TreeNode rootMotivazioneEsiti, List<ViwRisultatoSurvey> listRisultati) {
		for (ViwRisultatoSurvey appoRisultati : listRisultati) {
				ViwVerbale viwVerbaleEsito = sincronizzazioneService.getViwVerbaleBySessioneOggetto(appoRisultati.getCodiSessione(), appoRisultati.getCodiOggetto());
				String descEsito = Const.EMPTY_STRING;
				if(viwVerbaleEsito!=null){
					if(viwVerbaleEsito.getCodiStatoVerbale().equals(Const.POSITIVO_CON_RISERVA_INT)){
						descEsito = Const.ESITO_POSITIVO_CON_RISERVA;
					}else if(viwVerbaleEsito.getCodiStatoVerbale().equals(Const.NEGATIVO_INT)){
						descEsito = Const.ESITO_NEGATIVO;
					}
				}
					TreeNode padre = new DefaultTreeNode("Allegato: " + appoRisultati.getDescOggetto() + " " +descEsito,rootMotivazioneEsiti);
					nodeRisultati.add(padre);
					List<ViwRisultatoSurvey> listFigli = sessioniService.getAllRisultatiByCodiSessioneAndCodiOggetto(appoRisultati.getCodiSessione(), appoRisultati.getCodiOggetto());
					for(ViwRisultatoSurvey figlio : listFigli){
						if(figlio.getDescAnagRisultati()!= null){
							padre.getChildren().add(new DefaultTreeNode(figlio.getDescAnagRisultati()));
						}
					}
				
			}
			if (listRisultati.size()>0){
				RequestContext.getCurrentInstance().execute("PF('infoEsitiDialog').show()");
			}else{
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Non ci sono allegati con esito N/R associati alla sessione"));
			}
		}	

	public List<TreeNode> getNodeRisultati() {
		return nodeRisultati;
	}

	public void setNodeRisultati(List<TreeNode> nodeRisultati) {
		this.nodeRisultati = nodeRisultati;
	}

	public TreeNode getRootMotivazioneEsiti() {
		return rootMotivazioneEsiti;
	}

	public void setRootMotivazioneEsiti(TreeNode rootMotivazioneEsiti) {
		this.rootMotivazioneEsiti = rootMotivazioneEsiti;
	}

	public SincronizzazioneService getSincronizzazioneService() {
		return sincronizzazioneService;
	}

	public void setSincronizzazioneService(
			SincronizzazioneService sincronizzazioneService) {
		this.sincronizzazioneService = sincronizzazioneService;
	}
	
	public String descrizioneRegolaEsito(Integer codiSessione){
		String descrizioneRegolaEsitoAppo=Const.EMPTY_STRING;
		String descrizioneRegolaEsito = Const.EMPTY_STRING;
		listRisultati = sessioniService.getAllRisultatiNegativiRiservaByCodiSessione(codiSessione);
		if(listRisultati.size()>0){
			for (int i = 0; i < listRisultati.size(); i++) {
				ViwRisultatoSurvey risultati=listRisultati.get(i);
				ViwVerbale viwVerbaleEsito = sincronizzazioneService.getViwVerbaleBySessioneOggetto(risultati.getCodiSessione(), risultati.getCodiOggetto());
				String descEsito = Const.EMPTY_STRING;
				String descAllegato= Const.EMPTY_STRING;
				String stringaRisultati= Const.EMPTY_STRING;
				if(viwVerbaleEsito.getCodiStatoVerbale().equals(Const.POSITIVO_CON_RISERVA_INT)){
					descEsito = Const.POSITIVO_CON_RISERVA;
				}else if(viwVerbaleEsito.getCodiStatoVerbale().equals(Const.NEGATIVO_INT)){
					descEsito = Const.NEGATIVO;
				}

				switch (risultati.getCodiOggetto()) {
				case Const.ALLEGATO_A:
					descAllegato=Const.A;
					break;
				case Const.ALLEGATO_B:
					descAllegato=Const.B;
					break;
				case Const.ALLEGATO_C:
					descAllegato=Const.C;
					break;
				case Const.ALLEGATO_D:
					descAllegato=Const.D2;
					break;
				case Const.ALLEGATO_D1:
					descAllegato=Const.D1;
					break;
				case Const.ALLEGATO_E:
					descAllegato=Const.E;
					break;
				case Const.ALLEGATO_NOKIA:
					descAllegato=Const.NOKIA;
					break;
				case Const.ALLEGATO_ERICSSON:
					descAllegato=Const.ERICSSON;
					break;
				case Const.ALLEGATO_HUAWEI:
					descAllegato=Const.HUAWEI;
					break;
				}

				stringaRisultati=risultati.getCodiAnagRisultato() + ",";

				if(descrizioneRegolaEsitoAppo.contains(descAllegato+"_"+descEsito)) {
					descrizioneRegolaEsitoAppo= descrizioneRegolaEsitoAppo + stringaRisultati;
				}
				else {
					if(descrizioneRegolaEsitoAppo.lastIndexOf(',') > 0){
						descrizioneRegolaEsitoAppo = descrizioneRegolaEsitoAppo.substring(0, descrizioneRegolaEsitoAppo.length() -1);
						descrizioneRegolaEsitoAppo = descrizioneRegolaEsitoAppo + "|";
						descrizioneRegolaEsito = descrizioneRegolaEsito + descrizioneRegolaEsitoAppo;
						descrizioneRegolaEsitoAppo = descAllegato+"_"+descEsito+":"+stringaRisultati;
					}
					else {
						descrizioneRegolaEsitoAppo=descrizioneRegolaEsitoAppo+descAllegato+"_"+descEsito+":"+stringaRisultati;
					}
				}
			}
			descrizioneRegolaEsitoAppo = descrizioneRegolaEsitoAppo.substring(0, descrizioneRegolaEsitoAppo.length() -1);
			descrizioneRegolaEsitoAppo = descrizioneRegolaEsitoAppo + "|";
			descrizioneRegolaEsito = descrizioneRegolaEsito + descrizioneRegolaEsitoAppo;
			return descrizioneRegolaEsito;
		}else{
			return descrizioneRegolaEsito;
		}
	}
	
	public void openAssocUsersManuale(){
		if(selectedSessione==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare una collaudo della lista"));
			return;
		}
		if(!selectedSessione.getCodiStato().equals(Const.DISPONIBILE) && !selectedSessione.getCodiStato().equals(Const.IN_LAVORAZIONE)){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione stato della sessione non valido per l'operazione"));
			return;
		}
		setTelecomOrImpUser(null); //Variabile usata per sapere quale tipologia di utente � stata selezionata
		//		LISTA DI TUTTI GLI UTENTI associati e no   DI QUEL AREA DI COMPETENZA

		//List<ViwUtenti> userlistForbuiltHashMap = sessioniService.getUtentiListByAreaComp(selectedSessione.getCodiAreaComp());
		TabRegioni regioneByName = sessioniService.getIdRegioneByNome(selectedSessione.getDescRegione());
		if(regioneByName== null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione regione della sessione non trovata"));
			return;
		}

		setCodiUtentiByRegione(userService.getUtentiByRegione(regioneByName.getIdRegione()));
		List<ViwUtenti> userlistForbuiltHashMap = userService.getUserInCodiUtente(getCodiUtentiByRegione());
		List<ViwUtenti> vendorToAddInHashMap = getUtentiVendor();
		for (ViwUtenti anagUtenti : userlistForbuiltHashMap) {
			utentiHashMap.put(setInfoListPickListManuale(anagUtenti), anagUtenti);
		}
		
		for(ViwUtenti vendors: vendorToAddInHashMap){
			if(!utentiHashMap.containsValue(vendors)){
				utentiHashMap.put(setInfoListPickListManuale(vendors),vendors);
			}
		}
		/* Mi serve per non rimandare l' EMAIL agli utenti che erano gi� associati*/
		List<ViwUtenti> utentiAssociati=userService.getAllAssocSessionManuale(selectedSessione.getCodiSessione());
		for (ViwUtenti viwUtenti : utentiAssociati) {
			viwUtenti.setFlagIsAssoc(true);
			utentiHashMap.put(setInfoListPickListManuale(viwUtenti), viwUtenti);
		}
		
		List<String> utentiTarget =	listUtentiToStringManuale(utentiAssociati);
		
		setUtentiToAssoc(new DualListModel<>(new ArrayList<String>(), utentiTarget));
		RequestContext.getCurrentInstance().execute("PF('assocUsersManualeDialog').show()");
	}
	
	private String setInfoListPickListManuale(ViwUtenti anagUtenti) {
		String impOrNot="";
		switch (anagUtenti.getCodiRole()) {
		case Const.UTENTE_VENDOR:
			impOrNot = Const.DESC_UTENTE_VENDOR;
			break;
		case Const.Referente_Territoriale_OP:
			impOrNot = Const.UTENTE_REFTEROP;
			break;
		case Const.Operatore:
			impOrNot = Const.UTENTE_OPER_AOU;
			break;
		default:
			impOrNot = "Ruolo non trovato";
			break;
		}
		
		return anagUtenti.getDescUser()+" ("+anagUtenti.getDescNome()+" "+anagUtenti.getDescCgn()+"-"+impOrNot+")";
	}
	
	private List<String> listUtentiToStringManuale(List<ViwUtenti> arrUtenti) {
		List<String> arrUtentiString = new ArrayList<String>();
		for (ViwUtenti anagUtenti : arrUtenti) {
			arrUtentiString.add(setInfoListPickListManuale(anagUtenti));
		}
		return arrUtentiString;
	}

	public void listenerBodyTipoTecnicoManuale(){
		List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
		try {
			switch(getTelecomOrImpUser()){
			case 1:
				listaUtenti = userService.getRefTerOPByRegione(getCodiUtentiByRegione());
				break;
			case 2: 
				listaUtenti = userService.getOperAOUByRegione(getCodiUtentiByRegione());
				break;
			case 3:
				listaUtenti = getUtentiVendor();
				break;
			default: 
				break;
			}
		} catch (Exception e) {
			
		}
		setUtentiToAssoc(getDualListModelAssocUtentiManuale(utentiToAssoc.getTarget(),listaUtenti));
		
	}
	
	public List<ViwUtenti> getUtentiVendor(){
		List<ViwUtenti> listaUtenti = new ArrayList<ViwUtenti>();
		try {
			Integer codiVendor = null;
			if(selectedSessione.getFlgApparatoEricsson() == true) {
				codiVendor = Const.VENDOR_ERICSSON;		
			}
			else if (selectedSessione.getFlgApparatoHuawei() == true) {
				codiVendor = Const.VENDOR_HUAWEI;
			}
			else if (selectedSessione.getFlgApparatoNsn() == true) {
				codiVendor = Const.VENDOR_NOKIA;
			} if(codiVendor!= null){
				listaUtenti = sessioniService.getUtentiVendor(codiVendor);
			}
			else  {
				TabPoloTecnologico tabPoloTecnologico= new TabPoloTecnologico();
					tabPoloTecnologico =sessioniService.findPoloTecnologicoByRegione(selectedSessione.getDescRegione());
					listaUtenti = sessioniService.getUtentiVendor(tabPoloTecnologico.getCodiVendor());
			}
		} catch (Exception e) {
		}
		return listaUtenti;
	}
	
	private  DualListModel<String> getDualListModelAssocUtentiManuale(List<String> utentiAssocOnDb, List<ViwUtenti> arrUtenti){
		List<String> utentiSources = new ArrayList<String>();
		if(utentiAssocOnDb!=null && utentiAssocOnDb.size()>0){
			for (ViwUtenti anagUtenti : arrUtenti) {
				String descUserForPicklist = setInfoListPickListManuale(anagUtenti);
				if(!utentiAssocOnDb.contains(descUserForPicklist)){
					utentiSources.add(descUserForPicklist);
				}
			}
		}else
			utentiSources = listUtentiToStringManuale(arrUtenti);
		return new DualListModel<String>(utentiSources, utentiAssocOnDb);
	}


	public void listenerTipoTecnicoManuale() {
		if(telecomOrImpUser==null){
			setCodiImpresa(null);
			return;}
		else{
			setCodiImpresa(null);
			listenerBodyTipoTecnicoManuale();
		}
	}
	
	public void saveAssocUtenteManuale() {
		try {
			List<ViwUtenti> utentiAssociati=userService.getAllAssocSessionManuale(selectedSessione.getCodiSessione());
			List<Integer> codiUserAssoc= new ArrayList<Integer>();
			for (ViwUtenti viwUtenti : utentiAssociati) {
				codiUserAssoc.add(viwUtenti.getCodiUtente());
			}
			List<String> target= utentiToAssoc.getTarget();
			List<ViwUtenti> utenti= new ArrayList<>();
			for (String string : target) {
				utenti.add(utentiHashMap.get(string));
			}
			Boolean result = sessioniService.saveAssocUtente(utenti,selectedSessione,codiUserAssoc);
			if(result){
				reloadCollaudiInLavorazione();
				RequestContext.getCurrentInstance().execute("PF('assocUsersManualeDialog').hide()");
			}else
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Associazione Fallita"));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Associazione Fallita"));
			e.printStackTrace();
		}
	}



	public String getDescOggettiTabRisSurvey() {
		return descOggettiTabRisSurvey;
	}

	public void setDescOggettiTabRisSurvey(String descOggettiTabRisSurvey) {
		this.descOggettiTabRisSurvey = descOggettiTabRisSurvey;
	}

	public TabNote getTabNote() {
		return tabNote;
	}

	public void setTabNote(TabNote tabNote) {
		this.tabNote = tabNote;
	}

	public List<TabNote> getListTabNote() {
		return listTabNote;
	}

	public void setListTabNote(List<TabNote> listTabNote) {
		this.listTabNote = listTabNote;
	}

	public TabNote getSelectedNota() {
		return selectedNota;
	}

	public void setSelectedNota(TabNote selectedNota) {
		this.selectedNota = selectedNota;
	}

	public List<ViwEsitoSurvey> getListNoteEsito() {
		return listNoteEsito;
	}

	public void setListNoteEsito(List<ViwEsitoSurvey> listNoteEsito) {
		this.listNoteEsito = listNoteEsito;
	}

	public List<TabNote> getListNoteDitta() {
		return listNoteDitta;
	}

	public void setListNoteDitta(List<TabNote> listNoteDitta) {
		this.listNoteDitta = listNoteDitta;
	}

	public ViwSessioni getSessioneSelezionata() {
		return sessioneSelezionata;
	}

	public void setSessioneSelezionata(ViwSessioni sessioneSelezionata) {
		this.sessioneSelezionata = sessioneSelezionata;
	}
	
	public List<Integer> getCodiUtentiByRegione (){
		return codiUtentiByRegione;
	}
	
	public void setCodiUtentiByRegione(List<Integer> codiUtentiByRegione){
		this.codiUtentiByRegione = codiUtentiByRegione;
	}

	public UserService getUserService(){
		return userService;
	}
	
	public void setUserService(UserService userService){
		this.userService = userService;
	}
	
	public boolean renderedCompilazione(){
		Integer codiRuolo = currentUser.getCodiRole();
		if(codiRuolo == Const.Referente_Territoriale_OP || codiRuolo == Const.UTENTE_VENDOR || codiRuolo == Const.Operatore){
			
			return true;
		}
		return false;
	}
	
	public void fillOggettiMap(){
		List<TabOggetti> listaOggetti = anagService.getAllOggetti();
		if(listaOggetti!= null && listaOggetti.size()>0){
			for(TabOggetti oggetto : listaOggetti){
				oggettiMap.put(oggetto.getCodiOggetto(), oggetto.getDescOggetto());
			}
		}
	}

	public HashMap<Integer, String> getOggettiMap() {
		return oggettiMap;
	}

	public void setOggettiMap(HashMap<Integer, String> oggettiMap) {
		this.oggettiMap = oggettiMap;
	}
	
	public Boolean isSessioneWithNota(Integer codiSessione){ 
		ViwSessioniNote sessioneNota = sessioniNoteMap.get(codiSessione);
		if(sessioneNota != null && (sessioneNota.getFlgNotaMoi() || sessioneNota.getFlgNotaMos() || sessioneNota.getFlgNotaWeb())){
			return true;
		}
		return false;
	}	
	
	public void callDialog(){
		if(selectedSessione.getCodiStato().equals(Const.CHIUSO)){
			RequestContext.getCurrentInstance().execute("PF('noteWebTabletDialogArchiviati').show()");
		}else{
		    RequestContext.getCurrentInstance().execute("PF('noteWebTabletDialog').show()");
		}
	}
	
	public void sendNote(){
		if(selectedNota!=null){
			if(selectedNota.getDescUser().equals(currentUser.getUsername())){
			sessioniService.sendMailNota(currentUser, selectedSessione, selectedNota);
	        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Info", "Nota inviata"));
			}else{
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione le note possono essere inviate solo da chi le inserisce"));
				return;
			}
		}else{
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare la nota da inviare"));
			return;
		}
	}
	
	
	private void fillSessioniNoteMap(List<ViwSessioni> arrCollaudiInLavorazione, List<ViwSessioni> arrCollaudiArchiviati) {
		List<Integer> listCodiSessioni = new ArrayList<Integer>();
		for(ViwSessioni sessione : arrCollaudiInLavorazione){
			listCodiSessioni.add(sessione.getCodiSessione());
		}
		for(ViwSessioni sessione : arrCollaudiArchiviati){
			listCodiSessioni.add(sessione.getCodiSessione());
		}
		List<ViwSessioniNote> sessioniNote = sessioniService.getSessioniNoteByListSessioni(listCodiSessioni);
		for(ViwSessioniNote sessioneConNota : sessioniNote){
			sessioniNoteMap.put(sessioneConNota.getCodiSessione(), sessioneConNota);
		}
	}

	public HashMap<Integer, ViwSessioniNote> getSessioniNoteMap() {
		return sessioniNoteMap;
	}

	public void setSessioniNoteMap(HashMap<Integer, ViwSessioniNote> sessioniNoteMap) {
		this.sessioniNoteMap = sessioniNoteMap;
	}
	
}



