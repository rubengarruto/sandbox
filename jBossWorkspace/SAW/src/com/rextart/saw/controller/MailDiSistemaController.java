package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.service.UserService;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.MethodUtilities;

@ManagedBean(name = "mailDiSistemaController")
@ViewScoped
public class MailDiSistemaController implements Serializable, Controller{
	
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{userService}")
	private UserService userService;
	
	private List<ViwUtenti> arrUtenti;
	private List<String> listAreeComp;
	private List<String> listRoles;
	private List<String> selectedAreaComp = new ArrayList<>();
    private List<String> selectedRoles = new ArrayList<>();
	
	private CurrentUser currentUser;
	
	private String textBody;
	
	@ManagedProperty(value="#{areaDiCompLabel}")
	private String areaDiCompLabel;
	
	@ManagedProperty(value="#{roleLabel}")
	private String roleLabel;
	
	private String textOggetto;
	
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		setListAreeComp(userService.getAllDescAreaComp());
		setListRoles(userService.getAllDescRoles());
		if (selectedAreaComp.size() == 0) {
	    	areaDiCompLabel = "Selezionare";
	    }
		if (selectedRoles.size() == 0) {
	    	roleLabel = "Selezionare";
	    }
	}



	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void openNew() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void save() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public List<String> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<String> listRoles) {
		this.listRoles = listRoles;
	}

	public List<String> getListAreeComp() {
		return listAreeComp;
	}
	
	public void setListAreeComp(List<String> listAreeComp) {
		this.listAreeComp = listAreeComp;
	}
	
	public List<String> getSelectedAreaComp() {
        return selectedAreaComp;
    }
 
    public void setSelectedAreaComp(List<String> selectedAreaComp) {
        this.selectedAreaComp = selectedAreaComp;
    }
 
    public List<String> getSelectedRoles() {
        return selectedRoles;
    }
 
    public void setSelectedRoles(List<String> selectedRoles) {
        this.selectedRoles = selectedRoles;
    }

	public UserService getUserService() {
		return userService;
	}



	public void setUserService(UserService userService) {
		this.userService = userService;
	}



	public List<ViwUtenti> getArrUtenti() {
		return arrUtenti;
	}



	public void setArrUtenti(List<ViwUtenti> arrUtenti) {
		this.arrUtenti = arrUtenti;
	}
	
	
	
	public String getAreaDiCompLabel() {
		return areaDiCompLabel;
	}



	public void setAreaDiCompLabel(String areaDiCompLabel) {
		this.areaDiCompLabel = areaDiCompLabel;
	}

	public String getRoleLabel() {
		return roleLabel;
	}



	public void setRoleLabel(String roleLabel) {
		this.roleLabel = roleLabel;
	}
	
	public String getTextBody() {
		return textBody;
	}



	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}

	public String getTextOggetto() {
		return textOggetto;
	}



	public void setTextOggetto(String textOggetto) {
		this.textOggetto = textOggetto;
	}



	public void populateLabelAreaDiComp() {
	    /* Populating the label with the selected options */
	    areaDiCompLabel = new String("");
	    if (selectedAreaComp.size() == 0) {
	    	areaDiCompLabel = "Selezionare";
	    } else {
	        for (int i = 0; i < selectedAreaComp.size(); i++) {
	            if (areaDiCompLabel.length() == 0) {
	            	areaDiCompLabel = selectedAreaComp.get(i);
	            } else {
	                areaDiCompLabel = areaDiCompLabel + ", " + selectedAreaComp.get(i);
	            }
	        }
	    }
	}
	
	
	public void populateLabelRole() {
	    /* Populating the label with the selected options */
		roleLabel = new String("");
	    if (selectedRoles.size() == 0) {
	    	roleLabel = "Selezionare";
	    } else {
	        for (int i = 0; i < selectedRoles.size(); i++) {
	            if (roleLabel.length() == 0) {
	            	roleLabel = selectedRoles.get(i);
	            } else {
	            	roleLabel = roleLabel + ", " + selectedRoles.get(i);
	            }
	        }
	    }
	}
	
	public void reloadMail(){
		textOggetto = null;
		textBody = null;
		selectedAreaComp.clear();
		selectedRoles.clear();
		areaDiCompLabel = "Selezionare";
		roleLabel = "Selezionare";
		
	}
	
	
	public void invioMailDiSistema(){
		try{
			if(textOggetto.length()<1 || textBody.length() < 1 || selectedAreaComp.size() < 1 || selectedRoles.size() < 1){
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Uno o pi� dati necessari all'invio della mail non sono stati inseriti"));
				return;
			}
			
			boolean statoInvioMail = (boolean) userService.invioMailDiSistema(MethodUtilities.sostituisciAccentate(textOggetto), MethodUtilities.sostituisciAccentate(textBody), selectedAreaComp, selectedRoles);

			if(statoInvioMail){
				reloadMail();
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,Const.SEVERITY_INFO,"Mail inviata"));				
			}else{
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Invio Mail Fallito"));
			}
			
			
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Invio Mail Fallito"));
			e.printStackTrace();
		}
	}




	
    
    
}
