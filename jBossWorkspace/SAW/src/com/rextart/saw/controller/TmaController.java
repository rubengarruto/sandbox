package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.utility.Const;

@ManagedBean(name = "tmaController")
@ViewScoped
public class TmaController implements Serializable, Controller{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;
	
	@ManagedProperty(value="#{arrTma}")
	private List<TabAnagTma> arrTma;
	private TabAnagTma selectedTma;
	private TabAnagTma newTma;
	private CurrentUser currentUser;
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		arrTma=anagService.getAllAnagTma();
		setNewTma(new TabAnagTma());
	}

	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openNew() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save() {
		try{
			TabAnagTma tab = anagService.getAnagTmaUnique(newTma.getDescMarca(), newTma.getDescModello());
			if(tab == null){
				anagService.saveTma(newTma);
				updateAnagrafica();
				setNewTma(new TabAnagTma());
				reloadTableTma();
				RequestContext.getCurrentInstance().execute("PF('newTmaDialog').hide()");
			}else{
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"I dati inseriti sono gi� presenti"));	
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	private void reloadTableTma() {
		setArrTma(anagService.getAllAnagTma());
	}
	
	public AnagService getAnagService() {
		return anagService;
	}
	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}
	public List<TabAnagTma> getArrTma() {
		return arrTma;
	}
	public void setArrTma(List<TabAnagTma> arrTma) {
		this.arrTma = arrTma;
	}
	public TabAnagTma getSelectedTma() {
		return selectedTma;
	}
	public void setSelectedTma(TabAnagTma selectedTma) {
		this.selectedTma = selectedTma;
	}
	
	public TabAnagTma getNewTma(){
		return newTma;
	}
	
	public void setNewTma(TabAnagTma newTma){
		this.newTma = newTma;
	}
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public void updateAnagrafica(){
		TabSyncAnagrafica tabSyncAnagrafica = anagService.getSyncAnagByNameTable(Const.SYNC_TMA);
		Integer addNumeroVersione = tabSyncAnagrafica.getNumeVersione() + 1;
		tabSyncAnagrafica.setNumeVersione(addNumeroVersione);
		anagService.updateSyncAnagrafica(tabSyncAnagrafica);
	}
}
