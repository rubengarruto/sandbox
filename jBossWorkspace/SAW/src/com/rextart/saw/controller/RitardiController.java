package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.utility.Const;

@ManagedBean(name = "ritardiController")
@ViewScoped
public class RitardiController implements Serializable, Controller{
	
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;
	
	@ManagedProperty(value="#{arrRitardi}")
	private List<TabAnagRitardi> arrRitardi;
	private TabAnagRitardi selectedRitardi;
	private TabAnagRitardi newRitardi;
	private CurrentUser currentUser;
	
	@PostConstruct
	@Override
	public void init(){
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		arrRitardi = anagService.getAllRitardi();
		setNewRitardi(new TabAnagRitardi());
	}
	
	
	@Override
	public void openEdit() {
		if(selectedRitardi==null){
			RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un ritardo della lista"));
			return;
		}
		setNewRitardi(selectedRitardi);
		RequestContext.getCurrentInstance().execute("PF('editRitardiDialog').show()");		
	}

	@Override
	public void openNew() {
		// TODO Auto-generated method stub
		setNewRitardi(new TabAnagRitardi());

	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save() {
		try {
			TabAnagRitardi tab = anagService.getAnagRitardiUnique(newRitardi.getDescRitardo(), newRitardi.getFlagCheckOut());
			if(tab==null){
				anagService.saveRitardi(newRitardi);
				updateAnagrafica();
				setNewRitardi(new TabAnagRitardi());
				reloadTableRitardi();
				RequestContext.getCurrentInstance().execute("PF('newRitardiDialog').hide()");
			}else{
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"I dati inseriti sono gi� presenti"));
			}			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	private void reloadTableRitardi() {
		setArrRitardi(anagService.getAllRitardi());
	}
	
	public AnagService getAnagService() {
		return anagService;
	}
	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}
	public List<TabAnagRitardi> getArrRitardi() {
		return arrRitardi;
	}
	public void setArrRitardi(List<TabAnagRitardi> arrRitardi) {
		this.arrRitardi = arrRitardi;
	}
	public TabAnagRitardi getSelectedRitardi() {
		return selectedRitardi;
	}
	public void setSelectedRitardi(TabAnagRitardi selectedRitardi) {
		this.selectedRitardi = selectedRitardi;
	}
	
	public TabAnagRitardi getNewRitardi(){
		return newRitardi;
	}
	
	public void setNewRitardi(TabAnagRitardi newRitardi){
		this.newRitardi = newRitardi;
	}
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public void updateAnagrafica(){
		TabSyncAnagrafica tabSyncAnagrafica = anagService.getSyncAnagByNameTable(Const.SYNC_RITARDI);
		Integer addNumeroVersione = tabSyncAnagrafica.getNumeVersione() + 1;
		tabSyncAnagrafica.setNumeVersione(addNumeroVersione);
		anagService.updateSyncAnagrafica(tabSyncAnagrafica);
	}	
	
	public void updateRitardi(){
		try {
			if(selectedRitardi==null){
				RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN,Const.SEVERITY_WARNING,"Attenzione selezionare un ritardo della lista"));
				return;
			}
			anagService.updateRitardi(newRitardi, currentUser);
			reloadTableRitardi();
			RequestContext.getCurrentInstance().execute("PF('editRitardiDialog').hide()");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			reloadTableRitardi();
			e.printStackTrace();
		}
	}

}
