package com.rextart.saw.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.service.AnagService;
import com.rextart.saw.utility.Const;

@ManagedBean(name = "diplexerController")
@ViewScoped
public class DiplexerController implements Serializable, Controller{

private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{anagService}")
	private AnagService anagService;
	
	@ManagedProperty(value="#{arrDiplexer}")
	private List<TabAnagDiplexer> arrDiplexer;
	private TabAnagDiplexer selectedDiplexer;
	private TabAnagDiplexer newDiplexer;
	private CurrentUser currentUser;
	
	@PostConstruct
	@Override
	public void init() {
		setCurrentUser((CurrentUser) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal());
		arrDiplexer=anagService.getAllAnagDiplexer();
		setNewDiplexer(new TabAnagDiplexer());
	}

	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void openNew() {
		setNewDiplexer(new TabAnagDiplexer());
	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save() {
		try{
			TabAnagDiplexer tab = anagService.getAnagDiplexerUnique(newDiplexer.getDescMarca(), newDiplexer.getDescModello());
			if(tab==null){
				anagService.saveDiplexer(newDiplexer);
				updateAnagrafica();
				setNewDiplexer(new TabAnagDiplexer());
				reloadTableDiplexer();
				RequestContext.getCurrentInstance().execute("PF('newDiplexerDialog').hide()");
			}else{
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"I dati inseriti sono gi� presenti"));	
			}		
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,Const.SEVERITY_ERROR,"Salvataggio Fallito"));
			e.printStackTrace();
		}
	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub
		
	}
	
	private void reloadTableDiplexer() {
		setArrDiplexer(anagService.getAllAnagDiplexer());
	}
	
	public AnagService getAnagService() {
		return anagService;
	}
	public void setAnagService(AnagService anagService) {
		this.anagService = anagService;
	}
	public List<TabAnagDiplexer> getArrDiplexer() {
		return arrDiplexer;
	}
	public void setArrDiplexer(List<TabAnagDiplexer> arrDiplexer) {
		this.arrDiplexer = arrDiplexer;
	}
	public TabAnagDiplexer getSelectedDiplexer() {
		return selectedDiplexer;
	}
	public void setSelectedDiplexer(TabAnagDiplexer selectedDiplexer) {
		this.selectedDiplexer = selectedDiplexer;
	}
	
	public TabAnagDiplexer getNewDiplexer(){
		return newDiplexer;
	}
	
	public void setNewDiplexer(TabAnagDiplexer newDiplexer){
		this.newDiplexer = newDiplexer;
	}
	
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
	
	public void updateAnagrafica(){
		TabSyncAnagrafica tabSyncAnagrafica = anagService.getSyncAnagByNameTable(Const.SYNC_DIPLEXER);
		Integer addNumeroVersione = tabSyncAnagrafica.getNumeVersione() + 1;
		tabSyncAnagrafica.setNumeVersione(addNumeroVersione);
		anagService.updateSyncAnagrafica(tabSyncAnagrafica);
	}
}

