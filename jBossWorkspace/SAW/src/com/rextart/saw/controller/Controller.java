package com.rextart.saw.controller;

public interface Controller {
	
	/*@PostConstruct*/
	public void init();
	
	public void openEdit();
	public void openNew();
	public void rimuovi();
	public void save();
	public void filter();
	public void filterClear();

}
