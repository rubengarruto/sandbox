package com.rextart.saw.openKM;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;

import com.openkm.sdk4j.OKMWebservices;
import com.openkm.sdk4j.OKMWebservicesFactory;
import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.bean.Folder;
import com.openkm.sdk4j.exception.AccessDeniedException;
import com.openkm.sdk4j.exception.AutomationException;
import com.openkm.sdk4j.exception.DatabaseException;
import com.openkm.sdk4j.exception.ExtensionException;
import com.openkm.sdk4j.exception.FileSizeExceededException;
import com.openkm.sdk4j.exception.ItemExistsException;
import com.openkm.sdk4j.exception.LockException;
import com.openkm.sdk4j.exception.PathNotFoundException;
import com.openkm.sdk4j.exception.RepositoryException;
import com.openkm.sdk4j.exception.UnknowException;
import com.openkm.sdk4j.exception.UnsupportedMimeTypeException;
import com.openkm.sdk4j.exception.UserQuotaExceededException;
import com.openkm.sdk4j.exception.VirusDetectedException;
import com.openkm.sdk4j.exception.WebserviceException;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.utility.Const;

@Stateless
public class ServiceRepositoryImpl implements ServiceRepository{

	
	String url = Const.OKM_URL;
    String user = Const.OKM_USER;
    String pass = Const.OKM_PASSWORD;
    
    private OKMWebservices okm = OKMWebservicesFactory.newInstance(url, user, pass);
	
    
    //Upload file per il servizio syncMedia di SincronizzazioneRS
    public Document uploadMediaSurvey(Integer codiSessione,String nomeFile,String autor,InputStream is) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
    	SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	
    	System.out.println("Entro servizio UPLOAD OpenKM: "+df2.format(new Date().getTime()));
    	
    	Folder folderSession = new Folder();
    	
    	boolean existFolderSession =okm.hasNode(Const.PATH_MEDIA_SURVEY+codiSessione);
    	if(!existFolderSession){
    		System.out.println("Creo cartella SESSIONE ");
    		
    		folderSession = okm.createFolderSimple(Const.PATH_MEDIA_SURVEY+codiSessione);
    		folderSession.setPath(Const.PATH_MEDIA_SURVEY+codiSessione);
		}else {
			System.out.println("Cartella SESSIONE gia' esiste");
			
			folderSession.setPath(Const.PATH_MEDIA_SURVEY+codiSessione);
    		}
    	
    	Document doc = new Document();

    	boolean existFile =okm.hasNode(folderSession.getPath()+"/"+nomeFile); 

    	if(existFile)
    		try {
    			System.out.println("Foto gia' esistente la cancello ");
    			
    			okm.deleteDocument(folderSession.getPath()+"/"+nomeFile);
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			doc=null;
    		}
    	doc.setPath(folderSession.getPath()+"/"+nomeFile);
    	try {
    		System.out.println("Creazione Foto ");
    		
    		okm.createDocument(doc, is);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			doc=null;
		}
    	
    	System.out.println("Fine servizio UPLOAD OpenKM: "+df2.format(new Date().getTime()));
    	
    	return doc;
    	
    	}
    
    public Document uploadDocumentiCollaudo(Integer codiSessione,String nomeFile,byte[] file) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
    	
    	
    	Folder folderCollaudo = new Folder();
    	
    	boolean existFolderCollaudo =okm.hasNode(Const.PATH_MEDIA_SURVEY+codiSessione);
    	if(!existFolderCollaudo){
    		System.out.println("Creo cartella SESSIONE ");
    		
    		folderCollaudo = okm.createFolderSimple(Const.PATH_MEDIA_SURVEY+codiSessione);
    		folderCollaudo.setPath(Const.PATH_MEDIA_SURVEY+codiSessione);
		}else {
			
			
			folderCollaudo.setPath(Const.PATH_MEDIA_SURVEY+codiSessione);
    		}
    	
    	Document doc = new Document();

    	boolean existFile =okm.hasNode(folderCollaudo.getPath()+"/"+nomeFile); 

    	if(existFile)
    		try {
    			okm.deleteDocument(folderCollaudo.getPath()+"/"+nomeFile);
    		} catch (LockException e) {
    			System.out.println("Errore in uploadDocumentiCollaudo ");
    			e.printStackTrace();
    			doc=null;
    		}
    	doc.setPath(folderCollaudo.getPath()+"/"+nomeFile);
    	try {
    		InputStream inputStream = new ByteArrayInputStream(file);

    		okm.createDocument(doc, inputStream);
		} catch (Exception e) {
			System.out.println("Errore in uploadDocumentiCollaudo ");
			e.printStackTrace();
			doc=null;
		}
    	
    	return doc;
    	
    	}
    
    public void createFolderDocumentDyn(Integer codiSessione) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{


    	Folder folderCollaudo = new Folder();

    	boolean existFolderCollaudo =okm.hasNode(Const.PATH_MEDIA_DYN+codiSessione);
    	if(!existFolderCollaudo){
    		System.out.println("Creo cartella SESSIONE ");

    		folderCollaudo = okm.createFolderSimple(Const.PATH_MEDIA_DYN+codiSessione);
    		folderCollaudo.setPath(Const.PATH_MEDIA_DYN+codiSessione);
    	}else {


    		folderCollaudo.setPath(Const.PATH_MEDIA_DYN+codiSessione);
    		try {
    			okm.deleteFolder(folderCollaudo.getPath());
    			folderCollaudo = okm.createFolderSimple(folderCollaudo.getPath());
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}

    }
    
public Document uploadDocumentDyn(Integer codiSessione,String nomeFile,byte[] file) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
    	
	Folder folderCollaudo = new Folder();
	folderCollaudo.setPath(Const.PATH_MEDIA_DYN+codiSessione);
	Document doc = new Document();
	doc.setPath(folderCollaudo.getPath()+"/"+nomeFile);
	try {
		InputStream inputStream = new ByteArrayInputStream(file);

		okm.createDocument(doc, inputStream);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		doc=null;
	}

	return doc;

}

//public Document uploadSchedaRischi(Integer codiCollaudo,String nomeFile,byte[] file) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
//	
//	Folder folderCollaudo = new Folder();
//	folderCollaudo.setPath(Const.PATH_MEDIA_DYN+codiCollaudo);
//	Document doc = new Document();
//	doc.setPath(folderCollaudo.getPath()+"/"+nomeFile);
//	try {
//		InputStream inputStream = new ByteArrayInputStream(file);
//
//		okm.createDocument(doc, inputStream);
//	} catch (Exception e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//		doc=null;
//	}
//
//	return doc;
//
//}


	@Override
	public InputStream download(TabRepositoryDocument rep) {
		InputStream is = null;
		try{
			Folder folder = new Folder();
			boolean existFile =okm.hasNode(rep.getDescPath());
			if(existFile)
			is=okm.getContent(rep.getDescPath());
			else return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return is;
	}
	
	@Override
	public InputStream downloadMedia(TabRepositoryMedia rep) {
		InputStream is = null;
		try{
			Folder folder = new Folder();
			boolean existFile =okm.hasNode(rep.getDescPath());
			if(existFile)
			is=okm.getContent(rep.getDescPath());
			else return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return is;
	}
	
	@Override
	public InputStream downloadDocumentiDyn(TabRepositorySchedaRischi rep) {
		InputStream is = null;
		try{
			Folder folder = new Folder();
			boolean existFile =okm.hasNode(rep.getDescPath());
			if(existFile)
			is=okm.getContent(rep.getDescPath());
			else return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return is;
	}
	
	public InputStream downloadDocumentiDynRS(String path) {
		InputStream is = null;
		try{
			Folder folder = new Folder();
			boolean existFile =okm.hasNode(path);
			if(existFile)
			is=okm.getContent(path);
			else return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return is;
	}
    
	
	//Upload file per il servizio syncMedia di SincronizzazioneRS
    public void deleteExistD1(Integer codiSessione,String nomeFile) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
    	SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	
    	System.out.println("Entro servizio UPLOAD OpenKM: "+df2.format(new Date().getTime()));
    	
    	Folder folderSession = new Folder();
    	
    	
			
			folderSession.setPath(Const.PATH_MEDIA_SURVEY+codiSessione);
    	
    	boolean existFile =okm.hasNode(folderSession.getPath()+"/"+nomeFile); 

    	if(existFile)
    		try {
    			System.out.println("D1 gia' esistente la cancello ");
    			
    			okm.deleteDocument(folderSession.getPath()+"/"+nomeFile);
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
    	
    	}
   
}

