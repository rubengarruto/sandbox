package com.rextart.saw.openKM;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.exception.AccessDeniedException;
import com.openkm.sdk4j.exception.AutomationException;
import com.openkm.sdk4j.exception.DatabaseException;
import com.openkm.sdk4j.exception.ExtensionException;
import com.openkm.sdk4j.exception.FileSizeExceededException;
import com.openkm.sdk4j.exception.ItemExistsException;
import com.openkm.sdk4j.exception.PathNotFoundException;
import com.openkm.sdk4j.exception.RepositoryException;
import com.openkm.sdk4j.exception.UnknowException;
import com.openkm.sdk4j.exception.UnsupportedMimeTypeException;
import com.openkm.sdk4j.exception.UserQuotaExceededException;
import com.openkm.sdk4j.exception.VirusDetectedException;
import com.openkm.sdk4j.exception.WebserviceException;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class OpenKmService {

	@Autowired
	ServiceRepositoryImpl repService;
	
	public Document uploadSync(Integer codiSessione,String nomeFile,String autor,InputStream is)throws Exception {
		return repService.uploadMediaSurvey(codiSessione, nomeFile, autor, is);
	}
	
	public Document uploadDocumentiCollaudo(Integer codiSessione,String nomeFile, byte[] file)throws Exception {
		return repService.uploadDocumentiCollaudo(codiSessione, nomeFile,file);
	}
	
	public Document uploadDocumentDyn(Integer codiSessione,String nomeFile, byte[] file)throws Exception {
		return repService.uploadDocumentDyn(codiSessione, nomeFile,file);
	}
	
    public void createFolderDocumentDyn(Integer codiSessione) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException{
    			repService.createFolderDocumentDyn(codiSessione);
    }
    
    public void deleteExistD1(Integer codiSessione,String nomeFile)throws Exception {
		repService.deleteExistD1(codiSessione, nomeFile);
	}
//	public Document uploadSchedaRischi(Integer codiCollaudo,String nomeFile, byte[] file)throws Exception {
//		return repService.uploadSchedaRischi(codiCollaudo, nomeFile,file);
//	}
}
