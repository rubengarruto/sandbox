package com.rextart.saw.openKM;

import java.io.IOException;
import java.io.InputStream;

import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.exception.AccessDeniedException;
import com.openkm.sdk4j.exception.AutomationException;
import com.openkm.sdk4j.exception.DatabaseException;
import com.openkm.sdk4j.exception.ExtensionException;
import com.openkm.sdk4j.exception.FileSizeExceededException;
import com.openkm.sdk4j.exception.ItemExistsException;
import com.openkm.sdk4j.exception.PathNotFoundException;
import com.openkm.sdk4j.exception.RepositoryException;
import com.openkm.sdk4j.exception.UnknowException;
import com.openkm.sdk4j.exception.UnsupportedMimeTypeException;
import com.openkm.sdk4j.exception.UserQuotaExceededException;
import com.openkm.sdk4j.exception.VirusDetectedException;
import com.openkm.sdk4j.exception.WebserviceException;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;


public interface ServiceRepository {
	public Document uploadMediaSurvey(Integer codiSessione,String nomeFile,String autor,InputStream is) throws IOException, UnsupportedMimeTypeException, FileSizeExceededException, UserQuotaExceededException, VirusDetectedException, ItemExistsException, PathNotFoundException, AccessDeniedException, RepositoryException, DatabaseException, ExtensionException, AutomationException, UnknowException, WebserviceException;
	public InputStream download(TabRepositoryDocument rep);
	public InputStream downloadMedia(TabRepositoryMedia rep);
	public InputStream downloadDocumentiDyn(TabRepositorySchedaRischi rep);
}
