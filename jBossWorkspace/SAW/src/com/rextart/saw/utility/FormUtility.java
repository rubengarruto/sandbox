package com.rextart.saw.utility;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.entity.ViwElemCarattWOrder;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.rs.utility.Constants;

public class FormUtility 
{	
	public static String getDescLabelCarat(ViwElemCarattWOrder viwElemCarattWOrder, int index)
	{
		switch(index)
		{
		case 1:
		{
			return viwElemCarattWOrder.getDescLabelCarat1();
		}
		case 2:
		{
			return viwElemCarattWOrder.getDescLabelCarat2();
		}
		case 3:
		{
			return viwElemCarattWOrder.getDescLabelCarat3();
		}
		case 4:
		{
			return viwElemCarattWOrder.getDescLabelCarat4();
		}
		case 5:
		{
			return viwElemCarattWOrder.getDescLabelCarat5();
		}
		default:
		{
			return null;
		}
		}
	}

	public static boolean hasAnagLov(ViwElemCarattWOrder caratteristica, Integer flagIndex)
	{
		switch(flagIndex)
		{
			case 1: 
			{
				return caratteristica.getFlagLov1() == Const.TRUE;
			}
			case 2:
			{
				return caratteristica.getFlagLov2() == Const.TRUE;
			}
			case 3:
			{
				return caratteristica.getFlagLov3() == Const.TRUE;
			}
			case 4:
			{
				return caratteristica.getFlagLov4() == Const.TRUE;
			}
			default:
			{
				return caratteristica.getFlagLov5() == Const.TRUE;
			}
		}
	}

	public static String createId(Object ... k)
	{
		String outKey = "";
		for(int i=0; i<k.length; i++)
		{
			outKey += k[i];
			if(i+1 != k.length) outKey += "_";
		}

		return outKey;
	}

	public static String[] getIds(String id)
	{
		if(id == null) return null;
		return id.split("_");
	}

	public static boolean controlloStatoVerbale(Integer codiOggetto, ViwSessioni selectedSessione){
		switch(codiOggetto){
		case Constants.OGGETTO_A:
			if(selectedSessione.getCodiStatoA()==null || selectedSessione.getCodiStatoA().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;
		case Constants.OGGETTO_B:
			if(selectedSessione.getCodiStatoB()==null || selectedSessione.getCodiStatoB().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;
		case Constants.OGGETTO_C:
			if(selectedSessione.getCodiStatoC()==null || selectedSessione.getCodiStatoC().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;

		case Constants.OGGETTO_D:
			if(selectedSessione.getCodiStatoD2()==null || selectedSessione.getCodiStatoD2().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;

		case Constants.OGGETTO_NOKIA:
			if(selectedSessione.getCodiStatoNokia()==null || selectedSessione.getCodiStatoNokia().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;

		case Constants.OGGETTO_ERICSSON:
			if(selectedSessione.getCodiStatoEricsson()==null || selectedSessione.getCodiStatoEricsson().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;

		case Constants.OGGETTO_HUAWEI:
			if(selectedSessione.getCodiStatoHuawei()==null || selectedSessione.getCodiStatoHuawei().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;

		case Constants.OGGETTO_E:
			if(selectedSessione.getCodiStatoE()==null || selectedSessione.getCodiStatoE().equals(Const.VERBALE_NON_LAVORATO)){
				return true;
			}
			return false;
		default: return false;
		}
	}

	public static boolean isUnlocked(Integer codiOggetto, ViwSessioni selectedSessione){
		switch(codiOggetto){
		case Constants.OGGETTO_A:
			if(!selectedSessione.getFlgLockA()){
				return true;
			}
			return false;
		case Constants.OGGETTO_B:
			if(!selectedSessione.getFlgLockB()){
				return true;
			}
			return false;
		case Constants.OGGETTO_C:
			if(!selectedSessione.getFlgLockC()){
				return true;
			}
			return false;

		case Constants.OGGETTO_D:
			if(!selectedSessione.getFlgLockD()){
				return true;
			}
			return false;

		case Constants.OGGETTO_NOKIA:
			if(!selectedSessione.getFlgLockNsn()){
				return true;
			}
			return false;

		case Constants.OGGETTO_ERICSSON:
			if(!selectedSessione.getFlgLockEricsson()){
				return true;
			}
			return false;

		case Constants.OGGETTO_HUAWEI:
			if(!selectedSessione.getFlgLockHuawei()){
				return true;
			}
			return false;

		case Constants.OGGETTO_E:
			if(!selectedSessione.getFlgLockE()){
				return true;
			}
			return false;
		default: return false;
		}
	}

	public static boolean verificaUtenti(Integer codiOggetto, CurrentUser currentUser)
	{
		switch(codiOggetto){
		case Const.ALLEGATO_A: case Const.ALLEGATO_ERICSSON: case Const.ALLEGATO_HUAWEI: case Const.ALLEGATO_NOKIA:
			switch(currentUser.getCodiStruttura()){
			case Const.N_NOA: case Const.AOU: case Const.Ditte_MOI: case Const.VENDOR: return true;
			default: return false;
			}
		case Const.ALLEGATO_E: 
			switch(currentUser.getCodiStruttura()){
			case Const.AOU: return true;
			default: return false;
			}
		case Const.ALLEGATO_B: case Const.ALLEGATO_D: case Const.ALLEGATO_C: 
			switch(currentUser.getCodiStruttura()){
			case Const.AOU: case Const.Ditte_MOI: case Const.VENDOR: return true;
			default: return false;
			}
		}
		return false;
	}

	public static String getPageNameByCodiOggetto(Integer codiOggetto)
	{
		switch(codiOggetto)
		{
		case Const.ALLEGATO_A: return "formAllegatoA.xhtml";
		case Const.ALLEGATO_B: return "formAllegatoB.xhtml";
		case Const.ALLEGATO_C: return "formAllegatoC.xhtml";
		case Const.ALLEGATO_D: return "formAllegatoD2.xhtml";
		case Const.ALLEGATO_E: return "formAllegatoE.xhtml";
		case Const.ALLEGATO_HUAWEI: return "formAllegatoHauwei.xhtml";
		case Const.ALLEGATO_ERICSSON: return "formAllegatoEricsson.xhtml";
		case Const.ALLEGATO_NOKIA: return "formAllegatoNokia.xhtml";
		default: return null;
		}
	}
}