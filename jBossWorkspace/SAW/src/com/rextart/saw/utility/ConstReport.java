package com.rextart.saw.utility;



public class ConstReport {
	
	//Caratteristiche casi particolari
	public static final int	TestFlussiE1 = 577;
	public static final int	TensionePrimariaDC = 749;
	public static final int	TensionePrimariaAC = 750;
	public static final int	TensioneSecondariaDC = 752;
	public static final int	TensioneSecondariaAC = 753;
	public static final int TestFlussiNokia = 12;
	public static final int CompetenzaAllarmiNokia2G = 19;
	public static final int ConfigurazioneNokia2G =20;
	public static final int CompetenzaAllarmiNokia3G = 79;
	public static final int ConfigurazioneNokia3G =80;
	public static final int ModalitaGestioneRetNokia = 144;
	public static final int RetGestitoDaNokia = 145;
	public static final int SistemaProveDiServizioNokia = 157;
	public static final int[] ValoriMisuratiWIPMNokia = {209,210,212,213};
	public static final int TestFlussiHuawei = 369;
	public static final int TipoTelaioAllarmiEsterniHuawei = 376;
	public static final int CompetenzaAllarmiEsterniHuawei = 375;
	public static final int ModalitaGestioneRetHuawei = 509;
	public static final int RetGestitoDaHuawei = 510;
	public static final int SistemaProveDiServizioHuawei = 517;
	public static final int[] ValoriMisuratiWIPMHuawei = {771,772,774,775};
	public static final int	InfoCelleB = 782;
	public static final int	CondivisioneCalataB = 322;
	public static final int ElementoDettagliTecnologieInstallate=130;
	public static final int Via0=1;
	public static final int TipoTelaioAllarmiEsterniEricsson = 584;
	public static final int CompetenzaAllarmiEsterniEricsson = 583;
	public static final int SistemaProveDiServizioEricsson = 698;
	public static final int retGestitoDaEricsson=691;
	public static final int TipoMisuraRadB = 1032;
	public static final int StrumentoUsatoRadB = 1033;
	public static final int TipoMisuraRadD = 1035;
	public static final int StrumentoUsatoRadD = 1036;





	
	public static final int InfoCelleRadiantiE = 808;
	
	//TIPOLOGIE OGGETTO
	public static final int VerificheDiInstallazioneOpenAccessNokia = 7;
	public static final int VerificheFunzionaliApparatoCommissioningNokia = 9;
	public static final int ProveFunzionaliDiEsercizioNokia = 10;
	public static final int ProveFunzionaliDiServizioNokia = 11;
	public static final int VerificheDiInstallazioneWIPMNokia = 6;
	public static final int VerificheDiInstallazioneOpenAccessHuawei = 18;
	public static final int VerificheFunzionaliApparatoCommissioningHuawei = 19;
	public static final int ProveFunzionaliDiEsercizioHuawei = 20;
	public static final int ProveFunzionaliDiServizioHuawei = 21;
	public static final int VerificheDiInstallazioneWIPMHuawei = 22;
	public static final int DatiGeneraliRadiantiA = 24;
	public static final int DatiGeneraliRadiantiB = 25;
	public static final int DatiGeneraliRadiantiD = 26;
	public static final int DatiGeneraliTabletEricsson = 27;
	public static final int DatiGeneraliTabletHuawei = 28;
	public static final int DatiGeneraliTabletHuaweiWipm = 29;
	public static final int DatiGeneraliTabletEricssonWIPM = 30;
	public static final int DatiGeneraliTabletNokia = 31;
	public static final int DatiGeneraliTabletNokiaWipm = 32;








	
	//ELEMENTI
	public static final int AllarmiEsterniGestitiDa2G = 37;
	public static final int AllarmiEsterniGestitiDa3G = 38;
	public static final int MisureDiCaratterizzazioneDiApparato = 39;
	public static final int VerificaMHANokia = 40;
	public static final int VerificaRETNokia = 41;
	public static final int ChiusuraCommissioningNokia = 42;
	public static final int NotificaAllarmiNokia = 43;
	public static final int ContinuitaFlussiNokia = 44;
	public static final int ProveDiServizioNokia = 45;
	public static final int AllarmiEsterniHuawei = 80;
	public static final int MisureDiCaratterizzazioneDiApparatoHuawei = 81;
	public static final int VerificaTMAHuawei = 82;
	public static final int VerificaRETHuawei = 83;
	public static final int NotificaAllarmiHuawei = 84;
	public static final int ContinuitaFlussiHuawei = 85;
	public static final int ProveDiServizioHuawei = 86;
	public static final int StrumentazioneUsataAllineamenoAntenneRadA = 140;
	public static final int StrumentazioneUsataAllineamenoAntenneRadB = 141;
	public static final int StrumentazioneUsataAllineamenoAntenneRadD = 142;
	
	
	
	
	
	
    
	
}
