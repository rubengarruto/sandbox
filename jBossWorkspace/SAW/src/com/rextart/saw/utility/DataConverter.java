package com.rextart.saw.utility;

import java.io.Serializable;

import com.rextart.saw.bean.CollaudoDatiGeneraliBean;
import com.rextart.saw.entity.TabSessioni;

public class DataConverter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static TabSessioni getTabSessioni(TabSessioni tabSessioni, CollaudoDatiGeneraliBean collaudoBean) {
		tabSessioni.setClliSito(collaudoBean.getClliSito());
		tabSessioni.setCodiAreaComp(collaudoBean.getCodiAreaComp());
		tabSessioni.setCodiSistema(collaudoBean.getCodiSistema());
		tabSessioni.setCodiBanda(collaudoBean.getCodiBanda());
		tabSessioni.setCodiCollaudo(collaudoBean.getCodiCollaudo());
		tabSessioni.setCodiDettaglioColl(collaudoBean.getCodiDettaglioColl());
		tabSessioni.setCodiFinalitaColl(collaudoBean.getCodiFinalitaColl());
		tabSessioni.setCodiSessione(collaudoBean.getCodiSessione());
		tabSessioni.setCodiTipoImpianto(collaudoBean.getCodiTipoImpianto());
		tabSessioni.setCodiUnitaTerr(collaudoBean.getCodiUnitaTerr());
		tabSessioni.setCodiceDbr(collaudoBean.getCodiceDbr());
		tabSessioni.setDataSchedaPrRaEs(collaudoBean.getDataSchedaPrRaEs());
		tabSessioni.setDescCodice(collaudoBean.getDescCodice());
		tabSessioni.setDescNomeSito(collaudoBean.getDescNomeSito());
		tabSessioni.setDescNote(collaudoBean.getDescOperTim());
		tabSessioni.setDescOperTim(collaudoBean.getDescOperTim());
		tabSessioni.setDescProgettoRadio(collaudoBean.getDescProgettoRadio());
		tabSessioni.setDescProvincia(collaudoBean.getDescProvincia());
		tabSessioni.setDescRegione(collaudoBean.getDescRegione());
		tabSessioni.setFlgAllegatoA(collaudoBean.getFlgAllegatoA());
		tabSessioni.setFlgAllegatoB(collaudoBean.getFlgAllegatoB());
		tabSessioni.setFlgAllegatoC(collaudoBean.getFlgAllegatoC());
		tabSessioni.setFlgAllegatoD(collaudoBean.getFlgAllegatoD());
		tabSessioni.setFlgAllegatoD1(collaudoBean.getFlgAllegatoD1());
		tabSessioni.setFlgAllegatoE(collaudoBean.getFlgAllegatoE());
		tabSessioni.setFlgApparatoEricsson(collaudoBean.getFlgApparatoEricsson()); 
		tabSessioni.setFlgApparatoHuawei(collaudoBean.getFlgApparatoHuawei());
		tabSessioni.setFlgApparatoNsn(collaudoBean.getFlgApparatoNsn());
		tabSessioni.setDescIndirizzo(collaudoBean.getDescIndirizzo());
		tabSessioni.setDescLatitudine(collaudoBean.getLatitudine());
		tabSessioni.setDescLongitudine(collaudoBean.getLongitudine());
		tabSessioni.setTimeAcquisizione(collaudoBean.getTimeAcquisizione());
		tabSessioni.setTimePrevistoCollaudo(collaudoBean.getTimePrevistoCollaudo());
		return tabSessioni;
	}


}
