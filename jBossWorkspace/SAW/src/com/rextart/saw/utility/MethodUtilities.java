package com.rextart.saw.utility;

import java.util.List;

public class MethodUtilities {

	public static String creaAccentate(String stringa){
		String s = ""; 
		String aposString = new String("\'");
		try {
			s =  stringa.replace("&agrave;", "�").replace("&aacute;","�").replace("&egrave;", "�").replace("&igrave;", "�").replace("&uacute;", "�").replace("&ograve;", "�").replaceAll("&ugrave;", "�")
					.replace("&deg;", "�").replace("&oacute;", "�").replace("&eacute;", "�").replace( "&iacute;","�").replaceAll("&apos;", aposString).replace("&#8364", "�").replace("&#163", "�");
		} catch (NullPointerException e) {}
		return s; 
	}
	
	
	public static String sostituisciAccentate(String stringa){
		String s = "";
		String aposString = new String("\'");
		try {
			s = stringa.replace("�", "&agrave;").replace("�", "&aacute;").replace("�", "&egrave;").replace("�", "&igrave;").replace("�", "&iacute;")
					.replace("�", "&ograve;").replace("�", "&oacute;").replace("�", "&ugrave;").replace("�", "&agrave;").replace("�", "&egrave;").replace("�","&uacute;")
					.replace("�", "&igrave;").replace("�", "&ograve;").replace("�", "&ugrave;").replace("�", "&deg;").replace("�", "&eacute;").replaceAll(aposString, "&apos;")
					.replace("�", "&#8364;").replace("�", "&#163;");
		} catch (NullPointerException e) {}
		return s; 
	}
	
	public static String getStringCodiCliFromList(List<Integer> listCodi){
		String cliente = "";
		for (int i = 0; i < listCodi.size(); i++) {
			cliente=cliente+"cl="+ listCodi.get(i);;
			if(i != listCodi.size()-1)
				cliente += "&";
		}
		return cliente;
	}
	
	public static String getStringCodiReteFromList(List<Integer> listCodi){
		String rete = "";
		for (int i = 0; i < listCodi.size(); i++) {
			rete=rete+"r="+ listCodi.get(i);;
			if(i != listCodi.size()-1)
				rete += "&";
		}
		return rete;
	}
	
	public static String getStringCodiCoFromList(List<Integer> listCodi){
		String comprensorio = "";
		for (int i = 0; i < listCodi.size(); i++) {
			comprensorio=comprensorio+"co="+ listCodi.get(i);;
			if(i != listCodi.size()-1)
				comprensorio += "&";
		}
		return comprensorio;
	}
	
	public static String getStringCodiZonaFromList(List<Integer> listCodi){
		String zona = "";
		for (int i = 0; i < listCodi.size(); i++) {
			zona=zona+"z="+ listCodi.get(i);;
			if(i != listCodi.size()-1)
				zona += "&";
		}
		return zona;
	}


	/**
	 * Questi metodi ritornano il nome del documento corrispondente, che segue la nomenclatura da specifica
	 * @return
	 */
	public static Integer getTipoSchedaProgetto() {
		return 1;
	}

	public static Integer getTipoCheckList() {
		return 2;
	}

	public static Integer getTipoSchedaRadioEsecutiva() {
		return 3;
	}
	
// Ritorna una stringa con le sole iniziali maiuscole per ogni parola
	public static String capitalizeWords(String toCapitalize)
	{
		if(toCapitalize == null) return null;
		toCapitalize = toCapitalize.toLowerCase();
		String capitalized = "";
		for(String word : toCapitalize.split(" "))
		{
//			TODO gestire casi in cui la parola inizia con un carattere speciale (ex. parentesi)
			capitalized += " " + Character.toUpperCase(word.charAt(0));
			if(word.length() > 1) capitalized += word.length() > 1 ? word.substring(1) : "";
		}
		
		return capitalized.trim();
	}
	
	public static boolean isNullOrEmpty(String str)
	{
		return str == null || str.isEmpty();
	}
}
