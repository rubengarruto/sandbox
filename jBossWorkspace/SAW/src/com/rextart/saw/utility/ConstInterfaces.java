package com.rextart.saw.utility;

public interface ConstInterfaces {
	
	/*
	 * Costanti per Ruolo Struttura AreaCompetenza
	 * 
	 * */
	
	public static final String DESC_CODIROLE_SUPERVISOR="SUPERVISOR";
	public static final String DESC_CODIROLE_OPRETATIVO="OPERATIVO";
	public static final String DESC_CODIROLE_ADMIN="AMMINISTRATORE";
	public static final String DESC_CODIROLE_ADMIN_OPER="AMMINISTRATORE_OPERATIVO";
	public static final String DESC_CODIROLE_VENDOR="VENDOR";
	
	public static final int SUPERVISOR=1;
	public static final int RPA=2;
	public static final int GGU=3;
	public static final int DG=4;
	public static final int DG_OP=5;
	public static final int Referente_Territoriale_OP=6;
	public static final int Referente_Territoriale=7;
	public static final int Coordinatore_Operativo=8;
	public static final int OPERATORE=9;
	public static final int UTENTE_VENDOR=10;
	
	
	public static final int DITTE_MOI=7;
	public static final int NAZIONALE=1;
	
	public static final int STRUTTURA_AOA=4;
	public static final int STRUTTURA_AOL=5;
	public static final int STRUTTURA_AOU=6;
	 public static final int STRUTTURA_VENDOR=8;
	
	public Integer getSupervisor();
	public Integer getRpa();
	public Integer getDg();
	public Integer getGgu();
	public Integer getDgOp();
	public Integer getRefTerOp();
	public Integer getRefTer();
	public Integer getCorOp();
	public Integer getOperatore();
	public Integer getUserVendor();
	
	public Integer getStrutturaAOL();
	public Integer getStrutturaAOU();
	public Integer getStrutturaVendor();
	
	public String getDescCodiRoleSupervisor();
	public String getDescCodiRoleAdmin();
	public String getDescCodiRoleOperativo();
	public String getDescCodiRoleAdminOper();
	public String getDescCodiRoleVendor();

	/*
	 * Costanti per Utenti Impresa
	 * 
	 * */
	public static String DESC_UTENTE_IMPRESA ="MOI";
	
	/*
	 * Costanti stato Utenti
	 * 
	 * */
	public static final int UTENTE_CREATO =0;
	public static final int UTENTE_ABILITATO =1;
	public static final int UTENTE_SOSPESO =2;
	public static final int UTENTE_CESSATO =3;
	
	public Integer getUserCreato();
	public Integer getUserAbilitato();
	public Integer getUserSospeso();
	public Integer getUserCessato();
	
	public static final int ACQUISITO=1;
	public static final int DISPONIBILE=2;
    public static final int IN_LAVORAZIONE=3;
    public static final int ANNULLATO=4;
    public static final int CHIUSO=5;
    public Integer getAcquisito();
	public Integer getDisponibile();
	public Integer getInLavorazione();
	public Integer getChiuso();
	
	

	

}
