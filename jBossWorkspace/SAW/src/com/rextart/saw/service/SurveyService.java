package com.rextart.saw.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.rextart.saw.dao.ReportDao;
import com.rextart.saw.dao.SessioneDaoImpl;
import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.dao.SurveyDao;
import com.rextart.saw.dao.UserDaoImpl;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwSbloccoSezioni;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.utility.Const;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class SurveyService {
	
	 @Autowired
	 private SurveyDao surveyDao;
	 
	 @Autowired
	 private ReportDao reportDao;
	 
	 @Autowired
	 private SincronizzazioneDao sincronizzazioneDao;
	 
	 @Autowired
	 private SessioneDaoImpl sessioneDao;
	 
	 @Autowired
	 private UserDaoImpl userdao;

	 @Autowired
	 private SessioniService sessioniService;
	 
	 @Autowired
	 ApplicationMailer mailer;

	 @Autowired
	 MessageSource messages;
	 
	 @Autowired
	 private VelocityEngine velocityEngine;
	 
	 public List<TabSurvey> getListSurveyBySessione(List<Integer> listCodiSessione){
		 
		 if(listCodiSessione != null && listCodiSessione.size() > 0){
			 List<TabSurvey> listSurvey = surveyDao.getListSurveyBySessione(listCodiSessione);
			 ArrayList<TabSurvey> listSurveyToDelete = new ArrayList<>();
			 for(TabSurvey tabSurvey : listSurvey){
				 
				 if(surveyDao.isExistSurveyEsito(tabSurvey.getCodiSurvey())){
					 tabSurvey.setFlagEsito(true);
				 }
				 
				 if(tabSurvey.getCodiTipoOggetto()==Constants.OGGETTO_TIPO_CODI_D1_REMOTO){
					 listSurveyToDelete.add(tabSurvey);
				 }
			 }
			 listSurvey.removeAll(listSurveyToDelete);
			 return listSurvey;
		 }
		 	return new ArrayList<TabSurvey>();
	 }
	 
	 public List<TabSurveyDettaglio> getListDettagliBySurvey(List<Integer> listCodiSurvey){
		 return surveyDao.getListDettagliBySurvey(listCodiSurvey);
	 }
	 
	 public List<TabNoteCaratteristiche> getListNoteCaratt(List<Integer> listCodiSurvey){
		return surveyDao.getListNoteCaratt(listCodiSurvey);
	 }
	 
	 public List<TabRisultatoSurvey> getListRisultatiSurvey(List<Integer> listCodiSurvey){
		 return surveyDao.getListRisultatiSurvey(listCodiSurvey);
	 }
	 
	 public void deleteSurveyBySessione(Integer codiSessione){
		 surveyDao.deleteSurveyBySessione(codiSessione);
	 }
	 
	 public void saveSurveyForD1(TabRisultatoSurvey tabRisultatoSurvey, Integer codiSessione, Integer codiTipoOggetto){
		 surveyDao.saveSurveyForD1(tabRisultatoSurvey, codiSessione, codiTipoOggetto);
		 
		
	 }
	 
	 public void invioMailArchiviatoInD1(Integer codiSessione){
		 try{
		 ViwSessioni selectedSessione = sessioneDao.findViwSessioni(codiSessione);
			// In To tutti gli associati alla sessione
			List<ViwUtenti> listViwUtenti = userdao
					.getUtentiAssociatiByCodiSessione(codiSessione);
			List<String> userTo = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : listViwUtenti) {
				if(listUtentiFiltered.getFlgCessSosp()==false && listUtentiFiltered.getFlgReceiveEmail()==true){
					userTo.add(listUtentiFiltered.getDescEmail());
				}
			}

			// In Cc tutti i referenti territoriali e coordinatori relativi
			// alla stessa area di competenza della sessione
//			listViwUtenti = userdao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
			TabRegioni regioneByName = sessioneDao.getIdRegioneByNome(selectedSessione.getDescRegione());
			List<Integer> listCodiUtenti = userdao.getUtentiByRegione(regioneByName.getIdRegione());		
			List<ViwUtenti> utentiInCC = userdao.getUserRefTerAndCordByRegione(listCodiUtenti);

			List<String> userCc = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : utentiInCC) {
				if(listUtentiFiltered.getFlgReceiveEmail()==true){
					userCc.add(listUtentiFiltered.getDescEmail());
				}
			}
			
			List<ViwVerbale> verbali = sessioniService.getListVerbaliByCodiSessione(codiSessione);
			Integer esitoSessione = 1;
			for (ViwVerbale viwVerbale : verbali) {
				if(viwVerbale.getCodiStatoVerbale()==3) {
					esitoSessione = 3;
					break;
				}
				if(viwVerbale.getCodiStatoVerbale()==2) {
					esitoSessione=2;
				}
			}
			
			
			String esitoP;
			String esitoPcr;
			String esitoN;

			switch (esitoSessione) {
			case 1:
				esitoP = Const.ESITO_POSITIVO;
				esitoPcr = "";
				esitoN = "";
				break;
			case 2:
				esitoPcr = Const.ESITO_POSITIVO_CON_RISERVA;
				esitoP = "";
				esitoN = "";
				break;
			case 3:
				esitoN = Const.ESITO_NEGATIVO;
				esitoP = "";
				esitoPcr = "";
				break;
			default:
				esitoP = "";
				esitoPcr = "";
				esitoN = "";
				break;
			}
			
			if (userTo != null && userTo.size() > 0) {
				String object = messages.getMessage("sessioneArchiviataObj",
						new Object[] { selectedSessione.getDescCodice().toUpperCase()}, Const.locale);
				String body = messages
						.getMessage("sessioneArchiviataBody",
								new Object[] { selectedSessione.getCodiCollaudo(), selectedSessione.getDescCodice().toUpperCase(),
										selectedSessione.getDescNomeSito(),  esitoP, esitoPcr, esitoN},
								Const.locale);
				String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
						mailer.templateMail(body));
				mailer.sendMail(userTo, userCc, object, template);
				}
			} catch (Exception e) {}
	 }

	 
	public List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessione(Integer codiSessione) {
		
		List<ViwSbloccoSezioni> listViws= surveyDao.getListViwSbloccoSezioneByCodiSessione(codiSessione);
		
		//mi prendo solo uno per survey
		List<Integer> codiSurveyList = new ArrayList<>();
		
		List<ViwSbloccoSezioni> toReturn = new ArrayList<>();
		
		for (ViwSbloccoSezioni viwSbloccoSezioni : listViws) {
			
			if (!codiSurveyList.contains(viwSbloccoSezioni.getCodiSurvey())) {
				codiSurveyList.add(viwSbloccoSezioni.getCodiSurvey());
				toReturn.add(viwSbloccoSezioni);
			}
			
		}
		
		return toReturn;
		
	}
	
	private List<ViwSbloccoSezioni> getListViwSbloccoSezioneByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		
		List<ViwSbloccoSezioni> listViws= surveyDao.getListViwSbloccoSezioneByCodiSessioneByCodiOggetto(codiSessione,codiOggetto);
		
		//mi prendo solo uno per survey
		List<Integer> codiSurveyList = new ArrayList<>();
		
		List<ViwSbloccoSezioni> toReturn = new ArrayList<>();
		
		for (ViwSbloccoSezioni viwSbloccoSezioni : listViws) {
			
			if (!codiSurveyList.contains(viwSbloccoSezioni.getCodiSurvey())) {
				codiSurveyList.add(viwSbloccoSezioni.getCodiSurvey());
				toReturn.add(viwSbloccoSezioni);
			}
			
		}
		
		return toReturn;
	}
	
	

	public void deleteEsitoSurveyByCodiSurvey(Integer codiSurvey) {
		
		surveyDao.removeRisultatoSurveyByCodiSurvey(codiSurvey);
		
	}

	public void deleteRiferimentoFirmaByCodiSurvey(Integer codiSurvey) {
		
		surveyDao.removeRiferimentoFirmaByCodiSurvey(codiSurvey);
		
	}

	public void rimuoviEsitoSurvey(ViwSbloccoSezioni viw) {
		
		if(viw.getCodiTipoOggetto() == Const.ALLEGATO_D1){
			
		}
		deleteEsitoSurveyByCodiSurvey(viw.getCodiSurvey());
		deleteRiferimentoFirmaByCodiSurvey(viw.getCodiSurvey());
		if(viw.getCodiOggetto()==Const.ALLEGATO_ERICSSON || viw.getCodiOggetto()==Const.ALLEGATO_HUAWEI || viw.getCodiOggetto()==Const.ALLEGATO_NOKIA){
			TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(viw.getCodiOggetto(), viw.getCodiSessione());
			if(tabRelSessioneStatiVerbale!=null){
				tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_NON_LAVORATO);
			}
		}else{
			updateEsitoVerbale(viw.getCodiSessione(),viw.getCodiOggetto());
		}
	}

	public void deleteRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		
		surveyDao.removeRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(codiSessione,codiOggetto);
		
	}

	/**
	 * Aggiorno l'esito del verbale, guardando tutti i risultati dei survey di quell'oggetto (allegato,verbale)
	 * @param codiSessione
	 * @param codiOggetto
	 */
	private void updateEsitoVerbale(Integer codiSessione, Integer codiOggetto) {
	
		//rimuovo quelli che c'erano
		deleteRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(codiSessione,codiOggetto);

		//mi prendo tutti i risultati dei survey di quell'oggetto
		List<ViwSbloccoSezioni> listViwByOggetto = getListViwSbloccoSezioneByCodiSessioneAndCodiOggetto( codiSessione,  codiOggetto);
		
		//utilizzo la solita regola per stabilire l'esito, di default � positivo
		int esitoVerbale = Const.VERBALE_POSITIVO;
		
		for (ViwSbloccoSezioni viwSbloccoSezioni: listViwByOggetto) {
			
			//come trovo uno negativo l'esito del verbale diventa automaticamente negativo, quindi esco dal ciclo for
			if (viwSbloccoSezioni.getCodiValoreRisultatoSurvey()!=null && viwSbloccoSezioni.getCodiValoreRisultatoSurvey().equals(Const.VERBALE_NEGATIVO)) {
				esitoVerbale = Const.VERBALE_NEGATIVO;
				break;
			} 
			
			//come trovo uno positivo con riserva l'esito del verbale diventa automaticamente positivo con riserva, ma non esco dal ciclo for perch� potrebbe successivamente arrivare uno negativo
			if (viwSbloccoSezioni.getCodiValoreRisultatoSurvey()!=null && viwSbloccoSezioni.getCodiValoreRisultatoSurvey().equals(Const.VERBALE_POSITIVO_CON_RISERVA)) {
				esitoVerbale = Const.VERBALE_POSITIVO_CON_RISERVA;
				continue;
			} 
			
			//come trovo uno senza risultato e non sono positivo con riserva divento NON_LAVOTARTO, ma non esco dal ciclo for perch� potrebbe successivamente arrivare uno negativo o positivo con riserva
			if (viwSbloccoSezioni.getCodiValoreRisultatoSurvey()==null && esitoVerbale!=Const.VERBALE_POSITIVO_CON_RISERVA) {
				esitoVerbale = Const.VERBALE_NON_LAVORATO;
			} 
			
		}
		
		//salvo il risultato del verbale
		TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = new TabRelSessioneStatiVerbale();
		tabRelSessioneStatiVerbale.setCodiOggetto(codiOggetto);
		tabRelSessioneStatiVerbale.setCodiSessione(codiSessione);
		tabRelSessioneStatiVerbale.setCodiStatoVerbale(esitoVerbale);
		saveRelSessioneStatiVerbale(tabRelSessioneStatiVerbale);

		
		
	}

	public void saveRelSessioneStatiVerbale(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale) {
		surveyDao.saveRelSessioneStatiVerbale(tabRelSessioneStatiVerbale);
	}

	

	public List<TabRiferimentiFirma> getListRiferimentiFirma(List<Integer> listCodiSurvey){
		return surveyDao.getListRiferimentiFirmaByCodiSurvey(listCodiSurvey);
	 }
	
	// note tablet 
	public List<TabNote> getListNoteTablet(List<Integer> listNoteTablet) {
		return surveyDao.getListNoteTablet(listNoteTablet);
	}

	public List<TabSurvey> getListSurveyBySessioneOggetto(Integer codiSessione,Integer codiOggetto) {
		
			 List<TabSurvey> listSurvey = surveyDao.getListSurveyBySessioneByOggetto(codiSessione, codiOggetto);
			 ArrayList<TabSurvey> listSurveyToDelete = new ArrayList<>();
			 for(TabSurvey tabSurvey : listSurvey){
				 
				 if(surveyDao.isExistSurveyEsito(tabSurvey.getCodiSurvey())){
					 tabSurvey.setFlagEsito(true);
				 }
				 
				 if(tabSurvey.getCodiTipoOggetto()==Constants.OGGETTO_TIPO_CODI_D1_REMOTO){
					 listSurveyToDelete.add(tabSurvey);
				 }
			 }
			 listSurvey.removeAll(listSurveyToDelete);
			 return listSurvey;
		 
		 	
	}
	
	public TabSurvey getSurveyBySessioneTipoOggetto(Integer codiSessione, Integer codiTipoOggetto)
	{
		return reportDao.getSurveyBySessioneAndTipoOggetto(codiSessione, codiTipoOggetto);
	}
	
	public List<TabRelSessioneStatiVerbale> getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(Integer codiSessione, Integer codiOggetto){
		return surveyDao.getListByTabRelSessioneStatiVerbaleByCodiSessioneCodiOggetto(codiSessione, codiOggetto);
	}
	
	 
//	public  String getRisultatoAllegatoByCodiSessioneCodiOggetto(int codiSessione, int codiOggetto) {
//		try {
//
//			ArrayList<TabSurvey> surveyBySessioneOggetto = (ArrayList<TabSurvey>) surveyDao.getListSurveyBySessioneByOggetto(codiSessione,codiOggetto);
//
//			String risultato=Const.POSITIVO;
//			for (TabSurvey tabSurvey : surveyBySessioneOggetto) {
//
//				if (getRisultatoSurveyByCodiSurvey(tabSurvey.getCodiSurvey())!=null) {
//
//					switch (getRisultatoSurveyByCodiSurvey(tabSurvey.getCodiSurvey())) {
//
//					case Const.NEGATIVO_INT: 
//						return Const.NEGATIVO;
//					case Const.POSITIVO_CON_RISERVA_INT:
//						risultato = Const.POSITIVO_CON_RISERVA;
//						break;
//					case Const.POSITIVO_INT:
//						break;	
//
//					default: return Const.NON_APPLICABILE;
//
//
//					}
//
//				} else {
//					return Const.NON_APPLICABILE;
//				}
//
//			}
//
//			return risultato;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return Const.NON_APPLICABILE;
//		}
//
//
//	}
	
	public  String getRisultatoAllegatoByCodiSessioneCodiOggetto(int codiSessione, int codiOggetto) {
		try {

			TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(codiOggetto, codiSessione);

			if (tabRelSessioneStatiVerbale!=null) {

					switch (tabRelSessioneStatiVerbale.getCodiStatoVerbale()) {

					case Const.NEGATIVO_INT: 
						return Const.NEGATIVO;
					case Const.POSITIVO_CON_RISERVA_INT:
						return Const.POSITIVO_CON_RISERVA;
					case Const.POSITIVO_INT:
						return Const.POSITIVO;
					default: return Const.NON_APPLICABILE;


					}

				} else {
					return Const.NON_APPLICABILE;
				}
		} catch (Exception e) {
			e.printStackTrace();
			return Const.NON_APPLICABILE;
		}


	}

	public Integer getRisultatoSurveyByCodiSurvey(Integer codiSurvey) {
		
		ArrayList<Integer> listCodiSurvey = new ArrayList<>();
		listCodiSurvey.add(codiSurvey);
		List<TabRisultatoSurvey> listRisultatoSurvey = surveyDao.getListRisultatiSurvey(listCodiSurvey);
		if (listRisultatoSurvey!=null && listRisultatoSurvey.size()>0) {
			return listRisultatoSurvey.get(0).getCodiValoreRisultatoSurvey();
		}
		return null;
	}
	
	public List<TabRisultatoSurvey> getListRisultatoByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto){
		return surveyDao.getListRisultatoByCodiSessioneAndCodiOggetto(codiSessione, codiOggetto);
	}
	
	public TabAnagStatiSessione loadStatoChiuso(){
		return sessioneDao.loadStatoChiuso();
	}
	
	public TabSurveyDettaglio getSurveyDettaglioByFormKey(Integer codiSurvey, Integer codiElemento, Integer codiCaratteristica, String descLabel, Integer numeCella, Integer numeVia, Integer numeLivelloE)
	{
		return surveyDao.getSurveyDettaglioByFormKey(codiSurvey, codiElemento, codiCaratteristica, descLabel, numeCella, numeVia, numeLivelloE);
	}
	
	public boolean isValorizedSurveyByCodiCaratteristica(Integer codiSurvey, Integer codiCaratteristica)
	{
		return surveyDao.getValorizedSurveyDettaglioByCodiCaratteristica(codiSurvey, codiCaratteristica) != null;
	}
}
