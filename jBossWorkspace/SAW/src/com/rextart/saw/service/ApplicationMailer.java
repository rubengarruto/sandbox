package com.rextart.saw.service;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.rextart.saw.utility.Const;

@Service
@EnableAsync
public class ApplicationMailer {

	@Autowired
	private JavaMailSenderImpl mailSender;
	
	
	//TODO: inserire il path corretto per recuperare gli allegati.
	@Async
	public void sendMailAttachment(List<String> listAddressTo,List<String> listAddressCC, String subject,String body, String nameAttachment) throws MessagingException, UnsupportedEncodingException
	{
		if(Const.EMAIL_ON==true){ 
			String[] arrAdressTo = parseListToArray(listAddressTo);
			String[] arrAdressCC =parseListToArray(listAddressCC);
			MimeMessage httpMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(httpMessage, true); // use the true flag to indicate you need a multipart message
			helper.setFrom(new InternetAddress(Const.EMAIL_SYSTEM, Const.APP_NAME));
			helper.setTo(arrAdressTo);
			helper.setCc(arrAdressCC);
			helper.setSubject(subject);
			helper.setText(body,true); // use the true flag to indicate the text included is HTML
			httpMessage.setHeader("List-Unsubscribe", "mailto:support@rextart.com");
			FileSystemResource file = new FileSystemResource(new File("C:/Users/sara.scalzini/Desktop/bckDestop/"+nameAttachment)); //TODO:inserire path cartella dove reperire i file da inviare in allegato
			helper.addAttachment(nameAttachment, file);
			mailSender.send(httpMessage);
		}
	}
	
	@Async
	public void sendMail(List<String> listAddressTo, List<String> listAddressCC, String subject, String body) throws MessagingException, UnsupportedEncodingException {
		if(Const.EMAIL_ON==true){ 
			String[] arrAdressTo = parseListToArray(listAddressTo);
			String[] arrAdressCC =parseListToArray(listAddressCC);
			MimeMessage httpMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(httpMessage);
			helper.setFrom(new InternetAddress(Const.EMAIL_SYSTEM, Const.APP_NAME));
			helper.setTo(arrAdressTo);
			helper.setCc(arrAdressCC);
			helper.setSubject(subject);
			helper.setText(body,true); // use the true flag to indicate the text included is HTML
			httpMessage.setHeader("List-Unsubscribe", "mailto:support@rextart.com");
			mailSender.send(httpMessage);
		}
	}
	
	@Async
	public void sendMailDiSistema(List<String> listAddressBcc, String subject, String body) throws MessagingException, UnsupportedEncodingException {
		if(Const.EMAIL_ON==true){ 
			String[] arrAdressBcc = parseListToArray(listAddressBcc);
			MimeMessage httpMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(httpMessage);
			helper.setFrom(new InternetAddress(Const.EMAIL_SYSTEM, Const.MAIL_FROM));
			helper.setBcc(arrAdressBcc);
			helper.setSubject(subject);
			helper.setText(body,true); // use the true flag to indicate the text included is HTML
			httpMessage.setHeader("List-Unsubscribe", "mailto:support@rextart.com");
			mailSender.send(httpMessage);
		}
	}	
	
	@Async
	public void sendMailAttachmentByte(List<String> listAddressTo,List<String> listAddressCC, String subject,String body, byte[] attachment,String nomeFile) throws MessagingException, UnsupportedEncodingException
	{
		if(Const.EMAIL_ON==true){ 
			String[] arrAdressTo = parseListToArray(listAddressTo);
			String[] arrAdressCC =parseListToArray(listAddressCC);
			MimeMessage httpMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(httpMessage, true); // use the true flag to indicate you need a multipart message
			helper.setFrom(new InternetAddress(Const.EMAIL_SYSTEM, Const.APP_NAME));
			helper.setTo(arrAdressTo);
			helper.setCc(arrAdressCC);
			helper.setSubject(subject);
			helper.setText(body,true); // use the true flag to indicate the text included is HTML
			httpMessage.setHeader("List-Unsubscribe", "mailto:support@rextart.com");
            DataSource dataSource = new ByteArrayDataSource(attachment, "application/pdf");
            helper.addAttachment(nomeFile, dataSource);

			mailSender.send(httpMessage);
		}
	}
	
	
	private String[] parseListToArray(List<String> listAddress) {
		if(listAddress==null)
			return new String[0];
		String[] arrAdress = new String[listAddress.size()];
		for (int i = 0; i < listAddress.size(); i++) {
			arrAdress[i]=listAddress.get(i);
		}
		return arrAdress;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> templateMail(String body){
		Map model = new HashMap();
		model.put("body", body);
		return model;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> templateMail(String body, String lista_utenti_associati){
		Map model = new HashMap();
		model.put("body", body);
		model.put("lista_utenti_associati", lista_utenti_associati);
		return model;
	}
	
	
}
