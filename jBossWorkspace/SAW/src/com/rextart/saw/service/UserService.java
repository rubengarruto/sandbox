package com.rextart.saw.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.UserFilter;
import com.rextart.saw.dao.LogOperationDaoImpl;
import com.rextart.saw.dao.UserDaoImpl;
import com.rextart.saw.entity.TabAnagStatiUtente;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabRole;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwProfiloUtente;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwZoneRegioni;
import com.rextart.saw.utility.Const;



@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class UserService {
	
	 @Autowired
	 UserDaoImpl dbService;
	 
	 @Autowired
	 MessageSource messages;
	 
	 @Autowired
	 ApplicationMailer mailer;
	 
	 @Autowired
	 LogOperationDaoImpl operationLogger;
	 
	 @Autowired
	 private VelocityEngine velocityEngine;
	
	 public List<ViwUtenti> getAllUsers(){
		 return dbService.getAllUsers();
	 }
	 
	 public List<TabAreaCompetenza> getAllAreeComp(){
		 return dbService.getAllAreeComp();
	 }
	 
	 public List<TabRole> getAllRoles(){
		 return dbService.getAllRoles();
	 }
	 
	 public List<String> getAllDescAreaComp(){
		 return dbService.getAllDescAreaComp();
	 }
	 
	 public List<String> getAllDescRoles(){
		 return dbService.getAllDescRoles();
	 }
	 
	 public List<ViwProfiloUtente> getStruttureByRole(Integer codiRole){
		 return dbService.getStruttureByRole(codiRole);
	 }
	 
	 public List<TabStruttura> getAllStrutture(){
		 return dbService.getAllStrutture();
	 }
	 
	 public List<ViwUtenti> getFilterViwUser(UserFilter filter){
		 return dbService.getFilterViwUser(filter);
	 }
	 
	 public TabUtente getUtenteById(int codiUtente){
		 return dbService.getUtenteById(codiUtente);
	 }
	 
	 public List<TabZone> getTabZoneByAreaComp(Integer codiAreaComp){
		 return dbService.getTabZoneByAreaComp(codiAreaComp);
	 }
	 
	 public List<ViwAreaCompRegioni> getViwRegioniByAreaComp(Integer codiAreaComp){
		 return dbService.getViwRegioniByAreaComp(codiAreaComp);
	 }
	 
	 public List<TabGruppi> getTabGruppiByZona(Integer codiZona){
		 return dbService.getTabGruppiByZona(codiZona);
	 }
	 
	 public Object [] saveUser(TabUtente tabUtente,Integer impresa,CurrentUser currentUser,  List<Integer> selectedRegioni)throws Exception{
		 Object[] result = dbService.saveUser(tabUtente,impresa, selectedRegioni);
		 if((boolean) result[0]){
			 operationLogger.logOperation(Const.CREAZIONE_UTENTE,new Timestamp(System.currentTimeMillis()) , currentUser.getCodiUtente(),tabUtente.getDescUser());
			 if(tabUtente.isFlgReceiveEmail()){
				 try {
					 List<String> userTo=new ArrayList<String>();
					 userTo.add(tabUtente.getDescEmail());
					 String object = messages.getMessage("registrazioneObj", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn()}, Const.locale);
					 String body=Const.EMPTY_STRING;
					 if(tabUtente.isFlgImpresa()||tabUtente.getCodiVendor()!=Const.EMPTY_VALUE){
						 body =messages.getMessage("registrazioneBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser(), tabUtente.getDescPwd()}, Const.locale);
					 }else{
						 body =messages.getMessage("registrazioneTelecomBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser()}, Const.locale); 
					 }
					 String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					 mailer.sendMail(userTo, null, object, template);
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return result;
	 }
	 
	 public void updateUser(TabUtente tabUtente,Integer impresa,boolean cessato,CurrentUser currentUser, List<Integer> selectedRegioni)throws Exception{
			dbService.updateUser(tabUtente,impresa,cessato,selectedRegioni);
			operationLogger.logOperation(Const.MODIFICA_UTENTE,new Timestamp(System.currentTimeMillis()), currentUser.getCodiUtente(),tabUtente.getDescUser());
			if(cessato)
				operationLogger.logOperation(Const.CESSAZIONE_UTENTE,new Timestamp(System.currentTimeMillis()), currentUser.getCodiUtente(),tabUtente.getDescUser());
			if(tabUtente.isFlgReceiveEmail() &&  cessato){
				try {
					 List<String> userTo=new ArrayList<String>();
					 userTo.add(tabUtente.getDescEmail());
					 String object = messages.getMessage("cessazioneObj", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn()}, Const.locale);
					 String body=Const.EMPTY_STRING;
					 if(tabUtente.isFlgImpresa()||tabUtente.getCodiVendor()!=Const.EMPTY_VALUE){
						 body =messages.getMessage("cessazioneBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser(), tabUtente.getDescPwd()}, Const.locale);
					 }else{
						 body =messages.getMessage("cessazioneTelecomBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser()}, Const.locale); 
					 }
					 String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					 mailer.sendMail(userTo, null, object, template);
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			 }
	 }
	 
	 
	 public void abilitaUtente(TabUtente tabUtente,CurrentUser currentUser)throws Exception{
			dbService.abilitaUtente(tabUtente);
			operationLogger.logOperation(Const.ABILITAZIONE_UTENTE,new Timestamp(System.currentTimeMillis()), currentUser.getCodiUtente(),tabUtente.getDescUser());
			if(tabUtente.isFlgReceiveEmail()){
				try {
					 List<String> userTo=new ArrayList<String>();
					 userTo.add(tabUtente.getDescEmail());
					 String object = messages.getMessage("abilitazioneObj", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn()}, Const.locale);
					 String body=Const.EMPTY_STRING;
					 if(tabUtente.isFlgImpresa()||tabUtente.getCodiVendor()!=Const.EMPTY_VALUE){
						 body =messages.getMessage("abilitazioneBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser(), tabUtente.getDescPwd()}, Const.locale);
					 }else{
						 body =messages.getMessage("abilitazioneTelecomBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser()}, Const.locale); 
					 }
					 String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					 mailer.sendMail(userTo, null, object, template);
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			}
	 }
	 
	 public void disabilitaUtente(TabUtente tabUtente,CurrentUser currentUser)throws Exception{
			dbService.disabilitaUtente(tabUtente);
			operationLogger.logOperation(Const.SOSPENSIONE_UTENTE,new Timestamp(System.currentTimeMillis()), currentUser.getCodiUtente(),tabUtente.getDescUser());
			if(tabUtente.isFlgReceiveEmail()){
				try {
					 List<String> userTo=new ArrayList<String>();
					 userTo.add(tabUtente.getDescEmail());
					 String object = messages.getMessage("sospensioneObj", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn()}, Const.locale);
					 String body=Const.EMPTY_STRING;
					 if(tabUtente.isFlgImpresa()||tabUtente.getCodiVendor()!=Const.EMPTY_VALUE){
						body =messages.getMessage("sospensioneBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser(), tabUtente.getDescPwd()}, Const.locale);
					 }else{
						 body =messages.getMessage("sospensioneTelecomBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser()}, Const.locale); 
					 }
					 String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					 mailer.sendMail(userTo, null, object, template);
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			}
	 }
	 
	 public TabUtente authenticationUser(String user, String pass){
			return dbService.authenticationUser(user, pass);
	}
	 
	public TabUtente authenticationUserWithLDAP(String user, String pass){
		
			String url = Const.SAW_LDAP_URL;
			String base = Const.SAW_LDAP_BASE;
			String userDn = Const.SAW_LDAP_USER;
			String password = Const.SAW_LDAP_PWD;
			String filterOrganizzation=Const.SAW_LDAP_FILTER_ORGANIZZATION;
			String filterPerson=Const.SAW_LDAP_FILTER_PERSON;
			String filterUid=Const.SAW_LDAP_FILTER_UID;
		
			TabUtente utente =authenticationByUserName(user);
		
			if (utente!=null) {
				
				//se bisogna usare LDAP
				if(!utente.isFlgImpresa() && Const.SAW_LDAP_ON && !utente.isFlgIsAdmin() && utente.getCodiVendor()==Const.EMPTY_VALUE){
					boolean autenticate=false;
					try {
						LdapContextSource ctxSrc = new LdapContextSource();
						ctxSrc.setUrl(url);
						ctxSrc.setBase(base);
						ctxSrc.setUserDn(userDn);
						ctxSrc.setPassword(password);
						ctxSrc.afterPropertiesSet();
						LdapTemplate lt = new LdapTemplate(ctxSrc);

						AndFilter filter = new AndFilter();
						filter.and(new EqualsFilter(filterOrganizzation, filterPerson)).and(new EqualsFilter(filterUid, user));
						
						autenticate = lt.authenticate(DistinguishedName.EMPTY_PATH,filter.toString(),  pass);

						if (autenticate) {
							return utente;
						} else {
							return null;
						}
						
						} catch (Exception e) {
							e.printStackTrace();
							return null;
						}
					
				} else {//altrimenti
					
					return authenticationUser(user,pass);
				}
				
			}
			
			return null;
	}
	 
	 public List<TabImprese> getAllImprese(){
		 return dbService.getAllImprese();
	 }
	 
	 public TabUtente getUserById(Integer codiUtente){
		 return dbService.getUserById(codiUtente);
	 }
	 
	 public Object [] registerUser(TabUtente tabUtente,Integer impresa, List<Integer> selectedRegioni) throws Exception{
		 Object[] result = dbService.saveUser(tabUtente,impresa,selectedRegioni);
		 if((boolean) result[0]){
//			 operationLogger.logOperation(Const.CREAZIONE_UTENTE,new Timestamp(System.currentTimeMillis()) ,-1,tabUtente.getDescUser());
			 if(tabUtente.isFlgReceiveEmail()){
				 try {
					 List<String> userTo=new ArrayList<String>();
					 userTo.add(tabUtente.getDescEmail());
					 String object = messages.getMessage("registrazioneObj", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn()}, Const.locale);
					 String body=Const.EMPTY_STRING;
					 if(tabUtente.isFlgImpresa()||tabUtente.getCodiVendor()!=Const.EMPTY_VALUE){
						 body =messages.getMessage("registrazioneBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser(), tabUtente.getDescPwd()}, Const.locale);
					 }else{
						 body =messages.getMessage("registrazioneTelecomBody", new Object[] {tabUtente.getDescNome(), tabUtente.getDescCgn(), tabUtente.getDescUser()}, Const.locale); 
					 }
					 String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					 mailer.sendMail(userTo, null, object, template);
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return result;
	 }
	 
	 public List<TabAreaCompetenza> getAllAreeCompNotNazionale(){
		 return dbService.getAllAreeCompNotNazionale();
	 }
	 
	 public TabUtente authenticationByUserName(String user){
		 return dbService.authenticationByUserName(user);
	 }

	public ViwUtenti getViewByUtente(TabUtente tabUtente) {
		return dbService.getViwUtenteById(tabUtente.getCodiUtente());
	}
	
	public List<TabVendors> getAllVendor(){
		return dbService.getAllVendor();
	}
	
	public List<TabVendors> getVendorByCodi(Integer codiVendor){
		return dbService.getVendorByCodi(codiVendor);
	}
	
	public Boolean savePassword(String pwd,String descUser) throws Exception{
		Boolean results = dbService.updatePassword(pwd, descUser);
		if(results){
			TabUtente utente = null;
			utente= dbService.getTabUtenteByUserName(descUser);
			if(utente.isFlgReceiveEmail()){
				try {
					List<String> userTo=new ArrayList<String>();
					userTo.add(utente.getDescEmail());
					String object = messages.getMessage("changePasswordObj", new Object[]{}, Const.locale);
					String body=Const.EMPTY_STRING;
					body =messages.getMessage("changePasswordBody", new Object[] {utente.getDescNome(), utente.getDescCgn(), utente.getDescPwd()}, Const.locale);
					String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));
					mailer.sendMail(userTo, null, object, template);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}return results;
	} 
	
	public void resetPassword(TabUtente tabUtente){
		dbService.resetPassword(tabUtente);
	}
	
	public List<TabAnagStatiUtente> getAllStatiUtente() {
		return dbService.getAllStatiUtente();
	}
	
	
	public boolean invioMailDiSistema(String textOggetto, String textBody, List<String> selectedAreaDiComp, List<String> selectedRoles){
		try{
			List<String> userBcc = dbService.getUserForMailDiSistema(selectedAreaDiComp, selectedRoles);
			String oggetto = Const.OGGETTO_MAIL +  textOggetto;
			String body = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplateSistema.vm", mailer.templateMail(textBody));
			mailer.sendMailDiSistema(userBcc, oggetto, body);
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public List<ViwUtenti> getAdmin(){
		return dbService.getAdmin();
	}

	public List<ViwZoneRegioni> getViwZoneRegioni(List<Integer> selectedRegioni) {
		return dbService.getViwZoneRegioni(selectedRegioni);
	}

	public ViwAreaCompRegioni getRegioneByCodi(Integer codiRegione) {
		return dbService.getRegioneByCodi(codiRegione);
	}

	public List<Integer> getRegioniSelectedByCodiUtente(Integer codiUtente) {
		return dbService.getRegioniSelectedByCodiUtente(codiUtente);
	}
	
	// update profile
	public void saveProfile(CurrentUser currentUser, Integer codiImpresa, List<Integer> selectedRegioni, Integer codiZona, Integer codiGruppo) {
		dbService.updateProfile(currentUser,codiImpresa,selectedRegioni,codiZona,codiGruppo);
		operationLogger.logOperation(Const.MODIFICA_UTENTE,new Timestamp(System.currentTimeMillis()), currentUser.getCodiUtente(),currentUser.getUsername());	
	}
	
	public List<Integer> getUtentiByRegione(Integer codiIdRegione){
		return dbService.getUtentiByRegione(codiIdRegione);	
	}

	public List<ViwUtenti> getRefTerOPByRegione(List<Integer> codiUtente){
		return dbService.getRefTerOPByRegione(codiUtente);
	}
	
	public List<ViwUtenti> getOperAOUByRegione(List<Integer> codiUtente){
		return dbService.getOperAOUByRegione(codiUtente);
	}
	
	public List<ViwUtenti> getUserRefTerAndCordByRegione(List<Integer> codiUtente){
		return dbService.getUserRefTerAndCordByRegione(codiUtente);
	}

	public List<ViwUtenti> getUserInCodiUtente(List<Integer> codiUtente) {
		return dbService.getUserInCodiUtente(codiUtente);
	}
	
	public List<ViwUtenti> getAllAssocSessionManuale(Integer codiSessione){
		return dbService.getAllAssocSessionManuale(codiSessione);
	}
}
