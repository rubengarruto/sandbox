package com.rextart.saw.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.rextart.saw.bean.CollaudoDatiGeneraliBean;
import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.bean.SessioniFilter;
import com.rextart.saw.bean.WorkRequestBean;
import com.rextart.saw.dao.AnagDao;
import com.rextart.saw.dao.ImpreseDao;
import com.rextart.saw.dao.SessioneDao;
import com.rextart.saw.dao.SessioneDaoImpl;
import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.dao.UserDaoImpl;
import com.rextart.saw.entity.TabAnagDocumenti;
import com.rextart.saw.entity.TabAnagSistemiBande;
import com.rextart.saw.entity.TabAnagWorkRequest;
import com.rextart.saw.entity.TabAreaCompetenza;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabComuni;
import com.rextart.saw.entity.TabCountDocument;
import com.rextart.saw.entity.TabDettaglioCollaudo;
import com.rextart.saw.entity.TabFinalitaCollaudo;
import com.rextart.saw.entity.TabGruppi;
import com.rextart.saw.entity.TabImpianti;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabPoloTecnologico;
import com.rextart.saw.entity.TabProvince;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSesVerbaleParziale;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSchedaDyn;
import com.rextart.saw.entity.TabSessioni;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabUtente;
import com.rextart.saw.entity.TabVendors;
import com.rextart.saw.entity.TabWorkRequestColl;
import com.rextart.saw.entity.TabZone;
import com.rextart.saw.entity.ViwAreaCompRegioni;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwRisultatoSurvey;
import com.rextart.saw.entity.ViwSessImpUser;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwSessioniNote;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.bean.request.SurveyBean;
import com.rextart.saw.rs.bean.response.SessioniBean;
import com.rextart.saw.rs.utility.Constants;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.utility.Const;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class SessioniService {

	@Autowired
	private SessioneDao sessioneDao;
	
	@Autowired
	private SessioneDaoImpl sessioneDaoImpl;


	@Autowired
	private AnagDao anagDao;

	@Autowired
	ImpreseDao impreseDao;

	@Autowired
	UserDaoImpl userDao;

	@Autowired
	MessageSource messages;

	@Autowired
	ApplicationMailer mailer;

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	SincronizzazioneDao sincronizzazioneDao;
	
	
	public ViwSessioni findSessione(Integer codiSessione){
		return sessioneDao.findViwSessioni(codiSessione);
	}

	public List<TabSessioni> getAllSessioni(){
		return sessioneDao.getAllSessioni();
	}

	public List<ViwSessioni> getCollaudiInLavorazioneByAreaComp(Integer codiAreaComp,Integer statoFiltro){
		return sessioneDao.getCollaudiInLavorazioneByAreaComp(codiAreaComp,statoFiltro);
	}

	public List<ViwSessioni> getAllCollaudiInLavorazione(Integer statoFiltro){
		return sessioneDao.getAllCollaudiInLavorazione(statoFiltro);
	}

	public List<ViwSessioni> getAllCollaudiArchiviati(){
		return sessioneDao.getAllCollaudiArchiviati();
	}
	public List<ViwSessioni> getCollaudiArchiviatiByAreaComp(Integer codiAreaComp){
		return sessioneDao.getCollaudiArchiviatiByAreaComp(codiAreaComp);
	}

	public boolean isUserAssocSessione(Integer codiUtente, Integer codiSessione){
		return sessioneDao.isUserAssocSessione(codiUtente, codiSessione);
	}

	public boolean isLockUnlockOggetto(Integer codiSessione, Integer codiOggetto, boolean flagLock){
		boolean isLock = sessioneDao.isLockUnLockOggetto(codiSessione, codiOggetto, flagLock);
		UtilLog.printIsLockOggetto(isLock);

		return isLock;
	}
	public void lockUnLockDoc(Integer codiSessione, Integer codiOggetto, Integer codiUtente, boolean flagLock){
		sessioneDao.lockUnLockDoc(codiSessione, codiOggetto, codiUtente, flagLock);
		sessioneDao.storeLock(codiSessione, codiOggetto, codiUtente, flagLock);

	}
	public Integer getCodiAreaCompetenza(String descSiglaAreaComp){
		return sessioneDao.getCodiAreaCompetenza(descSiglaAreaComp);
	}

	public Integer getCodiVendor(String descVendor){
		return sessioneDao.getCodiVendor(descVendor);
	}

	public Object [] saveCollaudo(TabSessioni tabSessioni) throws Exception{
		Object[] result = sessioneDao.saveCollaudo(tabSessioni);
		if((boolean)result[0]){
			try {
//				List<ViwUtenti> utenti = userDao.getRefTerOPByCodiAreaComp(tabSessioni.getCodiAreaComp());+
				TabRegioni regione = sessioneDao.getIdRegioneByNome(tabSessioni.getDescRegione());
				List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regione.getIdRegione());
				List<ViwUtenti> utenti = userDao.getRefTerOPByRegione(listCodiUtenti);
				
				sessioneDao.saveAssocUtenteRefTerOPByCodiAreaComp(utenti, tabSessioni.getCodiSessione());	
				if(Const.EMAIL_ON){
					List<String> userTo=new ArrayList<String>();
					for (ViwUtenti viwUtenti : utenti) {
						//						 if(!viwUtenti.getFlagIsAssoc())
						if(viwUtenti.getFlgReceiveEmail()){
						userTo.add(viwUtenti.getDescEmail());
						}
					}
					if(userTo!=null && userTo.size()>0){
						String object = messages.getMessage("sessioneAcquisitaObj",new Object[] {tabSessioni.getDescCodice().toUpperCase()}, Const.locale);
						String body =messages.getMessage("sessioneAcquisitaBody", new Object[] {tabSessioni.getCodiCollaudo(), tabSessioni.getDescCodice().toUpperCase(),tabSessioni.getDescNomeSito()}, Const.locale);
						String lista_utenti_associati_al_collaudo = getHTMLListaUtentiAssociatiByCodiSessione(tabSessioni.getCodiSessione());
						String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplateCollaudoDisponibile.vm", mailer.templateMail(body,lista_utenti_associati_al_collaudo));
						mailer.sendMail(userTo, null, object, template);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public boolean rollbackCollaudo(Integer codiSessione){
		try{
			if(isSessioneStatoAcquisito(codiSessione) || isSessioneStatoDisponibile(codiSessione)){
				TabSessioni tabSessioni = sessioneDao.getSessioneByCodi(codiSessione);
				tabSessioni.setTimePrevistoCollaudo(null);
				tabSessioni.setCodiFinalitaColl(null);
				tabSessioni.setDescOperTim(null);
				tabSessioni.setCodiTipoImpianto(null);
				tabSessioni.setDescProgettoRadio(null);
				tabSessioni.setCodiDettaglioColl(null);
				tabSessioni.setFlgAllegatoA(false);
				tabSessioni.setFlgAllegatoB(false);
				tabSessioni.setFlgAllegatoC(false);
				tabSessioni.setFlgAllegatoD(false);
				tabSessioni.setFlgAllegatoD1(false);
				tabSessioni.setFlgAllegatoE(false);

				sessioneDao.saveCollaudo(tabSessioni);

				sessioneDao.deleteWorkRequestByCodiSessione(codiSessione);
				sessioneDao.deleteRelSessioneutenti(codiSessione);
//				List<ViwUtenti> utenti = userDao.getRefTerOPByCodiAreaComp(tabSessioni.getCodiAreaComp());
				TabRegioni regione = sessioneDao.getIdRegioneByNome(tabSessioni.getDescRegione());
				List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regione.getIdRegione());
				List<ViwUtenti> utenti = userDao.getRefTerOPByRegione(listCodiUtenti);
				sessioneDao.saveAssocUtenteRefTerOPByCodiAreaComp(utenti, tabSessioni.getCodiSessione());	
				if(sessioneDao.isSessioneStatoDisponibile(codiSessione)){
					sessioneDao.changeStatoSessione(codiSessione, Const.ACQUISITO);
				}

				surveyService.deleteSurveyBySessione(codiSessione);

			}else{
				return false;
			}


		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}

		return true;
	}
	public List<TabRelRitardiSessioni> getRitardiBySessione(Integer codiSessione){
		return sessioneDao.getRitardiBySessione(codiSessione);
	}

	public List<TabImpianti> getAllImpianti() {
		return anagDao.getAllImpianti();
	}

	public List<TabSistemi> getAllSistemi() {
		return anagDao.getAllSistema();
	}

	public List<TabBande> getAllBande() {
		return anagDao.getAllBande();
	}

	public List<TabDettaglioCollaudo> getAllDettagliColl() {
		return anagDao.getAllDettaglioColl();
	}

	public List<TabFinalitaCollaudo> getAllFinalitaColl() {
		return anagDao.getAllFinalitaColl();
	}

	public List<TabZone> getAllUnitaTerr(Integer codiArea) {
		return anagDao.getAllUnitaTerr(codiArea);
	}

	public Boolean mergeCollaudo(CollaudoDatiGeneraliBean selectedBean, Integer codiUser) {
		try {
			sessioneDao.update(selectedBean,codiUser);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Boolean  unlock(ViwSessioni sessioneSelected, Integer codiUser,Integer codiOggetto) {
		try {
			sessioneDao.unlock(sessioneSelected, codiUser, codiOggetto);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public List<TabImprese> getTableImpreseList(){
		return  impreseDao.getAllImprese();
	}

	public List<ViwUtenti> getRefTerOPByCodiAreaComp(Integer codiAreaComp){
		return userDao.getRefTerOPByCodiAreaComp(codiAreaComp);
	}

	public String getDescGruppoByCLLI(String clliSito){
		return sessioneDao.getDescGruppoByCLLI(clliSito);
	}

	public TabGruppi getTabGruppiDescGruppo(String descGruppo){
		return userDao.getTabGruppiDescGruppo(descGruppo);
	}

	public List<ViwUtenti> getUserMOIByAreaComp(Integer codiImpresa,Integer codiAreaComp){
		return userDao.getUserMOIByAreaComp(codiImpresa,codiAreaComp);
	}

	public List<ViwUtenti> getUtentiListAssoc(Integer codiSessione) {
		return sessioneDao.getAllUserAssocSession(codiSessione);
	}
	
	public List<ViwUtenti> getAllUserMailFineCollaudo(List<Integer> listCodiUtenti){
		return sessioneDao.getAllUserMailFineCollaudo(listCodiUtenti);
	}

	public List<ViwUtenti> getUtentiListByAreaComp(Integer codiAreaComp) {
		return userDao.getUtentiListByAreaComp(codiAreaComp);
	}

	public Boolean saveAssocUtente(List<ViwUtenti> utenti, ViwSessioni sessione,List<Integer> codiUserAssoc) {
		Boolean result = sessioneDao.saveAssocUtente( utenti,sessione.getCodiSessione(),codiUserAssoc);	
		if(result && Const.EMAIL_ON){
			try {
				List<String> userTo=new ArrayList<String>();
				for (ViwUtenti viwUtenti : utenti) {
					if(!viwUtenti.getFlagIsAssoc())
						userTo.add(viwUtenti.getDescEmail());
				}
				if(userTo!=null && userTo.size()>0){
					String object = messages.getMessage("assocUserSessionObj",new Object[]{sessione.getDescCodice().toUpperCase()}, Const.locale);
					String body =messages.getMessage("assocUserSessionBody", new Object[] {sessione.getCodiCollaudo(),sessione.getDescCodice().toUpperCase(),sessione.getDescNomeSito()}, Const.locale);
					String lista_utenti_associati_al_collaudo = getHTMLListaUtentiAssociatiByCodiSessione(sessione.getCodiSessione());
					String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplateCollaudoDisponibile.vm", mailer.templateMail(body,lista_utenti_associati_al_collaudo));
					mailer.sendMail(userTo, null, object, template);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private String getHTMLListaUtentiAssociatiByCodiSessione(Integer codiSessione) {

		ArrayList<ViwUtenti> utentiList = userDao.getUtentiAssociatiByCodiSessione(codiSessione);

		String listaHTMLUtenti = "";

		if (utentiList!=null && utentiList.size()>0) {

			ArrayList<Integer> codiUtentiProcessati = new ArrayList<>();

			listaHTMLUtenti ="<table style='font-size : 12px !important;'>"
					+ "  <tr><th>Ruolo/struttura</th>"
					+ "<th>Nome</th>"
					+ "<th>Telefono</th>"
					+ "<th>Mail</th>"
					+ "</tr>";

			String descTelefono;

			for (ViwUtenti viwUtenti : utentiList) {

				if (!codiUtentiProcessati.contains(viwUtenti.getCodiUtente())) {

					if (viwUtenti.getDescTelefono() == null) {
						descTelefono = " ";
					} else {
						descTelefono = viwUtenti.getDescTelefono();
					}

					listaHTMLUtenti = listaHTMLUtenti + "<tr>"
							+ "<td>" + viwUtenti.getDescRole()+" "+viwUtenti.getDescStruttura() + "</td>"
							+ "<td>" + viwUtenti.getDescNome() + " " + viwUtenti.getDescCgn() + "</td>"
							+ "<td>" + descTelefono + "</td>"
							+ "<td>" + viwUtenti.getDescEmail() + "</td>"
							+ "</tr>";
					codiUtentiProcessati.add(viwUtenti.getCodiUtente());
				}

			} 

			listaHTMLUtenti = listaHTMLUtenti + "</table>";
		}

		return listaHTMLUtenti;
	}

	public TabWorkRequestColl saveWorkRequest(WorkRequestBean workRequestBean,Integer codiUtente) {
		
		try {
			TabWorkRequestColl tab =sessioneDao.saveWorkRequest(workRequestBean,codiUtente) ;
			if(tab!=null)
				return tab;
			else 
				return null;
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean removeWorkRequest(TabWorkRequestColl selectedWorkRequest,Integer codiUtente) {
		try {
			sessioneDao.removeWorkRequest(selectedWorkRequest,codiUtente);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	public List<TabAnagWorkRequest> getAllAnagWorkRequest() {
		return anagDao.getAllAnagWorkRequest();
	}

	public List<TabWorkRequestColl> getWorkRequestBySessione(
			Integer codiSessione) {
		return sessioneDao.getWorkRequestBySessione(codiSessione);
	}

	//	public boolean findByCodiUserCodiAreaComp(Integer codiSessione,Integer codiUtente, Integer codiAreaComp){
	//		return sessioneDao.findByCodiUserCodiAreaComp(codiSessione, codiUtente, codiAreaComp);
	//	}
	public TabAnagSistemiBande findBandaSistemaByCodiImpianto(String descIdentificativo){
		return sessioneDao.findBandaSistemaByCodiImpianto(descIdentificativo);
	}

	public List<ViwUtenti> getOperNoaByCodiAreaComp(Integer codiAreaComp){
		return userDao.getOperNoaByCodiAreaComp(codiAreaComp);
	}

	public void changeStatoSessione(Integer codiSessione, Integer codiNewStato){
		sessioneDao.changeStatoSessione(codiSessione, codiNewStato);
	}

	public boolean isSessioneStatoAcquisito(Integer codiSessione){
		return sessioneDao.isSessioneStatoAcquisito(codiSessione);
	}

	public Integer getCodiStateInLavorazione(){
		return sessioneDao.getCodiStateInLavorazione();
	}

	public void insertRelSessioneStati(Integer codiSessione, Integer codiStato){
		sessioneDao.insertRelSessioneStati(codiSessione, codiStato);
	}

	public Integer getCodiStateDisponibile(){
		return sessioneDao.getCodiStateDisponibile();
	}

	public boolean isSessioneStatoDisponibile(Integer codiSessione){
		return sessioneDao.isSessioneStatoDisponibile(codiSessione);
	}

	public boolean isSessioneStatoInLavorazione(Integer codiSessione){
		return sessioneDao.isSessioneStatoInLavorazione(codiSessione);
	}

	public boolean isSessioneStatoAnnullato(Integer codiSessione){
		return sessioneDao.isSessioneStatoAnnullato(codiSessione);
	}

	public Integer getStateAcquisito(){
		return sessioneDao.getStateAcquisito();
	}


	public List<ViwUtenti> getAllAOUByAreaComp(Integer codiAreaComp){
		return userDao.getAllAOUByAreaComp(codiAreaComp);
	}


	public List<ViwUtenti> getOperAOUByAreaComp(Integer codiAreaComp){
		return userDao.getOperAOUByAreaComp(codiAreaComp);
	}


	public List<ViwUtenti> getDGoperRToperByAreaComp(Integer codiAreaComp){
		return userDao.getDGoperRToperByAreaComp(codiAreaComp);
	}


	public List<TabRelOggettiTipo> getListRelOggettiTipoBySessione(CollaudoDatiGeneraliBean sessione) {
		return sessioneDao.getListRelOggettiTipoBySessione(sessione);
	}

	public Boolean saveTabSurvey(List<TabRelOggettiTipo> tabOggettiTipos, Integer codiSessione) {
		return sessioneDao.saveTabSurvey(tabOggettiTipos, codiSessione);
	}

	public void rendiDiponibileCollaudo(CollaudoDatiGeneraliBean sessione, Integer codiNewStato) {
		changeStatoSessione(sessione.getCodiSessione(), codiNewStato);

//		List<ViwUtenti> utentiToAssocc = getOperAOUByAreaComp(sessione.getCodiAreaComp());
		TabRegioni regioneByName = sessioneDao.getIdRegioneByNome(sessione.getDescRegione());
		if(regioneByName!= null){
			List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regioneByName.getIdRegione());		
			List<ViwUtenti> utentiToAssocc = userDao.getOperAOUByRegione(listCodiUtenti);
			List<ViwUtenti> utentiVendor = new ArrayList<ViwUtenti>();
			Integer codiVendor = null;
			if(sessione.getFlgApparatoEricsson() == true) {
				codiVendor = Const.VENDOR_ERICSSON;
				utentiVendor = getUtentiVendor(codiVendor);
	
			}
			else if (sessione.getFlgApparatoHuawei() == true) {
				codiVendor = Const.VENDOR_HUAWEI;
				utentiVendor = getUtentiVendor(codiVendor);
			}
			else if (sessione.getFlgApparatoNsn() == true) {
				codiVendor = Const.VENDOR_NOKIA;
				utentiVendor = getUtentiVendor(codiVendor);
			}
	//vendor in base al polo tecnologico se codivendor == null
			else  {
				TabPoloTecnologico tabPoloTecnologico= new TabPoloTecnologico();
				utentiVendor = new ArrayList<ViwUtenti>();
					tabPoloTecnologico =sessioneDao.findPoloTecnologicoByRegione(sessione.getDescRegione());
					utentiVendor = getUtentiVendor(tabPoloTecnologico.getCodiVendor());
			}
	//		List<ViwUtenti> utentiInCC = userDao.getUserRefTerAndCordByAreaComp(sessione.getCodiAreaComp());
			List<ViwUtenti> utentiInCC = userDao.getUserRefTerAndCordByRegione(listCodiUtenti);
			if(utentiVendor.size()>0){
				utentiToAssocc.addAll(utentiVendor);
	//			utentiInCC.addAll(utentiVendor);
			}
			Boolean result = sessioneDao.saveAssocUtenteOperAOU( utentiToAssocc,sessione.getCodiSessione());	
			
			/*Controllo la versione Della Sessione se=1  salvo i survey altrimenti no*/
			ViwSessioni checkSessione=sessioneDao.findViwSessioni(sessione.getCodiSessione());
			
			Boolean result1=false; 
			if(checkSessione.getVersione()==Const.SESSIONE_NON_CLONATA){
				result1 = sessioneDao.saveTabSurvey(getListRelOggettiTipoBySessione(sessione), sessione.getCodiSessione());
			}else{
				result1=true;
			}
			List<ViwUtenti> utentiInTO = userDao.getUtentiAssociatiByCodiSessione(sessione.getCodiSessione());
	
			if(result && result1 && Const.EMAIL_ON){
				try {
					List<String> userTo=new ArrayList<String>();
					for (ViwUtenti viwUtenti : utentiInTO) {
						if(viwUtenti.getFlgCessSosp()==false && viwUtenti.getFlgReceiveEmail()==true){
						userTo.add(viwUtenti.getDescEmail());
						}
					}
					List<String> userCc=new ArrayList<String>();
					for (ViwUtenti viwUtenti : utentiInCC) {
						userCc.add(viwUtenti.getDescEmail());
					}
					if(userTo!=null && userTo.size()>0){
						String object =messages.getMessage("sessioneDisponibileObj",new Object[] {sessione.getDescCodice().toUpperCase()}, Const.locale);
						String body =messages.getMessage("sessioneDisponibileBody", new Object[] {sessione.getCodiCollaudo(), sessione.getDescCodice().toUpperCase(),sessione.getDescNomeSito()}, Const.locale);
						String lista_utenti_associati_al_collaudo = getHTMLListaUtentiAssociatiByCodiSessione(sessione.getCodiSessione());
						String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplateCollaudoDisponibile.vm", mailer.templateMail(body,lista_utenti_associati_al_collaudo));
						mailer.sendMail(userTo, userCc, object, template);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void invioMailD1(ViwSessioni selectedSessione, String anagDocumento, String nomeFile, Integer codiUtente,
			Integer codiEsito) {
		try {

			String esitoP;
			String esitoPcr;
			String esitoN;

			switch (codiEsito) {
			case 1:
				esitoP = Const.ESITO_POSITIVO;
				esitoPcr = "";
				esitoN = "";
				break;
			case 2:
				esitoPcr = Const.ESITO_POSITIVO_CON_RISERVA;
				esitoP = "";
				esitoN = "";
				break;
			case 3:
				esitoN = Const.ESITO_NEGATIVO;
				esitoP = "";
				esitoPcr = "";
				break;
			default:
				esitoP = "";
				esitoPcr = "";
				esitoN = "";
				break;
			}

			//In To tutti gli associati alla sessione
			List<ViwUtenti> listViwUtenti = userDao.getUtentiAssociatiByCodiSessione(selectedSessione.getCodiSessione());
			List<String> userTo = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : listViwUtenti) {
				if(listUtentiFiltered.getFlgCessSosp()==false && listUtentiFiltered.getFlgReceiveEmail()==true){
					userTo.add(listUtentiFiltered.getDescEmail());
				}
			}
			
			//In Cc tutti i referenti territoriali e coordinatori relativi alla stessa regione della sessione
//			listViwUtenti = userDao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
			TabRegioni regioneByName = sessioneDao.getIdRegioneByNome(selectedSessione.getDescRegione());
			List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regioneByName.getIdRegione());		
			List<ViwUtenti> utentiInCC = userDao.getUserRefTerAndCordByRegione(listCodiUtenti);


			List<String> userCc = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : utentiInCC) {
				if (listUtentiFiltered.getFlgReceiveEmail()==true){
					userCc.add(listUtentiFiltered.getDescEmail());
				}
			}
			
			if (userTo != null && userTo.size() > 0) {
				String object = messages.getMessage("uploadD1Obj",
						new Object[] { selectedSessione.getDescCodice().toUpperCase(), anagDocumento, esitoP, esitoPcr, esitoN}, Const.locale);
				String body = messages
						.getMessage("uploadD1Body",
								new Object[] { selectedSessione.getCodiCollaudo(), selectedSessione.getDescCodice().toUpperCase(),
										selectedSessione.getDescNomeSito(), nomeFile, anagDocumento, esitoP, esitoPcr, esitoN },
								Const.locale);
				String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
						mailer.templateMail(body));
				mailer.sendMail(userTo, userCc, object, template);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void invioMailDocUpload(ViwSessioni selectedSessione, Integer tipoDocumento, String nomeFile,
			Integer codiUtente) {
		try {

			// Filtro gli utenti presi dalla lista per codice ruolo e prendo le
			// loro email inserendole nella lista per inviarle in To
			TabUtente utente = userDao.getUserById(codiUtente);
			List<String> userTo = new ArrayList<String>();
			if(utente.isFlgReceiveEmail()==true && utente.isFlgCessSosp()==false){
				userTo.add(utente.getDescEmail());
			}

			String anagDocumento;

			switch (tipoDocumento) {
			case 2:
				anagDocumento = Const.FILE_PER_MISURAZIONI;
				break;
//			case 3:
//				anagDocumento = Const.MISURE;
//				break;
//			case 4:
//				anagDocumento = Const.MEDIA;
//				break;
			case 5:
				anagDocumento = Const.MISURE_PIM;
				break;
			case 6:
				anagDocumento = Const.MISURE_ROS;
				break;
			case 7:
				anagDocumento = Const.EXPORT_NODO;
				break;
			case 8:
				anagDocumento = Const.DOC_TEST_REPORT_RADIO;
				break;
			case 9:
				anagDocumento = Const.FOTO_SUPPORTO;
				break;
			case 10:
				anagDocumento = Const.FOTO_CELLA;
				break;
			case 11:
				anagDocumento = Const.FOTO_SALA;
				break;
			case 12:
				anagDocumento = Const.FOTO_LOCALITA;
				break;
		
			default:
				anagDocumento = Const.EMPTY_STRING;
				break;
			}
			if (userTo != null && userTo.size() > 0) {
				String object = messages.getMessage("uploadFileObj",
						new Object[] { selectedSessione.getDescCodice().toUpperCase(), anagDocumento }, Const.locale);
				String body = messages.getMessage("uploadFileBody", new Object[] { selectedSessione.getCodiCollaudo(),
						selectedSessione.getDescCodice().toUpperCase(), selectedSessione.getDescNomeSito(), nomeFile, anagDocumento },
						Const.locale);
				String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
						mailer.templateMail(body));
				mailer.sendMail(userTo, null, object, template);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void invioMailPev(ViwSessioni selectedSessione, Integer codiEsito) {
		try {

			String esitoP;
			String esitoPcr;
			String esitoN;

			switch (codiEsito) {
			case 1:
				esitoP = Const.ESITO_POSITIVO;
				esitoPcr = "";
				esitoN = "";
				break;
			case 2:
				esitoPcr = Const.ESITO_POSITIVO_CON_RISERVA;
				esitoP = "";
				esitoN = "";
				break;
			case 3:
				esitoN = Const.ESITO_NEGATIVO;
				esitoP = "";
				esitoPcr = "";
				break;
			default:
				esitoP = "";
				esitoPcr = "";
				esitoN = "";
				break;
			}

			//In To tutti gli associati alla sessione
			List<ViwUtenti> listViwUtenti = userDao.getUtentiAssociatiByCodiSessione(selectedSessione.getCodiSessione());
			List<String> userTo = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : listViwUtenti) {
				if(listUtentiFiltered.getFlgCessSosp()==false && listUtentiFiltered.getFlgReceiveEmail()==true){
					userTo.add(listUtentiFiltered.getDescEmail());
				}
			}
			
			//In Cc tutti i referenti territoriali e coordinatori relativi alla stessa regione della sessione
//			listViwUtenti = userDao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
			TabRegioni regioneByName = sessioneDao.getIdRegioneByNome(selectedSessione.getDescRegione());
			List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regioneByName.getIdRegione());		
			List<ViwUtenti> utentiInCC = userDao.getUserRefTerAndCordByRegione(listCodiUtenti);

			List<String> userCc = new ArrayList<String>();
			for (ViwUtenti listUtentiFiltered : utentiInCC) {
				if(listUtentiFiltered.getFlgReceiveEmail()==true){
					userCc.add(listUtentiFiltered.getDescEmail());
				}
			}
			
			if (userTo != null && userTo.size() > 0) {
				String object = messages.getMessage("invioPevObj",
						new Object[] { selectedSessione.getDescCodice().toUpperCase()}, Const.locale);
				String body = messages
						.getMessage("invioPevBody",
								new Object[] { selectedSessione.getCodiCollaudo(), esitoP, esitoPcr, esitoN, selectedSessione.getDescCodice().toUpperCase(),
										selectedSessione.getDescNomeSito()},
								Const.locale);
				String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
						mailer.templateMail(body));
				mailer.sendMail(userTo, userCc, object, template);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<ViwSessioni> getSessioniDownload() {
		return sessioneDao.getSessioniDownload();
	}

	public boolean isCheckRitardo(){
		Calendar calendar = new GregorianCalendar();
		int ore = calendar.get(Calendar.HOUR);
		int minuti = calendar.get(Calendar.MINUTE);

		if(calendar.get(Calendar.AM_PM) == 0){
			if(ore < Constants.oraLimiteCheckOut){
				return false;
			}
			if(ore == Constants.oraLimiteCheckOut && minuti <= Constants.minutiLimiteCheckOut){
				return false;
			}
		}

		return true;
	}
	public TabGruppi getGruppoByDescGruppo(String descGruppo){
		return sessioneDao.getGruppoByDescGruppo(descGruppo);
	}

	public List<TabAreaCompetenza> getAllAreaCompetenza(){
		return sessioneDao.getAllAreaCompetenza();
	}
	
	public List<ViwAreaCompRegioni> getRegioneByAreaComp(Integer codiAreaComp){
		return sessioneDao.getRegioneByAreaComp(codiAreaComp);
	}
	
	public List<TabProvince> getProvinceByIdRegione(String codiRegione){
		return sessioneDao.getProvinceByIdRegione(codiRegione);
	}
	
	public List<TabComuni> getComuneByIdProvincia(String codiProvincia){
		return sessioneDao.getComuneByIdProvincia(codiProvincia);
	}
	
	public String  getRegioneByNome(String nomeRegione){
		return sessioneDao.getRegioneByNome(nomeRegione);
	}
	
	public String  getProvinciaByNome(String nomeProvincia){
		return sessioneDao.getProvinciaByNome(nomeProvincia);
	}
	
	public List<TabVendors> getAllVendors(){
		return sessioneDao.getAllVendors();
	}


	public List<ViwVerbale> getListVerbaliBySessioni(List<SessioniBean> listSessioni) {
		 List<Integer> listCodiSessione = new ArrayList<>();
			for (SessioniBean sessioniBean: listSessioni)  {
				listCodiSessione.add(sessioniBean.getCodiSessione());
			}
			if(listCodiSessione.size()>0){
				return sessioneDao.getListVerbaliBySessioni(listCodiSessione);
			}else{
				return new ArrayList<ViwVerbale>();
			}
			
	}
	
	public List<ViwVerbale> getListVerbaliBySessioniInLavorazione(List<ViwSessioni> listSessioni) {
		 List<Integer> listCodiSessione = new ArrayList<>();
			for (ViwSessioni viwSessioni: listSessioni)  {
				listCodiSessione.add(viwSessioni.getCodiSessione());
			}
			return sessioneDao.getListVerbaliBySessioni(listCodiSessione);
	}
	
	public void sendAttachement(){
		byte[] data = null;
		Path path = Paths.get("C:/Users/Rosa/Desktop/SAW/SPERIMENTAZIONE_CENTRO_HUAWEI/CHT0C9_SCHPRC_20171122_W.PDF");
		try {
			data = Files.readAllBytes(path);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<String> userTo=new ArrayList<String>();
		 userTo.add("marco.verdecchia@rextart.com");
		try {
			mailer.sendMailAttachmentByte(userTo, null, "test", "body", data, "test.pdf");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public TabRelSesVerbaleParziale findByOggettoSessione(Integer codiOggetto,Integer codiSessione){
		return sessioneDao.findByOggettoSessione(codiOggetto,codiSessione);
	}
	
	public List<TabAnagDocumenti> getAllAnagDocumenti() {
		return anagDao.getAllAnagDocumenti();
	}
	
	public List<TabAnagDocumenti> getAllAnagDocumentiWithoutD1() {
		return anagDao.getAllAnagDocumentiWithoutD1();
	}
	
	public TabCountDocument findProgessiovoDocument(Integer codiSessione,Integer codiAnagDocumento){
		return  anagDao.findProgessiovoDocument(codiSessione, codiAnagDocumento);
	}
	
	public List<ViwUtenti> getUtentiVendor(Integer codiVendor){
		return userDao.getUtentiVendor(codiVendor);
	}

	public void saveRelSessVerbaleParziale(Integer codiOggetto,Integer codiSessione, boolean b) throws Exception {
		sincronizzazioneDao.saveRelSessVerbaleParziale(codiOggetto,codiSessione,b);
	}

	public List<TabSchedaDyn> getSchedaDynByCodiSessioneList(List<SessioniBean> listSessioni) {
		List<Integer> listCodiSessione = new ArrayList<>();
		for (SessioniBean sessione:listSessioni) {
			listCodiSessione.add(sessione.getCodiCollaudo());
		}
		return sessioneDao.getSchedaDynByCodiSessioneList(listCodiSessione);
	}
	
	public List<ViwSessioni> getCollaudiInLavorazioneByUserVendor(Integer codiVendor,Integer statoFiltro){
		return sessioneDao.getCollaudiInLavorazioneByUserVendor(codiVendor,statoFiltro);
	}
	
	public List<ViwSessioni> getCollaudiArchiviatiByUserVendor(Integer codiVendor){
		return sessioneDao.getCollaudiArchiviatiByUserVendor(codiVendor);
	}
	
	public List<ViwSessioni> getCollaudiInLavorazioneByMoi(Integer codiImpresa,Integer statoFiltro){
		return sessioneDao.getCollaudiInLavorazioneByMoi(codiImpresa,statoFiltro);
	}
	
	public List<ViwSessioni> getCollaudiInLavorazioneByMoiAssoc(Integer codiUtente,Integer statoFiltro){
		return sessioneDao.getCollaudiInLavorazioneByMoiAssoc(codiUtente,statoFiltro);
	}
	
	public List<ViwSessioni> getCollaudiArchiviatiByMoiAssoc(Integer codiUtente){
		return sessioneDao.getCollaudiArchiviatiByMoiAssoc(codiUtente);
	}
	
	
	
	public List<ViwSessioni> getCollaudiArchiviatiByMoi(Integer codiImpresa){
		return sessioneDao.getCollaudiArchiviatiByMoi(codiImpresa);
	}

	public List<TabRisultatoSurvey> getListRisultatoSurveyBySessioni(List<SurveyBean> surveyBeanList) {
		List<Integer> listCodi = new ArrayList<>();
		for (SurveyBean surveyBean: surveyBeanList) {
			listCodi.add(surveyBean.getCodiSurveyBackEnd());
		}
		return surveyService.getListRisultatiSurvey(listCodi);
	}
	
	public List<ViwSessImpUser> getDitteByCodiSessione(Integer codiSessione) {
		return sessioneDaoImpl.getDitteByCodiSessione(codiSessione);
	}
	
	public List<ViwSessImpUser> getUsersByCodiImpresaAndCodiSessione(Integer codiImpresa, Integer codiSessione) {
		return sessioneDaoImpl.getUsersByCodiImpresaAndCodiSessione(codiImpresa, codiSessione);	
	}
	
//	Metodi per invio a pev	
	public List<ViwVerbale> getListVerbaliByCodiSessione(Integer codiSessione) {
			return sessioneDao.getListVerbaliByCodiSessione(codiSessione);
	}
	
	public void setSentToPev(Integer codiSessione) {
		sessioneDao.setSentPev(codiSessione);
	}
	
	 public List<ViwSessioni> getFilterViwSessioni(SessioniFilter sessioniFilter){
		 return sessioneDao.getFilterViwSessioni(sessioniFilter);
	 }
	 public ViwSessioni findViwSessioniToClone(Integer codiCollaudo){
		 return sessioneDao.findViwSessioniToClone(codiCollaudo);
	 }
	 public Integer clonaCollaudo(Integer codiCollaudo, Integer codiSessione,Integer codiAreaComp) {
		 Integer result= sessioneDao.clonaCollaudo(codiCollaudo,codiSessione,codiAreaComp);
		 ViwSessioni sessione = sessioneDao.findViwSessioniToClone(codiCollaudo);
		 if(result != null && !result.equals("")){
				try {
//					List<ViwUtenti> utenti = userDao.getRefTerOPByCodiAreaComp(sessione.getCodiAreaComp());
					TabRegioni regione = sessioneDao.getIdRegioneByNome(sessione.getDescRegione());
					List<Integer> listCodiUtenti = userDao.getUtentiByRegione(regione.getIdRegione());
					List<ViwUtenti> utenti = userDao.getRefTerOPByRegione(listCodiUtenti);

					if(Const.EMAIL_ON){
						List<String> userTo=new ArrayList<String>();
						for (ViwUtenti viwUtenti : utenti) {
							//						 if(!viwUtenti.getFlagIsAssoc())
							userTo.add(viwUtenti.getDescEmail());
						}
						if(userTo!=null && userTo.size()>0){
							String object = messages.getMessage("sessioneAcquisitaObj",new Object[] {sessione.getDescCodice().toUpperCase()}, Const.locale);
							String body =messages.getMessage("sessioneAcquisitaBody", new Object[] {sessione.getCodiCollaudo(), sessione.getDescCodice().toUpperCase(),sessione.getDescNomeSito()}, Const.locale);
							String lista_utenti_associati_al_collaudo = getHTMLListaUtentiAssociatiByCodiSessione(sessione.getCodiSessione());
							String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplateCollaudoDisponibile.vm", mailer.templateMail(body,lista_utenti_associati_al_collaudo));
							mailer.sendMail(userTo, null, object, template);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return 0;	
				}
				
			}
		return result;
	 }
	 public List<ViwSessioni> findViwSessioniByCodiCollaudo(Integer codiCollaudo){
		 return sessioneDao.findViwSessioniByCodiCollaudo(codiCollaudo);
	 }
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessione(Integer codiSessione) {
		return sessioneDao.getAllRisultatiByCodiSessione(codiSessione);
	}
	
	public List<ViwRisultatoSurvey> getAllRisultatiByCodiSessioneAndCodiOggetto(Integer codiSessione, Integer codiOggetto) {
		return sessioneDao.getAllRisultatiByCodiSessioneAndCodiOggetto(codiSessione, codiOggetto);
	}
	
	public List<ViwRisultatoSurvey> getAllRisultatiNegativiRiservaByCodiSessione(Integer codiSessione) {
		return sessioneDao.getAllRisultatiNegativiRiservaByCodiSessione(codiSessione);
	}
	
	public String findIdentificativoByBandaAndSistema(Integer codiBanda, Integer codiSistema){
		return sessioneDao.findIdentificativoByBandaAndSistema(codiBanda, codiSistema);
		
	}
	
	public Character countChar(Integer count){
		char carattere = (char) ('A'+count);
		return carattere;
	}
	
	public TabRegioni getIdRegioneByNome(String nomeMaiuscolo){
		return sessioneDao.getIdRegioneByNome(nomeMaiuscolo) ;
	}
	
	public String getDescRegioneByNome(String nomeRegione){
		return sessioneDao.getDescRegioneByNome(nomeRegione);
	}
	
	public TabPoloTecnologico findPoloTecnologicoByRegione(String descRegione){
		return sessioneDao.findPoloTecnologicoByRegione(descRegione);
	}

	public List<ViwEsitoSurvey> getNoteTabletByCodiSessione(Integer codiSessione) {
		return sessioneDao.getNoteTabletByCodiSessione(codiSessione);
	}

	public void saveNotaWeb(String descNota, Integer codiSessione, String username) {
		sessioneDao.saveNotaWeb(descNota,codiSessione,username);
		
	}

	public List<TabNote> getNote(Integer codiSessione) {
		return sessioneDao.getNote(codiSessione);
	}

//	public String getDescOggetto(Integer codiOggetto) {
//		return sessioneDao.getDescOggetto(codiOggetto);
//	}

	public void updateNotaWeb(TabNote selectedNota) {
		sessioneDao.updateNotaWeb(selectedNota);
		
	}

	public List<ViwEsitoSurvey> getNoteDitta(Integer codiSessione) {
		return sessioneDao.getNoteDitta(codiSessione);

	}
	
	public List<TabNote> getNoteDittaBySessione(Integer codiSessione){
		return sessioneDao.getNoteDittaBySessione(codiSessione);
	}

	public void sendMailNota(CurrentUser currentUser, ViwSessioni selectedSessione, TabNote nota){
		ViwUtenti user = currentUser.getViwUtente() ;
		List<ViwUtenti> utenti;
		utenti = userDao.getUtentiAssociatiByCodiSessione(selectedSessione.getCodiSessione());
		if(utenti==null){
			utenti = new ArrayList<ViwUtenti>();
		}
		List<String> emailInTo = new ArrayList<String>();
		for(ViwUtenti utente : utenti){
			if(utente.getDescEmail()!=null){
				emailInTo.add(utente.getDescEmail());
			}
		}
		try {
			String subject = messages.getMessage("invioNota", new Object[]{selectedSessione.getDescCodice(), user.getDescUser()},Const.locale);
			String body = messages.getMessage("invioNotaBody", new Object[]{nota.getDescNota(), user.getDescUser(), selectedSessione.getDescCodice(), selectedSessione.getDescNomeSito()}, Const.locale);
			String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",mailer.templateMail(body));
			
			mailer.sendMail(emailInTo,null, subject, template);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public List<ViwSessioniNote> getSessioniNoteByListSessioni(List<Integer> listCodiSessioni){
		return sessioneDao.getSessioniNoteByListSessioni(listCodiSessioni);
	}
	
	
}
