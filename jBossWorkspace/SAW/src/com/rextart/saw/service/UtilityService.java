package com.rextart.saw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.ErrorDeviceDaoImpl;
import com.rextart.saw.entity.TabErrorDevice;


@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class UtilityService {
	
	@Autowired
	ErrorDeviceDaoImpl errorDeviceDaoImpl;
	
	
	public void saveErrorDevice(TabErrorDevice tabErrorDevice){
		errorDeviceDaoImpl.save(tabErrorDevice);
	}
	
	public List<TabErrorDevice> getAll(){
		return errorDeviceDaoImpl.getAll();
	}
}
