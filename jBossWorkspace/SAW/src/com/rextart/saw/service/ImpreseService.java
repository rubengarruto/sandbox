package com.rextart.saw.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.dao.ImpreseDaoImpl;
import com.rextart.saw.dao.LogOperationDaoImpl;
import com.rextart.saw.entity.TabImprese;
import com.rextart.saw.utility.Const;


@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class ImpreseService {
	
	 @Autowired
	 ImpreseDaoImpl dbService;
	 
	 @Autowired
	 LogOperationDaoImpl operationLogger;
	 
	 public List<TabImprese> getTableImpreseList(){
		 return  dbService.getAllImprese();
	 }
	 public void saveImpresa(TabImprese tabImprese,CurrentUser currentUser)throws Exception{
			dbService.saveImpresa(tabImprese);
			operationLogger.logOperation(Const.CREATA_IMPRESA,new Timestamp(System.currentTimeMillis()) , currentUser.getCodiUtente(),tabImprese.getDescImpresa());
	 }
	 public void updateImpresa(TabImprese tabImprese,CurrentUser currentUser)throws Exception{
			dbService.updateImpresa(tabImprese);
		 	operationLogger.logOperation(Const.MODIFICATA_IMPRESA,new Timestamp(System.currentTimeMillis()) , currentUser.getCodiUtente(),tabImprese.getDescImpresa());
	 }
	 
	 public TabImprese getImpresaById(int codiImpresa){
		 return dbService.getImpresaById(codiImpresa);
	 }
	 
	 public Object[] removeImpresa(TabImprese tabImprese,CurrentUser currentUser){
		 Object[] result= dbService.removeImpresa(tabImprese);
			if((boolean)result[0]) 
				operationLogger.logOperation(Const.ELIMINATA_IMPRESA,new Timestamp(System.currentTimeMillis()) , currentUser.getCodiUtente(),tabImprese.getDescImpresa());
			 return result;
	 }
	 
	public TabImprese getImpresaByCF (String descCf){
		return dbService.getImpresaByCF(descCf);	
	}
	
	public TabImprese getImpresaByCFAndByCodiImpresa (String descCf, Integer codiImpresa){
		return dbService.getImpresaByCFAndByCodiImpresa(descCf, codiImpresa);
		
	}

}
