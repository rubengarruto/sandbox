package com.rextart.saw.service;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.rextart.saw.dao.LogOperationDaoImpl;
import com.rextart.saw.entity.TabAnagOperTrace;
import com.rextart.saw.entity.TabLoginFailed;
import com.rextart.saw.entity.ViwOperTrace;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.utility.Const;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class OperationLogService {

	 @Autowired
	 LogOperationDaoImpl operationLogger;

	 @Autowired
	 ApplicationMailer mailer;

	 @Autowired
	 MessageSource messages;
	 
	 @Autowired
	 private VelocityEngine velocityEngine;
	 
	 @Autowired
	 UserService userService;


	 public void logOperation(int codiAnagOperTrace,Timestamp timeDataOper,Integer codiUtente,String descDettaglio) {
		 operationLogger.logOperation(codiAnagOperTrace, timeDataOper, codiUtente, descDettaglio);
	}
	 
	public List<ViwOperTrace> getAllOperTrace(){
		return operationLogger.getAllOperTrace();
	}
	
	public List<TabAnagOperTrace> getAllAnagOperTrace(){
		return operationLogger.getAllAnagOperTrace();
	}
	
	
		
	public List<TabLoginFailed> getAllLoginFailed(){
		return operationLogger.getAllLoginFailed();
	}
	
	public List<TabLoginFailed> getAllLoginFailedByIp(String ipUser){
		return operationLogger.getAllLoginFailedByIp(ipUser);
	}
	
	public void saveLoginFailed(TabLoginFailed tabLoginFailed)throws Exception{
			operationLogger.saveLoginFailed(tabLoginFailed);
	}
	 
	public void removeTabLoginFailed(String ipUser){
		operationLogger.removeLoginFailed(ipUser);
	}

	public void sendMail(String ipAddress, String username){
		List<ViwUtenti> utenti = new ArrayList<ViwUtenti>();
		try {
			utenti = userService.getAdmin();
		} catch (Exception e) {
		}
		List<String> emailInTo = new ArrayList<String>();
		for(ViwUtenti utente : utenti){
			if(utente.getDescEmail()!=null){
				emailInTo.add(utente.getDescEmail());
			}
		}
		try {
			String subject = messages.getMessage("loginFailedObj", new Object[]{},Const.locale);
			String body = messages.getMessage("loginFailedBody", new Object[]{ipAddress, username}, Const.locale);
			String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",mailer.templateMail(body));
			
			mailer.sendMail(emailInTo,null, subject, template);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void sendMailFOB(String userName, Timestamp data) {
		List<ViwUtenti> utenti = new ArrayList<ViwUtenti>();
		try {
			utenti = userService.getAdmin();
		} catch (Exception e) {
		}
		List<String> emailInTo = new ArrayList<String>();
		for(ViwUtenti utente : utenti){
			if(utente.getDescEmail()!=null){
				emailInTo.add(utente.getDescEmail());
			}
		}
		try {
			String subject = messages.getMessage("loginFOBObj", new Object[]{},Const.locale);
			String body = messages.getMessage("loginFOBBody", new Object[]{userName, data.getHours() + "." + data.getMinutes()}, Const.locale);
			String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",mailer.templateMail(body));
			
			mailer.sendMail(emailInTo,null, subject, template);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
