package com.rextart.saw.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.dao.UserDaoImpl;
import com.rextart.saw.entity.ViwUtenti;

@Service
@Transactional(readOnly=true)
public class CustomUserDetailsService implements UserDetailsService{

 @Autowired
 UserDaoImpl dbService;	
	
    public  UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
       
    	
    	boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
       
        ViwUtenti user = dbService.getAuthUser(login);
        return new CurrentUser(user.getDescUser(), user.getDescPwd(),user.getCodiUtente(),user.getDescNome(),user.getDescCgn(), user.getDescEmail(), user.getDescTelefono(),
        		user.getFlgReceiveEmail(), user.getDescRole(),user.getCodiRole(),user.getCodiStruttura(), user.getDescStruttura(),user.getDescAreaComp(),user.getCodiAreaComp(),user.getCodiZona(), user.getCodiGruppo(), enabled, accountNonExpired, credentialsNonExpired,
        		accountNonLocked, getAuthorities(user.getDescCodiRole()), user);    
    }

	public UserDaoImpl getDbService() {
		return dbService;
	}
	public void setDbService(UserDaoImpl dbService) {
		this.dbService = dbService;
	}
	public Collection<? extends GrantedAuthority> getAuthorities(String role) {
		List<String> grents = new ArrayList<>();
		grents.add(role);
        List<GrantedAuthority> authList = getGrantedAuthorities(grents);
        return authList;
    }
    
    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
       
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
