package com.rextart.saw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.bean.CurrentUser;
import com.rextart.saw.dao.AnagDaoImpl;
import com.rextart.saw.entity.TabAnagAntenne;
import com.rextart.saw.entity.TabAnagDiplexer;
import com.rextart.saw.entity.TabAnagLov;
import com.rextart.saw.entity.TabAnagRisultati;
import com.rextart.saw.entity.TabAnagRitardi;
import com.rextart.saw.entity.TabAnagStatiSessione;
import com.rextart.saw.entity.TabAnagTma;
import com.rextart.saw.entity.TabBande;
import com.rextart.saw.entity.TabCaratteristiche;
import com.rextart.saw.entity.TabCostrModAntenna;
import com.rextart.saw.entity.TabElementi;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRelCaratteristicheLov;
import com.rextart.saw.entity.TabRelCascadeCarattLov;
import com.rextart.saw.entity.TabRelElementiCaratt;
import com.rextart.saw.entity.TabRelOggettiElementi;
import com.rextart.saw.entity.TabRelOggettiTipo;
import com.rextart.saw.entity.TabRelServiziOggetti;
import com.rextart.saw.entity.TabServizi;
import com.rextart.saw.entity.TabSistemi;
import com.rextart.saw.entity.TabStruttura;
import com.rextart.saw.entity.TabSyncAnagrafica;
import com.rextart.saw.entity.TabTipologieOggetto;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class AnagService {

	@Autowired
	AnagDaoImpl dbService; 

	public List<TabAnagLov> getAllAnagLov(){
		return  dbService.getAllAnagLov();
	}

	public List<TabCaratteristiche> getAllCaratteristiche(){
		return  dbService.getAllCaratteristiche();
	}

	public List<TabElementi> getAllElementi(){
		return  dbService.getAllElementi();
	}

	public List<TabOggetti> getAllOggetti(){
		return  dbService.getAllOggetti();
	}

	public List<TabServizi> getAllServizi(){
		return  dbService.getAllServizi();
	}

	public List<TabStruttura> getAllStruttura(){
		return dbService.getAllStruttura();
	}

	public List<TabRelCaratteristicheLov> getAllRelCaratteristicheLov(){
		return  dbService.getAllRelCaratteristicheLov();
	}

	public List<TabRelElementiCaratt> getAllRelElementiCaratt(){
		return  dbService.getAllRelElementiCaratt();
	}

	public List<TabRelOggettiElementi> getAllRelOggettiElementi(){
		return  dbService.getAllRelOggettiElementi();
	}

	public List<TabRelServiziOggetti> getAllRelServiziOggetti(){
		return  dbService.getAllRelServiziOggetti();
	}

	public List<TabTipologieOggetto> getAllTipologieOggetto(){
		return  dbService.getAllTipologieOggetto();
	}

	public List<TabRelOggettiTipo> getAllRelOggettiTipo(){
		return  dbService.getAllRelOggettiTipo();
	}

	public List<TabSyncAnagrafica> getAllTabSyncAnagrafica(){
		return dbService.getAllTabSyncAnagrafica();
	}

	public TabSyncAnagrafica getSyncAnagByNameTable(String nameTable){
		return dbService.getByNameTable(nameTable);
	}

	public List<TabAnagStatiSessione> getAllAnagStatiSessione(){
		return  dbService.getAllAnagStatiSessione();
	}

	public List<TabAnagAntenne> getAllAnagAntenne() {
		return  dbService.getAllAnagAntenne();
	}

	public List<TabAnagDiplexer> getAllAnagDiplexer() {
		return  dbService.getAllAnagDiplexer();
	}

	public List<TabAnagTma> getAllAnagTma() {
		return  dbService.getAllAnagTma();
	}

	public List<TabAnagRisultati> getAllRisultati(){
		return dbService.getAllRisultati();
	}

	public List<TabAnagRitardi> getAllRitardi(){
		return dbService.getAllRitardi();
	}

	public List<TabBande> getAllBande(){
		return dbService.getAllBande();
	}

	public List<TabSistemi> getAllSistemi(){
		return dbService.getAllSistemi();
	}

	public void saveRitardi(TabAnagRitardi tabAnagRitardi)throws Exception{
		dbService.saveRitardi(tabAnagRitardi);
	}

	public void saveAntenne(TabAnagAntenne tabAnagAntenne)throws Exception{
		dbService.saveAntenne(tabAnagAntenne);
	}

	public void saveDiplexer(TabAnagDiplexer tabAnagDiplexer)throws Exception{
		dbService.saveDiplexer(tabAnagDiplexer);
	}

	public void saveTma(TabAnagTma tabAnagTma)throws Exception{
		dbService.saveTma(tabAnagTma);
	}

	public void updateSyncAnagrafica(TabSyncAnagrafica tabSyncAnagrafica){
		dbService.updateSyncAnagrafica(tabSyncAnagrafica);
	}

	public TabAnagRitardi getAnagRitardiUnique(String descRitardo, boolean flgCheckOut){
		return dbService.getAnagRitardiUnique(descRitardo, flgCheckOut);
	}

	public TabAnagAntenne getAnagAntenneUnique(String descMarca, String descModello){
		return dbService.getAnagAntenneUnique(descMarca, descModello);
	}

	public List<TabAnagAntenne> getAnagAntenneByDescMarca(String descMarca){
		return dbService.getAllAnagAntenneByDescMarca(descMarca);
	}

	public List<TabAnagTma> getAnagTmaByDescMarca(String descMarca){
		return dbService.getAllAnagTmaByDescMarca(descMarca);
	}

	public List<TabAnagDiplexer> getAnagDiplexerByDescMarca(String descMarca){
		return dbService.getAllAnagDiplexerByDescMarca(descMarca);
	}

	public List<String> getAllAnagAntenneDescMarca()
	{
		return dbService.getAllAnagAntenneDescMarca();
	}

	public List<String> getAllAnagTmaDescMarca()
	{
		return dbService.getAllAnagTmaDescMarca();
	}

	public List<String> getAllAnagDiplexerDescMarca()
	{
		return dbService.getAllAnagDiplexerDescMarca();
	}

	public TabAnagDiplexer getAnagDiplexerUnique(String descMarca, String descModello){
		return dbService.getAnagDiplexerUnique(descMarca, descModello);
	}

	public TabAnagTma getAnagTmaUnique(String descMarca, String descModello){
		return dbService.getAnagTmaUnique(descMarca, descModello);
	}

	public List<TabAnagStatiSessione> getAllAnagStatiSessioneNotClosed(){
		return  dbService.getAllAnagStatiSessioneNotClosed();
	}

	public TabAnagRitardi getAnagRitardiByCodiAnagRitardo(Integer codiAnagRitardo){
		return dbService.getAnagRitardiByCodiAnagRitardo(codiAnagRitardo);
	}

	public void updateRitardi(TabAnagRitardi tabAnagRitardi,CurrentUser currentUser)throws Exception{
		dbService.updateRitardi(tabAnagRitardi);
	}

	public List<TabCostrModAntenna> getAllCostrModAntenna(){
		return dbService.getAllCostrModAntenna();
	}

	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByCodiOggetto(Integer codiOggetto)
	{
		return dbService.getRelCascadeCarattLovByCodiOggetto(codiOggetto);
	}

	public List<TabRelCascadeCarattLov> getRelCascadeCarattLovByPrimaryCaratt(Integer codiOggetto, Integer codiCarattPrimary, Integer codiAnagLovPrimary)
	{
		return dbService.getRelCascadeCarattLovByPrimaryCaratt(codiOggetto, codiCarattPrimary, codiAnagLovPrimary);
	}
}
