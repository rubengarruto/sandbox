package com.rextart.saw.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.CarDAOImpl;
import com.rextart.saw.miaSezione.myBeans.Car;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class CarService {

	private CarDAOImpl carDAO = new CarDAOImpl();
	private Car tempCar;
	

	public List<Car> getAllCars() {
		return carDAO.getAllCars();
	}

	public void addCar(Car car) {
		carDAO.addCar(car);
	}

	public void createCar(Car car) {
		tempCar = car;
	}

	public void saveCar() {
		carDAO.addCar(tempCar);
	}
	
	public void removeCar(Car selectedCar) {
		
		carDAO.removeCar(selectedCar);
		
	}

}
