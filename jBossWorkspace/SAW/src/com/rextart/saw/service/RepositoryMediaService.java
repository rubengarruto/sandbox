package com.rextart.saw.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.RepositoryMediaDaoImpl;
import com.rextart.saw.entity.TabRepositoryDocument;
import com.rextart.saw.entity.TabRepositoryMedia;
import com.rextart.saw.entity.TabRepositorySchedaRischi;
import com.rextart.saw.entity.TabSchedaDyn;



@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class RepositoryMediaService {
	
	@Autowired
	 RepositoryMediaDaoImpl repositoryMedia;
	
	public Object[] saveRepositoryMedia(TabRepositoryMedia tabRepositoryMedia){
		List<TabRepositoryMedia> listMedia = repositoryMedia.getRepTabMediaByFilePath(tabRepositoryMedia.getDescPath());
		
		if(listMedia.size()>0){
			return repositoryMedia.saveRepositoryMedia(listMedia.get(0), false);
		}else{
			return repositoryMedia.saveRepositoryMedia(tabRepositoryMedia, true);
		}
		 
	 }
	
	
	public Object[]  saveRepositoryDocument(TabRepositoryDocument tabRepositoryDocument) {
		List<TabRepositoryDocument> listDoc = new ArrayList<TabRepositoryDocument>();
		if(!tabRepositoryDocument.isFlagD1())
			listDoc = repositoryMedia.getRepDocExist(tabRepositoryDocument.getCodiSessione(), tabRepositoryDocument.getDescNome());
		else
			listDoc = repositoryMedia.getRepDocD1Exist(tabRepositoryDocument.getCodiSessione());
			
		if(listDoc.size()>0){
			TabRepositoryDocument tabUpdate = listDoc.get(0);
			tabUpdate.setDescNome(tabRepositoryDocument.getDescNome());
			tabUpdate.setDescPath(tabRepositoryDocument.getDescPath());
			tabUpdate.setTimeCreate(tabRepositoryDocument.getTimeCreate());
			tabUpdate.setFlagD1(tabRepositoryDocument.isFlagD1());
			tabUpdate.setDescMimeType(tabRepositoryDocument.getDescMimeType());
			tabUpdate.setTipoFile(tabRepositoryDocument.getTipoFile());
			return repositoryMedia.saveRepositoryDocument(tabUpdate, false);
		}else{
			return repositoryMedia.saveRepositoryDocument(tabRepositoryDocument, true);
		}
	}
	
	public void removeDocumentDyn(Integer codiSessione) throws Exception {
		repositoryMedia.removeRepositorySchedaRischi(codiSessione);
	}
	public Object[]  saveRepositorySchedaRischi(TabRepositorySchedaRischi tabRepositorySchedaRischi) {
			return repositoryMedia.saveRepositorySchedaRischi(tabRepositorySchedaRischi);
		}
	
	
	public TabRepositoryDocument getTabRepositoryDocumentVerbaleD1(Integer codiSessione){
		return  repositoryMedia.getTabRepositoryDocumentVerbaleD1(codiSessione);
	}
	
	public List<TabRepositoryDocument> getRepositoryDocumentByCodiSessione(Integer codiSessione){
		return repositoryMedia.getRepositoryDocumentByCodiSessione(codiSessione);
	}
	
	public List<TabRepositoryMedia> getRepositoryMediaByCodiSessione(Integer codiSessione){
		return repositoryMedia.getRepositoryMediaByCodiSessione(codiSessione);
	}
	
	public TabRepositorySchedaRischi getTabRepositorySchedaRischi(Integer codiSessione){
		return repositoryMedia.getTabRepositorySchedaRischi(codiSessione);
	}
	
	public List<TabRepositorySchedaRischi> getRepositoryDocumentDynByCodiSessione(Integer codiSessione){
		return repositoryMedia.getRepositoryDocumentDynByCodiSessione(codiSessione);
	}
	public TabSchedaDyn getTabSchedaDyn(Integer codiSessione){
		return repositoryMedia.getTabSchedaDyn(codiSessione);
	}
	public Object[] saveSchedaDyn(TabSchedaDyn tabSchedaDyn){
		return repositoryMedia.saveSchedaDyn(tabSchedaDyn);
	}
	public Object[] updateSchedaDyn(TabSchedaDyn tabSchedaDyn){
		return repositoryMedia.updateSchedaDyn(tabSchedaDyn);
	}
	
	public List<TabRepositoryDocument> getTabRepositoryDocumentByName(int codiSessione, String descNome) {
		return repositoryMedia.getRepDocExist(codiSessione, descNome);
	}
	
	public List<TabRepositoryDocument> getTabRepositoryDocumentByTipo(int codiSessione, Integer tipoFile) {
		return repositoryMedia.getRepDocExistBytipo(codiSessione, tipoFile);
	}
	
	public List<TabRepositoryDocument> getRepDocD1Exist(Integer codiSessione){
		return repositoryMedia.getRepDocD1Exist(codiSessione);
	}
	
	public void removeDocumentD1(Integer codiSessione) throws Exception {
		repositoryMedia.removeRepositoryD1(codiSessione);
	}
	
	public List<TabRepositoryMedia> getAllBySessioneAndCategoria(Integer codiSessione, String descCategoria){
		return repositoryMedia.getAllBySessioneAndCategoria(codiSessione, descCategoria);
	}
	
	public List<TabRepositoryMedia> getFiveRepMediaBySessione(Integer codiSessione){
		return repositoryMedia.getFiveRepMediaBySessione(codiSessione);
	}
	
	public TabRepositoryDocument getDocumentBySessioneAndNome(Integer codiSessione, String descNome){
		return repositoryMedia.findDocBySessioneAndNome(codiSessione, descNome);
	}
}
