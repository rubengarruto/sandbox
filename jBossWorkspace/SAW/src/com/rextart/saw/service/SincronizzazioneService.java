package com.rextart.saw.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.rextart.saw.dao.ErrorDeviceDao;
import com.rextart.saw.dao.LogOperationDaoImpl;
import com.rextart.saw.dao.SessioneDaoImpl;
import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.dao.UserDaoImpl;
import com.rextart.saw.entity.TabErrorDevice;
import com.rextart.saw.entity.TabNote;
import com.rextart.saw.entity.TabNoteCaratteristiche;
import com.rextart.saw.entity.TabOggetti;
import com.rextart.saw.entity.TabRegioni;
import com.rextart.saw.entity.TabRelRitardiSessioni;
import com.rextart.saw.entity.TabRelSessioneStatiVerbale;
import com.rextart.saw.entity.TabRiferimentiFirma;
import com.rextart.saw.entity.TabRisultatoSurvey;
import com.rextart.saw.entity.TabSurveyDettaglio;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwUtenti;
import com.rextart.saw.entity.ViwVerbale;
import com.rextart.saw.rs.bean.request.ErrorDeviceBean;
import com.rextart.saw.rs.bean.request.SincronizzazioneBean;
import com.rextart.saw.rs.bean.request.SurveyDettaglioBean;
import com.rextart.saw.rs.utility.DataConverter;
import com.rextart.saw.rs.utility.UtilLog;
import com.rextart.saw.utility.Const;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class SincronizzazioneService {

	@Autowired
	SincronizzazioneDao sincronizzazioneDao;

	@Autowired
	 SurveyService surveyService;
	
	@Autowired
	SessioneDaoImpl sessionidao;

	@Autowired
	UserDaoImpl userdao;

	@Autowired
	SessioniService sessioniService;

	@Autowired
	ErrorDeviceDao errorDeviceDao;

	@Autowired
	LogOperationDaoImpl operTrace;

	@Autowired
	ApplicationMailer mailer;

	@Autowired
	MessageSource messages;

	@Autowired
	private VelocityEngine velocityEngine;

	@Autowired
	ReportService reportService;

	public void saveSurveyDettaglio(SurveyDettaglioBean surveyDettaglioBean)
	{
		TabSurveyDettaglio tabSurveyDettaglio = DataConverter.getTabSurveyDettaglio(surveyDettaglioBean);
		if(surveyDettaglioBean.getCodiDettaglio() != null)
		{
			tabSurveyDettaglio.setCodiDettaglio(surveyDettaglioBean.getCodiDettaglio());
			sincronizzazioneDao.updateTabSurveyDettaglio(tabSurveyDettaglio);
		}
		else sincronizzazioneDao.saveTabSurveyDettaglio(tabSurveyDettaglio);
	}
	
//	TODO ottimizzare salvando/aggiornando pi� record alla volta
	public void saveSurveyDettaglioLst(List<SurveyDettaglioBean> surveyDettaglioBeanLst)
	{
		for(SurveyDettaglioBean surveyDettaglioBean : surveyDettaglioBeanLst) 
			saveSurveyDettaglio(surveyDettaglioBean);
	}

	/**
	 * metodo per il salvataggio totale di tutte le tabelle necessarie per
	 * SincronizzazioneRS
	 * 
	 * @throws Exception
	 **/
	public void saveSincronizzazione(SincronizzazioneBean sincronizzazioneBean) throws Exception {

		TabSurveyDettaglio tabSurveyDettaglio = null;

		List<SurveyDettaglioBean> surveyDettaglio = sincronizzazioneBean.getSurveyDettaglioBeans();
		
		// operazioni di delete
		if(sincronizzazioneBean.getListCodiSurveyAll() != null){
			UtilLog.printSurveyDettCanc(sincronizzazioneBean.getListCodiSurveyAll());
			
			for (Integer codiSurvey : sincronizzazioneBean.getListCodiSurveyAll()) {
				sincronizzazioneDao.deleteTabSurveyDettaglioByCodiSurvey(codiSurvey);
			}
		}
		
		for (SurveyDettaglioBean surveyDettaglioBean : surveyDettaglio) {
			tabSurveyDettaglio= new TabSurveyDettaglio();
			tabSurveyDettaglio= DataConverter.getTabSurveyDettaglio(surveyDettaglioBean);

			sincronizzazioneDao.saveTabSurveyDettaglio(tabSurveyDettaglio);
		}
		
		List<TabRiferimentiFirma> listFirma = DataConverter.getTabRiferimentiFirma(sincronizzazioneBean.getRiferimentiFirmaBeans());
		for(TabRiferimentiFirma riferimenti : listFirma){

			sincronizzazioneDao.saveOrUpdateRiferimentiFirma(riferimenti);
		}

		List<TabRisultatoSurvey> listRisultato = DataConverter.getTabRisultatoSurvey(sincronizzazioneBean.getRisultatoSurveyBeans());
		ArrayList<Integer>oggettiList = new ArrayList<>();

		for(TabRisultatoSurvey entity : listRisultato){
			List<TabRisultatoSurvey> listRisultatiApparato = (List<TabRisultatoSurvey>)surveyService.getListRisultatoByCodiSessioneAndCodiOggetto(sincronizzazioneBean.getCodiSessione(), sincronizzazioneBean.getCodiOggetto());
			Boolean flagUpdateStato=true;
			TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(entity.getCodiOggetto(),entity.getCodiSessione());
			if(tabRelSessioneStatiVerbale==null){
				tabRelSessioneStatiVerbale= new TabRelSessioneStatiVerbale();
				tabRelSessioneStatiVerbale.setCodiOggetto(entity.getCodiOggetto());
				tabRelSessioneStatiVerbale.setCodiSessione(entity.getCodiSessione());
				tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_NON_LAVORATO);
				flagUpdateStato=false;
			}
			if(entity.getCodiOggetto() != Const.ALLEGATO_NOKIA && entity.getCodiOggetto() != Const.ALLEGATO_HUAWEI && entity.getCodiOggetto() != Const.ALLEGATO_ERICSSON){
				if(entity.getCodiValoreRisultatoSurvey()!=null && entity.getCodiValoreRisultatoSurvey()==Const.NEGATIVO_INT){
					tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_NEGATIVO);
					sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
				}else if(entity.getCodiValoreRisultatoSurvey()!=null && entity.getCodiValoreRisultatoSurvey()==Const.POSITIVO_CON_RISERVA_INT && !tabRelSessioneStatiVerbale.getCodiStatoVerbale().equals(Const.VERBALE_NEGATIVO)){
					tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.POSITIVO_CON_RISERVA_INT);
					sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
				}else if(!tabRelSessioneStatiVerbale.getCodiStatoVerbale().equals(Const.VERBALE_NEGATIVO) && !tabRelSessioneStatiVerbale.getCodiStatoVerbale().equals(Const.POSITIVO_CON_RISERVA_INT)&& !tabRelSessioneStatiVerbale.getCodiStatoVerbale().equals(Const.POSITIVO_INT)){
					tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_NON_LAVORATO);
					sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
				}

			}else if(listRisultatiApparato.size()==Const.ALL_RESULTS_SURVEY){
				boolean risultatoPositivoConRiserva = false;
				boolean risultatoPositivo = false;
				boolean allResults = true;
				for(TabRisultatoSurvey tabRisultatoSurvey : listRisultatiApparato){
					if(tabRisultatoSurvey.getCodiSurvey().equals(entity.getCodiSurvey())){
						allResults = false;
						break;
					}
				}
				if(allResults){
					listRisultatiApparato.add(entity);
					for(TabRisultatoSurvey tabRisultatoSurvey : listRisultatiApparato){
						if(tabRisultatoSurvey.getCodiValoreRisultatoSurvey()==Const.NEGATIVO_INT){
							risultatoPositivoConRiserva=false;
							risultatoPositivo = false;
							tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.VERBALE_NEGATIVO);
							sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
							break;
						}else if(tabRisultatoSurvey.getCodiValoreRisultatoSurvey()==Const.POSITIVO_CON_RISERVA_INT)	{
							risultatoPositivoConRiserva = true;
							risultatoPositivo = false;
						}else if(tabRisultatoSurvey.getCodiValoreRisultatoSurvey()==Const.POSITIVO_INT){
							risultatoPositivo = true;
						}
					}
					if(risultatoPositivoConRiserva){
						tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.POSITIVO_CON_RISERVA_INT);
						sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
					} else if(risultatoPositivo){
						tabRelSessioneStatiVerbale.setCodiStatoVerbale(Const.POSITIVO_INT);
						sincronizzazioneDao.saveRelSessioniStatiVerbale(tabRelSessioneStatiVerbale, flagUpdateStato);
					}
				}
			}
			sincronizzazioneDao.saveRisultatoSurvey(entity);
			if(!oggettiList.contains(entity.getCodiOggetto())){
				oggettiList.add(entity.getCodiOggetto());
			}

		}
		for (Integer integer : oggettiList) {
			sincronizzazioneDao.isVerbaleAllPositive(integer, sincronizzazioneBean.getCodiSessione());
		}
		List<TabNoteCaratteristiche> listNoteCaratteristiche = DataConverter.getTabNoteCaratteristicheEntity(sincronizzazioneBean.getNoteCaratteristicheBeans());
		for(TabNoteCaratteristiche entity : listNoteCaratteristiche){
			sincronizzazioneDao.saveNoteCaratteristiche(entity);
		}

		List<TabNote> listTabNote = DataConverter.getTabNoteEntity(sincronizzazioneBean.getListNoteBeans());
		for(TabNote entity : listTabNote){
			sincronizzazioneDao.saveNoteWebFromTablet(entity);
		}


		List<TabRelRitardiSessioni> listRelRitartiSessioni = DataConverter.getRelRitardiSessioniEntity(sincronizzazioneBean.getRelRitardiSessioniBeans());
		for(TabRelRitardiSessioni entity : listRelRitartiSessioni){
			sincronizzazioneDao.saveRelSessioneRitardo(entity);
		}

		TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(sincronizzazioneBean.getCodiOggetto(),sincronizzazioneBean.getCodiSessione());
		if(tabRelSessioneStatiVerbale != null && sincronizzazioneBean.isLastCall()){
			if(tabRelSessioneStatiVerbale.getCodiStatoVerbale() == 1 || tabRelSessioneStatiVerbale.getCodiStatoVerbale() == 2 || tabRelSessioneStatiVerbale.getCodiStatoVerbale() == 3){
				emailChiusuraSessione(tabRelSessioneStatiVerbale);

			}
		}

		if(sincronizzazioneDao.isExistCollaudoAllResults(sincronizzazioneBean.getCodiSessione()) && sincronizzazioneBean.isLastCall()){			
			try {
				sessionidao.changeStatoSessione(sincronizzazioneBean.getCodiSessione(), sessionidao.getStateChiuso().getCodiStato());
				ViwSessioni selectedSessione = sessionidao.findViwSessioni(sincronizzazioneBean.getCodiSessione());
				// In To tutti gli associati alla sessione
				List<ViwUtenti> listViwUtenti = userdao
						.getUtentiAssociatiByCodiSessione(sincronizzazioneBean.getCodiSessione());
				List<String> userTo = new ArrayList<String>();
				for (ViwUtenti listUtentiFiltered : listViwUtenti) {
					if(listUtentiFiltered.getFlgCessSosp()==false && listUtentiFiltered.getFlgReceiveEmail()==true){
						userTo.add(listUtentiFiltered.getDescEmail());
					}
				}

				// In Cc tutti i referenti territoriali e coordinatori relativi
				// alla stessa area di competenza della sessione
				//				listViwUtenti = userdao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
				TabRegioni regioneByName = sessioniService.getIdRegioneByNome(selectedSessione.getDescRegione());
				List<Integer> listCodiUtenti = userdao.getUtentiByRegione(regioneByName.getIdRegione());		
				listViwUtenti = userdao.getUserRefTerAndCordByRegione(listCodiUtenti);
				List<String> userCc = new ArrayList<String>();
				for (ViwUtenti listUtentiFiltered : listViwUtenti) {
					userCc.add(listUtentiFiltered.getDescEmail());
				}


				List<ViwVerbale> verbali = sessioniService.getListVerbaliByCodiSessione(selectedSessione.getCodiSessione());
				Integer esitoSessione = 1;
				for (ViwVerbale viwVerbale : verbali) {
					if(viwVerbale.getCodiStatoVerbale()==3) {
						esitoSessione = 3;
						break;
					}
					if(viwVerbale.getCodiStatoVerbale()==2) {
						esitoSessione=2;
					}
				}


				String esitoP;
				String esitoPcr;
				String esitoN;

				switch (esitoSessione) {
				case 1:
					esitoP = Const.ESITO_POSITIVO;
					esitoPcr = "";
					esitoN = "";
					break;
				case 2:
					esitoPcr = Const.ESITO_POSITIVO_CON_RISERVA;
					esitoP = "";
					esitoN = "";
					break;
				case 3:
					esitoN = Const.ESITO_NEGATIVO;
					esitoP = "";
					esitoPcr = "";
					break;
				default:
					esitoP = "";
					esitoPcr = "";
					esitoN = "";
					break;
				}

				if (userTo != null && userTo.size() > 0) {
					String object = messages.getMessage("sessioneArchiviataObj",
							new Object[] { selectedSessione.getDescCodice().toUpperCase()}, Const.locale);
					String body = messages.getMessage("sessioneArchiviataBody",new Object[] { selectedSessione.getCodiCollaudo(), selectedSessione.getDescCodice().toUpperCase(),
							selectedSessione.getDescNomeSito(),  esitoP, esitoPcr, esitoN},
							Const.locale);
					String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
							mailer.templateMail(body));
					mailer.sendMail(userTo, userCc, object, template);
				}
			} catch (Exception e) {
			}
			UtilLog.printControllaSurveyRisultati(sincronizzazioneBean.getCodiSessione(), true);
		}else{
			UtilLog.printControllaSurveyRisultati(sincronizzazioneBean.getCodiSessione(), false);
		}


		if (sincronizzazioneBean.isLastCall()) {
			sessioniService.lockUnLockDoc(sincronizzazioneBean.getCodiSessione(), sincronizzazioneBean.getCodiOggetto(),
					sincronizzazioneBean.getCodiUtente(), sincronizzazioneBean.isFlagLock());

			// sessionidao.lockUnLockDoc(sincronizzazioneBean.getCodiSessione(),
			// sincronizzazioneBean.getCodiOggetto(),
			// sincronizzazioneBean.getCodiUtente(),
			// sincronizzazioneBean.isFlagLock());
		}

		// Gestione chiusura parziale utente ditta
		// viene eseguito solo all'ULTIMA CHIAMATA se l'utente ditta ha chiuso
		// il suo collaudo
		if (sincronizzazioneBean.isFlgParziale() && sincronizzazioneBean.isLastCall()) {
			Object[] result = sincronizzazioneDao.saveRelSessVerbaleParziale(sincronizzazioneBean.getCodiOggetto(),
					sincronizzazioneBean.getCodiSessione(), true);
			if ((boolean)result[0]) {
				try {
					ViwSessioni selectedSessione = sessionidao.findViwSessioni(sincronizzazioneBean.getCodiSessione());
					byte[] attachment = null;
					switch (sincronizzazioneBean.getCodiOggetto()) {
					case 1:
						attachment = reportService.printVerbaleRadiantiA(selectedSessione);
						break;
					case 2:
						attachment = reportService.printVerbaleRadiantiB(selectedSessione);
						break;
					case 3:
						attachment = reportService.printVerbaleRadiantiC(selectedSessione);
						break;
					case 4:
						attachment = reportService.printVerbaleRadiantiD(selectedSessione);
						break;
					case 5:
						attachment = reportService.printApparatoNokia(selectedSessione);
						break;
					case 6:
						attachment = reportService.printApparatoEricsson(selectedSessione);
						break;
					case 7:
						attachment = reportService.printApparatoHuawei(selectedSessione);
						break;
					case 8:
						attachment = reportService.printVerbaleRadiantiE(selectedSessione);
						break;
					}

					// TabUtente utente =
					// userdao.getUserById(sincronizzazioneBean.getCodiUtente());
					// List<String> userTo=new ArrayList<String>();
					// userTo.add(utente.getDescEmail());

					// //Controllo su TabRelSessioneUtenti gli utenti che sono
					// associati alla sessione corrente e li inserisco in una
					// lista
					// List<TabRelSessioneUtenti> utentiRelSes =
					// userdao.getAllUtentiAssociatiByCodiSessione(selectedSessione.getCodiSessione());
					// List<Integer> listCodiUtenti = new ArrayList<Integer>();
					// for(TabRelSessioneUtenti listRelSesUtenti :
					// utentiRelSes){
					// listCodiUtenti.add(listRelSesUtenti.getCodiUtente());
					// }
					//
					// //Filtro gli utenti presi dalla lista per codice ruolo e
					// prendo le loro email inserendole nella lista per inviarle
					// in CC
					// List<ViwUtenti> listViwUtenti =
					// sessionidao.getAllUserMailFineCollaudo(listCodiUtenti);
					// List<String> userCc = new ArrayList<String>();
					// for(ViwUtenti listUtentiFiltered : listViwUtenti){
					// if(!userCc.contains(userTo))
					// userCc.add(listUtentiFiltered.getDescEmail());
					// }

					// In To tutti gli associati alla sessione
					List<ViwUtenti> listViwUtenti = userdao
							.getUtentiAssociatiByCodiSessione(selectedSessione.getCodiSessione());
					List<String> userTo = new ArrayList<String>();
					for (ViwUtenti listUtentiFiltered : listViwUtenti) {
						if(listUtentiFiltered.getFlgCessSosp()==false && listUtentiFiltered.getFlgReceiveEmail()==true){
							userTo.add(listUtentiFiltered.getDescEmail());
						}
					}

					// In Cc tutti i referenti territoriali e coordinatori
					// relativi alla stessa area di competenza della sessione
					//					listViwUtenti = userdao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
					TabRegioni regioneByName = sessioniService.getIdRegioneByNome(selectedSessione.getDescRegione());
					List<Integer> listCodiUtenti = userdao.getUtentiByRegione(regioneByName.getIdRegione());		
					List<ViwUtenti> utentiInCC = userdao.getUserRefTerAndCordByRegione(listCodiUtenti);


					List<String> userCc = new ArrayList<String>();
					for (ViwUtenti listUtentiFiltered : utentiInCC) {
						if(listUtentiFiltered.getFlgReceiveEmail()==true){
							userCc.add(listUtentiFiltered.getDescEmail());
						}
					}

					if (userTo != null && userTo.size() > 0) {
						TabOggetti tabOggetti = null;
						tabOggetti = sincronizzazioneDao.findTabOggetti(sincronizzazioneBean.getCodiOggetto());
						String object = messages.getMessage("chiusuraParzialeVerbaleObj",
								new Object[] { selectedSessione.getDescCodice().toUpperCase(), tabOggetti.getDescOggetto()}, Const.locale);
						String body = messages.getMessage("chiusuraParzialeVerbaleBody",
								new Object[] { tabOggetti.getDescOggetto(), selectedSessione.getDescNomeSito(),
								selectedSessione.getDescCodice().toUpperCase() },
								Const.locale);
						String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm",
								mailer.templateMail(body));
						mailer.sendMailAttachmentByte(userTo, userCc, object, template, attachment,
								tabOggetti.getDescOggetto()+".pdf");
					}
				} catch (Exception e) {
				}

			}

		}
	}

	//Fine gestione chiusura parziale utente ditta

	public void deleteSurveyDettaglio(List<SurveyDettaglioBean> listSurveyDettaglioBean){
		Integer codiSurveyDeleted = 0;
		for (SurveyDettaglioBean surveyDettaglioBean : listSurveyDettaglioBean) {
			if(surveyDettaglioBean.getCodiSurvey()!=0 && codiSurveyDeleted!=surveyDettaglioBean.getCodiSurvey()){
				codiSurveyDeleted = surveyDettaglioBean.getCodiSurvey();
				sincronizzazioneDao.deleteTabSurveyDettaglioByCodiSurvey(surveyDettaglioBean.getCodiSurvey());
			}
		}
	}


	public void deleteSaveErroriDevice(List<ErrorDeviceBean> listErrorDeviceBean, Integer codiUtente, String descImei){
		TabErrorDevice tabErrorDevice = null;
		errorDeviceDao.deleteByCodiUtenteImei(codiUtente, descImei);

		if(listErrorDeviceBean!=null){
			for (ErrorDeviceBean errorDeviceBean : listErrorDeviceBean) {
				if(!errorDeviceDao.isExistError(errorDeviceBean.getCodiCliente(), errorDeviceBean.getNumeVersionApp(), errorDeviceBean.getDescErrore())){
					tabErrorDevice = new TabErrorDevice();
					tabErrorDevice = DataConverter.getErrorDeviceEntity(errorDeviceBean);
					errorDeviceDao.save(tabErrorDevice);
				}
			}
		}
	}





	public void logOperTrace(Integer codiUtente, String descImei) {
		operTrace.logOperation(Const.SYNC_DEVICE, new Timestamp(System.currentTimeMillis()), codiUtente, descImei);

	}

	public void saveRiferimentiFirma(TabRiferimentiFirma tabRiferimentiFirma){
		sincronizzazioneDao.saveRiferimentiFirma(tabRiferimentiFirma);
	}

	public boolean isD1EsitoLibero(Integer codiSessione, Integer codiOggetto){
		if(codiOggetto == Const.ALLEGATO_D){
			TabRelSessioneStatiVerbale listaOggettiEsiti = sincronizzazioneDao.isRelSessioneStatiVerbaleExist(Const.ALLEGATO_D1, codiSessione);
			if(listaOggettiEsiti == null){
				listaOggettiEsiti = new TabRelSessioneStatiVerbale();
				listaOggettiEsiti.setCodiOggetto(codiOggetto);
				listaOggettiEsiti.setCodiSessione(codiSessione);
				listaOggettiEsiti.setCodiStatoVerbale(Const.VERBALE_NON_LAVORATO);
			}
			if(listaOggettiEsiti.getCodiStatoVerbale() == null || listaOggettiEsiti.getCodiStatoVerbale() == Const.VERBALE_NON_LAVORATO){
				return true;
			}
			return false;	
		}
		return true;

	}

	// controlli per verificare se � possibile effettuare la sincronizzazione
	public boolean isSincronizzaAvaible(Integer codiSessione){
		if(sessioniService.isSessioneStatoInLavorazione(codiSessione) 
				|| sessioniService.isSessioneStatoAnnullato(codiSessione)){
			return true;
		}
		return false;	
	}



	public boolean isExistCollaudoAllResults(Integer codiSessione){
		return sincronizzazioneDao.isExistCollaudoAllResults(codiSessione);
	}

	public boolean isOggettoOwner(Integer codiSessione, Integer codiOggetto, Integer codiUtenteLocking){
		return sincronizzazioneDao.isOggettoOwner(codiSessione, codiOggetto, codiUtenteLocking);
	}

	public ViwVerbale getViwVerbaleBySessioneOggetto(Integer codiSessione,Integer codiOggetto) {
		return sincronizzazioneDao.getViwVerbaleBySessioneOggetto(codiSessione,codiOggetto);
	}

	public void emailChiusuraSessione(TabRelSessioneStatiVerbale tabRelSessioneStatiVerbale){
		try{
			ViwSessioni selectedSessione = sessionidao.findViwSessioni(tabRelSessioneStatiVerbale.getCodiSessione());
			String descOggettoObject = "";
			String descOggettoBody = "";
			String esito;
			String positivo = "";
			String positivoConRiserva = "";
			String negativo = "";
			TabOggetti tabOggetti = sincronizzazioneDao.findTabOggetti(tabRelSessioneStatiVerbale.getCodiOggetto());
			switch (tabRelSessioneStatiVerbale.getCodiStatoVerbale()){
			case Const.VERBALE_POSITIVO: 
				esito = "Positivo";
				positivo = esito.toUpperCase();
				break;
			case Const.VERBALE_POSITIVO_CON_RISERVA:
				esito = "Positivo Con Riserva";
				positivoConRiserva = esito.toUpperCase();
				break;
			case Const.VERBALE_NEGATIVO:
				esito = "Negativo";
				negativo = esito.toUpperCase();
				break;
			default:
				esito = "";
				break;
			}
			if(tabOggetti.getCodiOggetto()==Const.ALLEGATO_NOKIA || tabOggetti.getCodiOggetto()==Const.ALLEGATO_HUAWEI || tabOggetti.getCodiOggetto()==Const.ALLEGATO_ERICSSON){
				descOggettoObject = "Apparato " + tabOggetti.getDescSigla();
				descOggettoBody = "apparato: " + tabOggetti.getDescSigla();
			} else {
				descOggettoObject = "Allegato: " + tabOggetti.getDescSigla();
				descOggettoBody = "allegato: " + tabOggetti.getDescSigla();
			}
			String object = messages.getMessage("verbaleConclusoObj", new Object[] {selectedSessione.getDescCodice().toUpperCase(), descOggettoObject, esito.toUpperCase()}, Const.locale);
			String body =messages.getMessage("verbaleConclusoBody", new Object[] {descOggettoBody,selectedSessione.getDescCodice().toUpperCase(),selectedSessione.getDescNomeSito(),positivo,positivoConRiserva,negativo}, Const.locale);
			String template = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mailTemplate.vm", mailer.templateMail(body));

			List<ViwUtenti> userAssoc = userdao.getUtentiAssociatiByCodiSessione(tabRelSessioneStatiVerbale.getCodiSessione());
			List<String> userTo = new ArrayList<String>();
			for(ViwUtenti user : userAssoc){
				if(user.getFlgCessSosp()==false && user.getFlgReceiveEmail()==true){
					userTo.add(user.getDescEmail());
				}
			}
			//			List<ViwUtenti> utentiInCC = userdao.getUserRefTerAndCordByAreaComp(selectedSessione.getCodiAreaComp());
			TabRegioni regioneByName = sessionidao.getIdRegioneByNome(selectedSessione.getDescRegione());
			List<Integer> listCodiUtenti = userdao.getUtentiByRegione(regioneByName.getIdRegione());		
			List<ViwUtenti> utentiInCC = userdao.getUserRefTerAndCordByRegione(listCodiUtenti);

			List<String> userCc=new ArrayList<String>();
			for (ViwUtenti viwUtenti : utentiInCC) {
				if(viwUtenti.getFlgReceiveEmail()==true){
					userCc.add(viwUtenti.getDescEmail());
				}
			}
			mailer.sendMail(userTo, userCc, object, template);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void changeStatoSessione(Integer codiSessione, Integer codiNewStato){
		sessionidao.changeStatoSessione(codiSessione, codiNewStato);
	}
	
	public String findDescOggettoByCodiOggetto(Integer codiOggetto){
		if(codiOggetto!=null){
			TabOggetti tabOggeto = sincronizzazioneDao.findTabOggetti(codiOggetto);
			if(tabOggeto!=null){
				return tabOggeto.getDescOggetto();
			}
		}
		return "";
	}
}
