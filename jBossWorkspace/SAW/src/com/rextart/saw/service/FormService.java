package com.rextart.saw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.FormDao;
import com.rextart.saw.entity.ViwAnagAntenneTmaDiplexer;
import com.rextart.saw.entity.ViwAnagLovCaratt;
import com.rextart.saw.entity.ViwElemCarattWOrder;
import com.rextart.saw.entity.ViwElementiCaratteristiche;
import com.rextart.saw.entity.ViwElementiOggetti;
import com.rextart.saw.entity.ViwTipiOggetti;
import com.rextart.saw.utility.Const;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class FormService {
	
	@Autowired
	private FormDao formDao;
	
	@Autowired
	private MessageSource messages;

	public List<ViwTipiOggetti> getAllTipologieOggetti()
	{
		return formDao.getAllTipiOggetti();
	}
	
	public List<ViwTipiOggetti> getTipologieByOggetto(Integer codiOggetto)
	{
		return formDao.getTipiByOggetto(codiOggetto);
	}
	
	public List<ViwElementiOggetti> getAllElementiOggetti()
	{
		return formDao.getAllElementiOggetti();
	}
	
	public List<ViwElementiOggetti> getElementiByTipo(Integer codiTipoOggetto)
	{
		return formDao.getElementiByTipoOggetto(codiTipoOggetto);
	}
	
	public List<ViwElementiCaratteristiche> getAllElementiCaratteristiche()
	{
		return formDao.getAllElementiCaratteristiche();
	}
	
	public List<ViwElementiCaratteristiche> getCaratteristicheByElemento(Integer codiElemento)
	{
		return formDao.getCaratteristicheByElemento(codiElemento);
	}
	
	public List<ViwElemCarattWOrder> getAllElementiCaratteristicheWithOrder()
	{
		return formDao.getAllElementiCaratteristicheWithOrder();
	}
	
	public List<ViwElemCarattWOrder> getCaratteristicheWithOrderByElemento(Integer codiElemento)
	{
		return formDao.getCaratteristicheWithOrderByElemento(codiElemento);
	}
	
	public List<ViwAnagLovCaratt> getAllAnagLovCaratteristiche()
	{
		return formDao.getAllAnagLovCaratt();
	}
	
	public List<ViwAnagLovCaratt> getAllAnagLovCarattCatalogo()
	{
		return formDao.getAllAnagLovCarattCatalogo();
	}
	
	public List<ViwAnagLovCaratt> getAnagLovByIdCaratteristica(Integer codiCaratteristica)
	{
		return formDao.getAnagLovByCaratt(codiCaratteristica);
	}
	
	public List<ViwAnagLovCaratt> getAnagLovByCaratteristica(Integer codiCaratteristica, String descLabel)
	{
		return formDao.getAnagLovByCaratt(codiCaratteristica, descLabel);
	}
	
	public List<ViwAnagAntenneTmaDiplexer> getAllAnagAntenneTmaDiplexer()
	{
		return formDao.getAllAnagAntenneTmaDiplexer();
	}
	
	public List<ViwAnagAntenneTmaDiplexer> getAnagAntenneTmaDiplexerByCodiCatalogo(Integer codiCatalogo)
	{
		return formDao.getAnagAntenneTmaDiplexerByCodiCatalogo(codiCatalogo);
	}
	
	public List<String> getDistinctDescMarcaByCodiCatalogo(Integer codiCatalogo)
	{
		return formDao.getDistinctDescMarcaByCodiCatalogo(codiCatalogo);
	}
		
	public List<ViwAnagAntenneTmaDiplexer> getModelloByDescMarca(Integer codiCatalogo, String descMarca)
	{
		return formDao.getModelloByDescMarca(codiCatalogo, descMarca);
	}
	
	public MessageSource getMessages() {
		return messages;
	}
	
	public void setMessages(MessageSource messages) {
		this.messages = messages;
	}
	
	public String getMessage(String propertyKey)
	{
		return messages.getMessage(propertyKey, new Object[]{},Const.locale);
	}
}
