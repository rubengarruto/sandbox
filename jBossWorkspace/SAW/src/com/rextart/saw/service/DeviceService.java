package com.rextart.saw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.DeviceDao;

@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class DeviceService {
	
	@Autowired
	private DeviceDao deviceDao;
	
	public boolean isDeviceExist(String descImei){
		return deviceDao.isDeviceExist(descImei);
	}
}
