package com.rextart.saw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.ApkDao;
import com.rextart.saw.entity.TabApk;


@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class ApkService {
	
	@Autowired
	ApkDao apkDao; 
	
	public TabApk getLastVersion(){
		return apkDao.getLastVersion();
	}
	
	public List<TabApk> getSqlFromVersion(int startVersion){
		return apkDao.getSqlFromVersion(startVersion);
	}
	
	public Integer getNumeLastVersion(){
		return apkDao.getNumeLastVersion();
	}

	public Object[] getScriptDbFromVersion(Integer numeVersione, Integer currentVersion) {
		return apkDao.getScriptDbFromVersion(numeVersione,currentVersion);
	}

}
