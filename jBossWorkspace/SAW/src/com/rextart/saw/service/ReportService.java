package com.rextart.saw.service;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rextart.saw.dao.ReportDao;
import com.rextart.saw.dao.SessioneDao;
import com.rextart.saw.dao.SincronizzazioneDao;
import com.rextart.saw.dao.SurveyDao;
import com.rextart.saw.entity.TabSurvey;
import com.rextart.saw.entity.TabWorkRequestColl;
import com.rextart.saw.entity.ViwApparatoEricsson;
import com.rextart.saw.entity.ViwApparatoHuawei;
import com.rextart.saw.entity.ViwApparatoNokia;
import com.rextart.saw.entity.ViwConfGenRadC;
import com.rextart.saw.entity.ViwEsitoSurvey;
import com.rextart.saw.entity.ViwMisureDiPimRadD;
import com.rextart.saw.entity.ViwMisureGenRadB;
import com.rextart.saw.entity.ViwProveFunzEseEricsson;
import com.rextart.saw.entity.ViwProveFunzSerEricsson;
import com.rextart.saw.entity.ViwRadiantiE;
import com.rextart.saw.entity.ViwRiferimentiFirma;
import com.rextart.saw.entity.ViwSessioni;
import com.rextart.saw.entity.ViwVerFunzAllarmi;
import com.rextart.saw.entity.ViwVerFunzAllarmiEricsson;
import com.rextart.saw.entity.ViwVerFunzMisureEricsson;
import com.rextart.saw.entity.ViwVerGenRadA;
import com.rextart.saw.entity.ViwVerInstWipmEricsson;
import com.rextart.saw.entity.ViwVerificheInstEricsson;
import com.rextart.saw.report.bean.AllarmiEsterni2GBean;
import com.rextart.saw.report.bean.AntenneInstallateRadiantiCBean;
import com.rextart.saw.report.bean.CelleRadiantiDBean;
import com.rextart.saw.report.bean.ConfigurazioneCelleRadiantiCBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletApparatoBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiABean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiBBean;
import com.rextart.saw.report.bean.DatiGeneraliTabletRadiantiDBean;
import com.rextart.saw.report.bean.DettaglioTecnologieRadiantiCBean;
import com.rextart.saw.report.bean.GestioneRetBean;
import com.rextart.saw.report.bean.MisureApogeoAntennaBean;
import com.rextart.saw.report.bean.MisureApogeoCelleBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneCalibrazioneRadiantiBBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneCondivisioneCalataBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneGeneraliRadiantiBBean;
import com.rextart.saw.report.bean.MisureDiCaratterizzazioneTipoCavoRadiantiBBean;
import com.rextart.saw.report.bean.MisureDtpValorePimSistemaRadiantiDBean;
import com.rextart.saw.report.bean.MisurePimSistemaRadiantiDBean;
import com.rextart.saw.report.bean.ProveDiServizioBean;
import com.rextart.saw.report.bean.RiferimentiBean;
import com.rextart.saw.report.bean.TestFlussiBean;
import com.rextart.saw.report.bean.VerFunzAllarmiEsterniBean;
import com.rextart.saw.report.bean.VerFunzMisureVerTestReportBean;
import com.rextart.saw.report.bean.VerInstWIPMBean;
import com.rextart.saw.report.bean.VerificaMHABean;
import com.rextart.saw.report.bean.VerificaRETBean;
import com.rextart.saw.report.bean.VerificaTMABean;
import com.rextart.saw.report.bean.VerificheFunzMisureCelleBean;
import com.rextart.saw.report.bean.VerificheFunzMisureCelleDoppiaLabelBean;
import com.rextart.saw.report.bean.VerificheGenCelleDoppiaLabelBean;
import com.rextart.saw.report.bean.VerificheGenChainingRETRadiantiABean;
import com.rextart.saw.report.bean.VerificheGenRadiantiABean;
import com.rextart.saw.report.bean.VerificheGenValoreAttesoRadiantiABean;
import com.rextart.saw.report.bean.VerificheInstTestFlussiE1EricssonBean;
import com.rextart.saw.report.bean.VerificheInstallazioneTensioniWIPMBean;
import com.rextart.saw.report.utility.UtilityReports;
import com.rextart.saw.utility.Const;
import com.rextart.saw.utility.ConstReport;




@Service
@Transactional(propagation = Propagation.REQUIRED, readOnly = false, rollbackFor = Exception.class)
public class ReportService {



	@Autowired
	private ReportDao reportDao;

	@Autowired
	private SessioneDao sessioneDao;

	@Autowired
	private SurveyDao surveyDao;
	
	@Autowired
	private SincronizzazioneDao sincronizzazioneDao;

	@Autowired
	private ServletContext servletContext;

	public byte[] printApparatoEricsson(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaEricsson(selectedSessione);
			attachment = printEricsson(jasperPrint, selectedSessione);
		
		return attachment;
	}

	//metodo che dovrebbe solo creare ma non fare il download 
	
	public byte[] generaEricsson(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaEricsson(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attach;
	}
	public JasperPrint creaEricsson(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint =null;
		try {
			//Collection necessaria da passare al metodo JasperPrint jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection); 
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );
			//

			String realpath = servletContext.getRealPath("/resources/reports/ericsson/VerbaleCollaudoApparatiERICSSON.jasper");
			List<ViwVerificheInstEricsson> arrayTitolo = reportDao.getListVerificheInstEricsson(selectedSessione.getCodiSessione());

			//LOGICA DATI GENERALI OA
			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_APPARATO);
			List<ViwApparatoEricsson> arrayDatiGeneraliTablet = reportDao.getListApparatoEricssonBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletEricsson);
			List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletBean = UtilityReports.getArrayTabletDatiGeneraliApparatoEricssonBean(arrayDatiGeneraliTablet);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
			BufferedImage firmaTimDatiGenerali=null;
			BufferedImage firmaDittaDatiGenerali=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimDatiGenerali = UtilityReports.getFirmaTim();
				firmaDittaDatiGenerali = UtilityReports.getFirmaDitta();
			} 
			//FINE LOGICA

			List<ViwVerificheInstEricsson> arrayTitoloWIPM = reportDao.getListVerificheInstEricsson(selectedSessione.getCodiSessione());
			List<ViwVerificheInstEricsson> arrayVerificheInst = reportDao.getListVerificheInstEricsson(selectedSessione.getCodiSessione());
			List<ViwEsitoSurvey> arrayEsitoVeriInst = reportDao.getListEsitoVeriInstEricsson(selectedSessione.getCodiSessione());
			//Riferimenti VeriInstEricsson
			List<RiferimentiBean> arrayyRiferimentiVeriInstEricssonBean= new ArrayList<>();
			if(arrayVerificheInst.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVeriInstEricsson = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificheInst.get(0).getCodiSurvey());
				arrayyRiferimentiVeriInstEricssonBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVeriInstEricsson);
			}
			BufferedImage firma_tim_VerIns=UtilityReports.getFirmaTim();
			BufferedImage firma_ditta_VerIns= UtilityReports.getFirmaDitta();

			//logica funz allarmi
			List<ViwVerFunzAllarmiEricsson> arrayVeriFunzAllarmi = reportDao.getListVerFunzAllarmiEricsson(selectedSessione.getCodiSessione());
			List<VerFunzAllarmiEsterniBean> arrayVeriFunzAllarmiBean = UtilityReports.getArrayAllarmiEriccson(arrayVeriFunzAllarmi);

			List<ViwVerificheInstEricsson> arrayTestFlussiE1 = new ArrayList<ViwVerificheInstEricsson>();
			VerificheInstTestFlussiE1EricssonBean verificheInstTestFlussiE1EricssonBean = new VerificheInstTestFlussiE1EricssonBean();
			List<VerificheInstTestFlussiE1EricssonBean> arrayverificheInstTestFlussiE1EricssonBean = new ArrayList<VerificheInstTestFlussiE1EricssonBean>();
			for (ViwVerificheInstEricsson viwVerificheInstEricsson : arrayVerificheInst) {

				if(viwVerificheInstEricsson.getCodiCaratteristica().equals(ConstReport.TestFlussiE1)){
					arrayTestFlussiE1.add(viwVerificheInstEricsson);
					verificheInstTestFlussiE1EricssonBean.setDescCaratteristica(viwVerificheInstEricsson.getDescCaratteristica());
					verificheInstTestFlussiE1EricssonBean.setDescElemento(viwVerificheInstEricsson.getDescElemento());
					verificheInstTestFlussiE1EricssonBean.setCodiElemento(viwVerificheInstEricsson.getCodiElemento());
					verificheInstTestFlussiE1EricssonBean.setDescNote(viwVerificheInstEricsson.getDescNote());
					if(viwVerificheInstEricsson.getNumeLivelloE()!=null){
						switch (viwVerificheInstEricsson.getNumeLivelloE()) {
						case 1:
							verificheInstTestFlussiE1EricssonBean.setDescLabel1(viwVerificheInstEricsson.getDescLabel());
							break;
						case 2:
							verificheInstTestFlussiE1EricssonBean.setDescLabel2(viwVerificheInstEricsson.getDescLabel());
							break;
						case 3:
							verificheInstTestFlussiE1EricssonBean.setDescLabel3(viwVerificheInstEricsson.getDescLabel());
							break;
						case 4:
							verificheInstTestFlussiE1EricssonBean.setDescLabel4(viwVerificheInstEricsson.getDescLabel());
							break;
						case 5:
							verificheInstTestFlussiE1EricssonBean.setDescLabel5(viwVerificheInstEricsson.getDescLabel());
							break;
						case 6:
							verificheInstTestFlussiE1EricssonBean.setDescLabel6(viwVerificheInstEricsson.getDescLabel());
							break;
						case 7:
							verificheInstTestFlussiE1EricssonBean.setDescLabel7(viwVerificheInstEricsson.getDescLabel());
							break;
						case 8:
							verificheInstTestFlussiE1EricssonBean.setDescLabel8(viwVerificheInstEricsson.getDescLabel());
							break;
						}
					} 
				}
			}
			arrayverificheInstTestFlussiE1EricssonBean.add(verificheInstTestFlussiE1EricssonBean);
			if(arrayTestFlussiE1.size()>0){
				arrayVerificheInst.removeAll(arrayTestFlussiE1);
			}


			/*LOGICA per estrarre Lista strumenti e componenti utilizzati codiElemento nella namedQuery 55 */
			//			HashMap<Integer, VerificheFunzMisureStrumentiBean> hashMap = new HashMap<>();
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureStrumenti = reportDao.getListVerFunzMisureStrumentiEricsson(selectedSessione.getCodiSessione());
			//			for (ViwVerFunzMisureEricsson viwVerFunzMisureEricsson : arrayVeriFunzMisureStrumenti) {
			//
			//				if(!hashMap.keySet().contains(viwVerFunzMisureEricsson.getCodiCaratteristica())){
			//					VerificheFunzMisureStrumentiBean verificheFunzMisureStrumentiBean = new VerificheFunzMisureStrumentiBean();
			//					verificheFunzMisureStrumentiBean.setDescElemento(viwVerFunzMisureEricsson.getDescElemento());
			//					verificheFunzMisureStrumentiBean.setDescCaratteristica(viwVerFunzMisureEricsson.getDescCaratteristica());
			//					if(viwVerFunzMisureEricsson.getDescLabel()!=null){
			//						if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Scadenza taratura"))
			//							verificheFunzMisureStrumentiBean.setDescScadenzaTaratura(viwVerFunzMisureEricsson.getDescValore());
			//						else
			//							verificheFunzMisureStrumentiBean.setDescNumeroSerie(viwVerFunzMisureEricsson.getDescValore());
			//					}
			//					hashMap.put(viwVerFunzMisureEricsson.getCodiCaratteristica(), verificheFunzMisureStrumentiBean);
			//				}else{
			//					if(viwVerFunzMisureEricsson.getDescLabel()!=null){
			//						if(viwVerFunzMisureEricsson.getDescLabel().equalsIgnoreCase("Scadenza taratura"))
			//							hashMap.get(viwVerFunzMisureEricsson.getCodiCaratteristica()).setDescScadenzaTaratura(viwVerFunzMisureEricsson.getDescValore());
			//						else
			//							hashMap.get(viwVerFunzMisureEricsson.getCodiCaratteristica()).setDescNumeroSerie(viwVerFunzMisureEricsson.getDescValore());
			//					}
			//				}
			//			}
			//			List<VerificheFunzMisureStrumentiBean> arrayVeriFunzMisureStrumentiBean = new ArrayList<VerificheFunzMisureStrumentiBean>(hashMap.values());
			/*FINE LOGICA per estrarre Lista strumenti e componenti utilizzati codiElemento nella namedQuery 55 */


			/*LOGICA per estrarre  Offest introdotti dalle connessioni codiElemento nella namedQuery 56 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOffsetBean = reportDao.getListVerFunzMisureOffsetEricsson(selectedSessione.getCodiSessione());
			/* FINE LOGICA per estrarre  Offest introdotti dalle connessioni codiElemento nella namedQuery 56 */ 

			/*LOGICA per estrarre  Canali RF con solo settori  codiElemento nella namedQuery 57 e caratteristiche 661,663,664 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureCanaliSettore = reportDao.getListVerFunzMisureCanaliSettoreEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBean> arrayVeriFunzMisureCanaliSettoreBean = UtilityReports.getArrayEricssonMisureSettoreBean(arrayVeriFunzMisureCanaliSettore);
			/*FINE LOGICA per estrarre  Canali RF con solo settori  codiElemento nella namedQuery 57 e caratteristiche 661,663,664 */ 

			/*LOGICA per estrarre  Canali Settori/Celle RF con  settori/celle  codiElemento nella namedQuery 57 e caratteristiche 662 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureCanaliSettoreCella = reportDao.getListVerFunzMisureCanaliSettoreCellaEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureSettoreCellaBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureCanaliSettoreCella);
			/*FINE LOGICA per estrarre  Canali Settori/Celle RF con  settori/celle  codiElemento nella namedQuery 57 e caratteristiche 662 */

			/*LOGICA per estrarre  verFunzMisureVerTestReportERICSSON  codiElemento nella namedQuery 99 e caratteristiche 848 */
			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureVerTestReport = reportDao.getListVeriFunzMisureVerTestReportEricsson(selectedSessione.getCodiSessione());
			List<VerFunzMisureVerTestReportBean> arrayVeriFunzMisureVerTestReportBean = UtilityReports.getArrayVerFunzMisureVerTestReportBean(arrayVeriFunzMisureVerTestReport);
			List<VerFunzMisureVerTestReportBean> arrayVeriFunzMisureVerTestReportNoteBean = UtilityReports.getArrayVerFunzMisureVerTestReportBean(arrayVeriFunzMisureVerTestReport);
			/*FINE LOGICA per estrarre  Tratte in fibra con solo celle  codiElemento nella namedQuery 58 e caratteristiche 665,666,667,668 */ 

			/*LOGICA per estrarre  Tratte in fibra con solo celle  codiElemento nella namedQuery 58 e caratteristiche 665,666,667,668 */
			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureTratteInFibra = reportDao.getListVeriFunzMisureTratteInFibraEricsson(selectedSessione.getCodiSessione());
			List<VerificheFunzMisureCelleBean> arrayVeriFunzMisureTratteInFibraBean = UtilityReports.getArrayEricssonMisureCelleBean(arrayVeriFunzMisureTratteInFibra);
			/*FINE LOGICA per estrarre  Tratte in fibra con solo celle  codiElemento nella namedQuery 58 e caratteristiche 665,666,667,668 */ 

			/*LOGICA per estrarre  Output Power Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 59  e caratteristica 669*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOutPowValRif = reportDao.getListVeriFunzMisureOutPowValRifEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureOutPowValRifBean> arrayVeriFunzMisureOutPowValRifBean= new ArrayList<VerificheFunzMisureOutPowValRifBean>();
			//			VerificheFunzMisureOutPowValRifBean verificheFunzMisureOutPowValRifBean = new VerificheFunzMisureOutPowValRifBean();
			//			verificheFunzMisureOutPowValRifBean.setDescCaratteristica(arrayVeriFunzMisureOutPowValRif.get(0).getDescCaratteristica());	
			//			verificheFunzMisureOutPowValRifBean.setDescElemento(arrayVeriFunzMisureOutPowValRif.get(0).getDescElemento());
			//			verificheFunzMisureOutPowValRifBean.setDescNote(arrayVeriFunzMisureOutPowValRif.get(0).getDescNote());
			//			for (ViwVerFunzMisureEricsson verFunzMisureEricsson : arrayVeriFunzMisureOutPowValRif) {
			//				if(verFunzMisureEricsson.getDescLabel()!=null){
			//					switch (verFunzMisureEricsson.getDescLabel()) {
			//					case "GSM:":
			//						verificheFunzMisureOutPowValRifBean.setDescLabel1(verFunzMisureEricsson.getDescValore());
			//						break;
			//					case "UMTS:":
			//						verificheFunzMisureOutPowValRifBean.setDescLabel2(verFunzMisureEricsson.getDescValore());
			//						break;
			//					case "LTE:":
			//						verificheFunzMisureOutPowValRifBean.setDescLabel3(verFunzMisureEricsson.getDescValore());
			//						break;
			//
			//					}
			//				}
			//			}
			//			arrayVeriFunzMisureOutPowValRifBean.add(verificheFunzMisureOutPowValRifBean);
			//			/* FINE LOGICA per estrarre  Output Power Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 59  e caratteristica 669 */ 
			//
			//			/*LOGICA per estrarre  Misure Output power con solo settori  codiElemento nella namedQuery 59 e caratteristiche 670 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureGsmOutPow = reportDao.getListVerFunzMisureGsmOutPow(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBean> arrayVeriFunzMisureGsmOutPowBean = UtilityReports.getArrayEricssonMisureSettoreBean(arrayVeriFunzMisureGsmOutPow);
			//			/*FINE LOGICA per estrarre  Misure Output power con solo settori  codiElemento nella namedQuery 59 e caratteristiche 670 */ 
			//
			//			/*LOGICA per estrarre  Misure Output power Settori/Celle  con  settori/celle  codiElemento nella namedQuery 59 e caratteristiche 671 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureUmtsOutPow = reportDao.getListVerFunzMisureUmtsOutPow(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureUmtsOutPowBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureUmtsOutPow);
			//			/*FINE LOGICA per estrarre  Misure Output power Settori/Celle  con  settori/celle  codiElemento nella namedQuery 59 e caratteristiche 671 */
			//
			//			/*LOGICA per estrarre  Misure Output power Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 59 e caratteristiche 672 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureLteOutPow = reportDao.getListVerFunzMisureLteOutPow(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBranchBean> arrayVeriFunzMisureLteOutPowBean= UtilityReports.getArrayEricssonMisureSettoreBranchBean(arrayVeriFunzMisureLteOutPow);
			//			/*FINE LOGICA per estrarre  Misure Output power Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 59 e caratteristiche 672 */
			//
			//			/*LOGICA per estrarre  Misure Frequency error con solo settori  codiElemento nella namedQuery 60 e caratteristiche 673,674,675 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureFrequencyErrorSettore = reportDao.getListVerFunzMisureFrequencyErrorEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBean> arrayVeriFunzMisureFrequencyErrorSettoreBean = UtilityReports.getArrayEricssonMisureSettoreBean(arrayVeriFunzMisureFrequencyErrorSettore);
			//			/*FINE LOGICA per estrarre  Misure Frequency error con solo settori  codiElemento nella namedQuery 60 e caratteristiche 673,674,675 */ 
			//
			//			/*LOGICA per estrarre  Misure Frequency error Settori/Celle  con  settori/celle  codiElemento nella namedQuery 60 e caratteristiche 676 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureFrequencyErrorSettoreCella = reportDao.getListVerFunzMisureFrequencyErrorSettoreCellaEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureFrequencyErrorSettoreCellaBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureFrequencyErrorSettoreCella);
			//			/*FINE LOGICA per estrarre  Misure Frequency error power Settori/Celle  con  settori/celle  codiElemento nella namedQuery 60 e caratteristiche 676 */
			//
			//			/*LOGICA per estrarre  Misure Frequency error Settori/Celle  con  settori/celle  codiElemento nella namedQuery 60 e caratteristiche 677 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureFrequencyErrorSettoreBranch = reportDao.getListVerFunzMisureFrequencyErrorSettoreBranchEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBranchBean> arrayVeriFunzMisureFrequencyErrorSettoreBranchBean= UtilityReports.getArrayEricssonMisureSettoreBranchBean(arrayVeriFunzMisureFrequencyErrorSettoreBranch);
			//			/*FINE LOGICA per estrarre  Misure Frequency error power Settori/Celle  con  settori/celle  codiElemento nella namedQuery 60 e caratteristiche 677 */
			//
			//			/*LOGICA per estrarre  Misure CPICH Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 61  e caratteristica 678*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureCPICHValRifBean = reportDao.getListVeriFunzMisureCPICHValRifEricsson(selectedSessione.getCodiSessione());
			//			/* FINE LOGICA per estrarre  Misure CPICH Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 61  e caratteristica 678 */
			//
			//			/*LOGICA per estrarre  Misure CPICH error Settori/Celle  con  settori/celle  codiElemento nella namedQuery 61 e caratteristiche 679 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureCPICHSettoreCella = reportDao.getListVeriFunzMisureCPICHSettoreCellaEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureCPICHSettoreCellaBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureCPICHSettoreCella);
			//			/*FINE LOGICA per estrarre Misure CPICH error power Settori/Celle  con  settori/celle  codiElemento nella namedQuery 61 e caratteristiche 679 */
			//
			//			/*LOGICA per estrarre  Misure OCCUPIED Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 62  e caratteristica 680*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOCCUPIEDValRifBean = reportDao.getListVeriFunzMisureOCCUPIEDValRifEricsson(selectedSessione.getCodiSessione());
			//			/* FINE LOGICA per estrarre  Misure OCCUPIED Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 62  e caratteristica 680 */
			//
			//			/*LOGICA per estrarre  Misure OCCUPIED Settori/Celle  con  settori/celle  codiElemento nella namedQuery 62 e caratteristiche 681 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOCCUPIEDSettoreCella = reportDao.getListVeriFunzMisureOCCUPIEDSettoreCellaEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureOCCUPIEDSettoreCellaBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureOCCUPIEDSettoreCella);
			//			/*FINE LOGICA per estrarre  Misure OCCUPIED Settori/Celle  con  settori/celle  codiElemento nella namedQuery 62 e caratteristiche 681 */
			//
			//			/*LOGICA per estrarre  Misure OCCUPIED LTE Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 62  e caratteristica 682*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOCCUPIEDValRifLTEBean = reportDao.getListVeriFunzMisureOCCUPIEDValRifLTEEricsson(selectedSessione.getCodiSessione());
			//			/* FINE LOGICA per estrarre  Misure OCCUPIED LTE Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 62  e caratteristica 682 */
			//
			//			/*LOGICA per estrarre   Misure OCCUPIED LTE Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 62 e caratteristiche 683 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureOCCUPIEDsettoriBranch = reportDao.getListVeriFunzMisureOCCUPIEDsettoriBranch(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBranchBean> arrayVeriFunzMisureOCCUPIEDsettoriBranchBean= UtilityReports.getArrayEricssonMisureSettoreBranchBean(arrayVeriFunzMisureOCCUPIEDsettoriBranch);
			//			/*FINE LOGICA per estrarre  Misure OCCUPIED LTE Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 62 e caratteristiche 683 */
			//
			//			/*LOGICA per estrarre  Misure EVM  Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 63  e caratteristica 684*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureEVMValRifBean = reportDao.getListVeriFunzMisureEVMValRifEricsson(selectedSessione.getCodiSessione());
			//			/* FINE LOGICA per estrarre  Misure EVM  Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 63  e caratteristica 684 */
			//
			//			/*LOGICA per estrarre  Misure EVM Settori/Celle  con  settori/celle  codiElemento nella namedQuery 63 e caratteristiche 685 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureEVMSettoreCella = reportDao.getListVeriFunzMisureEVMSettoreCellaEricsson(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreCellaBean> arrayVeriFunzMisureEVMSettoreCellaBean= UtilityReports.getArrayEricssonMisureSettoreCellaBean(arrayVeriFunzMisureEVMSettoreCella);
			//			/*FINE LOGICA per estrarre  Misure EVM Settori/Celle  con  settori/celle  codiElemento nella namedQuery 63 e caratteristiche 685 */
			//
			//			/*LOGICA per estrarre  Misure EVM LTE  Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 63  e caratteristica 686*/
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureEVMValRifLTEBean = reportDao.getListVeriFunzMisureEVMValRifLTEEricsson(selectedSessione.getCodiSessione());
			//			/* FINE LOGICA per estrarre  Misure EVM LTE  Valore di riferimento introdotti dalle connessioni codiElemento nella namedQuery 63  e caratteristica 686 */
			//
			//			/*LOGICA per estrarre   Misure EVM LTE Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 63 e caratteristiche 687 */
			//			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureEVMsettoriBranch = reportDao.getListVeriFunzMisureEVMsettoriBranch(selectedSessione.getCodiSessione());
			//			List<VerificheFunzMisureSettoreBranchBean> arrayVeriFunzMisureEVMsettoriBranchBean= UtilityReports.getArrayEricssonMisureSettoreBranchBean(arrayVeriFunzMisureEVMsettoriBranch);
			//			/*FINE LOGICA per estrarre  Misure OCCUPIED LTE Settori/Branch  con  settori/Branch  codiElemento nella namedQuery 62 e caratteristiche 687 */

			/*LOGICA per estrarre Verifivhe TMA  codiElemento nella namedQuery 64 e caratteristiche 688 ,689 */
			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureVerTMA = reportDao.getListMisureVerTMA(selectedSessione.getCodiSessione());
			List<VerificheFunzMisureCelleDoppiaLabelBean> arrayVeriFunzMisureVerTMABean= UtilityReports.getArrayEricssonMisureCelleDoppiaLabelBean(arrayVeriFunzMisureVerTMA);
			/*FINE LOGICA per estrarre Verifivhe TMA  codiElemento nella namedQuery 64 e caratteristiche 688 ,689 */

			/*LOGICA per estrarre Verifivhe RET  codiElemento nella namedQuery 65 e caratteristiche 692 ,693 */
			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureVerRET = reportDao.getListMisureVerRET(selectedSessione.getCodiSessione());
			List<VerificheFunzMisureCelleDoppiaLabelBean> arrayVeriFunzMisureVerRETBean= UtilityReports.getArrayEricssonMisureCelleDoppiaLabelBean(arrayVeriFunzMisureVerRET);
			/*FINE LOGICA per estrarre Verifivhe RET  codiElemento nella namedQuery 65 e caratteristiche 692 ,693 */

			/*LOGICA per estrarre  gestion ret codi caratt 690, 691*/
			List<ViwVerFunzMisureEricsson> arrayVeriFunzMisureVerGestioneRETBean = reportDao.getListMisureVerGestioneRET(selectedSessione.getCodiSessione());
			List<GestioneRetBean> arrayGestioneRetbean = UtilityReports.getArrayGestioneRetbean(arrayVeriFunzMisureVerGestioneRETBean);

			/* FINE LOGICA  */


			//Esito Verifiche funzionali apparato
			List<ViwEsitoSurvey> arrayEsitoVerFunzionaliApparati = reportDao.getListEsitoVerFunzionaliApparati(selectedSessione.getCodiSessione());

			//			Riferimenti VeriFunzionaleEricsson
			List<RiferimentiBean> arrayyRiferimentiVeriFunzionaleEricssonBean= new ArrayList<>();
			if(arrayVeriFunzMisureVerGestioneRETBean.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVeriFunzionaleEricsson = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVeriFunzMisureVerGestioneRETBean.get(0).getCodiSurvey());
				arrayyRiferimentiVeriFunzionaleEricssonBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVeriFunzionaleEricsson);
			}
			BufferedImage firma_tim_VerFunz=UtilityReports.getFirmaTim();
			BufferedImage firma_ditta_VerFunz= UtilityReports.getFirmaDitta();

			//Sezione 2 Prove funzionali di esercizio
			List<ViwProveFunzEseEricsson> arrayProveFunzEseEricssonBean= reportDao.getListProveFunzEseEricssonBean(selectedSessione.getCodiSessione());
			List<ViwEsitoSurvey> arrayEsitoProveFunzEseApparati =reportDao.getListEsitoProveFunzEseApparati(selectedSessione.getCodiSessione());


			//Riferimenti ProveFunzEseEricsson
			List<RiferimentiBean> arrayRiferimentiProveFunzEseApparatiEricssonBean= new ArrayList<>();
			if(arrayProveFunzEseEricssonBean.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVeriFunzionaleEricsson = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayProveFunzEseEricssonBean.get(0).getCodiSurvey());
				arrayRiferimentiProveFunzEseApparatiEricssonBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVeriFunzionaleEricsson);
			}
			BufferedImage firma_tim_ProveFunzEse=UtilityReports.getFirmaTim();



			//Sezione 2 Prove funzionali di servizio
			List<ViwProveFunzSerEricsson> arrayProveFunzionaliServizio = reportDao.getListProveFunzSerEricsson(selectedSessione.getCodiSessione());
			List<ProveDiServizioBean> arrayProveFunzionaliServizioBean = UtilityReports.getListProveFunzionaliServizioBean(arrayProveFunzionaliServizio);

			//Esito ProveFunzionaliServizio
			List<ViwEsitoSurvey> arrayEsitoProveFunzionaliServizio = reportDao.getListEsitoProveFunzSerEricsson(selectedSessione.getCodiSessione());

			//Riferimenti ProveFunzionaliServizio
			BufferedImage firma_tim_ProveFunzServ=null;
			List<RiferimentiBean> arrayRiferimentiProveFunzServizioApparatiEricssonBean= new ArrayList<>();
			if(arrayProveFunzionaliServizio.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVeriFunzionaleServizioEricsson = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayProveFunzionaliServizio.get(0).getCodiSurvey());
				arrayRiferimentiProveFunzServizioApparatiEricssonBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVeriFunzionaleServizioEricsson);
				firma_tim_ProveFunzServ=UtilityReports.getFirmaTim();
			}

			//DATI GENERALI WIPM
			List<ViwSessioni> arrayDatiGeneraliWIPM = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			List<ViwApparatoEricsson> arrayDatiGeneraliTabletWIPM = reportDao.getListApparatoEricssonBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletEricssonWIPM);
			List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletWIPMBean = UtilityReports.getArrayTabletDatiGeneraliApparatoEricssonBean(arrayDatiGeneraliTabletWIPM);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliWIPMBean= new ArrayList<>();
			BufferedImage firmaTimDatiGeneraliWIPM=null;
			BufferedImage firmaDittaDatiGeneraliWIPM=null;
			if(arrayDatiGeneraliTabletWIPM.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTabletWIPM.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliWIPMBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimDatiGeneraliWIPM = UtilityReports.getFirmaTim();
				firmaDittaDatiGeneraliWIPM = UtilityReports.getFirmaDitta();
			} 

			/*LOGICA per estrarre Verifivhe di installazione WIPM tensioni primarie e secondarie Dell'elemento Alimentazione */
			List<ViwVerInstWipmEricsson> arrayVerificheInstWIPM = reportDao.getListVerificheInstWIPMEricsson(selectedSessione.getCodiSessione());
			List<ViwVerInstWipmEricsson> arrayTensioniPrimarieSecondarie = new ArrayList<ViwVerInstWipmEricsson>();

			List<VerificheInstallazioneTensioniWIPMBean> arrayVerificheInstallazioneTensioniWIPMBean;
			HashMap<Integer, VerificheInstallazioneTensioniWIPMBean> hashMapVerInsTensioni = new HashMap<>();
			VerificheInstallazioneTensioniWIPMBean verificheInstallazioneTensioniWIPMBean = new VerificheInstallazioneTensioniWIPMBean();
			hashMapVerInsTensioni.put(ConstReport.TensionePrimariaDC, verificheInstallazioneTensioniWIPMBean);
			verificheInstallazioneTensioniWIPMBean = new VerificheInstallazioneTensioniWIPMBean();
			hashMapVerInsTensioni.put(ConstReport.TensionePrimariaAC, verificheInstallazioneTensioniWIPMBean);
			verificheInstallazioneTensioniWIPMBean = new VerificheInstallazioneTensioniWIPMBean();
			hashMapVerInsTensioni.put(ConstReport.TensioneSecondariaDC, verificheInstallazioneTensioniWIPMBean);
			verificheInstallazioneTensioniWIPMBean = new VerificheInstallazioneTensioniWIPMBean();
			hashMapVerInsTensioni.put(ConstReport.TensioneSecondariaAC, verificheInstallazioneTensioniWIPMBean);

			for (ViwVerInstWipmEricsson viwVerInstWipmEricsson : arrayVerificheInstWIPM) {

				if(viwVerInstWipmEricsson.getCodiCaratteristica().equals(ConstReport.TensionePrimariaDC)||viwVerInstWipmEricsson.getCodiCaratteristica().equals(ConstReport.TensionePrimariaAC)
						||viwVerInstWipmEricsson.getCodiCaratteristica().equals(ConstReport.TensioneSecondariaDC)||viwVerInstWipmEricsson.getCodiCaratteristica().equals(ConstReport.TensioneSecondariaAC)){

					arrayTensioniPrimarieSecondarie.add(viwVerInstWipmEricsson);
					if(viwVerInstWipmEricsson.getDescLabel()!=null){
						switch (viwVerInstWipmEricsson.getCodiCaratteristica()) {
						case ConstReport.TensionePrimariaDC:
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							if(viwVerInstWipmEricsson.getDescLabel().equalsIgnoreCase("Valore misurato"))
								hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescValoreMisurato(viwVerInstWipmEricsson.getDescValore());
							else
								hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescEsito(viwVerInstWipmEricsson.getDescLabelLov());
							break;
						case ConstReport.TensionePrimariaAC:
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							if(viwVerInstWipmEricsson.getDescLabel().equalsIgnoreCase("Valore misurato"))
								hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescValoreMisurato(viwVerInstWipmEricsson.getDescValore());
							else
								hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescEsito(viwVerInstWipmEricsson.getDescLabelLov());
							break;
						case ConstReport.TensioneSecondariaDC:
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							if(viwVerInstWipmEricsson.getDescLabel().equalsIgnoreCase("Valore misurato"))
								hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescValoreMisurato(viwVerInstWipmEricsson.getDescValore());
							else
								hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescEsito(viwVerInstWipmEricsson.getDescLabelLov());
							break;
						case ConstReport.TensioneSecondariaAC:
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							if(viwVerInstWipmEricsson.getDescLabel().equalsIgnoreCase("Valore misurato"))
								hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescValoreMisurato(viwVerInstWipmEricsson.getDescValore());
							else
								hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescEsito(viwVerInstWipmEricsson.getDescLabelLov());
							break;


						}
					} else {
						switch (viwVerInstWipmEricsson.getCodiCaratteristica()) {
						case ConstReport.TensionePrimariaDC:
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaDC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							break;
						case ConstReport.TensionePrimariaAC:
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensionePrimariaAC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							break;
						case ConstReport.TensioneSecondariaDC:
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaDC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							break;
						case ConstReport.TensioneSecondariaAC:
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescCaratteristica(viwVerInstWipmEricsson.getDescCaratteristica());
							hashMapVerInsTensioni.get(ConstReport.TensioneSecondariaAC).setDescNote(viwVerInstWipmEricsson.getDescNote());
							break;

						}
					}
				}
			}
			arrayVerificheInstallazioneTensioniWIPMBean=new ArrayList<>(hashMapVerInsTensioni.values());
			if(arrayTensioniPrimarieSecondarie.size()>0){
				arrayVerificheInstWIPM.removeAll(arrayTensioniPrimarieSecondarie);
			}

			//Esito VerificheInstWIPM
			List<ViwEsitoSurvey> arrayEsitoVerificheInstWIPM = reportDao.getListEsitoVerificheInstWIPM(selectedSessione.getCodiSessione());

			//			Riferimenti VerificheInstWIPM
			List<RiferimentiBean> arrayRiferimentiVerificheInstWIPMEricssonBean= new ArrayList<>();
			if(arrayVerificheInstWIPM.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerificheInstWIPMEricsson = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificheInstWIPM.get(0).getCodiSurvey());
				arrayRiferimentiVerificheInstWIPMEricssonBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerificheInstWIPMEricsson);

			}
			BufferedImage firma_tim_VerificheInstWIPM=UtilityReports.getFirmaTim();
			BufferedImage firma_ditta_VerificheInstWIPM=UtilityReports.getFirmaDitta();


			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanTitoloWIPM = new JRBeanCollectionDataSource(arrayTitoloWIPM, false);
			JRBeanCollectionDataSource beanVerificheInst = new JRBeanCollectionDataSource(arrayVerificheInst, false);
			JRBeanCollectionDataSource beanVerificheInstTestFlussiE1 = new JRBeanCollectionDataSource(arrayverificheInstTestFlussiE1EricssonBean, false);
			JRBeanCollectionDataSource beanEsitoVerificheInst = new JRBeanCollectionDataSource(arrayEsitoVeriInst, false);


			if(arrayVeriFunzAllarmi.size()==0){
				arrayVeriFunzAllarmi.add(new ViwVerFunzAllarmiEricsson());

			}
			JRBeanCollectionDataSource beanVerificheFunzAllarmi = new JRBeanCollectionDataSource(arrayVeriFunzAllarmiBean, false);
			//			JRBeanCollectionDataSource beanVerificheFunzMisureStrumenti = new JRBeanCollectionDataSource(arrayVeriFunzMisureStrumentiBean, false);
			//			JRBeanCollectionDataSource beanVerificheFunzMisureOffset = new JRBeanCollectionDataSource(arrayVeriFunzMisureOffsetBean, false);
			//			JRBeanCollectionDataSource beanVerificheFunzMisureCanaliSettore = new JRBeanCollectionDataSource(arrayVeriFunzMisureCanaliSettoreBean, false);
			//			JRBeanCollectionDataSource beanVerificheFunzMisureCanaliSettoreCella = new JRBeanCollectionDataSource(arrayVeriFunzMisureSettoreCellaBean, false);
			JRBeanCollectionDataSource beanVeriFunzMisureVerTestReport = new JRBeanCollectionDataSource(arrayVeriFunzMisureVerTestReportBean, false);
			JRBeanCollectionDataSource beanVerFunzMisureVerTestReportNote = new JRBeanCollectionDataSource(arrayVeriFunzMisureVerTestReportNoteBean, false);

			JRBeanCollectionDataSource beanVeriFunzMisureTratteInFibra = new JRBeanCollectionDataSource(arrayVeriFunzMisureTratteInFibraBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureOutPowValRif = new JRBeanCollectionDataSource(arrayVeriFunzMisureOutPowValRifBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureGsmOutPow = new JRBeanCollectionDataSource(arrayVeriFunzMisureGsmOutPowBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureUmtsOutPow = new JRBeanCollectionDataSource(arrayVeriFunzMisureUmtsOutPowBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureLteOutPow = new JRBeanCollectionDataSource(arrayVeriFunzMisureLteOutPowBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureFrequencyErrorSettore = new JRBeanCollectionDataSource(arrayVeriFunzMisureFrequencyErrorSettoreBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureFrequencyErrorSettoreCella = new JRBeanCollectionDataSource(arrayVeriFunzMisureFrequencyErrorSettoreCellaBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureFrequencyErrorSettoreBranch = new JRBeanCollectionDataSource(arrayVeriFunzMisureFrequencyErrorSettoreBranchBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureCPICHValRif = new JRBeanCollectionDataSource(arrayVeriFunzMisureCPICHValRifBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureCPICHSettoreCella = new JRBeanCollectionDataSource(arrayVeriFunzMisureCPICHSettoreCellaBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureOCCUPIEDValRif = new JRBeanCollectionDataSource(arrayVeriFunzMisureOCCUPIEDValRifBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureOCCUPIEDSettoreCella = new JRBeanCollectionDataSource(arrayVeriFunzMisureOCCUPIEDSettoreCellaBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureOCCUPIEDValRifLTE = new JRBeanCollectionDataSource(arrayVeriFunzMisureOCCUPIEDValRifLTEBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureOCCUPIEDsettoriBranch = new JRBeanCollectionDataSource(arrayVeriFunzMisureOCCUPIEDsettoriBranchBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureEVMValRif = new JRBeanCollectionDataSource(arrayVeriFunzMisureEVMValRifBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureEVMSettoreCella = new JRBeanCollectionDataSource(arrayVeriFunzMisureEVMSettoreCellaBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureEVMValRifLTE = new JRBeanCollectionDataSource(arrayVeriFunzMisureEVMValRifLTEBean, false);
			//			JRBeanCollectionDataSource beanVeriFunzMisureEVMsettoriBranch = new JRBeanCollectionDataSource(arrayVeriFunzMisureEVMsettoriBranchBean, false);
			JRBeanCollectionDataSource beanVeriFunzMisureVerTMABean = new JRBeanCollectionDataSource(arrayVeriFunzMisureVerTMABean, false);
			JRBeanCollectionDataSource beanVeriFunzMisureVerRETBean = new JRBeanCollectionDataSource(arrayVeriFunzMisureVerRETBean, false);
			JRBeanCollectionDataSource beanVeriFunzMisureVerGestioneRETBean = new JRBeanCollectionDataSource(arrayGestioneRetbean, false);
			JRBeanCollectionDataSource beanEsitoVerFunzionaliApparati = new JRBeanCollectionDataSource(arrayEsitoVerFunzionaliApparati, false);
			JRBeanCollectionDataSource beanDatiGeneraliApparati = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletApparati = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletBean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGeneraliEricsson = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);


			JRBeanCollectionDataSource beanProveFunzEseEricsson = new JRBeanCollectionDataSource(arrayProveFunzEseEricssonBean, false);
			JRBeanCollectionDataSource beanEsitoProveFunzEseApparati = new JRBeanCollectionDataSource(arrayEsitoProveFunzEseApparati, false);
			JRBeanCollectionDataSource beanProveFunzionaliServizio = new JRBeanCollectionDataSource(arrayProveFunzionaliServizioBean, false);
			JRBeanCollectionDataSource beanEsitoProveFunzionaliServizio = new JRBeanCollectionDataSource(arrayEsitoProveFunzionaliServizio, false);
			JRBeanCollectionDataSource beanDatiGeneraliWIPMApparati = new JRBeanCollectionDataSource(arrayDatiGeneraliWIPM, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletApparatiWIPM = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletWIPMBean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGeneraliEricssonWIPM = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliWIPMBean, false);



			JRBeanCollectionDataSource beanVerificheInstWIPM = new JRBeanCollectionDataSource(arrayVerificheInstWIPM, false);
			JRBeanCollectionDataSource beanVerificheInstallazioneTensioniWIPM = new JRBeanCollectionDataSource(arrayVerificheInstallazioneTensioniWIPMBean, false);
			JRBeanCollectionDataSource beanEsitoVerificheInstWIPM = new JRBeanCollectionDataSource(arrayEsitoVerificheInstWIPM, false);
			JRBeanCollectionDataSource beanRiferimentiVeriInstEricsson = new JRBeanCollectionDataSource(arrayyRiferimentiVeriInstEricssonBean, false);

			JRBeanCollectionDataSource beanRiferimentiVerFunzionaliApparatoERICSSON = new JRBeanCollectionDataSource(arrayyRiferimentiVeriFunzionaleEricssonBean, false);
			JRBeanCollectionDataSource beanRiferimentiProveFunzEseApparatiERICSSON = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzEseApparatiEricssonBean, false);
			JRBeanCollectionDataSource beanRiferimentiProveFunzServizioApparatiERICSSON = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzServizioApparatiEricssonBean, false);
			JRBeanCollectionDataSource beanRiferimentiVerificheInstWIPMERICSSON = new JRBeanCollectionDataSource(arrayRiferimentiVerificheInstWIPMEricssonBean, false);

			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String pathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(pathLogoSaw);

			Map<String, Object> parameters = new HashMap<String, Object>();

			//dati generali OA
			parameters.put("datiGeneraliApparatoERICSSON", beanDatiGeneraliApparati);
			parameters.put("datiGeneraliTabletApparatoERICSSON", beanDatiGeneraliTabletApparati);
			parameters.put("riferimentiDatiGeneraliERICSSON", beanRiferimentiDatiGeneraliEricsson);
			parameters.put("firma_tim_datiGenerali",firmaTimDatiGenerali);
			parameters.put("firma_ditta_datiGenerali",firmaDittaDatiGenerali);
			//verifiche di installazione OA
			parameters.put("verificaInstallazioneERICSSON", beanVerificheInst);
			parameters.put("verificaInstallazioneFlussiE1ERICSSON", beanVerificheInstTestFlussiE1);
			parameters.put("esitoVerInstallazioneERICSSON", beanEsitoVerificheInst);
			parameters.put("riferimentiVerInstallazioneERICSSON", beanRiferimentiVeriInstEricsson);
			parameters.put("firma_tim_VerIns", firma_tim_VerIns);
			parameters.put("firma_ditta_VerIns",firma_ditta_VerIns);
			//verifiche funzionali commissioning
			parameters.put("verificaFunzionaleAllarmiERICSSON", beanVerificheFunzAllarmi);
			//			parameters.put("verificaFunzionaleMisureStrumentiERICSSON", beanVerificheFunzMisureStrumenti);
			//			parameters.put("verificaFunzionaleMisureOffsetERICSSON", beanVerificheFunzMisureOffset);
			//			parameters.put("verificaFunzionaleMisureCanaliSettoreERICSSON", beanVerificheFunzMisureCanaliSettore);
			//			parameters.put("verificaFunzionaleMisureCanaliSettoreCellaERICSSON", beanVerificheFunzMisureCanaliSettoreCella);
			parameters.put("verFunzMisureVerTestReportERICSSON", beanVeriFunzMisureVerTestReport);
			parameters.put("verFunzMisureVerTestReportNoteERICSSON", beanVerFunzMisureVerTestReportNote);
			parameters.put("veriFunzMisureTratteInFibraERICSSON", beanVeriFunzMisureTratteInFibra);
			//			parameters.put("veriFunzMisureOutPowValRifERICSSON", beanVeriFunzMisureOutPowValRif);
			//			parameters.put("veriFunzMisureGsmOutPowERICSSON", beanVeriFunzMisureGsmOutPow);
			//			parameters.put("veriFunzMisureUmtsOutPowERICSSON", beanVeriFunzMisureUmtsOutPow);
			//			parameters.put("veriFunzMisureLteOutPowERICSSON", beanVeriFunzMisureLteOutPow);
			//			parameters.put("veriFunzMisureFrequencyErrorSettoreERICSSON", beanVeriFunzMisureFrequencyErrorSettore);
			//			parameters.put("veriFunzMisureFrequencyErrorSettoreCellaERICSSON", beanVeriFunzMisureFrequencyErrorSettoreCella);
			//			parameters.put("veriFunzMisureFrequencyErrorSettoreBranchERICSSON", beanVeriFunzMisureFrequencyErrorSettoreBranch);
			//			parameters.put("veriFunzMisureCPICHValRifERICSSON", beanVeriFunzMisureCPICHValRif);
			//			parameters.put("veriFunzMisureCPICHSettoreCellaERICSSON", beanVeriFunzMisureCPICHSettoreCella);
			//			parameters.put("veriFunzMisureOCCUPIEDValRifERICSSON", beanVeriFunzMisureOCCUPIEDValRif);
			//			parameters.put("veriFunzMisureOCCUPIEDSettoreCellaERICSSON", beanVeriFunzMisureOCCUPIEDSettoreCella);
			//			parameters.put("veriFunzMisureOCCUPIEDValRifLTEERICSSON", beanVeriFunzMisureOCCUPIEDValRifLTE);
			//			parameters.put("veriFunzMisureOCCUPIEDsettoriBranchERICSSON", beanVeriFunzMisureOCCUPIEDsettoriBranch);
			//			parameters.put("veriFunzMisureEVMValRifERICSSON", beanVeriFunzMisureEVMValRif);
			//			parameters.put("veriFunzMisureEVMSettoreCellaERICSSON", beanVeriFunzMisureEVMSettoreCella);
			//			parameters.put("veriFunzMisureEVMSValRifLTEERICSSON", beanVeriFunzMisureEVMValRifLTE);
			//			parameters.put("veriFunzMisureEVMsettoriBranchERICSSON", beanVeriFunzMisureEVMsettoriBranch);
			parameters.put("veriFunzMisureVerTMAERICSSON", beanVeriFunzMisureVerTMABean);
			parameters.put("veriFunzMisureVerRETERICSSON", beanVeriFunzMisureVerRETBean);
			parameters.put("veriFunzMisureVerGestioneRETERICSSON", beanVeriFunzMisureVerGestioneRETBean);
			parameters.put("esitoVerFunzionaliApparatoERICSSON", beanEsitoVerFunzionaliApparati);
			parameters.put("riferimentiVerFunzionaliApparatoERICSSON", beanRiferimentiVerFunzionaliApparatoERICSSON);
			parameters.put("firma_tim_VerFunz", firma_tim_VerFunz);
			parameters.put("firma_ditta_VerFunz",firma_ditta_VerFunz);
			//prove funzionali esercizio
			parameters.put("proveFunzEseERICSSON", beanProveFunzEseEricsson);	
			parameters.put("esitoProveFunzEseApparatiERICSSON", beanEsitoProveFunzEseApparati);
			parameters.put("riferimentiProveFunzEseApparatiERICSSON", beanRiferimentiProveFunzEseApparatiERICSSON);
			parameters.put("firma_tim_ProveFunzEse",firma_tim_ProveFunzEse);
			//prive funzionali servizio
			parameters.put("proveFunzionaliServizioERICSSON", beanProveFunzionaliServizio);
			parameters.put("esitoProveFunzionaliServizioERICSSON", beanEsitoProveFunzionaliServizio);
			parameters.put("riferimentiProveFunzServizioApparatiERICSSON", beanRiferimentiProveFunzServizioApparatiERICSSON);
			parameters.put("firma_tim_ProveFunzServ",firma_tim_ProveFunzServ);
			//dati generali WIPM
			parameters.put("datiGeneraliWIPMApparatoERICSSON", beanDatiGeneraliWIPMApparati);
			parameters.put("datiGeneraliTabletApparatoERICSSONWIPM", beanDatiGeneraliTabletApparatiWIPM);
			parameters.put("riferimentiDatiGeneraliERICSSONWIPM", beanRiferimentiDatiGeneraliEricssonWIPM);
			parameters.put("firma_tim_datiGeneraliWIPM",firmaTimDatiGeneraliWIPM);
			parameters.put("firma_ditta_datiGeneraliWIPM",firmaDittaDatiGeneraliWIPM);
			//Verifiche installazione WIPM
			parameters.put("verificaInstallazioneWIMPERICSSON", beanVerificheInstWIPM);
			parameters.put("verificaInstallazioneTensioniWIMPERICSSON", beanVerificheInstallazioneTensioniWIPM);
			parameters.put("esitoVerificheInstWIPMERICSSON", beanEsitoVerificheInstWIPM);
			parameters.put("riferimentiVerificheInstWIPMERICSSON", beanRiferimentiVerificheInstWIPMERICSSON);
			parameters.put("titoloERICSSON", beanTitolo);
			parameters.put("titoloWIPMERICSSON", beanTitoloWIPM);
			parameters.put("firma_tim_VerificheInstWIPM",firma_tim_VerificheInstWIPM);
			parameters.put("firma_ditta_VerificheInstWIPM",firma_ditta_VerificheInstWIPM);
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_ERICSSON,sincronizzazioneDao));


			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}


	public byte[] printEricsson(JasperPrint jasperPrint, ViwSessioni selectedSessione){
		byte[] attachment=null;
		try {
			attachment = JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_ERICSSON,sincronizzazioneDao);

			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");
			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();
		}catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF ERICSSON","Errore"));
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return attachment;
	}
	
	public byte[] printVerbaleRadiantiA(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaRadiantiA(selectedSessione);
			attachment = printRadiantiA(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaRadiantiA(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaRadiantiA(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attach;
	}
	
	public JasperPrint creaRadiantiA(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );

			String realpath = servletContext.getRealPath("/resources/reports/radiantiA/VerbaleRadiantiA.jasper");
			String realpathLogoTim  =servletContext.getRealPath("/img/logojasper.png");
			String logoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(logoSaw);

			//LOGICA per dati generali radianti A
			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_SISTEMA_RADIANTI);
			List<ViwVerGenRadA> arrayDatiGeneraliTablet = reportDao.getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadA(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliRadiantiA,ConstReport.StrumentazioneUsataAllineamenoAntenneRadA);
			List<DatiGeneraliTabletRadiantiABean> arrayDatiGeneraliTabletRadABean = UtilityReports.getArrayDatiGeneraliRadABean(arrayDatiGeneraliTablet);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
			BufferedImage firmaTimDatiGenerali=null;
			BufferedImage firmaDittaDatiGenerali=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiA = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiA);
				firmaTimDatiGenerali = UtilityReports.getFirmaTim();
				firmaDittaDatiGenerali= UtilityReports.getFirmaDitta();
			} 
			//FINE LOGICA dati generali

			List<ViwVerGenRadA> arrayTitolo = reportDao.getListVerificheGenRadiantiA(selectedSessione.getCodiSessione());
			/*LOGICA per estrarre Elementi costituenteil sistema radiante codi elemento 133 caratteristica 781 */
			List<ViwVerGenRadA> arrayElementiCostituente = reportDao.getListElementiCostituente(selectedSessione.getCodiSessione());
			List<VerificheGenCelleDoppiaLabelBean> arrayElementiCostituenteBean= UtilityReports.getArrayVerificheGenCelleDoppiaLabel(arrayElementiCostituente);
			/*FINE LOGICA per estrarre Elementi costituenteil sistema radiante codi elemento 133 caratteristica 781*/

			/*LOGICA per estrarre Elementi generali sistema radiante   */
			List<ViwVerGenRadA> arrayElementiGenerali = reportDao.getListElementiGenerali(selectedSessione.getCodiSessione());
			List<VerificheGenRadiantiABean>  arrayElementiGeneraliBean= UtilityReports.getArrayVerificheGenRadiantiA(arrayElementiGenerali);
			/*FINE LOGICA per estrarre Elementi generali sistema radiante  */

			/*LOGICA per estrarre Elementi generali triplo valore sistema radiante  */
			List<ViwVerGenRadA> arrayElementiTriploValoreGenerali = reportDao.getListElementiTriploValoreGenerali(selectedSessione.getCodiSessione());
			List<VerificheGenValoreAttesoRadiantiABean>  arrayElementiTriploValoreGeneraliBean= UtilityReports.getArrayVerificheGenValoreAttesoRadiantiA(arrayElementiTriploValoreGenerali);
			/*FINE LOGICA per estrarre Elementi generali triplo valore sistema radiante*/

			/*LOGICA per estrarre Elementi generali chaining RET sistema radiante  */
			List<ViwVerGenRadA> arrayElementiChainingRETGenerali = reportDao.getListElementiChainingRETGenerali(selectedSessione.getCodiSessione());
			List<VerificheGenChainingRETRadiantiABean>  arrayElementiChainingRETGeneraliBean= UtilityReports.getArrayChainingRETRadiantiBean(arrayElementiChainingRETGenerali);
			/*FINE LOGICA per estrarre Elementi generali chaining RET sistema radiante*/

			//Esito Verbale Radiandi A
			List<ViwEsitoSurvey> arrayEsitoVerbaleRadiandiA = reportDao.getListVerbaleRadiandiA(selectedSessione.getCodiSessione());

			//				Riferimenti Radiandi A
			List<RiferimentiBean> arrayyRiferimentiBean= new ArrayList<>();
			if(arrayElementiCostituente.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiA = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayElementiCostituente.get(0).getCodiSurvey());
				arrayyRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiA);
			}

			JRBeanCollectionDataSource beanDatiGeneraliVerbaleRadiantiA = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletRadiantiA = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletRadABean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGenerali = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanElementiCostituenteRadiantiA = new JRBeanCollectionDataSource(arrayElementiCostituenteBean, false);
			JRBeanCollectionDataSource beanElementiGeneraliRadiantiA = new JRBeanCollectionDataSource(arrayElementiGeneraliBean, false);
			JRBeanCollectionDataSource beanElementiTriploValoreGeneraliRadiantiA = new JRBeanCollectionDataSource(arrayElementiTriploValoreGeneraliBean, false);
			JRBeanCollectionDataSource beanElementiChainingRETGeneraliRadiantiA = new JRBeanCollectionDataSource(arrayElementiChainingRETGeneraliBean, false);
			JRBeanCollectionDataSource beanEsitoVerbaleRadiandiA = new JRBeanCollectionDataSource(arrayEsitoVerbaleRadiandiA, false);
			JRBeanCollectionDataSource beanRiferimentiVerbaleRadiandiA = new JRBeanCollectionDataSource(arrayyRiferimentiBean, false);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("datiGeneraliVerbaleRadiantiA", beanDatiGeneraliVerbaleRadiantiA);
			parameters.put("datiGeneraliTabletRadiantiA", beanDatiGeneraliTabletRadiantiA);
			parameters.put("riferimentiDatiGenerali", beanRiferimentiDatiGenerali);
			parameters.put("firma_ditta_datiGenerali", firmaDittaDatiGenerali);
			parameters.put("firma_tim_datiGenerali", firmaTimDatiGenerali);
			parameters.put("titoloRadiantiA", beanTitolo);
			parameters.put("elementiCostituenteRadiantiA", beanElementiCostituenteRadiantiA);
			parameters.put("elementiGeneraliRadiantiA", beanElementiGeneraliRadiantiA);
			parameters.put("elementiTriploValoreGeneraliRadiantiA", beanElementiTriploValoreGeneraliRadiantiA);
			parameters.put("elementiChainingRETGeneraliRadiantiA", beanElementiChainingRETGeneraliRadiantiA);
			parameters.put("esitoVerbaleRadiantiA", beanEsitoVerbaleRadiandiA);
			parameters.put("riferimentiVerbaleRadiantiA", beanRiferimentiVerbaleRadiandiA);
			parameters.put("firma_tim", UtilityReports.getFirmaTim());
			parameters.put("firma_ditta", UtilityReports.getFirmaDitta());
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRISP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_A,sincronizzazioneDao));

			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
			e.printStackTrace();
		}
		return jasperPrint;
	}
	
	
	public byte[]  printRadiantiA( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try {
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_A,sincronizzazioneDao);

			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRISP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);

			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
		e.printStackTrace();
		}
		return attachment;
	}
			
	public byte[] printVerbaleRadiantiB(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaRadiantiB(selectedSessione);
			attachment = printRadiantiB(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaRadiantiB(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaRadiantiB(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return attach;
	}
	
	public JasperPrint creaRadiantiB(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );
			//

			String realpath = servletContext.getRealPath("/resources/reports/radiantiB/VerbaleRadiantiB.jasper");
			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String logoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(logoSaw);
			

			//Logica per array dati generali B
			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_SISTEMA_RADIANTI);
			List<ViwMisureGenRadB> arrayDatiGeneraliTablet = reportDao.getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadB(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliRadiantiB,ConstReport.StrumentazioneUsataAllineamenoAntenneRadB);
			List<DatiGeneraliTabletRadiantiBBean> arrayDatiGeneraliTabletRadBBean = UtilityReports.getArrayTabletDatiGeneraliRadBBean(arrayDatiGeneraliTablet);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
			BufferedImage firmaTimDatiGenerali=null;
			BufferedImage firmaDittaDatiGenerali=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiB = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiB);
				firmaTimDatiGenerali = UtilityReports.getFirmaTim();
				firmaDittaDatiGenerali = UtilityReports.getFirmaDitta();
			} 
			//fine logica

			List<ViwMisureGenRadB> arrayTitolo = reportDao.getListMisureGenRadiantiB(selectedSessione.getCodiSessione());


			/*LOGICA per estrarre Condivisione Calata sistema radiante codi elemento 21 */
			List<ViwMisureGenRadB> arrayCondivisioneCalata = reportDao.getListCondivisioneCalataRadiantiB(selectedSessione.getCodiSessione());
			List<MisureDiCaratterizzazioneCondivisioneCalataBean> arrayElementiCostituenteBean= UtilityReports.getArrayRadiantiBCondivisioneCalataBean(arrayCondivisioneCalata);
			/*FINE LOGICA per estrarre Condivisione Calata sistema radiante codi elemento 21*/

			/*LOGICA per estrarre Misure Generali radiante B */
			List<ViwMisureGenRadB> arrayMisure = reportDao.getListMisureGeneraliRadiantiB(selectedSessione.getCodiSessione());
			List<MisureDiCaratterizzazioneGeneraliRadiantiBBean> arrayMisureBean= UtilityReports.getArrayRadiantiBMisureGeneraliBean(arrayMisure);
			/*FINE LOGICA per estrarre Misure Generali radiante B*/

			/*LOGICA per estrarre Misure Generali radiante B  Tipo Cavo codiCaratteristica 330*/
			List<ViwMisureGenRadB> arrayMisureTipoCavo = reportDao.getListMisureTipoCavoRadiantiB(selectedSessione.getCodiSessione());
			List<MisureDiCaratterizzazioneTipoCavoRadiantiBBean> arrayMisureTipoCavoBean= UtilityReports.getArrayRadiantiBMisureTipoCavoBean(arrayMisureTipoCavo);
			/*FINE LOGICA per estrarre Misure Generali radiante B  Tipo Cavo codiCaratteristica 330*/
			
			/*LOGICA per estrarre Misure Generali radiane B Calibrazione codiElemento 155*/
			List<ViwMisureGenRadB> arrayMisureCalibrazione = reportDao.getListMisureCalibrazioneRadiantiB(selectedSessione.getCodiSessione());
			List<MisureDiCaratterizzazioneCalibrazioneRadiantiBBean> arrayMisureCalibrazioneBean=UtilityReports.getArrayRadiantiBMisureCalibrazioneBean(arrayMisureCalibrazione);
			/*FINE LOGICA per estrarre Misure Generali radiane B Calibrazione codiElemento 155*/

			//Esito Verbale Radiandi B
			List<ViwEsitoSurvey> arrayEsitoVerbaleRadiandiB = reportDao.getListVerbaleRadiandiB(selectedSessione.getCodiSessione());

			//				Riferimenti Radiandi B
			List<RiferimentiBean> arrayyRiferimentiBean= new ArrayList<>();
			if(arrayMisureTipoCavo.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiC = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayMisureTipoCavo.get(0).getCodiSurvey());
				arrayyRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiC);
			}
			

			JRBeanCollectionDataSource beanDatiGeneraliVerbaleRadiantiB = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletVerbaleRadiantiB = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletRadBBean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGeneraliVerbaleRadiantiB = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanCondivisioneCalata = new JRBeanCollectionDataSource(arrayElementiCostituenteBean, false);
			JRBeanCollectionDataSource beanMisureGenerali = new JRBeanCollectionDataSource(arrayMisureBean, false);
			JRBeanCollectionDataSource beanEsitoVerbaleRadiandiB = new JRBeanCollectionDataSource(arrayEsitoVerbaleRadiandiB, false);
			JRBeanCollectionDataSource beanMisureTipoCavoVerbaleRadiandiB = new JRBeanCollectionDataSource(arrayMisureTipoCavoBean, false);
			JRBeanCollectionDataSource beanCalibrazioneRadiantiB = new JRBeanCollectionDataSource(arrayMisureCalibrazioneBean, false);
			JRBeanCollectionDataSource beanRiferimentiVerbaleRadiandiB = new JRBeanCollectionDataSource(arrayyRiferimentiBean, false);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("firma_tim", UtilityReports.getFirmaTim());
			parameters.put("firma_ditta", UtilityReports.getFirmaDitta());
			parameters.put("datiGeneraliVerbaleRadiantiB", beanDatiGeneraliVerbaleRadiantiB);
			parameters.put("datiGeneraliTabletVerbaleRadiantiB", beanDatiGeneraliTabletVerbaleRadiantiB);
			parameters.put("riferimentiDatiGeneraliVerbaleRadiantiB", beanRiferimentiDatiGeneraliVerbaleRadiantiB);
			parameters.put("firma_tim_datiGenerali", firmaTimDatiGenerali);
			parameters.put("firma_ditta_datiGenerali", firmaDittaDatiGenerali);
			parameters.put("titoloRadiantiB", beanTitolo);
			parameters.put("condivisioneCalataRadiantiB", beanCondivisioneCalata);
			parameters.put("misureGeneraliRadiantiB", beanMisureGenerali);
			parameters.put("esitoVerbaleRadiantiB", beanEsitoVerbaleRadiandiB);
			parameters.put("tipoCavoVerbaleRadiantiB", beanMisureTipoCavoVerbaleRadiandiB);
			parameters.put("calibrazioneRadiantiB", beanCalibrazioneRadiantiB);
			parameters.put("riferimentiVerbaleRadiantiB", beanRiferimentiVerbaleRadiandiB);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRROS"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_B,sincronizzazioneDao));
			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti B","Errore"));
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}
	
	public byte[]  printRadiantiB( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try {
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_B,sincronizzazioneDao);
			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRROS"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
		e.printStackTrace();
		}
	
	return attachment;
	}
	
	public byte[] printVerbaleRadiantiC(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaRadiantiC(selectedSessione);
			attachment = printRadiantiC(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaRadiantiC(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaRadiantiC(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return attach;
	}
	
	public JasperPrint creaRadiantiC(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );
			//

			String realpath = servletContext.getRealPath("/resources/reports/radiantiC/VerbaleRadiantiC.jasper");
			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String realpathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(realpathLogoSaw);

			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_SISTEMA_RADIANTI);
			List<ViwConfGenRadC> arrayTitolo = reportDao.getListConRadiantiC(selectedSessione.getCodiSessione());

			//				/*LOGICA per estrarre Configurazioni celle caratteristica 783 */
			List<ViwConfGenRadC> arrayConfCelle = reportDao.getListConfCelleRadiantiC(selectedSessione.getCodiSessione());
			List<ConfigurazioneCelleRadiantiCBean> arrayConfCelleBean= UtilityReports.getArrayConfCelleBean(arrayConfCelle);
			//				/*FINE LOGICA per estrarre Configurazioni celle caratteristica 783 */
			//				
			//				/*LOGICA  per estrarre Antenne installate caratteristica 340 */
			List<ViwConfGenRadC> arrayAntenneInstallate = reportDao.getListAntenneInstallateRadiantiC(selectedSessione.getCodiSessione());
			List<AntenneInstallateRadiantiCBean> arrayAntenneInstallateBean= UtilityReports.getArrayAntenneInstallateBean(arrayAntenneInstallate);
			//				/*FINE LOGICA per estrarre Antenne installate caratteristica 340*/

			/*LOGICA  per estrarre Dettaglio tecnologie installate codiElemento 130,23 */
			List<ViwConfGenRadC> arrayDettaglioTecnologie = reportDao.getListDettaglioTecnologieRadiantiC(selectedSessione.getCodiSessione());
			List<DettaglioTecnologieRadiantiCBean> arrayDettaglioTecnologieBean= UtilityReports.getArrayDettaglioTecnologieBean(arrayDettaglioTecnologie);
			/*FINE LOGICA  per estrarre Dettaglio tecnologie installate codiElemento 130,23 */


			//				Esito Verbale Radiandi C
			List<ViwEsitoSurvey> arrayEsitoVerbaleRadiandiC = reportDao.getListVerbaleRadiandiC(selectedSessione.getCodiSessione());

			//				Riferimenti Radiandi C
			List<RiferimentiBean> arrayyRiferimentiBean= new ArrayList<>();
			if(arrayAntenneInstallate.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiC = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayAntenneInstallate.get(0).getCodiSurvey());
				arrayyRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiC);
			}

			JRBeanCollectionDataSource beanDatiGeneraliVerbaleRadiantiC = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanConfCelle = new JRBeanCollectionDataSource(arrayConfCelleBean, false);
			JRBeanCollectionDataSource beanAntenneInstallate= new JRBeanCollectionDataSource(arrayAntenneInstallateBean, false);
			JRBeanCollectionDataSource beanDettaglioTecnologie= new JRBeanCollectionDataSource(arrayDettaglioTecnologieBean, false);
			JRBeanCollectionDataSource beanEsitoVerbaleRadiandiC = new JRBeanCollectionDataSource(arrayEsitoVerbaleRadiandiC, false);
			JRBeanCollectionDataSource beanRiferimentiVerbaleRadiandiC = new JRBeanCollectionDataSource(arrayyRiferimentiBean, false);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("firma_tim", UtilityReports.getFirmaTim());
			parameters.put("firma_ditta", UtilityReports.getFirmaDitta());
			parameters.put("datiGeneraliVerbaleRadiantiC", beanDatiGeneraliVerbaleRadiantiC);
			parameters.put("titoloRadiantiC", beanTitolo);
			parameters.put("confCelleVerbaleRadiantiC", beanConfCelle);
			parameters.put("antenneInstallateVerbaleRadiantiC", beanAntenneInstallate);
			parameters.put("dettaglioTecnologieVerbaleRadiantiC", beanDettaglioTecnologie);
			parameters.put("esitoVerbaleRadiantiC", beanEsitoVerbaleRadiandiC);
			parameters.put("riferimentiVerbaleRadiantiC", beanRiferimentiVerbaleRadiandiC);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRRET"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_C,sincronizzazioneDao));
			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti C","Errore"));
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}
	
	public byte[]  printRadiantiC( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try {
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_C,sincronizzazioneDao);
			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRRET"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return attachment;
	}

	
	public byte[] printVerbaleRadiantiD(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaRadiantiD(selectedSessione);
			attachment = printRadiantiD(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaRadiantiD(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaRadiantiD(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return attach;
	}
	
	public JasperPrint creaRadiantiD(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {

			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );

			String realpath = servletContext.getRealPath("/resources/reports/radiantiD/VerbaleRadiantiD.jasper");
			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String pathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(pathLogoSaw);


			//logica dati generali
			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_SISTEMA_RADIANTI);
			List<ViwMisureDiPimRadD> arrayDatiGeneraliTablet = reportDao.getListVerificheGenRadiantiABySessioneTipoOggettoElementoRadD(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliRadiantiD,ConstReport.StrumentazioneUsataAllineamenoAntenneRadD);
			List<DatiGeneraliTabletRadiantiDBean> arrayDatiGeneraliTabletRadDBean = UtilityReports.getArrayTabletDatiGeneraliRadDBean(arrayDatiGeneraliTablet);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
			BufferedImage firmaTimDatiGenerali=null;
			BufferedImage firmaDittaDatiGenerali=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiD = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiD);
				firmaTimDatiGenerali = UtilityReports.getFirmaTim();
				firmaDittaDatiGenerali = UtilityReports.getFirmaDitta();
			} 
			//fine logica






			List<ViwMisureDiPimRadD> arrayTitolo = reportDao.getListConRadiantiD(selectedSessione.getCodiSessione());
			List<ViwMisureDiPimRadD> arrayTitoloMisureDistance = reportDao.getListConRadiantiD(selectedSessione.getCodiSessione());

			/*LOGICA per estrarre Misura PIM  caratteristiche 828 758 */
			List<ViwMisureDiPimRadD> arrayMisuraPIM = reportDao.getListMisuraPim(selectedSessione.getCodiSessione());
			List<CelleRadiantiDBean> arrayyMisuraPIMBean= UtilityReports.getArrayCelleRadiantiDBean(arrayMisuraPIM);
			/*FINE LOGICA per estrarre Configurazioni celle caratteristica 783 */

			/*LOGICA per estrarre Misura PIM  codielemento 131 */
			List<ViwMisureDiPimRadD> arrayMisuraSistemaPIM = reportDao.getListMisuraSistemaPim(selectedSessione.getCodiSessione());
			List<MisurePimSistemaRadiantiDBean> arrayyMisuraSistemaPIMBean= UtilityReports.getArrayMisurePimSistemaRadiantiDBean(arrayMisuraSistemaPIM);
			/*FINE LOGICA per estrarre Misura PIM  codielemento 131  */

			/*LOGICA per estrarre MisuraDTP Valore di PIM  codiCaratteristica 762 */
			List<ViwMisureDiPimRadD> arrayMisuraDTPValorePim = reportDao.getListMisuraDtpValoreDiPim(selectedSessione.getCodiSessione());
			List<MisureDtpValorePimSistemaRadiantiDBean> arrayyMisuraDTPValorePimBean= UtilityReports.getArrayMisuraDtpRadiantiDBean(arrayMisuraDTPValorePim);
			/*FINE LOGICA per estrarre MisuraDTP Valore di PIM  codiCaratteristica 762  */

			/*LOGICA per estrarre MisuraDTP Valore di PIM  codiCaratteristica 763 */
			List<ViwMisureDiPimRadD> arrayMisuraDTPDistanza = reportDao.getListMisuraDtpDistanza(selectedSessione.getCodiSessione());
			List<MisureDtpValorePimSistemaRadiantiDBean> arrayyMisuraDTPDistanzaBean= UtilityReports.getArrayMisuraDtpRadiantiDBean(arrayMisuraDTPDistanza);
			/*FINE LOGICA per estrarre MisuraDTP Valore di PIM  codiCaratteristica 762  */


			//				Esito Verbale Radiandi D
			List<ViwEsitoSurvey> arrayEsitoVerbaleRadiandiD = reportDao.getListVerbaleRadiandiD(selectedSessione.getCodiSessione());

			//				Riferimenti Radiandi D
			List<RiferimentiBean> arrayyRiferimentiBean= new ArrayList<>();
			if(arrayMisuraDTPDistanza.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiD = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayMisuraDTPDistanza.get(0).getCodiSurvey());
				arrayyRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiD);
			}
			JRBeanCollectionDataSource beanDatiGeneraliVerbaleRadiantiD = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanArrayDatiGeneraliTabletRadDBean = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletRadDBean, false);
			JRBeanCollectionDataSource beanArrayRiferimentiDatiGeneraliBean = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanMisurePim = new JRBeanCollectionDataSource(arrayyMisuraPIMBean, false);
			JRBeanCollectionDataSource beanMisureSistemaPim= new JRBeanCollectionDataSource(arrayyMisuraSistemaPIMBean, false);
			JRBeanCollectionDataSource beanTitoloMisureDistance = new JRBeanCollectionDataSource(arrayTitoloMisureDistance, false);
			JRBeanCollectionDataSource beanMisureDtpValorePim = new JRBeanCollectionDataSource(arrayyMisuraDTPValorePimBean, false);
			JRBeanCollectionDataSource beanMisuraDTPDistanza = new JRBeanCollectionDataSource(arrayyMisuraDTPDistanzaBean, false);
			JRBeanCollectionDataSource beanEsitoVerbaleRadiandiD = new JRBeanCollectionDataSource(arrayEsitoVerbaleRadiandiD, false);
			JRBeanCollectionDataSource beanRiferimentiVerbaleRadiandiD = new JRBeanCollectionDataSource(arrayyRiferimentiBean, false);


			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("firma_tim", UtilityReports.getFirmaTim());
			parameters.put("firma_ditta", UtilityReports.getFirmaDitta());
			parameters.put("datiGeneraliVerbaleRadiantiD", beanDatiGeneraliVerbaleRadiantiD);
			parameters.put("datiGeneraliTabletVerbaleRadiantiD", beanArrayDatiGeneraliTabletRadDBean);
			parameters.put("datiGeneraliRiferimentiVerbaleRadiantiD", beanArrayRiferimentiDatiGeneraliBean);
			parameters.put("firma_tim_dati_generali",firmaTimDatiGenerali );
			parameters.put("firma_ditta_dati_generali",firmaDittaDatiGenerali );
			parameters.put("titoloRadiantiD", beanTitolo);
			parameters.put("misurePimRadiantiD", beanMisurePim);
			parameters.put("misureSistemaPimRadiantiD", beanMisureSistemaPim);
			parameters.put("titoloMisureDistanceRadiantiD", beanTitoloMisureDistance);
			parameters.put("misureDtpValorePimRadiantiD", beanMisureDtpValorePim);
			parameters.put("misureDtpDistanzaRadiantiD", beanMisuraDTPDistanza);
			parameters.put("esitoVerbaleRadiantiD", beanEsitoVerbaleRadiandiD);
			parameters.put("riferimentiVerbaleRadiantiD", beanRiferimentiVerbaleRadiandiD);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VPIM2"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_D,sincronizzazioneDao));
			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti D","Errore"));
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
		
	}
	
	public byte[]  printRadiantiD( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try{
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_D,sincronizzazioneDao);
			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VPIM2"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return attachment;
	}
	
	private String getData(Date data){
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		return df2.format(data);
	}

	
	public byte[] printVerbaleRadiantiE(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaRadiantiE(selectedSessione);
			attachment = printRadiantiE(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaRadiantiE(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaRadiantiE(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return attach;
	}
	
	public JasperPrint creaRadiantiE(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );

			String realpath = servletContext.getRealPath("/resources/reports/radiantiE/VerbaleRadiantiE.jasper");
			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String pathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(pathLogoSaw);

			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.VERIFICA_APOGEO);
			List<ViwRadiantiE> arrayTitolo = reportDao.getListConRadiantiE(selectedSessione.getCodiSessione());

			/*LOGICA per estrarre MisureApogeoCelle caratteristiche 808 809 */
			List<ViwRadiantiE> arrayMisureApogeoCelle = reportDao.getListCelleRadiantiE(selectedSessione.getCodiSessione());
			List<MisureApogeoCelleBean> arrayyCelleRadiantiEBean= UtilityReports.getArrayMisureApogeoCelleBean(arrayMisureApogeoCelle);
			/*FINE LOGICA per estrarre MisureApogeoCelle caratteristiche 808 809 */

			/*LOGICA per estrarre MisureApogeoAntenne elemento 135 */
			List<ViwRadiantiE> arrayMisureApogeoAntenna = reportDao.getListAntennaRadiantiE(selectedSessione.getCodiSessione());
			List<MisureApogeoAntennaBean> arrayyAntennaRadiantiEBean= UtilityReports.getArrayMisureApogeoAntennaBean(arrayMisureApogeoAntenna);
			/*FINE LOGICA per estrarre MisureApogeoAntenne elemento 135 */


			//				Esito Verbale Radiandi E
			List<ViwEsitoSurvey> arrayEsitoVerbaleRadiandiE = reportDao.getListVerbaleRadiandiE(selectedSessione.getCodiSessione());

			//				Riferimenti Radiandi E
			List<RiferimentiBean> arrayyRiferimentiRadiantiEBean= new ArrayList<>();
			if(arrayMisureApogeoAntenna.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimentiVerbaleRadiandiE = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayMisureApogeoAntenna.get(0).getCodiSurvey());
				arrayyRiferimentiRadiantiEBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerbaleRadiandiE);
			}
			JRBeanCollectionDataSource beanDatiGeneraliVerbaleRadiantiE = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanCelleRadiantiE = new JRBeanCollectionDataSource(arrayyCelleRadiantiEBean, false);
			JRBeanCollectionDataSource beanAntennaRadiantiE = new JRBeanCollectionDataSource(arrayyAntennaRadiantiEBean, false);


			JRBeanCollectionDataSource beanEsitoVerbaleRadiandiE = new JRBeanCollectionDataSource(arrayEsitoVerbaleRadiandiE, false);
			JRBeanCollectionDataSource beanRiferimentiVerbaleRadiandiE = new JRBeanCollectionDataSource(arrayyRiferimentiRadiantiEBean, false);


			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			parameters.put("firma_tim", UtilityReports.getFirmaTim());
			parameters.put("datiGeneraliVerbaleRadiantiE", beanDatiGeneraliVerbaleRadiantiE);
			parameters.put("titoloRadiantiE", beanTitolo);
			parameters.put("celleRadiantiE", beanCelleRadiantiE);
			parameters.put("antennaRadiantiE", beanAntennaRadiantiE);
			parameters.put("esitoVerbaleRadiantiE", beanEsitoVerbaleRadiandiE);
			parameters.put("riferimentiVerbaleRadiantiE", beanRiferimentiVerbaleRadiandiE);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"RLVAP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_E,sincronizzazioneDao));
			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti E","Errore"));
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}
	
	public byte[]  printRadiantiE( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try{
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_E,sincronizzazioneDao);
			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"RLVAP"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	return attachment;

		
	}

	public byte[] printApparatoNokia(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaNokia(selectedSessione);
			attachment = printNokia(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaNokia(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaNokia(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return attach;
	}
	

	public JasperPrint creaNokia(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try{
			ArrayList<Object> collection = new ArrayList<Object>();
		collection.add("");
		JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );
		//

		String realpath = servletContext.getRealPath("/resources/reports/nokia/VerbaleApparatoNokia.jasper");
		String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
		String pathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
		File fileLogoTim = new File(realpathLogoTim);
		File fileLogoSaw = new File(pathLogoSaw);

		List<ViwApparatoNokia> arrayTitolo = reportDao.getListApparatoNokia(selectedSessione.getCodiSessione());
		
		//LOGICA DATI GENERALI
		List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
		arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_APPARATO);
		List<ViwApparatoNokia> arrayDatiGeneraliTablet = reportDao.getListApparatoNokiaBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletNokia);
		List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletBean = UtilityReports.getArrayTabletDatiGeneraliApparatoNokiaBean(arrayDatiGeneraliTablet);
		List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
		BufferedImage firmaTimDatiGenerali=null;
		BufferedImage firmaDittaDatiGenerali=null;
		if(arrayDatiGeneraliTablet.size()>0){
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
			arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
			firmaTimDatiGenerali = UtilityReports.getFirmaTim();
			firmaDittaDatiGenerali = UtilityReports.getFirmaDitta();
		} 
		//fine logica 
		

		//				/*LOGICA Verifiche di Installazione a carico della funzione di open access codi tipo oggetto 7 */
		List<ViwApparatoNokia> arrayVerInst = reportDao.getListApparatoNokiaBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneOpenAccessNokia);
		List<ViwApparatoNokia> arrayTestFlussi = UtilityReports.getNokiaArrayVerinst(arrayVerInst);
		List<TestFlussiBean> arrayTestFlussiBean = UtilityReports.getArrayTestFlussiBean(arrayTestFlussi);
		//				/*FINE Verifiche di Installazione a carico della funzione di open access codi tipo oggetto 7 */

		//Esito Verifiche di installazione nokia
		List<ViwEsitoSurvey> arrayEsitoVerInst = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneOpenAccessNokia);

		//Riferimenti Verifiche di installazione nokia
		List<RiferimentiBean> arrayRiferimentiBean= new ArrayList<>();
		if (arrayVerInst.size()>0) {
			List<ViwRiferimentiFirma> arrayRiferimentiVerInst = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerInst.get(0).getCodiSurvey());
			arrayRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerInst);
		}
		BufferedImage firmaTimVerInsBean = UtilityReports.getFirmaTim();
		BufferedImage firmaDittaVerInsBean = UtilityReports.getFirmaDitta();


		//LOGICA ALLARMI ESTERNI 2G codi_elemento 37
		List<ViwVerFunzAllarmi> arrayAllarmi2G = reportDao.getListViwVerFunzBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.AllarmiEsterniGestitiDa2G);
		List<AllarmiEsterni2GBean> arrayAllarmiEsterni2GBean = UtilityReports.getArrayAllarmiNokia(arrayAllarmi2G);
		//FINE

		//LOGICA ALLARMI ESTERNI 3G codi_elemento 38
		List<ViwVerFunzAllarmi> arrayAllarmi3G = reportDao.getListViwVerFunzBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.AllarmiEsterniGestitiDa3G);
		List<AllarmiEsterni2GBean> arrayAllarmiEsterni3GBean = UtilityReports.getArrayAllarmiNokia(arrayAllarmi3G);
		//FINE

		//LOGICA MISURE DI CARATTERIZZAZIONE APPARATO codi_elemento 39
		List<ViwApparatoNokia> arrayMisCarattApparatoNokiaBean = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.MisureDiCaratterizzazioneDiApparato);
		//FINE LOGICA

		//LOGICA VERIFICA MHA codiElemento 40
		List<ViwApparatoNokia> arrayVerificaMHA = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.VerificaMHANokia);
		List<VerificaMHABean> arrayVerificaMHABean = UtilityReports.getArrayVerificaMHABean(arrayVerificaMHA);
		//FINE LOGICA


		//LOGICA VERIFICA RET codiElemento 41
		List<ViwApparatoNokia> arrayVerificaRET = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.VerificaRETNokia);
		List<VerificaRETBean> arrayVerificaRETBean = UtilityReports.getArrayVerificaRETBean(arrayVerificaRET);
		//FINE LOGICA

		//LOGICA CHIUSURA COMMISSIONING codiElemento 42
		List<ViwApparatoNokia> arrayChiusuraCommissioningBean = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia,ConstReport.ChiusuraCommissioningNokia);
		//FINE LOGICA

		//Esito Verifiche funzionali commissioning nokia
		List<ViwEsitoSurvey> arrayEsitoVerFunzCommissioningBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningNokia);

		//Riferimenti Verifiche FUNZIONALI COMMISIONING nokia
		List<RiferimentiBean> arrayRiferimentiFunzionaliCommissioningBean= new ArrayList<>();
		if (arrayVerificaRET.size()>0) {
			List<ViwRiferimentiFirma> arrayRiferimentiFunzCommissioning = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificaRET.get(0).getCodiSurvey());
			arrayRiferimentiFunzionaliCommissioningBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiFunzCommissioning);
		}
		BufferedImage firmaTimVerFunzBean = UtilityReports.getFirmaTim();
		BufferedImage firmaDittaVerFunzBean = UtilityReports.getFirmaDitta();

		//Logica per Notifica allarmi codiElemento 43 e codi tipo oggetto 10
		List<ViwApparatoNokia> arrayNotificaAllarmiBean = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioNokia,ConstReport.NotificaAllarmiNokia);
		//Fine logica

		//Logica per Continuita flussi codiElemento 44 e codi tipo oggetto 10
		List<ViwApparatoNokia> arrayContinuitaFlussiBean = reportDao.getListApparatoNokiaBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioNokia,ConstReport.ContinuitaFlussiNokia);
		//Fine logica

		//Esito prove funzionali di esercizio nokia
		List<ViwEsitoSurvey> arrayEsitoProveFunzEserBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioNokia);
		
		//Riferimenti ProveFunzionali di esercizio
		List<RiferimentiBean> arrayRiferimentiProveFunzEserBean= new ArrayList<>();
		if (arrayContinuitaFlussiBean.size()>0) {
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayContinuitaFlussiBean.get(0).getCodiSurvey());
			arrayRiferimentiProveFunzEserBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
		}
		BufferedImage firmaTimProveFunzEserBean = UtilityReports.getFirmaTim();
		BufferedImage firmaDittaProveFunzEserBean = UtilityReports.getFirmaDitta();

		//Logica per prove di servizio tipologiaOggetto 11 codiElemnto 45
		//N.B si usa ViwVerFunzAllarmi perch� usa la stessa logica
		List<ViwVerFunzAllarmi> arrayProveDiServizio = reportDao.getListViwVerFunzBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioNokia,ConstReport.ProveDiServizioNokia);
		List<ProveDiServizioBean> arrayProveDiServizioBean = UtilityReports.getArrayProveDiServizio(arrayProveDiServizio);
		//fine logica

		//Esito Prove funzionali di servizio nokia
		List<ViwEsitoSurvey> arrayEsitoProveFunzServizioBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioNokia);

		//Riferimenti ProveFunzionali di servizio
		List<RiferimentiBean> arrayRiferimentiProveFunzServizioBean= new ArrayList<RiferimentiBean>();
		BufferedImage firmaTimProveFunzServizioBean;
		BufferedImage firmaDittaProveFunzServizioBean;
		
		if (arrayProveDiServizio.size()>0) {
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayProveDiServizio.get(0).getCodiSurvey());
			if(arrayRiferimenti.size()>0){
				arrayRiferimentiProveFunzServizioBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimProveFunzServizioBean = UtilityReports.getFirmaTim();
				firmaDittaProveFunzServizioBean = UtilityReports.getFirmaDitta();
			}else{
				firmaTimProveFunzServizioBean = null;
				firmaDittaProveFunzServizioBean = null;
			}
		} 
		else{
			//Cerco il codiSurvey nel caso in cui non siano stati inseriti dati ma sia presente la firma, al fine di permettere la sua visualizzazione
			TabSurvey tabSurveyProveDiServizioNokia = reportDao.getSurveyBySessioneAndTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioNokia);
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(tabSurveyProveDiServizioNokia.getCodiSurvey());
			if(arrayRiferimenti.size()>0){
				arrayRiferimentiProveFunzServizioBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimProveFunzServizioBean = UtilityReports.getFirmaTim();
				firmaDittaProveFunzServizioBean = UtilityReports.getFirmaDitta();
			}else{
				firmaTimProveFunzServizioBean = null;
				firmaDittaProveFunzServizioBean = null;
			}
		}

		
		
		//logica dati generali WIPM
		List<ViwSessioni> arrayDatiGeneraliWIPM = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
		List<ViwApparatoNokia> arrayDatiGeneraliTabletWIPM = reportDao.getListApparatoNokiaBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletNokiaWipm);
		List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletVerbaleApparatoNokiaWIPMBean = UtilityReports.getArrayTabletDatiGeneraliApparatoNokiaBean(arrayDatiGeneraliTabletWIPM);
		List<RiferimentiBean> arrayRiferimentiDatiGeneraliVerbaleApparatoNokiaWIPMBean= new ArrayList<>();
		BufferedImage firmaTimDatiGeneraliWIPM=null;
		BufferedImage firmaDittaDatiGeneraliWIPM=null;
		if(arrayDatiGeneraliTablet.size()>0){
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTabletWIPM.get(0).getCodiSurvey());
			arrayRiferimentiDatiGeneraliVerbaleApparatoNokiaWIPMBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
			firmaTimDatiGeneraliWIPM = UtilityReports.getFirmaTim();
			firmaDittaDatiGeneraliWIPM = UtilityReports.getFirmaDitta();
		} 
		//fine logica dati generali wipm

		//logica Verifiche installazione WIPM Nokia codi tipo oggetto 6
		List<ViwApparatoNokia> arrayVerificheInstWIPMBean = reportDao.getListApparatoNokiaBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneWIPMNokia);
		List<VerInstWIPMBean>  arrayValoriMisuratiWIPM = UtilityReports.getListValoriMisuratiWIPM(arrayVerificheInstWIPMBean);
		//fine logica

		//Esito Verifiche di Installazione WIPM
		List<ViwEsitoSurvey> arrayEsitoVerInstWIPMBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneWIPMNokia);

		//Riferimenti ProveFunzionali di servizio
		List<RiferimentiBean> arrayRiferimentiVerInstWIPMBean= new ArrayList<RiferimentiBean>();
		BufferedImage firmaTimVerInstWIPMBean;
		BufferedImage firmaDittaVerInstWIPMBean;
		if (arrayVerificheInstWIPMBean.size()>0) {
			List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificheInstWIPMBean.get(0).getCodiSurvey());
			arrayRiferimentiVerInstWIPMBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
			firmaTimVerInstWIPMBean = UtilityReports.getFirmaTim();
			firmaDittaVerInstWIPMBean = UtilityReports.getFirmaDitta();
		} else {
			firmaTimVerInstWIPMBean = null;
			firmaDittaVerInstWIPMBean = null;
		}

		JRBeanCollectionDataSource beanDatiGeneraliVerbaleApparatoNokia = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
		JRBeanCollectionDataSource beanDatiGeneraliVerbaleApparatoNokiaWIPM = new JRBeanCollectionDataSource(arrayDatiGeneraliWIPM, false);
		JRBeanCollectionDataSource beanDatiGeneraliTabletVerbaleApparatoNokia = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletBean, false);
		JRBeanCollectionDataSource beanRiferimentiDatiGeneraliVerbaleApparatoNokia = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);
		JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
		JRBeanCollectionDataSource beanVerInst = new JRBeanCollectionDataSource(arrayVerInst, false);
		JRBeanCollectionDataSource beanTestFlussi= new JRBeanCollectionDataSource(arrayTestFlussiBean, false);
		JRBeanCollectionDataSource beanEsitoVerInstNokia = new JRBeanCollectionDataSource(arrayEsitoVerInst, false);
		JRBeanCollectionDataSource beanRiferimentiVerInstNokia = new JRBeanCollectionDataSource(arrayRiferimentiBean, false);
		JRBeanCollectionDataSource beanAllarmiEsterni2G = new JRBeanCollectionDataSource(arrayAllarmiEsterni2GBean, false);
		JRBeanCollectionDataSource beanAllarmiEsterni3G = new JRBeanCollectionDataSource(arrayAllarmiEsterni3GBean, false);
		JRBeanCollectionDataSource beanMisCarattApparato = new JRBeanCollectionDataSource(arrayMisCarattApparatoNokiaBean, false);
		JRBeanCollectionDataSource beanVerificaMHA = new JRBeanCollectionDataSource(arrayVerificaMHABean, false);
		JRBeanCollectionDataSource beanVerificaRET = new JRBeanCollectionDataSource(arrayVerificaRETBean, false);
		JRBeanCollectionDataSource beanChiusuraCommissioning = new JRBeanCollectionDataSource(arrayChiusuraCommissioningBean, false);
		JRBeanCollectionDataSource beanEsitoVerFunzCommissioning = new JRBeanCollectionDataSource(arrayEsitoVerFunzCommissioningBean, false);
		JRBeanCollectionDataSource beanRiferimentiVerFunzCommissioning = new JRBeanCollectionDataSource(arrayRiferimentiFunzionaliCommissioningBean, false);
		JRBeanCollectionDataSource beanNotificaAllarmi = new JRBeanCollectionDataSource(arrayNotificaAllarmiBean, false);
		JRBeanCollectionDataSource beanContinuitaFlussi = new JRBeanCollectionDataSource(arrayContinuitaFlussiBean, false);
		JRBeanCollectionDataSource beanEsitoProveFunzEser = new JRBeanCollectionDataSource(arrayEsitoProveFunzEserBean, false);
		JRBeanCollectionDataSource beanRiferimentiProveFunzEser = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzEserBean, false);
		JRBeanCollectionDataSource beanArrayProveDiServizio = new JRBeanCollectionDataSource(arrayProveDiServizioBean, false);
		JRBeanCollectionDataSource beanEsitoProveFunzServizio = new JRBeanCollectionDataSource(arrayEsitoProveFunzServizioBean, false);
		JRBeanCollectionDataSource beanRiferimentiProveFunzServizio = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzServizioBean, false);
		JRBeanCollectionDataSource verificheInstWIPMBean = new JRBeanCollectionDataSource(arrayVerificheInstWIPMBean, false);
		JRBeanCollectionDataSource valoriMisuratiWIPMBean = new JRBeanCollectionDataSource(arrayValoriMisuratiWIPM, false);
		JRBeanCollectionDataSource beanEsitoVerInstWIPM = new JRBeanCollectionDataSource(arrayEsitoVerInstWIPMBean, false);
		JRBeanCollectionDataSource beanRiferimentiVerInstWIPM = new JRBeanCollectionDataSource(arrayRiferimentiVerInstWIPMBean, false);
		JRBeanCollectionDataSource beanDatiGeneraliTabletVerbaleApparatoNokiaWIPM = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletVerbaleApparatoNokiaWIPMBean, false);
		JRBeanCollectionDataSource beanRiferimentiDatiGeneraliVerbaleApparatoNokiaWIPM = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliVerbaleApparatoNokiaWIPMBean, false);




		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("logo_tim", fileLogoTim);
		parameters.put("logo_saw", fileLogoSaw);
		//dati generali
		parameters.put("datiGeneraliVerbaleApparatoNokia", beanDatiGeneraliVerbaleApparatoNokia);
		parameters.put("datiGeneraliTabletVerbaleApparatoNokia", beanDatiGeneraliTabletVerbaleApparatoNokia);
		parameters.put("datiGeneraliRiferimentiVerbaleApparatoNokia", beanRiferimentiDatiGeneraliVerbaleApparatoNokia);
		parameters.put("firma_tim_datiGenerali",firmaTimDatiGenerali );
		parameters.put("firma_ditta_datiGenerali", firmaDittaDatiGenerali);
		//Verifiche installazione
		parameters.put("firma_tim_verIns",firmaTimVerInsBean );
		parameters.put("firma_ditta_verIns", firmaDittaVerInsBean);
		parameters.put("datiGeneraliVerbaleApparatoNokia", beanDatiGeneraliVerbaleApparatoNokia);
		parameters.put("titoloVerInstNokia", beanTitolo);
		parameters.put("verInst", beanVerInst);
		parameters.put("testFlussiNokia", beanTestFlussi);
		parameters.put("esitoVerInstNokia", beanEsitoVerInstNokia);
		parameters.put("riferimentiVerInstNokia", beanRiferimentiVerInstNokia);
		//prove funzionali apparato in commissioning
		parameters.put("allarmiEsterni2G",beanAllarmiEsterni2G);
		parameters.put("allarmiEsterni3G", beanAllarmiEsterni3G);
		parameters.put("misCarattApparato", beanMisCarattApparato);
		parameters.put("verificaMHA",beanVerificaMHA);
		parameters.put("verificaRET",beanVerificaRET);
		parameters.put("chiusuraCommissioning",beanChiusuraCommissioning);
		parameters.put("esitoVerFunzCommissioning", beanEsitoVerFunzCommissioning);
		parameters.put("riferimentiVerFunzCommissioning", beanRiferimentiVerFunzCommissioning);
		parameters.put("firma_tim_verFunz", firmaTimVerFunzBean);
		parameters.put("firma_ditta_verFunz", firmaDittaVerFunzBean);
		//prove funzionali di esercizio
		parameters.put("notificaAllarmi",beanNotificaAllarmi);
		parameters.put("continuitaFlussi",beanContinuitaFlussi);
		parameters.put("esitoProveFunzEser", beanEsitoProveFunzEser);
		parameters.put("riferimentiProveFunzEser", beanRiferimentiProveFunzEser);
		parameters.put("firma_tim_proveFunzEser", firmaTimProveFunzEserBean);
		parameters.put("firma_ditta_proveFunzEser", firmaDittaProveFunzEserBean);
		//prove funzionali di servizio
		parameters.put("prove_di_servizio",beanArrayProveDiServizio);
		parameters.put("esitoProveFunzServizio", beanEsitoProveFunzServizio);
		parameters.put("riferimentiProveFunzServizio", beanRiferimentiProveFunzServizio);
		parameters.put("firma_tim_proveFunzServizio", firmaTimProveFunzServizioBean);
		parameters.put("firma_ditta_proveFunzServizio", firmaDittaProveFunzServizioBean);
		//Verifiche di installazione WIPM
		parameters.put("datiGeneraliVerbaleApparatoNokiaWIPM", beanDatiGeneraliVerbaleApparatoNokiaWIPM);
		parameters.put("datiGeneraliTabletVerbaleApparatoNokiaWIPM", beanDatiGeneraliTabletVerbaleApparatoNokiaWIPM);
		parameters.put("datiGeneraliRiferimentiVerbaleApparatoNokiaWIPM", beanRiferimentiDatiGeneraliVerbaleApparatoNokiaWIPM);
		parameters.put("firma_tim_dati_generali_WIPM",firmaTimDatiGeneraliWIPM );
		parameters.put("firma_ditta_dati_generali_WIPM", firmaDittaDatiGeneraliWIPM);
		
		parameters.put("verificheInstWIPM",verificheInstWIPMBean);
		parameters.put("valoriMisuratiWIPM",valoriMisuratiWIPMBean);
		parameters.put("esitoVerInstWIPM", beanEsitoVerInstWIPM);
		parameters.put("riferimentiVerInstWIPM", beanRiferimentiVerInstWIPM);
		parameters.put("firma_tim_VerInstWIPM", firmaTimVerInstWIPMBean);
		parameters.put("firma_ditta_VerInstWIPM", firmaDittaVerInstWIPMBean);
		parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_NOKIA,sincronizzazioneDao));

		jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
	} catch (JRException e) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Apparato Nokia","Errore"));
		e.printStackTrace();
	}

	catch (Exception e) {
		e.printStackTrace();
	}
		return jasperPrint;
	}
	
	public byte[]  printNokia( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try {
			
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
				HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
				
				String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_NOKIA,sincronizzazioneDao);
				httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

				ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
				JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
				FacesContext.getCurrentInstance().responseComplete();

	
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	return attachment;

		
	}

	public byte[] printApparatoHuawei(ViwSessioni selectedSessione) {
		byte[] attachment=null;
			JasperPrint jasperPrint = creaHuawei(selectedSessione);
			attachment = printHuawei(jasperPrint, selectedSessione);
		
		return attachment;
	}

	
	public byte[] generaHuawei(ViwSessioni selectedSessione) {
		JasperPrint jasperPrint = creaHuawei(selectedSessione);
		byte[] attach = null;
		try {
			attach = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return attach;
	}
	

	public JasperPrint creaHuawei(ViwSessioni selectedSessione){
		JasperPrint jasperPrint =null;
		try {
			ArrayList<Object> collection = new ArrayList<Object>();
			collection.add("");
			JRBeanCollectionDataSource beanCollection = new JRBeanCollectionDataSource( collection, false );


			String realpath = servletContext.getRealPath("/resources/reports/huawei/VerbaleApparatoHuawei.jasper");
			String realpathLogoTim = servletContext.getRealPath("/img/logojasper.png");
			String pathLogoSaw = servletContext.getRealPath("/img/logoSaw.png");
			File fileLogoTim = new File(realpathLogoTim);
			File fileLogoSaw = new File(pathLogoSaw);

			//LOGICA DATI GENERALI
			List<ViwSessioni> arrayDatiGenerali = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			arrayDatiGenerali = getWorkRequestOfSessione(arrayDatiGenerali, selectedSessione, Const.COLLAUDO_APPARATO);
			List<ViwApparatoHuawei> arrayDatiGeneraliTablet = reportDao.getListApparatoHuaweiBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletHuawei);
			List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletBean = UtilityReports.getArrayTabletDatiGeneraliApparatoBean(arrayDatiGeneraliTablet);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliBean= new ArrayList<>();
			BufferedImage firmaTimDatiGenerali=null;
			BufferedImage firmaDittaDatiGenerali=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTablet.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimDatiGenerali = UtilityReports.getFirmaTim();
				firmaDittaDatiGenerali = UtilityReports.getFirmaDitta();
			} 
			//fine logica 

			//FINE LOGICA

			List<ViwApparatoHuawei> arrayTitolo = reportDao.getListApparatoHuawei(selectedSessione.getCodiSessione());

			/*LOGICA Verifiche di Installazione a carico della funzione di open access codi tipo oggetto 18 */
			List<ViwApparatoHuawei> arrayVerInst = reportDao.getListApparatoHuaweiBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneOpenAccessHuawei);
			List<ViwApparatoHuawei> arrayTestFlussi = UtilityReports.getHuaweiArrayVerinst(arrayVerInst);
			List<TestFlussiBean> arrayTestFlussiBean = UtilityReports.getArrayTestFlussiHuaweiBean(arrayTestFlussi);
			/*FINE Verifiche di Installazione a carico della funzione di open access codi tipo oggetto 18 */

			//Esito Verifiche di installazione nokia
			List<ViwEsitoSurvey> arrayEsitoVerInst = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneOpenAccessHuawei);

			//Riferimenti Verifiche di installazione nokia
			List<RiferimentiBean> arrayRiferimentiBean= new ArrayList<>();
			if (arrayVerInst.size()>0) {
				List<ViwRiferimentiFirma> arrayRiferimentiVerInst = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerInst.get(0).getCodiSurvey());
				arrayRiferimentiBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiVerInst);
			}
			BufferedImage firmaTimVerInsBean = UtilityReports.getFirmaTim();
			BufferedImage firmaDittaVerInsBean = UtilityReports.getFirmaDitta();


			//LOGICA ALLARMI ESTERNI codi_elemento 80
			List<ViwVerFunzAllarmi> arrayAllarmi = reportDao.getListViwVerFunzBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningHuawei,ConstReport.AllarmiEsterniHuawei);
			List<VerFunzAllarmiEsterniBean> arrayAllarmiEsterniBean = UtilityReports.getArrayAllarmiHuawei(arrayAllarmi);
			//FINE


			//LOGICA MISURE DI CARATTERIZZAZIONE APPARATO codi_elemento 81
			List<ViwApparatoHuawei> arrayMisCarattApparatoHuaweiBean = reportDao.getListApparatoHuaweiBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningHuawei,ConstReport.MisureDiCaratterizzazioneDiApparatoHuawei);
			//FINE LOGICA

			//LOGICA VERIFICA TMA codiElemento 82
			List<ViwApparatoHuawei> arrayVerificaTMA = reportDao.getListApparatoHuaweiBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningHuawei,ConstReport.VerificaTMAHuawei);
			List<VerificaTMABean> arrayVerificaTMABean = UtilityReports.getArrayVerificaTMABean(arrayVerificaTMA);
			//FINE LOGICA


			//LOGICA VERIFICA RET codiElemento 83
			List<ViwApparatoHuawei> arrayVerificaRET = reportDao.getListApparatoHuaweiBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningHuawei,ConstReport.VerificaRETHuawei);
			List<VerificaRETBean> arrayVerificaRETBean = UtilityReports.getArrayVerificaRETHuaweiBean(arrayVerificaRET);
			//FINE LOGICA

			//Esito Verifiche funzionali commissioning huawei
			List<ViwEsitoSurvey> arrayEsitoVerFunzCommissioningBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheFunzionaliApparatoCommissioningHuawei);

			//Riferimenti Verifiche FUNZIONALI COMMISIONING nokia
			List<RiferimentiBean> arrayRiferimentiFunzionaliCommissioningBean= new ArrayList<>();
			if (arrayVerificaRET.size()>0) {
				List<ViwRiferimentiFirma> arrayRiferimentiFunzCommissioning = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificaRET.get(0).getCodiSurvey());
				arrayRiferimentiFunzionaliCommissioningBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimentiFunzCommissioning);
			}
			BufferedImage firmaTimVerFunzBean = UtilityReports.getFirmaTim();
			BufferedImage firmaDittaVerFunzBean = UtilityReports.getFirmaDitta();

			//Logica per Notifica allarmi codiElemento 84 e codi tipo oggetto 20
			List<ViwApparatoHuawei> arrayNotificaAllarmiBean = reportDao.getListApparatoHuaweiBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioHuawei,ConstReport.NotificaAllarmiHuawei);
			//Fine logica

			//Logica per continuit� flussi codiElemento 85 e codi tipo oggetto 20
			List<ViwApparatoHuawei> arrayContinuitaFlussiBean = reportDao.getListApparatoHuaweiBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioHuawei,ConstReport.ContinuitaFlussiHuawei);
			//Fine logica

			//Esito prove funzionali di esercizio huawei
			List<ViwEsitoSurvey> arrayEsitoProveFunzEserBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiEsercizioHuawei);

			//Riferimenti ProveFunzionali di esercizio
			List<RiferimentiBean> arrayRiferimentiProveFunzEserBean= new ArrayList<>();
			if (arrayContinuitaFlussiBean.size()>0) {
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayContinuitaFlussiBean.get(0).getCodiSurvey());
				arrayRiferimentiProveFunzEserBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
			}
			BufferedImage firmaTimProveFunzEserBean = UtilityReports.getFirmaTim();
			BufferedImage firmaDittaProveFunzEserBean = UtilityReports.getFirmaDitta();

			//Logica per prove di servizio tipologiaOggetto 21 codiElemnto 86
			//N.B si usa ViwVerFunzAllarmi perch� usa la stessa logica
			List<ViwVerFunzAllarmi> arrayProveDiServizio = reportDao.getListViwVerFunzBySessioneByTipoOggettoElemento(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioHuawei,ConstReport.ProveDiServizioHuawei);
			List<ProveDiServizioBean> arrayProveDiServizioBean = UtilityReports.getArrayProveDiServizioHuawei(arrayProveDiServizio);
			//fine logica

			//Esito Prove funzionali di servizio huawei
			List<ViwEsitoSurvey> arrayEsitoProveFunzServizioBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioHuawei);

			//Cerco il codiSurvey nel caso in cui non siano stati inseriti dati ma sia presente la firma, al fine di permettere la sua visualizzazione
			
			//Riferimenti ProveFunzionali di servizio
			List<RiferimentiBean> arrayRiferimentiProveFunzServizioBean= new ArrayList<RiferimentiBean>();
			BufferedImage firmaTimProveFunzServizioBean;
			BufferedImage firmaDittaProveFunzServizioBean;
			if (arrayProveDiServizio.size()>0) {
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayProveDiServizio.get(0).getCodiSurvey());
				if(arrayRiferimenti.size()>0){
					arrayRiferimentiProveFunzServizioBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
					firmaTimProveFunzServizioBean = UtilityReports.getFirmaTim();
					firmaDittaProveFunzServizioBean = UtilityReports.getFirmaDitta();
				}else{
					firmaTimProveFunzServizioBean = null;
					firmaDittaProveFunzServizioBean = null;
				}
			} else{
				//Cerco il codiSurvey nel caso in cui non siano stati inseriti dati ma sia presente la firma, al fine di permettere la sua visualizzazione
				TabSurvey tabSurveyProveDiServizioHuawei=reportDao.getSurveyBySessioneAndTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.ProveFunzionaliDiServizioHuawei);
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(tabSurveyProveDiServizioHuawei.getCodiSurvey());
				if(arrayRiferimenti.size()>0){
					arrayRiferimentiProveFunzServizioBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
					firmaTimProveFunzServizioBean = UtilityReports.getFirmaTim();
					firmaDittaProveFunzServizioBean = UtilityReports.getFirmaDitta();
				}else{
					firmaTimProveFunzServizioBean = null;
					firmaDittaProveFunzServizioBean = null;
				}
			}


			//logica dati generali WIPM
			List<ViwSessioni> arrayDatiGeneraliWIPM = sessioneDao.getViwSessioneByCodi(selectedSessione.getCodiSessione());
			List<ViwApparatoHuawei> arrayDatiGeneraliTabletWIPM = reportDao.getListApparatoHuaweiBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.DatiGeneraliTabletHuaweiWipm);
			List<DatiGeneraliTabletApparatoBean> arrayDatiGeneraliTabletVerbaleApparatoHuaweiWIPMBean = UtilityReports.getArrayTabletDatiGeneraliApparatoBean(arrayDatiGeneraliTabletWIPM);
			List<RiferimentiBean> arrayRiferimentiDatiGeneraliVerbaleApparatoHuaweiWIPMBean= new ArrayList<>();
			BufferedImage firmaTimDatiGeneraliWIPM=null;
			BufferedImage firmaDittaDatiGeneraliWIPM=null;
			if(arrayDatiGeneraliTablet.size()>0){
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayDatiGeneraliTabletWIPM.get(0).getCodiSurvey());
				arrayRiferimentiDatiGeneraliVerbaleApparatoHuaweiWIPMBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimDatiGeneraliWIPM = UtilityReports.getFirmaTim();
				firmaDittaDatiGeneraliWIPM = UtilityReports.getFirmaDitta();
			} 
			//fine logica dati generali wipm

			//logica Verifiche installazione WIPM Nokia codi tipo oggetto 22
			List<ViwApparatoHuawei> arrayVerificheInstWIPMBean = reportDao.getListApparatoHuaweiBySessioneByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneWIPMHuawei);
			List<VerInstWIPMBean>  arrayValoriMisuratiWIPM = UtilityReports.getListValoriMisuratiWIPMHuawei(arrayVerificheInstWIPMBean);
			//fine logica

			//Esito Verifiche di Installazione WIPM
			List<ViwEsitoSurvey> arrayEsitoVerInstWIPMBean = reportDao.getListEsitoByTipoOggetto(selectedSessione.getCodiSessione(), ConstReport.VerificheDiInstallazioneWIPMHuawei);

			//Riferimenti ProveFunzionali di servizio
			List<RiferimentiBean> arrayRiferimentiVerInstWIPMBean= new ArrayList<RiferimentiBean>();
			BufferedImage firmaTimVerInstWIPMBean;
			BufferedImage firmaDittaVerInstWIPMBean;
			if (arrayVerificheInstWIPMBean.size()>0) {
				List<ViwRiferimentiFirma> arrayRiferimenti = (List<ViwRiferimentiFirma>) reportDao.getRiferimentoByCodiSurvey(arrayVerificheInstWIPMBean.get(0).getCodiSurvey());
				arrayRiferimentiVerInstWIPMBean= UtilityReports.getArrayRiferimentiBean(arrayRiferimenti);
				firmaTimVerInstWIPMBean = UtilityReports.getFirmaTim();
				firmaDittaVerInstWIPMBean = UtilityReports.getFirmaDitta();
			} else {
				firmaTimVerInstWIPMBean = null;
				firmaDittaVerInstWIPMBean = null;
			}

			JRBeanCollectionDataSource beanDatiGeneraliVerbaleApparatoHuawei = new JRBeanCollectionDataSource(arrayDatiGenerali, false);
			JRBeanCollectionDataSource beanDatiGeneraliVerbaleApparatoHuaweiWIPM = new JRBeanCollectionDataSource(arrayDatiGeneraliWIPM, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletVerbaleApparatoHuawei = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletBean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGeneraliVerbaleApparatoHuawei = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliBean, false);
			JRBeanCollectionDataSource beanTitolo = new JRBeanCollectionDataSource(arrayTitolo, false);
			JRBeanCollectionDataSource beanVerInst = new JRBeanCollectionDataSource(arrayVerInst, false);
			JRBeanCollectionDataSource beanTestFlussi= new JRBeanCollectionDataSource(arrayTestFlussiBean, false);
			JRBeanCollectionDataSource beanEsitoVerInstHuawei = new JRBeanCollectionDataSource(arrayEsitoVerInst, false);
			JRBeanCollectionDataSource beanRiferimentiVerInstHuawei = new JRBeanCollectionDataSource(arrayRiferimentiBean, false);
			JRBeanCollectionDataSource beanAllarmiEsterni = new JRBeanCollectionDataSource(arrayAllarmiEsterniBean, false);
			JRBeanCollectionDataSource beanMisCarattApparato = new JRBeanCollectionDataSource(arrayMisCarattApparatoHuaweiBean, false);
			JRBeanCollectionDataSource beanVerificaTMA = new JRBeanCollectionDataSource(arrayVerificaTMABean, false);
			JRBeanCollectionDataSource beanVerificaRET = new JRBeanCollectionDataSource(arrayVerificaRETBean, false);
			JRBeanCollectionDataSource beanEsitoVerFunzCommissioning = new JRBeanCollectionDataSource(arrayEsitoVerFunzCommissioningBean, false);
			JRBeanCollectionDataSource beanRiferimentiVerFunzCommissioning = new JRBeanCollectionDataSource(arrayRiferimentiFunzionaliCommissioningBean, false);
			JRBeanCollectionDataSource beanNotificaAllarmi = new JRBeanCollectionDataSource(arrayNotificaAllarmiBean, false);
			JRBeanCollectionDataSource beanContinuitaFlussi = new JRBeanCollectionDataSource(arrayContinuitaFlussiBean, false);
			JRBeanCollectionDataSource beanEsitoProveFunzEser = new JRBeanCollectionDataSource(arrayEsitoProveFunzEserBean, false);
			JRBeanCollectionDataSource beanRiferimentiProveFunzEser = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzEserBean, false);
			JRBeanCollectionDataSource beanArrayProveDiServizio = new JRBeanCollectionDataSource(arrayProveDiServizioBean, false);
			JRBeanCollectionDataSource beanEsitoProveFunzServizio = new JRBeanCollectionDataSource(arrayEsitoProveFunzServizioBean, false);
			JRBeanCollectionDataSource beanRiferimentiProveFunzServizio = new JRBeanCollectionDataSource(arrayRiferimentiProveFunzServizioBean, false);
			JRBeanCollectionDataSource verificheInstWIPMBean = new JRBeanCollectionDataSource(arrayVerificheInstWIPMBean, false);
			JRBeanCollectionDataSource valoriMisuratiWIPMBean = new JRBeanCollectionDataSource(arrayValoriMisuratiWIPM, false);
			JRBeanCollectionDataSource beanEsitoVerInstWIPM = new JRBeanCollectionDataSource(arrayEsitoVerInstWIPMBean, false);
			JRBeanCollectionDataSource beanRiferimentiVerInstWIPM = new JRBeanCollectionDataSource(arrayRiferimentiVerInstWIPMBean, false);
			JRBeanCollectionDataSource beanDatiGeneraliTabletVerbaleApparatoHuaweiWIPM = new JRBeanCollectionDataSource(arrayDatiGeneraliTabletVerbaleApparatoHuaweiWIPMBean, false);
			JRBeanCollectionDataSource beanRiferimentiDatiGeneraliVerbaleApparatoHuaweiWIPM = new JRBeanCollectionDataSource(arrayRiferimentiDatiGeneraliVerbaleApparatoHuaweiWIPMBean, false);



			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("logo_tim", fileLogoTim);
			parameters.put("logo_saw", fileLogoSaw);
			//dati generali
			parameters.put("datiGeneraliVerbaleApparatoHuawei", beanDatiGeneraliVerbaleApparatoHuawei);
			parameters.put("datiGeneraliTabletVerbaleApparatoHuawei", beanDatiGeneraliTabletVerbaleApparatoHuawei);
			parameters.put("datiGeneraliRiferimentiVerbaleApparatoHuawei", beanRiferimentiDatiGeneraliVerbaleApparatoHuawei);
			parameters.put("firma_tim_datiGenerali",firmaTimDatiGenerali );
			parameters.put("firma_ditta_datiGenerali", firmaDittaDatiGenerali);

			//Verifiche installazione
			parameters.put("firma_tim_verIns",firmaTimVerInsBean );
			parameters.put("firma_ditta_verIns", firmaDittaVerInsBean);
			parameters.put("titoloVerInstHuawei", beanTitolo);
			parameters.put("verInst", beanVerInst);
			parameters.put("testFlussiHuawei", beanTestFlussi);
			parameters.put("esitoVerInstHuawei", beanEsitoVerInstHuawei);
			parameters.put("riferimentiVerInstHuawei", beanRiferimentiVerInstHuawei);
			//prove funzionali apparato in commissioning
			parameters.put("allarmiEsterni",beanAllarmiEsterni);
			parameters.put("misCarattApparato", beanMisCarattApparato);
			parameters.put("verificaTMA",beanVerificaTMA);
			parameters.put("verificaRET",beanVerificaRET);
			parameters.put("esitoVerFunzCommissioning", beanEsitoVerFunzCommissioning);
			parameters.put("riferimentiVerFunzCommissioning", beanRiferimentiVerFunzCommissioning);
			parameters.put("firma_tim_verFunz", firmaTimVerFunzBean);
			parameters.put("firma_ditta_verFunz", firmaDittaVerFunzBean);
			//prove funzionali di esercizio
			parameters.put("notificaAllarmi",beanNotificaAllarmi);
			parameters.put("continuitaFlussi",beanContinuitaFlussi);
			parameters.put("esitoProveFunzEser", beanEsitoProveFunzEser);
			parameters.put("riferimentiProveFunzEser", beanRiferimentiProveFunzEser);
			parameters.put("firma_tim_proveFunzEser", firmaTimProveFunzEserBean);
			parameters.put("firma_ditta_proveFunzEser", firmaDittaProveFunzEserBean);
			//				//prove funzionali di servizio
			parameters.put("prove_di_servizio",beanArrayProveDiServizio);
			parameters.put("esitoProveFunzServizio", beanEsitoProveFunzServizio);
			parameters.put("riferimentiProveFunzServizio", beanRiferimentiProveFunzServizio);
			parameters.put("firma_tim_proveFunzServizio", firmaTimProveFunzServizioBean);
			parameters.put("firma_ditta_proveFunzServizio", firmaDittaProveFunzServizioBean);
			//Verifiche di installazione WIPM
			parameters.put("datiGeneraliVerbaleApparatoHuaweiWIPM", beanDatiGeneraliVerbaleApparatoHuaweiWIPM);
			parameters.put("datiGeneraliTabletVerbaleApparatoHuaweiWIPM", beanDatiGeneraliTabletVerbaleApparatoHuaweiWIPM);
			parameters.put("datiGeneraliRiferimentiVerbaleApparatoHuaweiWIPM", beanRiferimentiDatiGeneraliVerbaleApparatoHuaweiWIPM);
			parameters.put("firma_tim_dati_generali_WIPM",firmaTimDatiGeneraliWIPM );
			parameters.put("firma_ditta_dati_generali_WIPM", firmaDittaDatiGeneraliWIPM);
			parameters.put("verificheInstWIPM",verificheInstWIPMBean);
			parameters.put("valoriMisuratiWIPM",valoriMisuratiWIPMBean);
			parameters.put("esitoVerInstWIPM", beanEsitoVerInstWIPM);
			parameters.put("riferimentiVerInstWIPM", beanRiferimentiVerInstWIPM);
			parameters.put("firma_tim_VerInstWIPM", firmaTimVerInstWIPMBean);
			parameters.put("firma_ditta_VerInstWIPM", firmaDittaVerInstWIPMBean);
			parameters.put("nome_file", selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_HUAWEI,sincronizzazioneDao));
			
			jasperPrint = JasperFillManager.fillReport(realpath, parameters, beanCollection);
		} catch (JRException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Apparato Huawei","Errore"));
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return jasperPrint;
	}
	
	public byte[]  printHuawei( JasperPrint jasperPrint, ViwSessioni selectedSessione) {
		byte[] attachment=null;
		try{
			attachment=JasperExportManager.exportReportToPdf(jasperPrint);
			HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			
			String esitoAllegato = UtilityReports.getRisultatoAllegatoByCodiSessioneCodiOggetto(selectedSessione.getCodiSessione(),Const.ALLEGATO_HUAWEI,sincronizzazioneDao);
			httpServletResponse.addHeader("Content-disposition", "attachment; filename="+selectedSessione.getCodiceDbr()+"_"+"VRBCA"+selectedSessione.getDescCodice().substring(4)+"_"+getData(selectedSessione.getTimeAcquisizione()).replace("-","")+"_"+esitoAllegato+".pdf");

			ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
			FacesContext.getCurrentInstance().responseComplete();

		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Report - Export PDF Radianti A","Errore"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return attachment;
	}
	
	public List<ViwSessioni> getWorkRequestOfSessione(List<ViwSessioni> arrayDatiGenerali, ViwSessioni selectedSessione, Integer codiAnagWrs){
		TabWorkRequestColl tabWorkRequestColl = sessioneDao.findLastWRByCodiSessione(selectedSessione.getCodiSessione(), codiAnagWrs);
		if(tabWorkRequestColl!=null){
			for (ViwSessioni viwSessioni : arrayDatiGenerali) {
				viwSessioni.setDataWorkRequest(tabWorkRequestColl.getDateTimeWrs());
				viwSessioni.setDescNumeroWrs(tabWorkRequestColl.getDescNumeroWrs());
			}
		}		
		return arrayDatiGenerali;
	}


}
