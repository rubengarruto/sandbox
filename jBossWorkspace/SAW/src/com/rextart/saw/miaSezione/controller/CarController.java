package com.rextart.saw.miaSezione.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.rextart.saw.controller.Controller;
import com.rextart.saw.miaSezione.myBeans.Car;
import com.rextart.saw.rs.utility.CarFactory;
import com.rextart.saw.service.CarService;

@ManagedBean("carController")
@ViewScoped
public class CarController implements Serializable, Controller {

	private static final long serialVersionUID = 1L;

	// --------------------------------------------------------------------------------------------
	@ManagedProperty(value = "#{carService}")
	private CarService carService = new CarService();
	private List<Car> cars = new ArrayList<Car>();
	private Car selectedCar;
	private Car tempCar;
	private List<Car> filteredCars;
	private List<String> listBrands;



	@Inject
	private CarFactory carFactory;

	// --------------------------------------------------------------------------------------------
	public void saveCar() {
		carService.saveCar();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "System Message: ", "L'auto � stata salvata");
        FacesContext.getCurrentInstance().addMessage(null, message);
			
	}
	
	public void removeCar() {
		carService.removeCar(selectedCar);
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "System Message: ", "L'auto � stata eliminata");
        FacesContext.getCurrentInstance().addMessage(null, message);		
	}
	
	public List<String> listOfBrands(){
		CarFactory carFactory= new CarFactory();		
		return carFactory.getBrands() ;		
	}
	
	public List<String> getListBrands() {
		return listBrands;
	}

	public void setListBrands(List<String> listBrands) {
		this.listBrands = listBrands;
	}
	
	
	
	// --------------------------------------------------------------------------------------------
	public void growl() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("pop-up example", "esempio di pop-up"));
	}

	public Car getTempCar() {
		return tempCar;
	}

	public void setTempCar(Car tempCar) {
		this.tempCar = tempCar;
	}

	@PostConstruct
	@Override
	public void init() {
		setCars(carService.getAllCars());
		setListBrands(carFactory.getBrands());

		tempCar = new Car();
	}

	

	public Car getSelectedCar() {
		return selectedCar;
	}

	public void setSelectedCar(Car selectedCar) {
		this.selectedCar = selectedCar;
	}

	public List<Car> getFilteredCars() {
		return filteredCars;
	}

	public void setFilteredCars(List<Car> filteredCars) {
		this.filteredCars = filteredCars;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public List<String> getBrands() {
		return carFactory.getBrands();
	}

	public List<String> getColors() {
		return carFactory.getColors();
	}

	// Aggiungere una nuova auto
	public void createCar() {
		tempCar = new Car();
		carService.createCar(tempCar);
		
	}



	// ***** CONTROLLER DFLT METHODS *****
	@Override
	public void openEdit() {
		// TODO Auto-generated method stub
	}

	@Override
	public void openNew() {
		// TODO Auto-generated method stub

	}

	@Override
	public void rimuovi() {
		// TODO Auto-generated method stub

	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

	}

	@Override
	public void filter() {
		// TODO Auto-generated method stub

	}

	@Override
	public void filterClear() {
		// TODO Auto-generated method stub

	}

}
