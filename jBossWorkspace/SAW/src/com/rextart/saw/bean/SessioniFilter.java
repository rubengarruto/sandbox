package com.rextart.saw.bean;

import java.io.Serializable;

public class SessioniFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private Integer codiSistema;
	private Integer codiStato;
	
	public Integer getCodiSistema() {
		return codiSistema;
	}
	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}
	public Integer getCodiStato() {
		return codiStato;
	}
	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}

}
