package com.rextart.saw.bean;

import org.primefaces.model.UploadedFile;

public class UploadFileSawBean {
	private byte[] fileSchedaProgetto;
	private String nomeFileSchedaProgetto;
	private UploadedFile schedaProgetto;
	private byte[] fileCheckList;
	private String nomeFileCheckList;
	private byte[] fileSchedaRadio;
	private String nomeFileSchedaRadio;
	
	public byte[] getFileSchedaProgetto() {
		return fileSchedaProgetto;
	}
	public void setFileSchedaProgetto(byte[] fileSchedaProgetto) {
		this.fileSchedaProgetto = fileSchedaProgetto;
	}
	public String getNomeFileSchedaProgetto() {
		return nomeFileSchedaProgetto;
	}
	public void setNomeFileSchedaProgetto(String nomeFileSchedaProgetto) {
		this.nomeFileSchedaProgetto = nomeFileSchedaProgetto;
	}

	public byte[] getFileCheckList() {
		return fileCheckList;
	}
	public void setFileCheckList(byte[] fileCheckList) {
		this.fileCheckList = fileCheckList;
	}
	public String getNomeFileCheckList() {
		return nomeFileCheckList;
	}
	public void setNomeFileCheckList(String nomeFileCheckList) {
		this.nomeFileCheckList = nomeFileCheckList;
	}

	public byte[] getFileSchedaRadio() {
		return fileSchedaRadio;
	}
	public void setFileSchedaRadio(byte[] fileSchedaRadio) {
		this.fileSchedaRadio = fileSchedaRadio;
	}
	public String getNomeFileSchedaRadio() {
		return nomeFileSchedaRadio;
	}
	public void setNomeFileSchedaRadio(String nomeFileSchedaRadio) {
		this.nomeFileSchedaRadio = nomeFileSchedaRadio;
	}
	public UploadedFile getSchedaProgetto() {
		return schedaProgetto;
	}
	public void setSchedaProgetto(UploadedFile schedaProgetto) {
		this.schedaProgetto = schedaProgetto;
	}
	
	
	
}
