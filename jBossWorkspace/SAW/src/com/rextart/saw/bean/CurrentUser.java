package com.rextart.saw.bean;

import java.util.Collection;

import javax.faces.bean.SessionScoped;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.rextart.saw.entity.ViwUtenti;

@SessionScoped
public class CurrentUser extends User {

	private static final long serialVersionUID = 1L;
	Integer codiUtente;
	String nome;
	String cognome;
	String descEmail;
	String telefono;
	Boolean flgEmail;
	String descRole;
	Integer codiRole;
	Integer codiStruttura;
	String descStruttura;
	String descAreaComp;
	Integer codiAreaComp;
	Integer codiIpt;
	Integer codiZona;
	Integer codiGruppo;
	ViwUtenti viwUtente;
	String query;
	
	public CurrentUser(String username, String password,Integer codiUtente, String nome, String cognome , String descEmail, String telefono, Boolean flgEmail, String descRole, Integer codiRole,
			Integer codiStruttura, String descStruttura,
			String descAreaComp,
			Integer codiAreaComp,
			Integer codiZona,
			Integer codiGruppo,
			boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, ViwUtenti viwUtente) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		setCodiUtente(codiUtente);
		setNome(nome);
		setCognome(cognome);
		setDescEmail(descEmail);
		setTelefono(telefono);
		setDescEmail(descEmail);
		setTelefono(telefono);
		setFlgEmail(flgEmail);
		setDescRole(descRole);
		setDescStruttura(descStruttura);
		setDescAreaComp(descAreaComp);
		setCodiRole(codiRole);
		setCodiStruttura(codiStruttura);
		setCodiAreaComp(codiAreaComp);
		setCodiZona(codiZona);
		setCodiGruppo(codiGruppo);
		this.viwUtente=viwUtente;
	}



	public Integer getCodiUtente() {
		return codiUtente;
	}

	public void setCodiUtente(Integer codiUtente) {
		this.codiUtente = codiUtente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public ViwUtenti getViwUtente() {
		return viwUtente;
	}

	public void setViwUtente(ViwUtenti viwUtente) {
		this.viwUtente = viwUtente;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getDescEmail() {
		return descEmail;
	}

	public void setDescEmail(String descEmail) {
		this.descEmail = descEmail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Boolean getFlgEmail() {
		return flgEmail;
	}

	public void setFlgEmail(Boolean flgEmail) {
		this.flgEmail = flgEmail;
	}

	public String getDescRole() {
		return descRole;
	}

	public void setDescRole(String descRole) {
		this.descRole = descRole;
	}

	public String getDescStruttura() {
		return descStruttura;
	}

	public void setDescStruttura(String descStruttura) {
		this.descStruttura = descStruttura;
	}

	public String getDescAreaComp() {
		return descAreaComp;
	}

	public void setDescAreaComp(String descAreaComp) {
		this.descAreaComp = descAreaComp;
	}

	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}

	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}

	public Integer getCodiIpt() {
		return codiIpt;
	}

	public void setCodiIpt(Integer codiIpt) {
		this.codiIpt = codiIpt;
	}


	public Integer getCodiRole() {
		return codiRole;
	}


	public void setCodiRole(Integer codiRole) {
		this.codiRole = codiRole;
	}


	public Integer getCodiStruttura() {
		return codiStruttura;
	}



	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}



	public Integer getCodiZona() {
		return codiZona;
	}



	public void setCodiZona(Integer codiZona) {
		this.codiZona = codiZona;
	}



	public Integer getCodiGruppo() {
		return codiGruppo;
	}



	public void setCodiGruppo(Integer codiGruppo) {
		this.codiGruppo = codiGruppo;
	}

	
	
	
	
	
}
 