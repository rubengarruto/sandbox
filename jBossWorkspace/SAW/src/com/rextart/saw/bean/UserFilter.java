package com.rextart.saw.bean;

import java.io.Serializable;

public class UserFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codiAreaComp;
	private Integer codiRuolo;
	private Integer codiStruttura;
	private Boolean flgAbilitato;
	private Integer codiStatoUtente;
	
	public Integer getCodiRuolo() {
		return codiRuolo;
	}
	public void setCodiRuolo(Integer codiRuolo) {
		this.codiRuolo = codiRuolo;
	}
	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}
	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}
	public Integer getCodiStruttura() {
		return codiStruttura;
	}
	public void setCodiStruttura(Integer codiStruttura) {
		this.codiStruttura = codiStruttura;
	}
	public Boolean getFlgAbilitato() {
		return flgAbilitato;
	}
	public void setFlgAbilitato(Boolean flgAbilitato) {
		this.flgAbilitato = flgAbilitato;
	}
	public Integer getCodiStatoUtente() {
		return codiStatoUtente;
	}
	public void setCodiStatoUtente(Integer codiStatoUtente) {
		this.codiStatoUtente = codiStatoUtente;
	}
	
		
}
