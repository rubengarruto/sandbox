package com.rextart.saw.bean;

import java.io.Serializable;
import java.util.Date;

import com.rextart.saw.entity.ViwSessioni;

public class CollaudoDatiGeneraliBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codiSessione;
	private String clliSito;
	private Integer codiAreaComp;
	private String descAreaComp;
	private String descStato;
	private Integer codiBanda;
	private Integer codiCollaudo;
	private Integer codiDettaglioColl;
	private Integer codiFinalitaColl;
	private Integer codiSistema;
	private Integer codiTipoImpianto;
	private Integer codiUnitaTerr;
	private String codiceDbr;
	private Date dataSchedaPrRaEs;
	private String descCodice;
	private String descComune;
	private String descNomeSito;
	private String descNote;
	private String descOperTim;
	private String descProgettoRadio;
	private String descProvincia;
	private String descRegione;
	private Boolean flgAllegatoA;
	private Boolean flgAllegatoB;
	private Boolean flgAllegatoC;
	private Boolean flgAllegatoD;
	private Boolean flgAllegatoD1;
	private Boolean flgAllegatoE;
	private Boolean flgApparatoEricsson;
	private Boolean flgApparatoHuawei;
	private Boolean flgApparatoNsn;
	private String descIndirizzo;
	private String latitudine;
	private String longitudine;
	private Date timeAcquisizione;
	private Date timePrevistoCollaudo;
	private Boolean flgCollDisponibile;
	private String descSistema;
	private String descBanda;
	private String descZona;
	
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public String getClliSito() {
		return clliSito;
	}
	public void setClliSito(String clliSito) {
		this.clliSito = clliSito;
	}
	public Integer getCodiAreaComp() {
		return codiAreaComp;
	}
	public void setCodiAreaComp(Integer codiAreaComp) {
		this.codiAreaComp = codiAreaComp;
	}
	public Integer getCodiBanda() {
		return codiBanda;
	}
	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}
	public Integer getCodiCollaudo() {
		return codiCollaudo;
	}
	public void setCodiCollaudo(Integer codiCollaudo) {
		this.codiCollaudo = codiCollaudo;
	}
	public Integer getCodiDettaglioColl() {
		return codiDettaglioColl;
	}
	public void setCodiDettaglioColl(Integer codiDettaglioColl) {
		this.codiDettaglioColl = codiDettaglioColl;
	}
	public Integer getCodiFinalitaColl() {
		return codiFinalitaColl;
	}
	public void setCodiFinalitaColl(Integer codiFinalitaColl) {
		this.codiFinalitaColl = codiFinalitaColl;
	}
	public Integer getCodiSistema() {
		return codiSistema;
	}
	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}
	public Integer getCodiTipoImpianto() {
		return codiTipoImpianto;
	}
	public void setCodiTipoImpianto(Integer codiTipoImpianto) {
		this.codiTipoImpianto = codiTipoImpianto;
	}
	public Integer getCodiUnitaTerr() {
		return codiUnitaTerr;
	}
	public void setCodiUnitaTerr(Integer codiUnitaTerr) {
		this.codiUnitaTerr = codiUnitaTerr;
	}
	public String getCodiceDbr() {
		return codiceDbr;
	}
	public void setCodiceDbr(String codiceDbr) {
		this.codiceDbr = codiceDbr;
	}
	public Date getDataSchedaPrRaEs() {
		return dataSchedaPrRaEs;
	}
	public void setDataSchedaPrRaEs(Date dataSchedaPrRaEs) {
		this.dataSchedaPrRaEs = dataSchedaPrRaEs;
	}
	public String getDescCodice() {
		return descCodice;
	}
	public void setDescCodice(String descCodice) {
		this.descCodice = descCodice;
	}
	public String getDescComune() {
		return descComune;
	}
	public void setDescComune(String descComune) {
		this.descComune = descComune;
	}
	public String getDescNomeSito() {
		return descNomeSito;
	}
	public void setDescNomeSito(String descNomeSito) {
		this.descNomeSito = descNomeSito;
	}
	public String getDescNote() {
		return descNote;
	}
	public void setDescNote(String descNote) {
		this.descNote = descNote;
	}
	public String getDescOperTim() {
		return descOperTim;
	}
	public void setDescOperTim(String descOperTim) {
		this.descOperTim = descOperTim;
	}
	public String getDescProgettoRadio() {
		return descProgettoRadio;
	}
	public void setDescProgettoRadio(String descProgettoRadio) {
		this.descProgettoRadio = descProgettoRadio;
	}
	public String getDescProvincia() {
		return descProvincia;
	}
	public void setDescProvincia(String descProvincia) {
		this.descProvincia = descProvincia;
	}
	public String getDescRegione() {
		return descRegione;
	}
	public void setDescRegione(String descRegione) {
		this.descRegione = descRegione;
	}
	public Boolean getFlgAllegatoA() {
		return flgAllegatoA;
	}
	public void setFlgAllegatoA(Boolean flgAllegatoA) {
		this.flgAllegatoA = flgAllegatoA;
	}
	public Boolean getFlgAllegatoB() {
		return flgAllegatoB;
	}
	public void setFlgAllegatoB(Boolean flgAllegatoB) {
		this.flgAllegatoB = flgAllegatoB;
	}
	public Boolean getFlgAllegatoC() {
		return flgAllegatoC;
	}
	public void setFlgAllegatoC(Boolean flgAllegatoC) {
		this.flgAllegatoC = flgAllegatoC;
	}
	public Boolean getFlgAllegatoD() {
		return flgAllegatoD;
	}
	public void setFlgAllegatoD(Boolean flgAllegatoD) {
		this.flgAllegatoD = flgAllegatoD;
	}
	public Boolean getFlgAllegatoE() {
		return flgAllegatoE;
	}
	public void setFlgAllegatoE(Boolean flgAllegatoE) {
		this.flgAllegatoE = flgAllegatoE;
	}
	public Boolean getFlgApparatoEricsson() {
		return flgApparatoEricsson;
	}
	public void setFlgApparatoEricsson(Boolean flgApparatoEricsson) {
		this.flgApparatoEricsson = flgApparatoEricsson;
	}
	public Boolean getFlgApparatoHuawei() {
		return flgApparatoHuawei;
	}
	public void setFlgApparatoHuawei(Boolean flgApparatoHuawei) {
		this.flgApparatoHuawei = flgApparatoHuawei;
	}
	public Boolean getFlgApparatoNsn() {
		return flgApparatoNsn;
	}
	public void setFlgApparatoNsn(Boolean flgApparatoNsn) {
		this.flgApparatoNsn = flgApparatoNsn;
	}
	public String getDescIndirizzo() {
		return descIndirizzo;
	}
	public void setDescIndirizzo(String descIndirizzo) {
		this.descIndirizzo = descIndirizzo;
	}
	public String getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(String latitudine) {
		this.latitudine = latitudine;
	}
	public String getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(String longitudine) {
		this.longitudine = longitudine;
	}
	public Date getTimeAcquisizione() {
		return timeAcquisizione;
	}
	public void setTimeAcquisizione(Date timeAcquisizione) {
		this.timeAcquisizione = timeAcquisizione;
	}
	public String getDescAreaComp() {
		return descAreaComp;
	}
	public void setDescAreaComp(String descAreaComp) {
		this.descAreaComp = descAreaComp;
	}
	public String getDescStato() {
		return descStato;
	}
	public void setDescStato(String descStato) {
		this.descStato = descStato;
	}
	
	public Date getTimePrevistoCollaudo() {
		return timePrevistoCollaudo;
	}
	
	public void setTimePrevistoCollaudo(Date timePrevistoCollaudo) {
		this.timePrevistoCollaudo = timePrevistoCollaudo;
	}
	
	
	
	public String getDescSistema() {
		return descSistema;
	}
	public void setDescSistema(String descSistema) {
		this.descSistema = descSistema;
	}
	public String getDescBanda() {
		return descBanda;
	}
	public void setDescBanda(String descBanda) {
		this.descBanda = descBanda;
	}
	public String getDescZona() {
		return descZona;
	}
	public void setDescZona(String descZona) {
		this.descZona = descZona;
	}
	public Boolean getFlgCollDisponibile() {
		return flgCollDisponibile;
	}
	public void setFlgCollDisponibile(Boolean flgCollDisponibile) {
		this.flgCollDisponibile = flgCollDisponibile;
	}
	
	public Boolean getFlgAllegatoD1() {
		return flgAllegatoD1;
	}
	public void setFlgAllegatoD1(Boolean flgAllegatoD1) {
		this.flgAllegatoD1 = flgAllegatoD1;
	}
	
	public CollaudoDatiGeneraliBean(ViwSessioni viwSessioni) {
		super();
		this.codiSessione = viwSessioni.getCodiSessione();
		this.clliSito = viwSessioni.getClliSito();
		this.codiAreaComp =viwSessioni.getCodiAreaComp();
		this.codiBanda =viwSessioni.getCodiBanda();
		this.codiCollaudo =viwSessioni.getCodiCollaudo();
		this.codiDettaglioColl =viwSessioni.getCodiDettaglioColl();
		this.codiFinalitaColl =viwSessioni.getCodiFinalitaColl();
		this.codiSistema =viwSessioni.getCodiSistema();
		this.codiTipoImpianto =viwSessioni.getCodiTipoImpianto(); 
		this.codiUnitaTerr =viwSessioni.getCodiUnitaTerr(); 
		this.codiceDbr =viwSessioni.getCodiceDbr();
		this.dataSchedaPrRaEs =viwSessioni.getDataSchedaPrRaEs() ;
		this.descCodice =viwSessioni.getDescCodice();
		this.descComune =viwSessioni.getDescComune(); 
		this.descNomeSito = viwSessioni.getDescNomeSito();
		this.descNote =viwSessioni.getDescNote(); 
		this.descOperTim =viwSessioni.getDescOperTim(); 
		this.descProgettoRadio =viwSessioni.getDescProgettoRadio(); 
		this.descProvincia =viwSessioni.getDescProvincia(); 
		this.descRegione =viwSessioni.getDescRegione();
		this.flgAllegatoA = viwSessioni.getFlgAllegatoA();
		this.flgAllegatoB = viwSessioni.getFlgAllegatoB();
		this.flgAllegatoC = viwSessioni.getFlgAllegatoC();
		this.flgAllegatoD = viwSessioni.getFlgAllegatoD();
		this.flgAllegatoD1 = viwSessioni.getFlgAllegatoD1();
		this.flgAllegatoE = viwSessioni.getFlgAllegatoE();
		this.flgApparatoEricsson = viwSessioni.getFlgApparatoEricsson();
		this.flgApparatoHuawei = viwSessioni.getFlgApparatoHuawei();
		this.flgApparatoNsn = viwSessioni.getFlgApparatoNsn();
		this.descIndirizzo = viwSessioni.getDescIndirizzo();
		this.latitudine = viwSessioni.getDescLatitudine();
		this.longitudine = viwSessioni.getDescLongitudine();
		this.timeAcquisizione = viwSessioni.getTimeAcquisizione();
		this.descAreaComp = viwSessioni.getDescAreaComp();
		this.descStato =viwSessioni.getDescStato();
		this.timePrevistoCollaudo=viwSessioni.getTimePrevistoCollaudo();
		this.descBanda = viwSessioni.getDescBanda();
		this.descSistema = viwSessioni.getDescSistema();
		this.descZona = viwSessioni.getDescZona();
	}
	public CollaudoDatiGeneraliBean() {
		super();
		// TODO Auto-generated constructor stub
	}


}
