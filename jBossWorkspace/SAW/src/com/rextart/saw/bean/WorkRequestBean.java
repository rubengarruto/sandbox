package com.rextart.saw.bean;

import java.io.Serializable;
import java.util.Date;


public class WorkRequestBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer codiWrsColl;
	private Integer codiAnagWrs;
	private Integer codiBanda;
	private Integer codiOggetto;
	private Integer codiSessione;
	private Integer codiSistema;
	private String descNumeroWrs;
	private Date dataWorkRequest;
	
	public Integer getCodiWrsColl() {
		return codiWrsColl;
	}
	public void setCodiWrsColl(Integer codiWrsColl) {
		this.codiWrsColl = codiWrsColl;
	}
	public Integer getCodiAnagWrs() {
		return codiAnagWrs;
	}
	public void setCodiAnagWrs(Integer codiAnagWrs) {
		this.codiAnagWrs = codiAnagWrs;
	}
	public Integer getCodiBanda() {
		return codiBanda;
	}
	public void setCodiBanda(Integer codiBanda) {
		this.codiBanda = codiBanda;
	}
	public Integer getCodiOggetto() {
		return codiOggetto;
	}
	public void setCodiOggetto(Integer codiOggetto) {
		this.codiOggetto = codiOggetto;
	}
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public Integer getCodiSistema() {
		return codiSistema;
	}
	public void setCodiSistema(Integer codiSistema) {
		this.codiSistema = codiSistema;
	}
	public String getDescNumeroWrs() {
		return descNumeroWrs;
	}
	public void setDescNumeroWrs(String descNumeroWrs) {
		this.descNumeroWrs = descNumeroWrs;
	}

	public WorkRequestBean(Integer codiBanda,Integer codiSessione, Integer codiSistema) {
		super();
		this.codiBanda = codiBanda;
		this.codiSessione = codiSessione;
		this.codiSistema = codiSistema;
	}
	public Date getDataWorkRequest() {
		return dataWorkRequest;
	}
	public void setDataWorkRequest(Date dataWorkRequest) {
		this.dataWorkRequest = dataWorkRequest;
	}


}
