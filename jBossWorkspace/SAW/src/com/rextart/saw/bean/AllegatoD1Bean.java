package com.rextart.saw.bean;

import java.util.HashMap;

public class AllegatoD1Bean {

	private Boolean visibleForm=false;
	
	private Integer codiEsito;
	
	private HashMap<Integer,String> mapRisultatoSurvey;

	public Boolean getVisibleForm() {
		return visibleForm;
	}

	public void setVisibleForm(Boolean visibleForm) {
		this.visibleForm = visibleForm;
	}

	public Integer getCodiEsito() {
		return codiEsito;
	}

	public void setCodiEsito(Integer codiEsito) {
		this.codiEsito = codiEsito;
	}


	public  HashMap<Integer, String> getMapRisultatoSurvey() {
		return mapRisultatoSurvey;
	}

	public void setMapRisultatoSurvey(HashMap<Integer, String> mapRisultatoSurvey) {
		this.mapRisultatoSurvey = mapRisultatoSurvey;
	}

	
	
	
}
