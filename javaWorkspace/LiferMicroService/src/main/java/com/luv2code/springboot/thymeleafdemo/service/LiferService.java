package com.luv2code.springboot.thymeleafdemo.service;

import java.util.List;

import com.luv2code.springboot.thymeleafdemo.entity.Lifer;

public interface LiferService {

	public List<Lifer> findAll();
	
	public Lifer findById(int theId);
	
	public void save(Lifer theLifer);
	
	public void deleteById(int theId);
	
}
