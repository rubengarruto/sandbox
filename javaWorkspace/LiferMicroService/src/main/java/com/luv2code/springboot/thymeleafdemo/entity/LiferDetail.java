package com.luv2code.springboot.thymeleafdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="tab_lifers_detail")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LiferDetail {

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name="id_lifer_detail")
	private int idLifer;
	
	@Column(name="id_ministerio")
	String idMinisterio;
	
	@Column(name="id_mentor")
	private int idMentor;
	
	@Column(name="rit_start_flg")
	private boolean ritStartFlg;
	
	@Column(name="rit_next_flg")
	private boolean ritNextFlg;
	
	@Column(name="scuola_bib_flg")
	private boolean scuolaBibFlg;
		
}











