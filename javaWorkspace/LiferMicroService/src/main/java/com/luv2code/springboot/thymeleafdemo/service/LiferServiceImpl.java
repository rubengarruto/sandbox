package com.luv2code.springboot.thymeleafdemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luv2code.springboot.thymeleafdemo.dao.LiferRepository;
import com.luv2code.springboot.thymeleafdemo.entity.Lifer;

@Service
public class LiferServiceImpl implements LiferService {

	private LiferRepository liferRepository;

	@Autowired 
	public LiferServiceImpl(LiferRepository theliferRepository) {
		liferRepository = theliferRepository;
	}

	@Override
	public List<Lifer> findAll() {
		return liferRepository.findAllByOrderByLastNameAsc();
	}

	@Override
	public Lifer findById(int theId) {
		Optional<Lifer> result = liferRepository.findById(theId);

		Lifer theChoosenLifer = null;

		if (result.isPresent()) {
			theChoosenLifer = result.get();

		}
		else {
			throw new RuntimeException("Non è stato possibile trovare il lifer: "+theId);
			
		}
		
		return theChoosenLifer;
	}

	@Override
	public void save(Lifer theLiferToSave) {
		liferRepository.save(theLiferToSave);
	}

	@Override
	public void deleteById(int theId) {
		liferRepository.deleteById(theId);
	}

}
