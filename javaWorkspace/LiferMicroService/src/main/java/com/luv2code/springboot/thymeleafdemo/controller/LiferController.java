package com.luv2code.springboot.thymeleafdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.luv2code.springboot.thymeleafdemo.entity.Lifer;
import com.luv2code.springboot.thymeleafdemo.service.LiferService;

@Controller
@RequestMapping("/lifers")
public class LiferController {

	private LiferService liferService;
	
	

	@Autowired //@Autowired  è opzionale perchè c'è solo un costruttore
	public LiferController(LiferService theLiferService) {
		liferService = theLiferService;
	}

	@GetMapping("/list")
	public String listLifer(Model theModel) {

		// get lifer from DB
		List<Lifer> theLifers = liferService.findAll();

		// add to the spring model
		theModel.addAttribute("lifers", theLifers);

		return "ilTuoLife";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {

		Lifer theLifer = new Lifer();

		// add to the spring model
		theModel.addAttribute("lifer", theLifer);

		return "add-lifer";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("liferId") int theId, Model theModel) {

		Lifer theLifer = liferService.findById(theId);

		// add to the spring model
		theModel.addAttribute("lifer", theLifer);

		return "add-Lifer";
	}

	@PostMapping("/save")
	public String saveLifer(@ModelAttribute("lifer") Lifer theLifer) {

		liferService.save(theLifer);

		return "redirect:/lifers/list";
	}

	@GetMapping("/delete")
	public String deleteLifer(@RequestParam("liferId") int theId) {

		liferService.deleteById(theId);

		return "redirect:/lifers/list";
	}

}
