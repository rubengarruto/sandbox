package com.luv2code.springboot.thymeleafdemo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.luv2code.springboot.thymeleafdemo.entity.Lifer;

public interface LiferRepository extends JpaRepository<Lifer, Integer> {

	
	// ----------- CUSTOM METHODS -----------
	
	public List<Lifer> findAllByOrderByLastNameAsc();
}
