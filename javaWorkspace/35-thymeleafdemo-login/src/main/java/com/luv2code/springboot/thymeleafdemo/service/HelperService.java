package com.luv2code.springboot.thymeleafdemo.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("helperService")
public class HelperService {

    @Value("${server.servlet.context-path}")
    private String contextPath;

    public String getContextPath(){
        return contextPath;
    }
}
