package com.helloprime.controller;

import javax.faces.bean.RequestScoped;

@RequestScoped
public class BasicView {
     
    private String text="default";
 
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
}