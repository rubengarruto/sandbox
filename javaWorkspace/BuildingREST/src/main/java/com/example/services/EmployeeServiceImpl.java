package com.example.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entities.Employee;
import com.example.repositories.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements  EmployeeService {
	
	 @Autowired
	    private EmployeeRepository employeeRepository;
	  
	    public Iterable<Employee> findAll() {
	        return employeeRepository.findAll(); 
	    }

}
