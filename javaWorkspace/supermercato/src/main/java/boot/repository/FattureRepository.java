package boot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import boot.entity.Fatture;

@Repository
public interface FattureRepository extends CrudRepository<Fatture, Integer> {

}
