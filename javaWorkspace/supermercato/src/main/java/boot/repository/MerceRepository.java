package boot.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import boot.entity.Frutta;
import boot.entity.Merce;


@Repository
public interface MerceRepository extends CrudRepository<Merce, Integer> {

	@Query("select f From Frutta f")
	Iterable<Frutta> findAllFrutta(); //metodo che mi restituisce tutta la frutta	
	Merce findMerceByKey(Integer key);

	
}
