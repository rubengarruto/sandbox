package boot.application.bootstrap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.sun.jdi.Bootstrap;

import boot.entity.Arancia;
import boot.entity.Catalogo;
import boot.entity.Mele;
import boot.entity.Merce;
import boot.repository.MerceRepository;

@Component
public class BootStrap implements ApplicationListener<ContextRefreshedEvent> {
	static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

	private MerceRepository merceRepository;



	public BootStrap(MerceRepository merceRepository) {

		this.merceRepository = merceRepository;		
		logger.info("****" + merceRepository.getClass().getName());
	}


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("ContextRefreshedEvent received" +event.toString());
		insert();		
	}

	private void insert() {
		Merce mela = new Mele(1d,"Mela");
		Merce arancia = new Arancia(1.5d,"Arancia");
		
		Catalogo ctlFrutta= new Catalogo();		
		
		ctlFrutta.getMerci().add(mela);
		merceRepository.save(mela);
		
		ctlFrutta.getMerci().add(arancia);
		merceRepository.save(arancia);



	}

}

