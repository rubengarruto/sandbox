package boot.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import boot.entity.Etichetta;
import boot.entity.Fatture;
import boot.entity.Merce;
import boot.repository.FattureRepository;
import boot.repository.MerceRepository;

@Controller
public class ControllerCarrello {	
	
	public ControllerCarrello(MerceRepository merceRepository, FattureRepository fattureRepository) {
		super();
		this.merceRepository = merceRepository;
		this.fattureRepository=fattureRepository;
	}

	private MerceRepository merceRepository;
	private FattureRepository fattureRepository;
	
	@RequestMapping("/")
	public String index() { return "index";} //va a pigliare il jsp chiamato index
	
	@RequestMapping("/sceltaReparto")
	public String sceltaReparto() { return "sceltaReparto";}
	
	
	@RequestMapping("/ortofrutta")
	public String ortofrutta() { return "ortofrutta";} //va a pigliare il jsp chiamato ortofrutta
	
	@RequestMapping("/casalinghi")
	public String casalinghi() { return "casalinghi";} //va a pigliare il jsp chiamato casalinghi
//	
//	@RequestMapping("/getmerce")
//	public String merce() {
//		merceRepository.findAll();
//		return "ortofrutta";
//		}
	
	@RequestMapping("/getRepartoFrutta")
	public String repartoFrutta(Model model) {
		model.addAttribute("catalogoFrutta", merceRepository.findAllFrutta() );		
		return "ortofrutta";
		}
	
//	@RequestMapping("/getRepartoCasalinghi")
//	public String repartoCasalinghi(Model model) {
//		model.addAttribute("catalogoCasalinghi", merceRepository.findAllProdottiPU() );		
//		return "casalinghi";
//		}
//	
	
	
	@RequestMapping("/aggiungiAlCarrelloPW")
	public String aggiungiAlCarrelloPW (Model model,
			@RequestParam(value="key", required=true) Integer key,
			@RequestParam(value="qta",required=true) Double quantità,
			HttpSession sessione){
		if(sessione.getAttribute("carrello") != null) {
			System.err.print(sessione.getAttribute("carrello"));
		}
		List<Etichetta> listaEtichetta;
		listaEtichetta=(List<Etichetta>) sessione.getAttribute("carrello");
		if(listaEtichetta==null) {listaEtichetta=new ArrayList<>();}
		
			Etichetta etichetta = new Etichetta(merceRepository.findMerceByKey(key), quantità);
			listaEtichetta.add(etichetta);
			sessione.setAttribute("carrello", listaEtichetta);
			model.addAttribute("catalogoFrutta",merceRepository.findAllFrutta());
			return "ortofrutta";}
	

	
	@RequestMapping("/controllaCarrello")
	public String controllaCarrello(Model model, HttpSession sessione) {
		
		model.addAttribute("carrello",sessione.getAttribute("carrello"));
		
		return "controlla";
	}
	
	@RequestMapping("/pagamentoOrdine")	
	@Transactional
		public String pagamentoOrdine(Model model, HttpSession sessione) {
			Fatture fattura=new Fatture((List<Etichetta>) sessione.getAttribute("carrello"));
			
			sessione.setAttribute("carrello",null);
			fattureRepository.save(fattura);						
			model.addAttribute("fattura",fattura);
			return "scontrino";
		}
	}
	
		


