package boot.controller;

import java.util.ArrayList;
import java.util.List;

import boot.entity.Merce;

public class Carrello { //Observable

	private List<Merce> carrello = new ArrayList<Merce>();
	
	public Carrello() {
		
	}

	public void addMerce(Merce merce) {
		carrello.add(merce);
	}
	
	public void removeMerce(Merce merce) {
		carrello.remove(merce);
	}
	
	public void clearMerce() {
		carrello.clear();
	}

	public Merce getMerce(int index) {
		return carrello.get(index);
	}
	
	public int sizeCarrello() {
		return carrello.size();
	}
}
