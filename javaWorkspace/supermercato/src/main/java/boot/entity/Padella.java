package boot.entity;

import java.io.Serializable;
import javax.persistence.*;



/**
 * Entity implementation class for Entity:Padella
 *
 */
@Entity
@DiscriminatorValue(value ="PD")
public class Padella extends Merce implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Padella() {
		super();
	}
   
	public Padella(Double prezzo, String descrizione) {
		super(prezzo, descrizione);
		
	}

	@Override
	public String toString() {
		return "Padella=" + super.toString() + "]";
	}


	
}
