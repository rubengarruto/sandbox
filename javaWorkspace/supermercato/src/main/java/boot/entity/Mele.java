package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

import boot.entity.Frutta;

/**
 * Entity implementation class for Entity: Mele
 *
 */
@Entity
@DiscriminatorValue(value ="ML")
public class Mele extends Frutta implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Mele() {
		super();
	}
   
	public Mele( Double prezzo, String descrizione) {
		super( prezzo, descrizione);
		
		
	}

	@Override
	public String toString() {
		return "Mele=" + super.toString() + "]";
	}


	
}
