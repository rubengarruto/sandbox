package boot.entity;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(value ="PW")
public class ProdottiPW extends Merce implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Double prezzo_kg;
	

	public ProdottiPW() {
		// TODO Auto-generated constructor stub
	}
	
	public ProdottiPW( Double prezzo_kg, String descrizione) {
		super (descrizione);	
		this.prezzo_kg = prezzo_kg;
		
	}



	public Double getPrezzo_kg() {
		return prezzo_kg;
	}

	public void setPrezzo_kg(Double prezzo_kg) {
		this.prezzo_kg = prezzo_kg;
	}

	@Override
	public String toString() {
		return "ProdottiPW [prezzo_kg=" + prezzo_kg + "," + super.toString() + "]";
	}



	

}
