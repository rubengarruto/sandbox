package boot.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Fatture implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer key;
	
	@OneToMany(cascade = CascadeType.ALL)
	List<Etichetta> carrello = new ArrayList<>();
	
	
	public Fatture() {
	
	}
	
	public Fatture(	List<Etichetta> carrello) {
		this.carrello=carrello;
		
	}


	@Override
	public String toString() {
		return "Fatture [key=" + key + ", carrello=" + carrello + "]";
	}
	
	
	public Double totale() {
		Double tot=0d;
		for(Etichetta x:carrello) {
			tot=tot+x.getPrezzo();			
		}
		
		return tot;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public List<Etichetta> getCarrello() {
		return carrello;
	}

	public void setCarrello(List<Etichetta> carrello) {
		this.carrello = carrello;
	}


}
