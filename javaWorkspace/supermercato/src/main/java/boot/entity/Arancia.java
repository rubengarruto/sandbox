package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Arancia
 *
 */
@Entity
@DiscriminatorValue(value ="ARC")
public class Arancia extends Frutta implements Serializable {

	
	
	private static final long serialVersionUID = 1L;

	public Arancia() {
		super();
	}
	
	public Arancia( Double prezzo, String descrizione) {
		super(prezzo, descrizione);
	}

	@Override
	public String toString() {
		return "Arancia=" + super.toString() + "]";
	}
   
}

