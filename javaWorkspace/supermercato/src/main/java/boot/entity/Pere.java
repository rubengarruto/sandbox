package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

import boot.entity.Frutta;

/**
 * Entity implementation class for Entity: Pere
 *
 */
@Entity
@DiscriminatorValue(value ="PR")
public class Pere extends Frutta implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	

	public Pere() {
		super();
	}

	public Pere( Double prezzo, String descrizione) {
		super( prezzo, descrizione);
		
		
	}



	@Override
	public String toString() {
		return "Pere=" + super.toString() + "]";
	}
   
}
