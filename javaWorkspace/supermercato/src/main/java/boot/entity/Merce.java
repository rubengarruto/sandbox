package boot.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Merce
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value ="MRC")
public abstract class Merce implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO) //strategy= GenerationType.AUTO -> genero una key automatica

	private Integer key;
	private String descrizione;
	private Double prezzo;
	
public Double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(Double prezzo) {
		this.prezzo = prezzo;
	}



//@ManyToOne
//private Catalogo catalogo;
//	
//	public Catalogo getCatalogo() {
//	return catalogo;
//}
//
//public void setCatalogo(Catalogo catalogo) {
//	this.catalogo = catalogo;
//}

	public Merce() {
		super();
	}

	public Merce(Double prezzo, String descrizione) {
		super();
		this.prezzo=prezzo;
		this.descrizione = descrizione;
	}
	
	
	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getDescrizione() {
		return descrizione;
	}



	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Merce [key=" + key + ", descrizione=" + descrizione + ", prezzo=" + prezzo + ", catalogo=" 
				+ "]";
	}




}
