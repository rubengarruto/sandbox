package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

import boot.entity.Frutta;

/**
 * Entity implementation class for Entity: Kiwi
 *
 */
@Entity
@DiscriminatorValue(value ="KW")
public class Kiwi extends Frutta implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Kiwi() {
		super();
	}
	
	public Kiwi( Double prezzo, String descrizione) {
		super(prezzo, descrizione);
		
		
	}

	@Override
	public String toString() {
		return "Kiwi=" + super.toString() + "]";
	}
   
}
