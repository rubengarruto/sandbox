package boot.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.*;
import javax.persistence.OneToMany;

@Entity

public class Catalogo {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)

	private Integer key;

	
	@OneToMany		
	private Set<Merce> merci=new HashSet<>();
	
		
	
	
	public Integer getKey() {
		return key;
	}



	public void setKey(Integer key) {
		this.key = key;
	}


	public Set<Merce> getMerci() {
		return merci;
	}



	public void setMerci(Set<Merce> merci) {
		this.merci = merci;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Catalogo() {	
	
		
	}

}
