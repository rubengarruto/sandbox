package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

import boot.entity.Frutta;

/**
 * Entity implementation class for Entity: Banana
 *
 */
@Entity
@DiscriminatorValue(value ="BNN")
public class Banana extends Frutta implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Banana() {
		super();
	}
	
	public Banana( Double prezzo, String descrizione) {
		super(prezzo, descrizione);
		
		
	}

	@Override
	public String toString() {
		return "Banana=" + super.toString() + "]";
	}
   
}
