package boot.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@DiscriminatorValue(value ="FRT")
public abstract class  Frutta extends Merce implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Frutta() {
		super();
	}	
	
	public Frutta( Double prezzo, String descrizione) {
		super(prezzo, descrizione);
		
	}

	@Override
	public String toString() {
		return "Frutta=" + super.toString() + "]";
	}
   
   
}
