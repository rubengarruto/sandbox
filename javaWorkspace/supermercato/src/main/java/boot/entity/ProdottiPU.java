package boot.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(value ="PU")
public class ProdottiPU extends Merce implements Serializable {

	private Integer quantità;
	private Double prezzo;
	
	
	
	public ProdottiPU() {
		super();
	}


	public ProdottiPU(Integer quantità, Double prezzo, String descrizione) {
		super(descrizione);
		this.quantità = quantità;
		this.prezzo = prezzo;
	
	}


	public Integer getQuantità() {
		return quantità;
	}


	public void setQuantità(Integer quantità) {
		this.quantità = quantità;
	}


	public Double getPrezzo() {
		return prezzo;
	}


	public void setPrezzo(Double prezzo) {
		this.prezzo = prezzo;
	}


	@Override
	public String toString() {
		return "ProdottiPU [quantità=" + quantità + ", prezzo=" + prezzo + "," + super.toString() + "]";
	}
	
	

	
	
}


