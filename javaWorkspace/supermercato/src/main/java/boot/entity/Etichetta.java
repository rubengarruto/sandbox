package boot.entity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Etichetta  implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	private Merce merce;
	public Merce getMerce() {
		return merce;
	}

	public void setMerce(Merce merce) {
		this.merce = merce;
	}

	public Double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(Double prezzo) {
		this.prezzo = prezzo;
	}

	public Double getQuantità() {
		return quantità;
	}

	public void setQuantità(Double quantità) {
		this.quantità = quantità;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	private Double prezzo;
	private Double quantità;
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer key;
		
	public Etichetta() {
	}

	public Etichetta( Merce merce, Double quantità ) {
		super();		
		this.merce = merce;
		this.quantità= quantità;
		this.prezzo= quantità*((Merce) merce).getPrezzo();
	}

	@Override
	public String toString() {
		return "Etichetta [Merce=" + merce + ", prezzo=" + prezzo + ", peso=" + quantità + "]";
	}

	
	
}
