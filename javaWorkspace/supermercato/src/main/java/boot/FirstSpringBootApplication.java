package boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages= {"com.application.controller",
//		"com.application.entity.magazzino",
//		"com.application.repository"})
public class FirstSpringBootApplication {


	public static void main(String[] args) {
		SpringApplication.run(FirstSpringBootApplication.class, args);
	
		
	}

}
