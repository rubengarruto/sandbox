<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
       <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ page import="boot.entity.Fatture" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Emissione Scontrino</title>
</head>
<body>

<h1>Carrello</h1>
<table border="1">
<tr> <td>#</td><td>Descrizione</td><td>Prezzo</td></tr>
<%int i=1; %>
<% Fatture fat=(Fatture)request.getAttribute("fattura"); 
%>
<c:forEach var="frutto" items="${fattura.carrello}">


<tr>
<td><%= i %></td>

<td>${frutto.merce.descrizione}</td>
<td>${frutto.prezzo}</td>



</tr>

<%i++; %>

</c:forEach>
<tr><td colspan='2'><big>totale</big></td><td><%= fat.totale() %></td></tr>

</table>
</body>
</html>