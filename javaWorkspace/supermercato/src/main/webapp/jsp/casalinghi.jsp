<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CASALINGHI</title>
</head>
<body>
<h1> Reparto Casalinghi</h1>
<table border="1">
<tr> <td>#</td><td>Item</td><td>Descrizione</td><td>Prezzo</td><td></td><td></td></tr>

<c:forEach var="casalingo" items="${catalogoCasalinghi}">

<tr>
<form action="/aggiungiAlCarrelloPU" method="post">
<td><input type='text' value='${casalingo.key}' name='key' hidden='true'></input></td>
<td class="ite">${casalingo.getClass().getName()}</td>
<td>${casalingo.descrizione}</td>
<td>${casalingo.prezzo}</td>
<td><input type="text" name="qta"></input></td>

<td> <input type="submit"></input></td>

</form>
</tr>
</c:forEach>
<form action="/controllaCarrello" method="post">
<tr><td>  <input type="submit" value='Invia Ordine'></input></td></tr>
</form>
</table>
</body>
</html>