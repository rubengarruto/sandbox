<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ORTOFRUTTA</title>
</head>
<body>
<h1> Reparto Ortofrutta</h1>
<table border="1">
<tr> <td>#</td><td>Item</td><td>Descrizione</td><td>Prezzo/kg</td><td></td><td></td></tr>

<c:forEach var="frutto" items="${catalogoFrutta}">

<tr>
<form action="/aggiungiAlCarrelloPW" method="post">
<td><input type='text' value='${frutto.key}' name='key' hidden='true'></input></td>
<td class="ite">${frutto.getClass().getName()}</td>
<td>${frutto.descrizione}</td>
<td>${frutto.prezzo}</td>
<td><input type="text" name="qta"></input></td>

<td> <input type="submit"></input></td>

</form>

</tr>
</c:forEach>
<form action="/controllaCarrello" method="post">
<tr><td>  <input type="submit" value='Invia Ordine'></input></td></tr>
</form>

</table>

<script>var x=document.getElementsByClass("ite");
var i;
for(i=0; i<x.length;x++){

x[i].innerHTML=x[i].replace("boot.entity.","");
}
</script>

</body>
</html>