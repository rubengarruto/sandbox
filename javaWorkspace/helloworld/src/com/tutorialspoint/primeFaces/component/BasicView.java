package com.tutorialspoint.primeFaces.component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import lombok.Data;



@Data
@ManagedBean
@RequestScoped
public class BasicView {
	
    private String text; 
    
}
