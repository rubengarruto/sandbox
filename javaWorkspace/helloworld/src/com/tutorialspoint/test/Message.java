package com.tutorialspoint.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "message", eager = true)
@RequestScoped
public class Message {
   private String message = "ciao da Message!";
	
   public String getMessage() {
	  System.out.println("[myLog]: sono dentro getMessage()!");
      return message;
   }
   public void setMessage(String message) {
      this.message = message;
   }
}
