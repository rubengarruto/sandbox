package com.tutorialspoint.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean(name = "helloWorld", eager = true)
public class HelloWorld {
	
	@ManagedProperty(value = "#{message}")
	   private Message messageBean;
	   private String message;
   
   public HelloWorld() {
      System.out.println("[myLog]: HelloWorld started!");
   }
	
   public String getMessage() {
	      
	      if(messageBean != null) {
	    	 System.out.println("[myLog]: ho creato l'attributo message e lo sto popolando attraverso messageBean.getMessage()");
	         message = messageBean.getMessage();
	      }       
	      return message;
	   }
	   
	   public void setMessageBean(Message message) {
	      this.messageBean = message;
	   }
}
