package com.tutorialspoint.test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController implements Serializable {
	
	 // vai a homepage ---------------------------------------------------------------------------
	public String moveToHome() {
		  System.out.println("[myLog]: sono dentro il metodo moveToHome() del controller di navigazione!");
	      return "home";
	   }
	
	
	 // vai a pagina 2 ---------------------------------------------------------------------------
   public String moveToPage1() {
	  System.out.println("[myLog]: sono dentro il metodo moveToPage1() del controller di navigazione!");
      return "page1";
   }
  
   // vai a pagina 2 ---------------------------------------------------------------------------
   public String moveToPage2() {
		  System.out.println("[myLog]: sono dentro il metodo moveToPage2() del controller di navigazione!");
	      return "page2";
	   }
}
