package com.tutorialspoint.test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ManagedBean(name = "navigationControllerByID", eager = true)
@RequestScoped
public class NavigationControllerByID implements Serializable {
	
	   //this managed property will read value from request parameter pageId
	   @ManagedProperty(value = "#{param.pageId}")
	   private String pageId;

	   //condional navigation based on pageId
	   //if pageId is 3 show page3.xhtml,
	   //if pageId is 2 show page4.xhtml
	   //else show home.xhtml
	   
	   public String showPage() {
	      if(pageId == null) {
	         return "home";
	      }
	      
	      if(pageId.equals("3")) {
	         return "page3";
	      }else if(pageId.equals("4")) {
	         return "page4";
	      }else {
	         return "home";
	      }
	   }
	}