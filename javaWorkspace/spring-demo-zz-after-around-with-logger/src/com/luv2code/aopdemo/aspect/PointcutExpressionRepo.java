package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class PointcutExpressionRepo {

	//POINTCUT 1: used by all dao methods
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.*(..))")
	public void forDaopackage() {}
	
	//POINTCUT 2: used also if get***()  or set** is used!
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.get*(..))")
	public void getter() {}	
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.set*(..))")
	public void setter() {}
	
	//POINTCUT 3: used by all dao methods except getter and setter
	@Pointcut("forDaopackage() && !(getter() || setter())")
	public void forDaopackageNoGetterSetter() {}		
}
	
	
	

