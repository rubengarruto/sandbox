<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<html>

<head>
<title>List Customers</title>

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>CRM - CUSTOMER RELANTIONSHIP MANAGER</h2>
		</div>

		<div id="container">
			<div id="content">

				<table>
					<tr>
						<th>
							<!--  put New button: Add customer --> <input type="button"
							value="Add Customer"
							onclick="window.location.href='showFormForAdd'; return false;"
							class="add-button" />
						</th>
						<th>
							<!--  add a search box --> <form:form action="search"
								method="GET">
				Search customer: <input type="text" name="theSearchName" />

								<input type="submit" value="Search" class="add-button" />
							</form:form>
						</th>

					</tr>

				</table>

				<!-- add HTML table -->
				<table>

					<!-- header of the table -->
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Action</th>
					</tr>

					<!-- loop over and print customer -->
					<c:forEach var="tempCustomer" items="${customers}">

						<!-- construct an "update" link with customer id -->
						<c:url var="updateLink" value="/customer/showFormForUpdate">
							<c:param name="customerId" value="${tempCustomer.id}" />
						</c:url>

						<!-- construct an "delete" link with customer id -->
						<c:url var="deleteLink" value="/customer/delete">
							<c:param name="customerId" value="${tempCustomer.id}" />
						</c:url>

						<tr>
							<td>${tempCustomer.firstName}</td>
							<td>${tempCustomer.lastName}</td>
							<td>${tempCustomer.email}</td>
							<td>
								<!-- display the update link --> <a href="${updateLink}">Update</a>
								| <a href="${deleteLink}"
								onclick="if(!(confirm('Ma sei proprio sicuro sicuro che vuoi cancellare questo customer?'))) return false">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</table>


				<div>
					<p>
						<a href="${pageContext.request.contextPath}/"><i
							class="arrow left"></i>Back to Homepage</a>
					</p>
				</div>


			</div>
		</div>
	</div>
</body>

</html>









