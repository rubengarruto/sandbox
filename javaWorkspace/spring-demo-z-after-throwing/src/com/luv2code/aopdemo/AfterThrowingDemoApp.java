package com.luv2code.aopdemo;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.luv2code.aopdemo.dao.AccountDAO;
import com.luv2code.aopdemo.dao.MembershipDAO;

public class AfterThrowingDemoApp {

	public static void main(String[] args) {

		// 1. read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// 2. get the account and member bean from spring container
		AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		List<Account> theAccounts = null;

		try {
			// add a boolean flag to simulate exceptions
			boolean tripWire=true;
			theAccounts = theAccountDAO.findAccounts(tripWire);
		} catch (Exception exc) {
			System.err.println("\n\n!!! Main Program...caught exception !!! --> " + exc);
		}

		// 3. display the accounts
		System.out.println("\n\n [Program: AfterThrowingDemoApp]");
		System.out.println("- - - - - - - - ");
		System.out.println(theAccounts);
		System.out.println("N� of members: " + theAccounts.size());
		System.out.println("\n");

		// 4. close the context
		context.close();

	}

}
