package com.theserverside.demoJoinFaces.controller;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
 
@Controller
public class HomeController {
	
	private String formatPage=".xhtml";
	
     
    @RequestMapping("/")
    public String getfirstPage() {
        return "index"+formatPage;
    }
    
    @RequestMapping("/saluti")
    public String getSaluti() {
        return "saluti"+formatPage;
    }
 
}
