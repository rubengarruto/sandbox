package com.luv2code.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {

	int counter=1;

	//POINTCUT 1: used by all dao methods
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.*(..))")
	private void forDaopackage() {}
	
	//POINTCUT 2: used by also if get***()  or set** is used!
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.get*(..))")
	private void getter() {}	
	@Pointcut("execution(* com.luv2code.aopdemo.dao.*.set*(..))")
	private void setter() {}
	
	//POINTCUT 3: used by all dao methods except getter and setter
	@Pointcut("forDaopackage() && !(getter() || setter())")
	private void forDaopackageNoGetterSetter() {}
	
	
	@Before("forDaopackage()")
	public void beforeAddAccountAdvice() {
	System.out.println("\n =========> Executing  @Before advice on method");
	}
	
	@Before("forDaopackage()")
	public void performApiAnalytics() {
		System.out.println("\n =========> Performing API analytics...call number: "+counter);
		counter++;
		
	}
	
}
	
	
	

