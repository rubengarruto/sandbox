package List;

public class ViwPannelloOperativo {
	
	private Integer codiSessione;
	private String esitoCollaudo;
	private String esitoCellStyle;
	
	
	
	
	public ViwPannelloOperativo(Integer codiSessione, String esitoCollaudo) {
		this.codiSessione = codiSessione;
		this.esitoCollaudo = esitoCollaudo;
	}
	public Integer getCodiSessione() {
		return codiSessione;
	}
	public void setCodiSessione(Integer codiSessione) {
		this.codiSessione = codiSessione;
	}
	public String getEsitoCollaudo() {
		return esitoCollaudo;
	}
	public void setEsitoCollaudo(String esitoCollaudo) {
		this.esitoCollaudo = esitoCollaudo;
	}
	public String getEsitoCellStyle() {
		return esitoCellStyle;
	}
	public void setEsitoCellStyle(String esitoCellStyle) {
		this.esitoCellStyle = esitoCellStyle;
	}
	
	
	

}
