package List;

public class MyBean {

	private String attributoA;
	private String attributoB;
	private Boolean flgMyBean;
	
	
	
	
	
	
	
	public MyBean(String attributoA, String attributoB, Boolean flgMyBean) {
		this.attributoA = attributoA;
		this.attributoB = attributoB;
		this.flgMyBean = flgMyBean;
	}
	public String getAttributoA() {
		return attributoA;
	}
	public void setAttributoA(String attributoA) {
		this.attributoA = attributoA;
	}
	public String getAttributoB() {
		return attributoB;
	}
	public void setAttributoB(String attributoB) {
		this.attributoB = attributoB;
	}
	public Boolean getFlgMyBean() {
		return flgMyBean;
	}
	public void setFlgMyBean(Boolean flgMyBean) {
		this.flgMyBean = flgMyBean;
	}
	
}
