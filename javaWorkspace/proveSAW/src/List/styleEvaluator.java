package List;

import java.util.ArrayList;
import java.util.List;

import hasMap.ViwCounterCollaudi;

public class styleEvaluator {


	public static void main(String[] args) {

		List<ViwPannelloOperativo> collaudiDT = new ArrayList<ViwPannelloOperativo>();

		ViwPannelloOperativo collaudo1 = new ViwPannelloOperativo(1, "POSITIVO");
		ViwPannelloOperativo collaudo2 = new ViwPannelloOperativo(2, "POSITIVO CON RISERVA");
		ViwPannelloOperativo collaudo3 = new ViwPannelloOperativo(3, "NA");

		collaudiDT.add(collaudo1);
		collaudiDT.add(collaudo2);
		collaudiDT.add(collaudo3);

		String stylePositivo = "cellPositivo";
		String styleRiserva = "cellRiserva";
		String styleDefault = "cellDefault";

		for (ViwPannelloOperativo tempCol : collaudiDT) {

			switch (tempCol.getEsitoCollaudo()) {
			case "POSITIVO":
				tempCol.setEsitoCellStyle(stylePositivo);
				break;
			case "POSITIVO CON RISERVA":
				tempCol.setEsitoCellStyle(styleRiserva);
				break;

			default:
				tempCol.setEsitoCellStyle(styleDefault);
				break;
			}

			System.out.println(
					tempCol.getCodiSessione() + "-" + tempCol.getEsitoCollaudo() + "-" + tempCol.getEsitoCellStyle());

		}

	}
}
