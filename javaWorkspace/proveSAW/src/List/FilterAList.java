package List;

import java.util.ArrayList;
import java.util.List;

public class FilterAList {
	public static void main(String[] args) {
		
	List<MyBean>	notFiltereList= new ArrayList<MyBean>();
	List<MyBean>	filteredList= new ArrayList<MyBean>();
	
	
	MyBean bean1= new MyBean("attributeA1","attribute1B",false);
	MyBean bean2= new MyBean("attributeA2","attribute2B",false);
	MyBean bean3= new MyBean("attributeA3","attribute3B",true);
	MyBean bean4= new MyBean("attributeA4","attribute4B",true);
	MyBean bean5= new MyBean("attributeA5","attribute5B",false);
	MyBean bean6= new MyBean("attributeA6","attribute6B",false);
	
	notFiltereList.add(bean1);
	notFiltereList.add(bean2);
	notFiltereList.add(bean3);
	notFiltereList.add(bean4);
	notFiltereList.add(bean5);
	notFiltereList.add(bean6);
	
	for(int i=0; i<notFiltereList.size();i++) {
	System.out.println(notFiltereList.get(i).getAttributoA()+"-"+notFiltereList.get(i).getAttributoB()+"-"+notFiltereList.get(i).getFlgMyBean());
		if(notFiltereList.get(i).getFlgMyBean()==true)
		{filteredList.add(notFiltereList.get(i));}
		}
	
	System.out.println("\n");
	System.out.println("---------------------");
	for(int i=0; i<filteredList.size();i++) {
		System.out.println(filteredList.get(i).getAttributoA()+"-"+filteredList.get(i).getAttributoB()+"-"+filteredList.get(i).getFlgMyBean());
						}	
	}
}
