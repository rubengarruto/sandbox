package stringUtil;

public class CharAt {
	public static void main(String[] args) {
		
		String str ="10001";
		System.out.println(str);
		
		Character cha1 = new Character(str.charAt(0));
		Character cha2 = new Character(str.charAt(1));	
		Character cha3 = new Character(str.charAt(2));	
		Character cha4 = new Character(str.charAt(3));	
		Character cha5 = new Character(str.charAt(4));
		
		System.out.println(cha1 == '1'? "X":"-");
		System.out.println(cha2 == '1'? "X":"-");
		System.out.println(cha3 == '1'? "X":"-");
		System.out.println(cha4 == '1'? "X":"-");
		System.out.println(cha5 == '1'? "X":"-");
	
		System.out.println( );
		
	}
}


