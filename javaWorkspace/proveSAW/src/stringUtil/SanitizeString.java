package stringUtil;

public class SanitizeString {
	public static void main(String[] args) {
		
		String strToSanize="CiaO MOnDo";
		
		System.out.println(sanitize(strToSanize));
		
	}
	
	public static String sanitize(final String str ) {
		String sanize=str;
		if( !sanize.isEmpty() ) {
			sanize = sanize.trim();
		}
		sanize = sanize.toLowerCase();
		return sanize;
		}

}
