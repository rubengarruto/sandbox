package factories;

public class Apparati implements RadiantiFactory {

		
	Radiante apparato;
	
	@Override
	public Radiante newRadiante( String codice, String tipologia) {		
		this.apparato.newRadiante("codiceApparato"+codice,"Apparato - "+tipologia);	
		return apparato;
	}

	public Radiante getApparato() {
		return apparato;
	}

	public void setApparato(Radiante apparato) {
		this.apparato = apparato;
	}

}
