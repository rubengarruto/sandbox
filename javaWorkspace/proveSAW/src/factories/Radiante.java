package factories;

public class Radiante {

	private String codice;
	private String tipologia;

	@Override
	public String toString() {
		return "Radiante [codice=" + codice + ", tipologia=" + tipologia + "]";
	}


	public String getCodice() {
		return codice;
	}


	public void setCodice(String codice) {
		this.codice = codice;
	}


	public String getTipologia() {
		return tipologia;
	}


	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}


	private Radiante (final String codice,final String tipologia) {}

	
	public static Radiante newRadiante( final String codice, final String tipologia ) {
		return new Radiante( codice, tipologia );
		}

}

