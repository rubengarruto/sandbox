package factories;

public class Antenne implements RadiantiFactory {

	Radiante antenna;
		
	@Override
	public Radiante newRadiante( String codice, String tipologia) {		
		setAntenna(newRadiante("codiceAntenna"+codice,"Antenna - "+tipologia));		
		return antenna;
	}

	public Radiante getAntenna() {
		return antenna;
	}

	public void setAntenna(Radiante antenna) {
		this.antenna = antenna;
	}


}
