package factories;

public class Main {

	public static void main(String[] args) {
		Antenne antenna = new Antenne();
		antenna.newRadiante("001", "A");
		
		Apparati apparato = new Apparati();
		apparato.newRadiante("001", "Ericsson");
		
		System.out.println(antenna.getAntenna().toString());
		System.out.println(apparato.getApparato().toString());

	}

}
