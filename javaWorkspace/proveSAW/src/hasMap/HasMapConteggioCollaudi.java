package hasMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HasMapConteggioCollaudi {
	public static void main(String[] args) {

		List<String> labelStati = new ArrayList<String>();
		labelStati.add("COLLAUDI ACQUISITI");
		labelStati.add("COLLAUDI DISPONIBILI");
		labelStati.add("COLLAUDI IN LAVORAZIONE");

		// VALORIZZAZIONE PERIODO
		List<String> periodo = new ArrayList<String>();
		periodo.add("2018");
		periodo.add("2019");
		periodo.add("2020");

		List<Integer> y2018 = new ArrayList<Integer>();
		y2018.add(150);
		y2018.add(100);
		y2018.add(30);
		y2018.add(50);

		List<Integer> y2019 = new ArrayList<Integer>();
		y2019.add(170);
		y2019.add(120);
		y2019.add(50);
		y2019.add(80);

		List<Integer> y2020 = new ArrayList<Integer>();
		y2020.add(100);
		y2020.add(50);
		y2020.add(10);
		y2020.add(65);

		List<List<Integer>> yAxis = new ArrayList<List<Integer>>();
		yAxis.add(y2018);
		yAxis.add(y2019);
		yAxis.add(y2020);

		LinkedHashMap<String, List<Integer>> numCollPerPeriodo = new LinkedHashMap<>();

		print(numCollPerPeriodo);

		for (int i = 0; i < labelStati.size(); i++) {

			numCollPerPeriodo.put(periodo.get(i), yAxis.get(i));
		}

		System.out.println("Size of map is:" + numCollPerPeriodo.size());

		print(numCollPerPeriodo);
		if (numCollPerPeriodo.containsKey("COLLAUDI ACQUISITI")) {
			List<Integer> a = numCollPerPeriodo.get("COLLAUDI ACQUISITI");
			System.out.println("value for key" + " \"COLLAUDI ACQUISITI\" is:- " + a);
		}

		for (int i = 0; i < numCollPerPeriodo.size(); i++) {

			System.out.println("ANNO:" + periodo.get(i));
			System.out.println(numCollPerPeriodo.get(periodo.get(i)));

			for (int j = 0; j < numCollPerPeriodo.get(periodo.get(i)).size(); j++) {
				System.out.println(numCollPerPeriodo.get(periodo.get(i)).get(j));

			}
		}

	}

	public static void print(Map<String, List<Integer>> map) {
		if (map.isEmpty()) {
			System.out.println("map is empty\n ------------");
		}

		else {
			System.out.println(map);
		}
	}
}
