package hasMap;

public class Documento {
	
	private Integer idDoc;
	private Integer idSezione;
	private String titoloDoc;
	
	
	
	public Documento(Integer idDoc, Integer idSezione, String titoloDoc) {
		this.idDoc = idDoc;
		this.idSezione = idSezione;
		this.titoloDoc = titoloDoc;
	}



	public Integer getIdDoc() {
		return idDoc;
	}



	public void setIdDoc(Integer idDoc) {
		this.idDoc = idDoc;
	}



	public Integer getIdSezione() {
		return idSezione;
	}



	public void setIdSezione(Integer idSezione) {
		this.idSezione = idSezione;
	}



	public String getTitoloDoc() {
		return titoloDoc;
	}



	public void setTitoloDoc(String titoloDoc) {
		this.titoloDoc = titoloDoc;
	}



	@Override
	public String toString() {
		return "Documento [idDoc=" + idDoc + ", idSezione=" + idSezione + ", titoloDoc=" + titoloDoc + "]";
	}

	
}
