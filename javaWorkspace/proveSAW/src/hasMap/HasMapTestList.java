package hasMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HasMapTestList {
	public static void main(String[] args) {

		List<Documento> archivioDoc = new ArrayList<Documento>();
		List<Sezioni> sezioniDoc = new ArrayList<Sezioni>();

		HashMap<Integer, List<Documento>> documentiSezione = new HashMap<Integer, List<Documento>>();

		Documento doc1a = new Documento(1, 1, "Titolo_1A");
		Documento doc2a = new Documento(2, 1, "Titolo_2A");
		Documento doc1b = new Documento(3, 2, "Titolo_1B");
		Documento doc2b = new Documento(4, 2, "Titolo_2B");
		Documento doc3a = new Documento(5, 3, "Titolo_1C");
		Documento doc3b = new Documento(6, 3, "Titolo_2C");

		Sezioni sezA = new Sezioni(1, "sezione A");
		Sezioni sezB = new Sezioni(2, "sezione B");
		Sezioni sezC = new Sezioni(3, "sezione C");

		archivioDoc.add(doc1a);
		archivioDoc.add(doc1b);
		archivioDoc.add(doc2a);
		archivioDoc.add(doc2b);
		archivioDoc.add(doc3a);
		archivioDoc.add(doc3b);

		sezioniDoc.add(sezA);
		sezioniDoc.add(sezB);
		sezioniDoc.add(sezC);

		for (Documento docTemp : archivioDoc) {

			Integer x = docTemp.getIdSezione();
			List<Documento> tempList = documentiSezione.get(x);

			if (tempList == null) {
				
				tempList = new ArrayList<Documento>();
				
			} 
				
				tempList.add(docTemp);
				documentiSezione.put(x, tempList);
			}

		
		
		 for(Integer key : documentiSezione.keySet()) {
			 System.out.println(key);
			 System.out.println(documentiSezione.keySet());
				System.out.println(documentiSezione.get(key).toString());			
						  }
		

	}
}