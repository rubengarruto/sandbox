package hasMap;

public class ViwCounterCollaudi {
	
	private Integer codiStato;
	private Integer NumeroCollaudi;
	private String descStato;
	
	
	
	public ViwCounterCollaudi(Integer codiStato, Integer NumeroCollaudi, String descStato) {
		this.codiStato = codiStato;
		this.NumeroCollaudi = NumeroCollaudi;
		this.descStato = descStato;
	}



	public Integer getCodiStato() {
		return codiStato;
	}



	public void setCodiStato(Integer codiStato) {
		this.codiStato = codiStato;
	}



	public Integer getNumeroCollaudi() {
		return NumeroCollaudi;
	}



	public void setNumeroCollaudi(Integer numeroCollaudi) {
		NumeroCollaudi = numeroCollaudi;
	}



	public String getDescStato() {
		return descStato;
	}



	public void setDescStato(String descStato) {
		this.descStato = descStato;
	}
	
	



	
	
}
