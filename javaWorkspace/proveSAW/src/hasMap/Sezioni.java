package hasMap;

public class Sezioni {
	
	private Integer idSezione;
	private String TipoSezione;
	
	
	
	
	public Sezioni(Integer idSezione, String tipoSezione) {
		this.idSezione = idSezione;
		TipoSezione = tipoSezione;
	}
	public Integer getIdSezione() {
		return idSezione;
	}
	public void setIdSezione(Integer idSezione) {
		this.idSezione = idSezione;
	}
	public String getTipoSezione() {
		return TipoSezione;
	}
	public void setTipoSezione(String tipoSezione) {
		TipoSezione = tipoSezione;
	}
	
	
	
	
}
