package com.luv2code.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.luv2code.aopdemo.dao.AccountDAO;
import com.luv2code.aopdemo.dao.MembershipDAO;

public  class MainDemoApp {

	public static void main(String[] args) {
		
		// 1. read spring config java class
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		
		// 2. get the account and member bean from spring container
		AccountDAO theAccountDAO = context.getBean("accountDAO", AccountDAO.class);
		MembershipDAO theMembershipDAO = context.getBean("membershipDAO", MembershipDAO.class);
		
		//3. Call the Business method to a normal account and membership account
		Account myAccount = new Account();
		theAccountDAO.addAccount(myAccount, true);	
		theAccountDAO.doWork();
		
		theMembershipDAO.addAccount();
		theMembershipDAO.goToSleep();
		
		//4. close the context
		context.close();
		
		
	}
	
	
}
