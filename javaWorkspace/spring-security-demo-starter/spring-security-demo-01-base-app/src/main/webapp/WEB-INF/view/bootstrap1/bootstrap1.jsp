<!--====================================== LIBRARIES ======================================-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>REXTART-DEMO</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--========================  Latest compiled and minified CSS ============================-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


<!--================================  jQuery library ======================================-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!--==================================  Popper JS =========================================-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!--=======================================================================================-->


<!--=========================  Latest compiled JavaScript =================================-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>


<body>
	<!-- ======= NAVIGATION BAR ======= -->
	<div>
		<jsp:include page="../layout/nav-bar.jsp"></jsp:include>
	</div>

	<!-- ======= STAMPA NOME USER AUTENTICATO ======= -->
	<div class="container p-3 my-3 bg-primary text-white">
		<h2>welcome to...${pageContext.request.contextPath}</h2>
		<sec:authorize access="isAuthenticated()"> 
    Welcome Back, <sec:authentication property="name" />
		</sec:authorize>
	</div>

	<!-- ======= CONTAINER CON BORDO ======= -->
	<div class="container border">BORDER to the container</div>


	<div class="container border">

		<jsp:include page="content.jsp"></jsp:include>

	</div>

</body>
</html>