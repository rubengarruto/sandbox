<!--====================================== LIBRARIES ======================================-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--=======================================================================================-->

<html>

<!-- ======= HEAD ======= -->
<head>
<title>Custom Login Page</title>

<style type="text/css">
.failed{color:red;}
</style>

</head>


<!-- ======= BODY ======= -->
<body>
	<h3>REXTART - LOGIN DEMO -</h3>

	<form:form
		action="${pageContext.request.contextPath}/authenticateTheUser"
		method="POST">


<!-- ========== ERRORE MESSAGE ========-->
		<c:if test="${param.error != null}">
			<i class="failed">Sorry! You entered invalid username/password</i>
		</c:if>


		<!-- ========== INPUT USERNAME ========-->
		<p>
			User Name: <input type="text" name="username" placeholder="Username" />
		</p>


		<!-- ========== INPUT PASSWORD ========-->
		<p>
			Password: <input type="password" name="password"
				placeholder="Password" />
		</p>


		<!-- ========== LOGIN BUTTON ========-->
		<p>
			<input type="submit" value="Login" />
		</p>
	</form:form>
</body>
</html>
