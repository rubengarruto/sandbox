<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>QUESTO � UN CONTENUTO importato</title>

</head>
<body>
	<p>
		Hi,

		<sec:authorize access="isAuthenticated()">
			<sec:authentication property="name" />
			<%
				out.print(",");
			%>
		</sec:authorize>
		<%
			out.print("Today is: " + java.util.Calendar.getInstance().getTime());
		%>
	
</body>
</html>