<!--====================================== LIBRARIES ======================================-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->

<!DOCTYPE html>
<html>

<head>
<title>List Customers</title>
<head>
<title>LifeGroup-Manager</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--========================  Latest compiled and minified CSS ============================-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


<!--================================  jQuery library ======================================-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!--==================================  Popper JS =========================================-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<!--=========================  Latest compiled JavaScript =================================-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<!-- CSS ========================================================================-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

</head>
<body>
	<!-- ======= NAVIGATION BAR ======= -->
	<div>
		<jsp:include page="layout/nav-bar.jsp"></jsp:include>
	</div>

	<!-- ======= HEADER ======= -->
	<div class="container p-3 my-3 bg-primary text-white" align="center">
		<h2>RUBRICA</h2>
	</div>

	<!-- ======= CONTAINER CON BORDO ======= -->
	<div class="container border">

		<div id="content" align="center">

			<table>
				<tr>
					<th>
						<!--  put New button: Add customer --> <input type="button"
						value="Add Customer"
						onclick="window.location.href='showFormForAdd'; return false;"
						class="add-button" />
					</th>
					<th>
						<!--  add a search box --> <form:form action="search" method="GET">
				Search customer: <input type="text" name="theSearchName" />

							<input type="submit" value="Search" class="add-button" />
						</form:form>
					</th>

				</tr>

			</table>

			<!-- add HTML table -->
			<table>

				<!-- header of the table -->
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>

				<!-- loop over and print customer -->
				<c:forEach var="tempCustomer" items="${customers}">

					<!-- construct an "update" link with customer id -->
					<c:url var="updateLink" value="/customer/showFormForUpdate">
						<c:param name="customerId" value="${tempCustomer.id}" />
					</c:url>

					<!-- construct an "delete" link with customer id -->
					<c:url var="deleteLink" value="/customer/delete">
						<c:param name="customerId" value="${tempCustomer.id}" />
					</c:url>

					<tr>
						<td>${tempCustomer.firstName}</td>
						<td>${tempCustomer.lastName}</td>
						<td>${tempCustomer.email}</td>
						<td>
							<!-- display the update link --> <a href="${updateLink}">Update</a>
							| <a href="${deleteLink}"
							onclick="if(!(confirm('Vuoi davvero eliminare questo utente?'))) return false">Delete</a>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>