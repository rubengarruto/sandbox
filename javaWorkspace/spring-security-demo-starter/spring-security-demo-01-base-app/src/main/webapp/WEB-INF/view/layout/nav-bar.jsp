<!--====================================== LIBRARIES ======================================-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--=======================================================================================-->

<!DOCTYPE html>
<html lang="en">

<!-- ======= HEAD ======= -->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width-device-width" ,initial-scale=1.0>
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/resources/images/logo-rextart.png" />


<!-- FONTS ======================================================================-->
<link
	href="https://fonts.googleapis.com/css?family=Poppins&display=swap"
	rel="stylesheet">

<!-- CSS ========================================================================-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/nav-bar-style.css">
<title>Main Page</title>

<!-- IMAGES ===================================================================-->
<c:url value="/resources/images" var="images">
</c:url>
</head>


<!-- ======= BODY ======= -->
<body>

	<!-- ======= LOGO ======= -->
	<nav>
		<div class="logo">
			<h4>
				<img class="logo-rextart" src="${images}/logo-rextart.png">
			</h4>

		</div>
		<!-- ======= NAV BAR ======= -->
		<ul class="nav-links">
			<li><a href="${pageContext.request.contextPath}/home">Home</a></li>
			<li><a href="${pageContext.request.contextPath}/userAnag">Anagrafica</a></li>
			<li><a href="${pageContext.request.contextPath}/customer/list">Rubrica</a></li>
			<li><a href="${pageContext.request.contextPath}/boot">bootstrap1</a></li>			
		</ul>

		<!-- ======= BURGER MENU ======= -->
		<div class="burger">
			<div class="line1"></div>
			<div class="line2"></div>
			<div class="line3"></div>
		</div>
	</nav>


	<footer>
		<!-- ======= LOGOUT ======= -->

		<form:form action="${pageContext.request.contextPath}/logout "
			method="POST">
			<input class='ph-button ph-btn-red' type="submit" value="Logout" />
			<jsp:include page="../bootstrap1/content.jsp"></jsp:include>
		</form:form>
	</footer>

<script src="${pageContext.request.contextPath}/resources/js/nav-bar-app.js"></script>
</body>
</html>