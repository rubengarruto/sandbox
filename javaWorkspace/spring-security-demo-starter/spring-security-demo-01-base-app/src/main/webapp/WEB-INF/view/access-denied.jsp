<!--====================================== LIBRARIES ======================================-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->

<!DOCTYPE html>
<html>
<body>

	<!-- ======= NAVIGATION BAR ======= -->
	<div>
		<jsp:include page="layout/nav-bar.jsp"></jsp:include>
	</div>

<h1>ACCESS DENIED!!</h1>
<p>You don't  have the permission to access to these section</p>

</body>
</html>