<!--====================================== LIBRARIES ======================================-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->

<!DOCTYPE html>
<html>
<head>
<!--========================  Latest compiled and minified CSS ============================-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


<!--================================  jQuery library ======================================-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!--==================================  Popper JS =========================================-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<!--=========================  Latest compiled JavaScript =================================-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>



</head>




<body>

	<!-- ======= NAVIGATION BAR ======= -->
	<div>
		<jsp:include page="../layout/nav-bar.jsp"></jsp:include>
	</div>


	<div class="container my-1 p-1  bg-primary text-white" align="center">
		<h2>EMPLOYEE SECTION</h2>
	</div>
	<div class="container border">		
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">User</th>
						<th scope="col">Role</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td><sec:authentication property="principal.username" /></td>
						<td><sec:authentication property="principal.authorities" /></td>
					</tr>
				</tbody>
			</table>
		</div>
</body>
</html>