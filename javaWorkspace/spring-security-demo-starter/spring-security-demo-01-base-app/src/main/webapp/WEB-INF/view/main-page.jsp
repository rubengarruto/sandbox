<!--====================================== LIBRARIES ======================================-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->





<!--====================================== MAIN-PAGE ======================================-->
<!DOCTYPE html>
<html lang="en">


<!--=========================== HEAD ==========================-->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width-device-width" ,initial-scale=1.0>
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!--========================  Latest compiled and minified CSS ============================-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


<!--================================  jQuery library ======================================-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!--==================================  Popper JS =========================================-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<!--=========================  Latest compiled JavaScript =================================-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


<!-- CSS ========================================================================-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/nav-bar-style.css">
<title>REXTART-DEMO</title>


<!-- IMAGES ===================================================================-->
<c:url value="/resources/images" var="images">
</c:url>
</head>



<!-- ======= BODY ======= -->
<body>

	<!-- ===================== NAVIGATION BAR ===================== -->
	<div>
		<jsp:include page="layout/nav-bar.jsp"></jsp:include>
	</div>


	<!-- ===================== PAGINE ACCESIBILI/VISIBILI  IN BASE AL RUOLO: MANAGER ===================== -->

	<sec:authorize access="hasRole('EMPLOYEE')">
		<div class="container my-1 p-1  bg-primary text-white">
			<h2>EMPLOYEE SECTION</h2>
		</div>
		<div class="container border">
			<p>
				<a href="${pageContext.request.contextPath}/employees">Enter in
					the reserved Area</a> (Only Employees user(s) can access!!!)
			</p>
		</div>
	</sec:authorize>

	<!-- ===================== PAGINE ACCESIBILI/VISIBILI  IN BASE AL RUOLO: MANAGER ===================== -->

	<sec:authorize access="hasRole('MANAGER')">
		<hr>
		<div class="container my-1 p-1  bg-primary text-white">
			<h2>MANAGER SECTION</h2>
		</div>
		<div class="container border">
			<!-- Add a link to point to /leaders...this is for the managers  -->
			<p>
				<a href="${pageContext.request.contextPath}/leaders">Enter in
					the reserved Area</a> (Only Manager user(s) can access!!!)
			</p>
		</div>
		<hr>
	</sec:authorize>

	<!-- ===================== PAGINE ACCESIBILI/VISIBILI IN BASE AL RUOLO: ADMIN ===================== -->

	<sec:authorize access="hasRole('ADMIN')">
		<div class="container my-1 p-1  bg-primary text-white">
			<h2>ADMIN SECTION</h2>
		</div>
		<div class="container border">
			<!-- Add a link to point to /systems...this is for the admins  -->
			<p>
				<a href="${pageContext.request.contextPath}/systems">Enter in
					the reserved Area</a> (Only Admin user(s) can access!!!)
			</p>
		</div>
		<hr>
	</sec:authorize>
</body>
</html>