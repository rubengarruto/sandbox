<!--====================================== LIBRARIES ======================================-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!--=======================================================================================-->



<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>REXTART-DEMO</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--========================  Latest compiled and minified CSS ============================-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">


<!--================================  jQuery library ======================================-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<!--==================================  Popper JS =========================================-->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<!--=======================================================================================-->


<!--=========================  Latest compiled JavaScript =================================-->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>


<body>
	<!-- ======= NAVIGATION BAR ======= -->
	<div>
		<jsp:include page="../layout/nav-bar.jsp"></jsp:include>
	</div>

	<!-- ======= HEADER ======= -->
	<div class="container p-3 my-3 bg-primary text-white" align="center">
		<h2>DATI ANAGRAFICI</h2>		
	</div>

	<!-- ======= CONTAINER CON BORDO ======= -->
	<div class="container border">
		<table id="table-1">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Cognome</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Ruben</td>
					<td>Garruto</td>
					<td>rg@mail.it</td>
				</tr>
			</tbody>			
		</table>		
	</div>
</body>
</html>