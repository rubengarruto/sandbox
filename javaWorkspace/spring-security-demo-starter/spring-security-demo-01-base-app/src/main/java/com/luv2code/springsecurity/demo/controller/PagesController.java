package com.luv2code.springsecurity.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PagesController {

	
	// ------------  MAIN PAGE ------------ 
	@GetMapping(value = { "/", "home", "mainPage" })
	public String showHome() {
		// return "home";
		return "main-page";
	}
	
	
	// ------------  BOOT PAGES ------------
	@GetMapping("/boot")
	public String showFirstBootstrapPage() {
		return "bootstrap1/bootstrap1";
	}
	
	
	// ------------  ANAGRAPHIC PAGES ------------
	@GetMapping("/userAnag")
	public String userAnag() {
		return "anagrafica/anagrafica";
	}
	
	// ------------  EMPLOYEES PAGES ------------
		@GetMapping("/employees")
		public String showEmployesHome() {
			return "SectionEmployees/employeHome";
		}	

	
	// ------------  LEADERS PAGES ------------
	@GetMapping("/leaders")
	public String showLeadersHome() {
		return "SectionManager/leadersHome";
	}	
	
	
	// ------------  ADMIN PAGES ------------
	@GetMapping("/systems")
	public String showAdminHome() {
		return "SectionAdmin/adminHome";
	}
}
