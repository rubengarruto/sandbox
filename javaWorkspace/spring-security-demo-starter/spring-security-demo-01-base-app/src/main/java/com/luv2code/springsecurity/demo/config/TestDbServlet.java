package com.luv2code.springsecurity.demo.config;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestDbServlet
 * 
 * to test the connection right-click on the class and: "run on server"
 * 
 */
@WebServlet("/TestDbServlet")
public class TestDbServlet extends HttpServlet {
	

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// setup connection varibles
		String userDB = "springstudent";
		String passDB = "springstudent";
		String connectionDBUrl = "jdbc:mysql://localhost:3306/web_customer_tracker?useSSL=false&serverTimezone=UTC";
		String driver = "com.mysql.cj.jdbc.Driver";
		
		// get connection to database
		try {
			PrintWriter out=response.getWriter();
			
			out.println("Connecting to database --> "+connectionDBUrl);
			
			Class.forName(driver);
			Connection myConn= DriverManager.getConnection(connectionDBUrl, userDB, passDB);
			
			out.println("\nSuccesfully connected to database --> "+connectionDBUrl);
			
		} catch (Exception exc) {
			exc.printStackTrace();
			throw new ServletException();
		}

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

}

