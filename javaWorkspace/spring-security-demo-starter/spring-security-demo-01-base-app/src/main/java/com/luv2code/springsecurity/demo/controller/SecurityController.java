package com.luv2code.springsecurity.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecurityController {

	// ------------ CUSTOM LOGIN PAGE ------------
	@GetMapping("/showMyLoginPage")
	public String showMyLoginPage() {

		// return "basic-login";
		return "fancy-login";
// return "rextart-login";
	}

	// ------------ CUSTOM 403 ACCESS DENIED PAGE ------------
	@GetMapping("/access-denied")
	public String showAccessDenied() {

		return "access-denied";
// return "robot-403-denied";
	}
}
