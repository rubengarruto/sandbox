package com.luv2code.springboot.cruddemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.luv2code.springboot.cruddemo.entity.Employee;


@RepositoryRestResource(path="employees") //NOTE: /employees è il default e quindi non servirebbe specificarlo ma si puàò modificare a picere
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
