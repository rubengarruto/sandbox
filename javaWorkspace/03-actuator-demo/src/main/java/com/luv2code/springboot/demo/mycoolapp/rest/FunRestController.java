package com.luv2code.springboot.demo.mycoolapp.rest;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FunRestController {
		
	@Value("${admin.name}")
	private String adminName;
	
	// expose "/" that return "Hello World"
	
	@GetMapping("/")
	public String sayHello() {
		return "Hello World! Time on server is " + LocalDateTime.now()+"\n by Admin: "+adminName;
	}
	
	
	//expose a new endpoint for "Workout"
	@GetMapping("/workout")
	public String getDailyWorkout() {
		return "Run 5 km!";		
	}
	
	
	//expose a new endpoint for "Fortune"
		@GetMapping("/fortune")
		public String getDailyFortune() {
			return "Today is a beautiful day!";		
		}
		
	
}












