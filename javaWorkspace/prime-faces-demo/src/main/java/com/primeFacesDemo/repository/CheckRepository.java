package com.primeFacesDemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.primeFacesDemo.entity.Check;

public interface CheckRepository extends JpaRepository<Check, Integer>{

}
