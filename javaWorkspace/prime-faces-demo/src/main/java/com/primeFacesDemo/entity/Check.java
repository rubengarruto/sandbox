package com.primeFacesDemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.URL;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Check {

	@Id
	@GeneratedValue
	private int id;

	@Size(min = 1, message = "W�, manca il nome qua!")
	private String name;

	@Size(min = 1, message = "W�, manca l'url qua!")
	@URL(message="Invalid url!")
	private String url;

}
