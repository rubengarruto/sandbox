package com.primeFacesDemo.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.primeFacesDemo.entity.Check;
import com.primeFacesDemo.service.CheckService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ManagedBean(name="checkListController") //se non metto nulla helloController � il nome di default es. MyClasse() --> myClasse
public class CheckListController {	


	@ManagedProperty("#{checkService}")
	private CheckService checkService;	
	
	private List<Check> checks;
	private String str;
	
	@PostConstruct
	public void loadChecks() {
		checks = checkService.findAll();
			}	
	
	public void sayHello() {
		str = checkService.sayHello();
	}
	
	



	
	
}
