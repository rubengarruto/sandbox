package com.primeFacesDemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.primeFacesDemo.entity.Check;
import com.primeFacesDemo.repository.CheckRepository;
@Service
@Transactional(propagation=Propagation.REQUIRED,readOnly = false,rollbackFor = Exception.class)
public class CheckService {
	
	@Autowired
	private CheckRepository checkRepository;
	
	public List<Check> findAll() {
		return checkRepository.findAll();
	}

	public String sayHello() {
		
		return "Ciao da CheckService()";
	}
}
