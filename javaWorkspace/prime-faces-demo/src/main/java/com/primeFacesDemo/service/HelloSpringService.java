package com.primeFacesDemo.service;

import org.springframework.stereotype.Service;

@Service("helloSpringService") // puoi chiamarlo come vuoi basta che poi lo cambi nel controller in cui viene usato
public class HelloSpringService {

	public String sayHello() {
		return "Auguri a te e famiglia da un servizio";
	}
}