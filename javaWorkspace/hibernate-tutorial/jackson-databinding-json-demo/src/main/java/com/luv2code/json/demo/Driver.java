package com.luv2code.json.demo;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Driver {

	public static void main(String[] args)  {

		try {

			// create object mapper
			ObjectMapper mapper = new ObjectMapper();

			// read JSON file and map/convert to Java POJO: data/sample.json
			Student theStudent = mapper.readValue(new File("data/sample-full.json"), Student.class);

			// print
			System.out.println("First Name:" + theStudent.getFirstName());
			System.out.println("Last Name:" + theStudent.getLastName());
			System.out.println("City:" + theStudent.getAddress().getCity());

		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		
	}

}
