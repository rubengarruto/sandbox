package com.luv2code.hibernate.demo;

import java.text.ParseException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) throws ParseException {

		// create session factory
		SessionFactory sessionFactory = new Configuration().configure("hibernate_student.cfg.xml")
				.addAnnotatedClass(Student.class).buildSessionFactory();

		// create session
		Session session = sessionFactory.getCurrentSession();

		try {
			// use the session object to save Java object

			// create a student object
			System.out.println("Creating a new student object...");
			Student tempStudent = new Student( "Maccio", "Capatonda", "01/01/0000", "maccio@gmail.com");

			// start a transaction
			session.beginTransaction();

			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent);

			// commit transaction
			session.getTransaction().commit();
			System.out.println("DONE!!!");

		} finally {
			sessionFactory.close();
		}

	}

}
