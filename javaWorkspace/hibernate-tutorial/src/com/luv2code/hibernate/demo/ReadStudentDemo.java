package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
				.configure("hibernate_student.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// create session
		Session session=factory.getCurrentSession();
		
		
		try {
			//use the session object to save Java object
			
			//create a student object
			System.out.println("Creating a new student object...");
			Student tempStudent= new Student("Maccio","Capatonda","maccio@gmail.com");
			
			//start a transaction
			session.beginTransaction();
			
			//save the student object
			System.out.println("Saving the student...");
			System.out.println(tempStudent);
			session.save(tempStudent);
	
			//commit transaction
			session.getTransaction().commit();
			
			// find out the student's id: primary key
			System.out.println("Saved Student. Generated id:"+tempStudent.getId());
			
			//now get a new session and start transaction
			session = factory.getCurrentSession();
			session.beginTransaction();			
			
			//retrieve student based on the id
			System.out.println("\n Getting student with id: "+ tempStudent.getId());
			Student myStudent=session.get(Student.class, tempStudent.getId());
			System.out.println("Get complete:" + myStudent);
			
			//commit transaction
			session.getTransaction().commit();
			
			
			System.out.println("DONE!!!");
			
			}
		finally {
			factory.close();
		}
		
		
	}

}
