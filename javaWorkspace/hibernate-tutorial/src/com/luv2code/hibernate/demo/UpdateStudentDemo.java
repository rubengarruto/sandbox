package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate_student.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			int studentId = 2;

			// start a transaction
			session.beginTransaction();

			// UPDATE 1: ---------------------------------------------------------------------
			// the student witd id=1
			System.out.println("\nGetting student with id:" + studentId);
			Student myStudent = session.get(Student.class, studentId);
			System.out.println("Updating student id:" + studentId);
			myStudent.setFirstName("Luigi");
			System.out.println("updated!\n");

			// commit transaction
			session.getTransaction().commit();
			System.out.println("UPDATE 1 saved at DB");

			// UPDATE 2: ---------------------------------------------------------------------
			// update all the emails of all the students
			
			

			// create session
			System.out.println("Creating and starting a new session");
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			
			System.out.println("Updating the emails...");
			session.createQuery("UPDATE Student set email ='updated@mail.com'").executeUpdate();
			
			
			
			// commit transaction
			session.getTransaction().commit();
			System.out.println("UPDATE 2 saved at DB");

		} finally {
			factory.close();
		}
	}
}
