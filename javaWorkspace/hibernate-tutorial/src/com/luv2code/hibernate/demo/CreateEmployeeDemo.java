package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Employee;


public class CreateEmployeeDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory sessionFactory = new Configuration().configure("hibernate_employee.cfg.xml")
				.addAnnotatedClass(Employee.class).buildSessionFactory();

		// create session
		Session session = sessionFactory.getCurrentSession();

		try {
			// use the session object to save Java object

			// create a student object
			System.out.println("Creating a new employee object...");
			Employee tempEmployee1 = new Employee("Mario", "Rossi", "Rextart");
			Employee tempEmployee2 = new Employee("Giuseppe", "Verdi", "Rextart");
			Employee tempEmployee3 = new Employee("Luigi", "Verdi", "Accenture");
			Employee tempEmployee4 = new Employee("Luca", "Viola", "Accenture");
			Employee tempEmployee5 = new Employee("Mirko", "Arancio", "Reply");
			Employee tempEmployee6 = new Employee("Marino", "Rossi", "Reply");

			// start a transaction
			session.beginTransaction();

			// save the student object
			System.out.println("Saving the employee...");
					
			session.save(tempEmployee1);
			session.save(tempEmployee2);
			session.save(tempEmployee3);
			session.save(tempEmployee4);
			session.save(tempEmployee5);
			session.save(tempEmployee6);	

			// commit transaction
			session.getTransaction().commit();
			System.out.println("DONE!!!");

		} finally {
			sessionFactory.close();
		}

	}

}
