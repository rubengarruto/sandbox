package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {

			// start a transaction
			session.beginTransaction();

			// QUERY 1:
			// display all the students
			System.out.println("\nQUERY 1");
			List<Student> theStudents = session.createQuery("from Student").getResultList();
			displayStudents(theStudents);

			// QUERY 2:
			// search for student with surname ="Capatonda"
			System.out.println("\nQUERY 2");
			theStudents = session.createQuery("from Student s where s.lastName='Capatonda'").getResultList();
			displayStudents(theStudents);

			// QUERY 3:
			// search for student with surname ="Capatonda" or name = 'Gianni'
			System.out.println("\nQUERY 3");
			theStudents = session.createQuery("FROM Student s WHERE " + "s.lastName='Capatonda'OR s.firstName='Gianni'")
					.getResultList();
			displayStudents(theStudents);

			// QUERY 4:
			// search for student that ave '...apa...' in the surname
			System.out.println("\nQUERY 4");
			theStudents = session.createQuery("from Student s Where " + "s.lastName LIKE '%apa%'").getResultList();
			displayStudents(theStudents);

			// commit transaction
			session.getTransaction().commit();
			System.out.println("\nDONE!!!");

		} finally {
			factory.close();
		}

	}

	private static void displayStudents(List<Student> theStudents) {
		for (Student tempStudent : theStudents) {
			System.out.println(tempStudent);
		}
	}

}
