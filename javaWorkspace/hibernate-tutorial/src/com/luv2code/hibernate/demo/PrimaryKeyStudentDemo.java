package com.luv2code.hibernate.demo;

import java.text.ParseException;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.luv2code.hibernate.demo.entity.Student;

public class PrimaryKeyStudentDemo {

	public static void main(String[] args) throws ParseException {
		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate_student.cfg.xml")
				.addAnnotatedClass(Student.class).buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// use the session object to save Java object

			// create 3 student objects
			System.out.println("Creating  new students object...");

			String theDateOfBirthStr1 = "26/12/1998";
			Date theDateOfBirth1 = DateUtils.parseDate(theDateOfBirthStr1);

			String theDateOfBirthStr2 = "15/12/1989";
			Date theDateOfBirth2 = DateUtils.parseDate(theDateOfBirthStr2);

			String theDateOfBirthStr3 = "22/12/1992";
			Date theDateOfBirth3 = DateUtils.parseDate(theDateOfBirthStr3);

			Student tempStudent1 = new Student("Mimmo", "Memo", theDateOfBirth1, "mimmo@gmail.com");
			Student tempStudent2 = new Student("Mario", "Capatosta", theDateOfBirth2, "mario@gmail.com");
			Student tempStudent3 = new Student("Gianni", "Zaier", theDateOfBirth3, "zaier@gmail.com");

			// start a transaction
			session.beginTransaction();

			// save the student object
			System.out.println("Saving the student...");
			session.save(tempStudent1);
			session.save(tempStudent2);
			session.save(tempStudent3);

			// commit transaction
			session.getTransaction().commit();
			System.out.println("DONE!!!");

		} finally {
			factory.close();
		}

	}

}
