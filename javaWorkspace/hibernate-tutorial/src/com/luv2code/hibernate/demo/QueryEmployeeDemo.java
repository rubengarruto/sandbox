package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Employee;
import com.luv2code.hibernate.demo.entity.Student;

public class QueryEmployeeDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate_employee.cfg.xml")
				.addAnnotatedClass(Employee.class).buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {

			// start a transaction
			session.beginTransaction();

			// QUERY 1:
			// display all the students
			System.out.println("\nQUERY 1");
			List<Employee> theEmployees = session.createQuery("from Employee").getResultList();
			displayEmployee(theEmployees);

			// QUERY 2:
			// search for student with company="Rextart"
			System.out.println("\nQUERY 2");
			theEmployees = session.createQuery("from Employee e where e.company='Rextart'").getResultList();
			displayEmployee(theEmployees);

			// commit transaction
			session.getTransaction().commit();
			System.out.println("\nDONE!!!");

		} finally {
			factory.close();
		}

	}

	private static void displayEmployee(List<Employee> theEmployees) {
		for (Employee tempEmployee : theEmployees) {
			System.out.println(tempEmployee);
		}
	}

}
