package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Employee;
import com.luv2code.hibernate.demo.entity.Student;

public class ReadEmployeeDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate_employee.cfg.xml")
				.addAnnotatedClass(Employee.class).buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			// create a student object and save -------------------------------------------------
			System.out.println("Creating a new employee object...");
			Employee tempEmployee = new Employee("Mimmo", "Spada", "Modis");			
			session.beginTransaction();			
			System.out.println("Saving the Employee...");
			System.out.println(tempEmployee);
			session.save(tempEmployee);
			session.getTransaction().commit();
			System.out.println("Saved Employeed. Generated id:" + tempEmployee.getId());
			
			// retrieve an employee based on its id -------------------------------------------------
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("\nGetting employee with id: "+ tempEmployee.getId());
			Employee myEmployee=session.get(Employee.class, tempEmployee.getId());
			System.out.println("Get complete -->" + myEmployee);			
			session.getTransaction().commit();		
			
			
	

		} finally {
			factory.close();
		}

	}

}
