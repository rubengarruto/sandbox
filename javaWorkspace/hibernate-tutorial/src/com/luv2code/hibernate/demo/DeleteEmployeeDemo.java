package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Employee;

public class DeleteEmployeeDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate_employee.cfg.xml").addAnnotatedClass(Employee.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();

		try {
			

			// start a transaction ---------------------------------------------------------------------
			session.beginTransaction();

	
			// DELETE 1: ---------------------------------------------------------------------			
			System.out.println("Deleting a employee...");
			session.createQuery("DELETE FROM Employee where firstName = 'Mimmo'").executeUpdate();			
			session.getTransaction().commit();
			System.out.println("Deleted by DB");

		} finally {
			factory.close();
		}
	}
}
