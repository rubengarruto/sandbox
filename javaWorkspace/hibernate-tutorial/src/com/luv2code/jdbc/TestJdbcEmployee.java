package com.luv2code.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;


//Esempio di conessione ad un database
public class TestJdbcEmployee {

	public static void main(String[] args) {

		// nome DB -> hb_student_tracker
		String jdbcUrl ="jdbc:mysql://localhost:3306/hb_student_tracker?useSSL=false&serverTimezone=UTC";
		String user="hbstudent";
		String pass="hbstudent";
		
		
		try {
			
			System.out.println("Connecting to database:"+jdbcUrl);
			Connection myConn=DriverManager.getConnection(jdbcUrl,user,pass);
			System.out.println("Succesfully connected! "+jdbcUrl);

		} catch (Exception exc) {
			exc.printStackTrace();

		}

	}

}
